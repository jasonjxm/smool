package org.smool.sdk.wizard.j2segenerator.extensions.thingml;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.script.ScriptException;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.smool.sdk.ontmodel.ontmodel.Ontology;
import org.smool.sdk.ui.wizard.data.KPProjectData;
import org.smool.sdk.wizard.j2segenerator.J2SEGeneratorActivator;
import org.smool.sdk.wizard.j2segenerator.extensions.ParserTemplateLiteral;

/**
 * Extension to use ThingML features inside SMOOL.
 * <p>
 * https://github.com/TelluIoT/ThingML
 * </p>
 * <p>
 * ThingML license: Apache License Version 2.0
 * </p>
 */
public class ThingML {

	public static boolean isInstalled() {
		return (Platform.getBundle("thingml") != null);
	}

	public static void generateProject(IProject project, KPProjectData projectData, Ontology ontModel)
			throws CoreException, IOException, ScriptException {
		// prepare variables
		Map<String, Object> data = collectData(project, projectData, ontModel);

		// create ThingML project
		IFolder thingMLFolder = project.getFolder("thingml");
		thingMLFolder.create(false, true, null);

		// create parser
		ParserTemplateLiteral parser = new ParserTemplateLiteral(data);

		// get template files
		URL url = FileLocator.find(J2SEGeneratorActivator.getDefault().getBundle(),
				new org.eclipse.core.runtime.Path("resources/thingml"), null);
		URL urlFolder = FileLocator.toFileURL(url);
		Path folder = Paths.get(urlFolder.getFile());
		List<Path> paths = Files.walk(folder, 1).collect(Collectors.toList());
		paths.remove(0); // remove parent folder

		// for each template->set real values
		for (Path path : paths) {
			StringBuilder sb = new StringBuilder();
			Stream<String> lines = Files.lines(path);
			lines.forEach(text -> sb.append(text + '\n'));
			lines.close();
			String template = sb.toString();
			String result = parser.parse(template);
			Files.write(Paths.get(thingMLFolder.getRawLocation().toString(), path.getFileName().toString()),
					result.getBytes());
		}

	}

	private static Map<String, Object> collectData(IProject project, KPProjectData projectData, Ontology ontModel) {
		Map<String, Object> data = new HashMap<>();
		data.put("smool.projectName", project.getName());

		boolean isProducer = projectData.producedConcepts.size() > 0 ? true : false;
		boolean isConsumer = projectData.consumedConcepts.size() > 0 ? true : false;
		boolean isProsumer = isProducer && isConsumer ? true : false;
		String mainClass = isProsumer ? "ProsumerMain" : isConsumer ? "ConsumerMain" : "ProducerMain";

		String start = "try{ ProducerMain.main(new String[0]); }catch(Exception e){ e.printStackTrace(); }"
				.replace("ProducerMain", mainClass);
		data.put("smool.start()", start);

		data.put("smool.producedConcepts", projectData.producedConcepts.toString());
		data.put("smool.consumedConcepts", projectData.consumedConcepts.toString());

		// ontModel.classes

		return data;
	}

}
