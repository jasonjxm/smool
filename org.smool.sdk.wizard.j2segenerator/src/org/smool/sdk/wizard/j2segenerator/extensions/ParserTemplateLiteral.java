package org.smool.sdk.wizard.j2segenerator.extensions;

import java.util.HashMap;
import java.util.Map;

/**
 * Parser for JS template literals style
 * 
 * <p>
 * Nashorn js engine is being deprecated in Java 11. So instead of adding a new
 * JS engine as dependency, better to use a simple to use/maintain plain Java
 * class
 * </p>
 * 
 * <p>
 * Example:
 * </p>
 * <p>
 * Js template literal: `eeee ${project.description}`
 * </p>
 * <p>
 * result: "eee this is a test"
 * </p>
 * 
 * 
 * @author ubuntu
 *
 */
public class ParserTemplateLiteral {

	public static String S1 = "\\$\\{";
	public static String S2 = "}";
	public Map<String, Object> map;

	public ParserTemplateLiteral() {
		map = new HashMap<>();
	}

	public ParserTemplateLiteral(Map<String, Object> data) {
		map = data;
	}

	/**
	 * set the key/value and return object to allow concatenating set() in the same
	 * sentence (fluent API)
	 **/
	public ParserTemplateLiteral set(String k, Object v) {
		map.put(k, v);
		return this;
	}

	public Object get(String k) {
		Object o = map.get(k);
		try {
			return o.toString();
		} catch (Exception e) {
			return "";
		}
	}

	public String parse(String template) {
		StringBuilder sb = new StringBuilder();
		int lastIndex = 0;
		String[] data = template.split(S1);
		if (data.length == 1)
			return template;
		for (int r = 0; r < data.length; r++) {
			if (r == 0)
				sb.append(data[r]);
			else {
				String text = data[r];
				int index = text.indexOf(S2);
				String key = text.substring(0, index).trim();
				sb.append(get(key).toString());
				sb.append(text.substring(index + 1));
			}
		}
		return sb.toString();
	}

//	public static void main(String[] args) {
//		JSTemplateParser parser = new JSTemplateParser();
//		parser.set("text.smool", "PRRRRUEBA").set("text", "AEIOU");
//		String template = "eeee ${text} V8 ${text.smool}";
//		System.out.println(parser.parse(template));
//	}
}
