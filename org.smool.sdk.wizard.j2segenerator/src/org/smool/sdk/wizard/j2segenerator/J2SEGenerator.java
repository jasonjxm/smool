package org.smool.sdk.wizard.j2segenerator;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Vector;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.launching.JavaRuntime;
import org.smool.sdk.ontmodel.ontmodel.Ontology;
import org.smool.sdk.ui.perspective.SmoolNature;
import org.smool.sdk.ui.wizard.data.KPProjectData;
import org.smool.sdk.ui.wizard.exceptions.SmoolGenerationException;
import org.smool.sdk.ui.wizard.generation.IProjectGenerator;
import org.smool.sdk.ui.wizard.utils.SmoolGenerationUtils;
import org.smool.sdk.wizard.j2segenerator.extensions.thingml.ThingML;

import es.esi.gemde.modeltransformator.ModelTransformatorPlugin;

/**
 * 
 * @author Adrian Noguero (adrian.noguero@tecnalia.com)
 *
 */
public class J2SEGenerator implements IProjectGenerator {

	private IFolder srcFolder = null;
	private IFolder libFolder = null;
	private IProject project;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.smool.sdk.ui.wizard.generation.IProjectGenerator#generateProject(java.
	 * lang.String)
	 */
	@Override
	public void generateProject(String projectName) throws SmoolGenerationException {

		try {
			// Create the project, open it and convert it to a J2SE project
			IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
			IProject project = root.getProject(projectName);
			project.create(null);
			project.open(null);

			J2SEGeneratorActivator.getDefault().setProject(project);

			IFolder binFolder = project.getFolder("bin");
			binFolder.create(false, true, null);

			IJavaProject javaProject = JavaCore.create(project);

			// Set the nature of the project
			IProjectDescription description = project.getDescription();
			String[] natures = description.getNatureIds();
			String[] newNatures = new String[natures.length + 2];
			System.arraycopy(natures, 0, newNatures, 0, natures.length);
			newNatures[natures.length] = SmoolNature.NATURE_ID;
			newNatures[natures.length + 1] = JavaCore.NATURE_ID;
			IStatus status = ResourcesPlugin.getWorkspace().validateNatureSet(natures);

			// check the status and decide what to do
			if (status.getCode() == IStatus.OK) {
				description.setNatureIds(newNatures);
				project.setDescription(description, null);
			} else {
				throw new SmoolGenerationException("Nature is not a valid nature for the current workspace.", null);
			}

			// Configure Java project
			javaProject.setRawClasspath(new IClasspathEntry[0], null);
			javaProject.setOption(JavaCore.COMPILER_COMPLIANCE, JavaCore.VERSION_10);

			// Create the folder structure & configure
			javaProject.setOutputLocation(binFolder.getFullPath(), null);

			srcFolder = project.getFolder("src");
			srcFolder.create(false, true, null);
			IPackageFragmentRoot fragmentRoot = javaProject.getPackageFragmentRoot(srcFolder);
			IClasspathEntry[] oldEntries = javaProject.getRawClasspath();
			IClasspathEntry[] newEntries = new IClasspathEntry[oldEntries.length + 2];
			System.arraycopy(oldEntries, 0, newEntries, 0, oldEntries.length);
			newEntries[oldEntries.length] = JavaRuntime.getDefaultJREContainerEntry();
			newEntries[oldEntries.length + 1] = JavaCore.newSourceEntry(fragmentRoot.getPath());
			javaProject.setRawClasspath(newEntries, null);

			libFolder = project.getFolder("lib");
			libFolder.create(false, true, null);

			InputStream is = null;
			FileOutputStream os = null;

			// Copy LOG4J_LIB
			is = FileLocator.openStream(J2SEGeneratorActivator.getDefault().getBundle(),
					new Path(J2SEGeneratorActivator.PREFIX + J2SEGeneratorActivator.LOG4J_LIB), false);
			IFile log4jFile = libFolder.getFile(J2SEGeneratorActivator.LOG4J_LIB);
			os = new FileOutputStream(libFolder.getLocation().append("/" + J2SEGeneratorActivator.LOG4J_LIB).toFile());
			SmoolGenerationUtils.streamCopy(is, os);

			// Copy JSR173_LIB
			is = FileLocator.openStream(J2SEGeneratorActivator.getDefault().getBundle(),
					new Path(J2SEGeneratorActivator.PREFIX + J2SEGeneratorActivator.JSR173_LIB), false);
			IFile jsr173File = libFolder.getFile(J2SEGeneratorActivator.JSR173_LIB);
			os = new FileOutputStream(libFolder.getLocation().append("/" + J2SEGeneratorActivator.JSR173_LIB).toFile());
			SmoolGenerationUtils.streamCopy(is, os);

			// Copy SJSXP_LIB
			is = FileLocator.openStream(J2SEGeneratorActivator.getDefault().getBundle(),
					new Path(J2SEGeneratorActivator.PREFIX + J2SEGeneratorActivator.SJSXP_LIB), false);
			IFile sjsxpFile = libFolder.getFile(J2SEGeneratorActivator.SJSXP_LIB);
			os = new FileOutputStream(libFolder.getLocation().append("/" + J2SEGeneratorActivator.SJSXP_LIB).toFile());
			SmoolGenerationUtils.streamCopy(is, os);

			// Copy XALAN_LIB
			is = FileLocator.openStream(J2SEGeneratorActivator.getDefault().getBundle(),
					new Path(J2SEGeneratorActivator.PREFIX + J2SEGeneratorActivator.XALAN_LIB), false);
			IFile xalanFile = libFolder.getFile(J2SEGeneratorActivator.XALAN_LIB);
			os = new FileOutputStream(libFolder.getLocation().append("/" + J2SEGeneratorActivator.XALAN_LIB).toFile());
			SmoolGenerationUtils.streamCopy(is, os);

			// Copy COMMONS_LANG_LIB
			is = FileLocator.openStream(J2SEGeneratorActivator.getDefault().getBundle(),
					new Path(J2SEGeneratorActivator.PREFIX + J2SEGeneratorActivator.COMMONS_LANG_LIB), false);
			IFile commonsLangFile = libFolder.getFile(J2SEGeneratorActivator.COMMONS_LANG_LIB);
			os = new FileOutputStream(
					libFolder.getLocation().append("/" + J2SEGeneratorActivator.COMMONS_LANG_LIB).toFile());
			SmoolGenerationUtils.streamCopy(is, os);

			// Copy COMMON_LIB
			is = FileLocator.openStream(J2SEGeneratorActivator.getDefault().getBundle(),
					new Path(J2SEGeneratorActivator.PREFIX + J2SEGeneratorActivator.COMMON_LIB), false);
			IFile commonFile = libFolder.getFile(J2SEGeneratorActivator.COMMON_LIB);
			os = new FileOutputStream(libFolder.getLocation().append("/" + J2SEGeneratorActivator.COMMON_LIB).toFile());
			SmoolGenerationUtils.streamCopy(is, os);

			// Copy MODEL_LIB
			is = FileLocator.openStream(J2SEGeneratorActivator.getDefault().getBundle(),
					new Path(J2SEGeneratorActivator.PREFIX + J2SEGeneratorActivator.MODEL_LIB), false);
			IFile modelFile = libFolder.getFile(J2SEGeneratorActivator.MODEL_LIB);
			os = new FileOutputStream(libFolder.getLocation().append("/" + J2SEGeneratorActivator.MODEL_LIB).toFile());
			SmoolGenerationUtils.streamCopy(is, os);

			// Copy MODEL_DIRECT_LIB
			is = FileLocator.openStream(J2SEGeneratorActivator.getDefault().getBundle(),
					new Path(J2SEGeneratorActivator.PREFIX + J2SEGeneratorActivator.MODEL_DIRECT_LIB), false);
			IFile modelDirectFile = libFolder.getFile(J2SEGeneratorActivator.MODEL_DIRECT_LIB);
			os = new FileOutputStream(
					libFolder.getLocation().append("/" + J2SEGeneratorActivator.MODEL_DIRECT_LIB).toFile());
			SmoolGenerationUtils.streamCopy(is, os);

			// Copy MODEL_SMART_LIB
			is = FileLocator.openStream(J2SEGeneratorActivator.getDefault().getBundle(),
					new Path(J2SEGeneratorActivator.PREFIX + J2SEGeneratorActivator.MODEL_SMART_LIB), false);
			IFile modelSmartFile = libFolder.getFile(J2SEGeneratorActivator.MODEL_SMART_LIB);
			os = new FileOutputStream(
					libFolder.getLocation().append("/" + J2SEGeneratorActivator.MODEL_SMART_LIB).toFile());
			SmoolGenerationUtils.streamCopy(is, os);

			// Copy SSAP_LIB
			is = FileLocator.openStream(J2SEGeneratorActivator.getDefault().getBundle(),
					new Path(J2SEGeneratorActivator.PREFIX + J2SEGeneratorActivator.SSAP_LIB), false);
			IFile ssapFile = libFolder.getFile(J2SEGeneratorActivator.SSAP_LIB);
			os = new FileOutputStream(libFolder.getLocation().append("/" + J2SEGeneratorActivator.SSAP_LIB).toFile());
			SmoolGenerationUtils.streamCopy(is, os);

			oldEntries = javaProject.getRawClasspath();
			newEntries = new IClasspathEntry[oldEntries.length + 10];
			System.arraycopy(oldEntries, 0, newEntries, 0, oldEntries.length);
			newEntries[oldEntries.length] = JavaCore.newLibraryEntry(commonFile.getFullPath(), null, null);
			newEntries[oldEntries.length + 1] = JavaCore.newLibraryEntry(modelFile.getFullPath(), null, null);
			newEntries[oldEntries.length + 2] = JavaCore.newLibraryEntry(ssapFile.getFullPath(), null, null);
			newEntries[oldEntries.length + 3] = JavaCore.newLibraryEntry(commonsLangFile.getFullPath(), null, null);
			newEntries[oldEntries.length + 4] = JavaCore.newLibraryEntry(jsr173File.getFullPath(), null, null);
			newEntries[oldEntries.length + 5] = JavaCore.newLibraryEntry(log4jFile.getFullPath(), null, null);
			newEntries[oldEntries.length + 6] = JavaCore.newLibraryEntry(sjsxpFile.getFullPath(), null, null);
			newEntries[oldEntries.length + 7] = JavaCore.newLibraryEntry(xalanFile.getFullPath(), null, null);
			newEntries[oldEntries.length + 8] = JavaCore.newLibraryEntry(modelDirectFile.getFullPath(), null, null);
			newEntries[oldEntries.length + 9] = JavaCore.newLibraryEntry(modelSmartFile.getFullPath(), null, null);
			javaProject.setRawClasspath(newEntries, null);
			this.project = project;
		} catch (Exception e) {
			e.printStackTrace();
			throw new SmoolGenerationException("The project generation process failed.", e);
		}

	}

	public void generateExternalAddons(KPProjectData projectData, Ontology ontModel) throws Exception {
		// Install Third-party extensions (if available)
		if (ThingML.isInstalled()) {
			ThingML.generateProject(project, projectData, ontModel);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.smool.sdk.ui.wizard.generation.IProjectGenerator#generateModelLayer(org.
	 * smool.sdk.ontmodel.ontmodel.Class[], org.smool.sdk.ontmodel.ontmodel.Class[])
	 */
	@Override
	public void generateModelLayer(Ontology ontModel) throws SmoolGenerationException {
		if (srcFolder != null && libFolder != null && ontModel != null) {
			Vector<EObject> inputs = new Vector<EObject>();
			inputs.add(ontModel);
			try {
				ModelTransformatorPlugin.getService().executeTransformation(inputs,
						J2SEGeneratorActivator.ONTMODEL2JAVA, J2SEGeneratorActivator.MOFSCRIPT,
						srcFolder.getLocation().toOSString());
			} catch (Exception e) {
				e.printStackTrace();
				throw new SmoolGenerationException("The project generation process failed.", e);
			}

			// OWL file is not needed any more!!
//			InputStream is = null;
//			FileOutputStream os = null;

//			try {
//				// Copy OWL file
//				is = FileLocator.openStream(WizardActivator.getDefault().getBundle(), new Path(WizardActivator.CORE_ONTOLOGY), false);
//				IFolder ontologyFolder = srcFolder.getFolder(J2SEGeneratorActivator.ONTOLOGY_FOLDER);
//				ontologyFolder.create(false, true, null);
//				//IFile owlFile = ontologyFolder.getFile(J2SEGeneratorActivator.ONTOLOGY_OWL_FILE);
//				os = new FileOutputStream(ontologyFolder.getLocation().append("/"+ J2SEGeneratorActivator.ONTOLOGY_OWL_FILE).toFile());
//				SmoolGenerationUtils.ontologyCopy(is, os);
//			}
//			catch (Exception e) {
//				e.printStackTrace();
//				throw new SmoolGenerationException("The project generation process failed.", e);
//			}
		}

	}

}
