package org.smool.sdk.wizard.j2segenerator;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.smool.sdk.ui.wizard.exceptions.SmoolGenerationException;
import org.smool.sdk.ui.wizard.generation.ICommLayerGenerator;
import org.smool.sdk.ui.wizard.utils.SmoolGenerationUtils;

/**
 * @author Adrian Noguero (adrian.noguero@tecnalia.com)
 *
 */
public class TCPIPCommLayerGenerator implements ICommLayerGenerator {

	@Override
	public void generateCommLayer(boolean autoConnect) throws SmoolGenerationException {
		// TODO Auto-generated method stub
		try {
			copyLibs();
		}
		catch (Exception e) {
			
		}
	}

	@Override
	public void generateCommLayer(boolean autoConnect, String address, String port) throws SmoolGenerationException {
		// TODO Auto-generated method stub
		try {
			copyLibs();
		}
		catch (Exception e) {
			
		}
	}
	
	private void copyLibs() throws IOException, JavaModelException {
		IProject p = J2SEGeneratorActivator.getDefault().getProject();
		
		IFolder libFolder = p.getFolder("lib");
		
		if (!libFolder.exists()) {
			return;
		}
		
		InputStream is = null;
		FileOutputStream os = null;
		
		// Copy CONNECTOR_LIB
		is = FileLocator.openStream(J2SEGeneratorActivator.getDefault().getBundle(), new Path(J2SEGeneratorActivator.PREFIX + J2SEGeneratorActivator.CONNECTOR_LIB), false);
		IFile connectorFile = libFolder.getFile(J2SEGeneratorActivator.CONNECTOR_LIB);
		os = new FileOutputStream(libFolder.getLocation().append("/"+ J2SEGeneratorActivator.CONNECTOR_LIB).toFile());
		SmoolGenerationUtils.streamCopy(is, os);
		
		// Copy TCPIP_LIB
		is = FileLocator.openStream(J2SEGeneratorActivator.getDefault().getBundle(), new Path(J2SEGeneratorActivator.PREFIX + J2SEGeneratorActivator.TCPIP_LIB), false);
		IFile tcpipFile = libFolder.getFile(J2SEGeneratorActivator.TCPIP_LIB);
		os = new FileOutputStream(libFolder.getLocation().append("/"+ J2SEGeneratorActivator.TCPIP_LIB).toFile());
		SmoolGenerationUtils.streamCopy(is, os);
		
		IJavaProject javaProject = JavaCore.create(p);
		IClasspathEntry[] oldEntries = javaProject.getRawClasspath();
		IClasspathEntry[] newEntries = new IClasspathEntry[oldEntries.length + 2];
		System.arraycopy(oldEntries, 0, newEntries, 0, oldEntries.length);
		newEntries[oldEntries.length] = JavaCore.newLibraryEntry(connectorFile.getFullPath(), null, null);
		newEntries[oldEntries.length + 1] = JavaCore.newLibraryEntry(tcpipFile.getFullPath(), null, null);
		javaProject.setRawClasspath(newEntries, null);
	}

}
