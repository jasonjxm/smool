package org.smool.sdk.wizard.j2segenerator;

import java.net.URL;
import java.util.Vector;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;
import org.smool.sdk.ontmodel.ontmodel.Ontology;

import es.esi.gemde.modeltransformator.ModelTransformatorPlugin;
import es.esi.gemde.modeltransformator.service.IModelTransformationService;
import es.esi.gemde.modeltransformator.service.ITransformation;
import es.esi.gemde.modeltransformator.service.TransformationFactory;

/**
 * The activator class controls the plug-in life cycle
 */
public class J2SEGeneratorActivator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "org.smool.sdk.wizard.j2segenerator"; //$NON-NLS-1$
	public static final String ONTMODEL2JAVA = "OntModel2Java"; //$NON-NLS-1$
	public static final String ONTMODEL2JAVA_FILE = "resources/Ontmodel2Java.m2t"; //$NON-NLS-1$
	public static final String MOFSCRIPT = "MOFScript"; //$NON-NLS-1$

	public static final String PREFIX = "kp_libs/"; //$NON-NLS-1$
	public static final String BT_LIB = "org.smool.kpi.connector.bluetooth_2.2.5.jar"; //$NON-NLS-1$
	public static final String COMMON_LIB = "org.smool.kpi.common_2.2.5.jar"; //$NON-NLS-1$
	public static final String CONNECTOR_LIB = "org.smool.kpi.connector_2.2.5.jar"; //$NON-NLS-1$
	public static final String MODEL_LIB = "org.smool.kpi.model_2.2.5.jar"; //$NON-NLS-1$
	public static final String MODEL_DIRECT_LIB = "org.smool.kpi.model.direct_2.2.5.jar"; //$NON-NLS-1$
	public static final String MODEL_SMART_LIB = "org.smool.kpi.model.smart_2.2.5.jar"; //$NON-NLS-1$
	public static final String SSAP_LIB = "org.smool.kpi.ssap_2.2.5.jar"; //$NON-NLS-1$
	public static final String TCPIP_LIB = "org.smool.kpi.connector.tcpip_2.2.5.jar"; //$NON-NLS-1$
	public static final String COMMONS_LANG_LIB = "commons-lang.jar"; //$NON-NLS-1$
	public static final String JSR173_LIB = "jsr173_api.jar"; //$NON-NLS-1$
	public static final String LOG4J_LIB = "log4j-1.2.9.jar"; //$NON-NLS-1$
	public static final String SJSXP_LIB = "sjsxp.jar"; //$NON-NLS-1$
	public static final String XALAN_LIB = "xalan-2.6.0.jar"; //$NON-NLS-1$

	public static final String ONTOLOGY_FOLDER = "ontology"; //$NON-NLS-1$
	public static final String ONTOLOGY_OWL_FILE = "ontology.owl"; //$NON-NLS-1$

	// The shared instance
	private static J2SEGeneratorActivator plugin;
	private IProject theProject;

	/**
	 * The constructor
	 */
	public J2SEGeneratorActivator() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.
	 * BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		loadTransformations();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static J2SEGeneratorActivator getDefault() {
		return plugin;
	}

	/**
	 * Load transformations into GEMDE
	 */
	private void loadTransformations() {
		try {
			IModelTransformationService s = ModelTransformatorPlugin.getService();
			Vector<Class<? extends EObject>> inputs2 = new Vector<Class<? extends EObject>>();
			Vector<java.net.URI> transformationURIs2 = new Vector<java.net.URI>();

			URL file2 = FileLocator.find(getBundle(), new Path(ONTMODEL2JAVA_FILE), null);
			file2 = FileLocator.toFileURL(file2);

			transformationURIs2.add(file2.toURI());
			inputs2.add(Ontology.class);

			ITransformation t2;
			t2 = TransformationFactory.createTransformation(ONTMODEL2JAVA, MOFSCRIPT, transformationURIs2, "", inputs2);
			s.addTransformation(t2);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setProject(IProject p) {
		theProject = p;
	}

	public IProject getProject() {
		return theProject;
	}

}
