/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.sibviewer.views;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.ViewPart;
import org.smool.sdk.sib.service.IViewer;
import org.smool.sdk.ui.sibviewer.Activator;

/**
 * This class implements the SIB viewer view. 
 * The class is used to visualize the information contained in SIB server registered in
 * the OSGI Framework. Each different server is represented by a tab in the view.
 * Within each server tabs, there are information related to sessions, subscriptions and
 * triples.
 * 
 * @author Cristina L�pez, cristina.lopez@tecnalia.com, ESI
 *
 */
public class SIBViewer extends ViewPart {

	/**
	 * Object containing the servers tabs created.
	 */
	private static CTabFolder serverTabs;
	
	/**
	 * Orginial composite of the view
	 */
	private static Composite composite;
	
	private static ArrayList<String> pendingSIBs = new ArrayList<String>();
	private static HashMap<String, IViewer> viewers = new HashMap<String, IViewer>();

	public SIBViewer() {
		super();
	}
	
	/** 
	 * Gets the server tabs
	 * @return CTabFolder
	 */
	public static CTabFolder getServerTabs() {
		return serverTabs;
	}

	/**
	 * Sets the server tabs
	 * @param serverTabs
	 */
	public static void setServerTabs(CTabFolder serverTabs) {
		SIBViewer.serverTabs = serverTabs;
	}

	/**
	 * Gets the view composite
	 * @return
	 */
	public static Composite getComposite() {
		return composite;
	}
	
	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.ViewPart
	 */
	public void createPartControl(Composite parent) {
		System.out.println("Creates a server tab");
		serverTabs = new CTabFolder(parent, SWT.BORDER);
		composite = parent;
		for (String sib : pendingSIBs) {
			createServer(sib, viewers.get(sib));
		}
	}	
	
	@Override
	public void init(IViewSite site) throws PartInitException {
		super.init(site);
	}
	
	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.ViewPart
	 */
	public void setFocus() {
		serverTabs.setFocus();
	}
	
	/**
	 * Initialize the title of the viewer 
	 * 
	 * @param name String
	 */
	public void initializeTitle(String name) {
		setPartName(name);
	}

	/**
	 * This method is used externally by the Viewer service tracker customizer
	 * to create a new server tab when a new Viewer service is discovered
	 * 
	 * @param name
	 * @param viewer 
	 * @return SIBVisualizer
	 */
	public static SIBVisualizer createServer(String name, IViewer viewer) {
		if (serverTabs != null) {
			CTabItem server = new CTabItem(serverTabs, SWT.NONE);
			server.setText(name);
			SIBVisualizer info = createInfoElements(serverTabs, name);
			server.setControl(info);		
			serverTabs.setSelection(server);
			
			if (info != null) {
				viewer.addListener(info.getSessionListener());
				viewer.addListener(info.getSuscriptionListener());
				viewer.addListener(info.getSemanticContentListener());
				viewer.addListener(info.getSIBPropertiesListener());

				Activator.getDefault().getVisualizerMap().put(name, info);
			}
			return info;
		}
		else {
			pendingSIBs.add(name);
			viewers.put(name, viewer);
			return null;
		}
	}
		
	/**
	 * This method is used externally by the Viewer service tracker customizer
	 * to delete a server tab when a Viewer service is removed
	 * 
	 * @param name
	 */
	public static void removeServer(String name){
		CTabItem[] listItems=serverTabs.getItems();
		CTabItem server=null;
		Boolean found=false;
		int i=0;
		while (i<listItems.length && found==false){
			if (listItems[i].getText().equals(name)){
				found=true;
				server=listItems[i];
			}
			i++;
		}
		server.dispose();
	}

	/**
	 * This method is used internally by the createServer method to created
	 * the Sessions, Subscription and Triples information tabs witin the server tab.
	 *  
	 * @param parent
	 * @param name
	 * @return SIBVisualizer
	 */
	private static SIBVisualizer createInfoElements(Composite parent, String name){
		SIBVisualizer infoTabs=new SIBVisualizer(parent, SWT.BORDER);
				
		TabSessions sessionItem=new TabSessions(infoTabs, SWT.NONE);
		sessionItem.setText("Sessions");
		
		TabSubscriptions subscriptionItem=new TabSubscriptions(infoTabs, SWT.NONE);
		subscriptionItem.setText("Subscriptions");
				
		TabTriples triplesItem=new TabTriples(infoTabs, SWT.NONE);
		triplesItem.setText("Triples");
		
		TabSIBProperties sibProperties = new TabSIBProperties(infoTabs, SWT.NONE);
		sibProperties.setText("SIB Properties");
		
		infoTabs.setSelection(sessionItem);
		
		return infoTabs;
	}
}
