/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.sibviewer.views;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.smool.sdk.sib.data.ISubscriptionHolderListener;
import org.smool.sdk.sib.data.Subscription;
import org.smool.sdk.ui.sibviewer.provider.subscription.SubscriptionContentProvider;
import org.smool.sdk.ui.sibviewer.provider.subscription.SubscriptionLabelProvider;


/**
 * This class is used to visualize the Subscriptions information of one particular Sib 
 * server.
 * The class itself is an extension of CTabItem.
 * The class is also an implementation of a listener. This means that if the internal
 * subscription information of a server changes, its subscriptions tab is updated with the
 * new information consequently. 
 * 
 * @author Cristina L�pez, cristina.lopez@tecnalia.com, ESI
 *
 */
public class TabSubscriptions extends CTabItem implements ISubscriptionHolderListener{
	/**
	 * List of subscription that are displayed in the tab
	 */
	private List<Subscription> listSubscriptions;	
	/**
	 * Subscription Id Column.
	 * This column display the id settled by the server to identify each specific 
	 * subscription.
	 */
	private TableColumn idColumn;
	/**
	 * Node Id Column.
	 * This column displays the Id's of the Smart Applications that have required
	 * a subscription to the server
	 */
	private TableColumn nodeColumn;
	/**
	 * Query Column.
	 * This column displays the subscription query petition.
	 */
	private TableColumn queryColumn;
	/**
	 * Result Column.
	 * This columns displays the result to the query petition.
	 */
	private TableColumn resultColumn;
	
	/**
	 * Date Column,
	 * This column displays the date of creationg/update the triple.
	 */
	private TableColumn dateColumn;	
	
	/**
	 * Owner Column,
	 * This column displays the owner kp.
	 */
	/*private TableColumn ownerColumn;	
	
	/**
	 * Viewer manager
	 */
	private TableViewer viewer;
	/**
	 * Internal table of the viewer
	 */
	private Table table;
	
	private SubscriptionSorter subscriptionSorter;	
	
	/**
	 * Constructor
	 * @param parent
	 * @param style
	 */
	public TabSubscriptions(CTabFolder parent, int style) {
		super(parent, style);
		this.listSubscriptions = new ArrayList<Subscription>();
		createTableStructure(parent);
		this.setControl(table);
	}

	/*****************************************/
	/******** Table Structure Methods ********/
	/*****************************************/
	
	/**
	 * This method sets the structure of the tab
	 */
	public void createTableStructure(Composite parent){
		try{
			viewer= new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION);
			table= viewer.getTable();
			
			subscriptionSorter = new SubscriptionSorter();
			/* CHANGE JASON MANSELL change setSorter to set Comparator due to deprecated method*/
			viewer.setComparator(subscriptionSorter);			
			
			/*** Editable table  ***/
			final TableEditor editor = new TableEditor(table);
			//The editor must have the same size as the cell and must
			//not be any smaller than 50 pixels.
			editor.horizontalAlignment = SWT.LEFT;
			editor.grabHorizontal = true;
			editor.minimumWidth = 200;
			// editing the second column
			final int EDITABLECOLUMN = 3;
			
			table.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					// Clean up any previous editor control
					Control oldEditor = editor.getEditor();
					if (oldEditor != null) oldEditor.dispose();
			
					// Identify the selected row
					TableItem item = (TableItem)e.item;
					if (item == null) return;
			
					// The control that will be the editor must be a child of the Table
					Text newEditor = new Text(table, SWT.NONE);
					newEditor.setText(item.getText(EDITABLECOLUMN));
					newEditor.addModifyListener(new ModifyListener() {
						public void modifyText(ModifyEvent me) {
							Text text = (Text)editor.getEditor();
							editor.getItem().setText(EDITABLECOLUMN, text.getText());
						}
					});
					newEditor.selectAll();
					newEditor.setFocus();
					editor.setEditor(newEditor, item, EDITABLECOLUMN);
				}
			});			
			/***********************/
			
			idColumn = new TableColumn(table, SWT.LEFT);
			idColumn.setText("Subscription Id");
			idColumn.setWidth(100);
			idColumn.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					subscriptionSorter.setColumn(0);
					int dir = viewer.getTable().getSortDirection();
					if (viewer.getTable().getSortColumn() == idColumn) {
						dir = dir == SWT.UP ? SWT.DOWN : SWT.UP;
					} else {
						dir = SWT.DOWN;
					}
					viewer.getTable().setSortDirection(dir);
					viewer.getTable().setSortColumn(idColumn);
					viewer.refresh();
				}
			});
			
			nodeColumn= new TableColumn(table, SWT.LEFT);
			nodeColumn.setText("Node Id");
			nodeColumn.setWidth(150);
			nodeColumn.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					subscriptionSorter.setColumn(1);
					int dir = viewer.getTable().getSortDirection();
					if (viewer.getTable().getSortColumn() == nodeColumn) {
						dir = dir == SWT.UP ? SWT.DOWN : SWT.UP;
					} else {
						dir = SWT.DOWN;
					}
					viewer.getTable().setSortDirection(dir);
					viewer.getTable().setSortColumn(nodeColumn);
					viewer.refresh();
				}
			});			
			
			queryColumn= new TableColumn(table ,SWT.LEFT);
			queryColumn.setText("Query");
			queryColumn.setWidth(400);
			queryColumn.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					subscriptionSorter.setColumn(2);
					int dir = viewer.getTable().getSortDirection();
					if (viewer.getTable().getSortColumn() == queryColumn) {
						dir = dir == SWT.UP ? SWT.DOWN : SWT.UP;
					} else {
						dir = SWT.DOWN;
					}
					viewer.getTable().setSortDirection(dir);
					viewer.getTable().setSortColumn(queryColumn);
					viewer.refresh();
				}
			});				
			
			resultColumn= new TableColumn(table ,SWT.LEFT);
			resultColumn.setText("Result");
			resultColumn.setWidth(400);
			resultColumn.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					subscriptionSorter.setColumn(3);
					int dir = viewer.getTable().getSortDirection();
					if (viewer.getTable().getSortColumn() == resultColumn) {
						dir = dir == SWT.UP ? SWT.DOWN : SWT.UP;
					} else {
						dir = SWT.DOWN;
					}
					viewer.getTable().setSortDirection(dir);
					viewer.getTable().setSortColumn(resultColumn);
					viewer.refresh();
				}
			});				
			
			dateColumn = new TableColumn(table, SWT.LEFT);
			dateColumn.setText("Last updated");
			dateColumn.setWidth(150);
			
			dateColumn.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					subscriptionSorter.setColumn(4);
					int dir = viewer.getTable().getSortDirection();
					if (viewer.getTable().getSortColumn() == dateColumn) {
						dir = dir == SWT.UP ? SWT.DOWN : SWT.UP;
					} else {

						dir = SWT.DOWN;
					}
					viewer.getTable().setSortDirection(dir);
					viewer.getTable().setSortColumn(dateColumn);
					viewer.refresh();
				}
			});			
			/*
			ownerColumn = new TableColumn(table, SWT.LEFT);
			ownerColumn.setText("Owner KP");
			ownerColumn.setWidth(250);
			
			ownerColumn.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					subscriptionSorter.setColumn(5);
					int dir = viewer.getTable().getSortDirection();
					if (viewer.getTable().getSortColumn() == ownerColumn) {
						dir = dir == SWT.UP ? SWT.DOWN : SWT.UP;
					} else {

						dir = SWT.DOWN;
					}
					viewer.getTable().setSortDirection(dir);
					viewer.getTable().setSortColumn(ownerColumn);
					viewer.refresh();
				}
			});					
			*/
			table.setHeaderVisible(true);
			table.setLinesVisible(true);
			
			viewer.setContentProvider(new SubscriptionContentProvider());
			viewer.setLabelProvider(new SubscriptionLabelProvider());
		} catch(Exception e){
			e.printStackTrace();
		};
	}
	
	/**
	 * This method update the information that is displayed in the tab
	 * @param subscriptions
	 */
	public void updateContent(final List<Subscription> subscriptions) {
		try {
			if (viewer!= null) {
				if (subscriptions != null && subscriptions.size()>0){			
					Runnable r = new Runnable() {
						public void run() {
							viewer.setInput(subscriptions);
							viewer.refresh();
						};
					};
					viewer.getTable().getDisplay().syncExec(r);
				} else {
					reset();
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void reset() {
		try {
			if (viewer != null){
				Runnable r = new Runnable() {
					public void run() {
						viewer.setInput(null);
						viewer.refresh();
					};
				};
				viewer.getTable().getDisplay().syncExec(r);
			}
			this.listSubscriptions.clear();
		} catch (Exception ex) {
			ex.printStackTrace();
		}		
	}
	
	/**
	 * This method removes information from the tab
	 * @param subscriptions
	 */
	public void removeContent(final List<Subscription> subscriptions) {
		try {
			if (viewer!=null & subscriptions.size()>0){			
			Runnable r = new Runnable() {
				public void run() {
					viewer.remove(subscriptions);
					viewer.refresh();
				};
			};
			viewer.getTable().getDisplay().syncExec(r);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/*****************************************/
	/**** Session Holder Listener Methods*****/
	/*****************************************/
	
	@Override
	/*
	 * (non-Javadoc)
	 * @see org.smool.sdk.sib.data.ISubscriptionHolderListener
	 */
	public void addSubscription(Subscription subscription) {
		if (subscription != null) {
			this.listSubscriptions.add(subscription);
			updateContent(this.listSubscriptions);
		}			
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.smool.sdk.sib.data.ISubscriptionHolderListener
	 */
	public void initSubscriptions(Collection<Subscription> subscriptions) {
		if (subscriptions != null) {
			for (Subscription subscription : subscriptions) {
				this.listSubscriptions.add(subscription);
			}
			
			if ((subscriptions != null)&& (subscriptions.size()>0)) {
				updateContent(this.listSubscriptions);
			}
		} else {
			updateContent(null);
		}
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.smool.sdk.sib.data.ISubscriptionHolderListener
	 */
	public void removeSubscription(Long subscriptionId) {
		if (subscriptionId!=null){
			int i=0;
			boolean found=false;
			List<Subscription> delete= new ArrayList<Subscription>();
			while( i<this.listSubscriptions.size() && found==false) {
				Subscription current=(Subscription)this.listSubscriptions.get(i);
				if (current.getSubscriptionId()==subscriptionId){
					found=true;
					delete.add(current);
				}
				i++;
			}
			
			this.listSubscriptions.removeAll(delete);
			removeContent(delete);			
		}
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.smool.sdk.sib.data.ISubscriptionHolderListener
	 */
	public void removeSubscriptions(Collection<Long> subscriptionIds) {
		if (subscriptionIds!=null){
			Object[] idList= subscriptionIds.toArray();
			List<Subscription> deleteList= new ArrayList<Subscription>();
			for (int j=0; j<idList.length;j++){
				int i=0;
				boolean found=false;			
				while( i < this.listSubscriptions.size() && found==false) {
					Subscription current=(Subscription)this.listSubscriptions.get(i);
					if (current.getSubscriptionId()==idList[j]){
						found=true;
						deleteList.add(current);
					}
					i++;
				}
			}
			
			this.listSubscriptions.removeAll(deleteList);
			removeContent(deleteList);			
		}
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.smool.sdk.sib.data.ISubscriptionHolderListener
	 */
	public void updateSubscriptions(Collection<Subscription> subscriptions) {
		if (subscriptions != null){
			Object[] idList = subscriptions.toArray();
			
			List<Subscription> deleteList= new ArrayList<Subscription>();
			
			for (int j=0; j < idList.length; j++){
				int i=0;
				boolean found=false;	
				while(i<this.listSubscriptions.size() && found==false) {
					Subscription current = (Subscription)this.listSubscriptions.get(i);
					if (current.getSubscriptionId()==((Subscription)idList[j]).getSubscriptionId()){					
						found=true;
						deleteList.add(current);
					}
					i++;
				}
			}
			
			this.listSubscriptions.removeAll(deleteList);
			this.listSubscriptions.addAll(subscriptions);
			updateContent(this.listSubscriptions);	
		}		
	}
}
