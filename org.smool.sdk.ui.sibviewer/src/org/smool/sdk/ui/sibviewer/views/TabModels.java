/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.sibviewer.views;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.smool.sdk.sib.data.ISemanticContentHolderListener;
import org.smool.sdk.sib.data.Triple;
import org.smool.sdk.ui.sibviewer.provider.triple.TripleContentProvider;
import org.smool.sdk.ui.sibviewer.provider.triple.TripleLabelProvider;


/**
 * This class is used to visualize the ontology models available in the
 * Semantic Information Broker.
 * The class itself is an extension of CTabItem.
 * The class is also an implementation of a listener. This means that if the internal
 * triples information of a server changes, its triples tab is updated consequently
 * with the new information. 
 * 
 * @author Cristina L�pez , cristina.lopez@tecnalia.com, Tecnalia - Software Systems Engineering
 * @author Fran Ruiz, fran.ruiz@tecnalia.com, Tecnalia - Software Systems Engineering
 *
 */
public class TabModels extends CTabItem implements ISemanticContentHolderListener{

	/**
	 * List of sessions that are displayed on the tab
	 */
	private ArrayList<Triple> listTriples;
	/**
	 * Subject Column.
	 * This column displays the subject of the triple.
	 */
	private TableColumn subjectColumn;
	/**
	 * Predicate Column.
	 * This column displays the predicate of the triple.
	 */
	private TableColumn predicateColumn;
	
	/**
	 * Object Column,
	 * This column displays the object of the triple.
	 */
	private TableColumn objectColumn;
	
	/**
	 * Date Column,
	 * This column displays the date of creationg/update the triple.
	 */
	private TableColumn dateColumn;
	
	/**
	 * Viewer manager
	 */
	private TableViewer viewer;
	
	private TripleSorter tripleSorter;
	
	
	/**
	 * Internal table of the viewer
	 */
	private Table table;
	
	/**
	 * Constructor
	 * @param parent
	 * @param style
	 */
	public TabModels(CTabFolder parent, int style) {
		super(parent, style);
		this.listTriples = new ArrayList<Triple>();
		createTableStructure(parent);
		this.setControl(table);
	}

	/*****************************************/
	/******** Table Structure Methods ********/
	/*****************************************/
	
	/**
	 * This method sets the structure of the tab
	 */
	public void createTableStructure(Composite parent){
		try{
			viewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION);
			
			tripleSorter = new TripleSorter();
			/* CHANGE JASON MANSELL change setSorter to set Comparator due to deprecated method*/
			viewer.setComparator(tripleSorter);
			
			table = viewer.getTable();
			
			subjectColumn = new TableColumn(table, SWT.LEFT);
			subjectColumn.setText("Subject");
			subjectColumn.setWidth(250);
			
			subjectColumn.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					tripleSorter.setColumn(0);
					int dir = viewer.getTable().getSortDirection();
					if (viewer.getTable().getSortColumn() == subjectColumn) {
						dir = dir == SWT.UP ? SWT.DOWN : SWT.UP;
					} else {

						dir = SWT.DOWN;
					}
					viewer.getTable().setSortDirection(dir);
					viewer.getTable().setSortColumn(subjectColumn);
					viewer.refresh();
				}
			});
			
			predicateColumn= new TableColumn(table, SWT.LEFT);
			predicateColumn.setText("Predicate");
			predicateColumn.setWidth(300);
			
			predicateColumn.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					tripleSorter.setColumn(1);
					int dir = viewer.getTable().getSortDirection();
					if (viewer.getTable().getSortColumn() == predicateColumn) {
						dir = dir == SWT.UP ? SWT.DOWN : SWT.UP;
					} else {

						dir = SWT.DOWN;
					}
					viewer.getTable().setSortDirection(dir);
					viewer.getTable().setSortColumn(predicateColumn);
					viewer.refresh();
				}
			});			
			
			objectColumn= new TableColumn(table ,SWT.LEFT);
			objectColumn.setText("Object");
			objectColumn.setWidth(200);
			
			objectColumn.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					tripleSorter.setColumn(2);
					int dir = viewer.getTable().getSortDirection();
					if (viewer.getTable().getSortColumn() == objectColumn) {
						dir = dir == SWT.UP ? SWT.DOWN : SWT.UP;
					} else {

						dir = SWT.DOWN;
					}
					viewer.getTable().setSortDirection(dir);
					viewer.getTable().setSortColumn(objectColumn);
					viewer.refresh();
				}
			});				
			
			dateColumn = new TableColumn(table, SWT.LEFT);
			dateColumn.setText("Last updated");
			dateColumn.setWidth(150);
			
			dateColumn.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					tripleSorter.setColumn(3);
					int dir = viewer.getTable().getSortDirection();
					if (viewer.getTable().getSortColumn() == dateColumn) {
						dir = dir == SWT.UP ? SWT.DOWN : SWT.UP;
					} else {

						dir = SWT.DOWN;
					}
					viewer.getTable().setSortDirection(dir);
					viewer.getTable().setSortColumn(dateColumn);
					viewer.refresh();
				}
			});
						
			table.setHeaderVisible(true);
			table.setLinesVisible(true);
			
			viewer.setContentProvider(new TripleContentProvider());
			viewer.setLabelProvider(new TripleLabelProvider());
		}
		catch(Exception e){e.printStackTrace();}
	}
	
	/**
	 * This method update the information that is displayed in the tab
	 * @param triples
	 */
	public void updateContent(final List<Triple> triples) {
		try {
			if (viewer!=null) {
				if (triples != null && triples.size()>0) {
					Runnable r = new Runnable() {
						public void run() {
							viewer.setInput(triples);
							viewer.refresh();
						};
					};
					viewer.getTable().getDisplay().syncExec(r);
				} else {
					reset();
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void reset() {
		try {
			if (viewer != null){
				Runnable r = new Runnable() {
					public void run() {
						viewer.setInput(null);
						viewer.refresh();
					};
				};
				viewer.getTable().getDisplay().syncExec(r);
			}
			
			this.listTriples.clear();
		} catch (Exception ex) {
			ex.printStackTrace();
		}		
	}
	
	/**
	 * This method removes information from the tab
	 * @param triples
	 */
	public void removeContent(final List<Triple> triples) {
		try {
			if (viewer!=null && triples.size()>0){
				Runnable r = new Runnable() {
					public void run() {
						viewer.remove(triples);
						viewer.refresh();
					};
				};
				viewer.getTable().getDisplay().syncExec(r);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}	

	/*****************************************/
	/**** Session Holder Listener Methods*****/
	/*****************************************/
	
	@Override
	/*
	 * (non-Javadoc)
	 * @see org.smool.sdk.sib.data.ISemanticContentHolderListener
	 */
	public void addTriples(Collection<Triple> triples, String ownerKP) {
		for (Triple triple : triples) {
			triple.setOwner(ownerKP);
			this.listTriples.add(triple);	
		}
		
		if ((triples != null)&&(triples.size()>0)) {
			updateContent(this.listTriples);
		}	
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.smool.sdk.sib.data.ISemanticContentHolderListener
	 */
	public void initTriples(Collection<Triple> triples, String ownerKP) {
		if (triples != null) {
			for (Triple triple : triples) {
				triple.setOwner(ownerKP);
				this.listTriples.add(triple);			
			}
			
			if ((triples != null)&&(triples.size()>0)) {
				updateContent(this.listTriples);
			}
		} else {
			updateContent(null);
		}
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.smool.sdk.sib.data.ISemanticContentHolderListener
	 */
	public void removeTriples(Collection<Triple> triples, String ownerKP) {
		if ((triples != null)&&(triples.size()>0)) {
			Object[] paramList= triples.toArray();
			List<Triple> deleteList= new ArrayList<Triple>();
			for (int j=0; j<paramList.length;j++){
				int i=0;
				Triple removeTriple = (Triple) paramList[j];
				removeTriple.setOwner(ownerKP);
				boolean found=false;			
				while( i<this.listTriples.size() && found==false) {
					Triple current=(Triple)(this.listTriples.get(i));
					if (TripleContentProvider.equals(current,removeTriple)){
						found=true;
						deleteList.add(current);
					}
					i++;
				}
			}			
			this.listTriples.removeAll(deleteList);
			removeContent(deleteList);
		}	
	}
}
