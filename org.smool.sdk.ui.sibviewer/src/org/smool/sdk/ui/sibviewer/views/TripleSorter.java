package org.smool.sdk.ui.sibviewer.views;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
/* import org.eclipse.jface.viewers.ViewerSorter; - Deprecated*/
import org.smool.sdk.sib.data.Triple;

/* CHANGE JASON MANSELL change ViewerSorter to set ViewerComparator due to deprecated method*/
public class TripleSorter extends ViewerComparator{
	private int propertyIndex;
	// private static final int ASCENDING = 0;
	private static final int DESCENDING = 1;

	private int direction = DESCENDING;

	public TripleSorter() {
		this.propertyIndex = 0;
		direction = DESCENDING;
	}

	public void setColumn(int column) {
		if (column == this.propertyIndex) {
			// Same column as last sort; toggle the direction
			direction = 1 - direction;
		} else {
			// New column; do an ascending sort
			this.propertyIndex = column;
			direction = DESCENDING;
		}
	}

	@Override
	public int compare(Viewer viewer, Object e1, Object e2) {
		Triple t1 = (Triple) e1;
		Triple t2 = (Triple) e2;
		int rc = 0;
		switch (propertyIndex) {
		case 0:
			rc = t1.getSubject().compareTo(t2.getSubject());
			break;
		case 1:
			rc = t1.getPredicate().compareTo(t2.getPredicate());
			break;
		case 2:
			rc = t1.getObject().compareTo(t2.getObject());
			break;
		case 3:
			rc = t1.getDate().compareTo(t2.getDate());
			break;
		case 4:
			rc = t1.getOwner().compareTo(t2.getOwner());
			break;
		default:
			rc = 0;
		}
		// If descending order, flip the direction
		if (direction == DESCENDING) {
			rc = -rc;
		}
		return rc;
	}

}
