/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.sibviewer.views;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.smool.sdk.sib.data.ISIBPropertiesHolderListener;
import org.smool.sdk.sib.data.SIBProperty;
import org.smool.sdk.ui.sibviewer.provider.sibprops.SIBPropertiesContentProvider;
import org.smool.sdk.ui.sibviewer.provider.sibprops.SIBPropertiesLabelProvider;


/**
 * This class is used to visualize the SIB Properties information of one particular SIB.
 * The class itself is an extension of CTabItem.
 * The class is also an implementation of a listener. This means that if the internal
 * triples information of a server changes, its triples tab is updated consequently
 * with the new information. 
 * 
 * @author Fran Ruiz
 *
 */
public class TabSIBProperties extends CTabItem implements ISIBPropertiesHolderListener{

	/**
	 * List of sessions that are displayed on the tab
	 */
	private ArrayList<SIBProperty> listProps;
	
	/**
	 * Name Column.
	 * This column displays the property name.
	 */
	private TableColumn nameColumn;

	/**
	 * Value Column.
	 * This column displays the property value.
	 */
	private TableColumn valueColumn;
	
	/**
	 * Date Column,
	 * This column displays the date of creation/update the property.
	 */
	private TableColumn dateColumn;
	
	/**
	 * Viewer manager
	 */
	private TableViewer viewer;
	
	private SIBPropSorter propsSorter;
	
	
	/**
	 * Internal table of the viewer
	 */
	private Table table;
	
	/**
	 * Constructor
	 * @param parent
	 * @param style
	 */
	public TabSIBProperties(CTabFolder parent, int style) {
		super(parent, style);
		this.listProps = new ArrayList<SIBProperty>();
		createTableStructure(parent);
		this.setControl(table);
	}

	/*****************************************/
	/******** Table Structure Methods ********/
	/*****************************************/
	
	/**
	 * This method sets the structure of the tab
	 */
	public void createTableStructure(Composite parent){
		try{
			viewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION);
			
			propsSorter = new SIBPropSorter();
			/* Updated .setSorter(propsSorter); to setComparator(propsSorter) since setSorter deprecated*/ 
			viewer.setComparator(propsSorter);
			
			table = viewer.getTable();
			
			nameColumn = new TableColumn(table, SWT.LEFT);
			nameColumn.setText("Property");
			nameColumn.setWidth(250);
			
			nameColumn.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					propsSorter.setColumn(0);
					int dir = viewer.getTable().getSortDirection();
					if (viewer.getTable().getSortColumn() == nameColumn) {
						dir = dir == SWT.UP ? SWT.DOWN : SWT.UP;
					} else {

						dir = SWT.DOWN;
					}
					viewer.getTable().setSortDirection(dir);
					viewer.getTable().setSortColumn(nameColumn);
					viewer.refresh();
				}
			});
			
			valueColumn= new TableColumn(table, SWT.LEFT);
			valueColumn.setText("Value");
			valueColumn.setWidth(300);
			
			valueColumn.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					propsSorter.setColumn(1);
					int dir = viewer.getTable().getSortDirection();
					if (viewer.getTable().getSortColumn() == valueColumn) {
						dir = dir == SWT.UP ? SWT.DOWN : SWT.UP;
					} else {

						dir = SWT.DOWN;
					}
					viewer.getTable().setSortDirection(dir);
					viewer.getTable().setSortColumn(valueColumn);
					viewer.refresh();
				}
			});			
			
			dateColumn = new TableColumn(table, SWT.LEFT);
			dateColumn.setText("Last updated");
			dateColumn.setWidth(150);
			
			dateColumn.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					propsSorter.setColumn(2);
					int dir = viewer.getTable().getSortDirection();
					if (viewer.getTable().getSortColumn() == dateColumn) {
						dir = dir == SWT.UP ? SWT.DOWN : SWT.UP;
					} else {

						dir = SWT.DOWN;
					}
					viewer.getTable().setSortDirection(dir);
					viewer.getTable().setSortColumn(dateColumn);
					viewer.refresh();
				}
			});
			
			table.setHeaderVisible(true);
			table.setLinesVisible(true);
			
			viewer.setContentProvider(new SIBPropertiesContentProvider());
			viewer.setLabelProvider(new SIBPropertiesLabelProvider());
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * This method update the information that is displayed in the tab
	 * @param triples
	 */
	public void updateContent(final List<SIBProperty> properties) {
		try {
			if (viewer != null) {
				if (properties != null && properties.size()>0) {
					Runnable r = new Runnable() {
						public void run() {
							viewer.setInput(properties);
							viewer.refresh();
						};
					};
					viewer.getTable().getDisplay().syncExec(r);
				} else {
					reset();
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void reset() {
		try {
			if (viewer != null){
				Runnable r = new Runnable() {
					public void run() {
						viewer.setInput(null);
						viewer.refresh();
					};
				};
				viewer.getTable().getDisplay().syncExec(r);
			}
			
			this.listProps.clear();
		} catch (Exception ex) {
			ex.printStackTrace();
		}		
	}
	
	/**
	 * This method removes information from the tab
	 * @param triples
	 */
	public void removeContent(final List<SIBProperty> properties) {
		try {
			if (viewer!=null && properties.size()>0){
				Runnable r = new Runnable() {
					public void run() {
						viewer.remove(properties);
						viewer.refresh();
					};
				};
				viewer.getTable().getDisplay().syncExec(r);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}	

	/************************************************/
	/**** SIB Properties Holder Listener Methods*****/
	/************************************************/

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.smool.sdk.sib.data.ISIBPropertyHolderListener
	 */
	public void initProperties(Collection<SIBProperty> properties) {
		if (properties != null) {
			for (SIBProperty property : properties) {
				this.listProps.add(property);			
			}
			
			if ((properties != null)&&(properties.size()>0)) {
				updateContent(this.listProps);
			}
		} else {
			updateContent(null);
		}
	}
}
