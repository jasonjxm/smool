package org.smool.sdk.ui.sibviewer.views;
/* 
CHANGE JASON
Updated deprecated class ViewerSoter to ViewerComparator
*/
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.smool.sdk.sib.data.SIBProperty;


public class SIBPropSorter extends ViewerComparator{
	private int propertyIndex;
	// private static final int ASCENDING = 0;
	private static final int DESCENDING = 1;

	private int direction = DESCENDING;

	public SIBPropSorter() {
		this.propertyIndex = 0;
		direction = DESCENDING;
	}

	public void setColumn(int column) {
		if (column == this.propertyIndex) {
			// Same column as last sort; toggle the direction
			direction = 1 - direction;
		} else {
			// New column; do an ascending sort
			this.propertyIndex = column;
			direction = DESCENDING;
		}
	}

	@Override
	public int compare(Viewer viewer, Object e1, Object e2) {
		SIBProperty t1 = (SIBProperty) e1;
		SIBProperty t2 = (SIBProperty) e2;
		int rc = 0;
		switch (propertyIndex) {
		case 0:
			rc = t1.getPropertyName().compareTo(t2.getPropertyName());
			break;
		case 1:
			rc = t1.getPropertyValue().compareTo(t2.getPropertyValue());
			break;
		case 2:
			rc = t1.getDate().compareTo(t2.getDate());
			break;
		default:
			rc = 0;
		}
		// If descending order, flip the direction
		if (direction == DESCENDING) {
			rc = -rc;
		}
		return rc;
	}

}
