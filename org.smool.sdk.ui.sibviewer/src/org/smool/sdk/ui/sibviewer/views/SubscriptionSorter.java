package org.smool.sdk.ui.sibviewer.views;

import org.eclipse.jface.viewers.Viewer;
/* CHANGE JASON MANSELL change ViewerSorter to set ViewerComparator due to deprecated method*/
import org.eclipse.jface.viewers.ViewerComparator;
import org.smool.sdk.sib.data.Subscription;


public class SubscriptionSorter extends ViewerComparator {
	private int propertyIndex;
	private static final int ASCENDING = 0;	
	private static final int DESCENDING = 1;

	private int direction = DESCENDING;

	public SubscriptionSorter() {
		this.propertyIndex = 0;
		direction = ASCENDING;
	}

	public void setColumn(int column) {
		if (column == this.propertyIndex) {
			// Same column as last sort; toggle the direction
			direction = 1 - direction;
		} else {
			// New column; do an ascending sort
			this.propertyIndex = column;
			direction = DESCENDING;
		}
	}

	@Override
	public int compare(Viewer viewer, Object e1, Object e2) {
		Subscription s1 = (Subscription) e1;
		Subscription s2 = (Subscription) e2;
		int rc = 0;
		switch (propertyIndex) {
		case 0:
			rc = s1.getSubscriptionId().compareTo(s2.getSubscriptionId());
			break;
		case 1:
			rc = s1.getNodeId().compareTo(s2.getNodeId());
			break;
		case 2:
			rc = s1.getQuery().compareTo(s2.getQuery());
			break;
		case 3:
			rc = s1.getResult().compareTo(s2.getResult());
			break;
		case 4:
			rc = s1.getDate().compareTo(s2.getDate());
			break;
		default:
			rc = 0;
		}
		// If descending order, flip the direction
		if (direction == DESCENDING) {
			rc = -rc;
		}
		return rc;
	}

}
