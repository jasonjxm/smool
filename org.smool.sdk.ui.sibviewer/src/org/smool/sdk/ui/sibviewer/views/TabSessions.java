/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.sibviewer.views;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.smool.sdk.sib.data.ISessionHolderListener;
import org.smool.sdk.sib.data.Session;
import org.smool.sdk.ui.sibviewer.provider.session.SessionContentProvider;
import org.smool.sdk.ui.sibviewer.provider.session.SessionLabelProvider;


/**
 * This class is used to visualize the Sessions information of one particular Sib server.
 * The class itself is an extension of CTabItem.
 * The class is also an implementation of a listener. This means that if the internal
 * sessions information of a server changes, its sessions tab is updated
 * with the new information consequently. 
 * 
 * @author Cristina L�pez, cristina.lopez@tecnalia.com, ESI
 *
 */
public class TabSessions extends CTabItem implements ISessionHolderListener{

	/**
	 * List of sessions that are displayed on the tab
	 */
	private List<Session> listSessions;	
	/**
	 * Node Id column.
	 * This column displays the Id's of the Smart Applications connected to the server.
	 */
	private TableColumn nodeColumn;
	
	/**
	 * Date Column,
	 * This column displays the date of creationg/update the triple.
	 */
	private TableColumn dateColumn;
	
	/**
	 * Viewer manager
	 */
	private TableViewer viewer;	
	/**
	 * Internal viewer table
	 */
	private Table table;

	private SessionSorter sessionSorter;	
	
	/**
	 * Constructor
	 * @param parent
	 * @param style
	 */
	public TabSessions(CTabFolder parent, int style) {
		super(parent, style);
		this.listSessions= new ArrayList<Session>();
		createTableStructure(parent);
		this.setControl(table);
	}
	
	/*****************************************/
	/******** Table Structure Methods ********/
	/*****************************************/
	
	/**
	 * This method sets the structure of the tab
	 */
	public void createTableStructure(Composite parent){
		try{
			viewer= new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION);
			table= viewer.getTable();
			
			sessionSorter = new SessionSorter();
			/* CHANGE JASON MANSELL change setSorter to set Comparator due to deprecated method*/
			viewer.setComparator(sessionSorter);			
			
			nodeColumn= new TableColumn(table, SWT.LEFT);
			nodeColumn.setText("Node Id");
			nodeColumn.setWidth(250);

			nodeColumn.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					sessionSorter.setColumn(0);
					int dir = viewer.getTable().getSortDirection();
					if (viewer.getTable().getSortColumn() == nodeColumn) {
						dir = dir == SWT.UP ? SWT.DOWN : SWT.UP;
					} else {

						dir = SWT.DOWN;
					}
					viewer.getTable().setSortDirection(dir);
					viewer.getTable().setSortColumn(nodeColumn);
					viewer.refresh();
				}
			});			
			
			dateColumn= new TableColumn(table, SWT.LEFT);
			dateColumn.setText("Last joined");
			dateColumn.setWidth(150);			
			
			dateColumn.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					sessionSorter.setColumn(1);
					int dir = viewer.getTable().getSortDirection();
					if (viewer.getTable().getSortColumn() == dateColumn) {
						dir = dir == SWT.UP ? SWT.DOWN : SWT.UP;
					} else {

						dir = SWT.DOWN;
					}
					viewer.getTable().setSortDirection(dir);
					viewer.getTable().setSortColumn(dateColumn);
					viewer.refresh();
				}
			});			
			
			table.setHeaderVisible(true);
			table.setLinesVisible(true);
			
			viewer.setContentProvider(new SessionContentProvider());
			viewer.setLabelProvider(new SessionLabelProvider());
		}
		catch(Exception e){e.printStackTrace();};
	}
	
	/**
	 * This method update the information that is displayed in the tab
	 * @param sessions
	 */
	public void updateContent(final List<Session> sessions) {
		try {
			if(viewer!=null && sessions.size()>0){
				Runnable r = new Runnable() {
					public void run() {
						viewer.setInput(sessions);
						viewer.refresh();
					};
				};
				viewer.getTable().getDisplay().syncExec(r);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void reset() {
		try {
			if (viewer != null){
				Runnable r = new Runnable() {
					public void run() {
						viewer.setInput(null);
						viewer.refresh();
					};
				};
				viewer.getTable().getDisplay().syncExec(r);
			}
			this.listSessions.clear();
		} catch (Exception ex) {
			ex.printStackTrace();
		}		
	}
	
	
	
	/**
	 * This method removes information from the tab
	 * @param sessions
	 */
	public void removeContent(final List<Session> sessions) {
		try {
			if(viewer!=null && sessions.size()>0){
				Runnable r = new Runnable() {
					public void run() {
						viewer.remove(sessions);
						viewer.refresh();
					};
				};
				viewer.getTable().getDisplay().syncExec(r);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/*****************************************/
	/**** Session Holder Listener Methods*****/
	/*****************************************/
	
	@Override
	/*
	 * (non-Javadoc)
	 * @see org.smool.sdk.sib.data.ISessionHolderListener
	 */
	public void addSession(Session addedSession) {
		if (addedSession!=null){
			listSessions.add(addedSession);
			updateContent(this.listSessions);
		}
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.smool.sdk.sib.data.ISessionHolderListener
	 */
	public void initSessions(Collection<Session> sessions) {
		if ((sessions != null)&&(sessions.size()>0))
		{
			listSessions=new ArrayList<Session> (sessions);
			updateContent(this.listSessions);
		}	
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.smool.sdk.sib.data.ISessionHolderListener
	 */
	public void removeSession(Session removedSession) {
		if (removedSession!=null){
			int i=0;
			boolean found=false;
			List<Session> delete= new ArrayList<Session>();
			while( i<listSessions.size() && found==false)
			{
				Session current=(Session)listSessions.get(i);
				if (SessionContentProvider.equals(current, removedSession)){
					found=true;
					delete.add(listSessions.get(i));
				}
				i++;
			}
			
			listSessions.removeAll(delete);
			removeContent(delete);		
		}
	}
}
