/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.sibviewer.views.menu;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.smool.sdk.ui.sibviewer.views.SIBViewer;


/**
 * This class is used to create a new SIB Viewer instance
 * 
 * @author Cristina L�pez, cristina.lopez@tecnalia.com, ESI
 *
 */
public class ViewerSheetHandler extends AbstractHandler {

	/**
	 * Variable that counts how many instances of the SIB Viewer have been created
	 */
	private static int instanceCount = 1;
	
	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
		System.out.println("Creating new view");
		SIBViewer part = getView(event);
		if (part == null){
			return null;
		}
		try {
			String count = newSecondaryID(part);
			String defaultName = NLS.bind("SIB Inspector",
					new Object[] { part.getSite().getRegisteredName(), count });
			InputDialog dialog = new InputDialog(part.getSite().getShell(),
					NLS
							.bind("SIB Inspector",
									new String[] { part.getSite()
											.getRegisteredName() }),
					"Enter a name for the new view", defaultName,
					getValidator());
			
			if (dialog.open() != Window.OK){
				return this;
			}
			
			IViewPart newPart = part.getSite().getPage().showView(part.getSite().getId(), count,IWorkbenchPage.VIEW_ACTIVATE);
			/*SIBViewer newPart = (SIBViewer) part.getSite().getPage().showView(part.getSite().getId(), count,IWorkbenchPage.VIEW_ACTIVATE);
			newPart.init(part.getViewSite());
			newPart.createPartControl(part.getComposite());
			newPart.setServerTabs(part.getServerTabs());*/
			if (newPart instanceof SIBViewer) {
				((SIBViewer) newPart).initializeTitle(dialog.getValue());
			}
		}
		catch(Exception e){
			//throw new ExecutionException(e.getLocalizedMessage(), e);
			e.printStackTrace();
		}
		return this;
	}
	
	/**
	 * Gets the original SIB Viewer
	 * @param event
	 * @return SIBViewer
	 */
	public SIBViewer getView(ExecutionEvent event) {
		IWorkbenchPart part = HandlerUtil.getActivePart(event);
		if (part == null){
			return null;
		}
		return (SIBViewer) part;
	}

	/**
	 * Creates a new secondary id for the new instance of the SIB Viewer
	 * @param part
	 * @return String
	 */
	static String newSecondaryID(IViewPart part) {
		while (part.getSite().getPage().findViewReference(
				part.getSite().getId(), String.valueOf(instanceCount)) != null) {
			instanceCount++;
		}
		return String.valueOf(instanceCount);
	}
	
	/**
	 * Gets the input validator for the receiver.
	 * 
	 * @return IInputValidator
	 */
	private IInputValidator getValidator() {
		return new IInputValidator() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see org.eclipse.jface.dialogs.IInputValidator#isValid(java.lang.String)
			 */
			public String isValid(String newText) {
				if (newText.length() > 0){
					return null;
				}
				return "Name must not be empty";
			}
		};
	}

}
