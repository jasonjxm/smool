/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.sibviewer.views;

import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.widgets.Composite;

/**
 * This class is used to control the information related to sessions, subscriptions and
 * triples that is displayed in each server tab.
 * The class itself is an extension of the CTabFolder object.
 * 
 * The SIB Viewer information have to be updated according to changes in the internal
 * information of each server instance.
 * 
 * @author Cristina L�pez, cristina.lopez@tecnalia.com, ESI
 *
 */
public class SIBVisualizer extends CTabFolder {

	/**
	 * Default Constructor
	 * @param parent
	 * @param style
	 */
	public SIBVisualizer(Composite parent, int style) {
		super(parent, style);
	}

	/**
	 * Method used externally to obtain the Sessions tab of one server
	 * @return TabSessions
	 */
	public TabSessions getSessionListener(){
		CTabItem[] items=this.getItems();
		TabSessions sessions=null;
		boolean found=false;
		int i=0;
		
		while(i<items.length && found==false){
			if (items[i] instanceof TabSessions){
				found=true;
				sessions=(TabSessions)items[i];
			}
			i++;
		}
		return sessions;
	}
	
	/**
	 * Method used externally to obtain the Subscriptions tab of one server
	 * @return
	 */
	public TabSubscriptions getSuscriptionListener(){
		CTabItem[] items=this.getItems();
		TabSubscriptions subscriptions=null;
		boolean found=false;
		int i=0;
		
		while(i<items.length && found==false){
			if (items[i] instanceof TabSubscriptions){
				found=true;
				subscriptions=(TabSubscriptions)items[i];
			}
			i++;
		}				
		return subscriptions;
	}
	
	/**
	 * Methoud used externally to obtain the Triples tab of one server
	 * @return
	 */
	public TabTriples getSemanticContentListener(){
		CTabItem[] items=this.getItems();
		TabTriples triples=null;
		boolean found=false;
		int i=0;
		
		while(i<items.length && found==false){
			if (items[i] instanceof TabTriples){
				found=true;
				triples=(TabTriples)items[i];
			}
			i++;
		}				
		return triples;
	}
	
	/**
	 * Methoud used externally to obtain the Triples tab of one server
	 * @return
	 */
	public TabSIBProperties getSIBPropertiesListener(){
		CTabItem[] items=this.getItems();
		TabSIBProperties properties=null;
		boolean found=false;
		int i=0;
		
		while(i<items.length && found==false){
			if (items[i] instanceof TabSIBProperties){
				found=true;
				properties=(TabSIBProperties)items[i];
			}
			i++;
		}				
		return properties;
	}
	
}
