/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.sibviewer.provider.subscription;

import java.util.List;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.smool.sdk.sib.data.Subscription;


/**
 * This class is the Content Provider implementation required by the viewer of the 
 * Subscriptions tab.
 * The class responsibility is to provide Subscriptions objects to the Subscriptions tab.
 * 
 *@author Cristina Lopez, cristina.lopez@tecnalia.com, ESI
*/

public class SubscriptionContentProvider implements IStructuredContentProvider {

	@SuppressWarnings("unchecked")
	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider
	 */
	public Object[] getElements(Object inputElement) {
		if (inputElement instanceof List<?>) {
			List<Subscription> subscriptions = (List<Subscription>) inputElement;
			return subscriptions.toArray();
		} else {
			return null;
		}
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider
	 */
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider
	 */
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * Method used to check if two Subscription object are equal
	 * @param subscription1
	 * @param subscription2
	 * @return boolean
	 */
	public static boolean equals(Subscription subscription1, Subscription subscription2) {
		Boolean _return=false;

		Integer compare=subscription1.getSubscriptionId().compareTo(subscription2.getSubscriptionId());
		switch(compare){
		case 0: _return=true;
				break;
		default: _return=false;
			     break;
		}
		return _return;
	}

}
