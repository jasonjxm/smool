/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.sibviewer.provider.triple;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;

import org.eclipse.swt.graphics.Image;
import org.smool.sdk.sib.data.Triple;


/**
 * This class is the Label Provider implementation required by the viewer of the 
 * Triples tab. 
 * This class defines how the Triple objects are displayed in the Triple view
 * 
 *@author Cristina L�pez, cristina.lopez@tecnalia.com, ESI
*/

public class TripleLabelProvider extends LabelProvider implements ITableLabelProvider{

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITableLabelProvider
	 */
	public Image getColumnImage(Object element, int columnIndex) {
		return null;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITableLabelProvider
	 */
	public String getColumnText(Object element, int columnIndex) {
		Triple object = (Triple) element;
		String _return="";
		switch(columnIndex)
		{
			case 0:
				_return=object.getSubject();
				break;
			case 1:
				_return=object.getPredicate();
				break;
			case 2:
				_return=object.getObject();
				break;
			case 3:
				_return=object.getDate();
				break;
			case 4:
				_return=object.getOwner();
				break;
			default:
				_return="Error";
				break;
		}

		return _return;
	}
}
