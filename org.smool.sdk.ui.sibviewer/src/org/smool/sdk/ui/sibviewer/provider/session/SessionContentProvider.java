/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/

package org.smool.sdk.ui.sibviewer.provider.session;

import java.util.List;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.smool.sdk.sib.data.Session;

/**
 * This class is the Content Provider implementation required by the viewer of
 * the Sessions tab. The class responsibility is to provide Session objects to
 * the Session tab.
 * 
 * @author Cristina L�pez, cristina.lopez@tecnalia.com, ESI
 */

public class SessionContentProvider implements IStructuredContentProvider {

	@SuppressWarnings("unchecked")
	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider
	 */
	public Object[] getElements(Object inputElement) {
		if (inputElement instanceof List<?>) {
			List<Session> sessions = (List<Session>) inputElement;
			return sessions.toArray();
		} else {
			return null;
		}
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider
	 */
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider
	 */
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		// TODO Auto-generated method stub

	}

	/**
	 * Method used to check if two session object are equal
	 * 
	 * @param session1
	 * @param session2
	 * @return boolean
	 */
	public static boolean equals(Session session1, Session session2) {
		if (session1 != null && session2 != null) {
			return (session1.getNodeId().equals(session2.getNodeId()));
		} else {
			return (session1 == session2);
		}
	}
}
