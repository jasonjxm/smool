/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.sibviewer.provider.triple;

import java.util.List;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.smool.sdk.sib.data.Triple;



/**
 * This class is the Content Provider implementation required by the viewer of the 
 * Triples tab.
 * The class responsibility is to provide Subscriptions objects to the Triples tab.
 *  
 * @author Cristina L�pez, cristina.lopez@tecnalia.com, ESI
 */

public class TripleContentProvider implements IStructuredContentProvider{

	@SuppressWarnings("unchecked")
	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider
	 */
	public Object[] getElements(Object inputElement) {
		if (inputElement instanceof List<?>) {
			List<Triple> triple = (List<Triple>) inputElement;
			return triple.toArray();
		} else {
			return null;
		}
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider
	 */
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider
	 */
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * Method used to check if to Triple object are equal
	 * @param triple1
	 * @param triple2
	 * @return
	 */
	public static boolean equals(Triple triple1, Triple triple2) {
		Boolean _return=false;
		if ((triple1.getObject().equals(triple2.getObject()))&&(triple1.getSubject().equals(triple2.getSubject()))&&(triple1.getPredicate().equals(triple2.getPredicate())))
			_return=true;
		return _return;
	}
}
