/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.sibviewer.provider.sibprops;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.smool.sdk.sib.data.SIBProperty;


/**
 * This class is the Label Provider implementation required by the viewer of the 
 * SIB Properties tab. 
 * This class defines how the SIB Property objects are displayed in the SIB Properties view
 * 
 *@author Fran Ruiz
*/

public class SIBPropertiesLabelProvider extends LabelProvider implements ITableLabelProvider{

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITableLabelProvider
	 */
	public Image getColumnImage(Object element, int columnIndex) {
		return null;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITableLabelProvider
	 */
	public String getColumnText(Object element, int columnIndex) {
		SIBProperty prop = (SIBProperty) element;
		String _return="";
		switch(columnIndex)
		{
			case 0:
				_return=prop.getPropertyName();
				break;
			case 1:
				_return=prop.getPropertyValue();
				break;
			case 2:
				_return=prop.getDate();
				break;
			default:
				_return="Error";
				break;
		}

		return _return;
	}
}
