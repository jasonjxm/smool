/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.sibviewer.provider.subscription;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;

import org.eclipse.swt.graphics.Image;
import org.smool.sdk.sib.data.Subscription;


/**
 * This class is the Label Provider implementation required by the viewer of the 
 * Subscription tab. 
 * Thes class defines how the Subscription objects are displayed in the Subscription tab.
 * 
 *@author Cristina L�pez, cristina.lopez@tecnalia.com, ESI
*/

public class SubscriptionLabelProvider extends LabelProvider implements ITableLabelProvider {

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITableLabelProvider
	 */
	public Image getColumnImage(Object element, int columnIndex) {
		return null;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITableLabelProvider
	 */
	public String getColumnText(Object element, int columnIndex) {
		Subscription object = (Subscription) element;
		String _return="";
		
		switch(columnIndex)
		{
			case 0:
				_return=object.getSubscriptionId().toString();
				break;
			case 1:
				_return=object.getNodeId();
				break;
			case 2:
				_return=object.getQuery();
				break;
			case 3:
				_return=object.getResult();
				break;
			case 4:
				_return=object.getDate();
				break;
			default:
				_return="Error";
				break;
		}

		return _return;
	}

}
