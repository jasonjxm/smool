/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.sibviewer.provider.sibprops;

import java.util.List;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.smool.sdk.sib.data.SIBProperty;



/**
 * This class is the SIB Properties provider implementation required by the viewer of the 
 * SIB Properties tab.
 * The class responsibility is to provide SIB internal properties values to the SIB Properties tab.
 *  
 * @author Fran Ruiz
 */

public class SIBPropertiesContentProvider implements IStructuredContentProvider{

	@SuppressWarnings("unchecked")
	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider
	 */
	public Object[] getElements(Object inputElement) {
		if (inputElement instanceof List<?>) {
			List<SIBProperty> props = (List<SIBProperty>) inputElement;
			return props.toArray();
		} else {
			return null;
		}
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider
	 */
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider
	 */
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * Method used to check if two SIB Properties objects are equal
	 * @param prop1
	 * @param prop2
	 * @return
	 */
	public static boolean equals(SIBProperty prop1, SIBProperty prop2) {
		Boolean _return=false;
		if ((prop1.getPropertyName().equals(prop2.getPropertyName()))&&(prop1.getPropertyValue().equals(prop2.getPropertyValue()))) {
			_return=true;
		}
		return _return;
	}
}
