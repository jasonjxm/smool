/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.sibviewer;

import java.util.HashMap;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;
import org.smool.sdk.sib.service.IViewer;
import org.smool.sdk.ui.sibviewer.ViewerServiceTrackerCustomizer;
import org.smool.sdk.ui.sibviewer.views.SIBVisualizer;



/**
 * The Activator class controls the plug-in life cycle.
 * This plug-in implements a view used to display the internal elements of the
 * different existing SIB instances
 * 
 * @author Cristina L�pez, cristina.lopez@tecnalia.com, ESI
 */
@SuppressWarnings("rawtypes")
public class Activator extends AbstractUIPlugin {

	/**
	 * Plug-in id
	 */
	public static final String PLUGIN_ID = "org.smool.sdk.ui.sibviewer";

	/**
	 * Bundle context
	 */
	public static BundleContext bc = null;
	
	/**
	 * Shared instance of the Activator
	 */
	private static Activator plugin;
	
	/**
	 * Viewer service tracker customizer used by the plug-in to identify which serve information
	 * is available
	 */
	private ViewerServiceTrackerCustomizer sibstc;
	
	/**
	 * Hashmap used internally by the plug-in to identify each server with the tabs that
	 * displays its information 
	 */
	private HashMap<String,SIBVisualizer> visualizerMap= new HashMap<String,SIBVisualizer>();
	
	/**
	 * Activator Constructor
	 */
	public Activator() {
	}
	
	/**
	 * Gets the shared instance
	 * @return Activator
	 */
	public static Activator getDefault() {
		return plugin;
	}
	
	/**
	 * Gets instance of the service tracker customizer
	 * 
	 * @return ViewerServiceTrackerCustomizer
	 */
	public ViewerServiceTrackerCustomizer getSibstc() {
		return sibstc;
	}
	
	/**
	 * Gets the internal map of correspondences
	 * @return HashMap<String, SIBVisualizer>
	 */
	public HashMap<String, SIBVisualizer> getVisualizerMap() {
		return this.visualizerMap;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	//@SuppressWarnings({ "rawtypes", "unchecked" })
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		bc=context;
		// Service tracker customizer for the async SIB
		sibstc = new ViewerServiceTrackerCustomizer(bc);
		ServiceTracker tracker = new ServiceTracker(bc, IViewer.class.getName(), sibstc);
		tracker.open();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		//plugin = null;
		//super.stop(context);
		Activator.bc=null;
	}
	
	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}
	
}
