/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.sibviewer;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import org.smool.sdk.sib.service.IViewer;
import org.smool.sdk.ui.sibviewer.views.SIBViewer;
import org.smool.sdk.ui.sibviewer.views.SIBVisualizer;
@SuppressWarnings("rawtypes")


/**
 * This class is a service tracker customizer that detects if the Viewer service
 * of any server has been registered in the OSGI Framework.
 * 
 * When a new Viewer server is added, a new tab pointing to the server that register the
 * viewer service, is created in the plug-in.
 * If a Viewer service is removed, its corresponding server tab is deleted from the plug-in
 * 
 * @author Cristina L�pez, cristina.lopez@tecnalia.com ,ESI
 */
//@SuppressWarnings({ "rawtypes", "unchecked" })
public class ViewerServiceTrackerCustomizer implements ServiceTrackerCustomizer {

	/**
	 * Bundle context
	 */
	private BundleContext bc;		

	/**
	 * Constructor
	 * @param bc Bundle Context
	 */
	public ViewerServiceTrackerCustomizer(BundleContext bc){
		this.bc = bc;
	}
	
	@Override
	/*
	 * (non-Javadoc)
	 * @see org.osgi.util.tracker.ServiceTrackerCustomizer
	 */
	public Object addingService(ServiceReference reference) {

		Object obj = bc.getService(reference);
		
		if (obj instanceof IViewer) {
			IViewer viewer = (IViewer) bc.getService(reference);
			
			String serverid= (String) reference.getPropertyKeys()[0];
			
			SIBViewer.createServer(serverid, viewer);
			
			
			
			
		} 
		return obj;
	}
	
	@Override
	/*
	 * (non-Javadoc)
	 * @see org.osgi.util.tracker.ServiceTrackerCustomizer
	 */
	public void modifiedService(ServiceReference reference, Object serviceObject) {
		Object obj = bc.getService(reference);
		
		if (obj instanceof IViewer) {
			IViewer viewer = (IViewer) bc.getService(reference);			

			String serverid= (String) reference.getPropertyKeys()[0];
			
			SIBVisualizer visualizer = SIBViewer.createServer(serverid, viewer);
			viewer.addListener(visualizer.getSessionListener());
			viewer.addListener(visualizer.getSuscriptionListener());
			viewer.addListener(visualizer.getSemanticContentListener());
			viewer.addListener(visualizer.getSIBPropertiesListener());
			
			Activator.getDefault().getVisualizerMap().put(serverid, visualizer);
		}

	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.osgi.util.tracker.ServiceTrackerCustomizer
	 */
	public void removedService(ServiceReference reference, Object service) {
		Object obj = bc.getService(reference);
		
		if (obj instanceof IViewer) {
			IViewer viewer = (IViewer) bc.getService(reference);
			String serverid= (String) reference.getPropertyKeys()[0];
			SIBVisualizer visualizer=Activator.getDefault().getVisualizerMap().get(serverid);
			viewer.removeListener(visualizer.getSessionListener());
			viewer.removeListener(visualizer.getSuscriptionListener());
			viewer.removeListener(visualizer.getSemanticContentListener());
			viewer.removeListener(visualizer.getSIBPropertiesListener());
			
			SIBViewer.removeServer(serverid);

		}
	}

}
