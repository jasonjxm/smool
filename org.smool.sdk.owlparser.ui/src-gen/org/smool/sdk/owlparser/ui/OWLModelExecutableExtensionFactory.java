/*
 * generated by Xtext
 */
package org.smool.sdk.owlparser.ui;

import org.eclipse.xtext.ui.guice.AbstractGuiceAwareExecutableExtensionFactory;
import org.osgi.framework.Bundle;

import com.google.inject.Injector;

import org.smool.sdk.owlparser.ui.internal.OWLModelActivator;

/**
 * This class was generated. Customizations should only happen in a newly
 * introduced subclass. 
 */
public class OWLModelExecutableExtensionFactory extends AbstractGuiceAwareExecutableExtensionFactory {

	@Override
	protected Bundle getBundle() {
		return OWLModelActivator.getInstance().getBundle();
	}
	
	@Override
	protected Injector getInjector() {
		return OWLModelActivator.getInstance().getInjector(OWLModelActivator.ORG_SMOOL_SDK_OWLPARSER_OWLMODEL);
	}
	
}
