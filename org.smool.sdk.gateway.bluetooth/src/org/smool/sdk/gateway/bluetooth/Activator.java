package org.smool.sdk.gateway.bluetooth;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Properties;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;
import org.smool.sdk.gateway.service.IGatewayFactory;
import org.smool.sdk.sib.SIBActivator;
import org.smool.sdk.sib.service.ISIB;


public class Activator implements BundleActivator {
	
	private BluetoothGatewayFactory btManager;
	
	public Activator() {
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void start(BundleContext context) throws Exception {
	
		Dictionary props = new Hashtable();
		props.put("Gateway", "Bluetooth");
		
		this.btManager=new BluetoothGatewayFactory(context);
		context.registerService(IGatewayFactory.class.getName(), this.btManager, props);
		System.out.println("Manager for Bluetooth registered");
		ServiceTracker tracker = new ServiceTracker(context, ISIB.class.getName(), this.btManager);
		tracker.open();
		
		// Create any TCP/IP gateways defined in the configuration
		Properties configuration = SIBActivator.getConfiguration();
		String [] gws = configuration.getProperty("CREATE_BT_GW") != null ? configuration.getProperty("CREATE_BT_GW").split(",") : new String [0];
		String [] autostart = configuration.getProperty("AUTOSTART") != null ? configuration.getProperty("AUTOSTART").split(",") : new String [0];
		for (String gw : gws) {
			String [] gwProps = gw.split(":");
			if (gwProps.length == 4) {
				String sib = gwProps[0];
				String gwName = gwProps[1];
				String maxRetries = gwProps[2];
				String uuid = gwProps[3];

				ISIB sibInstance = SIBActivator.getSIB(sib); 
				if (sibInstance != null) {
					Properties cp = btManager.getConfigurableProperties();

					// Fill the properties
					cp.put(BluetoothGatewayConfiguration.UUID, uuid);
					cp.put(BluetoothGatewayConfiguration.RESPONSE_RETRY_NUMBERS, maxRetries);

					int id = btManager.create(gwName, sibInstance, cp);
					// Start the gw if autostart required
					for (String autosib : autostart) {
						if (autosib.equals(sib)) {
							btManager.getGateway(id).start();
						}
					}
				}
			}

		}
	}

	@Override
	public void stop(BundleContext _context) throws Exception {

		btManager.shutdown();
		btManager = null;
		System.out.println("Manager for Bluetooth unregistered");

	}

}