package org.smool.sdk.gateway.bluetooth.comm;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.InputStream;

import javax.bluetooth.RemoteDevice;
import javax.microedition.io.Connection;
import javax.obex.HeaderSet;
import javax.obex.Operation;
import javax.obex.ResponseCodes;

import org.smool.sdk.gateway.bluetooth.comm.util.RemoteBTConnector;


public class IdleState  extends ExchangerState{

	
	public IdleState() {
		super();
	}
	
	@Override
	public int onGet(Operation op) {
		return ResponseCodes.OBEX_HTTP_OK;
	}

	@Override
	public int onPut(Operation op, Connection _conn, ServerThread serverThread) {
		InputStream in=null;
		DataInputStream din=null;
		ByteArrayInputStream bin=null;
		try	{
			in = op.openInputStream();
			
			byte[] fullResult = null;
		
			byte[] buf = new byte[256];
			ByteArrayOutputStream bout = new ByteArrayOutputStream(2048);
			
			synchronized(this){
				int len=in.read(buf);
				while(len>=0){
					bout.write(buf,0,len);
					len=in.read(buf);
				}
			}
			
			fullResult = bout.toByteArray();
			bout.close();
			
			
			bin = new ByteArrayInputStream(fullResult);
			din = new DataInputStream(bin);
					
			int size=din.readInt();
			byte[] vData = new byte[size];
			din.read(vData);
			
			op.close();
			in.close();
			din.close();
			bin.close();

			RemoteDevice dev=RemoteDevice.getRemoteDevice(_conn);
			
			HeaderSet headerSet=op.getReceivedHeaders();
			String remoteUUID=(String)headerSet.getHeader(48);
			
			
			BTConnector btConnector=serverThread.getConnectors().get(dev.getBluetoothAddress());
			
			if(btConnector == null){//The Connector does not exist
				btConnector=new BTConnector(new RemoteBTConnector(dev, remoteUUID), serverThread);
				serverThread.notifyNewBluetoothConnection(btConnector);
			}
			btConnector.notifyOperation(vData);
			return ResponseCodes.OBEX_HTTP_OK;
		} 
		catch (Exception e) {
			e.printStackTrace();
		

			if(op!=null){
				try{
					op.close();
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}

			if(in!=null){
				try{
					in.close();
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
			if(din!=null){
				try{
					din.close();
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
			
			if(bin!=null){
				try{
					bin.close();
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
			return ResponseCodes.OBEX_HTTP_INTERNAL_ERROR;
		}
	}

	@Override
	public void stopBluetoothOperations() {
		
	}
	
	

}
