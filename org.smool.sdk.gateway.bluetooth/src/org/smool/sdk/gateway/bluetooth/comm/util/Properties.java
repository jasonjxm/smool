package org.smool.sdk.gateway.bluetooth.comm.util;

import java.io.InputStream;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Properties {
	
	private static HashMap<String, String> propertiesCache;
	
	private static Properties instance=null;
	
	private Properties(){
		propertiesCache=new HashMap<String, String>();
	}
	
	public static Properties getInstance(){
		if(instance==null)
			instance=new Properties();
		
		return instance;
	}
	
	public String getProperty(String _scope, String _propertyName){
	
		if(propertiesCache.containsKey(_propertyName))
			return propertiesCache.get(_propertyName);
		else{
			try{
				InputStream is = getClass().getResourceAsStream("/btGatewayProperties.xml");
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				DocumentBuilder builder = factory.newDocumentBuilder();
				Document doc = builder.parse(is);
				is.close();
				
				Node raiz=doc.getDocumentElement();
				NodeList nlProperty=XPathAPI.selectNodeList(raiz, "//"+_scope+"/"+_propertyName);
				
				String strPropertyValue=nlProperty.item(0).getFirstChild().getNodeValue();
				propertiesCache.put(_propertyName, strPropertyValue);
				return strPropertyValue;
				
			}catch(Exception e){
				e.printStackTrace();
				return new String();
			}
		}
	}

}
