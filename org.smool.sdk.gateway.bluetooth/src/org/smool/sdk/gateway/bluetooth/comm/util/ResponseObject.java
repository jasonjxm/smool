package org.smool.sdk.gateway.bluetooth.comm.util;

import org.smool.sdk.gateway.bluetooth.comm.BTConnector;


public class ResponseObject {
	
	private byte[] message;
	private String urlConnection;
	private int numFails;
	private BTConnector btConnector;
	
	public ResponseObject(byte[] _message, String _urlConnection, int _numFails, BTConnector _btConnector) {
		super();
		this.message = _message;
		this.urlConnection = _urlConnection;
		this.numFails=_numFails;
		this.btConnector=_btConnector;
	}
	
	public byte[] getMessage() {
		return message;
	}
	
	public void setMessage(byte[] message) {
		this.message = message;
	}
	
	public String getUrlConnection() {
		return urlConnection;
	}
	
	public void setUrlConnection(String urlConnection) {
		this.urlConnection = urlConnection;
	}
	
	public void setNumFails(int _numFails){
		this.numFails=_numFails;
	}
	
	public int getNumFails(){
		return this.numFails;
	}
	
	public BTConnector getConnector(){
		return this.btConnector;
	}

}
