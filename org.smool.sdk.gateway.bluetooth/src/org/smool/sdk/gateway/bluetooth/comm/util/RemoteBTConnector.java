package org.smool.sdk.gateway.bluetooth.comm.util;

import java.util.ArrayList;

import javax.bluetooth.RemoteDevice;

public class RemoteBTConnector {
	
	private RemoteDevice device;
	private ArrayList<String> vUrlServices;
	private String uuid;
	private boolean servicesSearched;

	public RemoteBTConnector(RemoteDevice _device, String _uuid){
		this.device=_device;
		this.vUrlServices=new ArrayList<String>();
		this.uuid=_uuid;
		this.servicesSearched=false;
	}
	
	
	public RemoteDevice getDevice(){
		return device;
	}
	
	public void setDevice(RemoteDevice _device){
		this.device=_device;
	}
	
	public void setUrlServices(ArrayList<String> _vUrlServices){
		this.vUrlServices=_vUrlServices;
	}
	
	public ArrayList<String> getUrlServices(){
		return this.vUrlServices;
	}
	
	public void addUrlService(String _urlService){
		this.vUrlServices.add(_urlService);
	}
	
	public String getUuid(){
		return this.uuid;
	}
	
	public boolean areServicesSearched(){
		return servicesSearched;
	}
	
	public void servicesSearched(){
		this.servicesSearched=true;
	}
}
