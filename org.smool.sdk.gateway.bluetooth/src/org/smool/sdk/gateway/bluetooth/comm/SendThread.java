package org.smool.sdk.gateway.bluetooth.comm;

import java.util.ArrayList;
import java.util.Date;

import org.smool.sdk.gateway.bluetooth.comm.util.Properties;
import org.smool.sdk.gateway.bluetooth.comm.util.ResponseObject;



public class SendThread implements Runnable {
	
	private Thread sendThread;
	private ArrayList<ResponseObject> pendingResponses;
	private Date timeLastSend=null;
	private ServerThread serverThread;
	
	private final static int SEND_INTERVAL=Integer.parseInt(Properties.getInstance().getProperty("btGatewayProperties", "SendingInterval"));
	private boolean finish;
	
	public SendThread(ServerThread _serverThread){
		this.sendThread=new Thread(this, "Bluetooth SendThread");
		this.pendingResponses=new ArrayList<ResponseObject>();
		this.serverThread=_serverThread;
		this.finish=false;
	}
	
	public synchronized void addRequestObject(ResponseObject _responseObject){
		this.pendingResponses.add(_responseObject);
		if(!this.serverThread.isConnected())
			notify();
	}
	
	public void startSendThread(){
		this.sendThread.start();
	}
	
	
	public synchronized void stopSendThread(){
		this.finish=true;
		notify();
	}
	
	public synchronized void wakeUp(){
		notify();
	}

	
	public synchronized void run(){
		if(timeLastSend==null){
			timeLastSend=new Date(0);
		}
		
		while(!finish){
			if(!this.pendingResponses.isEmpty()){
				
				
				Date currentDate=new Date();
				if(currentDate.getTime()-timeLastSend.getTime()<SEND_INTERVAL){
					try{
						Thread.sleep(SEND_INTERVAL-(currentDate.getTime()-timeLastSend.getTime()));
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				
				if(!this.serverThread.isConnected()){
					
					ResponseObject responseObject=this.pendingResponses.get(0);
					
					SendState sendState=new SendState(responseObject.getUrlConnection());
					this.serverThread.setState(sendState);
					
					
					sendState.doSend(responseObject.getMessage(), responseObject.getNumFails(), responseObject.getConnector(), this.serverThread);
					
					
					this.pendingResponses.remove(0);
					
					this.timeLastSend=new Date();
					
				}else{
					try{
						wait();
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				
			}else{
								
				try{
					wait();
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
	}
	
}
