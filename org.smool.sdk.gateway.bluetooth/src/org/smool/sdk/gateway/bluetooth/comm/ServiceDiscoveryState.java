package org.smool.sdk.gateway.bluetooth.comm;

import javax.bluetooth.DeviceClass;
import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.DiscoveryListener;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.ServiceRecord;
import javax.bluetooth.UUID;
import javax.microedition.io.Connection;
import javax.obex.Operation;

import org.smool.sdk.gateway.bluetooth.comm.util.RemoteBTConnector;



public class ServiceDiscoveryState extends ExchangerState implements DiscoveryListener, Runnable {

	private RemoteBTConnector remoteConnector;
	private String uuid;
	private DiscoveryAgent agent;
	private Thread serviceDiscoveryThread;
	private int transactionId;
	private static ServerThread serverThread;
	
	

	public ServiceDiscoveryState(ServerThread _parent, RemoteBTConnector _remoteConnector, String _uuid) throws Exception{
		super();
		serverThread=_parent;
		this.remoteConnector=_remoteConnector;
		this.remoteConnector.getUrlServices().clear();
		this.uuid=_uuid;
		
		// initiate Bluetooth
		LocalDevice local = LocalDevice.getLocalDevice();
		agent = local.getDiscoveryAgent();
		
		this.serviceDiscoveryThread=new Thread(this);
		this.serviceDiscoveryThread.start();

	}

	
	@Override
	public synchronized void serviceSearchCompleted(int arg0, int arg1) {
		this.remoteConnector.servicesSearched();
		notify();
	}

	@Override
	public void servicesDiscovered(int arg0, ServiceRecord[] _services) {
		for(int i=0;i<_services.length;i++){
			this.remoteConnector.getUrlServices().add(_services[i].getConnectionURL(ServiceRecord.NOAUTHENTICATE_NOENCRYPT, false));
		}
	
	}
	
	
	@Override
	public synchronized void run() {
		UUID[] uuids = new UUID[1];
		uuids[0] = new UUID(this.uuid, false);
		
		try{
			transactionId=agent.searchServices(null, uuids, this.remoteConnector.getDevice(), this);
			wait(); // wait until service search is done on this device
			
		}catch(Exception e){
			e.printStackTrace();
		}
		serverThread.serviceSearchFinished(this.remoteConnector.getDevice().getBluetoothAddress());
		
	}
	

	@Override
	public int onGet(Operation op) {
		return 0;
	}

	@Override
	public int onPut(Operation op, Connection _conn, ServerThread serverThread){
		return 0;
	}

	@Override
	public void stopBluetoothOperations() {
		agent.cancelServiceSearch(transactionId);
		notify();
		
	}

	
	@Override
	public void deviceDiscovered(RemoteDevice arg0, DeviceClass arg1) {}
	
	@Override
	public void inquiryCompleted(int arg0) {}


}
