package org.smool.sdk.gateway.bluetooth.comm;

import java.io.DataOutputStream;

import javax.microedition.io.Connection;
import javax.microedition.io.Connector;
import javax.obex.ClientSession;
import javax.obex.Operation;
import javax.obex.ResponseCodes;

import org.smool.sdk.gateway.bluetooth.comm.util.Properties;
import org.smool.sdk.gateway.bluetooth.comm.util.ResponseObject;
import org.smool.sdk.gateway.exception.GatewayException;


public class SendState extends ExchangerState{
	
	private ClientSession connection = null;
	private String url = null;
	private Operation operation = null;
	private DataOutputStream out = null;
	private static final int TIME_TO_WAIT_ON_FAIL_SEND=Integer.parseInt(Properties.getInstance().getProperty("btGatewayProperties", "TimeToWaitOnFailSend"));
	private static final int TIME_TO_DISCONNECT_ON_REMOTE=Integer.parseInt(Properties.getInstance().getProperty("btGatewayProperties", "TimeToDisconnectOnRemote"));
	private static final int TIME_TO_WRITE=500;
	
	public SendState(String _url) {
		super();
		url = _url;
	}

	@Override
	public int onGet(Operation op) {
		try{
			op.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return ResponseCodes.OBEX_HTTP_CONFLICT;
	}

	@Override
	public int onPut(Operation op, Connection _conn, ServerThread serverThread) {
		try{
			op.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return ResponseCodes.OBEX_HTTP_CONFLICT;
	}
	
	public void doSend(final byte[] _data, int _numFails, BTConnector _btConnector, ServerThread serverThread){
		try{
			connection = (ClientSession) Connector.open(url); //Can Throw an IOException
			connection.connect(null); //Can throw an IOException
			
			// Initiate the PUT request
			operation = connection.put(null); //Can throw an IOException
			
			out = operation.openDataOutputStream(); //Can throw an IOException
			
			byte[] vData = _data; 
			
			int vlen = vData.length;
			byte[] tmpBuf = new byte[vlen + 4];
			System.arraycopy(vData, 0, tmpBuf, 4, vlen);
			tmpBuf[0] = (byte) ((vlen >>> 24) & 0xff);
			tmpBuf[1] = (byte) ((vlen >>> 16) & 0xff);
			tmpBuf[2] = (byte) ((vlen >>> 8) & 0xff);
			tmpBuf[3] = (byte) ((vlen >>> 0) & 0xff);
			
			//sending data
			out.write(tmpBuf); //Can throw an IOException
			
			Thread.sleep(TIME_TO_WRITE);
			try{
				out.close();
			}catch(Exception e){
				e.printStackTrace();
				out.flush();
				out.close();
			}
			
			out=null;
			
			int responseCode = operation.getResponseCode();//Can throw an IOException

			operation.close();//Can throw an IOExceptoin
			serverThread.setState(new IdleState());
			
	        operation=null;
	        
	        
	        if(responseCode != ResponseCodes.OBEX_HTTP_OK){
	        	if(_numFails < serverThread.getResponseRetryNumber()){
	           		Thread.sleep(TIME_TO_WAIT_ON_FAIL_SEND);
					serverThread.addPendingResponse(new ResponseObject(_data, url, ++_numFails, _btConnector));
	        	}
	        }
		}catch(Exception exception){
			exception.printStackTrace();
			//TODO [JFGP] no lo borro con el fin de obtener los distintos mensajes conforme se produzcan fallos
			System.out.println("Mensaje "+exception.getMessage());
			if(_numFails < serverThread.getResponseRetryNumber()){
				try{
					Thread.sleep(TIME_TO_WAIT_ON_FAIL_SEND);
				}catch(Exception ex){
					ex.printStackTrace();
				}
				
				serverThread.addPendingResponse(new ResponseObject(_data, url, ++_numFails, _btConnector));
			}
			else{
				//TODO [JFGP] Excepciones durante una conexion
				//Posibles excepciones:
				//java.io.IOException: Failed to connect. [111] Connection refused --> fireConnectionError()
				//javax.bluetooth.BluetoothConnectionException: Connection timeout --> fire ConnectionTimeout()
				
				//_btConnector.fireConnectionXXXX(new GatewayException(e.getMessage(),e));
				if(exception instanceof java.io.IOException){
					_btConnector.fireConnectionError(new GatewayException(exception.getMessage()));
				}else if(exception instanceof javax.bluetooth.BluetoothConnectionException){
					_btConnector.fireConnectionTimeout();
				}
			}
				
		}
		finally	{
		      try{
		    	  if(out!=null){
		    		  out.close();
		    	  }
		      }catch(Exception e){
		    	  e.printStackTrace();
		      }
		      out=null;
			
		      try { // finalizing operation
		    	  if(operation!=null){
		    		  operation.close();
		    	  }
		      } 
		      catch (Exception e3) {
		    	e3.printStackTrace();
		      }
		      operation=null;
		      
		      try{
		    	  if(connection!=null){
		    		  connection.disconnect(null);
		    	  }
		      }catch(Exception e){
		    	  e.printStackTrace();
		      }
		      try{
		    	  if(connection!=null){
		    		  connection.close();
		    	  }
		      }catch(Exception e){
		    	  e.printStackTrace();
		      }
		      
		    		      
		      connection=null;
		      
		      try{
		    	  Thread.sleep(TIME_TO_DISCONNECT_ON_REMOTE);
		      }catch(Exception e){
		    	  e.printStackTrace();
		      }
		      
		      if(!(serverThread.getState() instanceof IdleState)){
		    	  serverThread.setState(new IdleState());
		      }
		
		    }
	}

	
	@Override
	public void stopBluetoothOperations() {
		
		if(connection!=null){
			try{
				connection.close();
			}catch(Exception e){
				e.printStackTrace();
			}
			connection=null;
		}
		
		if(operation!=null){
			try{
				operation.abort();
			}catch(Exception e){
				e.printStackTrace();
			}
			try{
				operation.close();
			}catch(Exception e){
				e.printStackTrace();
			}
			operation=null;
		}
		
		if(out != null){
			try{
				out.close();
			}catch(Exception e){
				e.printStackTrace();
			}
			out=null;
		}
		
	}
	
	

}
