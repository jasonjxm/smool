package org.smool.sdk.gateway.bluetooth.comm;

import javax.obex.Operation;
import javax.microedition.io.Connection;

public abstract class ExchangerState {
	
	public ExchangerState(){
		
	}
	
	public abstract int onGet(Operation op);
	public abstract int onPut(Operation op, Connection _conn, ServerThread _serverThread);
	public abstract void stopBluetoothOperations();
}
