package org.smool.sdk.gateway.bluetooth.comm;

import java.io.IOException;
import java.util.HashMap;

import javax.bluetooth.BluetoothStateException;
import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.ServiceRecord;
import javax.microedition.io.Connection;
import javax.microedition.io.Connector;
import javax.obex.HeaderSet;
import javax.obex.Operation;
import javax.obex.ServerRequestHandler;
import javax.obex.SessionNotifier;

import org.smool.sdk.gateway.bluetooth.BluetoothGateway;
import org.smool.sdk.gateway.bluetooth.comm.util.Properties;
import org.smool.sdk.gateway.bluetooth.comm.util.RemoteBTConnector;
import org.smool.sdk.gateway.bluetooth.comm.util.ResponseObject;
import org.smool.sdk.sib.service.ISIB;




public class ServerThread extends ServerRequestHandler implements Runnable{
	
	private final static int MAX_TIME_TO_DISCONNECT=Integer.parseInt(Properties.getInstance().getProperty("btGatewayProperties", "RemoteConnectionMaxTimeHold"));
	private final static String SERVICE_ADDRESS="SERVICE_ADDRESS";
	
	private BluetoothGateway btGateway;
	
	private static ExchangerState currentState;
	private String serverURL;
	
	private boolean cancelWaitingInvoked = false;
	private boolean connected = false;
	
	private Thread waitingThread;
	private SendThread sendThread;
	
	private SessionNotifier notifier = null;
	private Connection con = null;
	private ISIB sib;

	
	public ServerThread(BluetoothGateway newBluetoothConnectionlistener){
		this.btGateway=newBluetoothConnectionlistener;
		this.sendThread=new SendThread(this);
		this.sendThread.startSendThread();
		
	}
	
	public HashMap<String, BTConnector> getConnectors(){
		return this.btGateway.getConnectors();
	}
	
	public void notifyNewBluetoothConnection(BTConnector conn){
		this.btGateway.newBluetoothConnectionCreated(conn);
	}
	
	
	public void serviceSearchFinished(String _bluetoothAddress) {
		this.setState(new IdleState());
		
		this.getConnectors().get(_bluetoothAddress).notifyContinue();
	}

	
	public boolean isConnected() {
		return this.connected;
	}


	public void setSIB(ISIB _sib){
		this.sib=_sib;
	}
	
	public ISIB getSIB(){
		return this.sib;
	}

	public ExchangerState getState() {
		return currentState;
	}
	
	public void setState(ExchangerState state) {
		 currentState = state;
	}
	
	public String getServerURL(){
		return this.serverURL;
	}
	
	public int getResponseRetryNumber(){
		return Integer.parseInt(this.btGateway.getConfig().getResponseRetryNumber());
	}
	

	public void startWaiting(String _serverURL) {
		this.serverURL=_serverURL;
		
		cancelWaitingInvoked = false;
		waitingThread = new Thread(this);//instatiates the class a thread 
		waitingThread.start();//Starts the thread
		
		setState(new IdleState());
	}
	 
	public void cancelWaiting(){
		getState().stopBluetoothOperations();
		cancelWaitingInvoked = true;

		try{
			notifier.close(); // indicate to acceptAndOpen that it is canceled
		}catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public int onGet(Operation op) {
		return getState().onGet(op);
	}

	public int onPut(Operation op) {
		return getState().onPut(op, con, this);
	}

	public synchronized void onDisconnect(HeaderSet request, HeaderSet reply) {
		super.onDisconnect(request, reply);
		notify();// stops waiting in run()
	}
	
	
	
	@Override
	public synchronized void run() {
		
		// initialize stack and make the device discoverable
		try{
			LocalDevice local = LocalDevice.getLocalDevice();
			local.setDiscoverable(DiscoveryAgent.GIAC); 
		}catch (Exception e){
			e.printStackTrace();
			return;
		}
		
		try{
			notifier = (SessionNotifier) Connector.open(serverURL);
		}catch (IOException e2){
			e2.printStackTrace();
		}

		// the cycle stops only if cancelWaiting() was called
		do{
			try{   
				//NAO
				try {
					LocalDevice ld = LocalDevice.getLocalDevice();
					String serviceAddress = ld.getRecord((Connection)notifier).getConnectionURL(ServiceRecord.NOAUTHENTICATE_NOENCRYPT, true);
					java.util.Properties prop = this.btGateway.getProperties();
					prop.setProperty(SERVICE_ADDRESS, serviceAddress);
					
				} catch (BluetoothStateException e) {
					e.printStackTrace();
				}
				//fin NAO
				con = notifier.acceptAndOpen(this);
				this.connected=true;
				
				
				//If any problem happens in client side, the gateway will disconnect in 30 seconds
				wait(MAX_TIME_TO_DISCONNECT); // wait until the remote device disconnects or timeout
				
				try{
					if(con!=null){
						con.close();
					}
	
				}catch (Exception e0){
					e0.printStackTrace();
					con=null;
				}
					
				this.connected=false;
				this.notifyConnectorsWaitingDisconnectionFromDevice();
				this.sendThread.wakeUp();
				
			}catch (Exception e){
				return;
			}

		} while (!cancelWaitingInvoked);
		try{
			notifier.close();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	private void notifyConnectorsWaitingDisconnectionFromDevice(){
		for(BTConnector con:this.getConnectors().values()){
			if(!con.isSearching())
				con.notifyContinue();
		}
			
	}
	

	public void searchServicesForRemoteBTConnector(RemoteBTConnector _remoteBTConnector){
		try{
			
			this.setState(new ServiceDiscoveryState(this, _remoteBTConnector, _remoteBTConnector.getUuid()));
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	public void addPendingResponse(ResponseObject _responseObject){
			this.sendThread.addRequestObject(_responseObject);
	}
	
}
