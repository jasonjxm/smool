package org.smool.sdk.gateway.bluetooth.comm;

import java.util.ArrayList;

import org.smool.sdk.gateway.bluetooth.comm.util.Properties;
import org.smool.sdk.gateway.bluetooth.comm.util.RemoteBTConnector;
import org.smool.sdk.gateway.bluetooth.comm.util.ResponseObject;
import org.smool.sdk.gateway.connector.impl.Connector;

import com.intel.bluetooth.BlueCoveConfigProperties;


public class BTConnector extends Connector {

	
	private ServerThread serverThread;
	private RemoteBTConnector remoteBTConnector;
	private static final String OBEX_MTU="4096";
	private static final String CONNECTION_TIMEOUT=Properties.getInstance().getProperty("btGatewayProperties", "ConnectionTimeout");
	private static int id=0;
	private Thread thr=null;
	private boolean searching=false;
	
	public BTConnector(RemoteBTConnector _remoteBTConnector, ServerThread _serverThread){
			
		System.setProperty(BlueCoveConfigProperties.PROPERTY_CONNECT_TIMEOUT, CONNECTION_TIMEOUT);
		System.setProperty(BlueCoveConfigProperties.PROPERTY_OBEX_TIMEOUT, CONNECTION_TIMEOUT);
		System.setProperty(BlueCoveConfigProperties.PROPERTY_OBEX_MTU, OBEX_MTU);
		
		this.remoteBTConnector=_remoteBTConnector;
		this.serverThread=_serverThread;
	}
	
	
	
	public int getId(){
		return id++;
	}
	
	public void stopBluetoothGateway(){
		this.serverThread.cancelWaiting();
	}
	
	public boolean isSearching(){
		return this.searching;
	}
	
	
	
	public synchronized void notifyContinue(){
		if(thr!=null){
			synchronized(thr){
				thr.notify();
			}
		}
	}
	
	public RemoteBTConnector getRemoteBTConnector(){
		return this.remoteBTConnector;
	}
	
	
	public synchronized void notifyOperation(final byte[] _vData){
		
		thr=new Thread(new Runnable() {
            	public void run() {
            		if(serverThread.isConnected()){
            			try{
            				synchronized(thr){
            					thr.wait();
            				}
            			}catch(Exception e){
            				e.printStackTrace();
            			}
            		}
            	            		            		            		
            		try{
            			//No lo parseo por Dom para no sobrecargar mucho
            			String strMessage=new String(_vData);
            			
            			if(strMessage.contains("<transaction_type>JOIN</transaction_type>")){
            				serverThread.searchServicesForRemoteBTConnector(remoteBTConnector);
            				
            				try{
            					synchronized(thr){
            						searching=true;
            						thr.wait();
            						searching=false;
            					}
            				}catch(Exception e){
            					e.printStackTrace();
            				}
            			}
            			System.out.println("Receives a message from a Kp");
            			
            			fireMessageReceived(_vData);
            			
            			
            		}catch(Exception e){
            			e.printStackTrace();
            		}
            		
            	}
           	});
		thr.start();
		
	}
	
	


	@Override
	public void sendToKP(byte[] ba) {
		
		ArrayList<String> remoteServices=this.remoteBTConnector.getUrlServices();
		
		for(String urlService:remoteServices){
			System.out.println("Sends a message to a KP");
			
			this.sendResponse(ba, urlService);
		}
		
	}

	
	private void sendResponse(byte[] _obj, String _urlConnection){
		this.serverThread.addPendingResponse(new ResponseObject(_obj, _urlConnection, 0, this));
	}


	@Override
	public void close() {
		this.serverThread.cancelWaiting();
	}

}
