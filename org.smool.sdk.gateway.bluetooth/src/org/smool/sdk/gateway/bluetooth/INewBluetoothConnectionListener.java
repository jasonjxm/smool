package org.smool.sdk.gateway.bluetooth;

import org.smool.sdk.gateway.bluetooth.comm.BTConnector;

public interface INewBluetoothConnectionListener {
	public void newBluetoothConnectionCreated(BTConnector connector);

}
