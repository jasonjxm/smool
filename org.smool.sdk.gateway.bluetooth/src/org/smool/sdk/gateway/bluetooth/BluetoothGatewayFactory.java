package org.smool.sdk.gateway.bluetooth;

import java.util.Properties;

import org.osgi.framework.BundleContext;
import org.smool.sdk.gateway.AbstractGateway;
import org.smool.sdk.gateway.AbstractGatewayFactory;
import org.smool.sdk.gateway.exception.GatewayException;
import org.smool.sdk.sib.service.ISIB;


public class BluetoothGatewayFactory extends AbstractGatewayFactory{

	private static final String TYPE = "Bluetooth";
	private static final int UUIDLENGTH = 32; 
	
	public BluetoothGatewayFactory(BundleContext bc) {
		super(bc);	
	}
	
	@Override
	public AbstractGateway createNewInstance(String name, ISIB sib, Properties properties) throws GatewayException {
		try {
			if(checkUUID(properties.getProperty(BluetoothGatewayConfiguration.UUID)) && checkIntegerPositiveParameter(properties.getProperty(BluetoothGatewayConfiguration.RESPONSE_RETRY_NUMBERS))){
				BluetoothGateway gw = new BluetoothGateway(name, getType(), sib, properties);
				System.out.println("Created a new Bluetooth Gateway instance [" + gw.getName() + "]");
				return gw;
			}else{
				throw new GatewayException("Can not create a Bluetooth gateway instance.");
			}

		} catch(Exception e) {
			throw new GatewayException(e.getMessage(), e);
		}
	}

	@Override
	public Properties getConfigurableProperties() {
		return BluetoothGatewayConfiguration.getDefaultProperties();
	}

	@Override
	public String getType() {
		return TYPE;
	}

	private boolean checkUUID(String uuid) throws GatewayException{
		
		if(uuid.length() != UUIDLENGTH){
			throw new GatewayException("Malformed UUID, a correct UUID is a 32-hexadecimal caracters String. To generate one, please visit http://www.famkruithof.net/uuid/uuidgen");
		}else{
			
			for(int i=0;i<uuid.length();i++){
				int currentValue=(int)uuid.charAt(i);
				if(!((currentValue>=(int)'0' && currentValue<=(int)'9') || (currentValue>=(int)'A' && currentValue<=(int)'F') || (currentValue>=(int)'a' && currentValue<=(int)'f')))
					throw new GatewayException("Malformed UUID, a correct UUID is a 32-hexadecimal caracters String. To generate one, please visit http://www.famkruithof.net/uuid/uuidgen");
			}
		}
		
		for(AbstractGateway gw : instances.values()) {
			BluetoothGateway btgw = (BluetoothGateway)gw;
					
			if(uuid.equalsIgnoreCase(btgw.getConfig().getUUID())) {
				throw new GatewayException("UUID " + uuid + " is already used by gateway " + gw.getName());
			}
		}
		
		return true;
		
	}
	
	private boolean checkIntegerPositiveParameter(String _str){
		try{
			int value=Integer.parseInt(_str);
			if(value<0)
				return false;
		}catch(Exception e){
			return false;
		}
		
		return true;
	}

}
