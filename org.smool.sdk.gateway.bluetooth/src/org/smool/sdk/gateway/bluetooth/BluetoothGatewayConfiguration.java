package org.smool.sdk.gateway.bluetooth;

import java.util.Properties;

public class BluetoothGatewayConfiguration {

	public static final String UUID="UUID";
	public static final String RESPONSE_RETRY_NUMBERS="RESPONSE_RETRY_NUMBERS";
	
	/** The Bluetooth Gateway parameter default values */	
	public static final String DEFAULT_UUID = "ed495afe28ed11da94d900e08161165f";
	public static final String DEFAULT_REPONSE_RETRY_NUMBERS="4";
		
	private Properties properties;

	public BluetoothGatewayConfiguration() {
		this.properties=BluetoothGatewayConfiguration.getDefaultProperties();
	}
	
	public void setProperties(Properties properties) {
		if(properties == null) {
			return;
		}
		for(Object key : properties.keySet()) {
			Object value = properties.get(key);
			this.properties.put(key, value);
		}
	}
	
	public String getUUID(){
		Object objUuid=this.properties.get(UUID);
		if(objUuid==null)
			throw new NullPointerException();
		
		else return (String) objUuid;
	}
	
	
	public String getResponseRetryNumber(){
		Object objResponseRetryNumbers=this.properties.get(RESPONSE_RETRY_NUMBERS);
		if(objResponseRetryNumbers==null)
			throw new NullPointerException();
		
		else return (String) objResponseRetryNumbers;
		
	}
	
	
	public static Properties getDefaultProperties(){
		Properties prop=new Properties();
		prop.put(UUID, DEFAULT_UUID);
		prop.put("RESPONSE_RETRY_NUMBERS", DEFAULT_REPONSE_RETRY_NUMBERS);
			
		return prop;
	}
	
}
