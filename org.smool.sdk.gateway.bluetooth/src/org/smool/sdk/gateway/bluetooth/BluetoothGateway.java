package org.smool.sdk.gateway.bluetooth;

import java.util.Collection;
import java.util.HashMap;
import java.util.Properties;

import org.smool.sdk.gateway.AbstractGateway;
import org.smool.sdk.gateway.bluetooth.comm.BTConnector;
import org.smool.sdk.gateway.bluetooth.comm.ServerThread;
import org.smool.sdk.gateway.exception.GatewayException;
import org.smool.sdk.gateway.session.impl.Session;
import org.smool.sdk.sib.service.ISIB;


public class BluetoothGateway extends AbstractGateway implements INewBluetoothConnectionListener{
	
	
	private ServerThread btServerThread;
	private String serverURL;
	private BluetoothGatewayConfiguration config;
	
			

	public BluetoothGateway(String name, String type, ISIB sib, Properties properties) {
		super(name, type, sib, properties);
		this.config=new BluetoothGatewayConfiguration();
		this.config.setProperties(properties);
		serverURL="btgoep://localhost:"+this.config.getUUID()+";name=btgwsib"+sib.getName()+"_-_-_"+this.getName();
		
	}
	
	public HashMap<String, BTConnector> getConnectors(){
		HashMap<String, BTConnector> btConns=new HashMap<String, BTConnector>();
		
		Collection<Session> values=this.sessions.values();
		
		for(Session currentSession:values){
			if(currentSession.getConnector() instanceof BTConnector){
				BTConnector btConn=(BTConnector)currentSession.getConnector();
				btConns.put(btConn.getRemoteBTConnector().getDevice().getBluetoothAddress(), btConn);
			}
		}
		
		return btConns;
	}

	@Override
	public void startGateway() throws GatewayException {
		btServerThread=new ServerThread(this);
		btServerThread.startWaiting(this.serverURL);
		
	}

	@Override
	public void stopGateway() throws GatewayException {
		if(this.btServerThread!=null){
			this.btServerThread.cancelWaiting();
		
			try{
				Thread.sleep(10000);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
	}
	
	public void newBluetoothConnectionCreated(BTConnector connector){
		Session session = new Session();
		
		session.setSIB(this.sib);
		session.setConnector(connector);
		this.addSession(session);
	}

	public BluetoothGatewayConfiguration getConfig(){
		return this.config;
	}
}
