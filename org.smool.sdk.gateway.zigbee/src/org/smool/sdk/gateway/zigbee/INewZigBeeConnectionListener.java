package org.smool.sdk.gateway.zigbee;

import org.smool.sdk.gateway.zigbee.comm.ZigBeeConnector;

public interface INewZigBeeConnectionListener {
	public void newZigBeeConnectionCreated(ZigBeeConnector connector);
}
