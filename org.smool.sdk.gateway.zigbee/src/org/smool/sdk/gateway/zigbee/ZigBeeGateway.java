package org.smool.sdk.gateway.zigbee;

import java.util.Collection;
import java.util.HashMap;
import java.util.Properties;

import org.smool.sdk.gateway.AbstractGateway;
import org.smool.sdk.gateway.exception.GatewayException;
import org.smool.sdk.gateway.session.impl.Session;
import org.smool.sdk.gateway.zigbee.comm.ZigBeeConnector;
import org.smool.sdk.gateway.zigbee.comm.ZigBeeServer;
import org.smool.sdk.gateway.zigbee.comm.exception.ZigBeeConnectorNotOpen;
import org.smool.sdk.sib.service.ISIB;



public class ZigBeeGateway extends AbstractGateway implements INewZigBeeConnectionListener{

	private ZigBeeGatewayConfiguration config;
	private ZigBeeServer zbServer;	
	
	
	public ZigBeeGateway(String name, String type, ISIB sib, Properties properties) {
		super(name, type, sib, properties);
		
		this.config=new ZigBeeGatewayConfiguration();
		this.config.setProperties(properties);
		
	}
	
	public HashMap<String, ZigBeeConnector> getConnectors(){
		HashMap<String, ZigBeeConnector> btConns=new HashMap<String, ZigBeeConnector>();
		
		Collection<Session> values=this.sessions.values();
		
		for(Session currentSession:values){
			if(currentSession.getConnector() instanceof ZigBeeConnector){
				ZigBeeConnector zbConn=(ZigBeeConnector)currentSession.getConnector();
				btConns.put(zbConn.getRemoteAddress64(), zbConn);
			}
		}
		
		return btConns;
	}

	@Override
	public void startGateway() throws GatewayException {
		
		System.out.println("Starting Gateway");
		
		this.zbServer=new ZigBeeServer(this.config.getPort(), Integer.parseInt(this.config.getRate()), this);
		
		try {
			this.zbServer.openServer();
		} catch (ZigBeeConnectorNotOpen e) {
			e.printStackTrace();
			throw new GatewayException("Could not start ZigBee server");
		}
		
	}

	@Override
	public void stopGateway() throws GatewayException {
		this.zbServer.closeServer();
		
	}

	@Override
	public void newZigBeeConnectionCreated(ZigBeeConnector connector) {
		connector.setServer(this.zbServer);
		
		Session session = new Session();
		session.setSIB(this.sib);
		session.setConnector(connector);
		this.addSession(session);
		
	}
	
	public ZigBeeGatewayConfiguration getConfig(){
		return this.config;
	}
	
	public ZigBeeServer getZigBeeServer(){
		return this.zbServer;
	}

}
