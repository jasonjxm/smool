package org.smool.sdk.gateway.zigbee;

import java.util.Properties;

public class ZigBeeGatewayConfiguration {

	public static final String PORT="PORT";
	public static final String RATE="BAUDS_RATE";
	
	public static final String DEFAULT_PORT="COM2";
	public static final String DEFAULT_RATE="9600";
	
	private Properties properties;
	
	public ZigBeeGatewayConfiguration(){
		this.properties=ZigBeeGatewayConfiguration.getDefaultProperties();
	}
	
	public void setProperties(Properties properties) {
		if(properties == null) {
			return;
		}
		for(Object key : properties.keySet()) {
			Object value = properties.get(key);
			this.properties.put(key, value);
		}
	}
	
	public String getPort(){
		Object objPort=this.properties.get(PORT);
		if(objPort==null)
			throw new NullPointerException();
		
		else return (String) objPort;
	}
	
	public String getRate(){
		Object objRate=this.properties.get(RATE);
		if(objRate==null)
			throw new NullPointerException();
		
		else return (String) objRate;
	}
	
	public static Properties getDefaultProperties(){
		Properties prop=new Properties();
		prop.put(PORT, DEFAULT_PORT);
		prop.put(RATE, DEFAULT_RATE);
			
		return prop;
	}
		
}
