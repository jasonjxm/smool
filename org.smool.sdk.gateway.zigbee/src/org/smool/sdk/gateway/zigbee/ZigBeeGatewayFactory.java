package org.smool.sdk.gateway.zigbee;

import java.util.Properties;

import org.osgi.framework.BundleContext;
import org.smool.sdk.gateway.AbstractGateway;
import org.smool.sdk.gateway.AbstractGatewayFactory;
import org.smool.sdk.gateway.exception.GatewayException;
import org.smool.sdk.sib.service.ISIB;


public class ZigBeeGatewayFactory extends AbstractGatewayFactory{
	
	private static final String TYPE = "ZigBee";

	public ZigBeeGatewayFactory(BundleContext bc) {
		super(bc);
	}

	@Override
	public AbstractGateway createNewInstance(String name, ISIB sib, Properties properties) throws GatewayException {
		try {
			if(checkIntegerPositiveParameter(properties.getProperty(ZigBeeGatewayConfiguration.RATE))){
				ZigBeeGateway gw = new ZigBeeGateway(name, getType(), sib, properties);
				System.out.println("Created a new ZigBee Gateway instance [" + gw.getName() + "]");
				return gw;
			}else{
				throw new GatewayException("Can not create a ZigBee gateway instance.");
			}

		} catch(Exception e) {
			throw new GatewayException(e.getMessage(), e);
		}
	}

	@Override
	public Properties getConfigurableProperties() {
		return ZigBeeGatewayConfiguration.getDefaultProperties();
	}

	@Override
	public String getType() {
		return TYPE;
	}
	
	
	private boolean checkIntegerPositiveParameter(String _str){
		try{
			int value=Integer.parseInt(_str);
			if(value<0)
				return false;
		}catch(Exception e){
			return false;
		}
		
		return true;
	}

}
