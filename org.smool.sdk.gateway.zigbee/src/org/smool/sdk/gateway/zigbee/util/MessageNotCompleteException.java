package org.smool.sdk.gateway.zigbee.util;

@SuppressWarnings("serial")
public class MessageNotCompleteException extends Exception{
	
	public MessageNotCompleteException(){
		super("The message is not completed yet");
	}

}
