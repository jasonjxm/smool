package org.smool.sdk.gateway.zigbee.util;

import java.util.Hashtable;

public class ConcreteMessage {
	
	private Hashtable<Integer,int[]> htParts;
	private int numParts;
	private int idMessage;
	
	private long lastUpdated;
	
	
	public ConcreteMessage(int _idMessage){
		this.htParts=new Hashtable<Integer, int[]>();
		this.numParts=0;
		this.idMessage=_idMessage;
		this.lastUpdated=new java.util.Date().getTime();
		
	}
	
	public void setNumParts(int _numParts){
		this.numParts=_numParts;
	}
	
	public void addPart(int _partNumber, int[] _partialMessage){
		htParts.put(_partNumber, _partialMessage);
		this.lastUpdated=new java.util.Date().getTime();
	}
	
	
	public int[] getMessage() throws MessageNotCompleteException{
		int messageSize=0;
		
		if(this.numParts==0) throw new MessageNotCompleteException();
		
		else{
			for(int i=0;i<this.numParts;i++){
				int[] part = htParts.get(i);
				
				if(part != null){
					messageSize+=part.length;
				}else throw new MessageNotCompleteException();
				
			}
			
			int[] completeMessage=new int[messageSize];
			int completeMessageIndex=0;
			
			for(int i=0;i<this.numParts;i++){
				int[] part=htParts.get(i);
				
				if(part != null){
					System.arraycopy(part, 0, completeMessage, completeMessageIndex, part.length);
					completeMessageIndex+=part.length;
				}else throw new MessageNotCompleteException();
			}
			
			return completeMessage;
		}
		
	}

	public int getNumParts() {
		return numParts;
	}

	public int getIdMessage() {
		return idMessage;
	}
	
	
	public boolean isCompleted(){
		return this.htParts.size()==this.getNumParts();
	}
	
	public long getLastUpdated(){
		return this.lastUpdated;
	}
	
	
}
