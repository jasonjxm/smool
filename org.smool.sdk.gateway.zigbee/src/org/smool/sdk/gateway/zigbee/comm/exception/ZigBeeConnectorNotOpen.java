package org.smool.sdk.gateway.zigbee.comm.exception;

@SuppressWarnings("serial")
public class ZigBeeConnectorNotOpen extends Exception{
	
	public ZigBeeConnectorNotOpen(String _port, int _baudRate){
		super("Can not open serial port "+_port+" at "+_baudRate+" bauds");
	}

}
