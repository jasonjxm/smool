package org.smool.sdk.gateway.zigbee.comm.exception;

@SuppressWarnings("serial")
public class ZigBeeRequestException extends Exception{

	public ZigBeeRequestException(String _strAddr){
		super("Can not send message to "+_strAddr+" device");
	}
}
