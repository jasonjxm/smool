package org.smool.sdk.gateway.zigbee.comm;

import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.smool.sdk.gateway.zigbee.ZigBeeGateway;
import org.smool.sdk.gateway.zigbee.comm.exception.ZigBeeConnectorNotOpen;
import org.smool.sdk.gateway.zigbee.comm.exception.ZigBeeRequestException;
import org.smool.sdk.gateway.zigbee.util.ConcreteMessage;
import org.smool.sdk.gateway.zigbee.util.MessageBuilder;

import com.rapplogic.xbee.api.ApiId;
import com.rapplogic.xbee.api.PacketListener;
import com.rapplogic.xbee.api.XBeeAddress16;
import com.rapplogic.xbee.api.XBeeResponse;
import com.rapplogic.xbee.api.zigbee.ZNetRxResponse;



public class RequestsManager implements PacketListener{
	
			
	private ZigBeeGateway zbGateway;
	private ExecutorService responsesProcessor;
	
	public RequestsManager(ZigBeeGateway _zbGateway){
		super();
	
		this.zbGateway=_zbGateway;
		this.responsesProcessor=Executors.newSingleThreadExecutor();
	}
	
	@Override
	public void processResponse(final XBeeResponse response) {
		this.responsesProcessor.submit(new Runnable(){
				public void run(){
					if (response.getApiId() == ApiId.ZNET_RX_RESPONSE) {//It is a message from a KP
						
						ZNetRxResponse rx = (ZNetRxResponse)response;
						
						int[] receivedData=rx.getData();
						
						if(receivedData.length>= 4 && receivedData[0]=='s' && receivedData[1]=='i' && receivedData[2]=='b' && receivedData[3]=='?'){
							//Asking if we are a SIB
							try {
								zbGateway.getZigBeeServer().write(zbGateway.getSIB().getName().getBytes(), rx.getRemoteAddress64());
							} catch (ZigBeeRequestException e) {
								e.printStackTrace();
							}
						}else{
						
						
							HashMap<String, ZigBeeConnector> hmConnectors=zbGateway.getConnectors();
							String connKey=Arrays.toString(rx.getRemoteAddress64().getAddress());
						
							ZigBeeConnector zbConn=hmConnectors.get(connKey);
						
						
							if(zbConn == null){//The Connector does not exist
								try {
									zbConn=new ZigBeeConnector(rx.getRemoteAddress64());
								} catch (ZigBeeConnectorNotOpen e) {
									e.printStackTrace();
								}
								zbGateway.newZigBeeConnectionCreated(zbConn);
							}
						
						
							XBeeAddress16 remoteAddress=rx.getRemoteAddress16();
							int addrIdentifier=remoteAddress.get16BitValue();
									
						
							int idMensaje=receivedData[0];
							int idTrama=receivedData[1];
							int numTramas=1;
						
							int[] messageData;
							if(idTrama==0){//firt weft, the next four bytes contains the number of messages that will come
								numTramas=receivedData[2];
								messageData=new int[receivedData.length-3];
								System.arraycopy(receivedData, 3, messageData, 0, receivedData.length-3);
							}else{
								messageData=new int[receivedData.length-2];
								System.arraycopy(receivedData, 2, messageData, 0, receivedData.length-2);
							}
							
							MessageBuilder mb=MessageBuilder.getInstance();
							
							ConcreteMessage current=mb.getMessage(addrIdentifier, idMensaje);
							if(current==null){
								current=new ConcreteMessage(idMensaje);
								mb.addNewMessage(addrIdentifier, idMensaje, current);
							}
				
							if(idTrama==0){
								current.setNumParts(numTramas);
							}
							
							
							current.addPart(idTrama, messageData);
							
							if(current.isCompleted()){
								try{
									int[] message=current.getMessage();
									
									StringBuilder sb=new StringBuilder();
									for(int i=0;i<message.length;i++){
										sb.append((char)message[i]);
									}
									
									zbConn.notifyOperation(sb.toString().getBytes());
									
									mb.deleteMessage(addrIdentifier, idMensaje);
									
								}catch(Exception e){
									e.printStackTrace();
								}
							}
							
						}	
						
					}
				}
			});
		
		
	}
	
}
