package org.smool.sdk.gateway.zigbee.comm;

import java.util.Arrays;

import org.smool.sdk.gateway.connector.impl.Connector;
import org.smool.sdk.gateway.zigbee.comm.exception.ZigBeeConnectorNotOpen;

import com.rapplogic.xbee.api.XBeeAddress64;

public class ZigBeeConnector extends Connector {

	private static int id=0;
	
	private XBeeAddress64 remoteAddress;
	private ZigBeeServer zbServer;

	private Thread thr=null;
	
	public ZigBeeConnector(XBeeAddress64 _remoteAddress) throws ZigBeeConnectorNotOpen{
		this.remoteAddress=_remoteAddress;		
	}
	
	public String getRemoteAddress64(){
		return Arrays.toString(this.remoteAddress.getAddress());
	}
	
	
	public int getId(){
		return id++;
	}
	
	
	public void stopBluetoothGateway(){}
	
	
	public void close(){
		
	}
	
	
	public boolean isSearching(){
		return false;
	}
	
	
	public synchronized void notifyContinue(){}
	
	
	
	/*
	 * Mensaje recibido desde el KP
	 */
	public synchronized void notifyOperation(final byte[] _vData){
		
		thr=new Thread(new Runnable() {
        	public void run() {
        		try{
        			fireMessageReceived(_vData);
        			
        		}catch(Exception e){
        			e.printStackTrace();
        		}
        		
        	}
       	});
	thr.start();
	}
	
	public void setRemoteAddress(XBeeAddress64 _address){
		this.remoteAddress=_address;
	}
	
	public void setServer(ZigBeeServer _zbServer){
		this.zbServer=_zbServer;
	}
	
	public void sendToKP(byte[] ba){
		try{
			this.zbServer.write(ba, this.remoteAddress);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
