package org.smool.sdk.gateway.zigbee.comm;

import java.util.Arrays;

import org.smool.sdk.gateway.zigbee.ZigBeeGateway;
import org.smool.sdk.gateway.zigbee.comm.exception.ZigBeeConnectorNotOpen;
import org.smool.sdk.gateway.zigbee.comm.exception.ZigBeeRequestException;

import com.rapplogic.xbee.api.ApiId;
import com.rapplogic.xbee.api.XBee;
import com.rapplogic.xbee.api.XBeeAddress64;
import com.rapplogic.xbee.api.XBeeResponse;
import com.rapplogic.xbee.api.zigbee.ZNetTxRequest;
import com.rapplogic.xbee.api.zigbee.ZNetTxStatusResponse;
import com.rapplogic.xbee.api.zigbee.ZNetTxStatusResponse.DeliveryStatus;



public class ZigBeeServer {
	
	private final static int PAYLOAD_LENGTH=240;//240 is maximun number of bytes that supports in each communication
	private final static int MAX_RETRIES_NUMBER=5;
	
	private String serialPort;
	private int rate;
	
	private XBee xbee;
	private RequestsManager requestsManager;
	
	private int messageId;
	
	public ZigBeeServer(String _serialPort, int _rate, ZigBeeGateway _zbGateway){
		this.serialPort=_serialPort;
		this.rate=_rate;
		
		this.xbee=new XBee();
		
		this.requestsManager=new RequestsManager(_zbGateway);
		
		this.messageId=0;
	}
	
	public void openServer() throws ZigBeeConnectorNotOpen{
		try{
			
			this.xbee.open(this.serialPort, this.rate);
			
			this.xbee.addPacketListener(this.requestsManager);
			
		}catch(Exception e){
			e.printStackTrace();
			throw new ZigBeeConnectorNotOpen(this.serialPort, this.rate);
		}
	}
	
	public void write(byte[] _payload, XBeeAddress64 _address)throws ZigBeeRequestException{
		
		//Splits the payload in several fragments of 240 bytes. Thus it manages a number of message and a number of fragment
		
		//The format of each message will be
		//1st int for message id.
		//2nd int for weft id
		//3rd int for number of wefts (only for first weft)
		//the following 240 ints for payload
		this.messageId++;
		int weft;

		
		int[] payloadAsIntArray=ZigBeeServer.byteArrayToIntArray(_payload);
		int messagesNumber=(payloadAsIntArray.length/ZigBeeServer.PAYLOAD_LENGTH);
		if(payloadAsIntArray.length%ZigBeeServer.PAYLOAD_LENGTH != 0){
			messagesNumber++;
		}
		
		int retry=0;
		for(int i=0;i<messagesNumber;){
			
			int[] partialPayload=null;
			if(ZigBeeServer.PAYLOAD_LENGTH < payloadAsIntArray.length-(i*ZigBeeServer.PAYLOAD_LENGTH)){
				partialPayload=new int[ZigBeeServer.PAYLOAD_LENGTH];
				System.arraycopy(payloadAsIntArray, i*ZigBeeServer.PAYLOAD_LENGTH, partialPayload, 0, ZigBeeServer.PAYLOAD_LENGTH);
			}
			else{
				partialPayload=new int[payloadAsIntArray.length-(i*ZigBeeServer.PAYLOAD_LENGTH)];
				System.arraycopy(payloadAsIntArray, i*ZigBeeServer.PAYLOAD_LENGTH, partialPayload, 0, payloadAsIntArray.length-(i*ZigBeeServer.PAYLOAD_LENGTH));
			}
			
			
			
			weft=i;
			
			int[] currentPayload;
			if(weft==0)
				currentPayload=new int[partialPayload.length+3];
			else currentPayload=new int[partialPayload.length+2];
				
			currentPayload[0]=this.messageId;
			currentPayload[1]=weft;
			if(weft==0){
				currentPayload[2]=messagesNumber;
				System.arraycopy(partialPayload, 0, currentPayload, 3, partialPayload.length);
			}else{
				System.arraycopy(partialPayload, 0, currentPayload, 2, partialPayload.length);
			}
			
			
			
			ZNetTxRequest request = new ZNetTxRequest(_address, currentPayload);
			
			
			try{
				request.setFrameId(xbee.getNextFrameId());
				try{
					XBeeResponse response=this.xbee.sendSynchronous(request, 1500);//if timeout throws a com.rapplogic.xbee.api.XBeeTimeoutException
					if(response.getApiId() == ApiId.ZNET_TX_STATUS_RESPONSE){
						ZNetTxStatusResponse zNetResponse=(ZNetTxStatusResponse)response;
						if(zNetResponse.getDeliveryStatus() == DeliveryStatus.SUCCESS){
							i++;
						}
					}
					
					
				}catch(com.rapplogic.xbee.api.XBeeTimeoutException timeoutException){//Timeout
					
					if(retry<MAX_RETRIES_NUMBER)
						retry++;
					else throw new ZigBeeRequestException("Maximum number of retries reached");
				}
				
				
					
				
			}catch(Exception e){
				e.printStackTrace();
				
				throw new ZigBeeRequestException(Arrays.toString(_address.getAddress()));
			}
			
		}
		
		
		
	}
	
	public void closeServer(){
		this.xbee.close();
	}
	
	private static int[] byteArrayToIntArray(byte[] _data){
		
		int[] result=new int[_data.length];
		
		for(int i=0;i<_data.length;i++){
			result[i]=(int)_data[i];
		}
		return result;
		
	}

	
}
