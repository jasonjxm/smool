package org.smool.sdk.gateway.zigbee;

import java.util.Dictionary;
import java.util.Hashtable;
import org.eclipse.core.runtime.Plugin;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;
import org.smool.sdk.gateway.service.IGatewayFactory;
import org.smool.sdk.sib.service.ISIB;


/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends Plugin {

	private ZigBeeGatewayFactory zigBeeFactory;
	
	
	public Activator() {
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void start(BundleContext context) throws Exception {
		
		Dictionary props = new Hashtable();
		props.put("Gateway", "ZigBee");
		
		this.zigBeeFactory=new ZigBeeGatewayFactory(context);
		context.registerService(IGatewayFactory.class.getName(), this.zigBeeFactory, props);
		System.out.println("Manager for ZigBee registered");
		ServiceTracker tracker = new ServiceTracker(context, ISIB.class.getName(), this.zigBeeFactory);
		tracker.open();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.runtime.Plugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		zigBeeFactory.shutdown();
		zigBeeFactory = null;
		System.out.println("Manager for ZigBee unregistered");
	}
	
}
