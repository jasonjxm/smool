package org.smool.sdk.osgi.rest.servlet;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.http.HttpService;
import org.osgi.service.log.LogService;
import org.osgi.util.tracker.ServiceTracker;


/**
 * 
 * HttpService is the OSGi service that allows bundles in the OSGi environment 
 * to dynamically register and unregister both resources and servlets in the 
 * HttpService's URI namespace -- in other words, to map request URIs to either 
 * a static HTML file or to an HttpServlet. The HttpServiceTracker class is an 
 * object of type ServiceTracker, which simplifies the tracking of HttpService. 
 * 
 * @see http://www.javaworld.com/javaworld/jw-06-2008/jw-06-osgi3.html?page=3
 * 
 * @author Alejandro Villamarin
 * Modified by Mahmoud Mohamed, mmabdallah@itida.gov.eg - March,13rd 2013
 * Modified by Mahmoud Mohamed, mmabdallah@itida.gov.eg - March,18th 2013
 */

@SuppressWarnings("rawtypes")
public class HttpServiceTracker extends ServiceTracker{
    
	@SuppressWarnings("unchecked")
	public HttpServiceTracker(BundleContext context) {
        super(context, HttpService.class.getName(), null);
    }
    
	/**
	 * This is a callback method that will be invoked once HttpService is available
	 */
	@SuppressWarnings("unchecked")
	public Object addingService(ServiceReference reference) {
        
		HttpService httpService = (HttpService) context.getService(reference);
        
		try {
			//commennted because of using declative services 
            //httpService.registerServlet("/sib_overview", new RestServlet(), null, null);
            //httpService.registerServlet("/show_individual", new RestServlet(), null, null);
           // httpService.registerServlet("/show_contents", new RestServlet(), null, null);
            //httpService.registerServlet("/show_sessions", new RestServlet(), null, null);
            //httpService.registerServlet("/show_subscriptions", new RestServlet(), null, null);
            //Mahmoud 13-3-2013
            //httpService.registerServlet("/palette.json", new RestServlet(), null, null);
            //Mahmoud 13-3-2013
            
        } 
		
		catch (Exception e) {
			Activator.getDefault().getLogger().log(LogService.LOG_ERROR, "An error ocurred trying to register resources or servlet");
        }
        
		return httpService;
    }
	
    @SuppressWarnings("unchecked")
	public void removedService(ServiceReference reference, Object service) {
        HttpService httpService = (HttpService) service;
        httpService.unregister("/helloworld.html");
        httpService.unregister("/helloworld");
        super.removedService(reference, service);
    }
}
