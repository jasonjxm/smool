
package org.smool.sdk.osgi.rest.servlet;

//created by Mahmoud Mohamed, mmabdallah@itida.gov.eg - March,21th 2013
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.smool.sdk.sib.ssap.SIB;
import org.smool.sdk.ui.sibviewer.test.utils.Utils;

public class UpdateRestServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @param args
	 */
	private HashMap<String, SIB> runningSIBs;

	String readTriplesFile(String fileName) {
		BufferedReader br = null;
		String fileData = new String();
		try {

			String sCurrentLine;

			br = new BufferedReader(new FileReader(fileName));

			while ((sCurrentLine = br.readLine()) != null) {
				fileData = fileData + sCurrentLine + "\n";
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return fileData;
	}

	// Mahmoud 22-3-2013
	// convert triples to RDF-M3
	public String composeTripleParametes(String parType, String Subject, String SubjectType, String Predicate,
			String Object, String ObjectType) {
		String parameters = null;
		parameters = "<parameter name='" + parType + "' encoding='RDF-M3'>\n";

		parameters = parameters + "<triple_list xmlns=\"http://www.smool.org/ontology/individual#\"";
		parameters = parameters + " xml:base=\"http://www.smool.org/ontology/individual#\"";
		parameters = parameters + " xmlns:individual=\"http://www.smool.org/ontology/individual#\"";
		parameters = parameters + " xmlns:smoolcore=\"http://com.tecnalia.smool/core/smoolcore#\"";
		parameters = parameters + " xmlns:rdfs=\"http://www.w3.org/2000/01/rdf-schema#\"";
		parameters = parameters + " xmlns:xsd=\"http://www.w3.org/2001/XMLSchema#\"";
		parameters = parameters + " xmlns:owl=\"http://www.w3.org/2002/07/owl#\"";
		parameters = parameters + " xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">";
		parameters = parameters + " <triple>";
		// check type = "URI | bNode"
		if (SubjectType.isEmpty() || (SubjectType.equalsIgnoreCase("URI") == true)) {
			parameters = parameters + " <subject type = \"URI\">" + Subject + "</subject>";
		} else if (SubjectType.equalsIgnoreCase("bNode") == true) {
			parameters = parameters + " <subject type = \"bNode\">" + Subject + "</subject>";
		}
		parameters = parameters + " <predicate>" + Predicate + "</predicate>";
		// check type = "URI | bNode"
		if (ObjectType.isEmpty() || (ObjectType.equalsIgnoreCase("URI") == true)) {
			parameters = parameters + " <object type = \"URI\">" + Object + "</object>";
		} else if (ObjectType.equalsIgnoreCase("bNode") == true) {
			parameters = parameters + " <object type = \"bNode\">" + Object + "</object>";
		} else if (ObjectType.equalsIgnoreCase("literal") == true) {
			parameters = parameters + " <object type = \"literal\">" + Object + "</object>";
		}
		parameters = parameters + " </triple>";
		parameters = parameters + " </triple_list>\n";
		parameters = parameters + " </parameter>";

		return parameters;
	}

	// Mahmoud 22-3-2013
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String content = null;
		// http://localhost:8380/update?name=ttt&kpname=rest&subject=individual:_TemperatureSensor4192482265611770049&subjecttype=URI&predicate=rdf:type&object=smoolcore:TemperatureSensor&objecttype=bNode&newobject=smoolcore:physicalLoc
		// get the param that contains the sib ID that we must get
		Map<String, String[]> params = req.getParameterMap();
		// check that we actually have some params
		if (!params.isEmpty()) {
			String[] sibName = params.get("name");
			String SIBname = sibName[0];

			String[] kpname = params.get("kpname");
			String KPname = kpname[0];

			String[] subject = params.get("subject");
			String Subject = subject[0];

			String[] subjectType = params.get("subjecttype");
			String SubjectType = subjectType[0];

			String[] predicate = params.get("predicate");
			String Predicate = predicate[0];

			String[] object = params.get("object");
			String Object = object[0];

			String[] objectType = params.get("objecttype");
			String ObjectType = objectType[0];
			String[] newobject = params.get("newobject");
			String NewObject = newobject[0];

			if ((!SIBname.isEmpty()) && (!KPname.isEmpty()) && (!Subject.isEmpty()) && (!Predicate.isEmpty())
					&& (!Object.isEmpty())) {
				content = "removing [ " + Subject + " , " + Predicate + " , " + Object + " ]";
				runningSIBs = Activator.getDefault().getCurrentSIBs();
				if (!runningSIBs.isEmpty() && runningSIBs.containsKey(SIBname)) {
					// get the SIB
					SIB sib = runningSIBs.get(SIBname);
					if (sib != null) {
						// AsyncSSAPOSGiImpl service;
						if (Activator.getDefault().async == null) {
							Activator.getDefault().async = new AsyncSSAPOSGiImpl();
						}

						Activator.getDefault().async.setSIB(sib);
						Activator.getDefault().transactionId++;
						// public static void executeCommand (String operation, String parameters,
						// AsyncSSAPOSGiImpl sibConnection, String nodeId, String spaceId, Long
						// transactionId, ClientProxy proxy)
						String parameters = null;

						parameters = composeTripleParametes("remove_graph", Subject, SubjectType, Predicate, Object,
								ObjectType);
						parameters = parameters + "\n" + composeTripleParametes("insert_graph", Subject, SubjectType,
								Predicate, NewObject, ObjectType);

						Utils.executeCommand("Update", parameters, Activator.getDefault().async, KPname, SIBname,
								Activator.getDefault().transactionId, Activator.getDefault().proxy);

					}
				}
			} else {
				content = "one of the arguments is missing";
			}
		}
		PrintWriter writer = resp.getWriter();
		writer.print("<HTML>");
		writer.print("<HEAD><TITLE>SMOOL RESTFUL INTERFACE</TITLE></HEAD>");
		writer.print("<BODY>");
		writer.print("<BR>");
		writer.print(
				"<p align=\"center\"><font face=\"verdana\" size=\"5\" color=\"#5E3333\"><b>SmooL RESTful Interface</b></font></p>");
		writer.print("<BR>");
		writer.print(
				"<p align=\"left\"><font face=\"verdana\" size=\"2\" color=\"#5E3333\">" + content + "</font></p>");
		writer.print("<BR>");

		writer.print("</BODY>");
		writer.close();

	}

}
