package org.smool.sdk.osgi.rest.servlet;

//created by Mahmoud Mohamed, mmabdallah@itida.gov.eg - March,22th 2013
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.osgi.service.log.LogService;
import org.smool.sdk.sib.exception.SemanticModelException;
import org.smool.sdk.sib.model.query.SMOOLResultSet;
import org.smool.sdk.sib.ssap.SIB;

import com.hp.hpl.jena.query.QuerySolution;

public class QueryRestServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @param args
	 */
	private HashMap<String, SIB> runningSIBs;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String content = null;
		// http://localhost:8380/query?name=ttt&kpname=rest&subject=individual:_TemperatureSensor4192482265611770049&predicate=rdf:type&object=smoolcore:TemperatureSensor
		// get the param that contains the sib ID that we must get
		Map<String, String[]> params = req.getParameterMap();
		JSONObject jsonQueryResults;
		jsonQueryResults = new JSONObject();
		// check that we actually have some params
		if (!params.isEmpty()) {
			String[] sibName = params.get("name");
			String SIBname = sibName[0];

			String[] kpname = params.get("kpname");
			String KPname = kpname[0];

			String[] subject = params.get("subject");
			String Subject = subject[0];

			String[] predicate = params.get("predicate");
			String Predicate = predicate[0];

			String[] object = params.get("object");
			String Object = object[0];

			if ((!SIBname.isEmpty()) && (!KPname.isEmpty()) && (!Subject.isEmpty()) && (!Predicate.isEmpty())
					&& (!Object.isEmpty())) {
				content = "querying [ " + Subject + " , " + Predicate + " , " + Object + " ]";
				runningSIBs = Activator.getDefault().getCurrentSIBs();
				if (!runningSIBs.isEmpty() && runningSIBs.containsKey(SIBname)) {
					// get the SIB
					SIB sib = runningSIBs.get(SIBname);
					if (sib != null) {

						Iterator<String> it;
						it = getQueryResults(sib, Subject, Predicate, Object).iterator();

						while (it.hasNext()) {
							String current = it.next();
							jsonQueryResults.put(current, "QueryResults");
						}
					}
				}
			} else {
				content = "one of the arguments is missing";
			}
		}
		if (jsonQueryResults.size() != 0) {
			content = content + "<BR>" + jsonQueryResults.toJSONString();
		}
		PrintWriter writer = resp.getWriter();
		writer.print("<HTML>");
		writer.print("<HEAD><TITLE>SMOOL RESTFUL INTERFACE</TITLE></HEAD>");
		writer.print("<BODY>");
		writer.print("<BR>");
		writer.print(
				"<p align=\"center\"><font face=\"verdana\" size=\"5\" color=\"#5E3333\"><b>SmooL RESTful Interface</b></font></p>");
		writer.print("<BR>");
		writer.print(
				"<p align=\"left\"><font face=\"verdana\" size=\"2\" color=\"#5E3333\">" + content + "</font></p>");
		writer.print("<BR>");

		writer.print("</BODY>");
		writer.close();

	}

	// mahmoud 14-3-2013 method to get the leafnodes from SIB by using query
	private List<String> getQueryResults(SIB sib, String Subject, String Predicate, String Object) {
		List<String> returnRes = new ArrayList<String>();
		Collection<QuerySolution> queryResults = null;
		String queryString = new String();
		queryString = queryString + " PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>";
		queryString = queryString + " PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>";
		queryString = queryString + " PREFIX owl: <http://www.w3.org/2002/07/owl#>";
		queryString = queryString + " PREFIX  rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>";
		// queryString=queryString+"PREFIX xmlns:
		// <http://www.smool.org/ontology/individual#>";
		// queryString=queryString+" PREFIX xml:base:
		// <http://www.smool.org/ontology/individual#>";
		queryString = queryString + " PREFIX individual: <http://www.smool.org/ontology/individual#>";
		queryString = queryString + " PREFIX smoolcore: <http://com.tecnalia.smool/core/smoolcore#>";

		queryString = queryString + " SELECT ?s ?p ?o WHERE {" + Subject + "  " + Predicate + "  " + Object + " }";

		if (sib != null) {
			try {// FILTER
					// SMOOLResultSet t=
					// sib.getSemanticModel().query("PREFIX rdfs:
					// <http://www.w3.org/2000/01/rdf-schema#> select distinct ?child ?father where
					// {?child rdfs:subClassOf ?father } ");
					// query all Sensor, PhysicalSensor
				SMOOLResultSet t = sib.getSemanticModel().query(queryString);
				queryResults = t.getQuerySolutions();

			} catch (SemanticModelException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//
			if (queryResults != null) {
				Iterator<QuerySolution> itq = queryResults.iterator();
				Activator.getDefault().getLogger().log(LogService.LOG_INFO,
						"Query result for {" + Subject + "  " + Predicate + "  " + Object + " }");
				while (itq.hasNext()) {
					QuerySolution current = itq.next();
					Activator.getDefault().getLogger().log(LogService.LOG_INFO, current.toString());
					String readable = current.toString();
					readable = readable.replace(">", "");
					if (readable.contains("http://com.tecnalia.smool/core/smoolcore#") == true) {
						readable = readable.replace("<http://com.tecnalia.smool/core/smoolcore#", "smoolcore:");
					} else if (readable.contains("http://www.smool.org/ontology/individual#") == true) {
						readable = readable.replace("<http://www.smool.org/ontology/individual#", "individual:");
					}
					returnRes.add(readable);

				}
			}
		}

		return returnRes;
	}

	// mahmoud
}
