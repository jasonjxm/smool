package org.smool.sdk.osgi.rest.servlet;

import java.util.HashMap;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.log.LogService;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import org.smool.sdk.osgi.rest.servlet.data.Element;
import org.smool.sdk.sib.service.ISIB;
import org.smool.sdk.sib.ssap.SIB;


@SuppressWarnings("rawtypes")
public class SIBServiceTrackerCustomizer implements ServiceTrackerCustomizer {

	private BundleContext bc;
	
	/**
	 * SIB service
	 */
	protected ISIB asib = null;
	
	private static HashMap<String, SIB> currentSIBs=new HashMap<String, SIB>();

	public SIBServiceTrackerCustomizer(BundleContext bc){
		this.bc = bc;
	}
	
	public static HashMap<String, SIB> getcurrentSIBs()
	{
		return currentSIBs;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public Object addingService(ServiceReference reference) {
		
		// Obtains the SIB
		Object obj = bc.getService(reference);

		if (obj instanceof ISIB) {
			asib = (ISIB) bc.getService(reference);		
			String sibName=asib.getName();
			Activator.getDefault().getLogger().log(LogService.LOG_INFO, "Found a new SIB server with name " + sibName);
			currentSIBs.put(sibName, (SIB)asib);
			Activator.getDefault().setCurrentSIBs(currentSIBs);
			org.smool.sdk.osgi.rest.servlet.data.Element sib= new Element(asib);	
			Activator.getDefault().getSibList().add(sib);
			
			
			return asib;
		} 
		else {
			return null;
		}		
		
		//Object obj = bc.getService(reference);
		
		//since there can be several SIBs running, we check if the new service (bundle)
		//registered is a SIB. If so, store it in the hashmap that contains running SIBs
//		if (obj instanceof SIB) {
//			
//			SIB sib=(SIB) bc.getService(reference);
//			String sibName=sib.getName();
//			Activator.getDefault().getLogger().log(LogService.LOG_INFO, "Found a new SIB server with name " + sibName);
//			currentSIBs.put(sibName, sib);
//			Activator.getDefault().setCurrentSIBs(currentSIBs);
//			
//		}
//		return obj;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	/*
	 * (non-Javadoc)
	 * @see org.osgi.util.tracker.ServiceTrackerCustomizer
	 */
	public void modifiedService(ServiceReference reference, Object serviceObject) {
		// Obtains the SIB
		Object obj = bc.getService(reference);
		
		if (obj instanceof ISIB) {
			asib = (ISIB) bc.getService(reference);
			Activator.getDefault().getSibList().remove(serviceObject);
			Element sib= new Element(asib);	
			Activator.getDefault().getSibList().add(sib);

		}

	}

	@SuppressWarnings("unchecked")
	@Override
	/*
	 * (non-Javadoc)
	 * @see org.osgi.util.tracker.ServiceTrackerCustomizer
	 */
	public void removedService(ServiceReference reference, Object service) {
		
		Object obj = bc.getService(reference);
		
		if (obj instanceof ISIB) {
			asib = (ISIB) bc.getService(reference);
			Element sib=null;
			currentSIBs.remove(asib.getName());
			int i=0;
			boolean found=false;
			while(i<Activator.getDefault().getSibList().size() && found==false){
				Element current= Activator.getDefault().getSibList().get(i);
				if (current.getSib()==asib){
					found=true;
					sib=current;
				}
					i++;
			}
			Activator.getDefault().getSibList().remove(sib);

		}
	}


//	@SuppressWarnings("unchecked")
//	@Override
//	public void removedService(ServiceReference reference, Object service) {
//		Object obj = bc.getService(reference);
//		//If a SIB has been stopped, remove it from hashmap
//		if (obj instanceof SIB) {
//			SIB sib=(SIB) bc.getService(reference);
//			currentSIBs.remove(sib.getName());
//		}
//		
//	}

}
