/*******************************************************************************
 * Copyright (c) 2009-2013 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Alejandro Villamarin - alejandro.villamarin@tecnalia.com
 *******************************************************************************/
package org.smool.sdk.osgi.rest.servlet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.service.log.LogReaderService;
import org.osgi.service.log.LogService;
import org.osgi.util.tracker.ServiceTracker;
import org.smool.sdk.gateway.service.IGateway;
import org.smool.sdk.gateway.service.IGatewayFactory;
import org.smool.sdk.osgi.rest.servlet.data.Element;
import org.smool.sdk.osgi.rest.servlet.data.SibData;
import org.smool.sdk.sib.service.ISIB;
import org.smool.sdk.sib.service.ISIBFactory;
import org.smool.sdk.sib.service.IViewer;
import org.smool.sdk.sib.ssap.SIB;
import org.smool.sdk.ssapmessage.proxy.ClientProxy;

public class Activator implements BundleActivator {

	/**
	 * Reference to the bundle context
	 */
	private static BundleContext context;

	/**
	 * Self reference to the Activator, to be used by other classes if needed
	 */
	private static Activator __plugin;

//	/**
//	 * Reference to our implementation of LogListener class, that prints Log messages in our desired format
//	 */
//	private ConsoleLogImpl m_console = new ConsoleLogImpl();

	/**
	 * List with all the available LogReaderServices
	 */
	private LinkedList<LogReaderService> m_readers = new LinkedList<LogReaderService>();

	/**
	 * List with all the available LogServices
	 */
	private LinkedList<LogService> m_services = new LinkedList<LogService>();

	/**
	 * Reference to the LogService that will eventually be used for logging
	 */
	private LogService logger;

	/**
	 * ISIB Factory service tracker customizer
	 */
	private ServerManagerServiceTrackerCustomizer mgtstc;
	/**
	 * Gateway Factory service tracker customizer
	 */
	private GatewayManagerServiceTrackerCustomizer gwmgtstc;

	/**
	 * SIB service tracker customizer
	 */
	private SIBServiceTrackerCustomizer sibstc;

	/**
	 * Gateway service tracker customizer
	 */
	private GatewayServiceTrackerCustomizer gwstc;

	/**
	 * Gateway service tracker customizer
	 */
	private ViewerServiceTrackerCustomizer viewstc;

	/**
	 * A HashMap containing pairs of <sibName, SIB>
	 */
	private HashMap<String, SIB> currentSIBs = new HashMap<String, SIB>();

	/**
	 * List of SIB instances
	 */
	private ArrayList<Element> sibList = new ArrayList<Element>();

	/**
	 * List of SIB Viewers instances
	 */
	private ArrayList<SibData> sibViewerList = new ArrayList<SibData>();

	/**
	 * The ServiceTracker for the HttpServlet
	 */
	@SuppressWarnings("rawtypes")
	private ServiceTracker httpServiceTracker;

	/*
	 * We use a ServiceListener to dynamically keep track of all the
	 * LogReaderService service being registered or unregistered
	 * 
	 */
	private ServiceListener m_servlistener = new ServiceListener() {
		public void serviceChanged(ServiceEvent event) {
			BundleContext bc = event.getServiceReference().getBundle().getBundleContext();
			LogReaderService lrs = (LogReaderService) bc.getService(event.getServiceReference());
			if (lrs != null) {
				if (event.getType() == ServiceEvent.REGISTERED) {
					m_readers.add(lrs);
					// lrs.addLogListener(m_console);
				} else if (event.getType() == ServiceEvent.UNREGISTERING) {
					// lrs.removeLogListener(m_console);
					m_readers.remove(lrs);
				}
			}
		}
	};

	static BundleContext getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void start(BundleContext bundleContext) throws Exception {

		// get the self-reference
		__plugin = this;
		System.out.println("start(): Line = " + 156);
		// get the context
		Activator.context = bundleContext;
		// Get a list of all the registered LogReaderService, and add the console
		// listener
		ServiceTracker logReaderTracker = new ServiceTracker(context,
				org.osgi.service.log.LogReaderService.class.getName(), null);
		logReaderTracker.open();
		Object[] readers = logReaderTracker.getServices();
		if (readers != null) {
			for (int i = 0; i < readers.length; i++) {
				LogReaderService lrs = (LogReaderService) readers[i];
				m_readers.add(lrs);
				// specify what LogListener will spit log messages
				// ******************************************************************************************************************************************************
				// NOTE: NO NEED TO ADD OUR CONSOLE TO THE LOGREADERSERVICE, OTHERWISE WE WILL
				// BE LOGGING ALL EVENTS TWICE OR AS MANY TIMES AS CONSOLES
				// WE HAVE AND ADD THEM TO THE LOGREADERSERVICE, THUS, THIS MEANS THAT ONLY ONE
				// OF THE BUNDLES MUST HAVE THE CONSOLE, MOST SURELY THE SIB
				// OSGI BUNDLE. THIS MEANS WE'LL HAVE TO REDESING THE WHOLE LOGGING SYSTEM USED
				// IN ALL THE BUNDLES TO USE THIS APPROACH INSTEAD
				// ******************************************************************************************************************************************************
				// lrs.addLogListener(m_console);
			}
		}
		// close the tracker
		logReaderTracker.close();
		System.out.println("start(): Line = " + 180);
		// Add the ServiceListener, but with a filter so that we only receive events
		// related to LogReaderService
		String filter = "(objectclass=" + LogReaderService.class.getName() + ")";
		try {
			context.addServiceListener(m_servlistener, filter);
		} catch (InvalidSyntaxException e) {
			System.out.println("ERROR: Could not add service Listeners for LogReaderService.");
		}
		System.out.println("start(): Line = " + 189);
		// Get a list of all the registered LogService, and add the console listener
		ServiceTracker logServiceTracker = new ServiceTracker(context, org.osgi.service.log.LogService.class.getName(),
				null);
		logServiceTracker.open();
		Object[] services = logServiceTracker.getServices();
		if (services != null) {
			for (int i = 0; i < services.length; i++) {
				LogService ls = (LogService) services[i];
				String className = ls.getClass().getSimpleName();
				if (ls instanceof LogService && className.contains("LogServiceImpl")) {
					m_services.add(ls);
					logger = (LogService) ls;
					logger.log(LogService.LOG_INFO, "Starting the org.smool.sdk.osgi.rest.servlet...");
					break;
				}

			}
		}

		logServiceTracker.close();
		System.out.println("start(): Line = " + 211);
		// Obtains the SIB manager service
		mgtstc = new ServerManagerServiceTrackerCustomizer(context);
		ServiceTracker serverTracker = new ServiceTracker(context, ISIBFactory.class.getName(), mgtstc);
		serverTracker.open();

		// Obtains the Generic Gateway manager service
		gwmgtstc = new GatewayManagerServiceTrackerCustomizer(context);
		ServiceTracker tcpipGwTracker = new ServiceTracker(context, IGatewayFactory.class.getName(), gwmgtstc);
		tcpipGwTracker.open();

		// Obtains the registered SIB instances
		sibstc = new SIBServiceTrackerCustomizer(context);
		ServiceTracker sibTracker = new ServiceTracker(context, ISIB.class.getName(), sibstc);
		sibTracker.open();

		// Obtains the registered Gateway instances
		gwstc = new GatewayServiceTrackerCustomizer(context);
		ServiceTracker gwTracker = new ServiceTracker(context, IGateway.class.getName(), gwstc);
		gwTracker.open();

		// Obtains the registered SibViewer instances
		viewstc = new ViewerServiceTrackerCustomizer(context);
		ServiceTracker viewTracker = new ServiceTracker(context, IViewer.class.getName(), viewstc);
		viewTracker.open();

		// Create and open the HttpServiceTracker
		httpServiceTracker = new HttpServiceTracker(context);
		httpServiceTracker.open();
		// Mahmoud 21-3-2013
		// intialize the transactionId variable used in AsyncSSAPOSGiImpl
		transactionId = new Long(0);
		proxy = new ClientProxy();
		// Mahmoud 21-3-2013
	}

	// Mahmoud 21-3-2013
	/**
	 * Internal variable used to count the number of operations made in the server
	 */
	long transactionId;
	/**
	 * Contains the asynchronous connection to the server
	 */
	AsyncSSAPOSGiImpl async = null;
	/**
	 * Proxy used to connect to the server
	 */
	ClientProxy proxy;

	// Mahmoud 21-3-2013
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception {

		for (Iterator<LogReaderService> i = m_readers.iterator(); i.hasNext();) {
			// LogReaderService lrs = i.next();
			// lrs.removeLogListener(m_console);
			i.remove();
		}

		httpServiceTracker.close();
		httpServiceTracker = null;

		Activator.context = null;
		__plugin = null;
	}

	public static Activator getDefault() {
		return __plugin;
	}

	public LogService getLogger() {
		return logger;
	}

	/**
	 * @return the currentSIBs
	 */
	public HashMap<String, SIB> getCurrentSIBs() {
		return currentSIBs;
	}

	/**
	 * @param currentSIBs the currentSIBs to set
	 */
	public void setCurrentSIBs(HashMap<String, SIB> currentSIBs) {
		this.currentSIBs = currentSIBs;
	}

	/**
	 * @return the sibList
	 */
	public ArrayList<Element> getSibList() {
		return sibList;
	}

	/**
	 * @param sibList the sibList to set
	 */
	public void setSibList(ArrayList<Element> sibList) {
		this.sibList = sibList;
	}

	/**
	 * @return the sibViewerList
	 */
	public ArrayList<SibData> getSibViewerList() {
		return sibViewerList;
	}

	/**
	 * @return the viewstc
	 */
	public ViewerServiceTrackerCustomizer getViewstc() {
		return viewstc;
	}

	/**
	 * @param sibViewerList the sibViewerList to set
	 */
	public void setSibViewerList(ArrayList<SibData> sibViewerList) {
		this.sibViewerList = sibViewerList;
	}

}