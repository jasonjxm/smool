/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Cristina López, cristina.lopez@tecnalia.com (Tecnalia-ESI) - initial API, implementation and documentation
 *******************************************************************************/ 


package org.smool.sdk.osgi.rest.servlet.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.smool.sdk.sib.data.ISemanticContentHolderListener;
import org.smool.sdk.sib.data.Triple;


/**
 * This class is used to visualize the Triples information of one particular Sib server.
 * The class is also an implementation of a listener. 
 * 
 * @author Cristina López, cristina.lopez@tecnalia.com, Tecnalia
 *
 */

public class TripleData implements ISemanticContentHolderListener{

	/**
	 * List of sessions that are displayed on the tab
	 */
	private ArrayList<Triple> listTriples;
	
	public TripleData() {
		this.listTriples= new ArrayList<Triple>();
	}

	/**
	 * @return the listTriples
	 */
	public ArrayList<Triple> getListTriples() {
		return listTriples;
	}
	
	/**
	 * Method used to check if to Triple object are equal
	 * @param triple1
	 * @param triple2
	 * @return
	 */
	public static boolean equals(Triple triple1, Triple triple2) {
		Boolean _return=false;
		if ((triple1.getObject().equals(triple2.getObject()))&&(triple1.getSubject().equals(triple2.getSubject()))&&(triple1.getPredicate().equals(triple2.getPredicate())))
			_return=true;
		return _return;
	}

	/**
	/** Listener methods
	**/
	@Override
	public void initTriples(Collection<Triple> triples, String nodeId) {
		if (triples != null) {
			for (Triple triple : triples) {
				this.listTriples.add(triple);
				triple.setOwner(nodeId);
			}
		}
	}

	@Override
	public void addTriples(Collection<Triple> triples, String nodeId) {
		for (Triple triple : triples) {
			this.listTriples.add(triple);	
			triple.setOwner(nodeId);
		}
	}

	@Override
	public void removeTriples(Collection<Triple> triples, String nodeId) {
		if ((triples != null)&&(triples.size()>0)) {
			Object[] paramList= triples.toArray();
			List<Triple> deleteList= new ArrayList<Triple>();
			for (int j=0; j<paramList.length;j++){
				int i=0;
				Triple removeTriple = (Triple) paramList[j];
				removeTriple.setOwner(nodeId);
				boolean found=false;			
				while( i<this.listTriples.size() && found==false) {
					Triple current=(Triple)(this.listTriples.get(i));
					if (equals(current,removeTriple)){
						found=true;
						deleteList.add(current);
					}
					i++;
				}
			}			
			this.listTriples.removeAll(deleteList);
		}
	}

	@Override
	public void reset() {
		this.listTriples.clear();
		
	}

}
