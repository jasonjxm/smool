package org.smool.sdk.osgi.rest.servlet;

//created by Mahmoud Mohamed, mmabdallah@itida.gov.eg - March,19th 2013
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.smool.sdk.sib.ssap.SIB;

public class ConnectToSIB extends HttpServlet {

	/**
	 *                  
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @param args
	 */
	private HashMap<String, SIB> runningSIBs;
	// from MainWindow.jav of /org.smool.sdk.ui.sibviewer.test project

	/**
	 * Method called internally to join the server
	 */

	private void joinServer(String KPName, String spaceIdentifier) {
		Activator.getDefault().transactionId++;
		Activator.getDefault().async.join(KPName, spaceIdentifier, Activator.getDefault().transactionId, null,
				Activator.getDefault().proxy);
	}

	// from MainWindow.jav of /org.smool.sdk.ui.sibviewer.test project
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		// proxy.addSIBMessageListener(this);
		String content = null;
		// http://localhost:8380/connect?name=ttt&kpname=rest
		// get the param that contains the sib ID that we must get
		Map<String, String[]> params = req.getParameterMap();
		// check that we actually have some params
		if (!params.isEmpty()) {
			String[] sibName = params.get("name");
			String SIBname = sibName[0];

			String[] kpname = params.get("kpname");
			String KPname = kpname[0];

			if ((!SIBname.isEmpty()) && (!KPname.isEmpty())) {

				runningSIBs = Activator.getDefault().getCurrentSIBs();
				if (!runningSIBs.isEmpty() && runningSIBs.containsKey(SIBname)) {
					// get the SIB
					SIB sib = runningSIBs.get(SIBname);
					if (sib != null) {

						// AsyncSSAPOSGiImpl service;
						if (Activator.getDefault().async == null) {
							Activator.getDefault().async = new AsyncSSAPOSGiImpl();
						}

						Activator.getDefault().async.setSIB(sib);
						joinServer(KPname, SIBname);// Integer.toString(sib.getId()));

						content = "connecting KP " + KPname + " to SIB" + SIBname;
						PrintWriter writer = resp.getWriter();
						writer.print("<HTML>");
						writer.print("<HEAD><TITLE>SMOOL RESTFUL INTERFACE</TITLE></HEAD>");
						writer.print("<BODY>");
						writer.print("<BR>");
						writer.print(
								"<p align=\"center\"><font face=\"verdana\" size=\"5\" color=\"#5E3333\"><b>SmooL RESTful Interface</b></font></p>");
						writer.print("<BR>");
						writer.print("<p align=\"left\"><font face=\"verdana\" size=\"2\" color=\"#5E3333\">" + content
								+ "</font></p>");
						writer.print("<BR>");

						writer.print("</BODY>");
						writer.close();

					}
				}
			}

		}
	}
}
