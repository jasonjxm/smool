package org.smool.sdk.osgi.rest.servlet;

//Modified by Mahmoud Mohamed, mmabdallah@itida.gov.eg - March,13rd 2013
//Modified by Mahmoud Mohamed, mmabdallah@itida.gov.eg - March,26th 2013
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.osgi.service.log.LogService;
import org.smool.sdk.osgi.rest.servlet.data.Element;
import org.smool.sdk.osgi.rest.servlet.data.SibData;
import org.smool.sdk.sib.data.Session;
import org.smool.sdk.sib.data.Subscription;
import org.smool.sdk.sib.data.Triple;
import org.smool.sdk.sib.exception.SIBException;
import org.smool.sdk.sib.exception.SemanticModelException;
import org.smool.sdk.sib.model.query.SMOOLResultSet;
import org.smool.sdk.sib.service.ISIB;
import org.smool.sdk.sib.ssap.SIB;

import com.hp.hpl.jena.query.QuerySolution;

public class RestServlet extends HttpServlet {

	/**
	 * Just for serializing purposes
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * A HashMap containing the current SIBs
	 */
	private HashMap<String, SIB> runningSIBs;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		boolean empty = false;
		// Get the request URI and construct the response depending on that
		String requestURI = req.getRequestURI();
		// Mahmoud 26-3-2013 add new String(); because json strings starts with
		// null other wise
		String response = new String();
		// check incoming alias, so we know how to build the response

		if (requestURI.contains("/sib_overview") && (requestURI.contains("/sib_overview.json") == false)) {
			// Get the running sibs from the Activator
			runningSIBs = Activator.getDefault().getCurrentSIBs();
			response = "<table border=\"1\" bordercolor=\"#5E3333\" style=\"background-color:#DECEBF\" width=\"100%\" cellpadding=\"1\" cellspacing=\"1\">"
					+ "<tr><td><p align=\"center\"><font face=\"verdana\" color=\"#DB2C2C\"><b>SIB</b></font></p></td>"
					+ "<td><p align=\"center\"align=\"center\"><font face=\"verdana\" color=\"#DB2C2C\"><b>Name</b></font></p></td></tr>";
			if (runningSIBs != null && !runningSIBs.isEmpty()) {
				// iterate over the SIBs
				for (Map.Entry<String, SIB> entry : runningSIBs.entrySet()) {
					SIB value = entry.getValue();
					response = response + "<tr><td><p align=\"center\"><font face=\"verdana\" color=\"#5E3333\">"
							+ value.getId() + "</font></p></td>"
							+ "<td><a href=\"http://localhost:8380/show_individual?name=" + value.getName() + "\">"
							+ value.getName() + "</a></td></tr>";
				}
			}

			response = response + "</table>";

			// get the writer to print
			PrintWriter writer = resp.getWriter();
			writer.print("<HTML>");
			writer.print("<HEAD><TITLE>SMOOL RESTFUL INTERFACE</TITLE></HEAD>");
			writer.print("<BODY>");
			writer.print("<BR>");
			writer.print(
					"<p align=\"center\"><font face=\"verdana\" size=\"5\" color=\"#5E3333\"><b>SmooL RESTful Interface</b></font></p>");
			writer.print("<BR>");
			writer.print(
					"<p align=\"left\"><font face=\"verdana\" size=\"2\" color=\"#5E3333\">Click in the SIB name to view its data</font></p>");
			writer.print("<BR>");
			writer.print(response);
			writer.print("</BODY>");
			writer.close();
		} else if (requestURI.contains("/sib_overview.json")) {
			// Mahmoud 26-3-2013 add json responce
			// Get the running sibs from the Activator
			runningSIBs = Activator.getDefault().getCurrentSIBs();

			if (runningSIBs != null && !runningSIBs.isEmpty()) {
				response = new String();
				// iterate over the SIBs
				for (Map.Entry<String, SIB> entry : runningSIBs.entrySet()) {
					SIB value = entry.getValue();
					JSONObject onesib = new JSONObject();
					onesib.put("SIBId", value.getId());
					onesib.put("SIBName", value.getName());
					response = response + onesib.toJSONString();
				}
			} else {
				response = "{}";
			}
			// get the writer to print
			PrintWriter writer = resp.getWriter();
			writer.print(response);
			writer.close();

		} else if (requestURI.contains("/show_individual") && (requestURI.contains("/show_individual.json") == false)) {
			// get the param that contains the sib ID that we must get
			Map<String, String[]> params = req.getParameterMap();
			// check that we actually have some params
			if (!params.isEmpty()) {
				String[] sibName = params.get("name");
				String name = sibName[0];
				if (!name.isEmpty()) {
					runningSIBs = Activator.getDefault().getCurrentSIBs();
					if (!runningSIBs.isEmpty() && runningSIBs.containsKey(name)) {
						// get the SIB
						SIB sib = runningSIBs.get(name);
						if (sib != null) {
							// Query the semantic model
							Collection<Triple> triples = sib.getSemanticModel().getAllTriples();
							Iterator<Triple> it = triples.iterator();
							response = "<table border=\"1\" bordercolor=\"#5E3333\" style=\"background-color:#DECEBF\" width=\"100%\" cellpadding=\"1\" cellspacing=\"1\">"
									+ "<tr><td><p align=\"center\"><font face=\"verdana\" color=\"#DB2C2C\"><b>Owner</b></font></p></td>"
									+ "<td><p align=\"center\"align=\"center\"><font face=\"verdana\" color=\"#DB2C2C\"><b>Date</b></font></p></td>"
									+ "<td><p align=\"center\"align=\"center\"><font face=\"verdana\" color=\"#DB2C2C\"><b>Subject</b></font></p></td>"
									+ "<td><p align=\"center\"align=\"center\"><font face=\"verdana\" color=\"#DB2C2C\"><b>Predicate</b></font></p></td>"
									+ "<td><p align=\"center\"align=\"center\"><font face=\"verdana\" color=\"#DB2C2C\"><b>Object</b></font></p></td></tr>";

							while (it.hasNext()) {
								Triple current = it.next();
								Activator.getDefault().getLogger().log(LogService.LOG_INFO, current.toString());
								response = response
										+ "<tr><td><p align=\"center\"><font face=\"verdana\" color=\"#5E3333\">"
										+ current.getOwner() + "</font></p></td>"
										+ "<td><p align=\"center\"><font face=\"verdana\" color=\"#5E3333\">"
										+ current.getDate() + "</font></p></td>"
										+ "<td><p align=\"center\"><font face=\"verdana\" color=\"#5E3333\">"
										+ current.getSubject() + "</font></p></td>"
										+ "<td><p align=\"center\"><font face=\"verdana\" color=\"#5E3333\">"
										+ current.getPredicate() + "</font></p></td>"
										+ "<td><p align=\"center\"><font face=\"verdana\" color=\"#5E3333\">"
										+ current.getObject() + "</font></p></td></tr>";

							}
						}

						response = response + "</table>";

						// get the writer to print
						PrintWriter writer = resp.getWriter();
						writer.print("<HTML>");
						writer.print("<HEAD><TITLE>SMOOL RESTFUL INTERFACE</TITLE></HEAD>");
						writer.print("<BODY>");
						writer.print("<BR>");
						writer.print(
								"<p align=\"center\"><font face=\"verdana\" size=\"5\" color=\"#5E3333\"><b>Individual SIB semantic model</b></font></p>");
						writer.print("<BR>");
						writer.print(
								"<p align=\"left\"><font face=\"verdana\" size=\"2\" color=\"#5E3333\">Here comes the list of triples that represent the semantic model of the selected SIB. If you'd like to inspect the existing indviduals <a href=\"http://localhost:8380/show_contents?name="
										+ name + "\">click here!</a></font></p>");
						writer.print("<BR>");
						writer.print(
								"<p align=\"left\"><font face=\"verdana\" size=\"2\" color=\"#5E3333\">Current clients (or sessions) connected to this SIB can be seen <a href=\"http://localhost:8380/show_sessions?name="
										+ name + "\">here</a></font></p>");
						writer.print("<BR>");
						writer.print(
								"<p align=\"left\"><font face=\"verdana\" size=\"2\" color=\"#5E3333\">Click <a href=\"http://localhost:8380/show_subscriptions?name="
										+ name
										+ "\">here</a> to get a list of current subscribers in the selected SIB</font></p>");
						writer.print("<BR>");
						writer.print(response);
						writer.print("</BODY>");
						writer.close();

					} else {
						Activator.getDefault().getLogger().log(LogService.LOG_INFO,
								"A SIB with the provided Name could not be found. Aborting.");
						empty = true;
					}
				} else {
					Activator.getDefault().getLogger().log(LogService.LOG_INFO,
							"SIB name provided seems to be empty. Aborting.");
					empty = true;
				}
			} else {
				Activator.getDefault().getLogger().log(LogService.LOG_INFO,
						"Could not find a GET param with named \"name\" in HttpRequest. Aborting.");
				empty = true;
			}

			// check empty flag to contrusct an empty response
			if (empty) {
				PrintWriter writer = resp.getWriter();
				writer.print("<HTML>");
				writer.print("<HEAD><TITLE>SMOOL RESTFUL INTERFACE</TITLE></HEAD>");
				writer.print("<BODY>");
				writer.print("<BR>");
				writer.print(
						"<p align=\"center\"><font face=\"verdana\" size=\"5\" color=\"#5E3333\"><b>Individual SIB semantic model</b></font></p>");
				writer.print("<BR>");
				writer.print(
						"<p align=\"left\"><font face=\"verdana\" size=\"2\" color=\"#5E3333\">Selected SIB seems to be empty or does not exist...we're as puzzled as you are!</font></p>");
				writer.print("<BR>");
				writer.print(response);
				writer.print("</BODY>");
				writer.close();
			}
		} else if (requestURI.contains("/show_individual.json")) {
			// Mahmoud 26-3-2013 add json responce
			// get the param that contains the sib ID that we must get
			Map<String, String[]> params = req.getParameterMap();
			// check that we actually have some params
			if (!params.isEmpty()) {
				String[] sibName = params.get("name");
				String name = sibName[0];
				if (!name.isEmpty()) {
					runningSIBs = Activator.getDefault().getCurrentSIBs();
					if (!runningSIBs.isEmpty() && runningSIBs.containsKey(name)) {
						// get the SIB
						SIB sib = runningSIBs.get(name);
						if (sib != null) {
							// Query the semantic model
							Collection<Triple> triples = sib.getSemanticModel().getAllTriples();
							Iterator<Triple> it = triples.iterator();
							response = new String();
							while (it.hasNext()) {
								Triple current = it.next();
								JSONObject oneindividual = new JSONObject();
								oneindividual.put("Owner", current.getOwner());
								oneindividual.put("Date", current.getDate());
								oneindividual.put("Subject", current.getSubject());
								oneindividual.put("Predicate", current.getPredicate());
								oneindividual.put("Object", current.getObject());
								response = response + oneindividual.toJSONString();
							}
						}

						// get the writer to print
						PrintWriter writer = resp.getWriter();
						writer.print(response);

						writer.close();

					} else {
						Activator.getDefault().getLogger().log(LogService.LOG_INFO,
								"A SIB with the provided Name could not be found. Aborting.");
						empty = true;
					}
				} else {
					Activator.getDefault().getLogger().log(LogService.LOG_INFO,
							"SIB name provided seems to be empty. Aborting.");
					empty = true;
				}
			} else {
				Activator.getDefault().getLogger().log(LogService.LOG_INFO,
						"Could not find a GET param with named \"name\" in HttpRequest. Aborting.");
				empty = true;
			}

			// check empty flag to contrusct an empty response
			if (empty) {
				PrintWriter writer = resp.getWriter();
				writer.print("{}");
				writer.close();
			}
		} else if (requestURI.contains("/show_contents")) {
			// get the param that contains the sib ID that we must get
			Map<String, String[]> params = req.getParameterMap();
			// check that we actually have some params
			if (!params.isEmpty()) {
				String[] sibName = params.get("name");
				String name = sibName[0];
				if (!name.isEmpty()) {
					runningSIBs = Activator.getDefault().getCurrentSIBs();
					if (!runningSIBs.isEmpty() && runningSIBs.containsKey(name)) {
						// get the SIB
						SIB sib = runningSIBs.get(name);
						if (sib != null) {
							// Mahmoud 26-3-2013
							if (requestURI.contains("/show_contents.json") == false) {
								response = "<table border=\"1\" bordercolor=\"#5E3333\" style=\"background-color:#DECEBF\" width=\"100%\" cellpadding=\"1\" cellspacing=\"1\">"
										+ "<tr><td><p align=\"center\"><font face=\"verdana\" color=\"#DB2C2C\"><b>Owner</b></font></p></td>"
										+ "<td><p align=\"center\"align=\"center\"><font face=\"verdana\" color=\"#DB2C2C\"><b>Date</b></font></p></td>"
										+ "<td><p align=\"center\"align=\"center\"><font face=\"verdana\" color=\"#DB2C2C\"><b>Subject</b></font></p></td>"
										+ "<td><p align=\"center\"align=\"center\"><font face=\"verdana\" color=\"#DB2C2C\"><b>Predicate</b></font></p></td>"
										+ "<td><p align=\"center\"align=\"center\"><font face=\"verdana\" color=\"#DB2C2C\"><b>Object</b></font></p></td></tr>";

							}

							// Query the semantic model for SIB data
							SibData sibData = getSIBDatabyID(sib.getId());
							// If selected SIB has some data
							if (sibData != null) {
								// Get the individuals from the selected SIB
								List<Triple> triples = sibData.getTriples().getListTriples();
								triples = locateIndividuals(triples);

								// check not empty results
								if (triples != null && !triples.isEmpty()) {

									Iterator<Triple> it = triples.iterator();
									// iterate each Triple
									while (it.hasNext()) {
										Triple current = it.next();
										Activator.getDefault().getLogger().log(LogService.LOG_INFO, current.toString());
										// Mahmoud 26-3-2013
										if (requestURI.contains("/show_contents.json") == false) {
											response = response
													+ "<tr><td><p align=\"center\"><font face=\"verdana\" color=\"#5E3333\">"
													+ current.getOwner() + "</font></p></td>"
													+ "<td><p align=\"center\"><font face=\"verdana\" color=\"#5E3333\">"
													+ current.getDate() + "</font></p></td>"
													+ "<td><p align=\"center\"><font face=\"verdana\" color=\"#5E3333\">"
													+ current.getSubject() + "</font></p></td>"
													+ "<td><p align=\"center\"><font face=\"verdana\" color=\"#5E3333\">"
													+ current.getPredicate() + "</font></p></td>"
													+ "<td><p align=\"center\"><font face=\"verdana\" color=\"#5E3333\">"
													+ current.getObject() + "</font></p></td></tr>";
										} else {
											JSONObject oneindividual = new JSONObject();
											oneindividual.put("Owner", current.getOwner());
											oneindividual.put("Date", current.getDate());
											oneindividual.put("Subject", current.getSubject());
											oneindividual.put("Predicate", current.getPredicate());
											oneindividual.put("Object", current.getObject());
											response = response + oneindividual.toJSONString();
										}

									}

									// get the writer to print
									PrintWriter writer = resp.getWriter();
									// Mahmoud 26-3-2013
									if (requestURI.contains("/show_contents.json") == false) {
										writer.print("<HTML>");
										writer.print("<HEAD><TITLE>SMOOL RESTFUL INTERFACE</TITLE></HEAD>");
										writer.print("<BODY>");
										writer.print("<BR>");
										writer.print(
												"<p align=\"center\"><font face=\"verdana\" size=\"5\" color=\"#5E3333\"><b>Individual SIB ontology individuals</b></font></p>");
										writer.print("<BR>");
										writer.print(
												"<p align=\"left\"><font face=\"verdana\" size=\"2\" color=\"#5E3333\">This is a list containing all the individuals in the selected SIB</font></p>");
										writer.print("<BR>");
										writer.print(response);
										writer.print("</BODY>");
									} else {
										writer.print(response);
									}

									writer.close();
								} else {
									Activator.getDefault().getLogger().log(LogService.LOG_INFO,
											"Selected SIB seems to have no Individuals.");
									empty = true;
								}
							}

						}
						// end the table
						if (requestURI.contains("/show_contents.json") == false) {
							response = response + "</table>";
						}

					} else {
						Activator.getDefault().getLogger().log(LogService.LOG_INFO,
								"A SIB with the provided Name could not be found. Aborting.");
						empty = true;
					}
				} else {
					Activator.getDefault().getLogger().log(LogService.LOG_INFO,
							"SIB name provided seems to be empty. Aborting.");
					empty = true;
				}
			} else {
				Activator.getDefault().getLogger().log(LogService.LOG_INFO,
						"Could not find a GET param with named \"name\" in HttpRequest. Aborting.");
				empty = true;
			}

			// check empty flag to contrusct an empty response
			if (empty) {
				PrintWriter writer = resp.getWriter();
				// Mahmoud 26-3-2013
				if (requestURI.contains("/show_contents.json") == false) {
					writer.print("<HTML>");
					writer.print("<HEAD><TITLE>SMOOL RESTFUL INTERFACE</TITLE></HEAD>");
					writer.print("<BODY>");
					writer.print("<BR>");
					writer.print(
							"<p align=\"center\"><font face=\"verdana\" size=\"5\" color=\"#5E3333\"><b>Individual SIB ontology individuals</b></font></p>");
					writer.print("<BR>");
					writer.print(
							"<p align=\"left\"><font face=\"verdana\" size=\"2\" color=\"#5E3333\">There are no individuals in the selected SIB. There seems to be no KPs producing data.</font></p>");
					writer.print("<BR>");
					writer.print(response);
					writer.print("</BODY>");
				} else {
					writer.print("{}");
				}

				writer.close();
			}
		}

		// ARF 20-03-19: service for disconnecting KPs
		else if (requestURI.contains("/security")) {
			security(req, resp);

		}

		// Create the HttpResponse to show selected SIB sessions
		else if (requestURI.contains("/show_sessions")) {
			// get the param that contains the sib ID that we must get
			Map<String, String[]> params = req.getParameterMap();
			// check that we actually have some params
			if (!params.isEmpty()) {
				String[] sibName = params.get("name");
				String name = sibName[0];
				if (!name.isEmpty()) {
					runningSIBs = Activator.getDefault().getCurrentSIBs();
					if (!runningSIBs.isEmpty() && runningSIBs.containsKey(name)) {
						// get the SIB
						SIB sib = runningSIBs.get(name);
						if (sib != null) {
							// Mahmoud 26-3-2013
							if (requestURI.contains("/show_sessions.json") == false) {
								response = "<table border=\"1\" bordercolor=\"#5E3333\" style=\"background-color:#DECEBF\" width=\"100%\" cellpadding=\"1\" cellspacing=\"1\">"
										+ "<tr><td><p align=\"center\"><font face=\"verdana\" color=\"#DB2C2C\"><b>KP Name</b></font></p></td>"
										+ "<td><p align=\"center\"align=\"center\"><font face=\"verdana\" color=\"#DB2C2C\"><b>Connected since</b></font></p></td></tr>";
							}

							// Query the semantic model for SIB data
							SibData sibData = getSIBDatabyID(sib.getId());
							// If selected SIB has some data
							if (sibData != null) {
								// Get the sessions from the selected SIB
								List<Session> sessions = sibData.getSessions().getListSessions();

								// check not empty results
								if (sessions != null && !sessions.isEmpty()) {

									Iterator<Session> it = sessions.iterator();
									// iterate each Session
									while (it.hasNext()) {
										Session current = it.next();
										String kpName = new String();
										String connectionTime = new String();
										kpName = current.getNodeId();
										connectionTime = current.getDate();
										// check for empty values
										if (kpName.isEmpty())
											kpName = "Undefined";
										if (connectionTime.isEmpty())
											connectionTime = "Unknown";
										// Mahmoud 26-3-2013
										if (requestURI.contains("/show_sessions.json") == false) {
											response = response
													+ "<tr><td><p align=\"center\"><font face=\"verdana\" color=\"#5E3333\">"
													+ kpName + "</font></p></td>"
													+ "<td><p align=\"center\"><font face=\"verdana\" color=\"#5E3333\">"
													+ connectionTime + "</font></p></td></tr>";
										} else {
											JSONObject onesession = new JSONObject();
											onesession.put("KPName", kpName);
											onesession.put("ConnectionTime", connectionTime);
											response = response + onesession.toJSONString();
										}

									}

									// get the writer to print
									PrintWriter writer = resp.getWriter();
									// Mahmoud 26-3-2013
									if (requestURI.contains("/show_sessions.json") == false) {
										writer.print("<HTML>");
										writer.print("<HEAD><TITLE>SMOOL RESTFUL INTERFACE</TITLE></HEAD>");
										writer.print("<BODY>");
										writer.print("<BR>");
										writer.print(
												"<p align=\"center\"><font face=\"verdana\" size=\"5\" color=\"#5E3333\"><b>Individual SIB connected KPs</b></font></p>");
										writer.print("<BR>");
										writer.print(
												"<p align=\"left\"><font face=\"verdana\" size=\"2\" color=\"#5E3333\">This is a list containing all the clients connected to the selected SIB</font></p>");
										writer.print("<BR>");
										writer.print(response);
										writer.print("</BODY>");
									} else {
										writer.print(response);
									}

									writer.close();
								} else {
									Activator.getDefault().getLogger().log(LogService.LOG_INFO,
											"Seems that no clients are connected to the selected SIB at the moment.");
									empty = true;
								}
							}

						}
						// end the table
						response = response + "</table>";
					} else {
						Activator.getDefault().getLogger().log(LogService.LOG_INFO,
								"A SIB with the provided Name could not be found. Aborting.");
						empty = true;
					}
				} else {
					Activator.getDefault().getLogger().log(LogService.LOG_INFO,
							"SIB name provided seems to be empty. Aborting.");
					empty = true;
				}
			} else {
				Activator.getDefault().getLogger().log(LogService.LOG_INFO,
						"Could not find a GET param with named \"name\" in HttpRequest. Aborting.");
				empty = true;
			}

			// check empty flag to contrusct an empty response
			if (empty) {
				PrintWriter writer = resp.getWriter();
				// Mahmoud 26-3-2013
				if (requestURI.contains("/show_sessions.json") == false) {
					writer.print("<HTML>");
					writer.print("<HEAD><TITLE>SMOOL RESTFUL INTERFACE</TITLE></HEAD>");
					writer.print("<BODY>");
					writer.print("<BR>");
					writer.print(
							"<p align=\"center\"><font face=\"verdana\" size=\"5\" color=\"#5E3333\"><b>Individual SIB connected KPs</b></font></p>");
					writer.print("<BR>");
					writer.print(
							"<p align=\"left\"><font face=\"verdana\" size=\"2\" color=\"#5E3333\">Looks like there are no clients connected to the selected SIB, go back and select another one or wait for KPs to connect.</font></p>");
					writer.print("<BR>");
					writer.print(response);
					writer.print("</BODY>");
				} else {
					writer.print("{}");
				}

				writer.close();
			}
		}
		// Create the HttpResponse to show selected SIB sessions
		else if (requestURI.contains("/show_subscriptions")) {
			// get the param that contains the sib ID that we must get
			Map<String, String[]> params = req.getParameterMap();
			// check that we actually have some params
			if (!params.isEmpty()) {
				String[] sibName = params.get("name");
				String name = sibName[0];
				if (!name.isEmpty()) {
					runningSIBs = Activator.getDefault().getCurrentSIBs();
					if (!runningSIBs.isEmpty() && runningSIBs.containsKey(name)) {
						// get the SIB
						SIB sib = runningSIBs.get(name);
						if (sib != null) {
							// Mahmoud 26-3-2013
							if (requestURI.contains("/show_subscriptions.json") == false) {
								response = "<table border=\"1\" bordercolor=\"#5E3333\" style=\"background-color:#DECEBF\" width=\"100%\" cellpadding=\"1\" cellspacing=\"1\">"
										+ "<tr><td><p align=\"center\"><font face=\"verdana\" color=\"#DB2C2C\"><b>Subscription Owner</b></font></p></td>"
										+ "<td><p align=\"center\"><font face=\"verdana\" color=\"#DB2C2C\"><b>Creation Time</b></font></p></td>"
										+ "<td><p align=\"center\"><font face=\"verdana\" color=\"#DB2C2C\"><b>Subscription Query</b></font></p></td>"
										+ "<td><p align=\"center\"align=\"center\"><font face=\"verdana\" color=\"#DB2C2C\"><b>Subscription Result</b></font></p></td></tr>";
							}

							// Query the semantic model for SIB data
							SibData sibData = getSIBDatabyID(sib.getId());
							// If selected SIB has some data
							if (sibData != null) {
								// Get the sessions from the selected SIB
								List<Subscription> subscriptions = sibData.getSubscriptions().getListSubscriptions();

								// check not empty results
								if (subscriptions != null && !subscriptions.isEmpty()) {

									Iterator<Subscription> it = subscriptions.iterator();
									// iterate each Subscription
									while (it.hasNext()) {
										Subscription current = it.next();
										String kpName = new String();
										String creationTime = new String();
										String subscriptionQuery = new String();
										String subscriptionResult = new String();
										kpName = current.getNodeId();
										creationTime = current.getDate();
										subscriptionQuery = current.getQuery();
										subscriptionResult = current.getResult();
										// check for empty values
										if (kpName.isEmpty())
											kpName = "Undefined";
										if (creationTime.isEmpty())
											creationTime = "Unknown";
										if (subscriptionQuery.isEmpty())
											subscriptionQuery = "Undefined";
										if (subscriptionResult.isEmpty())
											subscriptionResult = "Undefined";

										Activator.getDefault().getLogger().log(LogService.LOG_INFO, current.toString());
										// Mahmoud 26-3-2013
										if (requestURI.contains("/show_subscriptions.json") == false) {
											response = response
													+ "<tr><td><p align=\"center\"><font face=\"verdana\" color=\"#5E3333\">"
													+ kpName + "</font></p></td>"
													+ "<td><p align=\"center\"><font face=\"verdana\" color=\"#5E3333\">"
													+ creationTime + "</font></p></td>"
													+ "<td><p align=\"center\"><font face=\"verdana\" color=\"#5E3333\">"
													+ subscriptionQuery + "</font></p></td>"
													+ "<td><p align=\"center\"><font face=\"verdana\" color=\"#5E3333\">"
													+ subscriptionResult + "</font></p></td></tr>";
										} else {
											JSONObject onesubscription = new JSONObject();
											onesubscription.put("KPName", kpName);
											onesubscription.put("CreationTime", creationTime);
											onesubscription.put("SubscriptionQuery", subscriptionQuery);
											onesubscription.put("SubscriptionResult", subscriptionResult);
											response = response + onesubscription.toJSONString();
										}

									}

									// get the writer to print
									PrintWriter writer = resp.getWriter();
									// Mahmoud 26-3-2013
									if (requestURI.contains("/show_subscriptions.json") == false) {
										writer.print("<HTML>");
										writer.print("<HEAD><TITLE>SMOOL RESTFUL INTERFACE</TITLE></HEAD>");
										writer.print("<BODY>");
										writer.print("<BR>");
										writer.print(
												"<p align=\"center\"><font face=\"verdana\" size=\"5\" color=\"#5E3333\"><b>Individual SIB current subscritpions</b></font></p>");
										writer.print("<BR>");
										writer.print(
												"<p align=\"left\"><font face=\"verdana\" size=\"2\" color=\"#5E3333\">This is a list containing all the existing subscriptions in the selected SIB</font></p>");
										writer.print("<BR>");
										writer.print(response);
										writer.print("</BODY>");
									}

									writer.close();
								} else {
									Activator.getDefault().getLogger().log(LogService.LOG_INFO,
											"Seems that are no subsciptions in the selected SIB at the moment.");
									empty = true;
								}
							}
						}
						// end the table
						// Mahmoud 26-3-2013
						if (requestURI.contains("/show_subscriptions.json") == false) {
							response = response + "</table>";
						}

					} else {
						Activator.getDefault().getLogger().log(LogService.LOG_INFO,
								"A SIB with the provided Name could not be found. Aborting.");
						empty = true;
					}
				} else {
					Activator.getDefault().getLogger().log(LogService.LOG_INFO,
							"SIB name provided seems to be empty. Aborting.");
					empty = true;
				}
			} else {
				Activator.getDefault().getLogger().log(LogService.LOG_INFO,
						"Could not find a GET param with named \"name\" in HttpRequest. Aborting.");
				empty = true;
			}
			// check empty flag to contrusct an empty response
			if (empty) {
				PrintWriter writer = resp.getWriter();
				// Mahmoud 26-3-2013
				if (requestURI.contains("/show_subscriptions.json") == false) {
					writer.print("<HTML>");
					writer.print("<HEAD><TITLE>SMOOL RESTFUL INTERFACE</TITLE></HEAD>");
					writer.print("<BODY>");
					writer.print("<BR>");
					writer.print(
							"<p align=\"center\"><font face=\"verdana\" size=\"5\" color=\"#5E3333\"><b>Individual SIB current subscritpions</b></font></p>");
					writer.print("<BR>");
					writer.print(
							"<p align=\"left\"><font face=\"verdana\" size=\"2\" color=\"#5E3333\">Looks like there are no subscriptions in the selected SIB, go back and select another one or wait for KPs to subscribe.</font></p>");
					writer.print("<BR>");
					writer.print(response);
					writer.print("</BODY>");
				} else {
					writer.print("{}");
				}

				writer.close();
			}
		} else if (requestURI.contains("/palette.json")) {
			// Mahmoud 15-3-2013 send jason

			String sensorsString = new String();
			String actuarttorString = new String();
			String rulesString = new String();

			// compose the json response
			// get the param that contains the sib ID that we must get
			Map<String, String[]> params = req.getParameterMap();
			// check that we actually have some params
			if (!params.isEmpty()) {
				String[] sibName = params.get("name");
				String name = sibName[0];
				if (!name.isEmpty()) {
					runningSIBs = Activator.getDefault().getCurrentSIBs();
					if (!runningSIBs.isEmpty()) {
						// get the SIB
						SIB sib = runningSIBs.get(name);
						Iterator<String> it;

						it = getLeafs(sib, "smoolcore:Sensor", "smoolcore:PhysicalSensor").iterator();
						while (it.hasNext()) {
							JSONObject sensors;
							sensors = new JSONObject();
							String current = it.next();
							sensors.put("PhysicalSensor", current);
							sensorsString = sensorsString + sensors.toJSONString();
						}

						it = getLeafs(sib, "smoolcore:Sensor", "smoolcore:LogicalSensor").iterator();
						while (it.hasNext()) {
							JSONObject sensors;
							sensors = new JSONObject();
							String current = it.next();
							sensors.put("LogicalSensor", current);
							sensorsString = sensorsString + sensors.toJSONString();
						}

						it = getLeafs(sib, "smoolcore:Actuator", "smoolcore:PhysicalActuator").iterator();
						while (it.hasNext()) {
							JSONObject actuators;
							actuators = new JSONObject();
							String current = it.next();
							actuators.put("PhysicalActuator", current);
							actuarttorString = actuarttorString + actuators.toJSONString();
						}

						it = getLeafs(sib, "smoolcore:Actuator", "smoolcore:LogicalActuator").iterator();
						while (it.hasNext()) {
							JSONObject actuators;
							actuators = new JSONObject();
							String current = it.next();
							actuators.put("LogicalActuator", current);
							actuarttorString = actuarttorString + actuators.toJSONString();
						}

						it = getLeafs(sib, "smoolcore:Rule", null).iterator();
						while (it.hasNext()) {
							JSONObject rules;
							rules = new JSONObject();
							String current = it.next();
							rules.put("Rule", current);
							rulesString = rulesString + rules.toJSONString();
						}

					}
				} else {
					Activator.getDefault().getLogger().log(LogService.LOG_INFO, "no SIBS was found");

				}
			}

			// send the json reponse
			PrintWriter writer = resp.getWriter();
			writer.print(sensorsString);
			writer.print(actuarttorString);
			writer.print(rulesString);
			writer.close();

		}
	}

	// mahmoud 14-3-2013 method to get the leafnodes from SIB by using query
	private List<String> getLeafs(SIB sib, String firstClass, String secondClass) {
		List<String> returnRes = new ArrayList<String>();
		;
		/* CHANGE JASON list not used - List<String> Res = null; */
		Collection<QuerySolution> queryResults = null;
		String queryString = null;
		if (secondClass == null) {
			queryString = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> PREFIX smoolcore: <http://com.tecnalia.smool/core/smoolcore#> SELECT ?s WHERE {?s rdfs:subClassOf "
					+ firstClass + " }";
		} else {
			queryString = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> PREFIX smoolcore: <http://com.tecnalia.smool/core/smoolcore#> SELECT ?s WHERE {?s rdfs:subClassOf "
					+ firstClass + ". ?s rdfs:subClassOf " + secondClass + " }";
		}
		if (sib != null) {
			try {// FILTER
					// SMOOLResultSet t=
					// sib.getSemanticModel().query("PREFIX rdfs:
					// <http://www.w3.org/2000/01/rdf-schema#> select distinct ?child ?father where
					// {?child rdfs:subClassOf ?father } ");
					// query all Sensor, PhysicalSensor
				SMOOLResultSet t = sib.getSemanticModel().query(queryString);
				queryResults = t.getQuerySolutions();

			} catch (SemanticModelException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// for each result make query to check if it has more children or
			// not
			if (queryResults != null) {
				Iterator<QuerySolution> itq = queryResults.iterator();
				Activator.getDefault().getLogger().log(LogService.LOG_INFO, "Query result for  " + firstClass);
				while (itq.hasNext()) {
					QuerySolution current = itq.next();
					String readable = current.toString().replace("( ?s = <", "");
					readable = readable.toString().replace("> )", "");
					readable = readable.toString().replace("http://com.tecnalia.smool/core/smoolcore#", "smoolcore:");

					SMOOLResultSet t = null;
					try {
						t = sib.getSemanticModel().query(
								"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> PREFIX smoolcore: <http://com.tecnalia.smool/core/smoolcore#> SELECT ?s WHERE {?s rdfs:subClassOf  "
										+ readable + "}");
					} catch (SemanticModelException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					Collection<QuerySolution> queryResults2 = t.getQuerySolutions();

					// check if it has no more childern
					if (queryResults2.size() == 1) {
						Activator.getDefault().getLogger().log(LogService.LOG_INFO, readable);
						returnRes.add(readable.replace("smoolcore:", ""));
					} else {

					}

				}
			}
		}

		return returnRes;
	}

	// mahmoud
	/**
	 * This method seeks a SIB server in the list of SIB instances registered in
	 * OSGi
	 * 
	 * @param id
	 * @return ISIB
	 */
	private ISIB getSIBbyID(int id) {
		ISIB sib = null;
		List<Element> elements = Activator.getDefault().getSibList();

		int i = 0;
		boolean found = false;
		while (i < elements.size() && found == false) {
			if (elements.get(i).getSib().getId() == id) {
				found = true;
				sib = elements.get(i).getSib();
			} else {
				i++;
			}

		}

		return sib;

	}

	/**
	 * This method seeks the data of a specific SIB in the SIBViewer instances
	 * registered in OSGi
	 * 
	 * @param id
	 * @return SibData
	 */
	private SibData getSIBDatabyID(int id) {
		SibData sibData = null;
		List<SibData> elements = Activator.getDefault().getSibViewerList();
		ISIB sib = getSIBbyID(id);

		int i = 0;
		boolean found = false;
		while (i < elements.size() && found == false) {
			// if (elements.get(i).getSibId() == id) {
			if (elements.get(i).getSib() == sib) {
				found = true;
				sibData = elements.get(i);
			} else {
				i++;
			}
		}
		return sibData;
	}

	/**
	 * This method navigates the list of Triples received, and return only the
	 * individuals
	 * 
	 */
	private List<Triple> locateIndividuals(List<Triple> triples) {

		List<Triple> returnTriples = new ArrayList<Triple>();

		for (int i = 0; i < triples.size(); i++) {
			Triple current = triples.get(i);
			if (current.getSubject().startsWith("individual:")) {
				returnTriples.add(current);
			}
		}

		return returnTriples;

	}

	private void security(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		// get action command to be executed
		String action = req.getParameter("action");
		if (action == null) {
			res.setStatus(400);
			res.getWriter().write("/security?action=[block,etc]&kp=$kpName");
			return;
		}

		// execute method based on action value
		switch (action) {
		case "block":
			blockKPs(req, res);
			break;
		case "kill":
			// kill SERVER (not a kp) to allow systemd service to restart it
			// since this is a sensitive operation, do not document it even if rest calls to
			// smool are safe in production environments
			System.exit(1001);
		default:
			res.setStatus(400);
		}
	}

	/**
	 * GET /block?kp=$kpname
	 * <p>
	 * Disconnect one or some clients based on KP nomenclature. If KP is
	 * MyDevice-11223344, only that KP is disconnected. If KP is MyDevice, this KP
	 * and subsequent MyDevice-dddddddd will be disconnected
	 * </p>
	 * 
	 * @param req
	 * @param resp
	 * @throws ServletException
	 * @throws IOException
	 */
	private void blockKPs(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		Map<String, String[]> params = req.getParameterMap();
		String kpName = params.get("kp")[0];
		HashMap<String, SIB> sibs = Activator.getDefault().getCurrentSIBs();
		SIB sib = sibs.get(sibs.keySet().iterator().next());
		try {
			sib.blockKPs(kpName);
		} catch (SIBException e) {
			res.setStatus(500);
			res.getWriter().write("SIB error:" + e.getMessage());
			res.getWriter().close();
		}
		res.setStatus(204);
	}

	private Set<String> blockKPs = new TreeSet<>();
}