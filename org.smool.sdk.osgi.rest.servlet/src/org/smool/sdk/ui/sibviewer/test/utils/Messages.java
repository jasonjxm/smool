package org.smool.sdk.ui.sibviewer.test.utils;

/*
 *    Copyright 2010 European Software Institute
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *    
 *      http://www.apache.org/licenses/LICENSE-2.0
 *    
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *    
 */

import java.util.Vector;

/**
 * This class contains the default messages used by the test window to initialize the
 * operation parameters area.
 * Each kind of operation has its own default message.
 * 
 * @author Cristina L�pez, cristina.lopez@tecnalia.com, ESI
 *
 */
public class Messages{

	/**
	 * This method obtain the default ontology model
	 * @return String
	 */
	public static String getOntology() {
		StringBuilder contents = new StringBuilder();
		
		contents.append("<?xml version='1.0'?>");
		contents.append("<!DOCTYPE rdf:RDF [");
		contents.append("<!ENTITY test 'http://www.sofia-project.eu/test/tempsensor#'>");
		contents.append("<!ENTITY owl 'http://www.w3.org/2002/07/owl#'>");
		contents.append("<!ENTITY rdf 'http://www.w3.org/1999/02/22-rdf-syntax-ns#'>");
		contents.append("<!ENTITY rdfs 'http://www.w3.org/2000/01/rdf-schema#'>");
		contents.append("<!ENTITY xsd 'http://www.w3.org/2001/XMLSchema#'>");
		contents.append("]>");
		contents.append("<rdf:RDF xmlns='&test;' xmlns:test='&test;' xml:base='&test;' xmlns:owl='&owl;' xmlns:rdf='&rdf;' xmlns:rdfs='&rdfs;' xmlns:xsd='&xsd;'>");
		contents.append("<owl:Ontology rdf:about=''/>");
		contents.append("<owl:Class rdf:ID='Device'/>");
		contents.append("<owl:Class rdf:ID='Result'/>");
		contents.append("<owl:DatatypeProperty rdf:ID='hasValue'>");
		contents.append("<rdfs:domain rdf:resource='#Result'/>");
		contents.append("<rdfs:range rdf:resource='&xsd;float'/>");
		contents.append("</owl:DatatypeProperty>");
		contents.append("<owl:DatatypeProperty rdf:ID='hasUnit'>");
		contents.append("<rdfs:domain rdf:resource='#Result'/>");
		contents.append("<rdfs:range rdf:resource='&xsd;string'/>");
		contents.append("</owl:DatatypeProperty>");
		contents.append("<owl:ObjectProperty rdf:ID='hasResult'>");
		contents.append("<rdfs:domain rdf:resource='#Device'/>");
		contents.append("<rdfs:range rdf:resource='#Result'/>");
		contents.append("</owl:ObjectProperty>");
		contents.append("</rdf:RDF>");				    
		return contents.toString();
	}
	
	/**
	 * This method obtains the default insert message
	 * @return Vector<String>
	 */
	public static Vector<String> getDefaultInsertMessage()
	{
		Vector<String> _return= new Vector<String>();
		_return.add("<parameter name='insert_graph' encoding='RDF-XML'>");
		_return.add("<?xml version='1.0'?>");
		_return.add("<!DOCTYPE rdf:RDF [");
		_return.add("<!ENTITY owl 'http://www.w3.org/2002/07/owl#'>");
		_return.add("<!ENTITY xsd 'http://www.w3.org/2001/XMLSchema#'>");
		_return.add("<!ENTITY rdfs 'http://www.w3.org/2000/01/rdf-schema#'>");
		_return.add("<!ENTITY rdf 'http://www.w3.org/1999/02/22-rdf-syntax-ns#'>");
		_return.add("<!ENTITY wmc 'https://194.30.38.163/sofia/ontology/1.2/city/wmc.owl#'>");
		_return.add("]>");
		_return.add("<rdf:RDF xmlns='https://194.30.38.163/sofia/ontology/1.2/city/wmc.owl#' ");
		_return.add("xml:base='https://194.30.38.163/sofia/ontology/1.2/city/wmc.owl#' ");
		_return.add("xmlns:dc='http://purl.org/dc/elements/1.1/#' ");
		_return.add("xmlns:rdfs='http://www.w3.org/2000/01/rdf-schema#' ");
		_return.add("xmlns:owl='http://www.w3.org/2002/07/owl#' ");
		_return.add("xmlns:xsd='http://www.w3.org/2001/XMLSchema#' ");
		_return.add("xmlns:rdf='http://www.w3.org/1999/02/22-rdf-syntax-ns#' ");
		_return.add("xmlns:wmc='https://194.30.38.163/sofia/ontology/1.2/city/wmc.owl#' ");
		_return.add("xmlns:dcterms='http://purl.org/dc/terms/#'>");
		_return.add("<EmergencyDeclaration rdf:about='#EmergencyDeclared'>");
		_return.add("<description rdf:datatype='&xsd;string'>Emergency declared at the train station</description>");
		_return.add("<declaredAt rdf:datatype='&xsd;dateTime'>2010-05-17T13:17:42Z</declaredAt>");
		_return.add("<area rdf:resource='&wmc;StationArea' />");
		_return.add("<signature rdf:datatype='&xsd;string'>$iGN4TUR3-t3sT</signature>");
		_return.add("<certificate rdf:datatype='&xsd;string'>C3r71f1c4T3-t3sT</certificate>");
		_return.add("</EmergencyDeclaration>");
		_return.add("<GisDisk2Points rdf:about='#StationArea'>");
		_return.add("<name rdf:datatype='&xsd;string'>Station area</name>");
		_return.add("<center rdf:resource='&wmc;StationLocation' />");
		_return.add("<border rdf:datatype='&xsd;double'>25</border>");
		_return.add("</GisDisk2Points>");
		_return.add("<GPSLocation rdf:about='#StationLocation'>");
		_return.add("<name rdf:datatype='&xsd;string'>Station Platform Zone</name>");
		_return.add("<elevation rdf:datatype='&xsd;double'>62</elevation>");
		_return.add("<latitude rdf:datatype='&xsd;double'>50.84562</latitude>");
		_return.add("<longitude rdf:datatype='&xsd;double'>4.35687</longitude>");
		_return.add("</GPSLocation>");
		_return.add("</rdf:RDF>");
		_return.add("</parameter>");
		return _return;
	}

	/**
	 * This method obtains the default update message
	 * @return Vector<String>
	 */
	public static Vector<String> getDefaultUpdateMessage()
	{
		Vector<String> _return= new Vector<String>();
		_return.add("<parameter name='insert_graph' encoding='RDF-XML'>");
		_return.add("<?xml version='1.0'?>");
		_return.add("<!DOCTYPE rdf:RDF [");
		_return.add("<!ENTITY owl 'http://www.w3.org/2002/07/owl#'>");
		_return.add("<!ENTITY xsd 'http://www.w3.org/2001/XMLSchema#'>");
		_return.add("<!ENTITY rdfs 'http://www.w3.org/2000/01/rdf-schema#'>");
		_return.add("<!ENTITY rdf 'http://www.w3.org/1999/02/22-rdf-syntax-ns#'>");
		_return.add("<!ENTITY wmc 'https://194.30.38.163/sofia/ontology/1.2/city/wmc.owl#'>");
		_return.add("]>");
		_return.add("<rdf:RDF xmlns='https://194.30.38.163/sofia/ontology/1.2/city/wmc.owl#' ");
		_return.add("xml:base='https://194.30.38.163/sofia/ontology/1.2/city/wmc.owl#' ");
		_return.add("xmlns:dc='http://purl.org/dc/elements/1.1/#' ");
		_return.add("xmlns:rdfs='http://www.w3.org/2000/01/rdf-schema#' ");
		_return.add("xmlns:owl='http://www.w3.org/2002/07/owl#' ");
		_return.add("xmlns:xsd='http://www.w3.org/2001/XMLSchema#' ");
		_return.add("xmlns:rdf='http://www.w3.org/1999/02/22-rdf-syntax-ns#' ");
		_return.add("xmlns:wmc='https://194.30.38.163/sofia/ontology/1.2/city/wmc.owl#' ");
		_return.add("xmlns:dcterms='http://purl.org/dc/terms/#'>");
		_return.add("<EmergencyDeclaration rdf:about='#EmergencyDeclared'>");
		_return.add("<expectedEnd rdf:datatype='&xsd;dateTime'>2010-05-17T14:00:00Z</expectedEnd>");
		_return.add("</EmergencyDeclaration>");
		_return.add("</rdf:RDF>");
		_return.add("</parameter>");
		_return.add("<parameter name='REMOVETRIPLES' type='RDFXML'>");
		_return.add("<?xml version='1.0'?>");
		_return.add("<!DOCTYPE rdf:RDF [");
		_return.add("<!ENTITY owl 'http://www.w3.org/2002/07/owl#'>");
		_return.add("<!ENTITY xsd 'http://www.w3.org/2001/XMLSchema#'>");
		_return.add("<!ENTITY rdfs 'http://www.w3.org/2000/01/rdf-schema#'>");
		_return.add("<!ENTITY rdf 'http://www.w3.org/1999/02/22-rdf-syntax-ns#'>");
		_return.add("<!ENTITY wmc 'https://194.30.38.163/sofia/ontology/1.2/city/wmc.owl#'>");
		_return.add("]>");
		_return.add("<rdf:RDF xmlns='https://194.30.38.163/sofia/ontology/1.2/city/wmc.owl#' ");
		_return.add("xml:base='https://194.30.38.163/sofia/ontology/1.2/city/wmc.owl#' ");
		_return.add("xmlns:dc='http://purl.org/dc/elements/1.1/#' ");
		_return.add("xmlns:rdfs='http://www.w3.org/2000/01/rdf-schema#' ");
		_return.add("xmlns:owl='http://www.w3.org/2002/07/owl#' ");
		_return.add("xmlns:xsd='http://www.w3.org/2001/XMLSchema#' ");
		_return.add("xmlns:rdf='http://www.w3.org/1999/02/22-rdf-syntax-ns#' ");
		_return.add("xmlns:wmc='https://194.30.38.163/sofia/ontology/1.2/city/wmc.owl#' ");
		_return.add("xmlns:dcterms='http://purl.org/dc/terms/#'>");
		_return.add("<EmergencyDeclaration rdf:about='#EmergencyDeclared'>");
		_return.add("<declaredAt rdf:datatype='&xsd;dateTime'>2010-05-17T13:17:42Z</declaredAt>");
		_return.add("</EmergencyDeclaration>");
		_return.add("</rdf:RDF>");
		_return.add("</parameter>");		
		return _return;
	}

	/**
	 * This method obtains the default quey message
	 * @return Vector<String>
	 */
	public static Vector<String> getDefaultQueryMessage()
	{		
		Vector<String> _return= new Vector<String>();
		_return.add("<parameter name='query' encoding='RDF-XML'>");
		_return.add("PREFIX wmc: <https://194.30.38.163/sofia/ontology/1.2/city/wmc.owl#>");
		_return.add("PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>");
		_return.add("SELECT ?emergencydesc ?time ?end ?areaname");
		_return.add("WHERE { ?emergency rdf:type wmc:EmergencyDeclaration . ");
		_return.add("?emergency wmc:description ?emergencydesc . ");
		_return.add("?emergency wmc:declaredAt ?time . ");
		_return.add("?emergency wmc:expectedEnd ?end . ");
		_return.add("?emergency wmc:area ?area . ");
		_return.add("?area wmc:name ?areaname} ");
		_return.add("</parameter>");
		return _return;
	}
	
	/**
	 * This method obtains the default subscribe message
	 * @return Vector<String>
	 */
	public static Vector<String> getDefaultSubscribeMessage()
	{		
		Vector<String> _return= new Vector<String>();
		_return.add("<parameter name='query' encoding='RDF-XML'>");
		_return.add("PREFIX wmc: <https://194.30.38.163/sofia/ontology/1.2/city/wmc.owl#>");
		_return.add("PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>");
		_return.add("SELECT ?emergencydesc ?time ?end ?areaname");
		_return.add("WHERE { ?emergency rdf:type wmc:EmergencyDeclaration . ");
		_return.add("?emergency wmc:description ?emergencydesc . ");
		_return.add("?emergency wmc:declaredAt ?time . ");
		_return.add("?emergency wmc:expectedEnd ?end . ");
		_return.add("?emergency wmc:area ?area . ");
		_return.add("?area wmc:name ?areaname} ");
		_return.add("</parameter>");
		return _return;
	}
	
	/**
	 * This method obtains the default unsubscribe message
	 * @return Vector<String>
	 */
	public static Vector<String> getDefaultUnsubscribeMessage()
	{		
		Vector<String> _return= new Vector<String>();
		_return.add("<parameter name='SUBSCRIPTIONID'>1</parameter>");
		return _return;
	}
	
	/**
	 * This method obtains the default remove message
	 * @return
	 */
	public static Vector<String> getDefaultRemoveMessage()
	{		
		Vector<String> _return= new Vector<String>();
		_return.add("<parameter name='remove_graph' encoding='RDF-XML'>");
		_return.add("<?xml version='1.0'?>");
		_return.add("<!DOCTYPE rdf:RDF [");
		_return.add("<!ENTITY owl 'http://www.w3.org/2002/07/owl#'>");
		_return.add("<!ENTITY xsd 'http://www.w3.org/2001/XMLSchema#'>");
		_return.add("<!ENTITY rdfs 'http://www.w3.org/2000/01/rdf-schema#'>");
		_return.add("<!ENTITY rdf 'http://www.w3.org/1999/02/22-rdf-syntax-ns#'>");
		_return.add("<!ENTITY wmc 'https://194.30.38.163/sofia/ontology/1.2/city/wmc.owl#'>");
		_return.add("]>");
		_return.add("<rdf:RDF xmlns='https://194.30.38.163/sofia/ontology/1.2/city/wmc.owl#' ");
		_return.add("xml:base='https://194.30.38.163/sofia/ontology/1.2/city/wmc.owl#' ");
		_return.add("xmlns:dc='http://purl.org/dc/elements/1.1/#' ");
		_return.add("xmlns:rdfs='http://www.w3.org/2000/01/rdf-schema#' ");
		_return.add("xmlns:owl='http://www.w3.org/2002/07/owl#' ");
		_return.add("xmlns:xsd='http://www.w3.org/2001/XMLSchema#' ");
		_return.add("xmlns:rdf='http://www.w3.org/1999/02/22-rdf-syntax-ns#' ");
		_return.add("xmlns:wmc='https://194.30.38.163/sofia/ontology/1.2/city/wmc.owl#' ");
		_return.add("xmlns:dcterms='http://purl.org/dc/terms/#'>");
		_return.add("<EmergencyDeclaration rdf:about='#EmergencyDeclared'>");
		_return.add("<description rdf:datatype='&xsd;string'>Emergency declared at the train station</description>");
		_return.add("<declaredAt rdf:datatype='&xsd;dateTime'>2010-05-17T13:17:42Z</declaredAt>");
		_return.add("<expectedEnd rdf:datatype='&xsd;dateTime'>2010-05-17T14:00:00Z</expectedEnd>");
		_return.add("<area rdf:resource='&wmc;StationArea' />");
		_return.add("<signature rdf:datatype='&xsd;string'>$iGN4TUR3-t3sT</signature>");
		_return.add("<certificate rdf:datatype='&xsd;string'>C3r71f1c4T3-t3sT</certificate>");
		_return.add("</EmergencyDeclaration>");
		_return.add("</rdf:RDF>");
		_return.add("</parameter>");
		return _return;
	}

}
