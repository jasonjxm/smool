
/*******************************************************************************
* Copyright (c) 2012 Tecnalia Research and Innovation.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
* This file is a result of OWL 2 java transformation using EMF
* 
* Generated by SMOOL SDK Wizard
*******************************************************************************/ 
package org.smool.generic.java.producer.model.smoolcore;
       
import org.smool.kpi.model.smart.IAbstractOntConcept;
import org.smool.generic.java.producer.model.smoolcore.IPhysicalLocation;
       
import org.smool.generic.java.producer.model.smoolcore.ILocation3D;
       

/**
 * This class implements interface for the ontology concept Location3DSensor
 * including all its properties.
 * @author Genrated via EMF OWL to java transformation
 * @version 1.0
 */

public interface ILocation3DSensor extends IAbstractOntConcept, ILocationSensor{

   /*
 	* PROPERTIES: GETTERS AND SETTERS
 	*/
   
 	/**
 	* Sets the deviceID property.
 	* @param deviceID String value
 	*/
 	public void setDeviceID(String deviceID );

	/**
 	* Gets the deviceID property.
 	* @return a String value
	*/
 	public String getDeviceID();

 	/**
 	* Sets the vendor property.
 	* @param vendor String value
 	*/
 	public void setVendor(String vendor );

	/**
 	* Gets the vendor property.
 	* @return a String value
	*/
 	public String getVendor();

 	/**
 	* Sets the location property.
 	* @param location IPhysicalLocation value
 	*/
 	public void setLocation(IPhysicalLocation location );

	/**
 	* Gets the location property.
 	* @return a IPhysicalLocation value
	*/
 	public IPhysicalLocation getLocation();

 	/**
 	* Sets the location3d property.
 	* @param location3d ILocation3D value
 	*/
 	public void setLocation3d(ILocation3D location3d );

	/**
 	* Gets the location3d property.
 	* @return a ILocation3D value
	*/
 	public ILocation3D getLocation3d();

 	/**
 	* Sets the physicalLoc property.
 	* @param physicalLoc IPhysicalLocation value
 	*/
 	public void setPhysicalLoc(IPhysicalLocation physicalLoc );

	/**
 	* Gets the physicalLoc property.
 	* @return a IPhysicalLocation value
	*/
 	public IPhysicalLocation getPhysicalLoc();
}
