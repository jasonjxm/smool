
/*******************************************************************************
* Copyright (c) 2012 Tecnalia Research and Innovation.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
* This file is a result of OWL 2 java transformation using EMF
* Contributors:
*    Enas Ashraf (inas@itida.gov.eg) - creation of level 2 metamodel and transformation to java classes 
*    Adrian Noguero (Tecnalia Research and Innovation - Software Systems Engineering) - reation of level 1 metamodel by creating ...
*******************************************************************************/ 
package org.smool.generic.java.producer.model.smoolcore.impl;
     
import org.smool.kpi.model.smart.AbstractOntConcept;
import org.smool.kpi.model.smart.KPProducer;
import org.smool.kpi.model.smart.KPConsumer;
import org.smool.kpi.model.smart.slots.FunctionalDatatypeSlot;
import org.smool.generic.java.producer.model.smoolcore.IPresenceInformation;

/**
 * This class implements the ontology concept PresenceInformation
 * including all its properties.
 * @author Genrated via EMF OWL to java transformation
 * @version 1.0
 */
public class PresenceInformation extends AbstractOntConcept implements IPresenceInformation, KPProducer, KPConsumer{

    //Not needed.. public static String CLASS_NAMESPACE = "http://com.tecnalia.smool/core/smoolcore#";
  	//Not needed.. public static String CLASS_ID = "PresenceInformation";
  	public static String CLASS_IRI = "http://com.tecnalia.smool/core/smoolcore#PresenceInformation"; 
  		
  		
  	/**
    * The Constructor
    */
    public PresenceInformation() {
    	super();
        init();
	}
    	
    	
	/**
 	* The Constructor
 	* @param id the Actuator identifier
 	*/
	public PresenceInformation(String id) {
      	/** Call superclass to establish the identifier */
      	super(id);
      	init();
	}
    
    	
    	
	/**
 	* Inits the fields associated to a ontology concept
 	*/
	public void init() {
      	/** Sets the context of this ontology concept */
      	this._setClassContext("smoolcore", CLASS_IRI);

      	/** Sets the individual context */
      	this._setDefaultIndividualContext();
    
      
      	// Creates the active property
      	String activeIRI = "http://com.tecnalia.smool/core/smoolcore#active";
      	String activePrefix = "smoolcore";

      	FunctionalDatatypeSlot < Boolean > activeSlot= new FunctionalDatatypeSlot<Boolean>(Boolean.class);
      	activeSlot._setIRI(activeIRI);
      	activeSlot._setPrefix(activePrefix);
      	activeSlot.setRange("xsd:Boolean");
      	this._addProperty(activeSlot);
  	  
  	  
      	// Creates the presence property
      	String presenceIRI = "http://com.tecnalia.smool/core/smoolcore#presence";
      	String presencePrefix = "smoolcore";

      	FunctionalDatatypeSlot < Boolean > presenceSlot= new FunctionalDatatypeSlot<Boolean>(Boolean.class);
      	presenceSlot._setIRI(presenceIRI);
      	presenceSlot._setPrefix(presencePrefix);
      	presenceSlot.setRange("xsd:Boolean");
      	this._addProperty(presenceSlot);
  	  
  	}
	/*
	* PROPERTIES: GETTERS AND SETTERS
	*/
 	
 	/**
 	* Sets the active property.
 	* @param active Boolean value
 	*/
	public void setActive(Boolean active) {
		this.updateAttribute("active",active);        
	}
		
	 /**
 	* Gets the active property.
 	* @return a Boolean value
 	*/
	public Boolean getActive() {
    	return (Boolean) this._getFunctionalProperty("active").getValue();
	}

 	/**
 	* Sets the presence property.
 	* @param presence Boolean value
 	*/
	public void setPresence(Boolean presence) {
		this.updateAttribute("presence",presence);        
	}
		
	 /**
 	* Gets the presence property.
 	* @return a Boolean value
 	*/
	public Boolean getPresence() {
    	return (Boolean) this._getFunctionalProperty("presence").getValue();
	}

}
