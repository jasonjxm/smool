
/*******************************************************************************
* Copyright (c) 2012 Tecnalia Research and Innovation.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
* This file is a result of OWL 2 java transformation using EMF
* Contributors:
*    Enas Ashraf (inas@itida.gov.eg) - creation of level 2 metamodel and transformation to java classes 
*    Adrian Noguero (Tecnalia Research and Innovation - Software Systems Engineering) - reation of level 1 metamodel by creating ...
*******************************************************************************/ 
package org.smool.generic.java.producer.model.smoolcore.impl;
     
import org.smool.kpi.model.smart.AbstractOntConcept;
import org.smool.kpi.model.smart.KPProducer;
import org.smool.kpi.model.smart.KPConsumer;
import org.smool.kpi.model.smart.slots.FunctionalDatatypeSlot;
import org.smool.generic.java.producer.model.smoolcore.ILocation2D;

/**
 * This class implements the ontology concept Location2D
 * including all its properties.
 * @author Genrated via EMF OWL to java transformation
 * @version 1.0
 */
public class Location2D extends AbstractOntConcept implements ILocation2D, KPProducer, KPConsumer{

    //Not needed.. public static String CLASS_NAMESPACE = "http://com.tecnalia.smool/core/smoolcore#";
  	//Not needed.. public static String CLASS_ID = "Location2D";
  	public static String CLASS_IRI = "http://com.tecnalia.smool/core/smoolcore#Location2D"; 
  		
  		
  	/**
    * The Constructor
    */
    public Location2D() {
    	super();
        init();
	}
    	
    	
	/**
 	* The Constructor
 	* @param id the Actuator identifier
 	*/
	public Location2D(String id) {
      	/** Call superclass to establish the identifier */
      	super(id);
      	init();
	}
    
    	
    	
	/**
 	* Inits the fields associated to a ontology concept
 	*/
	public void init() {
      	/** Sets the context of this ontology concept */
      	this._setClassContext("smoolcore", CLASS_IRI);

      	/** Sets the individual context */
      	this._setDefaultIndividualContext();
    
      
      	// Creates the x property
      	String xIRI = "http://com.tecnalia.smool/core/smoolcore#x";
      	String xPrefix = "smoolcore";

      	FunctionalDatatypeSlot < Double > xSlot= new FunctionalDatatypeSlot<Double>(Double.class);
      	xSlot._setIRI(xIRI);
      	xSlot._setPrefix(xPrefix);
      	xSlot.setRange("xsd:Double");
      	this._addProperty(xSlot);
  	  
  	  
      	// Creates the y property
      	String yIRI = "http://com.tecnalia.smool/core/smoolcore#y";
      	String yPrefix = "smoolcore";

      	FunctionalDatatypeSlot < Double > ySlot= new FunctionalDatatypeSlot<Double>(Double.class);
      	ySlot._setIRI(yIRI);
      	ySlot._setPrefix(yPrefix);
      	ySlot.setRange("xsd:Double");
      	this._addProperty(ySlot);
  	  
  	}
	/*
	* PROPERTIES: GETTERS AND SETTERS
	*/
 	
 	/**
 	* Sets the x property.
 	* @param x Double value
 	*/
	public void setX(Double x) {
		this.updateAttribute("x",x);        
	}
		
	 /**
 	* Gets the x property.
 	* @return a Double value
 	*/
	public Double getX() {
    	return (Double) this._getFunctionalProperty("x").getValue();
	}

 	/**
 	* Sets the y property.
 	* @param y Double value
 	*/
	public void setY(Double y) {
		this.updateAttribute("y",y);        
	}
		
	 /**
 	* Gets the y property.
 	* @return a Double value
 	*/
	public Double getY() {
    	return (Double) this._getFunctionalProperty("y").getValue();
	}

}
