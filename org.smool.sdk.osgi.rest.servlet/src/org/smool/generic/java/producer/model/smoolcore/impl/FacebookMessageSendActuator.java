
/*******************************************************************************
* Copyright (c) 2012 Tecnalia Research and Innovation.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
* This file is a result of OWL 2 java transformation using EMF
* Contributors:
*    Enas Ashraf (inas@itida.gov.eg) - creation of level 2 metamodel and transformation to java classes 
*    Adrian Noguero (Tecnalia Research and Innovation - Software Systems Engineering) - reation of level 1 metamodel by creating ...
*******************************************************************************/ 
package org.smool.generic.java.producer.model.smoolcore.impl;
     
import org.smool.kpi.model.smart.AbstractOntConcept;
import org.smool.kpi.model.smart.KPProducer;
import org.smool.kpi.model.smart.KPConsumer;
import org.smool.kpi.model.smart.slots.FunctionalDatatypeSlot;
import org.smool.kpi.model.smart.slots.FunctionalObjectSlot;
import org.smool.generic.java.producer.model.smoolcore.IFacebookMessageSendActuator;
import org.smool.generic.java.producer.model.smoolcore.ILogicalLocation;
import org.smool.generic.java.producer.model.smoolcore.impl.LogicalLocation;
import org.smool.generic.java.producer.model.smoolcore.IFacebookAccount;
import org.smool.generic.java.producer.model.smoolcore.impl.FacebookAccount;
import org.smool.generic.java.producer.model.smoolcore.IMessage;
import org.smool.generic.java.producer.model.smoolcore.impl.Message;

/**
 * This class implements the ontology concept FacebookMessageSendActuator
 * including all its properties.
 * @author Genrated via EMF OWL to java transformation
 * @version 1.0
 */
public class FacebookMessageSendActuator extends AbstractOntConcept implements IFacebookMessageSendActuator, KPProducer, KPConsumer{

    //Not needed.. public static String CLASS_NAMESPACE = "http://com.tecnalia.smool/core/smoolcore#";
  	//Not needed.. public static String CLASS_ID = "FacebookMessageSendActuator";
  	public static String CLASS_IRI = "http://com.tecnalia.smool/core/smoolcore#FacebookMessageSendActuator"; 
  		
  		
  	/**
    * The Constructor
    */
    public FacebookMessageSendActuator() {
    	super();
        init();
	}
    	
    	
	/**
 	* The Constructor
 	* @param id the Actuator identifier
 	*/
	public FacebookMessageSendActuator(String id) {
      	/** Call superclass to establish the identifier */
      	super(id);
      	init();
	}
    
    	
    	
	/**
 	* Inits the fields associated to a ontology concept
 	*/
	public void init() {
      	/** Sets the context of this ontology concept */
      	this._setClassContext("smoolcore", CLASS_IRI);

      	/** Sets the individual context */
      	this._setDefaultIndividualContext();
    
      
      	// Creates the deviceID property
      	String deviceIDIRI = "http://com.tecnalia.smool/core/smoolcore#deviceID";
      	String deviceIDPrefix = "smoolcore";

      	FunctionalDatatypeSlot < String > deviceIDSlot= new FunctionalDatatypeSlot<String>(String.class);
      	deviceIDSlot._setIRI(deviceIDIRI);
      	deviceIDSlot._setPrefix(deviceIDPrefix);
      	deviceIDSlot.setRange("xsd:String");
      	this._addProperty(deviceIDSlot);
  	  
  	  
      	// Creates the vendor property
      	String vendorIRI = "http://com.tecnalia.smool/core/smoolcore#vendor";
      	String vendorPrefix = "smoolcore";

      	FunctionalDatatypeSlot < String > vendorSlot= new FunctionalDatatypeSlot<String>(String.class);
      	vendorSlot._setIRI(vendorIRI);
      	vendorSlot._setPrefix(vendorPrefix);
      	vendorSlot.setRange("xsd:String");
      	this._addProperty(vendorSlot);
  	  
  	  
      	// Creates the destination property
      	String destinationIRI = "http://com.tecnalia.smool/core/smoolcore#destination";
      	String destinationPrefix = "smoolcore";

      	FunctionalObjectSlot < LogicalLocation > destinationSlot= new FunctionalObjectSlot<LogicalLocation>(LogicalLocation.class);
      	destinationSlot._setIRI(destinationIRI);
      	destinationSlot._setPrefix(destinationPrefix);
      	
      	this._addProperty(destinationSlot);
  	  
  	  
      	// Creates the fbDestination property
      	String fbDestinationIRI = "http://com.tecnalia.smool/core/smoolcore#fbDestination";
      	String fbDestinationPrefix = "smoolcore";

      	FunctionalObjectSlot < FacebookAccount > fbDestinationSlot= new FunctionalObjectSlot<FacebookAccount>(FacebookAccount.class);
      	fbDestinationSlot._setIRI(fbDestinationIRI);
      	fbDestinationSlot._setPrefix(fbDestinationPrefix);
      	
      	this._addProperty(fbDestinationSlot);
  	  
  	  
      	// Creates the fbSource property
      	String fbSourceIRI = "http://com.tecnalia.smool/core/smoolcore#fbSource";
      	String fbSourcePrefix = "smoolcore";

      	FunctionalObjectSlot < FacebookAccount > fbSourceSlot= new FunctionalObjectSlot<FacebookAccount>(FacebookAccount.class);
      	fbSourceSlot._setIRI(fbSourceIRI);
      	fbSourceSlot._setPrefix(fbSourcePrefix);
      	
      	this._addProperty(fbSourceSlot);
  	  
  	  
      	// Creates the logicalLoc property
      	String logicalLocIRI = "http://com.tecnalia.smool/core/smoolcore#logicalLoc";
      	String logicalLocPrefix = "smoolcore";

      	FunctionalObjectSlot < LogicalLocation > logicalLocSlot= new FunctionalObjectSlot<LogicalLocation>(LogicalLocation.class);
      	logicalLocSlot._setIRI(logicalLocIRI);
      	logicalLocSlot._setPrefix(logicalLocPrefix);
      	
      	this._addProperty(logicalLocSlot);
  	  
  	  
      	// Creates the message property
      	String messageIRI = "http://com.tecnalia.smool/core/smoolcore#message";
      	String messagePrefix = "smoolcore";

      	FunctionalObjectSlot < Message > messageSlot= new FunctionalObjectSlot<Message>(Message.class);
      	messageSlot._setIRI(messageIRI);
      	messageSlot._setPrefix(messagePrefix);
      	
      	this._addProperty(messageSlot);
  	  
  	  
      	// Creates the origin property
      	String originIRI = "http://com.tecnalia.smool/core/smoolcore#origin";
      	String originPrefix = "smoolcore";

      	FunctionalObjectSlot < LogicalLocation > originSlot= new FunctionalObjectSlot<LogicalLocation>(LogicalLocation.class);
      	originSlot._setIRI(originIRI);
      	originSlot._setPrefix(originPrefix);
      	
      	this._addProperty(originSlot);
  	  
  	}
	/*
	* PROPERTIES: GETTERS AND SETTERS
	*/
 	
 	/**
 	* Sets the deviceID property.
 	* @param deviceID String value
 	*/
	public void setDeviceID(String deviceID) {
		this.updateAttribute("deviceID",deviceID);        
	}
		
	 /**
 	* Gets the deviceID property.
 	* @return a String value
 	*/
	public String getDeviceID() {
    	return (String) this._getFunctionalProperty("deviceID").getValue();
	}

 	/**
 	* Sets the vendor property.
 	* @param vendor String value
 	*/
	public void setVendor(String vendor) {
		this.updateAttribute("vendor",vendor);        
	}
		
	 /**
 	* Gets the vendor property.
 	* @return a String value
 	*/
	public String getVendor() {
    	return (String) this._getFunctionalProperty("vendor").getValue();
	}

 	/**
 	* Adds the destination property.
 	* @param destination ILogicalLocation value to add
 	*/
	public void addDestination(ILogicalLocation destination) {
	  
	  if (!this.containsAttribute("destination",destination)) {
            this.addAttribute("destination",destination);
        }
		
	}
		
	 /**
 	* Removes the destination property.
 	* @param destination ILogicalLocation value to remove
 	*/
	public void removeDestination(ILogicalLocation destination) {
	  if (this.containsAttribute("destination",destination)) {
            this.removeAttribute("destination",destination);
        }
    	
	}

 	/**
 	* Adds the fbDestination property.
 	* @param fbDestination IFacebookAccount value to add
 	*/
	public void addFbDestination(IFacebookAccount fbDestination) {
	  
	  if (!this.containsAttribute("fbDestination",fbDestination)) {
            this.addAttribute("fbDestination",fbDestination);
        }
		
	}
		
	 /**
 	* Removes the fbDestination property.
 	* @param fbDestination IFacebookAccount value to remove
 	*/
	public void removeFbDestination(IFacebookAccount fbDestination) {
	  if (this.containsAttribute("fbDestination",fbDestination)) {
            this.removeAttribute("fbDestination",fbDestination);
        }
    	
	}

 	/**
 	* Sets the fbSource property.
 	* @param fbSource IFacebookAccount value
 	*/
	public void setFbSource(IFacebookAccount fbSource) {
		this.updateAttribute("fbSource",fbSource);        
	}
		
	 /**
 	* Gets the fbSource property.
 	* @return a IFacebookAccount value
 	*/
	public IFacebookAccount getFbSource() {
    	return (IFacebookAccount) this._getFunctionalProperty("fbSource").getValue();
	}

 	/**
 	* Sets the logicalLoc property.
 	* @param logicalLoc ILogicalLocation value
 	*/
	public void setLogicalLoc(ILogicalLocation logicalLoc) {
		this.updateAttribute("logicalLoc",logicalLoc);        
	}
		
	 /**
 	* Gets the logicalLoc property.
 	* @return a ILogicalLocation value
 	*/
	public ILogicalLocation getLogicalLoc() {
    	return (ILogicalLocation) this._getFunctionalProperty("logicalLoc").getValue();
	}

 	/**
 	* Sets the message property.
 	* @param message IMessage value
 	*/
	public void setMessage(IMessage message) {
		this.updateAttribute("message",message);        
	}
		
	 /**
 	* Gets the message property.
 	* @return a IMessage value
 	*/
	public IMessage getMessage() {
    	return (IMessage) this._getFunctionalProperty("message").getValue();
	}

 	/**
 	* Sets the origin property.
 	* @param origin ILogicalLocation value
 	*/
	public void setOrigin(ILogicalLocation origin) {
		this.updateAttribute("origin",origin);        
	}
		
	 /**
 	* Gets the origin property.
 	* @return a ILogicalLocation value
 	*/
	public ILogicalLocation getOrigin() {
    	return (ILogicalLocation) this._getFunctionalProperty("origin").getValue();
	}

}
