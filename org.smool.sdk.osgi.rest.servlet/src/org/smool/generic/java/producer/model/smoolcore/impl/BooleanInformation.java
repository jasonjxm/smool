
/*******************************************************************************
* Copyright (c) 2012 Tecnalia Research and Innovation.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
* This file is a result of OWL 2 java transformation using EMF
* Contributors:
*    Enas Ashraf (inas@itida.gov.eg) - creation of level 2 metamodel and transformation to java classes 
*    Adrian Noguero (Tecnalia Research and Innovation - Software Systems Engineering) - reation of level 1 metamodel by creating ...
*******************************************************************************/ 
package org.smool.generic.java.producer.model.smoolcore.impl;
     
import org.smool.kpi.model.smart.AbstractOntConcept;
import org.smool.kpi.model.smart.KPProducer;
import org.smool.kpi.model.smart.KPConsumer;
import org.smool.kpi.model.smart.slots.FunctionalDatatypeSlot;
import org.smool.generic.java.producer.model.smoolcore.IBooleanInformation;

/**
 * This class implements the ontology concept BooleanInformation
 * including all its properties.
 * @author Genrated via EMF OWL to java transformation
 * @version 1.0
 */
public class BooleanInformation extends AbstractOntConcept implements IBooleanInformation, KPProducer, KPConsumer{

    //Not needed.. public static String CLASS_NAMESPACE = "http://com.tecnalia.smool/core/smoolcore#";
  	//Not needed.. public static String CLASS_ID = "BooleanInformation";
  	public static String CLASS_IRI = "http://com.tecnalia.smool/core/smoolcore#BooleanInformation"; 
  		
  		
  	/**
    * The Constructor
    */
    public BooleanInformation() {
    	super();
        init();
	}
    	
    	
	/**
 	* The Constructor
 	* @param id the Actuator identifier
 	*/
	public BooleanInformation(String id) {
      	/** Call superclass to establish the identifier */
      	super(id);
      	init();
	}
    
    	
    	
	/**
 	* Inits the fields associated to a ontology concept
 	*/
	public void init() {
      	/** Sets the context of this ontology concept */
      	this._setClassContext("smoolcore", CLASS_IRI);

      	/** Sets the individual context */
      	this._setDefaultIndividualContext();
    
      
      	// Creates the active property
      	String activeIRI = "http://com.tecnalia.smool/core/smoolcore#active";
      	String activePrefix = "smoolcore";

      	FunctionalDatatypeSlot < Boolean > activeSlot= new FunctionalDatatypeSlot<Boolean>(Boolean.class);
      	activeSlot._setIRI(activeIRI);
      	activeSlot._setPrefix(activePrefix);
      	activeSlot.setRange("xsd:Boolean");
      	this._addProperty(activeSlot);
  	  
  	}
	/*
	* PROPERTIES: GETTERS AND SETTERS
	*/
 	
 	/**
 	* Sets the active property.
 	* @param active Boolean value
 	*/
	public void setActive(Boolean active) {
		this.updateAttribute("active",active);        
	}
		
	 /**
 	* Gets the active property.
 	* @return a Boolean value
 	*/
	public Boolean getActive() {
    	return (Boolean) this._getFunctionalProperty("active").getValue();
	}

}
