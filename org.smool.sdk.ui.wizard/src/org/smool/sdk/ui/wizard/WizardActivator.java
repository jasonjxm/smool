package org.smool.sdk.ui.wizard;

import java.io.File;
import java.net.URL;
import java.util.Hashtable;
import java.util.Vector;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;
import org.smool.sdk.ontmodel.ontmodel.Ontology;
import org.smool.sdk.owlparser.owlmodel.OWLModel;
import org.smool.sdk.ui.wizard.generation.ICommLayerGenerator;
import org.smool.sdk.ui.wizard.generation.IProjectGenerator;

import es.esi.gemde.modeltransformator.ModelTransformatorPlugin;
import es.esi.gemde.modeltransformator.mofscriptengine.MOFScriptEngine;
import es.esi.gemde.modeltransformator.service.IModelTransformationService;
import es.esi.gemde.modeltransformator.service.ITransformation;
import es.esi.gemde.modeltransformator.service.TransformationFactory;

/**
 * The activator class controls the plug-in life cycle
 * 
 * @author Adrian Noguero (adrian.noguero@tecnalia.com)
 */

public class WizardActivator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "org.smool.sdk.ui.wizard"; //$NON-NLS-1$
	public static final String SMOOL_ICON = "icons/smool_logo_small.png"; //$NON-NLS-1$
	public static final String CONCEPT_ICON = "icons/concept.gif"; //$NON-NLS-1$
	public static final String CORE_ONTOLOGY = "smool-core.owl"; //$NON-NLS-1$
	public static final String OWL2ONTMODEL_FILE = "resources/Owl2Ontmodel.m2t"; //$NON-NLS-1$

	public static final String PLUGINS_FOLDER = "/.metadata/.plugins"; //$NON-NLS-1$
	public static final String WP_ONTOLOGY_NAME = "smoolcore.ontmodel"; //$NON-NLS-1$
	public static final String OWL2ONTMODEL = "Owl2OntModel"; //$NON-NLS-1$

	public static final String MOFSCRIPT = "MOFScript"; //$NON-NLS-1$

	public static final String COMMLAYER_EXTENSION_ID = "org.smool.sdk.ui.wizard.commlayer"; //$NON-NLS-1$
	public static final String PROJECT_TYPE_EXTENSION_ID = "org.smool.sdk.ui.wizard.generators"; //$NON-NLS-1$

	public static final String SIB_PLUGIN_ID = "org.smool.sib.osgi"; //$NON-NLS-1$

	// The shared instance
	private static WizardActivator plugin;
	private Ontology loadedOntology = null;
	private ResourceSet rs = new ResourceSetImpl();
	private Hashtable<String, IProjectGenerator> projectGenerators;
	private Hashtable<String, String> projectGeneratorRegistry;
	private Hashtable<String, Hashtable<String, ICommLayerGenerator>> commLayerGenerators;

	/**
	 * The constructor
	 */
	public WizardActivator() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.
	 * BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		projectGenerators = new Hashtable<String, IProjectGenerator>();
		projectGeneratorRegistry = new Hashtable<String, String>();
		commLayerGenerators = new Hashtable<String, Hashtable<String, ICommLayerGenerator>>();
		loadEngine();
		loadTransformations();
		loadExtensions();
	}

	private void loadEngine() {
		IModelTransformationService s = ModelTransformatorPlugin.getService();
		MOFScriptEngine engine = new MOFScriptEngine();
		s.registerEngine(engine);
	}

	private void loadExtensions() {
		// Get the elements contributing in the extension point
		IConfigurationElement[] projectTypes = Platform.getExtensionRegistry()
				.getConfigurationElementsFor(PROJECT_TYPE_EXTENSION_ID);

		for (IConfigurationElement pt : projectTypes) {
			try {
				String id = pt.getAttribute("id");
				String name = pt.getAttribute("name");
				Object genClass = pt.createExecutableExtension("generator_class");
				IProjectGenerator prGen = null;
				if (genClass != null && genClass instanceof IProjectGenerator) {
					prGen = (IProjectGenerator) genClass;
				}

				if (prGen != null) {
					projectGeneratorRegistry.put(id, name);
					projectGenerators.put(name, prGen);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// Get the elements contributing in the extension point
		IConfigurationElement[] commLayers = Platform.getExtensionRegistry()
				.getConfigurationElementsFor(COMMLAYER_EXTENSION_ID);

		for (IConfigurationElement cl : commLayers) {
			try {
				String name = cl.getAttribute("name");
				String projectID = cl.getAttribute("project_id");
				String prGenName = projectGeneratorRegistry.get(projectID);
				Object obj = cl.createExecutableExtension("generator_class");
				ICommLayerGenerator commLayerGen = null;
				if (obj != null && obj instanceof ICommLayerGenerator) {
					commLayerGen = (ICommLayerGenerator) obj;
				}

				if (prGenName != null && commLayerGen != null) {
					Hashtable<String, ICommLayerGenerator> gens = commLayerGenerators.get(prGenName);
					if (gens == null) {
						gens = new Hashtable<String, ICommLayerGenerator>();
					}
					gens.put(name, commLayerGen);
					commLayerGenerators.put(prGenName, gens);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	/**
	 * Load transformations into GEMDE
	 */
	private void loadTransformations() {
		try {
			IModelTransformationService s = ModelTransformatorPlugin.getService();
			Vector<Class<? extends EObject>> inputs1 = new Vector<Class<? extends EObject>>();
			Vector<java.net.URI> transformationURIs1 = new Vector<java.net.URI>();

			URL file1 = FileLocator.find(getBundle(), new Path(OWL2ONTMODEL_FILE), null);
			file1 = FileLocator.toFileURL(file1);

			transformationURIs1.add(file1.toURI());
			inputs1.add(OWLModel.class);

			ITransformation t1;
			t1 = TransformationFactory.createTransformation(OWL2ONTMODEL, MOFSCRIPT, transformationURIs1, "", inputs1);
			s.addTransformation(t1);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static WizardActivator getDefault() {
		return plugin;
	}

	private IPath getTempFolder() {
		IPath tempFolderLocation = ResourcesPlugin.getWorkspace().getRoot().getLocation()
				.append(PLUGINS_FOLDER + "/" + PLUGIN_ID);
		System.out.println(tempFolderLocation.toFile().getAbsolutePath());
		if (!tempFolderLocation.toFile().exists()) {
			tempFolderLocation.toFile().mkdir();
		}
		return tempFolderLocation;
	}

	private boolean hasWorkspaceOntology() {
		boolean found = false;

		for (File f : getTempFolder().toFile().listFiles()) {
			if (f.getName().equals(WP_ONTOLOGY_NAME)) {
				found = true;
				break;
			}
		}

		return found;
	}

	/**
	 * Returns the most updated Ontology available. If a workspace ontology is
	 * available, then that ontology is loaded and returned; else, the default
	 * ontology is loaded and returned.
	 * 
	 * @return the Ontology model element loaded.
	 */
	public Ontology getOntology() {
		// ARF 23-09-2020 if the ontology is in the workspace, that one will be loaded,
		// otherwise return to the former ontology load.
		String owlFileAddr = ResourcesPlugin.getWorkspace().getRoot().getLocation().toOSString() + File.separator
				+ CORE_ONTOLOGY;
		File f = new File(owlFileAddr);
		if (f.exists()) {
			loadOWLOntology(owlFileAddr);
		} else {
			MessageDialog.openWarning(null, "SMOOL ontology file",
					"The ontology file 'smool-core.owl' is recomended to be downloaded from SMOOL repo and stored in  $workspace. This allows to use the latest ontology updates,or customize the ontology with ad-hoc concepts. Expected location at "
							+ owlFileAddr);
			if (hasWorkspaceOntology()) {
				loadWorkspaceOntology();
			} else {
				loadDefaultOntology();
			}
		}
		return loadedOntology;

	}

	/**
	 * Get the loaded project generation types.
	 * 
	 * @return an array of String containing the names of the project generation
	 *         types.
	 */
	public String[] getProjectTypes() {
		if (projectGenerators == null) {
			return new String[0];
		} else {
			return projectGenerators.keySet().toArray(new String[0]);
		}
	}

	/**
	 * Get the communication layers available for a certain project generation type.
	 * 
	 * @param prType. The name of the project generation type.
	 * @return an array of String containing the names of the communication layer
	 *         generation types available for the selected project generation type.
	 */
	public String[] getCommLayers(String prType) {
		Hashtable<String, ICommLayerGenerator> commLayers = commLayerGenerators.get(prType);
		if (commLayers == null) {
			return new String[0];
		} else {
			return commLayers.keySet().toArray(new String[0]);
		}
	}

	/**
	 * Get the {@link IProjectGenerator} instance associated to the selected project
	 * generator name.
	 * 
	 * @param prGenName. The name of the selected project generator.
	 * @return the {@link IProjectGenerator} instance or null if none is registered.
	 */
	public IProjectGenerator getProjectGenerator(String prGenName) {
		return projectGenerators.get(prGenName);
	}

	/**
	 * Get the {@link ICommLayerGenerator} instance associated to a project
	 * generator name and a communication layer generator name.
	 * 
	 * @param prGenNam. The name of the selected project generator.
	 * @param commLayerName. The name of the selected communications layer
	 *        generator.
	 * @return the {@link ICommLayerGenerator} instance or null is none is
	 *         registered.
	 */
	public ICommLayerGenerator getCommLayerGenerator(String prGenNam, String commLayerName) {
		Hashtable<String, ICommLayerGenerator> commLayers = commLayerGenerators.get(prGenNam);
		if (commLayers != null) {
			return commLayers.get(commLayerName);
		} else {
			return null;
		}
	}

	private void loadDefaultOntology() {
		URI uri = URI.createPlatformPluginURI(SIB_PLUGIN_ID + "/" + CORE_ONTOLOGY, true);
		loadOWLOntology(uri.toFileString());
	}

	private void loadOWLOntology(String fileAddr) {
		URI uri = URI.createFileURI(fileAddr);
		Resource res = rs.getResource(uri, true);
		OWLModel model = null;
		TreeIterator<EObject> i = res.getAllContents();
		while (i.hasNext() && model == null) {
			EObject o = i.next();
			if (o instanceof OWLModel) {
				model = (OWLModel) o;
			}
		}

		if (model != null) {

			// Prepare transformation
			Vector<EObject> inputs = new Vector<EObject>();
			inputs.add(model);

			// Launch the transformation
			try {
				ModelTransformatorPlugin.getService().executeTransformation(inputs, OWL2ONTMODEL, MOFSCRIPT,
						getTempFolder().toFile().getAbsolutePath());
			} catch (Exception e) {
				e.printStackTrace();
			}

			IPath ontModelPath = getTempFolder().append("/" + WP_ONTOLOGY_NAME);
			Resource ontRes = rs.getResource(URI.createFileURI(ontModelPath.toFile().getAbsolutePath()), true);

			TreeIterator<EObject> i2 = ontRes.getAllContents();
			while (i2.hasNext() && loadedOntology == null) {
				EObject o = i2.next();
				if (o instanceof Ontology) {
					loadedOntology = (Ontology) o;
				}
			}
		}
	}

	private void loadWorkspaceOntology() {
		IPath ontModelPath = getTempFolder().append("/" + WP_ONTOLOGY_NAME);
		Resource ontRes = rs.getResource(URI.createFileURI(ontModelPath.toFile().getAbsolutePath()), true);

		TreeIterator<EObject> i2 = ontRes.getAllContents();
		while (i2.hasNext() && loadedOntology == null) {
			EObject o = i2.next();
			if (o instanceof Ontology) {
				loadedOntology = (Ontology) o;
			}
		}
	}

}
