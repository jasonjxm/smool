/**
 * 
 */
package org.smool.sdk.ui.wizard.generation;

import org.smool.sdk.ontmodel.ontmodel.Ontology;
import org.smool.sdk.ui.wizard.data.KPProjectData;
import org.smool.sdk.ui.wizard.exceptions.SmoolGenerationException;

/**
 * Interface of the project generation engines. It must be implemented by
 * contributing plugins.
 * 
 * @author Adrian Noguero (adrian.noguero@tecnalia.com)
 *
 */
public interface IProjectGenerator {

	/**
	 * Generate the project infraestructure in the workspace according to a specific
	 * project name.
	 * 
	 * @param projectName. The name of the project to be generated.
	 * @throws SmoolGenerationException. If an error occurs during the project
	 *         generation process.
	 */
	public void generateProject(String projectName) throws SmoolGenerationException;

	/**
	 * Generate a model layer compatible with the selected project type.
	 * 
	 * @param ontModel. The {@link Ontology} model including the produced and
	 *        consumed concepts.
	 * @throws SmoolGenerationException. If an error occurs during the model layer
	 *         generation process.
	 */
	public void generateModelLayer(Ontology ontModel) throws SmoolGenerationException;

	/**
	 * Generate code from third party providers
	 */
	public void generateExternalAddons(KPProjectData projectData, Ontology ontModel) throws Exception;

}
