/**
 * 
 */
package org.smool.sdk.ui.wizard.generation;

import org.smool.sdk.ui.wizard.exceptions.SmoolGenerationException;

/**
 * @author Adrian Noguero (adrian.noguero@tecnalia.com)
 *
 */
public interface ICommLayerGenerator {

	public void generateCommLayer(boolean autoConnect) throws SmoolGenerationException;
	
	public void generateCommLayer(boolean autoConnect, String address, String port) throws SmoolGenerationException;
	
}
