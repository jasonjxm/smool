package org.smool.sdk.ui.wizard.data;

import java.util.Vector;

import org.smool.sdk.ontmodel.ontmodel.Class;

public class KPProjectData {

	public String projectName = new String();
	public String projectType = new String();
	
	public Vector<Class> producedConcepts = new Vector<Class>();
	public Vector<Class> consumedConcepts = new Vector<Class>();
	
	public String commLayer = new String();
	
	public boolean autoconnect = true;
	public boolean autodetectSIB = true;
	
	public String sibName = new String();
	public String sibAddr = new String();
	public String sibPort = new String();
	
}
