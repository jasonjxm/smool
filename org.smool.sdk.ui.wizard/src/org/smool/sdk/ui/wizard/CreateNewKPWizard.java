/**
 * 
 */
package org.smool.sdk.ui.wizard;

import java.io.ByteArrayInputStream;
import java.util.Vector;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTreeViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.smool.sdk.ontmodel.ontmodel.Class;
import org.smool.sdk.ontmodel.ontmodel.GenerationTypes;
import org.smool.sdk.ontmodel.ontmodel.OntmodelFactory;
import org.smool.sdk.ontmodel.ontmodel.Ontology;
import org.smool.sdk.ontmodel.ontmodel.ProjectInfo;
import org.smool.sdk.ontmodel.ontmodel.Relationship;
import org.smool.sdk.ontmodel.ontmodel.RelationshipTypes;
import org.smool.sdk.ui.wizard.data.KPProjectData;
import org.smool.sdk.ui.wizard.exceptions.SmoolGenerationException;
import org.smool.sdk.ui.wizard.generation.ICommLayerGenerator;
import org.smool.sdk.ui.wizard.generation.IProjectGenerator;

/**
 * @author Adrian Noguero (adrian.noguero@tecnalia.com)
 *
 */
public class CreateNewKPWizard extends Wizard implements INewWizard {

	// Constants
	private final String DATA_URI = "http://com.tecnalia.smool/core/smoolcore#Data";
	private final String NATURE_URI = "http://com.tecnalia.smool/core/smoolcore#Nature";

	// Properties
	private boolean canFinish;
	private KPProjectData data;
	private Ontology ontModel;
	private CheckboxTreeViewer pConceptsTree;
	private CheckboxTreeViewer cConceptsTree;

	public CreateNewKPWizard() {
		super();
		canFinish = false;
		data = new KPProjectData();
		ImageDescriptor id = WizardActivator.imageDescriptorFromPlugin(WizardActivator.PLUGIN_ID,
				WizardActivator.SMOOL_ICON);
		setDefaultPageImageDescriptor(id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IWorkbenchWizard#init(org.eclipse.ui.IWorkbench,
	 * org.eclipse.jface.viewers.IStructuredSelection)
	 */
	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		// Nothing to be done
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish() {
		// Update the ontology model according to the selections
		ProjectInfo info = OntmodelFactory.eINSTANCE.createProjectInfo();
		info.setProjectName(data.projectName);
		info.setAutoDetect(data.autodetectSIB);
		if (!data.autodetectSIB) {
			info.setSIBName(data.sibName);
			info.setIpAddress(data.sibAddr);
			info.setPort(data.sibPort);
		}
		ontModel.setProjectInfo(info);
		for (Class c : ontModel.getClasses()) {
			c.setGenerationType(GenerationTypes.NONE);
		}

		for (Class c : data.producedConcepts) {
			c.setGenerationType(GenerationTypes.PRODUCER);
			/*
			 * for (Attribute a : c.getAttributes()) { if
			 * (a.getType().equals(AttributeTypes.FUNCTIONAL_OBJECT)) { Class c2 =
			 * ontModel.getClassByName(a.getRange()); if (c2 != null) {
			 * c2.recursivelySetGenerationType(GenerationTypes.PRODUCER); } } }
			 */
		}

		for (Class c : data.consumedConcepts) {
			c.setGenerationType(GenerationTypes.CONSUMER);

			/*
			 * for (Attribute a : c.getAttributes()) { if
			 * (a.getType().equals(AttributeTypes.FUNCTIONAL_OBJECT)) { Class c2 =
			 * ontModel.getClassByName(a.getRange()); if (c2 != null) {
			 * c2.recursivelySetGenerationType(GenerationTypes.CONSUMER); } } }
			 */
		}

		IProjectGenerator projectGen = WizardActivator.getDefault().getProjectGenerator(data.projectType);
		ICommLayerGenerator commLayerGen = WizardActivator.getDefault().getCommLayerGenerator(data.projectType,
				data.commLayer);
		if (projectGen == null || commLayerGen == null) {
			MessageDialog.openError(getShell(), "Couldn't load generators",
					"The new project couldn't be generated because the selected generators couldn't be loaded.");
			return false;
		}

		try {
			projectGen.generateProject(data.projectName);
		} catch (SmoolGenerationException e) {
			e.printStackTrace();
			MessageDialog.openError(getShell(), "Project generation raised an exception",
					"The project couldn't be generated because an exception has been raised: " + e.getMessage());
			return false;
		}

		try {
			projectGen.generateModelLayer(ontModel);

		} catch (SmoolGenerationException e) {
			e.printStackTrace();
			MessageDialog.openError(getShell(), "Model layer generation raised an exception",
					"The model layer couldn't be generated because an exception has been raised: " + e.getMessage());
			return false;
		}

		// use third party tools
		try {
			projectGen.generateExternalAddons(data, ontModel);
		} catch (Exception e1) {
			e1.printStackTrace();
			MessageDialog.openError(getShell(), "Third party extensions failed ", e1.getMessage());
			return false;
		}

		try {
			if ((data.autoconnect && data.autodetectSIB) || !data.autoconnect) {
				commLayerGen.generateCommLayer(data.autoconnect);
			} else {
				commLayerGen.generateCommLayer(data.autoconnect, data.sibAddr, data.sibPort);
			}
		} catch (SmoolGenerationException e) {
			e.printStackTrace();
			MessageDialog.openError(getShell(), "Model layer generation raised an exception",
					"The model layer couldn't be generated because an exception has been raised: " + e.getMessage());
			return false;
		}

		// ARF: 24/07/19 create project template for future regenerations
		createTemplate(data.projectName, data.producedConcepts, data.consumedConcepts);

		// Refresh workspace
		try {
			ResourcesPlugin.getWorkspace().getRoot().refreshLocal(IResource.DEPTH_INFINITE, null);
		} catch (CoreException e) {
			e.printStackTrace();
			// Just ignore this
		}

		return true;
	}

	/**
	 * Create a .template file with the KP concepts to be produced and consumed
	 * 
	 * @param projectName
	 * @param producedConcepts
	 * @param consumedConcepts
	 */
	private void createTemplate(String projectName, Vector<Class> producedConcepts, Vector<Class> consumedConcepts) {
		// project name
		StringBuilder sb = new StringBuilder();
		sb.append("name=" + projectName);

		// produced concepts
		StringBuilder sb1 = new StringBuilder();
		for (Class c : producedConcepts) {
			sb1.append(c.getURI().substring(c.getURI().indexOf("#") + 1));
			sb1.append(',');
		}
		if (sb1.length() > 0)
			sb1.deleteCharAt(sb1.length() - 1);
		sb.append('\n' + "produce=" + sb1.toString());

		// consumed concepts
		sb1 = new StringBuilder();
		for (Class c : consumedConcepts) {
			sb1.append(c.getURI().substring(c.getURI().indexOf("#") + 1));
			sb1.append(',');
		}
		if (sb1.length() > 0)
			sb1.deleteCharAt(sb1.length() - 1);
		sb.append('\n' + "consume=" + sb1);

		// add trailing \n (in case someone edits and save file, a trailing newline is
		// usually added)
		sb.append('\n');

		// save template file
		saveTemplate(projectName, sb.toString());
	}

	private void saveTemplate(String projectName, String content) {
		String fileName = projectName + ".template";
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		IProject project = root.getProject(projectName);
		IFile ifile = project.getFile(fileName);
		try {
			ifile.create(new ByteArrayInputStream(content.getBytes()), true, null);
			System.out.println(fileName + "  file created");
		} catch (Exception e) {
			System.out.println("WARNING:" + fileName
					+ "  file created. This is not a problem for the current KP, but the file is important when automatic regeneration of KP is required in the future");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.Wizard#canFinish()
	 */
	@Override
	public boolean canFinish() {
		return canFinish;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.Wizard#addPages()
	 */
	@Override
	public void addPages() {
		super.addPages();
		addPage(new NewKpProjectWizardPage());
	}

	private class NewKpProjectWizardPage extends WizardPage {

		private Combo typeCombo;
		private Combo commLayerCombo;
		private Text sibNameText;
		private Text sibAddressText;
		private Text sibPortText;
		private Button autoDiscoveryButton;

		public NewKpProjectWizardPage() {
			super("NewKpProjectWizardPage");

		}

		private void updatePageStatus() {
			boolean projectOk = !data.projectName.equals("")
					&& !ResourcesPlugin.getWorkspace().getRoot().getProject(data.projectName).exists()
					&& !data.projectType.equals("");
			boolean conceptsOk = (data.producedConcepts.size() > 0 || data.consumedConcepts.size() > 0)
					&& ontModel != null;
			boolean optionsOk = !data.autoconnect || (data.autoconnect && data.autodetectSIB)
					|| (data.autoconnect && addressOk(data.sibAddr, data.sibPort) && !data.sibName.equals(""));
			canFinish = projectOk && conceptsOk && optionsOk;
			setPageComplete(true);
		}

		private boolean addressOk(String addr, String port) {
			String[] parts = addr.split("\\.");

			if (parts.length != 4) {
				return false;
			}

			boolean first = true;
			try {
				for (String s : parts) {

					int i = Integer.parseInt(s);
					if (first) {
						if ((i <= 0) || (i >= 255)) {
							return false;
						}
					} else {
						if ((i < 0) || (i > 255)) {
							return false;
						}
					}
					first = false;
				}
			} catch (Exception e) {
				return false;
			}

			int portno = Integer.parseInt(port);
			if (portno <= 0 || portno > 65536) {
				return false;
			}

			return true;
		}

		private void loadData() {
			// Load data
			for (String s : WizardActivator.getDefault().getProjectTypes()) {
				typeCombo.add(s);
			}
			typeCombo.select(0);
			updateTypeOfProject(typeCombo);
			ontModel = WizardActivator.getDefault().getOntology();
			pConceptsTree.getTree().clearAll(true);
			cConceptsTree.getTree().clearAll(true);

			ImageDescriptor id = WizardActivator.imageDescriptorFromPlugin(WizardActivator.PLUGIN_ID,
					WizardActivator.CONCEPT_ICON);
			Image img = id.createImage();

			if (ontModel != null) {

				for (Class c : ontModel.getClasses()) {

					if (c.getRelationships().isEmpty() && !c.getURI().equals(DATA_URI)
							&& !c.getURI().equals(NATURE_URI)) {

						TreeItem pi = new TreeItem(pConceptsTree.getTree(), SWT.NONE);
						TreeItem ci = new TreeItem(cConceptsTree.getTree(), SWT.NONE);

						pi.setData(c);
						ci.setData(c);

						String classURI = c.getURI();
						pi.setText(classURI.substring(classURI.lastIndexOf('#') + 1));
						ci.setText(classURI.substring(classURI.lastIndexOf('#') + 1));

						pi.setImage(img);
						ci.setImage(img);

						createSubconcepts(pi, c, ontModel, img);
						createSubconcepts(ci, c, ontModel, img);

						expandAll(pi);
						expandAll(ci);
					}

				}

				pConceptsTree.getTree().pack();
				cConceptsTree.getTree().pack();

			} else {
				MessageDialog.openError(getShell(), "Error loading ontology concepts",
						"SMOOL couldn't load any ontologies. This might have been caused by a corruption of the installation.");
			}
		}

		private void expandAll(TreeItem parent) {
			parent.setExpanded(true);
			for (TreeItem child : parent.getItems())
				expandAll(child);
		}

		private void updateTypeOfProject(Combo c) {
			if (!data.projectType.equals(c.getItem(c.getSelectionIndex()))) {
				data.projectType = c.getItem(c.getSelectionIndex());
				commLayerCombo.removeAll();
				for (String s : WizardActivator.getDefault().getCommLayers(data.projectType)) {
					commLayerCombo.add(s);
				}
				commLayerCombo.select(0);
				data.commLayer = commLayerCombo.getItem(0);
				updatePageStatus();
			}
		}

		private void createSubconcepts(TreeItem parent, Class c, Ontology ont, Image img) {
			for (Class c2 : ont.getClasses()) {
				if (isSubclassOf(c2, c)) {
					TreeItem item = new TreeItem(parent, SWT.NONE);
					item.setData(c2);
					String classURI = c2.getURI();
					item.setText(classURI.substring(classURI.lastIndexOf('#') + 1));
					item.setImage(img);
					createSubconcepts(item, c2, ont, img);
				}
			}

		}

		private boolean isSubclassOf(Class subClass, Class parent) {
			for (Relationship r : subClass.getRelationships()) {
				String parentName = parent.getURI().substring(parent.getURI().lastIndexOf('#') + 1);
				if (r.getType() == RelationshipTypes.SUB_CLASS_OF && r.getArg().equals(parentName)) {
					return true;
				}
			}
			return false;
		}

		private TreeItem[] getAllItems(Tree tree) {
			Vector<TreeItem> items = new Vector<TreeItem>();

			for (TreeItem i : tree.getItems()) {
				items.add(i);
				addSubitems(i, items);
			}

			return items.toArray(new TreeItem[0]);
		}

		private void addSubitems(TreeItem i, Vector<TreeItem> items) {
			for (TreeItem i2 : i.getItems()) {
				items.add(i2);
				addSubitems(i2, items);
			}
		}

		private void markIntermediateProducedItems(CheckboxTreeViewer tree) {
			tree.setCheckedElements(new Object[0]);
			Vector<TreeItem> items = new Vector<TreeItem>();
			for (Class c : data.producedConcepts) {
				for (TreeItem i : getAllItems(tree.getTree())) {
					if (i.getData().equals(c)) {
						items.add(i);
					}
				}
			}
			for (TreeItem i : items.toArray(new TreeItem[0])) {
				i.setChecked(true);
				TreeItem parent = i.getParentItem();
				while (parent != null) {
					parent.setChecked(true);
					parent = parent.getParentItem();
				}
			}

		}

		private void markIntermediateConsumedItems(CheckboxTreeViewer tree) {
			tree.setCheckedElements(new Object[0]);
			Vector<TreeItem> items = new Vector<TreeItem>();
			for (Class c : data.consumedConcepts) {
				for (TreeItem i : getAllItems(tree.getTree())) {
					if (i.getData().equals(c)) {
						items.add(i);
					}
				}
			}
			for (TreeItem i : items.toArray(new TreeItem[0])) {
				i.setChecked(true);
				TreeItem parent = i.getParentItem();
				while (parent != null) {
					parent.setChecked(true);
					parent = parent.getParentItem();
				}
			}

		}

		@Override
		public void createControl(Composite parent) {
			Composite topPanel = new Composite(parent, SWT.NONE);
			GridLayout layout = new GridLayout(6, true);
			topPanel.setLayout(layout);

			// Create groups to organize the UI
			Group projectGroup = new Group(topPanel, SWT.SHADOW_NONE);
			GridData projectGroupData = new GridData(GridData.FILL_HORIZONTAL);
			projectGroupData.horizontalSpan = 6;
			projectGroup.setLayoutData(projectGroupData);
			projectGroup.setText("Project Information");

			Group modelGroup = new Group(topPanel, SWT.SHADOW_NONE);
			GridData modelGroupData = new GridData(GridData.FILL_HORIZONTAL);
			modelGroupData.horizontalSpan = 6;
			modelGroup.setLayoutData(modelGroupData);
			modelGroup.setText("Shared Concepts");

			Group optionsGroup = new Group(topPanel, SWT.SHADOW_NONE);
			GridData optionsGroupData = new GridData(GridData.FILL_HORIZONTAL);
			optionsGroupData.horizontalSpan = 6;
			optionsGroup.setLayoutData(optionsGroupData);
			optionsGroup.setText("Generation Options");

			// Create new GridLayouts per group
			projectGroup.setLayout(new GridLayout(6, true));
			modelGroup.setLayout(new GridLayout(6, true));
			optionsGroup.setLayout(new GridLayout(6, true));

			// Add elements to Project Group
			Label nameLabel = new Label(projectGroup, SWT.NONE);
			GridData nameLabelData = new GridData(GridData.FILL_HORIZONTAL);
			nameLabelData.horizontalSpan = 2;
			nameLabel.setText("Project Name:");
			nameLabel.setLayoutData(nameLabelData);

			Text nameText = new Text(projectGroup, SWT.SINGLE | SWT.BORDER);
			GridData nameTextData = new GridData(GridData.FILL_HORIZONTAL);
			nameTextData.horizontalSpan = 4;
			nameText.setText("");
			nameText.setLayoutData(nameTextData);

			Label typeLabel = new Label(projectGroup, SWT.NONE);
			GridData typeLabelData = new GridData(GridData.FILL_HORIZONTAL);
			typeLabelData.horizontalSpan = 2;
			typeLabel.setText("Project Type:");
			typeLabel.setLayoutData(typeLabelData);

			typeCombo = new Combo(projectGroup, SWT.BORDER | SWT.DROP_DOWN | SWT.READ_ONLY);
			GridData typeComboData = new GridData(GridData.FILL_HORIZONTAL);
			typeComboData.horizontalSpan = 4;
			typeCombo.setLayoutData(typeComboData);

			// Add elements to Model Group
			Label pConceptsLabel = new Label(modelGroup, SWT.NONE);
			GridData pConceptsLabelData = new GridData(GridData.FILL_HORIZONTAL);
			pConceptsLabelData.horizontalSpan = 3;
			pConceptsLabel.setText("Produced Concepts:");
			pConceptsLabel.setLayoutData(pConceptsLabelData);

			Label cConceptsLabel = new Label(modelGroup, SWT.NONE);
			GridData cConceptsLabelData = new GridData(GridData.FILL_HORIZONTAL);
			cConceptsLabelData.horizontalSpan = 3;
			cConceptsLabel.setText("Consumed Concepts:");
			cConceptsLabel.setLayoutData(cConceptsLabelData);

			pConceptsTree = new CheckboxTreeViewer(modelGroup, SWT.V_SCROLL | SWT.H_SCROLL | SWT.BORDER);
			GridData pConceptsTreeData = new GridData(GridData.FILL_BOTH);
			pConceptsTreeData.horizontalSpan = 3;
			pConceptsTreeData.grabExcessVerticalSpace = true;
			pConceptsTreeData.minimumHeight = 200;
			pConceptsTreeData.heightHint = 300;
			pConceptsTree.getTree().setLayoutData(pConceptsTreeData);

			cConceptsTree = new CheckboxTreeViewer(modelGroup, SWT.V_SCROLL | SWT.H_SCROLL | SWT.BORDER);
			GridData cConceptsTreeData = new GridData(GridData.FILL_BOTH);
			cConceptsTreeData.horizontalSpan = 3;
			cConceptsTreeData.grabExcessVerticalSpace = true;
			cConceptsTreeData.minimumHeight = 200;
			cConceptsTreeData.heightHint = 300;
			cConceptsTree.getTree().setLayoutData(cConceptsTreeData);

			Label empty1Label = new Label(modelGroup, SWT.NONE);
			GridData empty1LabelData = new GridData(GridData.FILL_HORIZONTAL);
			empty1LabelData.horizontalSpan = 5;
			empty1Label.setText("");
			empty1Label.setLayoutData(empty1LabelData);

			Label empty2Label = new Label(modelGroup, SWT.NONE);
			GridData empty2LabelData = new GridData(GridData.FILL_HORIZONTAL);
			empty2LabelData.horizontalSpan = 5;
			empty2Label.setText("");
			empty2Label.setLayoutData(empty2LabelData);

			// Add elements to Options Group
			Label commLayerLabel = new Label(optionsGroup, SWT.NONE);
			GridData commLayerLabelData = new GridData(GridData.FILL_HORIZONTAL);
			commLayerLabelData.horizontalSpan = 2;
			commLayerLabel.setText("Select the communications layer:");
			commLayerLabel.setLayoutData(commLayerLabelData);

			commLayerCombo = new Combo(optionsGroup, SWT.BORDER | SWT.DROP_DOWN | SWT.READ_ONLY);
			GridData commLayerComboData = new GridData();
			commLayerComboData.horizontalSpan = 4;
			commLayerComboData.minimumWidth = 300;
			commLayerComboData.widthHint = 300;
			commLayerCombo.setLayoutData(commLayerComboData);

//			Button autoConnectButton = new Button(optionsGroup, SWT.CHECK);
//			GridData autoConnectButtonData = new GridData(GridData.FILL_HORIZONTAL);
//			autoConnectButtonData.horizontalSpan = 6;
//			autoConnectButton.setText("Connect to SIB on Start-Up");
//			autoConnectButton.setLayoutData(autoConnectButtonData);
//			autoConnectButton.setSelection(true);

			Label sibNameLabel = new Label(optionsGroup, SWT.NONE);
			GridData sibNameLabelData = new GridData(GridData.FILL_HORIZONTAL);
			sibNameLabelData.horizontalSpan = 1;
			sibNameLabel.setText("SIB Name:");
			sibNameLabel.setLayoutData(sibNameLabelData);

			sibNameText = new Text(optionsGroup, SWT.SINGLE | SWT.BORDER);
			GridData sibNameTextData = new GridData(GridData.FILL_HORIZONTAL);
			sibNameTextData.horizontalSpan = 1;
			sibNameText.setText("");
			sibNameText.setLayoutData(sibNameTextData);

			Label sibAddressLabel = new Label(optionsGroup, SWT.NONE);
			GridData sibAddressLabelData = new GridData(GridData.FILL_HORIZONTAL);
			sibAddressLabelData.horizontalSpan = 1;
			sibAddressLabel.setText("SIB IP address:");
			sibAddressLabel.setLayoutData(sibAddressLabelData);

			sibAddressText = new Text(optionsGroup, SWT.SINGLE | SWT.BORDER);
			GridData sibAddressTextData = new GridData(GridData.FILL_HORIZONTAL);
			sibAddressTextData.horizontalSpan = 1;
			sibAddressText.setText("");
			sibAddressText.setLayoutData(sibAddressTextData);

			Label sibPortLabel = new Label(optionsGroup, SWT.NONE);
			GridData sibPortLabelData = new GridData(GridData.FILL_HORIZONTAL);
			sibPortLabelData.horizontalSpan = 1;
			sibPortLabel.setText("SIB Port:");
			sibPortLabel.setLayoutData(sibPortLabelData);

			sibPortText = new Text(optionsGroup, SWT.SINGLE | SWT.BORDER);
			GridData sibPortTextData = new GridData(GridData.FILL_HORIZONTAL);
			sibPortTextData.horizontalSpan = 1;
			sibPortText.setText("");
			sibPortText.setLayoutData(sibPortTextData);

			Label empty3Label = new Label(optionsGroup, SWT.NONE);
			GridData empty3LabelData = new GridData(GridData.FILL_HORIZONTAL);
			empty3LabelData.horizontalSpan = 5;
			empty3Label.setText("");
			empty3Label.setLayoutData(empty3LabelData);

			autoDiscoveryButton = new Button(optionsGroup, SWT.CHECK);
			GridData autoDiscoveryButtonData = new GridData(GridData.FILL_HORIZONTAL);
			autoDiscoveryButtonData.horizontalSpan = 1;
			autoDiscoveryButton.setText("Automatically discover SIB");
			autoDiscoveryButton.setLayoutData(autoDiscoveryButtonData);
			autoDiscoveryButton.setSelection(true);

			// Set initial disabled elements
			sibNameText.setEnabled(false);
			sibAddressText.setEnabled(false);
			sibPortText.setEnabled(false);

			// Load Ontology model data
			loadData();
			pConceptsTree.setAutoExpandLevel(CheckboxTreeViewer.ALL_LEVELS);
			cConceptsTree.setAutoExpandLevel(CheckboxTreeViewer.ALL_LEVELS);

			// Add listeners
			nameText.addModifyListener(new ModifyListener() {

				@Override
				public void modifyText(ModifyEvent e) {
					data.projectName = ((Text) e.widget).getText();
					updatePageStatus();
				}
			});

			sibNameText.addModifyListener(new ModifyListener() {

				@Override
				public void modifyText(ModifyEvent e) {
					data.sibName = ((Text) e.widget).getText();
					updatePageStatus();
				}

			});

			sibAddressText.addModifyListener(new ModifyListener() {

				@Override
				public void modifyText(ModifyEvent e) {
					data.sibAddr = ((Text) e.widget).getText();
					updatePageStatus();
				}

			});

			sibPortText.addModifyListener(new ModifyListener() {

				@Override
				public void modifyText(ModifyEvent e) {
					data.sibPort = ((Text) e.widget).getText();
					updatePageStatus();
				}
			});

//			autoConnectButton.addSelectionListener(new SelectionListener() {
//				
//				@Override
//				public void widgetSelected(SelectionEvent e) {
//					data.autoconnect = ((Button)e.widget).getSelection();
//					sibNameText.setEnabled(data.autoconnect && !data.autodetectSIB);
//					sibAddressText.setEnabled(data.autoconnect && !data.autodetectSIB);
//					sibPortText.setEnabled(data.autoconnect && !data.autodetectSIB);
//					autoDiscoveryButton.setEnabled(data.autoconnect);
//					updatePageStatus();
//				}
//				
//				@Override
//				public void widgetDefaultSelected(SelectionEvent e) {}
//			});

			autoDiscoveryButton.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					data.autodetectSIB = ((Button) e.widget).getSelection();
					sibNameText.setEnabled(data.autoconnect && !data.autodetectSIB);
					sibAddressText.setEnabled(data.autoconnect && !data.autodetectSIB);
					sibPortText.setEnabled(data.autoconnect && !data.autodetectSIB);
					updatePageStatus();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			typeCombo.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					updateTypeOfProject((Combo) e.widget);
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			commLayerCombo.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					Combo c = (Combo) e.widget;
					data.commLayer = c.getItem(c.getSelectionIndex());
					updatePageStatus();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			pConceptsTree.addCheckStateListener(new ICheckStateListener() {

				@Override
				public void checkStateChanged(CheckStateChangedEvent e) {
					Class c = (Class) e.getElement();
					TreeItem i = null;
					for (TreeItem i2 : getAllItems(((CheckboxTreeViewer) e.getSource()).getTree())) {
						if (i2.getData().equals(c)) {
							i = i2;
						}
					}

					if (i == null) {
						// Avoid unexpected NullPointers
						return;
					}

					if (i.getItemCount() != 0) {
						i.setChecked(!i.getChecked());
					} else {
						if (i.getChecked()) {
							data.producedConcepts.add(c);
						} else {
							data.producedConcepts.remove(c);
						}
						markIntermediateProducedItems((CheckboxTreeViewer) e.getSource());
					}
					updatePageStatus();
				}

			});

			cConceptsTree.addCheckStateListener(new ICheckStateListener() {

				@Override
				public void checkStateChanged(CheckStateChangedEvent e) {
					Class c = (Class) e.getElement();
					TreeItem i = null;
					for (TreeItem i2 : getAllItems(((CheckboxTreeViewer) e.getSource()).getTree())) {
						if (i2.getData().equals(c)) {
							i = i2;
						}
					}

					if (i == null) {
						// Avoid unexpected NullPointers
						return;
					}

					if (i.getItemCount() != 0) {
						i.setChecked(!i.getChecked());
					} else {
						if (i.getChecked()) {
							data.consumedConcepts.add(c);
						} else {
							data.consumedConcepts.remove(c);
						}
						markIntermediateConsumedItems((CheckboxTreeViewer) e.getSource());
					}

					updatePageStatus();
				}

			});

			// Set the title & description
			setTitle("Create a new KP project");
			setDescription("Set the properties of the new KP project.");

			// Set the control!!
			setControl(topPanel);

			setPageComplete(true);
		}

	}

}
