package org.smool.sdk.ui.wizard.exceptions;

/**
 * 
 * Wrapper class for exceptions raised during the project/comm layer generation processes.
 * 
 * @author Adrian Noguero (adrian.noguero@tecnalia.com)
 *
 */
public class SmoolGenerationException extends Exception {

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -885296847013899417L;

	public SmoolGenerationException(String message, Exception ex) {
		super(message, ex);
	}

}
