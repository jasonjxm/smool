package org.smool.sdk.ui.wizard.utils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public abstract class SmoolGenerationUtils {

	/**
	 * An input stream into an output stream.
	 * 
	 * Useful to copy files (e.g. libs, config files...) in the project generation process.
	 * 
	 * @param is. An {@link InputStream} to read from.
	 * @param os. An {@link OutputStream} to write to.
	 * @throws IOException
	 */
	public static void streamCopy(InputStream is, OutputStream os) throws IOException {
		byte [] buffer = new byte [2048];
		int read = 0;
		while ((read = is.read(buffer)) > 0) {
			os.write(buffer, 0, read);
			os.flush();
		}
		is.close();
		os.close();
	}

	public static void ontologyCopy(InputStream is, FileOutputStream os) throws IOException {
		StringBuffer sb = new StringBuffer();
		
		byte [] buffer = new byte [2048];
		int read = 0;
		while ((read = is.read(buffer)) > 0) {
			sb.append(new String(buffer, 0, read));
		}
		
		is.close();
		String res = sb.toString();
		
		// Remove non-valid header
		res = res.substring(res.indexOf("<rdf:RDF"));
		
		// Remove the <Ontology> tag
		res = res.replace(res.substring(res.indexOf("<owl:Ontology"), res.indexOf("/>", res.indexOf("<owl:Ontology"))+2), "");
		
		// Replace URIs
		res = res.replaceAll("&smoolcore;", "http://com.tecnalia.smool/core/smoolcore#");
		res = res.replaceAll("&owl;", "http://www.w3.org/2002/07/owl#");
		res = res.replaceAll("&xsd;", "http://www.w3.org/2001/XMLSchema#");
		
		// Store
		os.write(res.getBytes());
		os.flush();
		os.close();
	}
	
}
