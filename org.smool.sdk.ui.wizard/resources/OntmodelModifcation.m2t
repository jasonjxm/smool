/**
 * transformation OntmodelModification
 * date: 24/6/2012
 * author: Enas Ashraf : inas@itida.gov.eg
 * description: takes two owl2 files and modify the baseOwl classes inherited from importedOwl by adding their attributes 
 */

texttransformation OntmodelModification (in baseOwl:"http://www.smool.org/sdk/ontmodel/Ontmodel",
										 in importedOwl:"http://www.smool.org/sdk/ontmodel/Ontmodel",
										 out output:"http://www.smool.org/sdk/ontmodel/Ontmodel" (output.Ontology)) {

  module::main () {
     var modifiedOnt:baseOwl.Ontology = baseOwl;
     var importedOnt:importedOwl.Ontology = importedOwl;
     var baseOntName:String = modifiedOnt.URI.substringAfter("#"); 
    
    
    stdout.println("Modified ont " + modifiedOnt.URI)
    stdout.println("Imported ont " + importedOnt.URI)
    stdout.println("")
     
     // look for imported classes
     modifiedOnt.Classes->forEach(importedClass:baseOwl.Class | modifiedOnt.URI != importedClass.URI.substringBefore("#") and importedOnt.URI == importedClass.URI.substringBefore("#")) {
       stdout.println("Found an imported class... " + importedClass.URI);
       importedOnt.Classes->forEach(root:importedOwl.Class) {
         if (root.URI == importedClass.URI) {
           stdout.println("Found imported class in root ontology...");
           importedClass.Attributes.addAll(root.Attributes);
         }
       }
     }
     
     //check the relationship for each class
     modifiedOnt.Classes->forEach(baseOwl2Class:baseOwl.Class ){
       baseOwl2Class.Relationships->forEach(baseOwl2Relationship:baseOwl.Relationship | baseOwl2Relationship.Type == "SubClassOf"  ){
         //stdout.println("SubClassOf relationship found on class " + baseOwl2Class.URI.substringAfter("#").toLower())
         if( baseOwl2Relationship.ReferenceOntology.toLower()==importedOnt.NameSpace.ID.toLower()){
           stdout.println("Imported class " + baseOwl2Relationship.Arg.toLower() + " is defined in " + importedOnt.NameSpace.ID.toLower())
           //get the class and copy the attributes
           importedOnt.Classes->forEach(importedClass:importedOwl.Class | importedClass.URI.substringAfter("#").toLower()== baseOwl2Relationship.Arg.toLower()){
             importedClass.Attributes->forEach(importedClassAttr:importedOwl.Attribute){
              stdout.println("Copying attribute " + importedClassAttr.URI) 
              baseOwl2Class.Attributes.add(importedClassAttr);
               
               //manage inheretence within the base ontology  
               modifiedOnt.Classes->forEach(subClass:baseOwl.Class ){
                 subClass.Relationships->forEach(rel:baseOwl.Relationship | rel.Type == "SubClassOf"){
                   if(rel.Arg == baseOwl2Class.URI.substringAfter("#")){
                     stdout.println("Copying attribute also to " + subClass.URI.substringAfter("#"))
                     subClass.Attributes.add(importedClassAttr);
                   }
                   
                 }
                 
               }
             }
           }           
         }
       }
     }
     
     var processedUPs:List = new List();
     var processed:Boolean = false;
     modifiedOnt.UnprocessedProperties->forEach(up:baseOwl.UnprocessedProperty) {
       processed = false;
       if (up.uri.substringBefore("#").equals(importedOnt.URI)) {
         //stdout.println("Found an Unprocessed Property: " + up.uri);
         
         var attr:importedOwl.Attribute = importedOnt.getAttribute4UP(up);
         if (attr != null) {
           //stdout.println("Found associated attribute in imported ontology");
           var domainClasses:List = new List();
           importedOnt.Classes->forEach(c:importedOwl.Class) {
             if (containsSimilar(c.Attributes, attr)) {
               domainClasses.add(c);
             }
           }
         
           //stdout.println("Found domain classes: " + domainClasses);
         
           modifiedOnt.Classes->forEach(c:baseOwl.Class | c.Relationships.size() > 0) {
             c.Relationships->forEach(rel:baseOwl.Relationship) {
               if (superTypeMatch(rel, domainClasses)) {
                 //stdout.println("Found class " + c.URI + ", which is a subclass of a domain class.");
                 processed = true;
                 c.addAttribute(modifiedOnt, attr);
               }
             }
           }
           
           if (not processed) {
             processed = true;
             stdout.println("****UP not generated for "+ up.id +" Check the models!!!****");
             modifiedOnt.Classes.addAll(domainClasses);
           }
         
         }
         else {
           stdout.println("****Couldn't find the Unprocessed Property: " + up.id +" Check the models!!!****");
           
         }
       }
       else {
       
         /*if (up.superProps.size() > 0) {
           stdout.println("Looking at superProps: " + up.superProps);
         }*/
       
         up.superProps->forEach(s:String | s.substringBefore("#").equals(importedOnt.URI)) {
           //stdout.println("Super property "+ s.substringAfter("#") +" is in imported ontology!");
           importedOnt.Classes->forEach(c:importedOwl.Class) {
             c.Attributes->forEach(a:importedOwl.Attribute) {
               if (a.URI.equals(s.substringAfter("#"))) {
                 //stdout.println("Found associated attribute for super property "+ a.URI +" in imported ontology");
                 var domainClasses:List = new List();
                 importedOnt.Classes->forEach(c:importedOwl.Class) {
                   if (containsSimilar(c.Attributes, a)) {
                     domainClasses.add(c);
                   }
                 }
         
                 //stdout.println("Found domain classes for super property: " + domainClasses);
                 
         
                 modifiedOnt.Classes->forEach(c:baseOwl.Class | c.Relationships.size() > 0) {
                   c.Relationships->forEach(rel:baseOwl.Relationship) {
                     if (superTypeMatch(rel, domainClasses)) {
                       //stdout.println("Found class " + c.URI + ", which is a subclass of a domain class.");
                       c.addAttribute(modifiedOnt, a);
                       processed = true;
                     }
                   }
                 }
                 
                 if (not processed) {
                   processed = true;
                   stdout.println("****UP not generated for "+ up.id +"!! Check the models!!!****");
                   modifiedOnt.Classes.addAll(domainClasses);
                 }
               }
             }
           }
         }
       }
       
       if (processed) {
         processedUPs.add(up);
       }
     }
     
     processedUPs->forEach(up:baseOwl.UnprocessedProperty) {
       modifiedOnt.UnprocessedProperties.remove(up);
     }
     
     // Process Unprocessed Properties that are subproperties of newly added ones
     var processedCount:Integer = 1;
     while (processedCount > 0) {
       processedCount = doProcess(modifiedOnt);
       stdout.println("I Processed " + processedCount + " UPs");
     }
     
     output = modifiedOnt;
     output.store(modifiedOnt.NameSpace.ID+ "_modified.owl2");
     
  }
  
  importedOwl.Ontology::getAttribute4UP (up:baseOwl.UnprocessedProperty) : importedOwl.Attribute {
    self.Classes->forEach(c:importedOwl.Class) {
      c.Attributes->forEach(a:importedOwl.Attribute) {
        if (a.URI.equals(up.id)) {
          return a;
        }
      }
    }
    
    return null;
  }
  
  module::superTypeMatch(rel:baseOwl.Relationship, domainClasses:List) : Boolean {
    var res:Boolean = false;
    
    domainClasses->forEach(c:importedOwl.Class) {
      if (c.URI.substringAfter("#").equals(rel.Arg)) {
        return true;
      }
    }
    
    result = res;
  }
  
  baseOwl.Class::addAttribute(output:baseOwl.Ontology, attr:baseOwl.Attribute) {
    var found:Boolean = false;
    self.Attributes->forEach(a:baseOwl.Attribute) {
      if (attr.URI.equals(a.URI)) {
        found = true;
      }
    }
    if (not found) {
      self.Attributes.add(attr);
    }
    //stdout.println("Adding " + attr.URI + " to " + self.URI.substringAfter("#"));
    output.Classes->forEach(subclass:baseOwl.Class | subclass != self) {
      subclass.Relationships->forEach(rel:baseOwl.Relationship | rel.Arg == self.URI.substringAfter("#")) {
        //stdout.println("Adding attribute also to subclass: " + subclass.URI);
        subclass.addAttribute(output, attr);
      }
    }
  }
  
  module::containsSimilar(l:List, attr:importedOwl.Attribute) : Boolean {
    l->forEach(a:importedOwl.Attribute) {
      if (a.URI == attr.URI) {
        return true;
      }
    }
    return false;
  }
  
  module::doProcess(ont:baseOwl.Ontology) : Integer {
    var i:Integer = 0;
    var processed:List = new List();
    ont.UnprocessedProperties->forEach(up:baseOwl.UnprocessedProperty) {
      var done:Boolean = false;
      ont.Classes->forEach(c:baseOwl.Class) {
        c.Attributes->forEach(a:baseOwl.Attribute) {
          up.superProps->forEach(s:String) {
            //stdout.print("Comparing " + s + " AND " + a.URI)
            if (s.substringAfter("#") == a.URI) {
              //stdout.println("--> MATCH!!")
              var attr:baseOwl.Attribute = new baseOwl.Attribute();
              attr.URI = up.id;
              attr.Range = up.range
              attr.RangeOntology = up.rangeOnt;
              if (attr.Range == "String" or attr.Range == "Integer" or attr.Range == "Boolean" or attr.Range == "Float" or attr.Range == "Double") {
                attr.Type = "FunctionalDatatype";
              }
              else {
                attr.Type = "FunctionalObject";
              } 
              c.Attributes.addOrg(attr);
              done = true;
              i = i+1;
            }
            else {
              //stdout.println("--> nope")
            }
          }
        }
      }
      
      if (done) {
        processed.add(up);
      }
    }
    
    processed->forEach(up:baseOwl.UnprocessedProperty) {
      ont.UnprocessedProperties.remove(up);
    }
    
    result = i;
  }
 
}