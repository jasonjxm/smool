/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.osgi.commands;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import org.smool.sdk.osgi.commands.data.SibData;
import org.smool.sdk.sib.service.IViewer;


/**
 * This class is a service tracker customizer that detects if the Viewer service
 * of any server has been registered in the OSGI Framework.
 * 
 * When a new Viewer server is detected, this plug-in is subscribed to the information about
 * the sessions, subscriptions and triples. 
 * 
 * @author Cristina López, cristina.lopez@tecnalia.com, Tecnalia
 */

@SuppressWarnings("rawtypes")
public class ViewerServiceTrackerCustomizer implements ServiceTrackerCustomizer {

	/**
	 * SIB IViewer service
	 */
	protected IViewer viewer = null;
	
	
	/**
	 * Bundle context
	 */
	private BundleContext bc;		

	/**
	 * Constructor
	 * @param bc Bundle Context
	 */
	public ViewerServiceTrackerCustomizer(BundleContext bc){
		this.bc = bc;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	/*
	 * (non-Javadoc)
	 * @see org.osgi.util.tracker.ServiceTrackerCustomizer
	 */
	public Object addingService(ServiceReference reference) {

		Object obj = bc.getService(reference);
		
		if (obj instanceof IViewer) {
			viewer = (IViewer) bc.getService(reference);
			SibData sibData= new SibData();	
			sibData.setSib(viewer.getSib());
			
			viewer.addListener(sibData.getSessions());
			viewer.addListener(sibData.getSubscriptions());
			viewer.addListener(sibData.getTriples());
			
			Activator.getDefault().getSibViewerList().add(sibData);
			
		} 
		return obj;
	}
	
	@SuppressWarnings({ "unchecked" })
	@Override
	/*
	 * (non-Javadoc)
	 * @see org.osgi.util.tracker.ServiceTrackerCustomizer
	 */
	public void modifiedService(ServiceReference reference, Object serviceObject) {
		Object obj = bc.getService(reference);
		
		if (obj instanceof IViewer) {
			viewer = (IViewer) bc.getService(reference);
			
			SibData sibData= new SibData();
			sibData.setSib(viewer.getSib());
			
			viewer.addListener(sibData.getSessions());
			viewer.addListener(sibData.getSubscriptions());
			viewer.addListener(sibData.getTriples());
			
			Activator.getDefault().getSibViewerList().remove((SibData) serviceObject);
			Activator.getDefault().getSibViewerList().add(sibData);
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	/*
	 * (non-Javadoc)
	 * @see org.osgi.util.tracker.ServiceTrackerCustomizer
	 */
	public void removedService(ServiceReference reference, Object service) {
		Object obj = bc.getService(reference);
		
		if (obj instanceof IViewer) {
			viewer = (IViewer) bc.getService(reference);
			
			//int sibId=Integer.parseInt(reference.getPropertyKeys()[0]);
			
			SibData sibData= null;
			int i=0;
			boolean found=false;
			while(i<Activator.getDefault().getSibViewerList().size() && found==false){
				SibData current= Activator.getDefault().getSibViewerList().get(i);
				//if (current.getSibId()==sibId){
				if (viewer.getSib()==current.getSib()){
					found=true;
					sibData=current;
				}
					i++;
			}
	
			viewer.removeListener(sibData.getSessions());
			viewer.removeListener(sibData.getSubscriptions());
			viewer.removeListener(sibData.getTriples());
			
			Activator.getDefault().getSibViewerList().remove(sibData);

		}
	}

}
