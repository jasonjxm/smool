/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Cristina L�pez (Fundacion European Software Institute) - initial API, implementation and documentation
 *******************************************************************************/

package org.smool.sdk.osgi.commands.provider;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.osgi.framework.console.CommandInterpreter;
import org.eclipse.osgi.framework.console.CommandProvider;
import org.osgi.service.log.LogService;
import org.smool.sdk.gateway.exception.GatewayException;
import org.smool.sdk.gateway.service.IGateway;
import org.smool.sdk.gateway.service.IGatewayFactory;
import org.smool.sdk.osgi.commands.Activator;
import org.smool.sdk.osgi.commands.data.Element;
import org.smool.sdk.osgi.commands.data.SibData;
import org.smool.sdk.sib.data.Session;
import org.smool.sdk.sib.data.Subscription;
import org.smool.sdk.sib.data.Triple;
import org.smool.sdk.sib.exception.SIBException;
import org.smool.sdk.sib.service.ISIB;
import org.smool.sdk.sib.service.ISIBFactory;



/**
 * This class extend the OSGi commands provider by default. The extensions are
 * exclusively part of the Smool project.
 * 
 * All the Smool commands start with the keyword "sspace" followed by the
 * operation to execute. This is the list of operations: sspace help sspace help
 * props sspace list sspace info sspace create sspace destroy sspace start
 * sspace stop
 * 
 * The syntax of the commands can be checked typing "sspace help" in the OSGi
 * console
 * 
 * 
 * @author Cristina L�pez, cristina.lopez@tecnalia.com, Tecnalia
 * 
 */
public class SmoolCommandProvider implements CommandProvider {

	/**
	 * Gateway Manager Services Map
	 */
	protected HashMap<String, IGatewayFactory> gwMgtMap = null;

	/**
	 * SIB Factory Service
	 */
	protected ISIBFactory sibMgt = null;

	/**
	 * Abstract Gateway Factory Service
	 */
	protected IGatewayFactory gwMgt = null;
	
	/**
	 * Used to validate IP addresses 
	 */
	private static final String PATTERN = 
	        "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
	        "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
	        "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
	        "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

	/**
	 * Constructor
	 */
	public SmoolCommandProvider() {
		gwMgtMap = Activator.getDefault().getgwmgtstc().getGwMgtMap();
		sibMgt = Activator.getDefault().getMgtstc().getSibManager();

	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osgi.framework.console.CommandProvider#getHelp()
	 */
	public String getHelp() {
		StringBuffer buffer = new StringBuffer(60);
		buffer.append("---SMOOL Commands---\n\t");
		buffer.append("sspace list -> List the available Servers and Gateways\n\t");
		buffer.append("sspace create -sib -name=XXX -> Creates a Sib instance\n\t");
		buffer.append("sspace create -gw -name=XXX -type="
				+ getGatewayTypes()
				+ " -idSib=99999 -props={ENDPOINT=IP:PORT} for a single IP:PORT or -props={ENDPOINTS=[IP1:PORT1, ...IPN:PORTN]} for several IPs and ports \n\t");
		buffer.append("For instance: Create a TCP/IP gateway that listens to a specific IP and PORT\n\t");
		buffer.append("sspace create -gw -name=gw1 -type=TCP/IP -idSib=1 -props={ENDPOINT=172.22.100.166:23000}\n\n\t");
		buffer.append("sspace destroy [-sib|-gw] -id=99999 -> Destroys a Sib or Gateway instance\n\t");
		buffer.append("sspace start [-sib|-gw] -id=99999 -> Starts a Sib or Gateway\n\t");
		buffer.append("sspace stop [-sib|-gw] -id=99999 -> Stops a Sib or Gateway\n\t");
		buffer.append("sspace info [-sib|-gw] -id=99999 -> List the properties of a Sib or Gateway\n\t");
		buffer.append("sspace sibData -[sessions|subscriptions|triples|individuals] -idSib=99999 -> List the information available in the Sib");
		return buffer.toString();
	}

	/**
	 * This method prints in the console the default configurable properties of
	 * the different types of Gateway The method is invoked when the command
	 * "sspace help props" is executed
	 * 
	 * @return String
	 */
	public String getHelpProps() {
		StringBuffer buffer = new StringBuffer(60);
		Iterator<String> it = gwMgtMap.keySet().iterator();
		while (it.hasNext()) {
			String type = it.next();
			buffer.append("These are the default configurable properties values for a Gateway of type "
					+ type + "\n\t");
			Properties properties = gwMgtMap.get(type)
					.getConfigurableProperties();
			Iterator<Object> it2 = properties.keySet().iterator();
			while (it2.hasNext()) {
				Object label = it2.next();
				String value = properties.get(label).toString();
				buffer.append(label + " :" + value + "\n\t");
			}
			buffer.append("\n");

		}
		return buffer.toString();
	}

	/**
	 * Method that defines the commands of Smool
	 * 
	 * @param ci
	 */
	public void _sspace(CommandInterpreter ci) {
		String operation = ci.nextArgument();
		if (operation != null) {
			if (operation.equals("list")) {
				list(ci);
			} else if (operation.equals("create")) {
				create(ci);
			} else if (operation.equals("destroy")) {
				destroy(ci);
			} else if (operation.equals("start")) {
				start(ci);
			} else if (operation.equals("stop")) {
				stop(ci);
			} else if (operation.equals("info")) {
				info(ci);
			} else if (operation.equals("sibData")){
				sibData(ci);
			} else if (operation.equals("help")) {
				String props = ci.nextArgument();
				if (props == null || props.equals("")) {
					ci.println(getHelp());
				} else if (props.equals("props")) {
					ci.println(getHelpProps());
				}
			} else {
				ci.println("The operation introduced does not exits. Please, choose another operation from the list: ");
				ci.println(getHelp());
			}

		} else {
			ci.println("Incorrect syntax. Try again.");
			ci.println(getHelp());
		}

	}

	/**
	 * Method that implements the logic of the command "list"
	 * 
	 * @param ci
	 */
	public void list(CommandInterpreter ci) {

		if (Activator.getDefault().getSibList().size() > 0) {
			ci.println("List of active SIBs and Gateways: ");
			ci.println(String.format("%5s %27s %27s %10s %7s", " Id  ",
					"Sib Name         ", "Gw Name          ", "Gw Type   ",
					"Status"));
			String line = "----- --------------------------- --------------------------- ---------- -------";
			ci.println(line);

			for (int i = 0; i < Activator.getDefault().getSibList().size(); i++) {

				Element element = Activator.getDefault().getSibList().get(i);
				ISIB sib = Activator.getDefault().getSibList().get(i).getSib();
				String status = "";
				if (sib.isRunning()) {
					status = "Started";
				} else if (sib.isStopped()) {
					status = "Stopped";
				}
				// Print the SIB related information
				ci.println(String.format("%5s %27s %27s %10s %7s",
						Integer.toString(sib.getId()), sib.getName(), " ", " ",
						status));

				// Print the SIB Gateways related information
				for (int j = 0; j < element.getGateways().size(); j++) {
					IGateway gw = element.getGateways().get(j);
					String gwStatus = "";
					if (gw.isRunning()) {
						gwStatus = "Started";
					} else if (gw.isStopped()) {
						gwStatus = "Stopped";
					}
					ci.println(String.format("%5s %27s %27s %10s %7s",
							Integer.toString(gw.getId()), "", gw.getName(),
							gw.getType(), gwStatus));
				}

			}
		} else {
			Activator.getDefault().getLogger().log(LogService.LOG_ERROR, "RequestURI was show_contents. Constructing SIB contents response");
			ci.println("There are not active servers.");
		}

	}

	/**
	 * 
	 * This function is used to validate the command line properties for gateway creation
	 * The function adds some default values for IPADDRESS/HOST properties if not specified in the GW creation command
	 * Created by Enas Ashraf - May, 16th - Fix GW properties bug  
	 * @param gwProps
	 * @return validatedGwProps 
	 */
	public Properties validateGwProps(Properties gwProps) {
		Properties validatedGwProps = gwProps;
		//Check if the properties are empty
		if (validatedGwProps.isEmpty()){
			String IP = getNetIP();
			validatedGwProps.put("ENDPOINT", IP + ":23000");
			Activator.getDefault().getLogger().log(LogService.LOG_DEBUG, "No IP:PORT was specidied when creating the Gateway. Using the following one instead " + validatedGwProps.getProperty("ENDPOINT"));
		}
		//Iterate the props and populate
		else{
//			for (Map.Entry<Object, Object> entry : validatedGwProps.entrySet()) {
//				ArrayList<String> endpoints = new ArrayList<String>();
//			    String ip = (String) entry.getKey();
//			    String port = (String) entry.getValue();
//			    endpoints.add(ip + ":" + port);
//			    
//			    // ... 
//			}
			if (validatedGwProps.getProperty("IPADDRESS") == null) {
				validatedGwProps.put("IPADDRESS", getNetIP());
				Activator.getDefault().getLogger().log(LogService.LOG_DEBUG, validatedGwProps.getProperty("IPADDRESS"));
			}
			if(validatedGwProps.getProperty("HOST")==null){
				validatedGwProps.put("HOST", validatedGwProps.getProperty("IPADDRESS"));
			}
			if(validatedGwProps.getProperty("DEFAULT_IPADDRESS")==null){
				validatedGwProps.put("DEFAULT_IPADDRESS", validatedGwProps.getProperty("IPADDRESS"));
			}
		}
		
		
		return validatedGwProps;
	}


	/**
	 * This function is used to validate the IP address
	 * Same function as in TCPIPGatewayConfiguration class
	 * Added here by Enas Ashraf - May, 16th - Fix GW  properties bug 
	 * 
	 * @param ip
	 * @return valid true/false
	 */
	// Method that checks if a valid ipv4 address is passed. Added by AVK
	// 06/13/2011
	public static boolean validIP(String ip) {
		try {
			if (ip == null || ip.isEmpty()) {
				return false;
			}

			String[] parts = ip.split("\\.");
			if (parts.length != 4) {
				return false;
			}

			for (String s : parts) {
				try {
					int i = Integer.parseInt(s);
					if ((i < 0) || (i > 255)) {
						return false;
					}
				} 
				catch (NumberFormatException e) {
					return false;
				}
			}

			return true;
		} 
		catch (NumberFormatException nfe) {
			Activator.getDefault().getLogger().log(LogService.LOG_ERROR, "Could not validate provided IP" + ip);
			return false;
		}
	}

	/**
	 * getNetIP gets the first valid network IP 
	 * This is used in case the user di't specify the IPADDRESS in the command line options for gateway creation
	 * This function behavior is a simplified version of the one used in getDefaultProperties()
	 * getDefaultProperties() gets all network adapters and addresses but  this only gets the first valid one
	 * Added by: Enas Ashraf - May, 16th - Fix GW properties bug - Edit tag = //EA-GW 
	 *@return first valid network address
	 */
	public static String getNetIP() {

		//Properties properties = new Properties();
		// This map will hold pairs <adapterName, list of addresses>

		Enumeration<NetworkInterface> nets = null;
		//boolean valid = false;

		try {
			nets = NetworkInterface.getNetworkInterfaces();
		} 
		catch (SocketException e) {
			Activator.getDefault().getLogger().log(LogService.LOG_ERROR, "Could not obtain the list of network interfaces to start the TCP/IP gateway");
		}
		for (NetworkInterface netint : Collections.list(nets)) {
			// Check if the adapter name contains the *blue* sequence, which probably
			// means it is a Bluetooth adapter that we don't want to show
			//valid = false;

			if (!netint.getDisplayName().toUpperCase().contains("BLUE")) {
				// list of addresses for an adapter
				//ArrayList<String> addresses = new ArrayList<String>();

				Enumeration<InetAddress> inetAddresses = netint
						.getInetAddresses();
				for (InetAddress inetAddress : Collections.list(inetAddresses)) {
					if (validIP(inetAddress.toString().substring(1))) {
						// check that the given ip is valid
						return inetAddress.toString().substring(1);
					}
				}
			}
		}
		return null;
	}

	/**
	 * Method that implements the logic of the command "create"
	 * 
	 * @param ci
	 *            Edited by Enas Ashraf - May, 16th - Fix GW properties bug -
	 *            Edit tag = //EA-GW
	 */
	@SuppressWarnings("unchecked")
	public void create(CommandInterpreter ci) {
		try {
			boolean error = true;
			//Used to store the valid endPoints
			ArrayList<String> validEndpoints = new ArrayList<String>();
			String op = ci.nextArgument();
			String param = ci.nextArgument();
			if (op != null && param != null && param.startsWith("-name=")
					&& (op.equals("-sib") || op.equals("-gw"))) {
				// Create Sib instance
				String name = param.substring(6);
				if (name != null && !name.isEmpty()) {
					if (op.equals("-sib")) {
						error = false;
						int id = sibMgt.create(name, null);
						ci.println("Server " + name + " created with id " + id);
					}
					// Create Gateway instance
					else if (op.equals("-gw")) {

						String typeParam = ci.nextArgument();

						if (typeParam != null && typeParam.startsWith("-type=")) {

							String gwType = typeParam.substring(6);
							if (gwType != null && !gwType.isEmpty()
									&& gwTypeExits(gwType)) {

								String sibParam = ci.nextArgument();
								if (sibParam != null
										&& sibParam.startsWith("-idSib=")) {

									String idSib = sibParam.substring(7);
									error = false;
									String propsParam = ci.nextArgument();
									IGatewayFactory gwMgt = gwMgtMap.get(gwType);
									ISIB sibServer = getSIBbyID(Integer.parseInt(idSib));
									Properties gwProps = gwMgt.getConfigurableProperties();
									
									
									
									if (propsParam != null) {
										if (propsParam.startsWith("-props=")) {
											//Get the list of available IPs obtained, to compare them with the indicated by the user, if none match, an error should be triggered
											HashMap<String, String> networkAdapters =  (HashMap<String, String>) gwProps.get("NETWORK_ADAPTER");
											//Used to store the adapters IP obtained from the TCPIPGatewayConfiguration object
											ArrayList<String> adapters = new ArrayList<String>();
											if (!networkAdapters.isEmpty()){
												for (Object ip : networkAdapters.values()) {
													if (ip instanceof ArrayList){
														ArrayList<String> ep = (ArrayList<String>) ip;
														String finalIP = ep.get(0);
														if (finalIP.startsWith("/"))
															finalIP = finalIP.substring(1);
														adapters.add(finalIP);
													}
//													adapters.add(ip);
												}
														
											}
											
											
											//Remove props= from the string
											String props = propsParam.substring(7);
											//Parse the properties
											if (props != null&& !props.isEmpty()) {
												//Make sure the first and last characters are curly bracers
												if (props.startsWith("{") && props.endsWith("}")){
													//Check if the remaining string contains ENDPOINT or ENDPOINTS
													props = props.substring(1, props.length());
													props = props.substring(0, props.length() - 1);
													int endPointsCounter = countSubstring("ENDPOINTS", props);
													
													if (props.contains("ENDPOINTS") && endPointsCounter > 0){
														//remove the ENDPOINTS substring
														props = props.substring(9);
														//check if first 2 characters are "=[" and last one is "]
														if (props.startsWith("=[") && props.endsWith("]")){
															//remove first 2 chars and last one
															props = props.substring(2);
															props = props.substring(0, props.length() - 1);
															//Get all the pairs IP:PORT, separated by ;
															StringTokenizer tk = new StringTokenizer(props, ",");
															if (tk.countTokens() > 0){
																while (tk.hasMoreElements()) {
																	String current = tk.nextToken();
																	//Get the tokens separated by ":"
																	StringTokenizer st = new StringTokenizer(current, ":");
																	if (st.countTokens() == 2){
																		String ip = st.nextToken().trim();
																		if (validIP(ip)){
																			//Get the Port
																			String port = st.nextToken().trim();
																			//check this is a valid port number
																			if (validatePORT(port)){
																				//Check that the provided IP is valid (exists in the machine)
																				if (availableAdapter(adapters, ip)){
																					//Store the IP:PORT
//																					gwProps.put(ip, port);
																					validEndpoints.add(ip + ":" + port);
																				}
																				else{
																					error = true;
																					ci.println("ERROR: Provided IP in the some of the ENDPOINTS does not match any available IPs for the current machine. Specify a different one.");
																				}
																			}
																		}
																		
																	}
																	
																}
															}
															else{
																error = true;
																ci.println(getHelpProps());
															}
														}
														else{
															error = true;
															ci.println(getHelpProps());
														}
													}
													//Check if there is only one ENDPOINT instead
													else{
														
														int endPointCounter = countSubstring("ENDPOINT", props);
														if (props.contains("ENDPOINT") && endPointCounter > 0){
															//remove the ENDPOINT substring
															props = props.substring(8);
															//check if first 2 characters are "=[" and last one is "]
															if (props.trim().startsWith("=")){
																//remove first 2 chars and last one
																props = props.substring(1);
																//Check that no list is provided instead of a single value
																if (props.contains("[") || props.contains("]")){
																	error = true;
																	ci.println("ERROR: If you want to provide a list of ENDPOINTS use the keyword ENDPOINTS");
																	
																}
																else{
																	//Get the IP:PORT
																	StringTokenizer st = new StringTokenizer(props, ":");
																	if (st.countTokens() == 2){
																		String ip = st.nextToken();
																		if (validIP(ip)){
																			//Get the Port
																			String port = st.nextToken();
																			//check this is a valid port number
																			if (validatePORT(port)){
																				
																				//Check that the provided IP is valid (exists in the machine)
																				if (availableAdapter(adapters, ip)){
																					//Store the IP:PORT
//																					gwProps.put(ip, port);
																					validEndpoints.add(ip + ":" + port);
																				}
																				else{
																					error = true;
																					ci.println("ERROR: Provided IP in the ENDPOINT does not match any available IPs for the current machine. Specify a different one.");
																				}
																				
																				
																			}
																		}
																	}
																}
																
																
															}
														}
														else{
															error = true;
															ci.println(getHelpProps());
														}
													}	
												}
												//Error, wrong parameters
												else{
													error = true;
													ci.println("ERSROR: Some of the provided parameteres for the Gateway are incorrect.");
													ci.println(getHelpProps());
												}
											} 
											else {
												error = true;
												ci.println(getHelpProps());
											}
										} 
										else {
											error = true;
											ci.println(getHelpProps());
										}
									}
									if (error == false) {
										//Iterate the list of validEndpoints and create a new property ENDPOINT = IP:PORT or ENDPOINTS = {IP:PORT, ..., IP:PORT}
										if (!validEndpoints.isEmpty()){
											//Remove duplicates from ArrayList (just in case)
											HashSet<String> hs = new HashSet<String>();
											hs.addAll(validEndpoints);
											validEndpoints.clear();
											validEndpoints.addAll(hs);
											//Check if we only have one endpoint or several...
											if (validEndpoints.size() == 1){
												String endpoint = validEndpoints.get(0);
												gwProps.put("ENDPOINT", endpoint);
											}
											else{
												Iterator<String> iterator = validEndpoints.iterator();
												StringBuilder builder = new StringBuilder();
												builder.append("{");
												while (iterator.hasNext()){
													String current = iterator.next();
													if (iterator.hasNext()){
														builder.append(current + ",");
													}
													else{
														builder.append(current);
													}
												}
												builder.append("}");
												gwProps.put("ENDPOINTS", builder.toString());
											}
										}
										// EA-GW - Check if all required
										Properties validatedGwProps = validateGwProps(gwProps);
										int id = gwMgt.create(name, sibServer,validatedGwProps); //EA-GW
										ci.println("Gateway " + name+ " created successfully with id " + id);
									}
								}
							} 
							else if (!gwTypeExits(gwType)) {
								Activator.getDefault().getLogger().log(LogService.LOG_DEBUG, "Provided Gateway type does not exist");
								ci.println("The gateway type introduced does not exits.");
								ci.println("The available gateway types are: "
										+ getGatewayTypes());
								ci.println("");
							}
						}
					}
				}
			}
			if (error == true) {
				createErrorMessage(ci);
			}
		} 		
		catch (SIBException e) {
			Activator.getDefault().getLogger().log(LogService.LOG_ERROR, "Could not create SIB server. Reason is " + e.getMessage());
			ci.println("Could not create SIB server. Reason is " + e.getMessage());
		} 
		catch (NumberFormatException e) {
			Activator.getDefault().getLogger().log(LogService.LOG_ERROR, "SIB ID must be a valid integer number, only numeric characters are allowed");
			ci.println("SIB ID must be a valid integer number, only numeric characters are allowed");
		} 
		catch (GatewayException e) {
			Activator.getDefault().getLogger().log(LogService.LOG_ERROR, "Could not create gateway. Reason is " + e.getMessage());
			ci.println("Could not create gateway, reason is" + e.getMessage());
		}
	}

	/**
	 * Method that implements the logic of the command destroy
	 * 
	 * @param ci
	 */
	public void destroy(CommandInterpreter ci) {
		try {
			boolean error = true;
			String op = ci.nextArgument();
			String param = ci.nextArgument();
			if (op != null && param != null && param.startsWith("-id=")
					&& (op.equals("-sib") || op.equals("-gw"))) {
				String id = param.substring(4);
				if (id != null && !id.isEmpty()) {
					error = false;
					if (op.equals("-sib")) {
						List<IGateway> gwList = getSIBGateways(Integer
								.parseInt(id));

						while (gwList.size() > 0) {
							IGateway gw = gwList.get(0);
							if (gw.isRunning()) {
								gw.stop();
							}
							IGatewayFactory gwMgt = gwMgtMap.get(gw.getType());
							gwMgt.destroy(gw.getId());
							ci.println("Gateway " + id + " deleted");
						}
						sibMgt.destroy(Integer.parseInt(id));
						ci.println("Server " + id + " deleted");
					} else if (op.equals("-gw")) {
						IGateway gw = getGatewayById(Integer.parseInt(id));
						if (gw.isRunning()) {
							gw.stop();
						}
						IGatewayFactory gwMgt = gwMgtMap.get(gw.getType());
						gwMgt.destroy(Integer.parseInt(id));
						ci.println("Gateway " + id + " deleted");
					}
				}
			}
			if (error == true) {
				destroyErrorMessage(ci);
			}
		}
		catch (SIBException e) {
			Activator.getDefault().getLogger().log(LogService.LOG_ERROR, "Could not destroy SIB server. Reason is " + e.getMessage());
			ci.println("Could not destroy SIB server. Reason is " + e.getMessage());
		} 
		catch (NumberFormatException e) {
			Activator.getDefault().getLogger().log(LogService.LOG_ERROR, "SIB ID must be a valid integer number, only numeric characters are allowed");
			ci.println("SIB ID must be a valid integer number, only numeric characters are allowed");
		} 
		catch (GatewayException e) {
			Activator.getDefault().getLogger().log(LogService.LOG_ERROR, "Could not destroy gateway. Reason is " + e.getMessage());
			ci.println("Could not destroy gateway, reason is" + e.getMessage());
		}
	}

	/**
	 * Method that implements the logic of the command "start"
	 * 
	 * @param ci
	 */
	public void start(CommandInterpreter ci) {
		try {
			boolean error = true;
			String op = ci.nextArgument();
			String param = ci.nextArgument();
			if (op != null && param != null && param.startsWith("-id=")
					&& (op.equals("-sib") || op.equals("-gw"))) {
				String id = param.substring(4);
				if (id != null && !id.isEmpty()) {
					error = false;
					if (op.equals("-sib")) {
						getSIBbyID(Integer.parseInt(id)).start();
						ci.println("Server " + id + " started sucessfully");
					} 
					else if (op.equals("-gw")) {
						IGateway gw = getGatewayById(Integer.parseInt(id));
						if (gw.getSIB().isRunning()) {
							gw.start();
							ci.println("Gateway " + id + " started sucessfully");
						} 
						else {
							ci.println("In order to start the Gateway, the corresponding SIB server must have been previously started");
						}
					}
				}
			}
			if (error == true) {
				startErrorMessage(ci);
			}
		} 
		catch (SIBException e) {
			Activator.getDefault().getLogger().log(LogService.LOG_ERROR, "Could not start SIB server. Reason is " + e.getMessage());
			ci.println("Could not start SIB server. Reason is " + e.getMessage());
		} 
		catch (NumberFormatException e) {
			Activator.getDefault().getLogger().log(LogService.LOG_ERROR, "SIB ID must be a valid integer number, only numeric characters are allowed");
			ci.println("SIB ID must be a valid integer number, only numeric characters are allowed");
		} 
		catch (GatewayException e) {
			Activator.getDefault().getLogger().log(LogService.LOG_ERROR, "Could not start gateway. Reason is " + e.getMessage());
			ci.println("Could not start gateway, reason is" + e.getMessage());
		}
	}

	/**
	 * Method that implements the logic of the command "stop"
	 * 
	 * @param ci
	 */
	public void stop(CommandInterpreter ci) {
		try {
			boolean error = true;
			String op = ci.nextArgument();
			String param = ci.nextArgument();
			if (op != null && param != null && param.startsWith("-id=")
					&& (op.equals("-sib") || op.equals("-gw"))) {
				String id = param.substring(4);
				if (id != null && !id.isEmpty()) {
					error = false;
					if (op.equals("-sib")) {
						List<IGateway> gwList = getSIBGateways(Integer
								.parseInt(id));
						for (int i = 0; i < gwList.size(); i++) {
							IGateway gw = gwList.get(i);
							if (gw.isRunning()) {
								gw.stop();
							}
							ci.println("gateway " + gw.getId()
									+ " stopped sucessfully");
						}

						getSIBbyID(Integer.parseInt(id)).stop();
						ci.println("Server " + id + " stopped sucessfully");
					} else if (op.equals("-gw")) {
						getGatewayById(Integer.parseInt(id)).stop();
						ci.println("gateway " + id + " stopped sucessfully");
					}
				}
			}
			if (error == true) {
				stopErrorMessage(ci);
			}
		} 
		catch (SIBException e) {
			Activator.getDefault().getLogger().log(LogService.LOG_ERROR, "Could not stop SIB server. Reason is " + e.getMessage());
			ci.println("Could not destroy SIB server. Reason is " + e.getMessage());
		} 
		catch (NumberFormatException e) {
			Activator.getDefault().getLogger().log(LogService.LOG_ERROR, "SIB ID must be a valid integer number, only numeric characters are allowed");
			ci.println("SIB ID must be a valid integer number, only numeric characters are allowed");
		} 
		catch (GatewayException e) {
			Activator.getDefault().getLogger().log(LogService.LOG_ERROR, "Could not stop gateway. Reason is " + e.getMessage());
			ci.println("Could not stop gateway, reason is" + e.getMessage());
		}
	}

	/**
	 * Method that implements the logic of the command "info"
	 * 
	 * @param ci
	 */
	public void info(CommandInterpreter ci) {
		try {
			boolean error = true;
			String el = ci.nextArgument();
			String param = ci.nextArgument();
			if (el != null && param != null && param.startsWith("-id=")
					&& (el.equals("-sib") || el.equals("-gw"))) {
				String id = param.substring(4);
				if (id != null && !id.isEmpty()) {
					error = false;
					if (el.equals("-sib")) {

						ISIB sib = getSIBbyID(Integer.parseInt(id));
						if (sib != null) {
							ci.println("Id: " + sib.getId());
							ci.println("Name: " + sib.getName());
							String status = "";
							if (sib.isRunning()) {
								status = "Started";
							} 
							else if (sib.isStopped()) {
								status = "Stopped";
							}
							ci.println("Status: " + status);
						} 
						else {
							Activator.getDefault().getLogger().log(LogService.LOG_ERROR, "No SIB with the provided identifier does not exist");
							ci.println("No SIB with the provided identifier does not exist");
						}
					} 
					else if (el.equals("-gw")) {
						IGateway gw = getGatewayById(Integer.parseInt(id));
						if (gw != null) {
							ci.println("Id: " + gw.getId());
							ci.println("Name: " + gw.getName());
							String status = "";
							if (gw.isRunning()) {
								status = "Started";
							} else if (gw.isStopped()) {
								status = "Stopped";
							}
							ci.println("Status: " + status);
							Properties props = gw.getProperties();
							Iterator<Object> it = props.keySet().iterator();
							while (it.hasNext()) {
								Object p = it.next();
								String label = p.toString();
								String value = props.get(p).toString();
								ci.println(label + ": " + value);
							}
						} 
						else {
							Activator.getDefault().getLogger().log(LogService.LOG_ERROR, "No gateway with the provided identifier does not exist");
							ci.println("No gateway with the provided identifier does not exist");
						}
					}
				}
			}
			if (error == true) {
				infoErrorMessage(ci);
			}

		} 
		catch (NoSuchElementException e) {
			Activator.getDefault().getLogger().log(LogService.LOG_ERROR, "There was an error trying to retrieve info");
		}
	}

	/**
	 * Method that implements the logic of the command "sibData"
	 * @param ci
	 */
	public void sibData(CommandInterpreter ci){
		try{
			boolean error=true;
			String op=ci.nextArgument();
			String param= ci.nextArgument();
			if(op!=null && param!=null && param.startsWith("-idSib=") && (op.equals("-sessions") || op.equals("-subscriptions") || op.equals("-individuals") || op.equals("-triples"))){
				String id= param.substring(7);
				if(id!=null && !id.isEmpty())
				{
					error=false;
					ISIB sib= getSIBbyID(Integer.parseInt(id));
					SibData sibData= getSIBDatabyID(Integer.parseInt(id));
					if (sib!=null && sibData!=null){
						if (sib.isRunning()){
							/* SESSIONS */
							if (op.equals("-sessions")){
								ci.println(" ");
								ci.println(sib.getName()+" Sessions: ");
								ci.println(" ");
								
								if (sibData.getSessions()!=null && sibData.getSessions().getListSessions()!=null && sibData.getSessions().getListSessions().size()>0){
									ci.println(String.format("%30s %23s", "            Node Id           ", "           Date       "));
									String line="------------------------------   -----------------------";
									ci.println(line);
									
									List<Session> sessions=sibData.getSessions().getListSessions();
									for (int i=0; i<sessions.size();i++){
										Session current= sessions.get(i);
										ci.println(String.format("%30s %23s", current.getNodeId(),"  "+current.getDate() ));
									}
								}
								else{
									Activator.getDefault().getLogger().log(LogService.LOG_DEBUG, "No current sessions in the selected SIB");
									ci.println("The are no sessions in the Sib");
								}
							
							
							}
							/* SUBSCRIPTIONS */
							else if (op.equals("-subscriptions")){
								ci.println(" ");
								ci.println(sib.getName()+" Subscriptions: ");
								ci.println(" ");	
								
								if (sibData.getSubscriptions()!=null && sibData.getSubscriptions().getListSubscriptions()!=null && sibData.getSubscriptions().getListSubscriptions().size()>0){
									ci.println(String.format("%12s %30s %60s %23s", "Subscription", "Node Id","Query", "Date"));
									String line="------------ ------------------------------ ------------------------------------------------------------ -----------------------";
									ci.println(line);
									
									List<Subscription> subscriptions=sibData.getSubscriptions().getListSubscriptions();
									for (int i=0; i<subscriptions.size();i++){
										Subscription current= subscriptions.get(i);
										ci.println(String.format("%12s %30s %60s %23s", Long.toString(current.getSubscriptionId()), current.getNodeId(),current.getQuery(),current.getDate() ));
									}
								}
								else{
									Activator.getDefault().getLogger().log(LogService.LOG_DEBUG, "No current subscriptions in the selected SIB");
									ci.println("The are no subscriptions in the Sib");
								}
							
								
							}
							/* TRIPLES */
							else if (op.equals("-triples") || op.equals("-individuals")){
								ci.println(" ");
								if (op.equals("-individuals")){
									ci.println(sib.getName()+" Semantic Data (Individuals): ");									
								}
								else if (op.equals("-triples")){
									ci.println(sib.getName()+" Semantic Data (Triples): ");									
								}

								ci.println(" ");
								
								if (sibData.getTriples()!=null && sibData.getTriples().getListTriples()!=null && sibData.getTriples().getListTriples().size()>0){
									ci.println(String.format("%50s %25s %50s %23s %30s", "Subject", "Predicate","Object", "           Date       ", "Owner KP"));
									String line="-------------------------------------------------- ------------------------- -------------------------------------------------- ----------------------- ------------------------------ ";
									ci.println(line);
									
									List<Triple> triples=sibData.getTriples().getListTriples();
									if (op.equals("-individuals")){
										triples=locateIndividuals(triples);
									}
									if (triples.size()>0)
									{
										for (int i=0; i<triples.size();i++){
											Triple current= triples.get(i);
											ci.println(String.format("%50s %25s %50s %23s %30s", current.getSubject(), current.getPredicate(), current.getObject(), current.getDate(), current.getOwner()));
										}
									}
									else{
										ci.println("The are no individuals in the Sib");
									}
								}
								else{
									Activator.getDefault().getLogger().log(LogService.LOG_DEBUG, "No data in the selected SIB");
									ci.println("The is no semantic data in the Sib");
								}
							}
						}
						else{
							Activator.getDefault().getLogger().log(LogService.LOG_ERROR, "Indicated SIB is not started. No information to show.");
							ci.println("Indicated SIB is not started. No information to show.");
						}
					}
					else{
						error=true;
					}
						
				}
				else{
					Activator.getDefault().getLogger().log(LogService.LOG_ERROR, "No SIB matches the identifier provided");
					ci.println("No SIB matches the identifier provided");
				}
			}
			if (error==true){
				sibDataErrorMessage(ci);
			}

		}
		catch (NumberFormatException e) {
			Activator.getDefault().getLogger().log(LogService.LOG_ERROR, "Provided SIB ID is not valid. Only numeric characters allowed");
			ci.println("Provided SIB ID is not valid. Only numeric characters allowed");
		} 
	}
	
	/**
	 * This method returns a String containing the gateway types that are
	 * available is the OSGi registry
	 * 
	 * @return String
	 */
	private String getGatewayTypes() {
		String _return = "";

		_return = gwMgtMap.keySet().toString();

		return _return;
	}

	/**
	 * This method checks if the type received as parameter exits in the OSGi
	 * registry
	 * 
	 * @param type
	 * @return boolean
	 */
	private boolean gwTypeExits(String type) {
		boolean exits = false;
		Object[] types = (Object[]) gwMgtMap.keySet().toArray();
		int i = 0;
		while (i < types.length && exits == false) {
			String current = types[i].toString();
			if (current.equals(type)) {
				exits = true;
			}
			i++;
		}

		return exits;
	}

	/**
	 * This method seeks a SIB server in the list of SIB instances registered in
	 * OSGi
	 * 
	 * @param id
	 * @return ISIB
	 */
	private ISIB getSIBbyID(int id) {
		ISIB sib = null;
		List<Element> elements = Activator.getDefault().getSibList();

		int i = 0;
		boolean found = false;
		while (i < elements.size() && found == false) {
			if (elements.get(i).getSib().getId() == id) {
				found = true;
				sib = elements.get(i).getSib();
			} else {
				i++;
			}

		}

		return sib;

	}

	/**
	 * This method returns the list of Gateways of an specific SIB server
	 * 
	 * @param id
	 * @return List<IGateway>
	 */
	private List<IGateway> getSIBGateways(int id) {
		List<IGateway> gwList = new ArrayList<IGateway>();

		List<Element> elements = Activator.getDefault().getSibList();
		int i = 0;
		boolean found = false;
		while (i < elements.size() && found == false) {
			if (elements.get(i).getSib().getId() == id) {
				found = true;
				gwList = elements.get(i).getGateways();
			} else {
				i++;
			}
		}
		return gwList;

	}

	/**
	 * This method seeks a Gateway in the list og Gateway instance registered in
	 * OSGi
	 * 
	 * @param id
	 * @return
	 */
	private IGateway getGatewayById(int id) {
		IGateway gw = null;

		int i = 0;
		boolean found = false;

		while (i < Activator.getDefault().getSibList().size() && found == false) {
			Element element = Activator.getDefault().getSibList().get(i);
			for (int j = 0; j < element.getGateways().size(); j++) {
				IGateway current = element.getGateways().get(j);
				if (current.getId() == id) {
					found = true;
					gw = current;
				}
			}
			i++;
		}

		return gw;
	}
	
	/**
	 * This method seeks the data of a specific SIB in the SIBViewer instances registered in OSGi 
	 * 
	 * @param id
	 * @return SibData
	 */
	private SibData getSIBDatabyID(int id) {
		SibData sibData = null;
		List<SibData> elements = Activator.getDefault().getSibViewerList();
		ISIB sib= getSIBbyID(id);
		
		int i = 0;
		boolean found = false;
		while (i < elements.size() && found == false) {
			//if (elements.get(i).getSibId() == id) {
			if (elements.get(i).getSib()==sib){
				found = true;
				sibData = elements.get(i);
			} else {
				i++;
			}
		}
		return sibData;
	}

	/**
	 * This method navigates the list of Triples received, and return only the individuals
	 * 
	 */
	private List<Triple> locateIndividuals(List<Triple> triples){
		
		List<Triple> returnTriples= new ArrayList<Triple>();
		
		for (int i=0; i<triples.size(); i++){
			Triple current= triples.get(i);
			if(current.getSubject().startsWith("individual:")){
				returnTriples.add(current);
			}
		}
		
		return returnTriples;
		
	}
	
	
	
	/***** ERROR MESSAGES ******/

	/**
	 * Create command error message
	 */
	private void createErrorMessage(CommandInterpreter ci/* , int error */) {
		ci.println("Create command syntax was incorrect. Please try again. Check below for correct syntax");
		ci.println("        sspace create -sib -name=XXX -> Creates a Sib instance");
		ci.println("        sspace create -gw -name=XXX -type="
				+ getGatewayTypes()
				+ " -idSib=99999 -props={ENDPOINT=IP:PORT} for a single IP:PORT or -props={ENDPOINTS=[IP1:PORT1, ...IPN:PORTN]} for several IPs and ports");
		ci.println("For instance: Create a TCP/IP gateway that listens to a specific IP and PORT");
		ci.println("        sspace create -gw -name=gw1 -type=TCP/IP -idSib=1 -props={ENDPOINT=172.22.100.166:23000}");
	}

	/**
	 * Destroy command error message
	 * 
	 * @param ci
	 */
	private void destroyErrorMessage(CommandInterpreter ci) {
		ci.println("Destroy command syntax incorrect. Try again");
		ci.println("        sspace destroy [-sib|-gw] -id=99999 -> Destroys a Sib or Gateway instance");
	}

	/**
	 * Start command error message
	 * 
	 * @param ci
	 */
	private void startErrorMessage(CommandInterpreter ci) {
		ci.println("Start command syntax incorrect. Try again");
		ci.println("        sspace start [-sib|-gw] -id=99999 -> Starts a Sib or Gateway");
	}

	/**
	 * Stop command error message
	 * 
	 * @param ci
	 */
	private void stopErrorMessage(CommandInterpreter ci) {
		ci.println("Stop command syntax incorrect. Try again");
		ci.println("        sspace stop [-sib|-gw] -id=99999 -> Stops a Sib or Gateway");
	}

	/**
	 * Info command error message
	 * 
	 * @param ci
	 */
	private void infoErrorMessage(CommandInterpreter ci) {
		ci.println("Info command syntax incorrect. Try again");
		ci.println("         sspace info [-sib|-gw] -id=99999 -> List the properties of a Sib or Gateway");
	}
	
	/**
	 * sibData command error message
	 * @param ci
	 */
	private void sibDataErrorMessage (CommandInterpreter ci){
		ci.println("SIBData command syntax incorrect. Try again");
		ci.println("        sspace sibData -[sessions|subscriptions|triples|individuals] -idSib=99999 -> List the information available in the Sib");
	}
	
	/**
	 * Counts the number of ocurrences of a substring in a provided String
	 * @param subStr	The substring to search
	 * @param str		The String to be used for searching
	 * @return			The number of ocurrences
	 */
	public static int countSubstring(String subStr, String str){
		return (str.length() - str.replace(subStr, "").length()) / subStr.length();
	}
	
	/**
	 * Validates a IP 
	 * @param ip	The String with the possibly valid IP	
	 * @return	TRUE if provided String is a valid IP
	 */
	public static boolean validateIP(final String ip){          

		Pattern pattern = Pattern.compile(PATTERN);
	    Matcher matcher = pattern.matcher(ip);
	    return matcher.matches();             
	}
	
	/**
	 * Validates a PORT as String
	 * @param ip	The String with the possibly valid PORT	
	 * @return	TRUE if provided stirng is a valid PORT (integer between 1024 and 65535
	 */
	public static boolean validatePORT(final String port){          
		boolean isNumber = port.matches("\\d+");
		if (isNumber){
			//Try to convert to Integer
			try {
				int portAsInt = Integer.parseInt(port);
				if (portAsInt > 1024 && portAsInt <= 65535){
					return true;
				}
				else{
					return false;
				}
			} 
			catch (NumberFormatException  e) {
				return false;
			}
			
		}
		else{
			return false;
		}
	}
	
	/**
	 * Checks if the provided IP is contained in a position of the provided ArrayList. Maybe is not a bad idea to use a Set instead of a List,
	 * but I don't care if there are duplicates, as long as we find the IP, we proceed.
	 * @param adapters	The list containing the network adapters IPs 
	 * @param ip		The ip to be searched in the list
	 * @return			TRUE if the IP was found in the list
	 */
	private boolean availableAdapter(ArrayList<String> adapters, String ip){
		if (!adapters.isEmpty()){
			if (adapters.contains(ip)){
				return true;
			}
			else{
				return false;
			}
		}
		else{
			return false;
		}
	}

}
