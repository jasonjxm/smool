/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Cristina L�pez (Fundacion European Software Institute) - initial API, implementation and documentation
 *******************************************************************************/

package org.smool.sdk.osgi.commands;

import java.util.ArrayList;
import java.util.LinkedList;

import org.eclipse.core.runtime.Plugin;
import org.eclipse.osgi.framework.console.CommandProvider;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.service.log.LogReaderService;
import org.osgi.service.log.LogService;
import org.osgi.util.tracker.ServiceTracker;
import org.smool.sdk.gateway.service.IGateway;
import org.smool.sdk.gateway.service.IGatewayFactory;
import org.smool.sdk.osgi.commands.data.Element;
import org.smool.sdk.osgi.commands.data.SibData;
import org.smool.sdk.osgi.commands.provider.SmoolCommandProvider;
import org.smool.sdk.sib.service.ISIB;
import org.smool.sdk.sib.service.ISIBFactory;
import org.smool.sdk.sib.service.IViewer;

/**
 * This Activator class prepares the plug-in to work both with the manager of
 * the SIB and the manager of the Gateway.
 * 
 * This plug-in extends the OsGi default available commands with a collection of
 * commands that will be available in the scope of Smool to execute operations
 * over SIB servers and Gateways.
 * 
 * @author Cristina López, cristina.lopez@tecnalia.com, Tecnalia
 */
public class Activator extends Plugin {

	/**
	 * Plug-in Id
	 */
	public static final String PLUGIN_ID = "org.smool.sdk.osgi.commands";

	/**
	 * Shared Activator Instance
	 */
	private static Activator plugin;

	/**
	 * Command Provider service property
	 */
	private CommandProvider service;

	/**
	 * ISIB Factory service tracker customizer
	 */
	private ServerManagerServiceTrackerCustomizer mgtstc;
	/**
	 * Gateway Factory service tracker customizer
	 */
	private GatewayManagerServiceTrackerCustomizer gwmgtstc;

	/**
	 * SIB service tracker customizer
	 */
	private SIBServiceTrackerCustomizer sibstc;

	/**
	 * Gateway service tracker customizer
	 */
	private GatewayServiceTrackerCustomizer gwstc;

	/**
	 * Gateway service tracker customizer
	 */
	private ViewerServiceTrackerCustomizer viewstc;

	/**
	 * List of SIB instances
	 */
	private ArrayList<Element> sibList = new ArrayList<Element>();

	/**
	 * List of SIB Viewers instances
	 */
	private ArrayList<SibData> sibViewerList = new ArrayList<SibData>();

	/**
	 * List with all the available LogReaderServices
	 */
	private LinkedList<LogReaderService> m_readers = new LinkedList<LogReaderService>();

	/**
	 * List with all the available LogServices
	 */
	private LinkedList<LogService> m_services = new LinkedList<LogService>();

	/**
	 * Reference to the LogService that will eventually be used for logging
	 */
	private LogService logger;

//	/**
//	 * Reference to our implementation of LogListener class, that prints Log messages in our desired format
//	 */
//	private ConsoleLogImpl m_console = new ConsoleLogImpl();

	/*
	 * We use a ServiceListener to dynamically keep track of all the
	 * LogReaderService service being registered or unregistered
	 * 
	 */
	private ServiceListener m_servlistener = new ServiceListener() {
		public void serviceChanged(ServiceEvent event) {
			BundleContext bc = event.getServiceReference().getBundle().getBundleContext();
			LogReaderService lrs = (LogReaderService) bc.getService(event.getServiceReference());
			if (lrs != null) {
				if (event.getType() == ServiceEvent.REGISTERED) {
					m_readers.add(lrs);
					// lrs.addLogListener(m_console);
				} else if (event.getType() == ServiceEvent.UNREGISTERING) {
					// lrs.removeLogListener(m_console);
					m_readers.remove(lrs);
				}
			}
		}
	};

	/**
	 * The constructor
	 */
	public Activator() {
	}

	/**
	 * Retrieves the ISIB Factory service tracker customizer
	 * 
	 * @return ServerManagerServiceTrackerCustomizer
	 */
	public ServerManagerServiceTrackerCustomizer getMgtstc() {
		return mgtstc;
	}

	/**
	 * Retrieves the IGateway factory service tracker customizer
	 * 
	 * @return GatewayManagerServiceTrackerCustomizer
	 */
	public GatewayManagerServiceTrackerCustomizer getgwmgtstc() {
		return gwmgtstc;
	}

	/**
	 * Retrieves the SIB service tracker customizer
	 * 
	 * @return SIBServiceTrackerCustomizer
	 */
	public SIBServiceTrackerCustomizer getSibstc() {
		return sibstc;
	}

	/**
	 * Retrieves the Gateway service tracker customizer
	 * 
	 * @return
	 */
	public GatewayServiceTrackerCustomizer getGwstc() {
		return gwstc;
	}

	/**
	 * @return the viewstc
	 */
	public ViewerServiceTrackerCustomizer getViewstc() {
		return viewstc;
	}

	/**
	 * Retrieves the list of SIB instances
	 * 
	 * @return
	 */
	public ArrayList<Element> getSibList() {
		return sibList;
	}

	/**
	 * @return the sibViewerList
	 */
	public ArrayList<SibData> getSibViewerList() {
		return sibViewerList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.
	 * BundleContext)
	 */
	// @SuppressWarnings({"rawtypes", "unchecked"})
	// @SuppressWarnings({"rawtypes"})
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void start(BundleContext context) throws Exception {
		plugin = this;

		// Get a list of all the registered LogReaderService, and add the console
		// listener
		ServiceTracker logReaderTracker = new ServiceTracker(context,
				org.osgi.service.log.LogReaderService.class.getName(), null);
		logReaderTracker.open();
		Object[] readers = logReaderTracker.getServices();
		if (readers != null) {
			for (int i = 0; i < readers.length; i++) {
				LogReaderService lrs = (LogReaderService) readers[i];
				m_readers.add(lrs);
				// specify what LogListener will spit log messages
				// lrs.addLogListener(m_console);
			}
		}
		// close the tracker
		logReaderTracker.close();

		// Add the ServiceListener, but with a filter so that we only receive events
		// related to LogReaderService
		String filter = "(objectclass=" + LogReaderService.class.getName() + ")";
		try {
			context.addServiceListener(m_servlistener, filter);
		} catch (InvalidSyntaxException e) {
			System.out.println("ERROR: Could not add service Listeners for LogReaderService.");
		}

		// Get a list of all the registered LogService, and add the console listener
		ServiceTracker logServiceTracker = new ServiceTracker(context, org.osgi.service.log.LogService.class.getName(),
				null);
		logServiceTracker.open();
		Object[] services = logServiceTracker.getServices();
		if (services != null) {
			for (int i = 0; i < services.length; i++) {
				LogService ls = (LogService) services[i];
				String className = ls.getClass().getSimpleName();
				if (ls instanceof LogService && className.contains("LogServiceImpl")) {
					m_services.add(ls);
					logger = (LogService) ls;
					logger.log(LogService.LOG_INFO, "Starting the org.smool.sdk.osgi.rest.servlet...");
					break;
				}

			}
		}

		logServiceTracker.close();

		// Obtains the SIB manager service
		mgtstc = new ServerManagerServiceTrackerCustomizer(context);
		ServiceTracker serverTracker = new ServiceTracker(context, ISIBFactory.class.getName(), mgtstc);
		serverTracker.open();

		// Obtains the Generic Gateway manager service
		gwmgtstc = new GatewayManagerServiceTrackerCustomizer(context);
		ServiceTracker tcpipGwTracker = new ServiceTracker(context, IGatewayFactory.class.getName(), gwmgtstc);
		tcpipGwTracker.open();

		// Obtains the registered SIB instances
		sibstc = new SIBServiceTrackerCustomizer(context);
		ServiceTracker sibTracker = new ServiceTracker(context, ISIB.class.getName(), sibstc);
		sibTracker.open();

		// Obtains the registered Gateway instances
		gwstc = new GatewayServiceTrackerCustomizer(context);
		ServiceTracker gwTracker = new ServiceTracker(context, IGateway.class.getName(), gwstc);
		gwTracker.open();

		// Obtains the registered SibViewer instances
		viewstc = new ViewerServiceTrackerCustomizer(context);
		ServiceTracker viewTracker = new ServiceTracker(context, IViewer.class.getName(), viewstc);
		viewTracker.open();

		// register the service
		service = new SmoolCommandProvider();
		context.registerService(CommandProvider.class.getName(), service, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		service = null;
		mgtstc = null;
		gwmgtstc = null;
		sibstc = null;
		gwstc = null;
		viewstc = null;

		plugin = null;
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

	public LogService getLogger() {
		return logger;
	}

}
