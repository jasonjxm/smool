/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Cristina López, cristina.lopez@tecnalia.com (Tecnalia-ESI) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.osgi.commands.data;

import org.smool.sdk.sib.service.ISIB;


/**
 * This class is utilized in the Commands plugins to store the sessions, subscriptions
 * and triples information of an specific SIB.
 * 
 * @author Cristina López, cristina.lopez@tecnalia.com, ESI
 *
 */
public class SibData{
	
	/*
	 * Sib Identifier
	 */
	//private int sibId;
	
	/*
	 * Associated Sib
	 */
	private ISIB sib;
	
	/*
	 * List of sessions of the SIB 
	 * The sessions are obtained using the SessionData object, which implements a listener of sessions.
	 */
	private SessionData sessions;
	
	
	/*
	 * List of subscriptions of the SIB 
	 * The sessions are obtained using the SubscriptionData object, which implements a listener of subscriptions.
	 */	 
	private SubscriptionData subscriptions;
	
	/*
	 * List of triples of the SIB 
	 * The triples are obtained using the TriplesData object, which implements a listener of triples.
	 */ 
	private TripleData triples;
	
	
	public SibData(){
		sessions=new SessionData();		
		subscriptions= new SubscriptionData();
		triples= new TripleData();
	}
	
	/**
	 * @return the sib
	 */
	public ISIB getSib() {
		return sib;
	}


	/**
	 * @param sib the sib to set
	 */
	public void setSib(ISIB sib) {
		this.sib = sib;
	}	

	/**
	 * @return the sessions
	 */
	public SessionData getSessions() {
		return sessions;
	}

	/**
	 * @param sessions the sessions to set
	 */
	public void setSessions(SessionData sessions) {
		this.sessions = sessions;
	}

	/**
	 * @return the subscriptions
	 */
	public SubscriptionData getSubscriptions() {
		return subscriptions;
	}

	/**
	 * @param subscriptions the subscriptions to set
	 */
	public void setSubscriptions(SubscriptionData subscriptions) {
		this.subscriptions = subscriptions;
	}

	/**
	 * @return the triples
	 */
	public TripleData getTriples() {
		return triples;
	}

	/**
	 * @param triples the triples to set
	 */
	public void setTriples(TripleData triples) {
		this.triples = triples;
	}

}
