/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Cristina L�pez (Fundacion European Software Institute) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.osgi.commands.data;

import java.util.ArrayList;
import java.util.List;

import org.smool.sdk.gateway.service.IGateway;
import org.smool.sdk.sib.service.ISIB;


/**
 * This is a data class is utilized in the Commands plug-in to keep a registry of the
 * existing SIBs and associated gateways
 * 
 * @author Cristina López, cristina.lopez@tecnalia.com, ESI
 *
 */
public class Element{
	
	/**
	 * SIB server instance
	 */
	private ISIB sib;
	
	/**
	 * List of gateways of the SIB
	 */
	private List<IGateway> gateways= new ArrayList<IGateway>();
	
	/**
	 * List of extensions
	 */
	//TODO
	
	/**
	 * Default Constructor
	 */
	public Element (){		
	}
	
	/**
	 * Constructor
	 * @param sib
	 */
	public Element (ISIB sib){
		this.sib=sib;
	}
	
	/**
	 * Retrieves the SIB elements
	 * @return
	 */
	public ISIB getSib() {
		return sib;
	}
	
	/**
	 * Sets the sib element
	 * @param sib
	 */
	public void setSib(ISIB sib) {
		this.sib = sib;
	}
	
	/**
	 * Retrieves the list of Gateways of the Sib element
	 * @return
	 */
	public List<IGateway> getGateways() {
		return gateways;
	}
	
	/**
	 * Sets the list of Gateways of the Sib element
	 * @param gateways
	 */
	public void setGateways(List<IGateway> gateways) {
		this.gateways = gateways;
	}
	
}
