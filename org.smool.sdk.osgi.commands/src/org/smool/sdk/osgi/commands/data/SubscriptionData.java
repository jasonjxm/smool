/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Cristina López, cristina.lopez@tecnalia.com (Tecnalia-ESI) - initial API, implementation and documentation
 *******************************************************************************/ 


package org.smool.sdk.osgi.commands.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.smool.sdk.sib.data.ISubscriptionHolderListener;
import org.smool.sdk.sib.data.Subscription;


/**
 * This class is used to visualize the Subscriptions information of one particular Sib server.
 * The class is also an implementation of a listener. 
 * 
 * @author Cristina López, cristina.lopez@tecnalia.com, Tecnalia
 *
 */

public class SubscriptionData implements ISubscriptionHolderListener {

	/**
	 * List of subscription that are displayed in the tab
	 */
	private List<Subscription> listSubscriptions;
	
	public SubscriptionData() {
		listSubscriptions= new ArrayList<Subscription>();
	}
	
	/**
	 * @return the listSubscriptions
	 */
	public List<Subscription> getListSubscriptions() {
		return listSubscriptions;
	}
	
	/**
	/** Listener methods
	**/
	@Override
	public void initSubscriptions(Collection<Subscription> subscriptions) {
		if (subscriptions != null) {
			for (Subscription subscription : subscriptions) {
				this.listSubscriptions.add(subscription);
			}
		}
	}

	@Override
	public void addSubscription(Subscription subscription) {
		if (subscription != null) {
			this.listSubscriptions.add(subscription);
		}	
	}

	@Override
	public void removeSubscription(Long subscriptionId) {
		if (subscriptionId!=null){
			int i=0;
			boolean found=false;
			List<Subscription> delete= new ArrayList<Subscription>();
			while( i<this.listSubscriptions.size() && found==false) {
				Subscription current=(Subscription)this.listSubscriptions.get(i);
				if (current.getSubscriptionId()==subscriptionId){
					found=true;
					delete.add(current);
				}
				i++;
			}
			
			this.listSubscriptions.removeAll(delete);			
		}
	}

	@Override
	public void removeSubscriptions(Collection<Long> subscriptionIds) {
		if (subscriptionIds!=null){
			Object[] idList= subscriptionIds.toArray();
			List<Subscription> deleteList= new ArrayList<Subscription>();
			for (int j=0; j<idList.length;j++){
				int i=0;
				boolean found=false;			
				while( i < this.listSubscriptions.size() && found==false) {
					Subscription current=(Subscription)this.listSubscriptions.get(i);
					if (current.getSubscriptionId()==idList[j]){
						found=true;
						deleteList.add(current);
					}
					i++;
				}
			}
			
			this.listSubscriptions.removeAll(deleteList);			
		}
	}

	@Override
	public void updateSubscriptions(Collection<Subscription> subscriptions) {
		if (subscriptions != null){
			Object[] idList = subscriptions.toArray();
			
			List<Subscription> deleteList= new ArrayList<Subscription>();
			
			for (int j=0; j < idList.length; j++){
				int i=0;
				boolean found=false;	
				while(i<this.listSubscriptions.size() && found==false) {
					Subscription current = (Subscription)this.listSubscriptions.get(i);
					if (current.getSubscriptionId()==((Subscription)idList[j]).getSubscriptionId()){					
						found=true;
						deleteList.add(current);
					}
					i++;
				}
			}
			
			this.listSubscriptions.removeAll(deleteList);
			this.listSubscriptions.addAll(subscriptions);
		}	
	}

	@Override
	public void reset() {
		this.listSubscriptions.clear();	
		
	}

}
