/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Cristina López, cristina.lopez@tecnalia.com (Tecnalia-ESI) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.osgi.commands.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.smool.sdk.sib.data.ISessionHolderListener;
import org.smool.sdk.sib.data.Session;



/**
 * This class is used to visualize the Sessions information of one particular Sib server.
 * The class is also an implementation of a listener. 
 * 
 * @author Cristina López, cristina.lopez@tecnalia.com, Tecnalia
 *
 */

public class SessionData implements ISessionHolderListener{

	/**
	 * List of sessions that are displayed on the tab
	 */
	private List<Session> listSessions;	
	
	
	
	public SessionData() {
		listSessions= new ArrayList<Session>();
	}

	/**
	 * @return the listSessions
	 */
	public List<Session> getListSessions() {
		return listSessions;
	}

	/**
	/** Listener methods
	**/
	@Override
	public void initSessions(Collection<Session> sessions) {
		if ((sessions != null)&&(sessions.size()>0))
		{
			listSessions=new ArrayList<Session> (sessions);
		}	
	}

	@Override
	public void addSession(Session addedSession) {
		if (addedSession!=null){
			listSessions.add(addedSession);
		}
	}

	@Override
	public void removeSession(Session removedSession) {
		if (removedSession!=null){			
			listSessions.remove(removedSession);	
		}
	}

	@Override
	public void reset() {
			this.listSessions.clear();	
	}

}
