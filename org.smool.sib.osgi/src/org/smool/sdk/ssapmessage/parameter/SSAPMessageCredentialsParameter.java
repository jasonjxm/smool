/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ssapmessage.parameter;



/**
 * The specialized parameter for credentials
 * 
 *
 */
public class SSAPMessageCredentialsParameter extends SSAPMessageParameter {
	
	private String username;
	private String password;
	
	private static String USERNAME = "username";
	private static String PASSWORD = "passwd";
	
	/**
	 * Constructor of credentials parameter in a SSAP message
	 * @param username the username
	 * @param passwd the password
	 */
	public SSAPMessageCredentialsParameter(String username, String passwd) {
		super(SSAPMessageParameter.NameAttribute.CREDENTIALS, getXMLCredentials(username, passwd));
		if (username != null && !username.equals("")) {
			this.setUsername(username);
		}
		
		if (passwd != null && !passwd.equals("")) {
			this.setPassword(passwd);
		}		
	}	
	
	private static String getXMLCredentials(String username, String passwd) {
		if (username != null && !username.equals("") &&
				passwd != null && !passwd.equals("")) {
			StringBuilder sb = new StringBuilder();
			sb.append("<username>").append(username).append("</username>\n");
			sb.append("<passwd>").append(passwd).append("</passwd>\n");
			return sb.toString();
		}
		return null;
	}

	/**
	 * Returns the username of credentials
	 * @return A String with the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Sets the username
	 * @param username the username param
	 */
	public void setUsername(String username) {
		this.username = username;
	}		
	
	/**
	 * Returns the password of credentials
	 * @return A String with the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password
	 * @param password the password param
	 */
	public void setPassword(String password) {
		this.password = password;
	}		
	
	/**
	 * Gets the content of the SSAPMessage, including parameters
	 * @return a string representation of the content of a SSAPMessage
	 */
	@Override
	protected String contentXML() {
		return this.getContent();
	}	
	
	/**
	 * Returns the content of the parameter
	 * @return A String with the content of the parameter
	 */
	@Override
	public String getContent() {
		StringBuilder sb = new StringBuilder();
		if (content != null) {
			sb.append("<").append(USERNAME).append(">");
			sb.append(username);
			sb.append("</").append(USERNAME).append(">\n");
			sb.append("<").append(PASSWORD).append(">");
			sb.append(password);
			sb.append("</").append(PASSWORD).append(">\n");
		} else {
			super.getContent();
		}
		return sb.toString();
	}	
}
