/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.sib;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Properties;

import org.eclipse.core.runtime.Plugin;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.service.log.LogReaderService;
import org.osgi.service.log.LogService;
import org.osgi.util.tracker.ServiceTracker;
import org.smool.sdk.sib.exception.SIBException;
import org.smool.sdk.sib.mgt.SIBFactory;
import org.smool.sdk.sib.service.ISIB;
import org.smool.sdk.sib.service.ISIBFactory;


/** 
 * The activator class for the Semantic Information Broker (SIB).
 * 
 * A plug-in subclasses this class and overrides the appropriate life cycle 
 * methods in order to react to the life cycle requests automatically issued 
 * by the platform. For compatibility reasons, the methods called for those 
 * life cycle events vary, please see the "Constructors and life cycle methods" 
 * section below.
 * 
 *  Conceptually, the plug-in runtime class represents the entire plug-in 
 *  rather than an implementation of any one particular extension the plug-in 
 *  declares. A plug-in is not required to explicitly specify a plug-in 
 *  runtime class; if none is specified, the plug-in will be given a default 
 *  plug-in runtime object that ignores all life cycle requests 
 *  (it still provides access to the corresponding plug-in descriptor).
 *  
 *  In the case of more complex plug-ins, it may be desirable to define a 
 *  concrete subclass of Plugin. However, just subclassing Plugin is not 
 *  sufficient. The name of the class must be explicitly configured in the 
 *  plug-in's manifest (plugin.xml) file with the class attribute of the 
 *  <code><plugin></code> element markup.
 *  
 *  Instances of plug-in runtime classes are automatically created by the 
 *  platform in the course of plug-in activation. For compatibility reasons, 
 *  the constructor used to create plug-in instances varies, please see the 
 *  "Constructors and life cycle methods" section below.
 *  
 *  The concept of bundles underlies plug-ins. However it is safe to regard 
 *  plug-ins and bundles as synonyms. 
 * 
 */
public class SIBActivator extends Plugin {

	/** The plug-in id */
	public static final String PLUGIN_ID = "org.smool.sdk.sib";

	/** The shared instance */
	@SuppressWarnings("unused")
	private static SIBActivator plugin;
	
	/** The bundle context */
	private static BundleContext bc;
	
	/** The SIB Factory **/
	private static SIBFactory sibFactory;
	
	/** Configuration Properties **/
	private static Properties configuration = new Properties();
	
	/**
	 * List with all the available LogReaderServices 
	 */
	private LinkedList<LogReaderService> m_readers = new LinkedList<LogReaderService>();
	
	/**
	 * List with all the available LogServices
	 */
	private LinkedList<LogService> m_services = new LinkedList<LogService>();
	
	/**
	 * Reference to the LogService that will eventually be used for logging
	 */
	private static LogService logger;
	
	/**
	 * Reference to our implementation of LogListener class, that prints Log messages in our desired format
	 */
	private ConsoleLogImpl m_console = new ConsoleLogImpl();
	
	/* 
	 *  We use a ServiceListener to dynamically keep track of all the LogReaderService service being
     * 	registered or unregistered
     * 
     */
    private ServiceListener m_servlistener = new ServiceListener() {
        public void serviceChanged(ServiceEvent event)
        {
            BundleContext bc = event.getServiceReference().getBundle().getBundleContext();
            LogReaderService lrs = (LogReaderService)bc.getService(event.getServiceReference());
            if (lrs != null)
            {
                if (event.getType() == ServiceEvent.REGISTERED)
                {
                    m_readers.add(lrs);
                    lrs.addLogListener(m_console);
                } else if (event.getType() == ServiceEvent.UNREGISTERING)
                {
                    lrs.removeLogListener(m_console);
                    m_readers.remove(lrs);
                }
            }
        }
    };

	
	/**
	 * Creates the plug-in runtime object.
	 * 
	 * Note that instances of plug-in runtime classes are automatically 
	 * created by the platform in the course of plug-in activation. 
	 */	
	public SIBActivator() {
//		try {
//			//URL url = Loader.getResource("log4j.properties");
//			//PropertyConfigurator.configure(url);
//			//logger = Logger.getLogger(SIBActivator.class);
//			//System.out.println ("[" + url.toString() + "] Logger initilized correctly.");
//		} 
//		catch (Exception e) {
//			BasicConfigurator.configure();
//			//logger = Logger.getLogger(SIBActivator.class);
//			//logger.setLevel(Level.DEBUG);
//			//System.out.println ("Found problem trying to initialize Logger: " + e.toString());
//
//			//BasicConfigurator.configure();
//			//logger.info("Activates the SIB bundle");
//		}
	}

	/**
	 * Starts up this plug-in.
	 * 
	 * This method overrides the Plugin class in subclasses that need to do 
	 * something when this plug-in is started. The start class calls the
	 * <code>super()</code> method to ensure that any system requirements 
	 * can be met.
	 * 
	 *  If this method throws an exception, it is taken as an indication that 
	 *  plug-in initialization has failed; as a result, the plug-in will not 
	 *  be activated; moreover, the plug-in will be marked as disabled and 
	 *  ineligible for activation for the duration.
	 *  
	 *  Plug-in startup code should be robust. In the event of a startup 
	 *  failure, the plug-in's shutdown method will be invoked automatically, 
	 *  in an attempt to close open files, etc.
	 *  
	 *  <code>Note 1</code>: This method is automatically invoked by the 
	 *  platform the first time any code in the plug-in is executed.
	 *  
	 *  <code>Note 2</code>: This method is intended to perform simple 
	 *  initialization of the plug-in environment. The platform may terminate 
	 *  initializers that do not complete in a timely fashion.
	 *  
	 *  <code>Note 3</code>: The class loader typically has monitors acquired 
	 *  during invocation of this method. It is strongly recommended that this 
	 *  method avoid synchronized blocks or other thread locking mechanisms, 
	 *  as this would lead to deadlock vulnerability.
	 *  
	 *  <code>Note 4</code>: The supplied bundle context represents the plug-in 
	 *  to the OSGi framework. For security reasons, it is strongly recommended 
	 *  that this object should not be divulged.
	 *  
	 *  <code>Note 5</code>: This method and the stop(BundleContext) may be 
	 *  called from separate threads, but the OSGi framework ensures that both 
	 *  methods will not be called simultaneously.
	 *  
	 *  <strong>Clients must never explicitly call this method.</strong>
	 *   
	 * @see org.eclipse.core.runtime.Plugin#start(BundleContext)
	 */
	@SuppressWarnings({ "static-access", "rawtypes", "unchecked" })
	public void start(BundleContext context) throws Exception {
		super.start(context);
		
		plugin = this;
		
		this.bc = context;
		
		// Get a list of all the registered LogReaderService, and add the console listener
    	ServiceTracker logReaderTracker = new ServiceTracker(context, org.osgi.service.log.LogReaderService.class.getName(), null);
        logReaderTracker.open();
        Object[] readers = logReaderTracker.getServices();
        if (readers != null)
        {
            for (int i=0; i<readers.length; i++)
            {
                LogReaderService lrs = (LogReaderService)readers[i];
                m_readers.add(lrs);
                //specify what LogListener will spit log messages
                lrs.addLogListener(m_console);
            }
        }
        //close the tracker 
        logReaderTracker.close();
       
        // Add the ServiceListener, but with a filter so that we only receive events related to LogReaderService
        String filter = "(objectclass=" + LogReaderService.class.getName() + ")";
        try {
            context.addServiceListener(m_servlistener, filter);
        } 
        catch (InvalidSyntaxException e) {
            System.out.println("ERROR: Could not add service Listeners for LogReaderService.");
        }
        
        // Get a list of all the registered LogService, and add the console listener
        ServiceTracker logServiceTracker = new ServiceTracker(context, org.osgi.service.log.LogService.class.getName(), null);
        logServiceTracker.open();
        Object[] services = logServiceTracker.getServices();
        if (services != null)
        {
            for (int i=0; i < services.length; i++)
            {
            	LogService ls = (LogService)services[i];
            	String className = ls.getClass().getSimpleName();
            	//ARF 04-09-15 ExtendedLogServiceImpl is also available, and if className.equals("LogServiceImpl") instead of contains, the log was nool-> error when start and sib not activated
            	if (ls instanceof LogService && className.contains("LogServiceImpl")){
            		m_services.add(ls);
            		logger = (LogService) ls;
            		logger.log(LogService.LOG_INFO, "Starting the org.smool.sdk.osgi.rest.servlet...");
            		break;
            	}
                
            }
         }

        logServiceTracker.close();

		
		
		try{
			SIBFactory sibManager = new SIBFactory();
			Dictionary<String,String> dict = new Hashtable<String,String>();
			dict.put("SIBMgt", "Semantic Information Broker Management");
			context.registerService(ISIBFactory.class.getName(), sibManager, dict);
			logger.log(LogService.LOG_INFO, "SIB Management services registered.");
			sibFactory = sibManager;
			//logger.debug("SIB Management services registered.");
		} 
		catch(Exception ex){
			ex.printStackTrace();
			System.out.println("logger==null?" + logger == null);
			logger.log(LogService.LOG_INFO, "Cannot start the SIB services:" + ex.getMessage());
			//logger.error("Cannot start the SIB services: " + ex.getMessage(), ex);
			//ex.printStackTrace();
		}
		
		readConfiguration();
		
		// Create and start any sibs in the configuration
		String [] sibs = configuration.getProperty("CREATE_SIBS") != null ? configuration.getProperty("CREATE_SIBS").split(",") : new String [0];
		String [] autostart = configuration.getProperty("AUTOSTART") != null ? configuration.getProperty("AUTOSTART").split(",") : new String [0];
		for (String sib : sibs) {
			int id = sibFactory.create(sib, null);
			for (String autosib : autostart) {
				if (autosib.equals(sib)) {
					sibFactory.getSIB(id).start();
				}
			}
		}
		
	}

	private void readConfiguration() {
		File f = new File("."+File.separator+"configuration"+File.separator+"sib.ini");
		if (f.exists() && f.isFile() && f.canRead()) {
			String prop = null;
			try {
				BufferedReader br = new BufferedReader(new FileReader(f));
				while ((prop = br.readLine()) != null) {
					try {
						String [] keyval = prop.split("=");
						if (keyval.length == 2) {
							if (keyval[0].equals("CREATE_SIBS") || keyval[0].equals("CREATE_TCPIP_GW") ||keyval[0].equals("CREATE_WEBSOCKET_GW") || keyval[0].equals("CREATE_BT_GW") || keyval[0].equals("AUTOSTART")) {
								configuration.put(keyval[0].trim().toUpperCase(), keyval[1].trim());
							}
						}
					}
					catch (NullPointerException e) {
						// Do nothing, probably incorrect file
					}
				}
				br.close();
			} catch (FileNotFoundException e) {
				// If any exception is caught just ignore auto configuration
				configuration.clear();
			} catch (IOException ioe) {
				configuration.clear();
			}
			
		}
	}

	/**
	 * Stops this plug-in.
	 * 
	 * This method overrides the Plugin class in subclasses that need to do 
	 * something when this plug-in is shut down. The stop class calls the
	 * <code>super()</code> method to ensure that any system requirements 
	 * can be met.
	 * 
	 * Plug-in shutdown code should be robust. In particular, this method 
	 * should always make an effort to shut down the plug-in. 
	 * Furthermore, the code should not assume that the plug-in was started 
	 * successfully, as this method will be invoked in the event of a failure 
	 * during startup.
	 * 
	 * <code>Note 1</code>: If a plug-in has been automatically started, 
	 * this method will be automatically invoked by the platform when the 
	 * platform is shut down.
	 * 
	 * <code>Note 2<code>: This method is intended to perform simple 
	 * termination of the plug-in environment. The platform may terminate 
	 * invocations that do not complete in a timely fashion.
	 * 
	 * <code>Note 3</code>: The supplied bundle context represents the plug-in 
	 * to the OSGi framework. For security reasons, it is strongly recommended 
	 * that this object should not be divulged.
	 * 
	 * <code>Note 4</code>: This method and the start(BundleContext) may be 
	 * called from separate threads, but the OSGi framework ensures that both 
	 * methods will not be called simultaneously.
	 * 
	 * <strong>Clients must never explicitly call this method.</strong>
	 * 
	 * @see org.eclipse.core.runtime.Plugin#stop(BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
		
	}

	/**
	 * Gets the current plugin bundle context
	 * @return the bundle context
	 * @see org.osgi.framework.BundleContext
	 */
	public static BundleContext getBundleContext() {
		return bc;
	}

	/**
	 * Gets the current logger
	 * @return the logger
	 * @see org.apache.log4j.Logger
	 */
	public static LogService getLogger() {
		return logger;
	}

	/**
	 * Sets te logger
	 * @param logger the Logger
	 * @see org.apache.log4j.Logger
	 */
	public static void setLogger(LogService logger) {
		SIBActivator.logger = logger;
	}
	
	public static Properties getConfiguration() {
		return configuration;
	}
	
	public static ISIB getSIB(int id) {
		try {
			return sibFactory.getSIB(id);
		} catch (SIBException e) {
			return null;
		}
	}
	
	public static ISIB getSIB(String name) {
		try {
			return sibFactory.getSIB(name);
		} catch (SIBException e) {
			return null;
		}
	}
	
	
	
}
