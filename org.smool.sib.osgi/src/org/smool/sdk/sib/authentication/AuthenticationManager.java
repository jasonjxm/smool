package org.smool.sdk.sib.authentication;

import org.osgi.service.log.LogService;
import org.smool.sdk.sib.SIBActivator;


/**
 * 
 * @author Selex Elsag S.p.A.
 * 
 */
public class AuthenticationManager {
	
	/** The logger */
	private static LogService logger;	
	
	/**
     * The constructor that inits the Authentication Manager for the SIB
     */
	private AuthenticationManager() {
		// Inits the logger
    	logger = SIBActivator.getLogger();	
	}
	
    /**
     * SingletonHolder is loaded on the first execution of AuthenticationManager.getInstance() 
     * or the first access to SingletonHolder.INSTANCE, not before.
     */
    private static class SingletonHolder { 
    	private static final AuthenticationManager INSTANCE = new AuthenticationManager();
    }
  
    /**
     * Gets the instance of the AuthenticationManager singleton
     * @return The AuthenticationManager singleton
     */
    public static AuthenticationManager getInstance() {
      return SingletonHolder.INSTANCE;
    }	
	
    /**
     * Checks the authentication
     * @param username the username
     * @param passwd the password
     * @return <code>true</code> if the username/password pair is authenticated
     */
	public boolean authenticate(String username, String passwd){
		// TODO update this when diameter is working
		//logger.debug("Authentication request for <" + username + "," + passwd + ">");
		logger.log(LogService.LOG_INFO, "Authentication request for <" + username + "," + passwd + ">");
		return true;
	}
	
	/**
	 * Authorizes a node in a given smart space
	 * @param nodeId the node identifier
	 * @param sspace the smart space identifier
	 * @return <code>true</code> if the node is authorized to work on given smart space
	 */
	public boolean isAuthorized(String nodeId, String sspace){
		// TODO update this when diameter is working
		//logger.debug("Authorization request for  node " + nodeId + " in " + sspace);
		logger.log(LogService.LOG_INFO, "Authorization request for  node " + nodeId + " in " + sspace);
		return true;
	}
}
