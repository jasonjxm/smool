/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.sib.data;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * This class represents a SIB Property object
 *  
 * @author Fran Ruiz
 */

public class SIBProperty {

	/** Definition of SIB Property */
	private String name;
	private String value;
	
	private String date;
	
	private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
	
	/**
	 * Constructor with parameters 
	 * @param name a String representing the SIB property name
	 * @param value a String representing the value associated to the property
	 */
	public SIBProperty(String name, String value) {
		this.date = now();
		
		if (name != null) {
			this.name = name;
		}
		
		if (value != null) {
			this.value = value;
		}
	}

	/**
	 * Get method of the property name
	 * @return a String with the property name
	 */
	public String getPropertyName() {
		return name;
	}

	/**
	 * Set method of the subject property
	 * @param subject a String representing the subject
	 */
	public void setPropertyName(String name) {
		this.name = name;
	}

	/**
	 * Get method of the property value
	 * @return a String with the property value
	 */
	public String getPropertyValue() {
		return value;
	}

	/**
	 * Set method of the property value
	 * @param value a String representing the property value
	 */
	public void setPropertyValue(String value) {
		this.value = value;
	}

	/**
	 * Gets the current formatted date
	 * @return a String containing the formatted date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * Allows the equality comparision to another object
	 * @param obj the object to be compared to the current property pair
	 * @return <code>true</code> if the property pair is the same 
	 */
	@Override
	public boolean equals(Object obj) {
        if (obj instanceof SIBProperty) {
        	SIBProperty otherProperty = (SIBProperty) obj;
        	return this.name.equals(otherProperty.name) &&
        		this.value.equals(otherProperty.value);
        } else {
        	return false;
        }
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(name).append(":").append(value).append("\n");
		return sb.toString();
	}

	/**
	 * Gets the current formatted date
	 * @return a String with the formatted date
	 */
	private String now() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		return sdf.format(cal.getTime());
	}
}
