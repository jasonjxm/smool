/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.sib.data;

import java.util.Collection;

/**
 * This interface determines the set of methods needed to be implemented by
 * the classes responsible of the semantic content
 * 
 * @author Fran Ruiz, fran.ruiz@tecnalia.com, ESI
 */
public interface ISIBPropertiesHolderListener {

	/**
	 * Notifies the initialization of the property set
	 * @param properties the initial property set
	 */
	public void initProperties(Collection<SIBProperty> properties);
	
	/**
	 * Resets the content of the SIB
	 */
	public void reset();	
}
