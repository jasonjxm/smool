/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.sib.mgt;

import java.util.HashMap;
import java.util.Hashtable;

import org.osgi.framework.ServiceRegistration;
import org.osgi.service.log.LogService;
import org.smool.sdk.sib.SIBActivator;
import org.smool.sdk.sib.exception.SIBException;
import org.smool.sdk.sib.service.ISIB;
import org.smool.sdk.sib.service.ISIBFactory;
import org.smool.sdk.sib.service.IViewer;
import org.smool.sdk.sib.ssap.SIB;
import org.smool.sdk.sib.viewer.Viewer;


/**
 * This class implements the Server Management Interface
 * 
 * @author Cristina Lopez, ESI, cristina.lopez@tecnalia.com
 */
public class SIBFactory implements ISIBFactory {

	/** The list of created SIBs */
	private HashMap<Integer,SIBRegistration> servers;
	private HashMap<String,Integer> nameMap;
	
	/** The Logger */
	private static LogService logger;
	
	/**
	 * The Constructor 
	 */
	public SIBFactory() {
		servers = new HashMap<Integer, SIBRegistration>();
		nameMap = new HashMap<String, Integer>();
		logger = SIBActivator.getLogger();
	}
	
	/**
	 * Creates a new SIB instance
	 * @param props the set of parameters
	 * @return an unique SIB identifier obtained from the properties
	 * @throws SIBException when the SIB cannot be created
	 */
	//@SuppressWarnings("rawtypes")
	public int create(String name, Hashtable<String, Object> props) throws SIBException {
		
		try{
			// Creates a new SIB
			SIB sib = new SIB(name);
			
			// Add name property to the SIB.
			Hashtable<String, Object> sibProperties = new Hashtable<String, Object>();
			if(props != null) {
				sibProperties.putAll(props);
			}
			sibProperties.put(sib.getName(), sib.getId());
			
			// Registers the SIB Service.
			ServiceRegistration<?> sr1 = SIBActivator.getBundleContext().registerService(ISIB.class.getName(), sib, sibProperties);
			//logger.debug("SIB server registered.");
			logger.log(LogService.LOG_INFO, "SIB server registered");
			
			// Add new property (same as in SIB registration)
			Hashtable<String, Object> viewerProps = new Hashtable<String, Object>();
			viewerProps.putAll(sibProperties); 
			viewerProps.put(sib.getName(), "Viewer");

			// Registers the Viewer services
			Viewer viewService = new Viewer(sib);
			sib.addViewerListener(viewService); // adds the viewer to the listeners
			ServiceRegistration<?> sr2 = SIBActivator.getBundleContext().registerService(IViewer.class.getName(), viewService, viewerProps);	
			//logger.debug("Viewer service registered.");
			logger.log(LogService.LOG_INFO, "Viewer service registered");
			
			// Add to cache.
			SIBRegistration sr = new SIBRegistration();
			sr.setSIB(sib);
			sr.setSIBRegistration(sr1);
			sr.setViewerRegistration(sr2);
			
			servers.put(sib.getId(), sr);
			nameMap.put(name, sib.getId());

			//logger.debug("Created and registered new instance of a SIB server");
			logger.log(LogService.LOG_INFO, "Created and registered new instance of a SIB server");
			
			return sib.getId();
		} 
		catch(Exception e){
			//logger.error("Cannot create a new instance of the SIB: " + e.getMessage(), e.getCause());
			logger.log(LogService.LOG_ERROR, "Cannot create a new instance of the SIB: " + e.getMessage(), e.getCause());
			//e.printStackTrace();
			throw new SIBException(e.getMessage(), e.getCause());
		}
	}

	/**
	 * Destroys a SIB instance
	 * @param sibId the SIB identifier
	 * @throws SIBException when the SIB instance cannot be destroyed
	 */
	public void destroy(int sibId) throws SIBException {
		try {
			SIBRegistration sr = servers.get(sibId);
			if(sr.getSIB().isRunning()) {
				sr.stop();
			} 
			sr.unregister();
			logger.log(LogService.LOG_INFO, "SIB services unregistered.");
			//logger.debug("SIB services unregistered.");
			
			servers.remove(sibId);
			nameMap.remove(sr.getSIB().getName());
			logger.log(LogService.LOG_INFO, "SIB server deleted");
			//logger.debug("SIB server deleted");
		} 
		catch(Exception e) {
			logger.log(LogService.LOG_ERROR, "Cannot destroy a SIB instance: " + e.getMessage(), e.getCause());
			//logger.error("Cannot destroy a SIB instance: " + e.getMessage(), e.getCause());
			//e.printStackTrace();
			throw new SIBException(e.getMessage(), e.getCause());
		}
	}

	/**
	 * Gets a SIB for a given identifier
	 * @param sibId the SIB identifier
	 * @return a ISIB representing a SIB
	 * @throws SIBException 
	 * @throws SIBException when a SIB instance cannot be obtained
	 */
	public ISIB getSIB(int sibId) throws SIBException{
		ISIB _return = null;
		try{
			_return = servers.get(sibId).getSIB();
		} 
		catch(Exception e) {
			
			logger.log(LogService.LOG_ERROR, "Cannot get a SIB identified by " + sibId +": " + e.getMessage(), e.getCause());		
			//logger.error("Cannot get a SIB identified by " + sibId +": " + e.getMessage(), e.getCause());
			//e.printStackTrace();
			throw new SIBException(e.getMessage(), e.getCause());
		}
		return _return;
	}

	@Override
	public ISIB getSIB(String name) throws SIBException {
		ISIB _return = null;
		try{
			_return = servers.get(nameMap.get(name)).getSIB();
		} 
		catch(Exception e) {
			
			logger.log(LogService.LOG_ERROR, "Cannot get a SIB identified by " + name +": " + e.getMessage(), e.getCause());		
			//logger.error("Cannot get a SIB identified by " + sibId +": " + e.getMessage(), e.getCause());
			//e.printStackTrace();
			throw new SIBException(e.getMessage(), e.getCause());
		}
		return _return;
	}
}