/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/

package org.smool.sdk.sib.model.request;

import java.util.Collection;

import org.osgi.service.log.LogService;
import org.smool.sdk.sib.SIBActivator;
import org.smool.sdk.sib.data.Session;
import org.smool.sdk.sib.ssap.SIB;
import org.smool.sdk.ssapmessage.SSAPMessage;
import org.smool.sdk.ssapmessage.SSAPMessage.TransactionType;
import org.smool.sdk.ssapmessage.SSAPMessageRequest;
import org.smool.sdk.ssapmessage.SSAPMessageResponse;
import org.smool.sdk.ssapmessage.parameter.SSAPMessageParameter;
import org.smool.sdk.ssapmessage.parameter.SSAPMessageStatusParameter;

/**
 * This class is a request for executing an LEAVE REQUEST <i>LEAVE REQUEST</i>
 * notifies the SIB that the node is going to leave the smart space.
 * 
 * @author Fran Ruiz, ESI
 * @see org.smool.sdk.sib.model.request.NotifiableRequest
 * 
 */
public class LeaveRequest extends NotifiableRequest {

	/** The logger */
	private static LogService logger;

	/**
	 * Constructor for synchronous LEAVE requests
	 * 
	 * @param request the SSAP Message request containing the LEAVE Request
	 * @param sib     the current SIB
	 */
	public LeaveRequest(SSAPMessageRequest request, SIB sib) {

		super(request, sib);

		// Inits the logger
		logger = SIBActivator.getLogger();

	}

	/**
	 * Gets the request type
	 * 
	 * @return the TransactionType associated to this request (LEAVE)
	 */
	@Override
	public TransactionType getRequestType() {
		return SSAPMessage.TransactionType.LEAVE;
	}

	/**
	 * Process a single LEAVE transaction. <i>LEAVE CONFIRM</i> confirms the leaving
	 * of the smart space to the node. The smart space will not communicate with the
	 * node after <i>LEAVE CONFIRM</i> message (until another <i>JOIN REQUEST</i>).
	 * 
	 * @return SSAPMessageResponse with the response after processing the request.
	 */
	public SSAPMessageResponse processRequest() {
		// Prepare the response SSAP Message
		// Creates the SSAP Response Message
		response = new SSAPMessageResponse(request.getNodeId(), request.getSpaceId(), request.getTransactionId(),
				request.getTransactionType());

		session = sib.getSessionManager().removeNode(request.getNodeId());

		if (session == null) { // The node wasn't associated to this SIB

			logger.log(LogService.LOG_ERROR, "Node " + request.getNodeId() + " was not associated to this SIB");
			// logger.error("Node " + request.getNodeId() + " was not associated to this
			// SIB");

			SSAPMessageParameter param = new SSAPMessageStatusParameter(
					SSAPMessageStatusParameter.SIBStatus.STATUS_KP_MESSAGE_SYNTAX);

			response.addParameter(param);
		} else { // The node was successfully leave the SIB
			// Removes the model
			sib.getSemanticModel().remove(request.getNodeId());
			// OntModel removedModel = sib.getSemanticModel().remove(request.getNodeId());
			// if (removedModel != null) {
			// sib.getViewerListenerManager().removeModel(removedModel);
			// }

			// Removes the associated subscriptions
			Collection<Long> subscriptionIds = sib.getSubscriptionManager().removeSubscriptions(request.getNodeId());

			if (subscriptionIds.size() > 0) {
				sib.getViewerListenerManager().removeSubscription(subscriptionIds, request.getNodeId());
			}

			sib.getViewerListenerManager().removeSession(new Session(request.getNodeId()));

			// logger.debug("Node " + request.getNodeId() + " has successfully leave the
			// SIB");

			SSAPMessageParameter param = new SSAPMessageStatusParameter(SSAPMessageStatusParameter.SIBStatus.STATUS_OK);

			response.addParameter(param);

		}

		return response;
	}
}
