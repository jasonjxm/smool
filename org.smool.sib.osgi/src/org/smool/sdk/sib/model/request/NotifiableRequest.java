/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.sib.model.request;

import org.smool.sdk.sib.model.semantic.SemanticModel;
import org.smool.sdk.sib.ssap.SIB;
import org.smool.sdk.ssapmessage.SSAPMessageRequest;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;


/**
 * This abstract class is the responsible for checking the subscriptions
 * and create as many indications as needed. This abstract class must
 * be extended for all requests that modifies the semantic model content.
 * 
 * @author Fran Ruiz, fran.ruiz@tecnalia.com, ESI
 * @author Raul Otaolea, raul.otaolea@tecnalia.com, ESI 
 * @see org.smool.sdk.sib.model.request.Request
 *
 */
public abstract class NotifiableRequest extends Request {

	/**
	 * The default constructor
	 * @param request the SSAP Message request
	 * 
	 */
	public NotifiableRequest(SSAPMessageRequest request, SIB sib) {
		super(request, sib);
	}

	/**
	 * Executes the operation indicated in the SSAPMessageRequest.
	 * After doing this, checks the subscriptions and generates as
	 * many indications as needed.
	 */
	@Override
	public void run() {
		super.run();
		sib.getSubscriptionManager().checkSubscriptions(sib);
	}
	
	/**
	 * Checks if there are updates in SIB internal properties
	 * @param model the incoming model for insert/delete/update
	 * @return <code>true</code> if the internal model has been updated
	 */
	protected boolean hasSIBInternalUpdates(OntModel model) {
		StmtIterator it = model.listStatements();
		while(it.hasNext()) {
			Statement stmt = it.next();
		    Property  predicate = stmt.getPredicate();   // get the predicate
			
		    if (predicate.getNameSpace().equals(SemanticModel.SIBINTERNAL_NAMESPACE)) {
		    	return true;
		    }			
		}
		return false;
	}
}
