/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/

package org.smool.sdk.sib.model.request;

import org.osgi.service.log.LogService;
import org.smool.sdk.sib.SIBActivator;
import org.smool.sdk.sib.authentication.AuthenticationManager;
import org.smool.sdk.sib.data.Session;
import org.smool.sdk.sib.model.session.SIBSession;
import org.smool.sdk.sib.ssap.SIB;
import org.smool.sdk.ssapmessage.SSAPMessage;
import org.smool.sdk.ssapmessage.SSAPMessage.TransactionType;
import org.smool.sdk.ssapmessage.SSAPMessageRequest;
import org.smool.sdk.ssapmessage.SSAPMessageResponse;
import org.smool.sdk.ssapmessage.parameter.SSAPMessageCredentialsParameter;
import org.smool.sdk.ssapmessage.parameter.SSAPMessageParameter;
import org.smool.sdk.ssapmessage.parameter.SSAPMessageStatusParameter;

/**
 * This class is a request for executing an JOIN REQUEST If the credentials
 * given in the <i>credentials</i> parameter are accepted by the SIB, the node
 * will become a member of the smart space. The format of credentials is not
 * currently defined, and the join procedure may change in the future.
 * 
 * @author Fran Ruiz, ESI
 * @see org.smool.sdk.sib.model.request.Request
 * 
 */
public class JoinRequest extends Request {

	/** The logger */
	private static LogService logger;

	/**
	 * Constructor for synchronous join requests
	 * 
	 * @param proxy   the proxy associated to the KP
	 * @param request the SSAP Message request containing the JOIN Request
	 * @param sib     the current SIB
	 */
	public JoinRequest(SSAPMessageRequest request, SIB sib) {
		super(request, sib);

		// Inits the logger
		logger = SIBActivator.getLogger();

		if (request != null) {
			if (request.getNodeId() != null) {
				// Create the session with the information of the client proxy
				SIBSession session = new SIBSession(request.getNodeId());

				super.setSession(session);
			}
		} else {

		}
	}

	/**
	 * Gets the request type
	 * 
	 * @return the TransactionType associated to this request (JOIN)
	 */
	@Override
	public TransactionType getRequestType() {
		return SSAPMessage.TransactionType.JOIN;
	}

	/**
	 * Process a single JOIN transaction. The parameter <i>status</i> will signal
	 * the success of the join operation. If joining was successful, status will be
	 * <i>sib:Status_OK</i>, any other value signals an error in joining. A reason
	 * for failure may be that the node can not be accepted as a member because of
	 * lacking credentials, or a technical error in the SIB.
	 * 
	 * @return SSAPMessageResponse with the response after processing the request.
	 */
	public SSAPMessageResponse processRequest() {
		// The response
		SSAPMessageResponse response = null;

		/***************************************************************************************
		 *
		 * Modification made by AVK 06/15/2011 To avoid KPs not being able to reconnect
		 * when connection is interrupted (unplugged or whatever the reason) and they
		 * try to re-connect It was checking if the session already existed, which it
		 * did, and thus didn't allow to reconnect. Although it works, we don't
		 * recommend it since this is a very nasty feature (Not removig a KP when
		 * dropped and allowing to reconnect...)
		 * 
		 */
		// Adds the node to joined nodes
		// boolean nodeInserted =
		// sib.getSessionManager().addNode(request.getNodeId(), session);

		SIBSession cur = sib.getSessionManager().getSession(request.getNodeId());

		if (cur == null) {
			sib.getSessionManager().addSession(request.getNodeId(), session);
		} else {
			this.session = cur;
			logger.log(LogService.LOG_DEBUG, "[" + sib.getName() + "] Already exists");
			// logger.debug("[" + sib.getName() + "] Already exists");
		}

		// Obtain credentials parameter.
		SSAPMessageCredentialsParameter credentials = request.getCredentialsParameter();
		// check the null value
		boolean isAuthenticated;
		boolean isAuthorized;

		if (credentials != null) {
			String username = credentials.getUsername();
			String password = credentials.getPassword();
			if (username != null && !username.equals("") && password != null && !password.equals("")) {
				AuthenticationManager manager = AuthenticationManager.getInstance();
				isAuthenticated = manager.authenticate(username, password);

				// if authentication was success, check the authorization to write in current
				// sib by the nodeId
				if (isAuthenticated) {
					isAuthorized = AuthenticationManager.getInstance().isAuthorized(request.getNodeId(),
							request.getSpaceId());
				} else {
					isAuthorized = false;
				}
			} else {
				// if not available username and password, we temporary include access to the
				// SIB till credentials, authorization
				// and authentication is spread used.
				isAuthenticated = true;
				isAuthorized = true;
			}
		} else {
			// if not available credentials.
			isAuthenticated = true;
			isAuthorized = true;
		}

		if (isAuthenticated && isAuthorized) {
			sib.getViewerListenerManager().addSession(new Session(request.getNodeId()));

			// Creates the SSAP Response Message
			response = new SSAPMessageResponse(request.getNodeId(), request.getSpaceId(), request.getTransactionId(),
					request.getTransactionType());

			SSAPMessageParameter param = new SSAPMessageStatusParameter(SSAPMessageStatusParameter.SIBStatus.STATUS_OK);

			response.addParameter(param);
		} else {
			logger.log(LogService.LOG_DEBUG, "Access not granted to node" + request.getNodeId());
			// logger.info("Access not granted to node" + request.getNodeId());

			response = new SSAPMessageResponse(request.getNodeId(), request.getSpaceId(), request.getTransactionId(),
					request.getTransactionType());

			SSAPMessageParameter param = new SSAPMessageStatusParameter(
					SSAPMessageStatusParameter.SIBStatus.STATUS_ACCESS_DENIED);

			response.addParameter(param);
		}

//		} else {
//			logger.info("Cannot join the node");
//			
//			response = new SSAPMessageResponse(
//					request.getNodeId(), request.getSpaceId(), 
//					request.getTransactionId(), request.getTransactionType());
//			
//			SSAPMessageParameter param = new SSAPMessageStatusParameter(
//					SSAPMessageStatusParameter.SIBStatus.STATUS_KP_REQUEST);
//			
//			response.addParameter(param);
//		}

		/***************************************************************************************
		 *
		 * End of Modification made by AVK 06/15/2011 To avoid KPs not being able to
		 * reconnect when connection is interrupted (unplugged or whatever the reason)
		 * and they try to re-connect It was checking if the session already existed,
		 * which it did, and thus didn't allow to reconnect. Although it works, we don't
		 * recommend it since this is a very nasty feature (Not removig a KP when
		 * dropped and allowing to reconnect...)
		 * 
		 */

		return response;
	}

}
