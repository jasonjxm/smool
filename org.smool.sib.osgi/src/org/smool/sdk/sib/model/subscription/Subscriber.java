package org.smool.sdk.sib.model.subscription;

import java.util.ArrayList;

import org.smool.sdk.sib.exception.EncodingNotSupportedException;
import org.smool.sdk.sib.exception.TransformationException;
import org.smool.sdk.sib.model.query.SMOOLResultSet;
import org.smool.sdk.sib.util.EncodingFactory;
import org.smool.sdk.ssapmessage.parameter.SSAPMessageRDFParameter.TypeAttribute;


public class Subscriber {

	private TypeAttribute originalEncoding;
	private String originalQuery;
	private SMOOLResultSet results;
	private ArrayList<SubscribedKP> subscribedKPs;
	
	public Subscriber(TypeAttribute originalEncoding, 
			String originalQuery, 
			SMOOLResultSet results) {
		this.originalEncoding = originalEncoding;
		this.originalQuery = originalQuery;
		this.results = results;
		this.subscribedKPs = new ArrayList<SubscribedKP>();
	}
	
	public boolean addSubscribedKP(SubscribedKP kp) {
		if (!subscribedKPs.contains(kp)) {
			return subscribedKPs.add(kp);
		}
		return false;
	}
	
	public boolean removeSubscribedKP(SubscribedKP kp) {
		return subscribedKPs.remove(kp);
	}

	public SubscribedKP getSubscribedKP(String kpId) {
		for (SubscribedKP kp : subscribedKPs) {
			if (kp.getSession().getNodeId().equals(kpId)) {
				return kp;
			}
		}
		return null;
	}
	
	/**
	 * Gets the list of subscribed nodes
	 * @return the list of subscribed nodes
	 */
	public ArrayList<SubscribedKP> listSubscribedKPs() {
		return subscribedKPs;
	}	

	public boolean hasSubscribedKPs() {
		return subscribedKPs.size() > 0;
	}

	public TypeAttribute getOriginalEncoding() {
		return originalEncoding;
	}
	
	public SMOOLResultSet getResults() {
		return results;
	}
	
	public String getFormattedResults() throws TransformationException, EncodingNotSupportedException {
		return EncodingFactory.getInstance().getTransformer(originalEncoding).toOriginalEncoding(originalQuery, results);
	}

	public String getOriginalQuery() {
		return originalQuery;
	}

	public void updateResultSet(SMOOLResultSet results) {
		this.results = results;
	}
}
