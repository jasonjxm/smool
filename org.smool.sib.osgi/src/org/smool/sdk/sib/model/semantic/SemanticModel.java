/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/

package org.smool.sdk.sib.model.semantic;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.osgi.service.log.LogService;
import org.smool.sdk.sib.SIBActivator;
import org.smool.sdk.sib.data.Triple;
import org.smool.sdk.sib.data.util.TripleUtil;
import org.smool.sdk.sib.exception.SSAPSyntaxException;
import org.smool.sdk.sib.exception.SemanticModelException;
import org.smool.sdk.sib.model.query.SMOOLResultSet;
import org.smool.sdk.sib.ssap.SIB;
import org.smool.sdk.sib.util.EncodingFactory;
import org.smool.sdk.ssapmessage.parameter.SSAPMessageRDFParameter.TypeAttribute;

import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.AnonId;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.SimpleSelector;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import com.hp.hpl.jena.vocabulary.RDF;

/**
 * This class contains the Jena Model containing all the ontology information
 * stored in a Jena Ontology Model. It also provides the methods to creating,
 * adding, removing and querying about the model.
 * 
 * @author Alejandro Villamarin, alejandro.villamarin@tecnalia.com
 */
public class SemanticModel {

	/** Creates the basic variables for the model */
	private OntModel model;

	/** The logger */
	private static LogService logger;

	/**
	 * Reference to the SIB
	 */
	private SIB sib;

	/** The list of models for each nodeId **/
	private HashMap<String, OntModel> smartAppModels;

	/** The namespaces */
	private HashMap<String, String> nsPrefixes;

	/** BNodes mapping */
	private HashMap<String, String> bNodesMapping;

	private static final String DEFAULT_NAMESPACE = "http://emb1.esilab.org/sofia/ontology/";
	private static int bNodeSequential = 1;

	/**
	 * URL where the OWL with the internal properties of the SIB file exist
	 */
	public static final String SIBINTERNAL_ONTOLOGY = "http://emb1.esilab.org/sofia/adk/internal/smool-core.owl";

	public static final String SIBINTERNAL_NAMESPACE = "http://sofia.org/sib_internal#";
	public static final String SIBINTERNAL_ONTOLOGYNAME = "smool-core.owl";

	/**
	 * The constructor that inits the Semantic Model for the SIB
	 */
	public SemanticModel(SIB s) {

		sib = s;
		// Inits the logging
		logger = SIBActivator.getLogger();

		// Creates a void model
		// we use a simple OWL-Full stored in memory with transitive class hierarchy
		// inference
		model = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM_TRANS_INF);
		// model = ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM_TRANS_INF);

		smartAppModels = new HashMap<String, OntModel>();
		nsPrefixes = new HashMap<String, String>();

		bNodesMapping = new HashMap<String, String>();

		// this.createSIBInternalProperties();

	}

	/**
	 * Inserts into the SIB the smool-core ontology, located in the update site
	 * 
	 * @return TRUE if SmooL core ontology was inserted correctly
	 */
	public boolean loadOntologyCoreModel() {

		if (model != null) {
			try {
				InputStream is = this.getClass().getClassLoader().getResourceAsStream(SIBINTERNAL_ONTOLOGYNAME);

				TypeAttribute encoding = TypeAttribute.RDFXML;

				if (encoding == null || is == null) {
					throw new SSAPSyntaxException("Insert graph or encoding is null");
				}

				// Creates a new model
				OntModel model = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM, null);

				String insertContent = null;
				String modelEncoding = null;

				java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
				String ret = s.hasNext() ? s.next() : "";

				insertContent = EncodingFactory.getInstance().getTransformer(encoding).toRDFXML(ret);
				if (insertContent != null) {
					modelEncoding = "RDF/XML";
				}

				// Transformation of the String to a InputStream
				ByteArrayInputStream iis = new ByteArrayInputStream(insertContent.getBytes());
				model.read(iis, null, modelEncoding);

				// Closes the input stream
				if (iis != null) {
					iis.close();
				}

				// New functionality: BNode detection+creation of new bnodes resources
				renameBNodes2NamedResources(model, sib.getName());

				boolean modelAdded = addModel(model);

				if (modelAdded) {
					// Notifies the model listeners
					sib.getViewerListenerManager().addModel("smool_core_ontology", model);
					return true;

				} else {
					logger.log(LogService.LOG_ERROR,
							"There was an error trying to include Smool Core Ontology. Cannot create SIB");
					return false;
				}
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		logger.log(LogService.LOG_ERROR, "Model is null. Cannot proceed. Cannot create SIB");
		return false;
	}

	/**
	 * Gets the model associated to a node
	 * 
	 * @param nodeId the node identifier
	 * @return the OntModel for the node
	 */
	public OntModel getModel(String nodeId) {
		logger.log(LogService.LOG_DEBUG, "Getting the model for the node " + nodeId);
		// logger.debug("Getting the model for the node " + nodeId);

		if (smartAppModels.containsKey(nodeId)) {
			OntModel previousModel = smartAppModels.get(nodeId);
			return previousModel;
		} else {
			return null;
		}
	}

	/**
	 * Queries if a node has an associated semantic model
	 * 
	 * @param nodeId the node identifier
	 * @return <code>true</code> if the node has an associated model
	 */
	public boolean hasModel(String nodeId) {
		return smartAppModels.containsKey(nodeId);
	}

	/**
	 * Adds a submodel to the current model
	 * 
	 * @param nodeId   the node identifier
	 * @param submodel the model to be included
	 * @return <code>true</code> if the model is successfully loaded
	 */
	public boolean addModel(String nodeId, OntModel submodel) {
		try {
			model.add(submodel);

			// Getting the namespace prefixes
			Map<String, String> map = submodel.getNsPrefixMap();

			// Set the namespace prefixes
			for (String key : map.keySet()) {
				String value = map.get(key);
				model.setNsPrefix(key, value);
			}

			// Getting the namespace prefixes
			Map<String, String> parentMap = model.getNsPrefixMap();

			// Set the namespace prefixes
			for (String key : parentMap.keySet()) {
				String value = parentMap.get(key);
				submodel.setNsPrefix(key, value);
			}

			// model.write(System.out, "RDF/XML");

			if (smartAppModels.containsKey(nodeId)) {
				OntModel nodeModel = smartAppModels.get(nodeId);
				nodeModel.add(submodel);
			} else {
				smartAppModels.put(nodeId, submodel);
			}

			nsPrefixes.putAll(model.getNsPrefixMap());

			return true;
		} catch (Exception ex) {
			logger.log(LogService.LOG_ERROR,
					"An error arised during addition of the model. Reason: " + ex.getMessage() + ex.getCause());
			// logger.error("An error arised during addition of the model");
			// ex.printStackTrace();
			return false;
		}
	}

	/**
	 * Adds a submodel to the current model
	 * 
	 * @param submodel the model to be included
	 * @return <code>true</code> if the model is successfully loaded
	 */
	public boolean addModel(OntModel submodel) {
		try {
			model.add(submodel);

			// Getting the namespace prefixes
			Map<String, String> map = submodel.getNsPrefixMap();

			// Set the namespace prefixes
			for (String key : map.keySet()) {
				String value = map.get(key);
				model.setNsPrefix(key, value);
			}

			// Getting the namespace prefixes
			Map<String, String> parentMap = model.getNsPrefixMap();

			// Set the namespace prefixes
			for (String key : parentMap.keySet()) {
				String value = parentMap.get(key);
				submodel.setNsPrefix(key, value);
			}
			nsPrefixes.putAll(model.getNsPrefixMap());
			return true;
		} catch (Exception ex) {
			logger.log(LogService.LOG_ERROR,
					"An error arised during addition of the model. Reason: " + ex.getMessage() + ex.getCause());
			// logger.error("An error arised during addition of the model");
			// ex.printStackTrace();
			return false;
		}
	}

	/**
	 * Updates the submodel into the current model
	 * 
	 * @param nodeId       the node identifier
	 * @param insertModel  the model to be included
	 * @param removalModel the model to be removed
	 * @return <code>true</code> if the model is successfully loaded
	 */
	public boolean updateModel(String nodeId, OntModel insertModel, OntModel removalModel) {
		model.remove(removalModel);

		model.add(insertModel);

		// Getting the namespace prefixes
		Map<String, String> map = removalModel.getNsPrefixMap();

		// Set the namespace prefixes
		for (String key : map.keySet()) {
			model.removeNsPrefix(key);
		}

		// Getting the namespace prefixes
		map = insertModel.getNsPrefixMap();

		// Set the namespace prefixes
		for (String key : map.keySet()) {
			String value = map.get(key);
			model.setNsPrefix(key, value);
		}

		// Getting the namespace prefixes
		Map<String, String> parentMap = model.getNsPrefixMap();

		// Set the namespace prefixes
		for (String key : parentMap.keySet()) {
			String value = parentMap.get(key);
			insertModel.setNsPrefix(key, value);
			removalModel.setNsPrefix(key, value);
		}

		// model.write(System.out, "RDF/XML");

		if (smartAppModels.containsKey(nodeId)) {
			OntModel nodeModel = smartAppModels.get(nodeId);
			nodeModel.remove(removalModel);
			nodeModel.add(insertModel);
			smartAppModels.put(nodeId, nodeModel);
		} else {
			logger.log(LogService.LOG_ERROR,
					"There was a problem in updating the model, because the model was not previously included.");
			// logger.error("There was a problem in updating the model, " +
			// "because the model was not previously included.");
		}
		// smartAppModels.put(nodeId, submodel);
		nsPrefixes.putAll(model.getNsPrefixMap());

		return true;
	}

	/**
	 * Removes a submodel from the current model
	 * 
	 * @param nodeId   the node identifier
	 * @param submodel the model to be removed
	 * @return <code>true</code> if the model is successfully removed
	 */
	public ArrayList<Statement> removeModel(String nodeId, OntModel submodel) {
		ArrayList<Statement> removeStatements = new ArrayList<Statement>();

		// Getting the namespace prefixes
		Map<String, String> parentMap = model.getNsPrefixMap();

		// Set the namespace prefixes
		for (String key : parentMap.keySet()) {
			String value = parentMap.get(key);
			submodel.setNsPrefix(key, value);
		}

		StmtIterator it = submodel.listStatements();

		while (it.hasNext()) { // Adds to the remove statements the ones that are not rdf:type
			Statement stmt = it.next();
			logger.log(LogService.LOG_DEBUG,
					"Stmt ns:" + stmt.getPredicate().getNameSpace() + " id:" + stmt.getPredicate().getLocalName());
			// logger.debug("Stmt ns:" + stmt.getPredicate().getNameSpace() + " id:" +
			// stmt.getPredicate().getLocalName());
			if (!(stmt.getPredicate().equals(RDF.type))) {
				if (model.contains(stmt)) {
					logger.log(LogService.LOG_DEBUG, "Individual statement to remove: " + stmt.toString());
					// logger.debug("Individual statement to remove: " + stmt.toString());
					removeStatements.add(stmt);
				}
			}
		}

		// Remove the statements
		model.remove(removeStatements);

		/**
		 * Remove the individuals that were in the incoming model as subject but do not
		 * have any other properties
		 */
		it = submodel.listStatements();

		while (it.hasNext()) {
			Statement stmt = it.next();

			if (!removeStatements.contains(stmt)) {
				Resource subjectResource = stmt.getSubject();

				Resource objectResource = null;

				RDFNode object = stmt.getObject();

				if (object instanceof Resource) {
					objectResource = (Resource) object;
				}

				ExtendedIterator<Individual> individuals = model.listIndividuals();

				while (individuals.hasNext()) {
					Individual individual = individuals.next();

					if (individual.equals(subjectResource)) {
						// the individual should have at least one property [IndividualId, rdf:type,
						// ClassId]
						if (!(individual.listProperties().toList().size() > 1)) {
							logger.log(LogService.LOG_DEBUG, "Deleting resource " + subjectResource.getLocalName());
							// logger.debug("Deleting resource " + subjectResource.getLocalName());
							model.remove(stmt);
							removeStatements.add(stmt);
						}
					}

					if (individual.equals(objectResource)) {
						// the individual should have at least one property [IndividualId, rdf:type,
						// ClassId]
						if (!(individual.listProperties().toList().size() > 1)) {
							StmtIterator objectStatements = model
									.listStatements(new SimpleSelector(objectResource, null, (RDFNode) null));

							while (objectStatements.hasNext()) {
								Statement objectStatement = objectStatements.next();
								model.remove(objectStatement);
								removeStatements.add(objectStatement);
							}
						}
					}
				}
			}
		}

		// update the model associated to the kp
		if (smartAppModels.containsKey(nodeId)) {
			try {
				OntModel previousModel = smartAppModels.get(nodeId);
				previousModel.remove(removeStatements);
				smartAppModels.put(nodeId, previousModel);
			} catch (Exception ex) {
				logger.log(LogService.LOG_ERROR,
						"Cannot add model for node " + nodeId + " . Reason: " + ex.getMessage() + ex.getCause());
				// logger.error("Cannot add model for node " + nodeId);
				// logger.error("Reason: " + ex.toString());
			}
			logger.log(LogService.LOG_DEBUG, "The model for node " + nodeId + " is removed");
			// logger.debug("The model for node " + nodeId + " is removed");

			nsPrefixes.putAll(model.getNsPrefixMap());

			return removeStatements;
		} else {
			logger.log(LogService.LOG_INFO, "Cannot remove the model for node " + nodeId);
//			logger.info("Cannot remove the model for node " + nodeId);
//			logger.info("Reason: Node has no model");
			return new ArrayList<Statement>();
		}
	}

	/**
	 * Removes a submodel from the current model
	 * 
	 * @param nodeId the node identifier
	 * @return the removed OntModel associated to the node
	 */
	public OntModel remove(String nodeId) {
		// logger.debug("Removes a submodel to the current model");

		if (smartAppModels.containsKey(nodeId)) {
			OntModel removedModel = null;

			try {
				// THE DATA IS NOW PERSISTENT
//				OntModel saModel = smartAppModels.get(nodeId);
//				
//				// note: only removes individuals
//				ExtendedIterator<Individual> it = saModel.listIndividuals();
//				
//				while (it.hasNext()) {
//					Individual individual = (Individual) it.next();
//					
//					Individual sharedIndividual = model.getIndividual(individual.getURI());
//					StmtIterator si = sharedIndividual.listProperties();
//
//					model.remove(si);
//				}

				// removes the node from the models
				removedModel = smartAppModels.remove(nodeId);

			} catch (Exception ex) {
				logger.log(LogService.LOG_ERROR,
						"Cannot remove the model for node " + nodeId + ". Reason: " + ex.getMessage() + ex.getCause());
//				logger.error("Cannot remove model for node " + nodeId);
//				logger.error("Reason: " + ex.toString());
//				ex.printStackTrace();
			}

			// logger.debug("The model for node " + nodeId + " is removed");
			return removedModel;
		} else {
			logger.log(LogService.LOG_ERROR, "Cannot remove the model for node " + nodeId);
//			logger.info("Cannot remove the model for node " + nodeId);
//			logger.info("Reason: Node has no model");
			return null;
		}

	}

	/**
	 * Queries about the SIB semantic model
	 * 
	 * @param sparqlQuery the query to send to the semantic model
	 * @param encoding    the encoding used in the query (RDF-XML, M3, WQL)
	 * @return the response
	 * @throws Exception
	 */
	public SMOOLResultSet query(String sparqlQuery) throws SemanticModelException {

		SMOOLResultSet results = null;

		try {
			Query jenaQuery = QueryFactory.create(sparqlQuery);

			// Execute the query and obtain results
			QueryExecution queryExec = QueryExecutionFactory.create(jenaQuery, model);

			try {
				ResultSet resultSet = queryExec.execSelect();

				// here we have the solution in a list
				results = new SMOOLResultSet(resultSet);

				// String result = SOFIAResultSetFormatter.asXMLString(results);
				// logger.debug("Obtained the following result(XMLString): \n" + result);
			} catch (Exception ex) {
				// ex.printStackTrace();
				logger.log(LogService.LOG_ERROR, "The query cannot be executed");
				// logger.error("The query cannot be executed");
				throw new SemanticModelException("Query cannot be executed due to " + ex.getMessage());
			} finally {
				queryExec.close();
			}

			return results;
		} catch (SemanticModelException ex) {
			logger.log(LogService.LOG_ERROR, "There was a SemanticModelException raised processing the Query. Reason: "
					+ ex.getMessage() + ex.getCause());
			// ex.printStackTrace();
			throw new SemanticModelException("Cannot execute query");
		} catch (Exception ex) {
			logger.log(LogService.LOG_ERROR,
					"There was an Exception raised processing the Query. Reason: " + ex.getMessage() + ex.getCause());
			// ex.printStackTrace();
			throw new SemanticModelException("Cannot create query " + sparqlQuery);
		}
	}

	/**
	 * Gets the bNodes for a model
	 * 
	 * @param model   the OntModel
	 * @param sibName the SIB name
	 * @return a String with the blank nodes
	 */
	public HashMap<String, String> renameBNodes2NamedResources(OntModel model, String sibName) {
		HashMap<String, String> bNodes = new HashMap<String, String>();

		ArrayList<Statement> removedStatements = new ArrayList<Statement>();
		ArrayList<Statement> addedStatements = new ArrayList<Statement>();

		// list the statements in the Model
		StmtIterator iter = model.listStatements();

		// print out the predicate, subject and object of each statement
		while (iter.hasNext()) {
			Statement stmt = iter.nextStatement(); // get next statement
			Resource subject = stmt.getSubject(); // get the subject
			Property predicate = stmt.getPredicate(); // get the predicate
			RDFNode object = stmt.getObject(); // get the object

			boolean anonPresence = false;

			if (subject.isAnon()) {
				anonPresence = true;
				// Create a new named node
				AnonId anonId = subject.getId();
				if (bNodesMapping.get(anonId.getLabelString()) == null) {
					StringBuilder newAnonURI = new StringBuilder();
					newAnonURI.append(DEFAULT_NAMESPACE).append(sibName).append("#").append("_bNode")
							.append(bNodeSequential++);
					bNodesMapping.put(anonId.getLabelString(), newAnonURI.toString());
					bNodes.put(anonId.getLabelString(), newAnonURI.toString());
					subject = model.createResource(newAnonURI.toString());
				} else {
					subject = model.getResource(bNodesMapping.get(anonId.getLabelString()));
					// bNode was already declared
					bNodes.put(anonId.getLabelString(), bNodesMapping.get(anonId.getLabelString()));
				}
			}

			if (predicate.isAnon()) {
				anonPresence = true;
				// Create a new named node
				AnonId anonId = predicate.getId();
				if (bNodesMapping.get(anonId.getLabelString()) == null) {
					StringBuilder newAnonURI = new StringBuilder();
					newAnonURI.append(DEFAULT_NAMESPACE).append(sibName).append("#").append("_bNodeProp")
							.append(bNodeSequential++);
					bNodesMapping.put(anonId.getLabelString(), newAnonURI.toString());
					bNodes.put(anonId.getLabelString(), newAnonURI.toString());
					predicate = model.createProperty(newAnonURI.toString());
				} else {
					predicate = model.getProperty(bNodesMapping.get(anonId.getLabelString()));
					// bNode was already declared
					bNodes.put(anonId.getLabelString(), bNodesMapping.get(anonId.getLabelString()));
				}
			}

			if (object.isAnon()) {
				anonPresence = true;
				// Create a new named node
				AnonId anonId = ((Resource) object).getId();
				if (bNodesMapping.get(anonId.getLabelString()) == null) {
					StringBuilder newAnonURI = new StringBuilder();
					newAnonURI.append(DEFAULT_NAMESPACE).append(sibName).append("#").append("_bNode")
							.append(bNodeSequential++);
					bNodesMapping.put(anonId.getLabelString(), newAnonURI.toString());
					bNodes.put(anonId.getLabelString(), newAnonURI.toString());
					object = model.createResource(newAnonURI.toString());
				} else {
					object = model.getResource(bNodesMapping.get(anonId.getLabelString()));
					// bNode was already declared
					bNodes.put(anonId.getLabelString(), bNodesMapping.get(anonId.getLabelString()));
				}
			}

			if (anonPresence) {
				Statement newStmt = model.createStatement(subject, predicate, object);
				addedStatements.add(newStmt);
				removedStatements.add(stmt);
			}
		}

		model.add(addedStatements);
		model.remove(removedStatements);

		return bNodes;
	}

	/**
	 * Gets the collection of triples referred to the individuals
	 * 
	 * @return the collection of triples of the model
	 */
	public Collection<Triple> getAllTriples() {
		logger.log(LogService.LOG_DEBUG, "Getting all the triples...");
		// logger.debug("Getting all the triples...");

		StmtIterator it = model.listStatements();

		return TripleUtil.obtainTriples(it.toList(), model.getNsPrefixMap());

	}

	/**
	 * Used to check if a remote machine is listening in a specific port
	 * 
	 * @param ip       The IP to test
	 * @param port     The port to test
	 * @param protocol TCP or UDP
	 * @return TRUE if machine is listening
	 */
	private boolean isMachineReacheable(String ip, int port, String protocol) {

		InetSocketAddress inetAddress = new InetSocketAddress(ip, port);
		Socket clientSocketTCP = null;
		DatagramSocket clientSocketUDP = null;
		if (protocol.trim().contains("TCP")) {
			try {
				logger.log(LogService.LOG_DEBUG, "Testing TCP connectivity to " + ip + " on port " + port);
				// logger.debug("Testing TCP connectivity to " + ip + " on port " + port);
				clientSocketTCP = new Socket();
				clientSocketTCP.connect(inetAddress, 3000);
				logger.log(LogService.LOG_DEBUG, "Machine is reacheable, connection is possible");
				// logger.debug("Machine is reacheable, connection is possible");
				clientSocketTCP.close();
				return true;
			} catch (IOException e) {
				logger.log(LogService.LOG_ERROR, "Machine with IP " + ip + " in port " + port + " is not reacheable.");
				// logger.error("Machine with IP " + ip + " in port " + port + " is not
				// reacheable.");
				return false;
			}
		} else if (protocol.trim().contains("UDP")) {
			try {
				logger.log(LogService.LOG_DEBUG, "Testing UDP connectivity to " + ip + " on port " + port);
				// logger.debug("Testing UDP connectivity to " + ip + " on port " + port);
				clientSocketUDP = new DatagramSocket();
				clientSocketUDP.connect(inetAddress);
				logger.log(LogService.LOG_DEBUG, "Machine is reacheable, connection is possible");
				// logger.debug("Machine is reacheable, connection is possible");
				clientSocketUDP.close();
				return true;
			} catch (SocketException e) {
				logger.log(LogService.LOG_ERROR, "Machine with IP " + ip + " in port " + port + " is not reacheable.");
				// logger.error("Machine with IP " + ip + " in port " + port + " is not
				// reacheable.");
				return false;
			}
		}
		return false;

	}

	/**
	 * Utility method to obtain all the individuals in the current
	 * SemanticModel.model
	 * 
	 * @return A collection of Triples, each one holding a Invidividual
	 */
	public Collection<Triple> getAllIndividuals() {

		ArrayList<Triple> ret = new ArrayList<Triple>();
		// Create an ontology model
		OntModel ontology = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM, model);
		// Get the list of individuals
		ExtendedIterator<Individual> individuals = ontology.listIndividuals();
		// Iterate the individuals
		while (individuals.hasNext()) {
			// Get the individual
			Individual individual = (Individual) individuals.next();
			// Get the properties of the individual
			StmtIterator stI = individual.listProperties();
			while (stI.hasNext()) {
				// Get the statement
				Statement st = (Statement) stI.next();
				// Get the triple information
				com.hp.hpl.jena.graph.Triple t = st.asTriple();
				// Construct our own Triple
				Triple triple = new Triple(t.getSubject().toString(), t.getPredicate().toString(),
						t.getObject().toString());
				ret.add(triple);
			}
		}
		return ret;
	}

}