package org.smool.sdk.sib;

import java.text.DateFormat;
import java.util.Date;

import org.osgi.service.log.LogEntry;
import org.osgi.service.log.LogListener;

public class ConsoleLogImpl implements LogListener
{
    public void logged(LogEntry log)
    {
    	// Make a new Date object. It will be initialized to the current time.
        Date now = new Date();
        String timestamp = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(now);
        String logLevel = new String(); 
        int level = log.getLevel();
        switch (level){
        case 1:
        	logLevel = "ERROR";
        	break;
        case 2:
        	logLevel = "WARNING";
        	break;
        case 3:
        	logLevel = "INFO";
        	break;
        case 4:
        	logLevel = "DEBUG";
        	break;
        default:
        	logLevel = "NONE";
        }
       
        if (log.getMessage() != null)
            System.out.println("[" + logLevel + "]" + " [" + log.getBundle().getSymbolicName() + "] " +  "[" + timestamp + "] " + log.getMessage());

    }
}
