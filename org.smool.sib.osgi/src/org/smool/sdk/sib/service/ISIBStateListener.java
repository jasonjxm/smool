package org.smool.sdk.sib.service;

public interface ISIBStateListener {

	public void SIBStopped(ISIB sib);
	public void SIBRunning(ISIB sib);
	
}
