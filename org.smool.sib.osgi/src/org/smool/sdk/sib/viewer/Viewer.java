/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.sib.viewer;

import org.osgi.service.log.LogService;
import org.smool.sdk.sib.SIBActivator;
import org.smool.sdk.sib.data.ISIBPropertiesHolderListener;
import org.smool.sdk.sib.data.ISemanticContentHolderListener;
import org.smool.sdk.sib.data.ISessionHolderListener;
import org.smool.sdk.sib.data.ISubscriptionHolderListener;
import org.smool.sdk.sib.service.IViewer;
import org.smool.sdk.sib.ssap.SIB;
import org.smool.sdk.sib.viewer.request.SIBPropertiesListenerRequest;
import org.smool.sdk.sib.viewer.request.SemanticContentListenerRequest;
import org.smool.sdk.sib.viewer.request.SessionListenerRequest;
import org.smool.sdk.sib.viewer.request.SubscriptionListenerRequest;
import org.smool.sdk.sib.viewer.request.ViewerRequest;


/**
 * This class implements the IViewer interface, which access to some information of the server 
 * (triplets, subscriptions and sessions) and visualizes it in a view created 
 * for this purpose.
 *  
 * @author Cristina Lopez, Cristina.Lopez@tecnalia.com - Tecnalia
 */
public class Viewer implements IViewer {

	/** The logger */
	private static LogService logger;
	
	private SIB sib;
	
	public Viewer(SIB sib) {
		logger = SIBActivator.getLogger();
		this.sib = sib;
	}
	
	/**
	 * @return the associated Sib
	 */
	public SIB getSib() {
		return sib;
	}
	
	/**
	 * Attach the Session Listener to the SIB.
	 * This session listener is listening to the changes of sessions in the
	 * SIB.
	 * @param listener a listener that implements the ISessionHolderListener methods
	 */
	@Override
	public void addListener(ISessionHolderListener listener) {
		logger.log(LogService.LOG_DEBUG, "Attaching session listener to the SIB");
		//logger.debug("Attaching session listener to the SIB");
		ViewerRequest request = new SessionListenerRequest(listener, sib);
		request.setAddListener(true);
		
		sib.getRequestedCommandExecutor().execute(request);
	}

	/**
	 * Detach the Session listener from the SIB. 
	 * Detach the listener from the SIB.
	 * @param listener a listener that implements the ISessionHolderListener methods
	 */
	@Override
	public void addListener(ISubscriptionHolderListener listener) {
		logger.log(LogService.LOG_DEBUG, "Attaching subscription listener to the SIB");
		//logger.debug("Attaching subscription listener to the SIB");
		ViewerRequest request = new SubscriptionListenerRequest(listener, sib);
		request.setAddListener(true);
		
		sib.getRequestedCommandExecutor().execute(request);
	}

	/**
	 * Attach the Subscription Listener to the SIB.
	 * This subscription listener is listening to the changes of subscribed 
	 * queries in the SIB.
	 * @param listener a listener that implements the ISubscriptionHolderListener methods
	 */
	@Override
	public void addListener(ISemanticContentHolderListener listener) {
		logger.log(LogService.LOG_DEBUG, "Attaching triple listener to the SIB");
		//logger.debug("Attaching triple listener to the SIB");
		ViewerRequest request = new SemanticContentListenerRequest(listener, sib);
		request.setAddListener(true);
		
		sib.getRequestedCommandExecutor().execute(request);
	}
	

	/**
	 * Attach the Subscription Listener to the SIB.
	 * This subscription listener is listening to the changes of subscribed 
	 * queries in the SIB.
	 * @param listener a listener that implements the ISubscriptionHolderListener methods
	 */
	@Override
	public void addListener(ISIBPropertiesHolderListener listener) {
		logger.log(LogService.LOG_DEBUG, "Attaching sib properties listener to the SIB");
		//logger.debug("Attaching sib properties listener to the SIB");
		ViewerRequest request = new SIBPropertiesListenerRequest(listener, sib);
		request.setAddListener(true);
		
		sib.getRequestedCommandExecutor().execute(request);

	}	

	/**
	 * Detach the Subscription listener from the SIB. 
	 * Detach the listener from the SIB.
	 * @param listener a listener that implements the ISubscriptionHolderListener methods
	 */
	@Override
	public void removeListener(ISessionHolderListener listener) {
		logger.log(LogService.LOG_DEBUG, "Detaching session listener to the SIB");
		//logger.debug("Detaching session listener to the SIB");
		ViewerRequest request = new SessionListenerRequest(listener, sib);
		request.setRemoveListener(true);
		
		sib.getRequestedCommandExecutor().execute(request);
	}

	/**
	 * Attach the Semantic Content Listener to the SIB.
	 * The semantic content listener is listening to the changes of the semantic
	 * model that is in the SIB. The most easy way to view this information
	 * is using the data expressed in triples.
	 * @param listener a listener that implements the ISemanticContentHolderListener methods
	 */
	@Override
	public void removeListener(ISubscriptionHolderListener listener) {
		logger.log(LogService.LOG_DEBUG, "Detaching subscription listener to the SIB");
		//logger.debug("Detaching subscription listener to the SIB");
		ViewerRequest request = new SubscriptionListenerRequest(listener, sib);
		request.setRemoveListener(true);
		
		sib.getRequestedCommandExecutor().execute(request);
	}

	/**
	 * Detach the semantic content listener from the SIB. 
	 * Detach the listener from the SIB.
	 * @param listener a listener that implements the ISemanticContentHolderListener methods
	 */
	@Override
	public void removeListener(ISemanticContentHolderListener listener) {
		logger.log(LogService.LOG_DEBUG, "Detaching triple listener to the SIB");
		//logger.debug("Detaching triple listener to the SIB");
		ViewerRequest request = new SemanticContentListenerRequest(listener, sib);
		request.setRemoveListener(true);
		
		sib.getRequestedCommandExecutor().execute(request);
	}
	
	/**
	 * Detach the SIB properties listener from the SIB. 
	 * Detach the listener from the SIB.
	 * @param listener a listener that implements the ISemanticContentHolderListener methods
	 */
	@Override
	public void removeListener(ISIBPropertiesHolderListener listener) {
		logger.log(LogService.LOG_DEBUG, "Detaching triple listener to the SIB");
		//logger.debug("Detaching sib properties listener to the SIB");
		ViewerRequest request = new SIBPropertiesListenerRequest(listener, sib);
		request.setRemoveListener(true);
		
		sib.getRequestedCommandExecutor().execute(request);
	}	
}
