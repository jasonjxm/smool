package org.smool.sdk.sib.util.transformer;

import java.util.HashMap;

import org.smool.sdk.sib.exception.TransformationException;
import org.smool.sdk.sib.model.query.SMOOLResultSet;
import org.smool.sdk.sib.model.query.SMOOLResultSetFormatter;


public class RDFXMLTransformer extends Transformer {

	@Override
	public String toNTriples(String s) throws TransformationException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String toRDFXML(String s) throws TransformationException {
		return s;
	}

	@Override
	public HashMap<String, String> toSPARQL(String s) throws TransformationException {
		HashMap<String, String> sparqlQueries = new HashMap<String, String>();
		sparqlQueries.put(s,s);
		return sparqlQueries;
		
	}

	@Override
	public String toOriginalEncoding(String query, SMOOLResultSet results)
			throws TransformationException {
		try {
			// Transforms the RDF-M3 structure into SPARQL
			return SMOOLResultSetFormatter.asXMLString(results);

		} 
		catch (Exception ex) {
			//ex.printStackTrace();
			throw new TransformationException("Cannot parse due to " + ex.getMessage());
		}
	}

}
