/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.sib.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.osgi.service.log.LogService;
import org.smool.sdk.sib.SIBActivator;

/**
 * This class loads the SIB properties
 *  
 */
public class SIBPropertiesLoader {
	
	/**
	 * A logger
	 */
	private LogService logger;
	
	private Properties sibProps;
	private static final String DEFAULT_PROPS = "/sib.properties"; 

	/**
	 * Constructor of PropertyLoader.
	 */
    private SIBPropertiesLoader() {
		sibProps = new Properties();
		this.init();
    }
	
    /**
     * Initializes the SIBProperties class, loading the 
     * properties data.
     */
	public void init() {
		try {
			logger = SIBActivator.getLogger();
			sibProps.load(getClass().getResourceAsStream(DEFAULT_PROPS));
		} 
		catch (FileNotFoundException e) {
			logger.log(LogService.LOG_ERROR, "The indicated file " + DEFAULT_PROPS + " does not exist");
			//System.out.println("The indicated file " + DEFAULT_PROPS + " does not exist");
			//System.out.println(e.toString());
		} 
		catch (IOException e) {
			logger.log(LogService.LOG_ERROR, "There were problems with opening of the properties file " + DEFAULT_PROPS);
//			System.out.println("There were problems with opening of the properties file " + DEFAULT_PROPS);
//			System.out.println(e.toString());
		}
	}
	
	/**
	 * Gets property from the SIB Properties
	 * @param key The key
	 * @return A string with the property value
	 */
	public String getProperty(String key) {
		return sibProps.getProperty(key);
	}

    /**
     * SingletonHolder is loaded on the first execution of SIBProperties.getInstance() 
     * or the first access to SingletonHolder.INSTANCE, not before.
     */
    private static class SingletonHolder { 
    	private static final SIBPropertiesLoader INSTANCE = new SIBPropertiesLoader();
    }
  
    /**
     * Gets the instance of the SIBProperties singleton
     * @return The SIBProperties singleton
     */
    public static SIBPropertiesLoader getInstance() {
      return SingletonHolder.INSTANCE;
    }
}
