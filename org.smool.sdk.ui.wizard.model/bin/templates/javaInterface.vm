/*******************************************************************************
 * Copyright (c) 2009,2012 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

#set($dot =".")
#set($ifzletter ="I")
#set($implpkg = ".impl")
#set($quote = '"')
#set($quotedClassName = "$quote$cls.getOWLClassName()$quote")
#set($quotedClassNamespace = "$quote$cls.getOWLClassNamespace()$quote")
package $basePackage$dot$cls.getOWLClassPrefix();

#if($cls.hasNonFunctionalProperties())
import java.util.Collection;
import java.util.Iterator;
#end

import org.smool.sdk.model.smart.IAbstractOntConcept;
## imports the object properties ranges
#if($cls.hasObjectProperties())
## add the domain property methods
#foreach($objProperty in $cls.listObjectProperties())
#foreach($objPropertyRange in $objProperty.listRanges())
#set($rangeClassPrefix = $objPropertyRange.getOWLClassPrefix())
#set($rangeClassName = $objPropertyRange.getOWLClassName())
import $basePackage$dot$rangeClassPrefix$dot$ifzletter$rangeClassName;
##import $basePackage$dot$rangeClassPrefix$implpkg$dot$rangeClassName;
#end
#end
#end
##importing superclasses
#if($cls.hasParents())
#set ($extendsdirective = " extends IAbstractOntConcept")
#set ($listsize = $cls.getParentListSize() - 1)
#set ($current = 0)
#set ($extendedifz = ",")
#foreach($parentClass in $cls.listSubClassOf())
#set($parentClassPrefix = $parentClass.getOWLClassPrefix())
#set($parentClassName = $parentClass.getOWLClassName())
import $basePackage$dot$parentClassPrefix$dot$ifzletter$parentClassName;
#set($extendedifz = "$extendedifz I$parentClassName")
#if ($current < $listsize)
#set ($current = $current + 1)
#set($extendedifz = "$extendedifz,")
#end ##end if ($current < $listsize)
#end ##end foreach($parentClass in $cls.listSubClassOf())
#else 
#set ($extendsdirective = " extends IAbstractOntConcept")
#set ($extendedifz = "")
#end ##end if($cls.hasParents())

## write the complete interface declaration in one line
/**
 * This interface represents the ontology concept $cls.getOWLClassName()
 * including all its properties.
 * @author TBD
 * @version 1.0
 */
public interface I$cls.getOWLClassName()$extendsdirective$extendedifz {

  public static String CLASS_NAMESPACE = $quotedClassNamespace;
  public static String CLASS_ID = $quotedClassName;
  public static String CLASS_IRI = CLASS_NAMESPACE + CLASS_ID;
  
#if($cls.hasObjectProperties())
    /*
     * OBJECT PROPERTIES: GETTERS AND SETTERS
     */

## add the domain property methods
#foreach($prop in $cls.listObjectProperties())
#foreach($range in $prop.listRanges())
#parse("/templates/javaInterfaceObjectPropertyMethod.vm")

#end
#end
#end
#if($cls.hasDatatypeProperties())
    /*
     * DATATYPE PROPERTIES: GETTERS AND SETTERS
     */

 ## add the domain property methods
#foreach($prop in $cls.listDatatypeProperties())
#set($range=$prop.getJavaMappedRange())
#parse("/templates/javaInterfaceDatatypePropertyMethod.vm")

#end
#end
}
