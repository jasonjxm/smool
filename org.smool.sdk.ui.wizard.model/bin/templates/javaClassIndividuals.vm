#set($individualName = "$cls.getOWLClassName()Individual")
    /**
     * Individuals associated to the current class
     */
    public enum $individualName {
#set ($listsize = $cls.getIndividualListSize() - 1)
#set ($current = 0)
## add the individual declaration as enum
#foreach($individual in $cls.listIndividuals())
#set($individualDeclaration = "$individual.getCapitalizedOWLIndividualName() ($quote$individual.getOWLIndividualName()$quote")
#set($individualValues = "")
#set($individualIndentation = "        ")
#foreach($dtProperty in $individual.listDatatypeProperties())
##if($dtProperty.isFunctional()) 
#set($datatypeValue= "$individual.getDatatypePropertyValue($dtProperty)")
##end
#set($individualValues = "$individualValues,$datatypeValue")
#end
#if ($current < $listsize)
#set ($current = $current + 1)
$individualIndentation$individualDeclaration$individualValues),
#else
$individualIndentation$individualDeclaration$individualValues);
#end
#end
            
        private final String id; // the identifier
#set ($constructorParams = "")
#foreach($dtProperty in $cls.listIndividualDatatypeProperties())
#set ($constructorParams = "$constructorParams, $dtProperty.getJavaMappedRange() $dtProperty.getOWLPropertyName()")
        private final $dtProperty.getJavaMappedRange() $dtProperty.getOWLPropertyName(); // the $dtProperty.getOWLPropertyName() property 
#end
        
        /**
         * Constructor for creating $cls.getOWLClassName() individuals
         * @param id the individual identifier
         * @param name the value taken for the name
#foreach($dtProperty in $cls.listIndividualDatatypeProperties())
         * @param $dtProperty.getOWLPropertyName() the value taken for the $dtProperty.getOWLPropertyName()  
#end
         */
        $individualName(String id$constructorParams) {
            this.id = id;
#foreach($dtProperty in $cls.listIndividualDatatypeProperties())
            this.$dtProperty.getOWLPropertyName() = $dtProperty.getOWLPropertyName();  
#end
        }
        
        /**
         * Gets the individual identifier
         * @return a string with the individual identifier
         */
        public String getID() {
            return id;
        }

#foreach($dtProperty in $cls.listIndividualDatatypeProperties())
        /**
         * Gets the individual $dtProperty.getOWLPropertyName()
         * @return the value for property $dtProperty.getOWLPropertyName()
         */
        public $dtProperty.getJavaMappedRange() get$dtProperty.getCapitalizedOWLPropertyName()() {
            return $dtProperty.getOWLPropertyName();
        }
#end        

#set ($classVariable = "$cls.getUncapitalizedOWLClassName()")
#set ($equalityCondition = ".getID().equals(id)")
        /**
         * Finds an $cls.getOWLClassName() for a given id
         * @param id the $cls.getOWLClassName() identifier
         * @return the individual corresponding to the individual. null if does not exists
         */
        public static $individualName find$cls.getOWLClassName()(String id) {
            for($individualName $classVariable : values()) {
                if($classVariable$equalityCondition) {
                    return $classVariable;
                }
            }
            return null;
        }

        /**
         * Gets a $cls.getOWLClassName() instance of current individual
         * @return a $cls.getOWLClassName() entity for the Individual
         */
        public $cls.getOWLClassName() getIndividual() {
            $cls.getOWLClassName() $cls.getUncapitalizedOWLClassName() = new $cls.getOWLClassName()(this.getID());
#if($cls.isProducer())
#foreach($dtProperty in $cls.listIndividualDatatypeProperties())
#set ($propSet = "set$dtProperty.getCapitalizedOWLPropertyName()(this.get$dtProperty.getCapitalizedOWLPropertyName()());")
            $cls.getUncapitalizedOWLClassName()$dot$propSet
#end
#end
            /** Sets the context of this ontology concept */
            $cls.getUncapitalizedOWLClassName()._setIndividualContext($quote$cls.getOWLClassPrefix()$quote, $quote$cls.getOWLClassNamespace()$quote);

            return $cls.getUncapitalizedOWLClassName();
        }        
    }
