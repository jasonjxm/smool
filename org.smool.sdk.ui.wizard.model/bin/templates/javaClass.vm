/*******************************************************************************
 * Copyright (c) 2009,2012 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

#set($dot = ".")
#set($quote = '"')
#set($implpkg = ".impl")
#set($ifzletter ="I")
#set($ifzname = "I$cls.getOWLClassName()")
#set($quotedClassName = "$quote$cls.getOWLClassName()$quote")
#set($quotedClassNamespace = "$quote$cls.getOWLClassNamespace()$quote")
package $basePackage$dot$cls.getOWLClassPrefix()$implpkg;

#if($cls.hasNonFunctionalProperties(false))
import java.util.Collection;
import java.util.Iterator;
#end

import $basePackage$dot$cls.getOWLClassPrefix()$dot$ifzname;
import org.smool.sdk.model.smart.AbstractOntConcept;
import org.smool.sdk.model.smart.KPConsumer;
import org.smool.sdk.model.smart.KPProducer;
#if($cls.hasFunctionalDatatypeProperties(false))
import org.smool.sdk.model.smart.slots.FunctionalDatatypeSlot;
#end
#if($cls.hasNonFunctionalDatatypeProperties(false))
import org.smool.sdk.model.smart.slots.NonFunctionalDatatypeSlot;
#end
#if($cls.hasFunctionalObjectProperties(false))
import org.smool.sdk.model.smart.slots.FunctionalObjectSlot;
#end
#if($cls.hasNonFunctionalObjectProperties(false))
import org.smool.sdk.model.smart.slots.NonFunctionalObjectSlot;
#end
#if($cls.hasObjectProperties(false))
#set($importstatements = "")
#foreach($range in $cls.listObjectPropertyRanges(false))
#set($importstatements = "$importstatements
import $basePackage$dot$range.getOWLClassPrefix()$dot$ifzletter$range.getOWLClassName();
import $basePackage$dot$range.getOWLClassPrefix()$implpkg$dot$range.getOWLClassName();")
#end
$importstatements
#end

/**
 * This class implements the ontology concept $cls.getOWLClassName()
 * including all its properties.
 * @author TBD
 * @version 1.0
 */
public class $cls.getOWLClassName() extends AbstractOntConcept implements $ifzname, KPConsumer, KPProducer {

#if($cls.hasIndividuals())      
#parse("/templates/javaClassIndividuals.vm")
#end 
  public static String CLASS_NAMESPACE = $quotedClassNamespace;
  public static String CLASS_ID = $quotedClassName;
  public static String CLASS_IRI = CLASS_NAMESPACE + CLASS_ID;
  

## constructor
    /**
     * The Constructor
     */
    public $cls.getOWLClassName()() {
        super();
        init();
    }
    
    /**
     * The Constructor
     * @param id the $cls.getOWLClassName() identifier
     */
    public $cls.getOWLClassName()(String id) {
      /** Call superclass to establish the identifier */
      super(id);
      init();
    }
    
    /**
     * Inits the fields associated to a ontology concept
     */
    public void init() {
      /** Sets the context of this ontology concept */
      this._setClassContext($quote$cls.getOWLClassPrefix()$quote, CLASS_IRI);

      /** Sets the individual context */
      this._setDefaultIndividualContext();
        
#if($cls.hasObjectProperties(false))      
## add the domain property methods
#foreach($prop in $cls.listObjectProperties(false))
#foreach($range in $prop.listRanges())
#parse("/templates/javaClassObjectPropertyDefinition.vm")
#end
#end
#end

#if($cls.hasDatatypeProperties(false))      
## add the domain property methods
#foreach($prop in $cls.listDatatypeProperties(false))
#set($range = $prop.getJavaMappedRange())
#parse("/templates/javaClassDatatypePropertyDefinition.vm")
#end
#end 
    }

#if($cls.hasObjectProperties(false))
    /*
     * OBJECT PROPERTIES: GETTERS AND SETTERS
     */

## add the domain property methods
#foreach($prop in $cls.listObjectProperties(false))
#foreach($range in $prop.listRanges())
#parse("/templates/javaClassObjectPropertyMethod.vm")
#end
#end
#end
#if($cls.hasDatatypeProperties(false))
    /*
     * DATATYPE PROPERTIES: GETTERS AND SETTERS
     */

## add the domain property methods
#foreach($prop in $cls.listDatatypeProperties(false))
#set($range = $prop.getJavaMappedRange())
#parse("/templates/javaClassDatatypePropertyMethod.vm")
#end
#end
}
