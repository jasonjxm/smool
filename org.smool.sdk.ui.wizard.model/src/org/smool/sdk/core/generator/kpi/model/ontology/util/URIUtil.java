/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.core.generator.kpi.model.ontology.util;


/**
 * Helper class to obtain URI content
 * 
 * @author Fran Ruiz, fran.ruiz@tecnalia.com, ESI
 */
public class URIUtil {

	/**
	 * Gets the namespace
	 * @param uri the uri
	 * @return a String with the class/individual identifier
	 */
	public static String getLocalName(String uri) {
		return uri.substring(uri.lastIndexOf('#')+1);
	}

	/**
	 * Gets the namespace associated to the IRI
	 * @param iri the iri
	 * @return a String containing the namespace
	 */
	public static String getNamespace(String uri) {
		return uri.substring(0,uri.lastIndexOf('#')+1);
	}
}
