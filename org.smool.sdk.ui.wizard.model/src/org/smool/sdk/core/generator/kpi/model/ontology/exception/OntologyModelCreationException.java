/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.core.generator.kpi.model.ontology.exception;

/**
 * The exception for the metamodel creation
 * 
 * @author Fran Ruiz, fran.ruiz@tecnalia.com, ESI
 *
 */
public class OntologyModelCreationException extends Exception {
	
	/** The Serial Version Unique Identifier **/
	private static final long serialVersionUID = -8368350323024103533L;

	/**
	 * This constructor is used to create a Connection Error Exception
	 * without parameters. 
	 */
	public OntologyModelCreationException() {
		super();
	}
	
	/**
	 * This constructor is used to create a Connection Error Exception
	 * passing a message as parameter
	 * @param msg a String indicating the error source
	 */
	public OntologyModelCreationException(String msg) {
		super(msg);
	}		
}
