/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.core.generator.kpi.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.apache.commons.collections.ExtendedProperties;
import org.apache.log4j.Logger;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.eclipse.jdt.core.IJavaProject;
import org.osgi.service.log.LogService;
import org.smool.sdk.common.properties.PropertyLoader;
import org.smool.sdk.core.generator.kpi.model.ontology.OWLClass;
import org.smool.sdk.core.generator.kpi.model.ontology.OWLDatatypeProperty;
import org.smool.sdk.core.generator.kpi.model.ontology.OWLIndividual;
import org.smool.sdk.core.generator.kpi.model.ontology.OWLObjectProperty;
import org.smool.sdk.core.generator.kpi.model.ontology.OWLOntology;
import org.smool.sdk.core.generator.kpi.model.ontology.OntologyModel;
import org.smool.sdk.core.generator.kpi.model.writer.ModelLayerWriter;
import org.smool.sdk.ui.wizard.model.pages.frames.ProjectHelper;

import com.hp.hpl.jena.ontology.DataRange;
import com.hp.hpl.jena.ontology.EnumeratedClass;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.ontology.Ontology;
import com.hp.hpl.jena.ontology.UnionClass;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import com.hp.hpl.jena.vocabulary.OWL;
import com.hp.hpl.jena.vocabulary.RDF;
import com.hp.hpl.jena.vocabulary.RDFS;



/**
 * This class is used to generate a SmartAppProjectModel based on the
 * information obtained from the SmartAppProject
 * 
 * @author Fran Ruiz, fran.ruiz@tecnalia.com, ESI
 */
public class ModelGenerator {

	/** Constant defining the default package */
	private static String DEFAULT_PACKAGE = "smartapp.default.package";
	
	/** Constant defining the default package */
	private static String OWLTHING_URI = "http://www.w3.org/2002/07/owl#Thing";
	
	/** List of project consumer classes selected in the wizard */
	private ArrayList<OntClass> consumer = new ArrayList<OntClass>();
	
	/** List of project producer classes selected in the wizard */
	private ArrayList<OntClass> producer = new ArrayList<OntClass>();

	/** The Ontology Model */
	private OntologyModel model;

	/** The initial ontology set */
	private OntModel initialOntology;	
	
	/** The final ontology */
	private OntModel ontology;
	
	/** Log for this class */
	private static Logger logger = Logger.getLogger(ModelGenerator.class);

	/** The Velocity Engine */
	private VelocityEngine vEngine;
	
	private IJavaProject smoolProject;

	private String projectName = "ontology";
	
	private LogService log;

	/**
	 * Public constructor
	 */
	public ModelGenerator() {
		logInfo("Called the constructor of the model generator service.");
		this.producer = new ArrayList<OntClass>();
		this.consumer = new ArrayList<OntClass>();
		logInfo("Creating a void ontology model");
		this.ontology = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM);
		logInfo("Done");
	}
	
	/**
	 * Generates a new model layer based on the parameters
	 * @param project the project name
	 * @param packageName the package name
	 * @param ontModel the ontology model
	 * @param producerOntClasses the producer classes
	 * @param consumerOntClasses the consumer classes
	 */
	public void generate(IJavaProject project,
			String packageName,
			OntModel ontModel,
			Collection<OntClass> producerOntClasses,
			Collection<OntClass> consumerOntClasses) {
		logInfo("Starting the generation of the model layer");
		
		try {
			this.model = new OntologyModel();
			
			this.smoolProject = project;
			
			logInfo("Getting the package");
			if (packageName.equals("")) {
				PropertyLoader props = PropertyLoader.getInstance();
				packageName = props.getProperty(DEFAULT_PACKAGE);
			}
			
			this.model.setBasePackageName(packageName + ".model");
			this.consumer.addAll(consumerOntClasses);
			this.producer.addAll(producerOntClasses);
			this.initialOntology = ontModel;
			
			logInfo("The class is going to create the model layer...");
			createModel();
			logInfo("Generating the source code...");
			generate();
			logInfo("Done!");
		} catch (ExceptionInInitializerError ex) {
			logInfo("Error in initializer: " + ex.getMessage());
			ex.printStackTrace();
		} catch (ClassFormatError ex) {
			logInfo("ClassFormatError: " + ex.getMessage());
			ex.printStackTrace();
		} catch (NoClassDefFoundError ex) {
			logInfo("NoClassDefFoundError: " + ex.getMessage());
			ex.printStackTrace();
		} catch (ClassCircularityError ex) {
			logInfo("ClassCircularityError: " + ex.getMessage());
			ex.printStackTrace();
		} catch (UnsatisfiedLinkError ex) {
			logInfo("UnsatisfiedLinkError: " + ex.getMessage());
			ex.printStackTrace();
		} catch (VerifyError ex) {
			logInfo("VerifyError: " + ex.getMessage());
			ex.printStackTrace();
		} catch (IncompatibleClassChangeError ex) {
			logInfo("IncompatibleClassChangeError: " + ex.getMessage());
			ex.printStackTrace();
		} catch (Exception ex) {
			logInfo("An error arised during the project generation: " + ex.getMessage());
			ex.printStackTrace();
		}
		
	}
	
	/**
	 * Performs the model creation process
	 */
	private void createModel() {
		logInfo("Getting selected ontologies information");
		createOWLOntologies();

		logInfo("Importing namespace prefixes");
		// Import namespaces
		importNamespacesPrefixes();
		
		logInfo("Creating class hierarchy");
		// Create class hierarchy
		createClassHierarchy();
		
		// Create properties associated to added classes
		createProperties();
		
		// Create individuals
		createIndividuals();
	}
	
	/**
	 * Sets the Project
	 * @param project an IProject that represents the project
	 */
	public void setProject(IJavaProject project) {
		this.smoolProject = project;
	}	
	
	/**
	 * Generates the source code
	 */
	private void generate() {
		try {
			this.initVelocityEngine();
			
			ModelLayerWriter writer = new ModelLayerWriter(vEngine, getVelocityContext());
	
			writer.createProjectClasses(model, ProjectHelper.getSourceFolder(smoolProject));

			writer.createOntologyModel(ontology, smoolProject.getProject(), projectName);
			
			writer.createConfiguration(smoolProject.getProject());
			
			writer.createOntologyMapping(smoolProject.getProject(), model);
		} catch (Exception ex) {
			logger.error("Cannot generate the smart application project");
			ex.printStackTrace();
		}
	}

	/**
	 * Creates the OWL ontologies 
	 */
	private void createOWLOntologies() {
		
		ExtendedIterator<Ontology> itOntologies = initialOntology.listOntologies();
		
		while (itOntologies.hasNext()) {
			Ontology owlOntology = itOntologies.next();
			if (owlOntology.getURI() != null) {
				OWLOntology newOntology = new OWLOntology(owlOntology.getURI());
				ExtendedIterator<OntResource> itImports = owlOntology.listImports();
				while(itImports.hasNext()) {
					OntResource importOntology = (OntResource) itImports.next();
					newOntology.addImports(new OWLOntology(importOntology.getURI()));
				}
				model.addOntology(newOntology);
			}
		}
	}	
	
	/**
	 * Gets the namespaces and its prefixes and adds it into the model
	 */
	private void importNamespacesPrefixes() {
		// Getting the namespace prefixes
		Map<String,String> map = initialOntology.getNsPrefixMap();
		
		// Set the namespace prefixes
		for (String key : map.keySet()) {
			String value = map.get(key);
			model.addNsPrefix(key, value);
		}

		// Adds namespace prefixes to the final ontology
		ontology.setNsPrefixes(map);
	}
	
	
	/**
	 * Creates the class hierarchy
	 */
	private void createClassHierarchy() {
		//logger.debug("Creating class hierarchy");
		
		// Adding consumer classes
		createClasses(consumer, true, false);

		// Adding producer classes
		createClasses(producer, false, true);
		
		// Create equivalent classes
		createEquivalentClasses();
	}
	
	/**
	 * Creates the class hierarchy
	 */
	private void createClasses(Collection<OntClass> ontClasses, boolean consumer, boolean producer) {
		try {	
			for (OntClass ontClass : ontClasses) {
				if (!isOWLThing(ontClass)) {
					addClassToModel(ontClass, consumer, producer);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	private boolean isOWLThing(OntClass ontClass) {
		return ontClass.getURI().equals(OWLTHING_URI);
	}

	/**
	 * Adds the class to model
	 */
	private OWLClass addClassToModel(OntClass ontClass, boolean consumer, boolean producer) {
		OWLClass owlClass = new OWLClass(ontClass.getURI());
		
		if (model.contains(owlClass)) {
			owlClass = model.getOWLClass(ontClass.getURI());
		} else {
			// assigns the consumer/producer values
			owlClass.setConsumer(consumer);
			owlClass.setProducer(producer);
			
			// Adds the class to the corresponding ontology
			OWLOntology owlOntology = model.getOWLOntology(owlClass.getOWLClassNamespace());
			if (owlOntology == null) {
				owlOntology = new OWLOntology(owlClass.getOWLClassNamespace());
				model.addOntology(owlOntology);
			}
			
			owlOntology.addClass(owlClass);
			createParentHierarchy(owlClass);
			importSubjectStatements(owlClass.getID());
			importObjectStatements(owlClass.getID());
		}
	
		if (producer) {
			owlClass.setProducer(true);
		} 
		
		if (consumer) {
			owlClass.setConsumer(true);
		}		
		
		return owlClass;
	}
	
	/**
	 * Creates the parent hierarchy
	 * @param ontClass
	 */
	private void createParentHierarchy(OWLClass owlClass) {
		try {
			if (initialOntology.getOntClass(owlClass.getID()) != null) {
				ExtendedIterator<OntClass> it = initialOntology.getOntClass(owlClass.getID()).listSuperClasses();
	
				while (it.hasNext()) {
					OntClass parentClass = it.next();
					if (!parentClass.isRestriction()) {
						if (!isOWLThing(parentClass)) {
							OWLClass owlParent = addClassToModel(parentClass, owlClass.isConsumer(), owlClass.isProducer());
							owlClass.addSubClassOf(owlParent);
						}
					}
				}
			}
		} catch (Exception ex) {
			logger.error("Cannot create the parent hierarchy for " + owlClass.getID());
			ex.printStackTrace();
		}
	}
	
	/**
	 * Creates the equivalence classes with current model classes
	 */
	private void createEquivalentClasses() {
		for (OWLClass owlClass : model.listOWLClasses()) {
			
			ExtendedIterator<OntClass> itEquivalent = 
				initialOntology.getOntClass(owlClass.getID()).listEquivalentClasses();
			while (itEquivalent.hasNext()) {
				OntClass equivalentClass = itEquivalent.next();
				
				OWLClass owlEquivalentClass = new OWLClass(equivalentClass.getURI());
				
				if (model.contains(owlEquivalentClass)) {
					owlEquivalentClass = model.getOWLClass(equivalentClass.getURI());
					owlEquivalentClass.addEquivalentClass(owlClass);
					owlClass.addEquivalentClass(owlEquivalentClass);
				} else {
					// Adds the class to the corresponding ontology
					OWLOntology owlOntology = model.getOWLOntology(owlEquivalentClass.getOWLClassNamespace());
					owlOntology.addClass(owlEquivalentClass);
					owlOntology.addClass(owlClass);
					owlEquivalentClass.addEquivalentClass(owlClass);
					owlClass.addEquivalentClass(owlEquivalentClass);
					importSubjectStatements(owlClass.getID());
					importObjectStatements(owlClass.getID());
					importSubjectStatements(owlEquivalentClass.getID());
					importObjectStatements(owlEquivalentClass.getID());
					
				}				
				
				addClassToModel(equivalentClass, owlClass.isConsumer(), owlClass.isProducer());
			}		
		}
	}
	
	/**
	 * Based on current properties, gets the set of new properties that can
	 * be defined as SmartAppClasses, and adding it to the model
	 */
	public void createProperties() {
		
		ArrayList<OWLClass> propertyClasses = new ArrayList<OWLClass>();
		
		propertyClasses.addAll(lookupProperties(model.listOWLClasses()));
		
		propertyClasses.addAll(lookupProperties(propertyClasses));
		
		// Adds all the parent classes to the model
		for (OWLClass owlClass : propertyClasses) {
			model.getOWLOntology(owlClass.getOWLClassNamespace()).addClass(owlClass);
			createParentHierarchy(owlClass);
			importSubjectStatements(owlClass.getID());
			importObjectStatements(owlClass.getID());			
		}		
	}
	
	/**
	 * Crates the property set
	 */
	private Collection<OWLClass> lookupProperties(Collection<OWLClass> owlClasses) {
		ArrayList<OWLClass> objectPropertyClasses = new ArrayList<OWLClass>();
		
		ExtendedIterator<OntProperty> props = initialOntology.listAllOntProperties();
		
		while(props.hasNext()) {
			OntProperty property = props.next();
			
			ExtendedIterator<? extends OntClass> domains = property.listDeclaringClasses();
			while (domains.hasNext()) {
				OntClass ontClass = domains.next();
				if (ontClass.isUnionClass()) { // if it is an union class
					UnionClass unionClass = ontClass.asUnionClass();
					
					ExtendedIterator<? extends OntClass> it = unionClass.listOperands();
					
					while(it.hasNext()) {
						OntClass componentClass = it.next();
						
						for (OWLClass owlClass : model.listOWLClasses()) {
							if (initialOntology.getOntClass(owlClass.getID()) != null) {
								if (componentClass.equals(initialOntology.getOntClass(owlClass.getID()))) {
									if (property.isDatatypeProperty()) {
										this.addDatatypeProperty(owlClass, property);
								    } else if (property.isObjectProperty()) {
								    	objectPropertyClasses.addAll(addObjectProperty(owlClass, property));
								    }
									importPredicateStatements(property.getURI());
								}
							}
						}				
					}
				} else { // if it is a simple class
					for (OWLClass owlClass : model.listOWLClasses()) {
						if (initialOntology.getOntClass(owlClass.getID()) != null) {
							if (initialOntology.getOntClass(owlClass.getID()).hasDeclaredProperty(property, true)) {
								if (property.isDatatypeProperty()) {
									this.addDatatypeProperty(owlClass, property);
									importPredicateStatements(property.getURI());
							    } else if (property.isObjectProperty()) {
							    	objectPropertyClasses.addAll(addObjectProperty(owlClass, property));
							    	importPredicateStatements(property.getURI());
							    }
							}
							//importPredicateStatements(property.getURI());
						}
					}
				}
			}
		}
		return objectPropertyClasses;		
	}	
	
	/**
	 * Crates the property set
	 */
	private void createIndividuals() {
		ExtendedIterator<Individual> individuals = initialOntology.listIndividuals();
		
		while(individuals.hasNext()) { // iterate over all individuals
			Individual individual = individuals.next();
			
			OWLIndividual owlIndividual;
			// Creates an individual (if it does not exists in the model)
			if (!individual.isAnon()) {
				owlIndividual = createIndividual(individual);
			} else {
				owlIndividual = createAnonIndividual(individual);
			}
			
			if (owlIndividual != null) {
				StmtIterator stmtIt = individual.listProperties();
				
				while (stmtIt.hasNext()) { // iterate over individual properties
					Statement stmt = stmtIt.next();
					Property property = stmt.getPredicate();
					RDFNode rdfNode = stmt.getObject();

					String propertyNamespace = property.getNameSpace();
		            if (!propertyNamespace.equals(OWL.getURI()) &&
		                    !propertyNamespace.equals(RDF.getURI()) &&
		                    !propertyNamespace.equals(RDFS.getURI())) { // exclude owl, rdf and rdfs vocabulary
		            	if (rdfNode.isLiteral() && rdfNode instanceof Literal) { // if the object is a literal, then use the datatype property
							OWLDatatypeProperty owlDatatypeProperty = model.getOWLDatatypeProperty(property.getURI());
							Literal literal = (Literal) rdfNode;
							String value = literal.getLexicalForm();
							if (value != null) {
								owlIndividual.addValue(owlDatatypeProperty, value);
							} else {
								System.out.println("Cannot assign the individual property");
							}
						} else {
							// The value is another individual
							OWLObjectProperty owlObjectProperty = model.getOWLObjectProperty(property.getURI());
							if (owlObjectProperty == null) {
								System.out.println("Property " + property.getURI() + " cannot be found in current model");
							} else {
								if (rdfNode.isResource()) {
									Resource resource = (Resource) rdfNode;
									if (resource.canAs(Individual.class) && !resource.isAnon()) {
										OWLIndividual value = createIndividual(resource);
										if (value != null) {
											owlIndividual.addValue(owlObjectProperty, value);
										} else {
											System.out.println("Cannot assign the individual property");
										}
									} else { // Anonymous individuals
										OWLIndividual value = createAnonIndividual(resource);
										if (value != null) {
											owlIndividual.addValue(owlObjectProperty, value);
										} else {
											System.out.println("Cannot assign the individual property -anon individual-");
										}
									}
								}
							}
							
						}
		            }
				}
			} else {
				System.out.println("Individual cannot be created: " + individual.toString());
			}
		}
	}		
	
	/**
	 * Gets the individual. If the individual does not exists in the model, this method creates it.
	 * @param individual the Jena Individual class
	 * @return the OWL Individual
	 */
	private OWLIndividual createIndividual(Resource individual) {
		Collection<Resource> individualClasses = getRDFTypes(individual);
		for(Resource individualResource : individualClasses) {
			OWLClass individualClass = model.getOWLClass(individualResource.getURI());
			
			if (individualClass != null) { // if the class belongs to our model
				// Adds the class to the corresponding ontology
				OWLIndividual owlIndividual = model.getOWLIndividual(individual.getURI());
				if (owlIndividual == null) {
					owlIndividual = new OWLIndividual(individual.getURI());
				}
				// Adds the individual to its associated class
				owlIndividual.addClass(individualClass);
				

				// Adds the individual to the corresponding ontology
				OWLOntology owlOntology = model.getOWLOntology(owlIndividual.getOWLIndividualNamespace());
				if (owlOntology == null) {
					owlOntology = new OWLOntology(owlIndividual.getOWLIndividualNamespace());
					model.addOntology(owlOntology);
				}
				
				owlOntology.addIndividual(owlIndividual);					
			}
		}
	
		
		// Already gets the individual
		OWLIndividual owlIndividual = model.getOWLIndividual(individual.getURI());
		
		return owlIndividual;
	}
	
	/**
	 * Gets the individual. If the individual does not exists in the model, this method creates it.
	 * @param individual the Jena Individual class
	 * @return the OWL Individual
	 */
	private OWLIndividual createAnonIndividual(Resource anonIndividual) {
		StmtIterator stmtIt = anonIndividual.listProperties();
		while (stmtIt.hasNext()) { // iterate over individual properties
			Statement stmt = stmtIt.next();
			Property property = stmt.getPredicate();
			RDFNode rdfNode = stmt.getObject();

			if (property.equals(RDF.type)) {
            	if (rdfNode.isResource()) {
					Resource resource = (Resource) rdfNode;
					if (resource.canAs(OntClass.class) && !resource.isAnon()) {
						OWLClass owlClass = model.getOWLClass(resource.getURI());
						if (owlClass != null) {
							OWLIndividual owlIndividual = model.getOWLIndividual(owlClass.getID() +anonIndividual.toString());
							if (owlIndividual == null) {
								owlIndividual = new OWLIndividual(owlClass.getID() +anonIndividual.toString());
							}
							
							// Sets as anonymous
							owlIndividual.setAnon(true);
							
							// Adds the individual to its associated class
							owlIndividual.addClass(owlClass);
							return owlIndividual;
						} else {
							System.out.println("Cannot create the anon individual");
						}
					}
				}	
			}
		}
		
		return null;
	}
	
	/**
	 * Gets the rdf types
	 */
	private Collection<Resource> getRDFTypes(Resource resource) {
		ArrayList<Resource> classTypes = new ArrayList<Resource>();
		StmtIterator stmtIt = resource.listProperties();
		while (stmtIt.hasNext()) {
			Statement stmt = stmtIt.next();
			Property property = stmt.getPredicate();
			RDFNode rdfNode = stmt.getObject();

			if (property.equals(RDF.type)) {
            	if (rdfNode.isResource()) {
					Resource classType = (Resource) rdfNode;
					classTypes.add(classType);
				}
			}
		}
		
		if (classTypes.size() > 0) {
			return classTypes;
		} else {
			return null;
		}
	}
	
	/**
	 * Adds a datatype property to the class
	 * @param smartAppClass the smart application class 
	 * @param property the OntProperty to create
	 */
	private void addDatatypeProperty(OWLClass owlClass, OntProperty property) {
		
		if (!owlClass.containsDatatypeProperty(property.getURI())) {
			// Adds the class to the corresponding ontology
			OWLDatatypeProperty owlProperty = model.getOWLDatatypeProperty(property.getURI());
			if (owlProperty == null) {
				owlProperty = new OWLDatatypeProperty(property.getURI());
			}
			
			// Set the functional property
			owlProperty.setFunctional(property.isFunctionalProperty());
			
			owlProperty.addDomain(owlClass);
			owlClass.addProperty(owlProperty);
				
			// Associates to its ranges
			for (OntResource resource : property.listRange().toList()) {
				if (resource.canAs(EnumeratedClass.class)) {
					if (resource.canAs(DataRange.class)) {
						DataRange dataRange = resource.as(DataRange.class);
						ExtendedIterator<Literal> oneofIterator = dataRange.listOneOf();
						while (oneofIterator.hasNext()) {
							Literal literal = oneofIterator.next();
							owlProperty.setRange(literal.getDatatypeURI());
						}
					}
				} else {
					owlProperty.setRange(resource.getURI());
				}
	    	}

			// Adds the class to the corresponding ontology
			OWLOntology owlOntology = model.getOWLOntology(owlProperty.getOWLPropertyNamespace());
			if (owlOntology == null) {
				owlOntology = new OWLOntology(owlProperty.getOWLPropertyNamespace());
				model.addOntology(owlOntology);
			}
			owlOntology.addProperty(owlProperty);
		}
	}
	
	/**
	 * Adds an object property to the class
	 * @param smartAppClass the smart application class 
	 * @param property the OntProperty to create
	 */
	private Collection<OWLClass> addObjectProperty(OWLClass owlClass, OntProperty property) {

		ArrayList<OWLClass> objectPropertyClasses = new ArrayList<OWLClass>();
		
		if (!owlClass.containsObjectProperty(property.getURI())) {
			// Adds the class to the corresponding ontology
			OWLObjectProperty owlProperty = model.getOWLObjectProperty(property.getURI());
			if (owlProperty == null) {
				owlProperty = new OWLObjectProperty(property.getURI());
			}

			// Set the functional property
			owlProperty.setFunctional(property.isFunctionalProperty());
			
			owlProperty.addDomain(owlClass);
			owlClass.addProperty(owlProperty);
			
			// Associates to its ranges
			for (OntResource resource : property.listRange().toList()) {
	    		if (resource.isClass() && !resource.isAnon()) { // only for named classes
	    			OWLClass rangeClass = model.getOWLClass(resource.getURI());
	    			
	    			if (rangeClass == null) {
	    				rangeClass = new OWLClass(resource.getURI());
	    				// Adds the class to the corresponding ontology
	    				OWLOntology owlOntology = model.getOWLOntology(owlClass.getOWLClassNamespace());
	    				if (owlOntology == null) {
	    					owlOntology = new OWLOntology(owlClass.getOWLClassNamespace());
	    					model.addOntology(owlOntology);
	    				}
	    				rangeClass.setConsumer(owlClass.isConsumer());
	    				rangeClass.setProducer(owlClass.isProducer());
	    				owlOntology.addClass(rangeClass);	    				
		    			objectPropertyClasses.add(rangeClass);	    				
	    			}

	    			owlProperty.addRange(rangeClass);
	    		} else if (resource.canAs(UnionClass.class)) { // anon class, maybe containing a collection of range classes
    				UnionClass unionClass = resource.as(UnionClass.class);
						
					ExtendedIterator<? extends OntClass> it = unionClass.listOperands();
						
					while(it.hasNext()) {
						OntClass componentClass = it.next();
							
						if (!componentClass.isAnon()) { // only for named classes
			    			OWLClass rangeClass = model.getOWLClass(componentClass.getURI());
			    			
			    			if (rangeClass == null) {
			    				rangeClass = new OWLClass(componentClass.getURI());
			    				// Adds the class to the corresponding ontology
			    				OWLOntology owlOntology = model.getOWLOntology(owlClass.getOWLClassNamespace());
			    				if (owlOntology == null) {
			    					owlOntology = new OWLOntology(owlClass.getOWLClassNamespace());
			    					model.addOntology(owlOntology);
			    				}
			    				rangeClass.setConsumer(owlClass.isConsumer());
			    				rangeClass.setProducer(owlClass.isProducer());
			    				owlOntology.addClass(rangeClass);	    				
				    			objectPropertyClasses.add(rangeClass);	    				
			    			}

			    			owlProperty.addRange(rangeClass);
						}
	    			}
	    		}
	    	}
			
			// Adds the class to the corresponding ontology
			OWLOntology owlOntology = model.getOWLOntology(owlProperty.getOWLPropertyNamespace());
			if (owlOntology == null) {
				owlOntology = new OWLOntology(owlProperty.getOWLPropertyNamespace());
				model.addOntology(owlOntology);
			}
			owlOntology.addProperty(owlProperty);			
		}
		
		return objectPropertyClasses;
	}		
	
	/**
	 * Imports the class associated statements
	 * @param owlClassID the uri identifying a class
	 */
	private void importSubjectStatements(String owlClassID) {
		ArrayList<Statement> newStatements = new ArrayList<Statement>();
		
		try {

			Resource searchResource = initialOntology.getResource(owlClassID);

			// Looks up for statements containing the owl class as subject 
			StmtIterator it = initialOntology.listStatements(searchResource, null, (RDFNode)null);
			while(it.hasNext()) {
				Statement stmt = it.next();
				newStatements.add(stmt);
			}

			ontology.add(newStatements);
		} catch (NullPointerException ex) {
			System.out.println("Cannot add to ontology statements about " + owlClassID);
			ex.printStackTrace();
		}
	}

	/**
	 * Imports the class associated statements
	 * @param owlClassID the uri identifying a class
	 */
	private void importPredicateStatements(String owlPropertyID) {
		ArrayList<Statement> newStatements = new ArrayList<Statement>();
		
		try {
			Property searchProperty = initialOntology.getProperty(owlPropertyID);
			// Looks up for statements containing the owl property as predicate 
			StmtIterator it = initialOntology.listStatements(null, searchProperty, (RDFNode)null);
			while(it.hasNext()) {
				Statement stmt = it.next();
				newStatements.add(stmt);
			}
			
			// Looks up for statements about the owl property (as subject) 
			StmtIterator it2 = initialOntology.listStatements((Resource)searchProperty, null, (RDFNode)null);
			while(it2.hasNext()) {
				Statement stmt = it2.next();
				newStatements.add(stmt);
			}			

			ontology.add(newStatements);
		} catch (NullPointerException ex) {
			System.out.println("Cannot add to ontology statements about " + owlPropertyID);
			ex.printStackTrace();
		}
	}

	/**
	 * Imports the class associated statements
	 * @param owlClassID the uri identifying a class
	 */
	private void importObjectStatements(String owlClassID) {
		ArrayList<Statement> newStatements = new ArrayList<Statement>();
		
		try {
			Resource searchResource = initialOntology.getResource(owlClassID);

			StmtIterator it = initialOntology.listStatements(null, null, (RDFNode)searchResource);
			while(it.hasNext()) {
				Statement stmt = it.next();
				newStatements.add(stmt);
			}
			
			ontology.add(newStatements);
		} catch (NullPointerException ex) {
			System.out.println("Cannot add to ontology statements about " + owlClassID);
			ex.printStackTrace();
		}
	}

	
	
	
	/**
	 * Inits the velocity engine
	 */
	private void initVelocityEngine() {
		//logger.debug("Start up the velocity engine...");
		
		try {
			vEngine = new VelocityEngine();
	
			vEngine.setExtendedProperties(getVelocityProperties());
		
			vEngine.init();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}		
	}
	
	/**
	 * Get the extended properties for velocity engine
	 */
	private ExtendedProperties getVelocityProperties() {
		//logger.debug("Setting the properties the Velocity engine");
		ExtendedProperties extProperties = new ExtendedProperties();
		extProperties.setProperty("resource.loader", "class");
		extProperties.setProperty("class.resource.loader.description",
				"Velocity Classpath Resource Loader");
		extProperties.setProperty("class.resource.loader.class",
			"org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
		extProperties.setProperty("classpath.resource.loader.path",
			"./velocitytemplates");
		extProperties.setProperty("class.resource.loader.cache", true);
		extProperties.setProperty("velocimacro.library", "");
		extProperties.setProperty( RuntimeConstants.RUNTIME_LOG_LOGSYSTEM_CLASS,
	      "org.apache.velocity.runtime.log.Log4JLogChute" );
		
		return extProperties;
	}
	
	/**
	 * Creates a new velocity context
	 * @return a velocity context
	 */
	private VelocityContext getVelocityContext() {
		VelocityContext vContext = new VelocityContext();
		vContext.put("basePackage", model.getBasePackageName());
		return vContext;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
		
	}		

	private synchronized void logInfo(String message) {
		message = "ModelGeneratorService - " + message;
	    LogService current = log;
	    if (current != null) {
	      current.log(LogService.LOG_INFO, message);
	    } else {
	      System.out.println(message);
	    }
	}

	public void setLog(LogService value) {
	    if (log != null) {
	      clearLog(log);
	    }
	    log = value;
	}

	public void clearLog(LogService value) {
	    if (log != value) {
	      return;
	    }
	    log = null;
	}

}
