/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.core.generator.kpi.model.ontology;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

/**
 * This class represents the ontology metamodel containing 
 * the internal structure.
 * 
 * @author Fran Ruiz, fran.ruiz@tecnalia.com, ESI
 */
public class OntologyModel {

	/** The ontologies */
	private ArrayList<OWLOntology> ontologies = new ArrayList<OWLOntology>();
	
	/** The base package name for the ontology model */
	private String basePackageName;
	
	/** The prefix mapping */
	private HashMap<String,String> nsPrefixes = new HashMap<String,String>();	
	
	/**
	 * Adds an ontology
	 * @param ontology an Ontology 
	 * @return <code>true</code> if the ontology is correctly added to ontologies
	 */
	public boolean addOntology(OWLOntology ontology) {
		if (!ontologies.contains(ontology)) {
			ontology.setModel(this);
			return ontologies.add(ontology);
		}
		return false;
	}
	
	/**
	 * Removes an ontology
	 * @param ontology the ontology to be removed 
	 * @return <code>true</code> if the ontology is correctly removed from ontologies
	 */
	public boolean removeOntology(OWLOntology ontology) {
		if (ontologies.contains(ontology)) {
			ontology.setModel(null);
			return ontologies.remove(ontology);
		}
		return false;
	}
	
	/**
	 * Gets the list of ontologies
	 * @return the list of ontologies
	 */
	public Collection<OWLOntology> getOntologies() {
		return ontologies;
	}
	
	/**
	 * Gets the OWL ontology contained in the current model
	 * @param id the ontology identifier
	 * @return the OWL Ontology identified by the incoming parameter
	 */
	public OWLOntology getOWLOntology(String id) {
		for (OWLOntology ontology : ontologies) {
			if (id.equals(ontology.getID()+'#')) {
				return ontology;
			}
		}
		return null;
	}		
	
	/**
	 * Establish the base package name
	 * @param packageName A String with the base package name
	 */
	public void setBasePackageName(String packageName) {
		this.basePackageName = packageName;
	}

	/**
	 * Gets the base package name
	 * @return a String with the base package name 
	 */
	public String getBasePackageName() {
		return basePackageName;
	}
	
	/**
	 * Adds a namespace prefix
	 * @param prefix the namespace prefix
	 * @param namespace the namespace uri 
	 * @return the old namespace if the pair &gt;prefix,namespace&lt; was already in the mapping. <code>null</code> if no mapping exists for that prefix
	 */
	public String addNsPrefix(String prefix, String namespace) {
		return nsPrefixes.put(prefix, namespace);
	}
	
	/**
	 * Removes a prefix associated to a namespace
	 * @param prefix the prefix to be removed
	 * @return the associated namespace. <code>null</code> if no prefix was associated
	 */
	public String removeNsPrefix(String prefix) {
		return nsPrefixes.remove(prefix);
	}
	
	/**
	 * Gets the list of imported ontologies
	 * @return the list of imported ontologies
	 */
	public HashMap<String, String> listNamespaces() {
		return nsPrefixes;
	}

	/**
	 * Gets the prefix associated to a namespace 
	 * @param namespace the namespace to search
	 * @return the prefix corresponding to the namespace
	 */
	public String getNsPrefix(String namespace) {
		for (String prefix : nsPrefixes.keySet()) {
			if (nsPrefixes.get(prefix).equals(namespace)) {
				return prefix;
			}
		}
		return null;
	}		
	
	/**
	 * Gets the namespace associated to a prefix 
	 * @param prefix the prefix
	 * @return the namespace corresponding to the prefix
	 */
	public String getNamespace(String prefix) {
		return nsPrefixes.get(prefix);
	}

	/**
	 * Determines if the OWL class is contained in the current model
	 * @param owlClass the OWL Class to be compared
	 * @return <code>true</code> if the owl class is contained in the current model
	 */
	public boolean contains(OWLClass owlClass) {
		for (OWLOntology ontology : ontologies) {
			if (ontology.contains(owlClass)) {
				return true;
			}
		}
		return false;
	}	

	/**
	 * Gets the OWL class contained in the current model
	 * @param id the class identifier
	 * @return the OWL Class identified by the incoming parameter
	 */
	public OWLClass getOWLClass(String id) {
		for (OWLOntology ontology : ontologies) {
			if (ontology.getOWLClass(id) != null) {
				return ontology.getOWLClass(id);
			}
		}
		return null;
	}

	/**
	 * Gets all OWL classes contained in the current model
	 * @return all OWL Classes of current model
	 */	
	public Collection<OWLClass> listOWLClasses() {
		ArrayList<OWLClass> owlClasses = new ArrayList<OWLClass>();
		for (OWLOntology ontology : ontologies) {
			owlClasses.addAll(ontology.getClasses());
		}
		return owlClasses;
	}

	/**
	 * Determines if the OWL individual is contained in the current model
	 * @param owlIndividual the OWL Individual to be compared
	 * @return <code>true</code> if the owl individual is contained in the current model
	 */
	public boolean contains(OWLIndividual owlIndividual) {
		for (OWLOntology ontology : ontologies) {
			if (ontology.contains(owlIndividual)) {
				return true;
			}
		}
		return false;
	}	

	/**
	 * Gets the OWL individual contained in the current model
	 * @param id the individual identifier
	 * @return the OWL Individual identified by the incoming parameter
	 */
	public OWLIndividual getOWLIndividual(String id) {
		for (OWLOntology ontology : ontologies) {
			if (ontology.getOWLIndividual(id) != null) {
				return ontology.getOWLIndividual(id);
			}
		}
		return null;
	}

	/**
	 * Gets all OWL individuals contained in the current model
	 * @return all OWL individuals of current model
	 */	
	public Collection<OWLIndividual> listOWLIndividuals() {
		ArrayList<OWLIndividual> owlIndividuals = new ArrayList<OWLIndividual>();
		for (OWLOntology ontology : ontologies) {
			owlIndividuals.addAll(ontology.getIndividuals());
		}
		return owlIndividuals;
	}	
	
	/**
	 * Gets the OWL property contained in the current model
	 * @param id the property identifier
	 * @return the OWL Property identified by the incoming parameter
	 */
	public OWLObjectProperty getOWLObjectProperty(String id) {
		for (OWLOntology ontology : ontologies) {
			OWLProperty owlProperty = ontology.getOWLProperty(id);
			if (owlProperty != null && owlProperty instanceof OWLObjectProperty) {
				return (OWLObjectProperty) owlProperty;
			}
		}
		return null;
	}	
	
	/**
	 * Gets the OWL property contained in the current model
	 * @param id the property identifier
	 * @return the OWL Property identified by the incoming parameter
	 */
	public OWLDatatypeProperty getOWLDatatypeProperty(String id) {
		for (OWLOntology ontology : ontologies) {
			OWLProperty owlProperty = ontology.getOWLProperty(id);
			if (owlProperty != null && owlProperty instanceof OWLDatatypeProperty) {
				return (OWLDatatypeProperty) owlProperty;
			}
		}
		return null;
	}		
}
