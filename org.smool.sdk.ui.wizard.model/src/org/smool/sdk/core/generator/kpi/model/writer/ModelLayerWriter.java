/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.core.generator.kpi.model.writer;

import java.io.ByteArrayInputStream;
import java.io.StringWriter;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.smool.sdk.common.properties.PropertyLoader;
import org.smool.sdk.core.generator.kpi.model.ontology.OWLClass;
import org.smool.sdk.core.generator.kpi.model.ontology.OntologyModel;

import com.hp.hpl.jena.ontology.OntModel;


/**
 * This class is the writer for the Smart App Project model layer
 * representing the ontology class
 * 
 * @author Fran Ruiz, fran.ruiz@tecnalia.com, ESI
 *
 */
public class ModelLayerWriter {

	/** Smart Application Project */
	private OntologyModel model;
	
	private IPackageFragmentRoot srcFolder;
	
	/** The Velocity engine and context */
	private VelocityEngine vEngine;
	private VelocityContext vContext;	
	
	/** Constants defined in smool.properties */
	private static String JAVACLASS_TEMPLATE = "templates.java.class";
	private static String JAVAIFZ_TEMPLATE = "templates.java.interface";
//	private static String PYTHONCLASS_TEMPLATE = "templates.python.class";
//	private static String PYTHONIFZ_TEMPLATE = "templates.python.interface";
//	private static String CCLASS_TEMPLATE = "templates.c.class";
//	private static String CIFZ_TEMPLATE = "templates.c.interface";
	
	private static String IMPLEMENTATION_PACKAGE = ".impl";
	
	/** The logger */
	private static Logger logger = Logger.getLogger(ModelLayerWriter.class);

	/**
	 * Constructor that stablish the velocity engine and context
	 * @param vEngine velocity engine
	 * @param vContext velocity context
	 */
	public ModelLayerWriter(VelocityEngine vEngine, VelocityContext vContext) {
		this.vEngine = vEngine;
		this.vContext = vContext;
	}	
	
	/**
	 * Creates the smart application project
	 * @param model the smart application project model
	 * @param srcRoot 
	 */
	public void createProjectClasses(OntologyModel model, IPackageFragmentRoot srcRoot) {
		try {
			this.model = model;
			this.srcFolder = srcRoot;
			
			// Throw the class creation
			for (OWLClass owlClass : model.listOWLClasses()) {
	        	//logger.debug("Creating Java Interface and Class from class " +  smartAppClass.getClassName());
	        	createJavaInterface(owlClass);
	        	createJavaClass(owlClass);
	        }

		} catch (Exception ex) {
			logger.error("Cannot generate the project model");
			ex.printStackTrace();
		}
	}
	
	/**
	 * Creates a ICompilationUnit using the class model passed as parameter
	 * @param smartAppClass a SmartAppClass representing a ontology concept
	 * @return the ICompilationUnit of created interface
	 * @throws CoreException when creation is not possible
	 */
	public ICompilationUnit createJavaInterface(OWLClass owlClass) 
			throws CoreException {
		
		try {
			// Creates the interface package name
			StringBuffer packageName = new StringBuffer();
			packageName.append(model.getBasePackageName());
			if (owlClass.getOWLClassPrefix() != null && !owlClass.getOWLClassPrefix().equals("")) {
				packageName.append(".");
				packageName.append(owlClass.getOWLClassPrefix());
			}

			// TODO [FJR] This is only for our java package
	        IPackageFragment packageFragment = srcFolder.getPackageFragment(packageName.toString());
	        
	        // If the package does not exists, create a the package
	        if (!packageFragment.exists()) {
	        	packageFragment = srcFolder.createPackageFragment(packageName.toString(), true,
	                    new NullProgressMonitor());
	        }
	
	        // Generates the string
		       
	    	String template = PropertyLoader.getInstance().getProperty(JAVAIFZ_TEMPLATE);
	        
	        String javaStatement = getOWLClassContent(owlClass, template);
	        
	        // Create the Interface
	        ICompilationUnit compilationUnit = packageFragment.createCompilationUnit("I" + owlClass.getOWLClassName()
	        		+ ".java", javaStatement, true, new NullProgressMonitor()); //$NON-NLS-1$
	        //log.debug("create(ClassInformation) - end"); //$NON-NLS-1$
	        
	        return compilationUnit;
		} catch (CoreException ex) {
			logger.error("CoreException - Failed to create interface: " + ex.toString());
			ex.printStackTrace();
		} catch (Exception ex) {
			logger.error("Failed to create interface: " + ex.toString());
			ex.printStackTrace();
		}
		return null;
    }

	/**
	 * Creates a ICompilationUnit using the class model passed as parameter
	 * @param smartAppClass a SmartAppClass representing a ontology concept
	 * @return the ICompilationUnit of created java class
	 * @throws CoreException when creation is not possible
	 */
	public ICompilationUnit createJavaClass(OWLClass owlClass) 
			throws CoreException {

		//logger.debug("Creating a Java Class..."); //$NON-NLS-1$

		try {
			// Creates the interface package name
			StringBuffer packageName = new StringBuffer();
			packageName.append(model.getBasePackageName());
			if (owlClass.getOWLClassPrefix() != null && !owlClass.getOWLClassPrefix().equals("")) {
				packageName.append(".");
				packageName.append(owlClass.getOWLClassPrefix());
				packageName.append(IMPLEMENTATION_PACKAGE);
			}
		
			// TODO [FJR] This is only for our java package
	        IPackageFragment packageFragment = srcFolder.getPackageFragment(packageName.toString());
	        
	        // If the package does not exists, create a the package
	        if (!packageFragment.exists()) {
	        	packageFragment = srcFolder.createPackageFragment(packageName.toString(), true,
	                    new NullProgressMonitor());
	        }
	
	        // Generates the string
		       
	    	String template = PropertyLoader.getInstance().getProperty(JAVACLASS_TEMPLATE);
	        
	        String javaStatement = getOWLClassContent(owlClass, template);
			
	        // Create the Interface
	        ICompilationUnit compilationUnit = packageFragment.createCompilationUnit(owlClass.getOWLClassName()
	        		+ ".java", javaStatement, true, new NullProgressMonitor()); //$NON-NLS-1$
	        //logger.debug("create(ClassInformation) - end"); //$NON-NLS-1$
	        
	        return compilationUnit;
		} catch (CoreException ex) {
			logger.error("CoreException - Failed to create interface: " + ex.toString());
			ex.printStackTrace();
		} catch (Exception ex) {
			logger.error("Failed to create interface: " + ex.toString());
			ex.printStackTrace();
		}
		return null;
	}

	
	/**
	 * Creates the Java Interface for a SmartAppClass
	 * @param smartAppClass the SmartAppClass
	 * @return the string representing the java class source code
	 * @throws CoreException
	 */
    private String getOWLClassContent(OWLClass owlClass, String template) throws CoreException {
    	try {
	    	return executeVelocity(template, 
	        		new String[] { "cls" }, //$NON-NLS-1$ //$NON-NLS-2$
	                new OWLClass[] { owlClass });
    	} catch (CoreException ex) {
    		logger.error("Cannot create " + owlClass.getOWLClassName());
    		throw new CoreException(ex.getStatus());
    	}
    }

    /**
     * Executes the velocity engine
     * @param templateName the template name
     * @param names the names taken by the objects
     * @param objs the objects
     * @return a string result of executing the engine
     * @throws CoreException
     */
	private String executeVelocity(String templateName, String[] names, Object[] objs)
            throws CoreException {
        try {
            for (int i = 0; i < names.length; i++) {
                vContext.put(names[i], objs[i]);
            }
            Template template = vEngine.getTemplate("/templates/"+templateName); //$NON-NLS-1$
            StringWriter out = new StringWriter();
            template.merge(vContext, out);
            String result = out.toString();
            out.flush();
            out.close();
            return result;
        } catch (Exception e) {
            logger.error("executeVelocity(String, String[], Object[])", e); //$NON-NLS-1$
            IStatus status = new Status(IStatus.ERROR, "org.smool.sdk.ui.projectplugin",
                    IStatus.OK, e.getMessage(), e);
            throw new CoreException(status);
        }
    }

	/**
	 * Creates the ontology model physical file
	 * @param ontModel the JENA OntModel
	 * @param project the project resource
	 * @param ontologyName the ontology to give to the ontology
	 */
	public void createOntologyModel(OntModel ontModel, IProject project, String ontologyName) {
		//logger.debug("creating owl model..."); //$NON-NLS-1$
		try {
			//project.open(null);
			IFolder srcFolder = project.getFolder("src");
			
			if (!srcFolder.exists()) {
				srcFolder.create(true, false, null);
			}
			
			IFolder ontologyFolder = srcFolder.getFolder("ontology");
			
			if (!ontologyFolder.exists()) {
				ontologyFolder.create(true, false, null);
			}
			
			IFile owlFile = ontologyFolder.getFile(ontologyName + ".owl");
			
			if (owlFile.exists()) {
				owlFile.delete(true, new NullProgressMonitor());
			}

			// Gets the model 
	        StringWriter sw = new StringWriter();
	        
	        ontModel.write(sw, "RDF/XML-ABBREV");
	        
	        sw.flush();

	        String owlStatement = sw.toString();
	        
	        owlFile.create(new ByteArrayInputStream(owlStatement.getBytes()), true, null);
		} catch (CoreException ex) {
			logger.error("CoreException - Failed to create owl file: " + ex.toString());
			ex.printStackTrace();
		}
	}
	
	/**
	 * Creates the ontology model physical file
	 * @param project the project resource
	 */
	public void createConfiguration(IProject project) {
		//logger.debug("creating owl model..."); //$NON-NLS-1$
		try {
			//project.open(null);
			IFolder srcFolder = project.getFolder("src");
			
			if (!srcFolder.exists()) {
				srcFolder.create(true, false, null);
			}
			
			IFolder configFolder = srcFolder.getFolder("config");
			
			if (!configFolder.exists()) {
				configFolder.create(true, false, null);
			}
			
			IFile configFile = configFolder.getFile("config.properties");
			
			if (configFile.exists()) {
				return;
			}

			// Create the config.properties file
	        StringBuffer sb = new StringBuffer();
	        
	        sb.append("DEBUG=true\n");
	        sb.append("DEFAULT_TCPIP_GW_PORT=23000\n");
	        sb.append("SIB_NAME=sib1\n");
	        
	        configFile.create(new ByteArrayInputStream(sb.toString().getBytes()), true, null);
		} catch (CoreException ex) {
			logger.error("CoreException - Failed to create config properties file: " + ex.toString());
			ex.printStackTrace();
		}
	}
	
	/**
	 * Creates the ontology model physical file
	 * @param project the project resource
	 */
	public void createOntologyMapping(IProject project, OntologyModel model) {
		//logger.debug("creating owl model..."); //$NON-NLS-1$
		try {
			//project.open(null);
			IFolder srcFolder = project.getFolder("src");
			
			if (!srcFolder.exists()) {
				srcFolder.create(true, false, null);
			}
			
			IFolder configFolder = srcFolder.getFolder("config");
			
			if (!configFolder.exists()) {
				configFolder.create(true, false, null);
			}
			
			IFile mappingFile = configFolder.getFile("mapping.properties");
			
			if (mappingFile.exists()) {
				mappingFile.delete(true, null);
			}			
			
			// Create the mapping.properties file
	        StringBuffer sb = new StringBuffer();
	        
			// Throw the class creation
			for (OWLClass owlClass : model.listOWLClasses()) {
				StringBuffer packageName = new StringBuffer();
				packageName.append(model.getBasePackageName());
				if (!owlClass.getOWLClassPrefix().equals("")) {
					packageName.append(".");
					packageName.append(owlClass.getOWLClassPrefix());
					packageName.append(IMPLEMENTATION_PACKAGE);
				}
				sb.append(packageName.toString()).append(".").append(owlClass.getOWLClassName());
				sb.append("=").append(owlClass.getID());
				sb.append("\n");
				
	        }
	        mappingFile.create(new ByteArrayInputStream(sb.toString().getBytes()), true, null);
		} catch (CoreException ex) {
			logger.error("CoreException - Failed to create mapping properties file: " + ex.toString());
			ex.printStackTrace();
		}
	}
	
}
