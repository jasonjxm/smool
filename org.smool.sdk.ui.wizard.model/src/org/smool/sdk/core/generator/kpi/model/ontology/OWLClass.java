/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.core.generator.kpi.model.ontology;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.lang.WordUtils;
import org.smool.sdk.common.naming.Naming;
import org.smool.sdk.core.generator.kpi.model.ontology.util.URIUtil;


/**
 * This class represents the own class containing 
 * all the properties associated to the class.
 * 
 * @author Fran Ruiz, fran.ruiz@tecnalia.com, ESI
 */
public class OWLClass implements Comparable<OWLClass> {

	/** The class URI */
	private String uri;
	
	/** The superclasses associated to the current class */
	private ArrayList<OWLClass> subClassOf = new ArrayList<OWLClass>();

	/** The equivalent classes */
	private ArrayList<OWLClass> equivalentClasses = new ArrayList<OWLClass>();	
	
	/** The enumerated set of individuals */
	private ArrayList<OWLIndividual> oneOf = new ArrayList<OWLIndividual>();
	
	/** The ontology to what the class belongs */
	private OWLOntology ontology;
	
	/** The datatype properties */
	private ArrayList<OWLDatatypeProperty> datatypeProperties = new ArrayList<OWLDatatypeProperty>();	
	
	/** The datatype properties */
	private ArrayList<OWLObjectProperty> objectProperties = new ArrayList<OWLObjectProperty>();	
	
	/** Determines if the class is a consumer */
	private boolean consumer;
	
	/** Determines if the class is a consumer */
	private boolean producer;
	
	/** Determines if the individual is anonymous */
	private boolean anon;

	/**
	 * Constructor with class URI as param
	 * @param uri class uri
	 */
	public OWLClass(String uri) {
		this.uri = uri;
		this.ontology = null;
		this.anon = false;
	}
	
	/**
	 * Gets the owl class identifier
	 * @return a String representing the owl class identifier
	 */
	public String getID() {
		return uri;
	}
	
	/**
	 * Gets the owl class name
	 * @return a String with the owl class name
	 */
	public String getOWLClassName() {
		String className = URIUtil.getLocalName(uri);
		return Naming.getNormalizedJavaClassName(className);
	}
	
	/**
	 * Gets the uncapitalized owl class name
	 * @return a String with the uncapitalized owl class name
	 */
	public String getUncapitalizedOWLClassName() {
		return WordUtils.uncapitalize(this.getOWLClassName());
	}	
	
	/**
	 * Gets the owl class namespace
	 * @return a String with the owl class namespace
	 */
	public String getOWLClassNamespace() {
		try {
			return URIUtil.getNamespace(uri);
		} catch (NullPointerException ex) {
			System.out.println("Cannot get namespace for class " + uri);
			ex.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * Sets the ontology to what is associated the current owl class
	 * @param ontology the ontology
	 */
	public void setOntology(OWLOntology ontology) {
		this.ontology = ontology;
	}
	
	/**
	 * Gets the prefix associated to the current owl class namespace
	 * @return the prefix associated to the current owl class namespace
	 */
	public String getOWLClassPrefix() {
		try {
			return ontology.getNsPrefix(this.getOWLClassNamespace());
		} catch (NullPointerException ex) {
			System.out.println("Cannot get prefix for class " + this.getID());
			ex.printStackTrace();
		}
		
		return null;
	}		
	
	/**
	 * Adds a superclass to this class
	 * @param owlClass an OWL Class 
	 * @return <code>true</code> if the owlClass is correctly added as superclass of current class
	 */
	public boolean addSubClassOf(OWLClass owlClass) {
		
		if (!subClassOf.contains(owlClass)) {
			return subClassOf.add(owlClass);
		}
		return false;
	}
	
	/**
	 * Removes a subClassOf restriction 
	 * @param owlClass the superclass to be removed
	 * @return <code>true</code> if the owlClass is correctly removed as superclass of current class
	 */
	public boolean removeSubClassOf(OWLClass owlClass) {
		if (subClassOf.contains(owlClass)) {
			return subClassOf.remove(owlClass);
		}
		return false;
	}
	
	/**
	 * Gets the list of direct superclasses
	 * @return the list of direct superclasses
	 */
	public Object[] listSubClassOf() {
		return listSubClassOf(true);
	}
	
	/**
	 * Gets the list of superclasses
	 * @param direct determines if the results are limited to direct superclasses
	 * @return the list of superclasses
	 */
	public Object[] listSubClassOf(boolean direct) {
		if (!direct) {
			ArrayList<OWLClass> superClasses = new ArrayList<OWLClass>();
			superClasses.addAll(subClassOf);
			for (OWLClass owlClass : subClassOf) {
				if (owlClass.listSubClassOf(false) != null && owlClass.listSubClassOf(false).length > 0) {
					OWLClass[] parentSuperClasses = (OWLClass[]) owlClass.listSubClassOf(false);
					for (int i = 0; i < parentSuperClasses.length; i++) {
						superClasses.add(parentSuperClasses[i]);
					}
				}
			}

			return superClasses.toArray();
		} else {
			return subClassOf.toArray();
		}
	}
	
	/**
	 * Returns if the current owl class has superclasses
	 * @return <code>true</code> if the current owl class has superclasses
	 */
	public boolean hasParents() {
		return this.subClassOf.size() > 0;
	}
	
	/**
	 * Returns if the size of current owl class has superclasses
	 * @return the number of direct parents for the current owl class
	 */
	public int getParentListSize() {
		return this.subClassOf.size();
	}	
	
	/**
	 * Adds an equivalent class to this class
	 * @param owlClass an OWL Class 
	 * @return <code>true</code> if the owlClass is correctly added as equivalent class
	 */
	public boolean addEquivalentClass(OWLClass owlClass) {
		
		if (!equivalentClasses.contains(owlClass)) {
			owlClass.addEquivalentClass(this);
			return equivalentClasses.add(owlClass);
		}
		return false;
	}
	
	/**
	 * Removes a equivalent class 
	 * @param owlClass the equivalent class to be removed
	 * @return <code>true</code> if the owlClass is correctly removed as equivalent class of current class
	 */
	public boolean removeEquivalentClass(OWLClass owlClass) {
		if (equivalentClasses.contains(owlClass)) {
			return equivalentClasses.remove(owlClass);
		}
		return false;
	}

	/**
	 * Gets the list of equivalent classes
	 * @return the list of equivalent classes
	 */
	public Collection<OWLClass> getEquivalentClasses() {
		return this.equivalentClasses;
	}	
	
	/**
	 * Gets the list of equivalent classes
	 * @return the list of equivalent classes
	 */
	public Object[] listEquivalentClasses() {
		return this.equivalentClasses.toArray();
	}
	
	/**
	 * Returns if the current owl class has individuals
	 * @return <code>true</code> if the current owl class has individuals
	 */
	public boolean hasIndividuals() {
		boolean nonAnonIndividuals = false;
		for(OWLIndividual individual : oneOf) {
			if (!individual.isAnon()) {
				nonAnonIndividuals = true;
			}
		}
		return nonAnonIndividuals;
	}
	
	/**
	 * Adds an individual as the set of enumeration
	 * @param owlIndividual the individual to be added
	 * @return <code>true</code> if the individual was correctly included as part of enumerated individuals
	 */
	public boolean addOneOf(OWLIndividual owlIndividual) {
		if (!oneOf.contains(owlIndividual)) {
			owlIndividual.setOntology(this.ontology);
			return oneOf.add(owlIndividual);
		}
		return false;
	}
	
	/**
	 * Removes an individual from the set of enumerated individuals
	 * @param individual the individual to be removed
	 * @return <code>true</code> if the individual was correctly removed from the enumerated individuals
	 */
	public boolean removeOneOf(OWLIndividual owlIndividual) {
		owlIndividual.setOntology(null);
		return oneOf.remove(owlIndividual);
	}
	
	/**
	 * Gets the list of individuals associated to the current owl class
	 * @return the list of individuals
	 */
	public Collection<OWLIndividual> getIndividuals() {
		return oneOf;
	}	
	
	/**
	 * Gets the list of enumerated individuals associated to the current owl class
	 * @return the list of enumerated individuals
	 */
	public Object[] listEnumeration() {
		return listOneOf();
	}

	/**
	 * Gets the list of enumerated individuals associated to the current owl class
	 * @return the list of enumerated individuals
	 */
	public Object[] listIndividuals() {
		return listOneOf();
	}	
	
	/**
	 * Gets the list of enumerated individuals associated to the current owl class
	 * @return the list of enumerated individuals
	 */
	public Object[] listOneOf() {
		return oneOf.toArray();
	}	
	
	/**
	 * Gets the list of datatype properties defined for the individual set
	 * @return the list of datatype properties associated to the set of individuals
	 */
	public Collection<OWLDatatypeProperty> getIndividualDatatypeProperties() {
		ArrayList<OWLDatatypeProperty> owlIndividualDatatypeProperties = new ArrayList<OWLDatatypeProperty>();
		for (OWLIndividual owlIndividual : getIndividuals()) {
			for (OWLDatatypeProperty owlDatatypeProperty : owlIndividual.getDatatypeProperties()) {
				if (!owlIndividualDatatypeProperties.contains(owlDatatypeProperty)) {
					owlIndividualDatatypeProperties.add(owlDatatypeProperty);
				}
			}
		}
		return owlIndividualDatatypeProperties;
	}
	
	/**
	 * Gets the list of datatype properties defined for the individual set
	 * @return the list of datatype properties associated to the set of individuals
	 */
	public Object[] listIndividualDatatypeProperties() {
		return this.getIndividualDatatypeProperties().toArray();
	}	
	/**
	 * Gets the size of individual list
	 * @return the size of individual associated to the current owl class
	 */
	public int getIndividualListSize() {
		return oneOf.size();
	}		
	
	/**
	 * Adds a datatype property to the current owl class
	 * @param datatypeProperty an OWL Datatype property 
	 * @return <code>true</code> if the datatype property is correctly added the current class
	 */
	public boolean addProperty(OWLDatatypeProperty datatypeProperty) {
		
		if (!datatypeProperties.contains(datatypeProperty)) {
			datatypeProperty.addDomain(this);
			return datatypeProperties.add(datatypeProperty);
		}
		return false;
	}
	
	/**
	 * Removes a datatype property from the current class 
	 * @param datatypeProperty the datatype property to remove
	 * @return <code>true</code> if the datatype property is correctly removed from the current class
	 */
	public boolean removeProperty(OWLDatatypeProperty datatypeProperty) {
		if (datatypeProperties.contains(datatypeProperty)) {
			datatypeProperty.removeDomain(this);
			return datatypeProperties.remove(datatypeProperty);
		}
		return false;
	}
	
	/**
	 * Gets the list of direct datatype properties
	 * @return the list of direct datatype properties
	 */
	public Collection<OWLDatatypeProperty> getDatatypeProperties() {
		return datatypeProperties;
	}
	
	/**
	 * Gets the list of datatype properties
	 * @return the list of direct datatype properties
	 */
	public Collection<OWLDatatypeProperty> getDatatypeProperties(boolean direct) {
		if (!direct) {
			ArrayList<OWLDatatypeProperty> allDatatypeProperties = new ArrayList<OWLDatatypeProperty>();
			allDatatypeProperties.addAll(datatypeProperties);
			for (OWLClass parentClass : subClassOf) {
				for (OWLDatatypeProperty parentDatatypeProperty : parentClass.getDatatypeProperties(false)) {
					if (!allDatatypeProperties.contains(parentDatatypeProperty)) {
						allDatatypeProperties.add(parentDatatypeProperty);
					}
				}
			}

			return allDatatypeProperties;
		} else {
			return datatypeProperties;
		}		
	}
	
	/**
	 * Determines if the class has datatype properties 
	 * @return <code>true</code> if the current class has at least one datatype property
	 */
	public boolean hasDatatypeProperties() {
		return this.datatypeProperties.size() > 0;
	}	
	
	/**
	 * Determines if the class has datatype properties 
	 * @return <code>true</code> if the current class has at least one datatype property
	 */
	public boolean hasDatatypeProperties(boolean direct) {
		if (this.datatypeProperties.size() > 0) {
			return true;
		} else {
			for (OWLClass parentClass : subClassOf) {
				return parentClass.hasDatatypeProperties(false);
			}
		}
		return false;
	}
	
	/**
	 * Gets the list of direct datatype properties
	 * @return the list of direct datatype properties
	 */
	public Object[] listDatatypeProperties() {
		return listDatatypeProperties(true);
	}
	
	/**
	 * Gets the list of datatype properties
	 * @param direct determines if the results are limited to direct datatype properties
	 * @return the list of datatype properties
	 */
	public Object[] listDatatypeProperties(boolean direct) {
		if (!direct) {
			ArrayList<OWLDatatypeProperty> allDatatypeProperties = new ArrayList<OWLDatatypeProperty>();
			allDatatypeProperties.addAll(datatypeProperties);
			for (OWLClass parentClass : subClassOf) {
				for (OWLDatatypeProperty parentDatatypeProperty : parentClass.getDatatypeProperties(false)) {
					if (!allDatatypeProperties.contains(parentDatatypeProperty)) {
						allDatatypeProperties.add(parentDatatypeProperty);
					}
				}
			}

			return allDatatypeProperties.toArray();
		} else {
			return datatypeProperties.toArray();
		}
	}
	

	/**
	 * Determines if the current owl class contains the current datatype property
	 * @param owlPropertyID the property identifier
	 * @return <code>true</code> if the property is already defined for the current class
	 */
	public boolean containsDatatypeProperty(String owlPropertyID) {
		for(OWLDatatypeProperty owlProperty : datatypeProperties) {
			if (owlProperty.getID().equals(owlPropertyID)) {
				return true;
			}
		}
		return false;
	}		
	
	/**
	 * Adds a object property to the current owl class
	 * @param objectProperty an OWL object property 
	 * @return <code>true</code> if the object property is correctly added the current class
	 */
	public boolean addProperty(OWLObjectProperty objectProperty) {
		if (!objectProperties.contains(objectProperty)) {
			objectProperty.addDomain(this);
			return objectProperties.add(objectProperty);
		}
		return false;
	}
	
	/**
	 * Removes an object property from the current class 
	 * @param objectProperty the object property to be removed
	 * @return <code>true</code> if the datatype property is correctly removed from the current class
	 */
	public boolean removeProperty(OWLObjectProperty objectProperty) {
		if (objectProperties.contains(objectProperty)) {
			objectProperty.removeDomain(this);
			return objectProperties.remove(objectProperty);
		}
		return false;
	}
	
	/**
	 * Gets the list of direct object properties
	 * @return the list of direct object properties
	 */
	public Collection<OWLObjectProperty> getObjectProperties() {
		return objectProperties;
	}
	
	
	/**
	 * Gets the list of direct object properties
	 * @return the list of direct object properties
	 */
	public Collection<OWLObjectProperty> getObjectProperties(boolean direct) {
		if (!direct) {
			ArrayList<OWLObjectProperty> allObjectProperties = new ArrayList<OWLObjectProperty>();
			allObjectProperties.addAll(objectProperties);
			for (OWLClass parentClass : subClassOf) {
				for (OWLObjectProperty parentObjectProperty : parentClass.getObjectProperties(false)) {
					if (!allObjectProperties.contains(parentObjectProperty)) {
						allObjectProperties.add(parentObjectProperty);
					}
				}
			}

			return allObjectProperties;
		} else {
			return objectProperties;
		}		
	}	
	/**
	 * Determines if the class has object properties 
	 * @return <code>true</code> if the current class has at least one object property
	 */
	public boolean hasObjectProperties() {
		return this.objectProperties.size() > 0;
	}
	
	/**
	 * Determines if the class has object properties 
	 * @return <code>true</code> if the current class has at least one object property
	 */
	public boolean hasObjectProperties(boolean direct) {
		if (this.objectProperties.size() > 0) {
			return true;
		} else {
			for (OWLClass parentClass : subClassOf) {
				return parentClass.hasObjectProperties(false);
			}
		}
		return false;
	}
	
	/**
	 * Gets the list of direct object properties
	 * @return the list of direct object properties
	 */
	public Object[] listObjectProperties() {
		return listObjectProperties(true);
	}
	
	/**
	 * Gets the list of object properties
	 * @param direct determines if the results are limited to direct object properties
	 * @return the list of object properties
	 */
	public Object[] listObjectProperties(boolean direct) {
		if (!direct) {
			ArrayList<OWLObjectProperty> allObjectProperties = new ArrayList<OWLObjectProperty>();
			allObjectProperties.addAll(objectProperties);
			for (OWLClass parentClass : subClassOf) {
				for (OWLObjectProperty parentObjectProperty : parentClass.getObjectProperties(false)) {
					if (!allObjectProperties.contains(parentObjectProperty)) {
						allObjectProperties.add(parentObjectProperty);
					}
				}
			}

			return allObjectProperties.toArray();
		} else {
			return objectProperties.toArray();
		}
	}
	
	/**
	 * Gets the list of object properties
	 * @return the list of object properties
	 */
	public Object[] listObjectPropertyRanges() {
		return listObjectPropertyRanges(true);
	}
	
	/**
	 * Gets the list of object properties
	 * @param direct determines if the results are limited to direct object properties
	 * @return the list of object properties
	 */
	public Object[] listObjectPropertyRanges(boolean direct) {
		try {
			ArrayList<OWLClass> objectPropertyRanges = new ArrayList<OWLClass>();
			if (!direct) {
				for(OWLObjectProperty owlProperty : objectProperties) {
					objectPropertyRanges.addAll(owlProperty.getRanges());
				}
				for (OWLClass parentClass : subClassOf) {
					for (OWLObjectProperty parentObjectProperty : parentClass.getObjectProperties(false)) {
						for(OWLClass owlRangeClass : parentObjectProperty.getRanges()) {
							if (!objectPropertyRanges.contains(owlRangeClass)) {
								objectPropertyRanges.add(owlRangeClass);
							}
						}
					}
				}
			} else {
				for(OWLObjectProperty owlProperty : objectProperties) {
					objectPropertyRanges.addAll(owlProperty.getRanges());
				}
			}
			return objectPropertyRanges.toArray();
			
		} catch (Exception ex) {
			System.out.println("Cannot get object property ranges for " + this.getOWLClassName());
			ex.printStackTrace();
		}
		
		return null;
	}		
	
	/**
	 * Determines if the current owl class contains the current object property
	 * @param owlPropertyID the property identifier
	 * @return <code>true</code> if the property is already defined for the current class
	 */
	public boolean containsObjectProperty(String owlPropertyID) {
		for(OWLObjectProperty owlProperty : objectProperties) {
			if (owlProperty.getID().equals(owlPropertyID)) {
				return true;
			}
		}
		return false;
	}	
	
	/**
	 * Sets the consumer mode of the class
	 * @param consumer a boolean value
	 */
	public void setConsumer(boolean consumer) {
		this.consumer = consumer;
		for (OWLClass parentClass : subClassOf) {
			parentClass.setConsumer(this.consumer);
		}	}
	
	/**
	 * Gets the consumer mode of the class
	 * @return a boolean value
	 */
	public boolean isConsumer() {
		return consumer;
	}
	
	/**
	 * Sets the producer mode of the class
	 * @param producer a boolean value
	 */
	public void setProducer(boolean producer) {
		this.producer = producer;
		for (OWLClass parentClass : subClassOf) {
			parentClass.setProducer(this.producer);
		}
	}	
	
	/**
	 * Gets the producer mode of the class
	 * @return a boolean value
	 */
	public boolean isProducer() {
		return producer;
	}

	/**
	 * Marks the class as anon
	 * @param anon a boolean value
	 */
	public void setAnon(boolean anon) {
		this.anon = anon;
	}	
	
	/**
	 * Gets the anon value
	 * @return a boolean value
	 */
	public boolean isAnon() {
		return anon;
	}	
	
	/**
	 * Determines if the class has equivalent classes 
	 * @return <code>true</code> if the current class has at least one equivalent class
	 */
	public boolean hasEquivalentClasses() {
		return this.equivalentClasses.size() > 0;
	}
	
	/**
	 * Determines if the class has non-functional (multivalued) properties 
	 * @return <code>true</code> if the current class has at least one non-functional property
	 */
	public boolean hasNonFunctionalProperties() {
		return hasNonFunctionalProperties(true);
	}
	
	/**
	 * Determines if the class has non-functional (multivalued) properties 
	 * @return <code>true</code> if the current class has at least one non-functional property
	 */
	public boolean hasNonFunctionalProperties(boolean direct) {
		if (direct) {
			for (OWLDatatypeProperty owlProperty : this.datatypeProperties) {
				if (!owlProperty.isFunctional()) {
					return true;
				}
			}

			for (OWLObjectProperty owlProperty : this.objectProperties) {
				if (!owlProperty.isFunctional()) {
					return true;
				}
			}		
			return false;			
		} else {
			if (hasNonFunctionalProperties(true)) {
				return true;
			}
			for (OWLClass parentClass : subClassOf) {
				if (parentClass.hasNonFunctionalProperties(false)) {
					return true;
				}
			}
			return false;
		}
	}
	
	/**
	 * Determines if the class has functional datatype properties 
	 * @return <code>true</code> if the current class has at least one functional datatype property
	 */
	public boolean hasFunctionalDatatypeProperties() {
		return hasFunctionalDatatypeProperties(true);
	}
	
	/**
	 * Determines if the class has functional datatype properties 
	 * @return <code>true</code> if the current class has at least one functional property
	 */
	public boolean hasFunctionalDatatypeProperties(boolean direct) {
		if (direct) {
			for (OWLDatatypeProperty owlProperty : this.datatypeProperties) {
				if (owlProperty.isFunctional()) {
					return true;
				}
			}
			return false;			
		} else {
			if (hasFunctionalDatatypeProperties(true)) {
				return true;
			}
			for (OWLClass parentClass : subClassOf) {
				if (parentClass.hasFunctionalDatatypeProperties(false)) {
					return true;
				}
			}
			return false;
		}
	}	
	
	/**
	 * Determines if the class has non-functional datatype properties 
	 * @return <code>true</code> if the current class has at least one non-functional datatype property
	 */
	public boolean hasNonFunctionalDatatypeProperties() {
		return hasNonFunctionalDatatypeProperties(true);
	}
	
	/**
	 * Determines if the class has functional datatype properties 
	 * @return <code>true</code> if the current class has at least one non-functional datatype property
	 */
	public boolean hasNonFunctionalDatatypeProperties(boolean direct) {
		if (direct) {
			for (OWLDatatypeProperty owlProperty : this.datatypeProperties) {
				if (!owlProperty.isFunctional()) {
					return true;
				}
			}
			return false;			
		} else {
			if (hasNonFunctionalDatatypeProperties(true)) {
				return true;
			}
			for (OWLClass parentClass : subClassOf) {
				if (parentClass.hasNonFunctionalDatatypeProperties(false)) {
					return true;
				}
			}
			return false;
		}
	}
	
	/**
	 * Determines if the class has functional object properties 
	 * @return <code>true</code> if the current class has at least one functional object property
	 */
	public boolean hasFunctionalObjectProperties() {
		return hasFunctionalObjectProperties(true);
	}
	
	/**
	 * Determines if the class has functional object properties 
	 * @return <code>true</code> if the current class has at least one functional object property
	 */
	public boolean hasFunctionalObjectProperties(boolean direct) {
		if (direct) {
			for (OWLObjectProperty owlProperty : this.objectProperties) {
				if (owlProperty.isFunctional()) {
					return true;
				}
			}
			return false;			
		} else {
			if (hasFunctionalObjectProperties(true)) {
				return true;
			}
			for (OWLClass parentClass : subClassOf) {
				if (parentClass.hasFunctionalObjectProperties(false)) {
					return true;
				}
			}
			return false;
		}
	}	
	
	/**
	 * Determines if the class has non-functional datatype properties 
	 * @return <code>true</code> if the current class has at least one non-functional datatype property
	 */
	public boolean hasNonFunctionalObjectProperties() {
		return hasNonFunctionalObjectProperties(true);
	}
	
	/**
	 * Determines if the class has functional datatype properties 
	 * @return <code>true</code> if the current class has at least one non-functional datatype property
	 */
	public boolean hasNonFunctionalObjectProperties(boolean direct) {
		if (direct) {
			for (OWLObjectProperty owlProperty : this.objectProperties) {
				if (!owlProperty.isFunctional()) {
					return true;
				}
			}
			return false;			
		} else {
			if (hasNonFunctionalObjectProperties(true)) {
				return true;
			}
			for (OWLClass parentClass : subClassOf) {
				if (parentClass.hasNonFunctionalObjectProperties(false)) {
					return true;
				}
			}
			return false;
		}
	}		

	@Override
	public int compareTo(OWLClass owlClass) {
		return this.uri.compareTo(owlClass.getID());
	}
	
	/**
	 * Compares the equality of ontologies
	 */
	@Override 
	public boolean equals( Object obj ) {
		if ( this == obj ) return true;
		if ( !(obj instanceof OWLClass) ) return false;

		OWLClass owlClass = (OWLClass) obj;
		try {
			return this.uri.equals(owlClass.getID());
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	/**
	 * Returns a string representation of the owl individual.
	 * @return a string representation of the owl individual
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("OWL Class ID: ").append(this.getID());
		sb.append("\n").append("SC(").append(this.subClassOf.size()).append(") ");
		sb.append("I(").append(this.oneOf.size()).append(") ");
		sb.append("Eq(").append(this.equivalentClasses.size()).append(") ");
		sb.append("DTP(").append(this.datatypeProperties.size()).append(") ");
		sb.append("OP(").append(this.objectProperties.size()).append(") ");
		return sb.toString();
	}	
}
