/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.core.generator.kpi.model.ontology;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.apache.commons.lang.WordUtils;
import org.smool.sdk.common.naming.Naming;
import org.smool.sdk.core.generator.kpi.model.ontology.exception.OntologyModelCreationException;
import org.smool.sdk.core.generator.kpi.model.ontology.util.URIUtil;



/**
 * This class represents the ontology individuals.
 * 
 * @author Fran Ruiz, fran.ruiz@tecnalia.com, ESI
 */
public class OWLIndividual implements Comparable<OWLIndividual> {

	/** The ontology URI */
	public String uri;
	
	/** The owl classes */
	private ArrayList<OWLClass> owlClasses = new ArrayList<OWLClass>();
	
	/** The ontology to what the class belongs */
	private OWLOntology ontology;	

	/** The owl datatype property values */
	private HashMap<OWLDatatypeProperty, Collection<String>> datatypeValues = new HashMap<OWLDatatypeProperty, Collection<String>>();

	/** The owl datatype property values */
	private HashMap<OWLObjectProperty, Collection<OWLIndividual>> objectValues = new HashMap<OWLObjectProperty, Collection<OWLIndividual>>();
	
	/** Determines if the individual is anonymous */
	private boolean anon;

	/**
	 * Constructor with ontology URI as param
	 * @param uri Ontology uri
	 */
	public OWLIndividual(String uri) {
		this.uri = uri;
		this.anon = false;
	}
	
	/**
	 * Gets the individual identifier
	 * @return a String representing the individual identifier
	 */
	public String getID() {
		return uri;
	}
	
	/**
	 * Gets the owl individual name
	 * @return a string with the owl individual name
	 */
	public String getOWLIndividualName() {
		String individualName = URIUtil.getLocalName(uri);
		return Naming.getNormalizedJavaClassName(individualName);
	}
	
	/**
	 * Gets the uncapitalized owl individual name
	 * @return a string with the uncapitalized owl individual name
	 */
	public String getUncapitalizedOWLIndividualName() {
		return WordUtils.uncapitalize(this.getOWLIndividualName());
	}
	
	/**
	 * Gets the capitalized owl individual name
	 * @return a string with the capitalized owl individual name
	 */
	public String getCapitalizedOWLIndividualName() {
		return this.getOWLIndividualName().toUpperCase();
	}	
	
	/**
	 * Gets the owl individual namespace
	 * @return a String with the owl individual namespace
	 */
	public String getOWLIndividualNamespace() {
		try {
			return URIUtil.getNamespace(uri);
		} catch (NullPointerException ex) {
			System.out.println("Cannot get namespace for individual " + uri);
			ex.printStackTrace();
		}
		
		return null;
	}	
	
	/**
	 * Sets the ontology to what is associated the current owl individual
	 * @param ontology the ontology
	 */
	public void setOntology(OWLOntology ontology) {
		this.ontology = ontology;
	}
	
	/**
	 * Gets the prefix associated to the current owl individual namespace
	 * @return the prefix associated to the current owl individual namespace
	 */
	public String getOWLIndividualPrefix() {
		return ontology.getNsPrefix(this.getOWLIndividualNamespace());
	}	
	
	/**
	 * Adds an owl class to this individual
	 * @param owlClass an OWL Class 
	 * @return <code>true</code> if the owlClass is correctly added in the individual
	 */
	public boolean addClass(OWLClass owlClass) {
		
		if (!owlClasses.contains(owlClass)) {
			owlClass.addOneOf(this);
			return owlClasses.add(owlClass);
		}
		return false;
	}
	
	/**
	 * Removes an owl class from the individual 
	 * @param owlClass the owl class to be removed
	 * @return <code>true</code> if the owlClass is correctly removed from the individual
	 */
	public boolean removeClass(OWLClass owlClass) {
		if (owlClasses.contains(owlClass)) {
			owlClass.removeOneOf(this);
			return owlClasses.remove(owlClass);
		}
		return false;
	}
	
	/**
	 * Adds a value to the current individual
	 * @param owlDatatypeProperty the datatype property in what the value takes value
	 * @param value the lexical representation of value 
	 * @return <code>true</code> if the value is correctly added
	 */
	public boolean addValue(OWLDatatypeProperty owlDatatypeProperty, String value) {
		if (datatypeValues.containsKey(owlDatatypeProperty)) {
			return datatypeValues.get(owlDatatypeProperty).add(value);
		} else {
			ArrayList<String> values = new ArrayList<String>();
			values.add(value);
			datatypeValues.put(owlDatatypeProperty, values);
			return true;
		}
	}
	
	/**
	 * Gets the list of datatype properties associated to the current individual
	 * @return the list of datatypes assigned to the current individual
	 */
	public Collection<OWLDatatypeProperty> getDatatypeProperties() {
		return datatypeValues.keySet();
	}		
	
	/**
	 * Gets the list of datatype properties associated to the current individual
	 * @return the list of datatypes assigned to the current individual
	 */
	public Object[] listDatatypeProperties() {
		return datatypeValues.keySet().toArray();
	}	

	/**
	 * Gets the value associated to the property
	 * @param property the datatype property
	 * @return the string representation of the value
	 * @throws OntologyModelCreationException 
	 */
	public String getDatatypePropertyValue(Object property) throws OntologyModelCreationException {
		if (property instanceof OWLDatatypeProperty) {
			OWLDatatypeProperty owlDatatypeProperty = (OWLDatatypeProperty) property;
			if (datatypeValues.containsKey(owlDatatypeProperty)) {
				if (datatypeValues.get(owlDatatypeProperty).size() > 1) {
					System.out.println("Property "+owlDatatypeProperty.getID()+" for individual " + this.getID() + " has more than one value");
				}
				for(String value : datatypeValues.get(owlDatatypeProperty)) {
					if (owlDatatypeProperty.isNumber()) {
						return value;
					} else {
						StringBuilder sb = new StringBuilder();
						sb.append("\"").append(value).append("\"");
						return sb.toString();
					}
				}
				
				return null;
			} else {
				throw new OntologyModelCreationException("Property "+owlDatatypeProperty.getID()+" not defined for the current individual: " + this.getID());
			}
		} else {
			throw new OntologyModelCreationException("Cannot get the datatype property value");
		}
	}
	
	/**
	 * Adds a value to the current individual
	 * @param owlObjectProperty the object property in what the value takes value
	 * @param value the individual 
	 * @return <code>true</code> if the value is correctly added
	 */
	public boolean addValue(OWLObjectProperty owlObjectProperty, OWLIndividual value) {
		if (objectValues.containsKey(owlObjectProperty)) {
			return objectValues.get(owlObjectProperty).add(value);
		} else {
			ArrayList<OWLIndividual> values = new ArrayList<OWLIndividual>();
			values.add(value);
			objectValues.put(owlObjectProperty, values);
			return true;
		}
	}
	
	/**
	 * Gets the list of datatype properties associated to the current individual
	 * @return the list of datatypes assigned to the current individual
	 */
	public Collection<OWLObjectProperty> getObjectProperties() {
		return objectValues.keySet();
	}	
	
	/**
	 * Gets the list of datatype properties associated to the current individual
	 * @return the list of datatypes assigned to the current individual
	 */
	public Object[] listObjectProperties() {
		return objectValues.keySet().toArray();
	}	
	
	/**
	 * Gets the value associated to the property
	 * @param property the datatype property
	 * @return the string representation of the value
	 * @throws OntologyModelCreationException 
	 */
	public OWLIndividual getObjectPropertyValue(Object property) throws OntologyModelCreationException {
		if (property instanceof OWLObjectProperty) {
			OWLObjectProperty owlObjectProperty = (OWLObjectProperty) property;
			if (objectValues.containsKey(owlObjectProperty)) {
				if (objectValues.get(owlObjectProperty).size() > 1) {
					System.out.println("Property "+owlObjectProperty.getID()+" for individual " + this.getID() + " has more than one value");
				}
				for(OWLIndividual value : objectValues.get(owlObjectProperty)) {
					return value;
				}
				
				return null;
			} else {
				throw new OntologyModelCreationException("Property "+owlObjectProperty.getID()+" not defined for the current individual: " + this.getID());
			}
		} else {
			throw new OntologyModelCreationException("Cannot get the datatype property value");
		}
	}
	/**
	 * Gets the list of owl classes
	 * @return the list of owl classes
	 */
	public Object[] listClasses() {
		return owlClasses.toArray();
	}
	
	/**
	 * Marks the class as anon
	 * @param anon a boolean value
	 */
	public void setAnon(boolean anon) {
		this.anon = anon;
	}	
	
	/**
	 * Gets the anon value
	 * @return a boolean value
	 */
	public boolean isAnon() {
		return anon;
	}	
	
	@Override
	public int compareTo(OWLIndividual owlIndividual) {
		return this.uri.compareTo(owlIndividual.getID());
	}
	
	/**
	 * Compares the equality of ontologies
	 */
	@Override 
	public boolean equals( Object obj ) {
		if ( this == obj ) return true;
		if ( !(obj instanceof OWLIndividual) ) return false;

		OWLIndividual owlIndividual = (OWLIndividual) obj;
		return this.uri.equals(owlIndividual.getID());
	}

	/**
	 * Returns a string representation of the owl individual.
	 * @return a string representation of the owl individual
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("OWL Individual ID: ").append(this.getID());
		sb.append("\n").append("DTV(").append(this.datatypeValues.toString()).append(") ");
		sb.append("OV(").append(this.objectValues.toString()).append(")");
		return sb.toString();
	}
}
