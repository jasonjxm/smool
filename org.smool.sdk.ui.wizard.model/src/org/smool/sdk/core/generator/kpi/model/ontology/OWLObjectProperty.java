/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.core.generator.kpi.model.ontology;

import java.util.ArrayList;
import java.util.Collection;

/**
 * This class represents an object property defined in an ontology.
 * 
 * @author Fran Ruiz, fran.ruiz@tecnalia.com, ESI
 */
public class OWLObjectProperty extends OWLProperty {

	private ArrayList<OWLClass> ranges = new ArrayList<OWLClass>();
	
	/**
	 * The constructor
	 * @param uri the object property uri
	 */
	public OWLObjectProperty(String uri) {
		super(uri);
	} 

	/**
	 * Adds a range class to this property
	 * @param owlClass an OWL Class 
	 * @return <code>true</code> if the owlClass is correctly added as range class
	 */
	public boolean addRange(OWLClass owlClass) {
		
		if (!ranges.contains(owlClass)) {
			return ranges.add(owlClass);
		}
		return false;
	}
	
	/**
	 * Removes a range class 
	 * @param owlClass the range class to be removed
	 * @return <code>true</code> if the owlClass is correctly removed as range class of current property
	 */
	public boolean removeRange(OWLClass owlClass) {
		if (ranges.contains(owlClass)) {
			return ranges.remove(owlClass);
		}
		return false;
	}

	/**
	 * Gets the list of range classes
	 * @return the list of range classes
	 */
	public Collection<OWLClass> getRanges() {
		return this.ranges;
	}	
	
	/**
	 * Gets the list of range classes
	 * @return the list of range classes
	 */
	public Object[] listRanges() {
		return this.ranges.toArray();
	}		

	/**
	 * Returns a string representation of the owl object property.
	 * @return a string representation of the owl object property
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("OWL Object Property ID: ").append(this.getID());
		sb.append("\n").append("D(").append(this.domains.toString()).append(") ");
		sb.append("R(").append(this.ranges.toString()).append(")");
		return sb.toString();
	}	
}
