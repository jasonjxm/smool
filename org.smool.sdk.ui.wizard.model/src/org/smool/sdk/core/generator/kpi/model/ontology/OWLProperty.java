/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.core.generator.kpi.model.ontology;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.lang.WordUtils;
import org.smool.sdk.common.naming.Naming;
import org.smool.sdk.core.generator.kpi.model.ontology.util.URIUtil;


/**
 * This class represents a property defined in an ontology.
 * 
 * @author Fran Ruiz, fran.ruiz@tecnalia.com, ESI
 */
public abstract class OWLProperty implements Comparable<OWLProperty> {
	
	/** The property URI */
	protected String uri;
	
	/** If the property is functional */
	protected boolean functional;
	
	/** Domains */
	protected ArrayList<OWLClass> domains = new ArrayList<OWLClass>();
	
	/** The ontology to what the property belongs */
	protected OWLOntology ontology;	
	
	/**
	 * Constructor with property URI as param
	 * @param uri property uri
	 */
	public OWLProperty(String uri) {
		this.uri = uri;
		this.functional = false; // by default, we stablish functional to false
	}
	
	/**
	 * Gets the owl class identifier
	 * @return a String representing the owl property identifier
	 */
	public String getID() {
		return uri;
	}

	/**
	 * Gets the owl property name
	 * @return a String with the owl property name
	 */
	public String getOWLPropertyName() {
		String propertyName = URIUtil.getLocalName(uri);
		String objectPropertyName = Naming.getNormalizedJavaPropertyName(propertyName);
		return objectPropertyName;
	}
	
	/**
	 * Gets the owl property name
	 * @return a String with the owl property name
	 */
	public String getUncapitalizedOWLPropertyName() {
		String propertyName = URIUtil.getLocalName(uri);
		String objectPropertyName = Naming.getNormalizedJavaPropertyName(propertyName);
		return WordUtils.uncapitalize(objectPropertyName);
	}	
	
	/**
	 * Gets the owl property name
	 * @return a String with the owl property name
	 */
	public String getCapitalizedOWLPropertyName() {
		String propertyName = URIUtil.getLocalName(uri);
		String objectPropertyName = Naming.getNormalizedJavaPropertyName(propertyName);
		return WordUtils.capitalize(objectPropertyName);
	}
	
	/**
	 * Gets the owl property namespace
	 * @return a String with the owl property namespace
	 */
	public String getOWLPropertyNamespace() {
		return URIUtil.getNamespace(uri);
	}	
	
	/**
	 * Gets the prefix associated to the current owl property namespace
	 * @return the prefix associated to the current owl property namespace
	 */
	public String getOWLPropertyPrefix() {
		try {
			return ontology.getNsPrefix(this.getOWLPropertyNamespace());
		} catch (Exception ex) {
			System.out.println("Cannot get prefix for property " + this.getOWLPropertyName());
			ex.printStackTrace();
		}
		return null;
	}		
	
	/**
	 * Determines if the property is functional
	 * @param functional <code>true</code> if the property is functional
	 */
	public void setFunctional(boolean functional) {
		this.functional = functional;
	}

	/**
	 * Gets the functional value
	 * @return <code>true</code> if the property is functional
	 */
	public boolean isFunctional() {
		return functional;
	}		
	
	/**
	 * Adds a domain class to this property
	 * @param owlClass an OWL Class 
	 * @return <code>true</code> if the owlClass is correctly added as domain class
	 */
	public boolean addDomain(OWLClass owlClass) {
		
		if (!domains.contains(owlClass)) {
			return domains.add(owlClass);
		}
		return false;
	}
	
	/**
	 * Removes a domain class 
	 * @param owlClass the domain class to be removed
	 * @return <code>true</code> if the owlClass is correctly removed as domain class of current property
	 */
	public boolean removeDomain(OWLClass owlClass) {
		if (domains.contains(owlClass)) {
			return domains.remove(owlClass);
		}
		return false;
	}

	/**
	 * Gets the list of domain classes
	 * @return the list of domain classes
	 */
	public Collection<OWLClass> getDomains() {
		return this.domains;
	}	
	
	/**
	 * Gets the list of domain classes
	 * @return the list of domain classes
	 */
	public Object[] listDomains() {
		return this.domains.toArray();
	}	
	
	/**
	 * Sets the ontology to what is associated the ontology
	 * @param ontology the ontology
	 */
	public void setOntology(OWLOntology ontology) {
		this.ontology = ontology;
	}	
		
	@Override
	public int compareTo(OWLProperty owlProperty) {
		return this.uri.compareTo(owlProperty.getID());
	}
	
	/**
	 * Compares the equality of ontologies
	 */
	@Override 
	public boolean equals( Object obj ) {
		if ( this == obj ) return true;
		if ( !(obj instanceof OWLProperty) ) return false;

		OWLProperty owlProperty = (OWLProperty) obj;
		return this.uri.equals(owlProperty.getID());
	}

	/**
	 * Returns a string representation of the owl property.
	 * @return a string representation of the owl property
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("OWL Property ID: ").append(this.getID());
		return sb.toString();
	}
}
