/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.core.generator.kpi.model.ontology;

/**
 * This class represents a datatype property defined in an ontology.
 * 
 * @author Fran Ruiz, fran.ruiz@tecnalia.com, ESI
 */
public class OWLDatatypeProperty extends OWLProperty {

	/**
	 * Enumerated XML Schema Datatypes
	 * @see http://www.w3.org/TR/2004/REC-xmlschema-2-20041028/#built-in-datatypes
	 */
	public enum XMLSchemaDatatype {  
		STRING			("string","String"), // Primitive 3.2.1
		BOOLEAN			("boolean", "Boolean"), // Primitive 3.2.2
		DECIMAL			("decimal", "java.math.BigDecimal"), // Primitive 3.2.3
		FLOAT			("float", "Float"), // Primitive 3.2.4
		DOUBLE			("double", "Double"), // Primitive 3.2.5
		DURATION		("duration", "javax.xml.datatype.Duration"), // Primitive 3.2.6
		DATETIME		("dateTime", "javax.xml.datatype.XMLGregorianCalendar"), // Primitive 3.2.7
		TIME			("time", "javax.xml.datatype.XMLGregorianCalendar"), // Primitive 3.2.8
		DATE 			("date", "javax.xml.datatype.XMLGregorianCalendar"), // Primitive 3.2.9
		GYEARMONTH		("gYearMonth", "javax.xml.datatype.XMLGregorianCalendar"), // Primitive 3.2.10
		GYEAR			("gYear", "javax.xml.datatype.XMLGregorianCalendar"), // Primitive 3.2.11
		GMONTHDAY		("gMonthDay", "javax.xml.datatype.XMLGregorianCalendar"), // Primitive 3.2.12
		GDAY			("gDay", "javax.xml.datatype.XMLGregorianCalendar"), // Primitive 3.2.13
		GMONTH			("gMonth", "javax.xml.datatype.XMLGregorianCalendar"), // Primitive 3.2.14
		HEXBINARY		("hexBinary", "byte[]"), // Primitive 3.2.15
		BASE64BINARY	("base64Binary", "byte[]"), // Primitive 3.2.16
		ANYURI			("anyURI", "java.net.URI"), // Primitive 3.2.17
		QNAME			("QName", "javax.xml.namespace.QName"),	// Primitive 3.2.18
		NOTATION		("NOTATION", "javax.xml.namespace.QName"),	// Primitive 3.2.19
		
		INTEGER			("integer","java.math.BigInteger"), // Derived 3.3.13
		LONG			("long", "Long"), // Derived 3.3.16
		INT				("int", "Integer"), // Derived 3.3.17 
		SHORT			("short", "Short"), // Derived 3.3.18
		BYTE			("byte", "Byte"), // Derived 3.3.19

		UNSIGNEDINT		("unsignedInt", "Long"), // Derived 3.3.22
		UNSIGNEDSHORT	("unsignedShort", "Integer"), // Derived 3.3.23
		UNSIGNEDBYTE	("unsignedByte", "Short"), // Derived 3.3.24
		ANYSIMPLETYPE	("anySimpleType", "java.lang.Object"); // ur-type 4.1
		
		private final String xsdDatatype;
		private final String javaMapping;
		
		XMLSchemaDatatype(String xsdDatatype, String javaMapping) {
			this.xsdDatatype = xsdDatatype;
			this.javaMapping = javaMapping;
		}
		
		public String getXSDDatatype() {
			return xsdDatatype;
		}

		public String getJavaMapping() {
			return javaMapping;
		}
		
		public static XMLSchemaDatatype findDatatype(String value) {
			for(XMLSchemaDatatype datatype : values()) {
				if(datatype.getXSDDatatype().equals(value)) {
					return datatype;
				}
			}
			return null;
		}
	}
	
	/** The datatype property datatype */
	private XMLSchemaDatatype range;
	private String xsdDatatype;

	private final String XSD_PREFIX = "xsd:";
	private final String XSD_NAMESPACE = "http://www.w3.org/2001/XMLSchema#";
	
	
	/**
	 * The constructor
	 * @param uri the datatype property uri
	 */
	public OWLDatatypeProperty(String uri) {
		super(uri);
		range = null;
		xsdDatatype = null;
	}
	
	/**
	 * Sets the range for the datatype property
	 * @param datatype the XML-Schema datatype
	 */
	public void setRange(String datatype) {
		String rawDatatype = getRawDatatype(datatype);
		XMLSchemaDatatype xsdDatatype = XMLSchemaDatatype.findDatatype(rawDatatype);
		if (xsdDatatype != null) {
			this.range = xsdDatatype;
		} else {
			this.range = null;
			this.xsdDatatype = datatype;
		}
	}

	/**
	 * Gets the range for the datatype property
	 * @return the Java datatype
	 */
	public String getJavaMappedRange() {
		if (range != null && range.getJavaMapping() != null) {
			return range.getJavaMapping();
		} else {
			// we stablish the java.lang.Object when the range is not defined
			return XMLSchemaDatatype.ANYSIMPLETYPE.getJavaMapping();
		}
	}	
	
	/**
	 * Gets the range for the datatype property
	 * @return the Java datatype
	 */
	public String getXSDRange() {
		if (range != null) {
			StringBuilder sb = new StringBuilder();
			sb.append(XSD_PREFIX);
			sb.append(range.getXSDDatatype());
			return sb.toString();
		} else if (xsdDatatype != null && isXSDDatatype(xsdDatatype)) {
			StringBuilder sb = new StringBuilder();
			sb.append(XSD_PREFIX);
			sb.append(getRawDatatype(this.xsdDatatype));
			return sb.toString();
		} else if (xsdDatatype != null && !isXSDDatatype(xsdDatatype)) {
			return this.xsdDatatype;
		} else  {
			return null;
		}
	}	
	
	/**
	 * Gets the raw XSD datatype
	 * @param datatype the full datatype (including prefix or namespace)
	 * @return the raw xml schema datatype (excluding prefix or namespace)
	 */
	private String getRawDatatype(String datatype) {
		if (datatype.startsWith(XSD_PREFIX)) {
			return datatype.substring(XSD_PREFIX.length());
		} else if (datatype.startsWith(XSD_NAMESPACE)) {
			return datatype.substring(XSD_NAMESPACE.length());
		} else {
			return datatype;
		}
		
	}
	
	/**
	 * Determines if the datatype is XSD
	 * @param datatype the full datatype (including prefix or namespace)
	 * @return a boolean determining if the datatype has the XSD namespace or prefix
	 */
	private boolean isXSDDatatype(String datatype) {
		if (datatype.startsWith(XSD_PREFIX) || datatype.startsWith(XSD_NAMESPACE)) {
			return true;
		} else {
			return false;
		}
		
	}	
	
	@Override
	public int compareTo(OWLProperty owlProperty) {
		return this.uri.compareTo(owlProperty.getID());
	}
	
	/**
	 * Compares the equality of ontologies
	 */
	@Override 
	public boolean equals( Object obj ) {
		if ( this == obj ) return true;
		if ( !(obj instanceof OWLDatatypeProperty) ) return false;

		OWLDatatypeProperty owlProperty = (OWLDatatypeProperty) obj;
		return this.uri.equals(owlProperty.getID());
	}		

	/**
	 * Returns a string representation of the owl datatype property.
	 * @return a string representation of the owl datatype property
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("OWL Datatype Property ID: ").append(this.getID());
		sb.append("\n").append("D(").append(this.domains.toString()).append(") ");
		sb.append("R(").append(this.range.getXSDDatatype()).append(")");
		return sb.toString();
	}
	
	public boolean isNumber() {
		return this.range == XMLSchemaDatatype.DOUBLE ||
			this.range == XMLSchemaDatatype.FLOAT ||
			this.range == XMLSchemaDatatype.INT ||
			this.range == XMLSchemaDatatype.INTEGER ||
			this.range == XMLSchemaDatatype.LONG ||
			this.range == XMLSchemaDatatype.SHORT ||
			this.range == XMLSchemaDatatype.BYTE ||
			this.range == XMLSchemaDatatype.UNSIGNEDBYTE ||
			this.range == XMLSchemaDatatype.UNSIGNEDINT ||
			this.range == XMLSchemaDatatype.UNSIGNEDSHORT;
	}
}
