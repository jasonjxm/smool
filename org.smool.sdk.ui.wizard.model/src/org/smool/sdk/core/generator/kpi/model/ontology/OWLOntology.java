/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.core.generator.kpi.model.ontology;

import java.util.ArrayList;
import java.util.Collection;

/**
 * This class represents the ontology metamodel containing 
 * the internal structure.
 * 
 * @author Fran Ruiz, fran.ruiz@tecnalia.com, ESI
 */
public class OWLOntology implements Comparable<OWLOntology> {

	/** The ontology URI */
	private String uri;
	
	/** The imported ontologies */
	private ArrayList<OWLOntology> imports = new ArrayList<OWLOntology>();
	
	/** The owl classes */
	private ArrayList<OWLClass> owlClasses = new ArrayList<OWLClass>();
	
	/** The owl properties */
	private ArrayList<OWLProperty> owlProperties = new ArrayList<OWLProperty>();
	
	/** The owl classes */
	private ArrayList<OWLIndividual> owlIndividuals = new ArrayList<OWLIndividual>();

	/** The parent model */
	private OntologyModel model;
	
	/**
	 * Constructor with ontology URI as param
	 * @param uri Ontology uri
	 */
	public OWLOntology(String uri) {
		//System.out.println("Creating new ontology: " + uri);
		if (uri.endsWith("#")) {
			uri = uri.substring(0, uri.length() - 1);
		}
		this.uri = uri;
		this.model = null;
	}
	
	/**
	 * Gets the ontology identifier
	 * @return a String representing the ontology identifier
	 */
	public String getID() {
		return uri;
	}
	
	/**
	 * Adds an importing ontology
	 * @param ontology an Ontology 
	 * @return <code>true</code> if the ontology is correctly added to imports
	 */
	public boolean addImports(OWLOntology ontology) {
		if (!imports.contains(ontology)) {
			return imports.add(ontology);
		}
		return false;
	}
	
	/**
	 * Removes an imported ontology
	 * @param ontology the ontology to be removed from imports
	 * @return <code>true</code> if the ontology is correctly removed from imports
	 */
	public boolean removeImports(OWLOntology ontology) {
		if (imports.contains(ontology)) {
			return imports.remove(ontology);
		}
		return false;
	}
	
	/**
	 * Gets the list of imported ontologies
	 * @return the list of imported ontologies
	 */
	public Object[] listImports() {
		return imports.toArray();
	}
	
	/**
	 * Sets the parent model
	 * @param model the parent model 
	 */
	public void setModel(OntologyModel model) {
		this.model = model;
		for (OWLOntology owlOntology : imports) {
			owlOntology.setModel(model);
		}
	}
	
	/**
	 * Gets the current model
	 * @return the associated model
	 */
	public OntologyModel getModel() {
		return model;
	}
	
	/**
	 * Adds an owl class to this ontology
	 * @param owlClass an OWL Class 
	 * @return <code>true</code> if the owlClass is correctly added in the ontology
	 */
	public boolean addClass(OWLClass owlClass) {
		
		if (!owlClasses.contains(owlClass)) {
			owlClass.setOntology(this);
			return owlClasses.add(owlClass);
		}
		return false;
	}
	
	/**
	 * Removes an owl class from the ontology 
	 * @param owlClass the owl class to be removed
	 * @return <code>true</code> if the owlClass is correctly removed from the ontology
	 */
	public boolean removeClass(OWLClass owlClass) {
		if (owlClasses.contains(owlClass)) {
			owlClass.setOntology(null);
			return owlClasses.remove(owlClass);
		}
		return false;
	}
	
	/**
	 * Gets the list of owl classes
	 * @return the list of owl classes
	 */
	public Object[] listClasses() {
		return owlClasses.toArray();
	}
	
	/**
	 * Determines if the OWL class is contained in the current model
	 * @param owlSearchClass the OWL Class to be compared
	 * @return <code>true</code> if the owl class is contained in the current model
	 */
	public boolean contains(OWLClass owlSearchClass) {
		for (OWLClass owlClass : owlClasses) {
			if (owlClass.equals(owlSearchClass)) {
				return true;
			}
		}
		return false;
	}	
	
	/**
	 * Gets the OWL class contained in the current ontology
	 * @param id the class identifier
	 * @return the OWL Class identified by the incoming parameter
	 */
	public OWLClass getOWLClass(String id) {
		for (OWLClass owlClass : owlClasses) {
			if (owlClass.getID().equals(id)) {
				return owlClass;
			}
		}
		return null;
	}		
	
	/**
	 * Gets the classes
	 * @return a set of owl classes
	 */
	public Collection<OWLClass> getClasses() {
		return owlClasses;
	}	
	
	/**
	 * Adds an owl property to this ontology
	 * @param owlProperty an OWL Property 
	 * @return <code>true</code> if the owlProperty is correctly added in the ontology
	 */
	public boolean addProperty(OWLProperty owlProperty) {
		
		if (!owlProperties.contains(owlProperty)) {
			owlProperty.setOntology(this);
			return owlProperties.add(owlProperty);
		}
		return false;
	}
	
	/**
	 * Removes an owl property from the ontology 
	 * @param owlProperty the owl property to be removed
	 * @return <code>true</code> if the owlProperty is correctly removed from the ontology
	 */
	public boolean removeProperty(OWLProperty owlProperty) {
		if (owlProperties.contains(owlProperty)) {
			owlProperty.setOntology(null);
			return owlProperties.remove(owlProperty);
		}
		return false;
	}
	
	/**
	 * Gets the list of owl properties
	 * @return the list of owl properties
	 */
	public Object[] listProperties() {
		return owlProperties.toArray();
	}	
	
	/**
	 * Gets the list of owl object properties
	 * @return the list of owl object properties
	 */
	public Object[] listObjectProperties() {
		ArrayList<OWLObjectProperty> objectProperties = new ArrayList<OWLObjectProperty>();
		for (OWLProperty property : owlProperties) {
			if (property instanceof OWLObjectProperty) {
				objectProperties.add((OWLObjectProperty)property);
			}
		}
		return objectProperties.toArray();	
	}	
	
	/**
	 * Gets the list of owl datatype properties
	 * @return the list of owl datatype properties
	 */
	public Object[] listDatatypeProperties() {
		ArrayList<OWLDatatypeProperty> datatypeProperties = new ArrayList<OWLDatatypeProperty>();
		for (OWLProperty property : owlProperties) {
			if (property instanceof OWLDatatypeProperty) {
				datatypeProperties.add((OWLDatatypeProperty)property);
			}
		}
		return datatypeProperties.toArray();
	}	
	
	/**
	 * Gets the OWL property contained in the current ontology
	 * @param id the property identifier
	 * @return the OWL Property identified by the incoming parameter
	 */
	public OWLProperty getOWLProperty(String id) {
		for (OWLProperty owlProperty : owlProperties) {
			if (owlProperty.getID().equals(id)) {
				return owlProperty;
			}
		}
		return null;
	}	
	
	/**
	 * Gets the prefix associated to a namespace 
	 * @param namespace the namespace to search
	 * @return the prefix corresponding to the namespace
	 */
	public String getNsPrefix(String namespace) {
		return this.model.getNsPrefix(namespace);
	}		
	
	/**
	 * Gets the namespace associated to a prefix 
	 * @param prefix the prefix
	 * @return the namespace corresponding to the prefix
	 */
	public String getNamespace(String prefix) {
		return this.model.getNamespace(prefix);
	}
	
	/**
	 * Adds an owl individual to this ontology
	 * @param owlIndividual an OWL Class 
	 * @return <code>true</code> if the owlIndividual is correctly added in the ontology
	 */
	public boolean addIndividual(OWLIndividual owlIndividual) {
		
		if (!owlIndividuals.contains(owlIndividual)) {
			owlIndividual.setOntology(this);
			return owlIndividuals.add(owlIndividual);
		}
		return false;
	}
	
	/**
	 * Removes an owl individual from the ontology 
	 * @param owlIndividual the owl individual to be removed
	 * @return <code>true</code> if the owlIndividual is correctly removed from the ontology
	 */
	public boolean removeIndividual(OWLIndividual owlIndividual) {
		if (owlIndividuals.contains(owlIndividual)) {
			owlIndividual.setOntology(null);
			return owlIndividuals.remove(owlIndividual);
		}
		return false;
	}
	
	/**
	 * Gets the list of owl individuals
	 * @return the list of owl individuals
	 */
	public Object[] listIndividuals() {
		return owlIndividuals.toArray();
	}
	
	/**
	 * Determines if the OWL individuals is contained in the current model
	 * @param owlSearchIndividual the OWL Individual to be compared
	 * @return <code>true</code> if the owl individuals is contained in the current model
	 */
	public boolean contains(OWLIndividual owlSearchIndividual) {
		for (OWLIndividual owlIndividual : owlIndividuals) {
			if (owlIndividual.equals(owlSearchIndividual)) {
				return true;
			}
		}
		return false;
	}	
	
	/**
	 * Gets the OWL individual contained in the current ontology
	 * @param id the individual identifier
	 * @return the OWL Individual identified by the incoming parameter
	 */
	public OWLIndividual getOWLIndividual(String id) {
		for (OWLIndividual owlIndividual : owlIndividuals) {
			if (owlIndividual.getID().equals(id)) {
				return owlIndividual;
			}
		}
		return null;
	}		
	
	/**
	 * Gets the classes
	 * @return a set of owl classes
	 */
	public Collection<OWLIndividual> getIndividuals() {
		return owlIndividuals;
	}		
	
	@Override
	public int compareTo(OWLOntology owlOntology) {
		return this.uri.compareTo(owlOntology.getID());
	}
	
	/**
	 * Compares the equality of ontologies
	 */
	@Override 
	public boolean equals( Object obj ) {
		if ( this == obj ) return true;
		if ( !(obj instanceof OWLOntology) ) return false;

		OWLOntology owlOntology = (OWLOntology) obj;
		return this.uri.equals(owlOntology.getID());
	}

	/**
	 * Returns a string representation of the owl ontology.
	 * @return a string representation of the owl ontology
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("OWL Ontology ID: ").append(this.getID());
		sb.append("\n").append("C(").append(this.owlClasses.size()).append(") ");
		sb.append("P(").append(this.owlProperties.size()).append(") ");
		sb.append("I(").append(this.owlIndividuals.size()).append(")");
		return sb.toString();
	}
}
