/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.wizard.model.data;

import java.util.Hashtable;
import java.util.Map;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.ontology.OntTools;
import com.hp.hpl.jena.rdf.model.ModelFactory;

/**
 * The Ontology Model. This model the ontologies in order to display it correctly
 * in the Wizard Ontology Selection page
 * 
 * @author Fran Ruiz, fran.ruiz@tecnalia.com
 *
 */
public class OntologyModel {

	/** The JENA OntModel */
	private OntModel model;
	
	/** The Namespaces */
	//private HashMap<String,String> namespaces = new HashMap<String,String>();
	
	/**
	 * Creates a single model
	 */
	public OntologyModel() {
		model = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM);
		// Sets the strict node because it is tried to get rdf:type owl:Class statements for all classes
		// @see http://tech.groups.yahoo.com/group/jena-dev/message/7340
		model.setStrictMode(false);
	}
	
	/**
	 * Gets the Ontology Model
	 * @return a JENA OntModel object
	 */
	public OntModel getOntModel() {
		return model;
	}
	
	/**
	 * Adds an ontology to the Ontology Model
	 * @param ontology the JENA OntModel to add
	 */
	public void addOntology(OntModel ontology) {
		
		model.add(ontology);
		
		// Getting the namespace prefixes
		Map<String,String> map = ontology.getNsPrefixMap();
		
		// Set the namespace prefixes
		for (String key : map.keySet()) {
			if (!key.equals("")) {
				String value = map.get(key);
				model.setNsPrefix(key, value);
			}
		}
	}
	
	/**
	 * Removes an ontology to the Ontology Model
	 * @param ontology the JENA OntModel to add
	 */
	public void removeOntology(OntModel ontology) {
		model.remove(ontology);
	}
	
	/**
	 * Gets the prefix for a given namespace
	 * @param namespace a String representing a namespace
	 */
	public String getPrefix(String namespace) {
		
		Hashtable<String,String> nsPrefixes = new Hashtable<String,String>();
		
		nsPrefixes.putAll(model.getNsPrefixMap());
		
		String prefix = null;
		for (String key : nsPrefixes.keySet()) {
			if (nsPrefixes.get(key).equals(namespace)) {
				prefix = key;
			}
		}
		
		return prefix;
	}

	/**
	 * Gets the list of root classes in the ontology
	 * @return a list of named classes in the ontology
	 */
	public Object[] getRootClasses() {
		try {
			return OntTools.namedHierarchyRoots(model).toArray();
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
		
	}
	
}
