package org.smool.sdk.ui.wizard.model.pages.util;

import java.io.InputStream;
import java.net.URI;

import org.smool.sdk.common.sax.ResourceUtil;
import org.smool.sdk.ui.wizard.model.data.SmartEnvironment;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.rdf.model.ModelFactory;


public class OntologyReader {
	
	private SmartEnvironment smartEnvironment;

	public OntologyReader(String fileName) {
		smartEnvironment = new SmartEnvironment();
		
		// Set the smart environment ontology model
		if (fileName != null) {
			try {
				System.out.println("reading " + fileName + "...");
				//logger.debug("Reading ontology " + ontology + "...");
				InputStream in = ResourceUtil.getInputStream(fileName);

				if (in != null) {
					OntModel owlModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM);
					owlModel.read(in, null);
					smartEnvironment.setOntologyModel(owlModel);
					smartEnvironment.setId(owlModel.getNsPrefixURI(""));
				} else {
					System.out.println("Cannot load ontology " + fileName);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	public OntologyReader(URI uri) {
		smartEnvironment = new SmartEnvironment();
		
		// Set the smart environment ontology model
		if (uri != null) {
			try {
				System.out.println("reading " + uri + "...");
				//logger.debug("Reading ontology " + ontology + "...");
				InputStream in = uri.toURL().openStream();

				if (in != null) {
					OntModel owlModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM);
					owlModel.read(in, null);
					smartEnvironment.setOntologyModel(owlModel);
					smartEnvironment.setId(owlModel.getNsPrefixURI(""));
				} else {
					System.out.println("Cannot load ontology " + uri);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}	
	
	public SmartEnvironment getSmartEnvironment() {
		return smartEnvironment;
	}
}
