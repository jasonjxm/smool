/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.wizard.model.pages.tools;

import org.apache.log4j.Logger;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.smool.sdk.ui.wizard.model.data.OntologyModel;

import com.hp.hpl.jena.ontology.OntClass;


/**
 * This class reads ontologies using Jena engine given and implements
 * its to show in a TreeViewer widget.
 * 
 * @author Fran Ruiz, fran.ruiz@tecnalia.com, ESI
 */
public class OntologyTreeContentProvider implements ITreeContentProvider {

	/** The ontology model */
	private OntologyModel ontologyModel = null;
	
	/** The logger */
	private static Logger logger = Logger.getLogger(OntologyTreeContentProvider.class);
	
	/**
	 * Creates a new ontology tree content provided
	 */
	public OntologyTreeContentProvider() {
		this.ontologyModel = null;
	}	
	
	/**
	 * Reads the URL where is located the ontology
	 * @param ontModel The Ontology Model
	 */
	public OntologyTreeContentProvider(OntologyModel ontModel) {
		if (ontModel != null) {
			this.ontologyModel = ontModel;
		}
	}
	
	/**
	 * Returns the subclasses of the given class.
	 * @param parentElement The ontology class acting as parent element
	 * @return An array of subclasses
	 */
	@Override
	public Object[] getChildren(Object parentElement) {
		Object children[] = null;
		
		try {
			OntClass node = (OntClass) parentElement;
			children = node.listSubClasses().toList().toArray();
		} catch (ClassCastException ex) {
			logger.error("Cannot cast the element into a ClassNode");
			logger.error(ex.getStackTrace().toString());
		}
		return children;
	}

	/**
	 * Returns the parent for the given ontology class, or null indicating that the class can't be computed. 
	 * In this case the tree-structured viewer can't expand a given node correctly if requested.
	 * @param element The ontology class
	 * @return the superclass for the given element 
	 */
	@Override
	public Object getParent(Object element) {
		OntClass parent = null;
		
		// Cast the element to ClassNode
		try {
			if (element instanceof OntClass) {
				OntClass node = (OntClass) element;
				parent = node.getSuperClass();
				if (parent != null) {
					if (!parent.isClass()) {
						parent = null;
					}
				} else {
					parent = null;
				}
				
			}
		} catch (ClassCastException ex) {
			logger.error("Cannot cast the element into JENA class");
			logger.error(ex.getStackTrace().toString());
			return null;
		}
		
		return parent;
	}

	/**
	 * Returns whether the given ontology class has subclasses.
	 * @param element The ontology class
	 * @return true if the given class has subclasses, and false if it has no subclasses
	 */
	@Override
	public boolean hasChildren(Object element) {
		// Cast the element to ClassNode
		try {
			OntClass node = (OntClass) element;
			
			return (node.listSubClasses().toList().size() > 0);
		} catch (ClassCastException ex) {
			logger.error("Cannot cast the element into JENA class");
			logger.error(ex.getStackTrace().toString());
			return false;
		}
	}

	/**
	 * Returns the root classes of the ontology.
	 * @param inputElement The input element
	 * @return the array of elements to display in the viewer
	 */
	@Override
	public Object[] getElements(Object inputElement) {
		Object[] elements = null;
		
		try {
			if (inputElement !=  null && inputElement instanceof OntClass) {
				OntClass node = (OntClass) inputElement;
				elements = node.listSubClasses().toList().toArray();
			} else {
				elements = ontologyModel.getRootClasses();
			}
		} catch (ClassCastException ex) {
			logger.error("Cannot cast the element into a ClassNode");
			ex.printStackTrace();
		} catch (Exception ex) {
			logger.error("Cannot get elements for " + inputElement.toString());
			ex.printStackTrace();
		}
		return elements;
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		// TODO Auto-generated method stub
	}

	public Object[] listRootClasses() {
		if (ontologyModel != null){
			return ontologyModel.getRootClasses();
		}
		else{
			return null;
		}
	}
}
