/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.wizard.model.pages.util;

import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

/**
 * This class is used to obtain information from the ontology classes
 * 
 * @author Fran Ruiz, fran.ruiz@tecnalia.com, ESI
 *
 */
public class OntologyUtil {

	/**
	 * Gets the OWL Doc 
	 * @param ontClass the ontology class
	 * @return the set of classes
	 */
	public static String getOWLDoc(OntClass ontClass) {
		StringBuffer sb = new StringBuffer();
		sb.append("Class: ");
		sb.append(ontClass.getLocalName());
		sb.append("\n\n");
		sb.append(ontClass.getURI());
		sb.append("\n\n");
		ExtendedIterator<OntClass> superClasses = ontClass.listSuperClasses();
		if (superClasses.toList().size() > 0) {
			for(OntClass parentClass : superClasses.toList()) {
				String currentSeparation = "\t";
				sb.append("� ");
				sb.append(parentClass.getLocalName());
				sb.append("\n");
				sb.append(currentSeparation);
				sb.append("� ");
				sb.append(ontClass.getLocalName());
				sb.append("\n");
				currentSeparation += currentSeparation;
				ExtendedIterator<OntClass> subClasses = ontClass.listSubClasses();
				int i = 0;
				for(OntClass subClass : subClasses.toList()) {
					i++;
					if (i < 3) { 
						sb.append(currentSeparation);
						sb.append("� ");
						sb.append(subClass.getLocalName());
						sb.append("\n");
					} else if (i == 3){
						sb.append(currentSeparation);
						sb.append("� ");
						int size = subClasses.toList().size();
						sb.append(size-4);
						sb.append(" more...");
						sb.append("\n");
						break;
					}
				}
			}
		} else {
			sb.append("� ");
			sb.append(ontClass.getLocalName());
			sb.append("\n");
			ExtendedIterator<OntClass> subClasses = ontClass.listSubClasses();
			for(OntClass subClass : subClasses.toList()) {
				sb.append("\t");
				sb.append("� ");
				sb.append(subClass.getLocalName());
				sb.append("\n");
			}			
		}
		sb.append("\n");

		ExtendedIterator<RDFNode> itComments = ontClass.listComments(null);
		if (itComments.hasNext()) {
			sb.append("Annotations:\n");
			while(itComments.hasNext()) {
				RDFNode node = (RDFNode) itComments.next();
				if (node.isLiteral()) {
					Literal literal = (Literal) node;
					sb.append("\t comment: \"");
					sb.append(literal.getString());
					sb.append("\" @");
					sb.append(literal.getLanguage());
					sb.append("\n");
				}
			}
		}

		ExtendedIterator<OntProperty> properties = ontClass.listDeclaredProperties();
		if (properties.hasNext()) {
			sb.append("\nUsage:\n");
			while(properties.hasNext()) {
				OntProperty property = (OntProperty) properties.next();
				if (property.isDatatypeProperty()) {
					sb.append("\t@DP ");
					sb.append(ontClass.getLocalName());
					sb.append(" ");
					sb.append(property.getLocalName());
					sb.append(" ");
					ExtendedIterator<? extends OntResource> ranges = property.listRange();
					for(Object range : ranges.toList()) {
						OntResource resource = (OntResource) range;
						if (resource.isDataRange()) {
							sb.append("xsd:");
							sb.append(resource.getLocalName());
						}
						sb.append(" ");
					}
					sb.append("\n");
					
				} else if (property.isObjectProperty()) {
					sb.append("\t@OP ");
					sb.append(ontClass.getLocalName());
					sb.append(" ");
					sb.append(property.getLocalName());
					sb.append(" ");
					ExtendedIterator<? extends OntResource> ranges = property.listRange();
					for(Object range : ranges.toList()) {
						if (range instanceof OntClass) {
							OntClass objectRange = (OntClass) range;
							sb.append(objectRange.getLocalName());
							sb.append(" ");
						} 
					}
					sb.append("\n");
				}
			}
		}

		
		
		
		return sb.toString();
	}

}
