/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.wizard.model.pages.tools;

import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.swt.graphics.Image;
import org.smool.sdk.ui.wizard.model.data.SmartEnvironment;


/**
 * This class represent the content for an Smart Environment Tree label provider
 * 
 * @author Fran Ruiz, fran.ruiz@tecnalia.com, ESI
 */
public class SmartEnvironmentTreeLabelProvider implements ILabelProvider {

	@Override
	public Image getImage(Object element) {
        // If the node represents a directory, return the directory image.
        // Otherwise, return the file image.
		SmartEnvironment node = (SmartEnvironment) element;
		
		return node.getImage();
	}

	@Override
	public String getText(Object element) {
        // If the node represents a smart environment
		// return the smart environment name
		SmartEnvironment node = (SmartEnvironment) element;
	   	   
	   	return node.getName();
	}

	@Override
	public void addListener(ILabelProviderListener listener) {
	}

	@Override
	public boolean isLabelProperty(Object element, String property) {
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener listener) {
	}

	@Override
	public void dispose() {
	}
}
