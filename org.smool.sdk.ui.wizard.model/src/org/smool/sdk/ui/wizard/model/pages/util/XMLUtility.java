package org.smool.sdk.ui.wizard.model.pages.util;



import java.io.PrintStream;



import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;


import org.w3c.dom.Document;


/**
 * This interface contain the main functionalities for XML parsing and writing
 * @author carmen 08/03/2012
 * It is based in the STAX way provided in 
 * http://www.vogella.de/articles/JavaXML/article.html#javastax
 * 
 */
public class XMLUtility{
	
	//Atributes
	private String cacheFile;
	
	public void setFile(String cacheFile) {
		this.cacheFile = cacheFile;
	}	
	
	// Main Functions
	public  void readXMLfile(String filepath){};
	
	
	// Writing the data from the wizard within the file
	public  void writeXMLfile() 
		throws Exception{
		
		// Building a new document from scratch
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document testDoc = builder.newDocument();
		
		// Creating the elements

		
		
		
		 
		 //Saving the document 
		DOMSource source = new DOMSource(testDoc);
		PrintStream ps = new PrintStream(cacheFile);
		StreamResult result = new StreamResult(ps);
		
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = (Transformer) transformerFactory.newTransformer();
		transformer.transform(source, result);
		
		// Create a XMLOutputFactory
//		XMLOutputFactory factory = XMLOutputFactory.newInstance();
			// Create XMLEventWriter
//			XMLEventWriter eventWriter = factory
//					.createXMLEventWriter(new FileOutputStream(cacheFile));
//			// Create a EventFactory
//			XMLEventFactory eventFactory = XMLEventFactory.newInstance();
//			XMLEvent end = eventFactory.createDTD("\n");
//			// Create and write Start Tag
//			StartDocument startDocument = eventFactory.createStartDocument();
//			eventWriter.add(startDocument);
//			// Create project open tag
//			StartElement configStartElement = eventFactory.createStartElement("",
//					"", "project");
//			eventWriter.add(configStartElement);
//			eventWriter.add(end);
//			WizardData data= WizardData.getInstance();
			// Write the different nodes
//			createNode(eventWriter, "project-name", data.getProjectName());
//			createNode(eventWriter, "package-name", data.getPackageName());
//			createNode(eventWriter, "execution-context", Integer.toString(data.getContext()));
//			
//			//Write the Child nodes
//			createNode(eventWriter, "smart-env", "0");

//			eventWriter.add(eventFactory.createEndElement("", "", "project"));
//			eventWriter.add(end);
//			eventWriter.add(eventFactory.createEndDocument());
//			eventWriter.close();
	}
//	private Document createElement(Document doc) {
//		WizardData data = WizardData.getInstance();
//		// Creating the first element
//		Element el =  doc.createElement("project");
//	
//		
//		
////		el.setTextContent("xxx");
//		doc.appendChild(el);
//		return doc;
//	}

//		private void createNode(XMLEventWriter eventWriter, String name,
//				String value) throws XMLStreamException {
//
//			XMLEventFactory eventFactory = XMLEventFactory.newInstance();
//			XMLEvent end = eventFactory.createDTD("\n");
//			XMLEvent tab = eventFactory.createDTD("\t");
//			// Create Start node
//			StartElement sElement = eventFactory.createStartElement("", "", name);
//			eventWriter.add(tab);
//			eventWriter.add(sElement);
//			// Create Content
//			Characters characters = eventFactory.createCharacters(value);
//			eventWriter.add(characters);
//			// Create End node
//			EndElement eElement = eventFactory.createEndElement("", "", name);
//			eventWriter.add(eElement);
//			eventWriter.add(end);
//
//		}
//		
//		private void createNodeAndChilds(XMLEventWriter eventWriter, String name,
//				String child) throws XMLStreamException {
//			XMLEventFactory eventFactory = XMLEventFactory.newInstance();
//			XMLEvent end = eventFactory.createDTD("\n");
//			XMLEvent tab = eventFactory.createDTD("\t");
//			// Create Start node
//			StartElement sElement = eventFactory.createStartElement("", "", name);
//			eventWriter.add(tab);
//			eventWriter.add(sElement);
//			
//			// Create Childs
//			Characters characters = eventFactory.createCharacters(value);
//			eventWriter.add(characters);
//			
//			//Ending the father
//			
//		
//		}
		
		

//	}

		
		
		
		
	
	
	//GETTERS & SETTERS
	
}