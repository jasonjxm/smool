/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.wizard.model.pages.frames;

import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jdt.core.IJavaModel;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.ui.JavaElementLabelProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.ElementListSelectionDialog;
import org.smool.sdk.common.nls.SmoolMessages;


/**
 * A class that implements the frame where a project location
 * is set.
 *  
 * @author Fran Ruiz, fran.ruiz@tecnalia.com, ESI
 *
 */
public class ProjectSelectionDialog {

	/** Constants definition */
	private final Shell shell;

	/** The list of the SMOOL projects */
	private IJavaProject[] smoolProjects;
	
	public ProjectSelectionDialog(Shell parent) {
		shell = parent;
	}
	
	/**
	 * This method is used to obtain a SMOOL Project given a project name 
	 * @param projectName the SMOOL project name
	 * @return the IJavaProject representing the smool project
	 */
	public IJavaProject selectSMOOLProject(String projectName) {
		ILabelProvider labelProvider = new JavaElementLabelProvider(JavaElementLabelProvider.SHOW_DEFAULT);
		
		// Creates a new selection dialog
		ElementListSelectionDialog dialog = new ElementListSelectionDialog(shell, labelProvider);
		
		dialog.setTitle(SmoolMessages.ProjectSelectionDialog_windowTitle);
		dialog.setMessage(SmoolMessages.ProjectSelectionDialog_windowDescription);
		
		// Gets the workspace root
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		
		// Gets the IJavaModel
		IJavaModel javaModel = JavaCore.create(root);
		
		dialog.setElements(listSMOOLProjects(javaModel));
		
		IJavaProject javaProject = null;
		if (projectName != null && projectName.length() > 0) {
			javaProject = javaModel.getJavaProject(projectName);
		}
		
		 // if we found it, we set the initial selection in the dialog to this one.
		if (javaProject != null) {
			dialog.setInitialSelections(new Object[] { javaProject });
		}
		
		if (dialog.open() == Window.OK) {
			return (IJavaProject) dialog.getFirstResult();
		}
		
		return null;
	}
	
	/** 
	 * Returns the list of SMOOL projects.  
	 * Because this list can be time consuming, this class caches the list of 
	 * project.  It is recommended to call this method instead of  
	 * {@link ProjectHelper#listSMOOLProjects()}. 
 	 * @param javaModel the java model. Can be null.  
     */
	public IJavaProject[] listSMOOLProjects(IJavaModel javaModel) { 
		if (smoolProjects == null) { 
			if (javaModel == null) { 
				smoolProjects = ProjectHelper.listSMOOLProjects();

			} else { 
				smoolProjects = ProjectHelper.listSMOOLProjects(javaModel);
			} 
		}
		
		return smoolProjects;
	}	
}
