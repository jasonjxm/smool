/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Alejandro Villamarin (Tecnalia Researh and Innovation - Software Systems Engineering) - bug fixing
 *******************************************************************************/ 

package org.smool.sdk.ui.wizard.model.pages;

import java.io.InputStream;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTreeViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.TreeItem;
import org.smool.sdk.common.nls.SmoolMessages;
import org.smool.sdk.common.properties.PropertyLoader;
import org.smool.sdk.ui.wizard.model.ModelGeneratorWizard;
import org.smool.sdk.ui.wizard.model.data.OntologyModel;
import org.smool.sdk.ui.wizard.model.data.WizardData;
import org.smool.sdk.ui.wizard.model.pages.tools.OntologyTreeContentProvider;
import org.smool.sdk.ui.wizard.model.pages.tools.OntologyTreeLabelProvider;
import org.smool.sdk.ui.wizard.model.pages.util.OntologyUtil;

import com.hp.hpl.jena.ontology.OntClass;


/**
 * This class implements the UI page of the ADK Wizard where we can determine
 * the application domain of the Knowledge Processor
 * 
 * @author Fran Ruiz (fran.ruiz@tecnalia.com), Alejandro Villamarin (alejandro.villamarin@tecnalia.com) Tecnalia
 * 
 */
public class OntologyConceptSelectionPage extends WizardPage {

	/** The Logger */
	private static Logger logger = Logger
			.getLogger(OntologyConceptSelectionPage.class);

	/** The Producer Ontology Concept Selection tree Area */
	private CheckboxTreeViewer ontologyTreeViewer;

	private Label ontologyLabel;

	/** The Consumer Ontology Concept Selection tree Area */
	// private OntologySelectionArea consumerOntologyTree;

	/** The context */
	private String context;
	
	public static String PRODUCER = "producer";
	public static String CONSUMER = "consumer";
	
	private static String TOOLBAR_SELECTALL = "images.toolbar.selectall";		
	private static String TOOLBAR_DESELECTALL = "images.toolbar.deselectall";		
	private static String TOOLBAR_EXPAND = "images.toolbar.expand";		
	private static String TOOLBAR_COLLAPSE = "images.toolbar.collapse";		

	/** Listeners corresponding to the tree selection */
	private ICheckStateListener treeModifyListener = new ICheckStateListener() {
		public void checkStateChanged(CheckStateChangedEvent event) {
			if (event.getChecked()) {
				setParentsChecked(event.getElement());
				OntClass element = (OntClass) event.getElement();
				if (context.equals(PRODUCER)) {
					WizardData.getInstance().addProducerOntologyConcept(element);
				} else if (context.equals(CONSUMER)) {
					WizardData.getInstance().addConsumerOntologyConcept(element);
				}
			} else {
				setParentsUnchecked(event.getElement());
				OntClass element = (OntClass) event.getElement();
				if (context.equals(PRODUCER)) {
					WizardData.getInstance().removeProducerOntologyConcept(element);
				} else if (context.equals(CONSUMER)) {
					WizardData.getInstance().removeConsumerOntologyConcept(element);
				}
			}

			// check the validity of the page content
			boolean valid = validatePage();
			setPageComplete(valid);
		}
	};

	/**
	 * Constructor for WizardOntologyConceptSelectionPage.
	 */
	public OntologyConceptSelectionPage(String context) {
		super(SmoolMessages.OntologyConceptSelectionPage_windowTitle);

		this.context = context;

		if (context.equals(OntologyConceptSelectionPage.CONSUMER)) {
			setTitle(SmoolMessages.OntologyConceptSelectionPage_consumer_pageTitle);
			setDescription(SmoolMessages.OntologyConceptSelectionPage_consumer_pageDescription);
		} else {
			setTitle(SmoolMessages.OntologyConceptSelectionPage_producer_pageTitle);
			setDescription(SmoolMessages.OntologyConceptSelectionPage_producer_pageDescription);
		}
	}

	/**
	 * Creates the wizard page components
	 * @param parent the composite
	 */
	@Override
	public void createControl(Composite parent) {

		// Creates the composite for the ontology concept selection
		Composite container = new Composite(parent, SWT.NULL);

		// Initializes the computation of horizontal and vertical
		// dialog units based on the size of current font.
		initializeDialogUnits(parent);

		// // Help system
		// PlatformUI.getWorkbench().getHelpSystem().setHelp(container,
		// ProjectPluginActivator.PLUGIN_ID +
		// ".help.ontologyconceptselection");

		GridLayout ontologyLayout = new GridLayout();

		ontologyLayout.numColumns = 2;
		ontologyLayout.makeColumnsEqualWidth = true;

		container.setLayout(ontologyLayout);
		container.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		GridData labelData = new GridData();
		labelData.horizontalSpan = 2;

		// The ontology label
		ontologyLabel = new Label(container, SWT.NONE);
		if (context.equals(OntologyConceptSelectionPage.CONSUMER)) {
			ontologyLabel.setText(SmoolMessages.OntologySelectionArea_consumerLabel);
		} else {
			ontologyLabel.setText(SmoolMessages.OntologySelectionArea_producerLabel);
		}

		// ontologyLabel.setText(SofiaADKMessages.OntologyConceptSelectionPage_ontologyLabel);
		ontologyLabel.setFont(container.getFont());
		ontologyLabel.setLayoutData(labelData);

		createToolbar(container);		
		
		// The associated text that describes the selected smart environment
		final Text descriptionText = new Text(container, SWT.BORDER | SWT.WRAP
				| SWT.MULTI | SWT.READ_ONLY | SWT.V_SCROLL | SWT.H_SCROLL);
		GridData gridData = new GridData();
		gridData.horizontalAlignment = SWT.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.verticalAlignment = SWT.FILL;
		gridData.grabExcessVerticalSpace = true;
		gridData.verticalSpan = 2;
		descriptionText.setLayoutData(gridData);

		descriptionText.setText("");
		descriptionText.setEditable(false);
		descriptionText.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));

		// Create the tree viewer to display the file tree
		GridData ontologyTreeData = new GridData(GridData.FILL_BOTH);

		ontologyTreeViewer = new CheckboxTreeViewer(container);
		ontologyTreeViewer.getTree().setLayoutData(ontologyTreeData);
		ontologyTreeViewer.setContentProvider(new OntologyTreeContentProvider());
		ontologyTreeViewer.setLabelProvider(new OntologyTreeLabelProvider());

		ontologyTreeViewer.addCheckStateListener(treeModifyListener);
		
		ontologyTreeViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			// The selected ontology
			OntClass ontClass = null;

			public void selectionChanged(SelectionChangedEvent event) {
				IStructuredSelection selection = (IStructuredSelection) event
						.getSelection();

				Object selectedElement = selection.getFirstElement();
				if (selectedElement == null) {
					ontClass = null;
					descriptionText.setText("");
				} else {
					ontClass = (OntClass) selectedElement;
					String owlDoc = OntologyUtil.getOWLDoc(ontClass);
					descriptionText.setText(owlDoc);
				}
			}
		});
		
		updatePage();

		setPageComplete(validatePage());

		// Show description on opening
		setErrorMessage(null);

		setMessage(null);
		setControl(container);
	}
	
	public void updatePage() {
		updateOntologyReferences();
		ArrayList<OntClass> selection = new ArrayList<OntClass>();
		if (context.equals(PRODUCER)) {
			selection.addAll(WizardData.getInstance().getProducerOntologyConcepts());
		}
		else if (context.equals(CONSUMER)) {
			selection.addAll(WizardData.getInstance().getConsumerOntologyConcepts());
		}
		
		/*for (TreeItem ti : ontologyTreeViewer.getTree().getItems()) {
			ti.setChecked(false);
		}*/
		
		for (OntClass o : selection) {
			ontologyTreeViewer.setChecked(o, true);
			OntClass temp = o;
			while (temp.getSuperClass() != null) {
				temp = temp.getSuperClass();
				ontologyTreeViewer.setGrayChecked(temp, true);
			}
		}
		
		//ontologyTreeViewer.refresh();
	}

	/**
	 * Create the selection buttons in the ontology concept selection tree.
	 * @param parent the composite
	 */
	private void createToolbar(Composite parent) {
		
		ImageDescriptor selectAllImage = getImage(TOOLBAR_SELECTALL);
		ImageDescriptor deselectAllImage = getImage(TOOLBAR_DESELECTALL);
		ImageDescriptor expandImage = getImage(TOOLBAR_EXPAND);
		ImageDescriptor collapseImage = getImage(TOOLBAR_COLLAPSE);
		
	    // Actions
	    Action actionSelectAll = new Action(SmoolMessages.OntologySelectionArea_selectAll,
	        selectAllImage) {
	      public void run() {
			  try {
				  TreeItem[] items = ontologyTreeViewer.getTree().getItems();
				  for(TreeItem item : items) {
					  item.setChecked(true);
					  addElementToModel(item.getData());
					  checkItems(item);
				  }
			  } catch (Exception ex) {
				  ex.printStackTrace();
			  }
			  
			  // check the validity of the page content
			  boolean valid = validatePage();
			  setPageComplete(valid);			  
	      }
	    };
	    
	    Action actionDeselectAll = new Action(SmoolMessages.OntologySelectionArea_deselectAll,
	    	deselectAllImage) {
		  public void run() {
			  try {
				  TreeItem[] items = ontologyTreeViewer.getTree().getItems();
				  for(TreeItem item : items) {
					  item.setChecked(false);
					  removeElementFromModel(item.getData());
					  uncheckItems(item);
				  }
			  } catch (Exception ex) {
				  ex.printStackTrace();
			  }
			  
			  // check the validity of the page content
			  boolean valid = validatePage();
			  setPageComplete(valid);			  
			  
		  }
		};
		    
		Action actionExpand = new Action(SmoolMessages.OntologySelectionArea_expand,
			expandImage) {
		  public void run() {
			  ontologyTreeViewer.expandAll();
		  }
		};
			    
		Action actionCollapse = new Action(SmoolMessages.OntologySelectionArea_collapse,
			collapseImage) {
		  public void run() {
			  ontologyTreeViewer.collapseAll();
		  }
		};
		
		ToolBar ontologyTreeBar = new ToolBar(parent, SWT.RIGHT | SWT.FLAT);

	    ToolBarManager toolbarManager = new ToolBarManager(ontologyTreeBar);
 		
 		toolbarManager.add(actionSelectAll);
		toolbarManager.add(actionDeselectAll);
		toolbarManager.add(new Separator());
		toolbarManager.add(actionExpand);
	    toolbarManager.add(actionCollapse);

	    toolbarManager.update(true);
	    
	    ontologyTreeBar.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false));
	}
	
	
	private void checkItems(TreeItem parent) {
		TreeItem[] items = parent.getItems();
		for (TreeItem item : items) {
			item.setChecked(true);
			addElementToModel(item.getData());
			checkItems(item);
		}
	}

	private void addElementToModel(Object data) {
		try {
			OntClass element = (OntClass) data;
			if (context.equals(PRODUCER)) {
				((ModelGeneratorWizard) getWizard()).getModel()
						.addProducerOntologyConcept(element);
			} else if (context.equals(CONSUMER)) {
				((ModelGeneratorWizard) getWizard()).getModel()
						.addConsumerOntologyConcept(element);
			}
		} catch (ClassCastException ex) {
			ex.printStackTrace();
		}
	}

	private void uncheckItems(TreeItem parent) {
		TreeItem[] items = parent.getItems();
		for (TreeItem item : items) {
			item.setChecked(false);
			removeElementFromModel(item.getData());
			uncheckItems(item);
		}
	}	
	
	private void removeElementFromModel(Object data) {
		try {
			OntClass element = (OntClass) data;
			if (context.equals(PRODUCER)) {
				((ModelGeneratorWizard) getWizard()).getModel()
						.removeProducerOntologyConcept(element);
			} else if (context.equals(CONSUMER)) {
				((ModelGeneratorWizard) getWizard()).getModel()
						.removeConsumerOntologyConcept(element);
			}
		} catch (ClassCastException ex) {
			ex.printStackTrace();
		}
	}
	
	/**
	 * Updates the ontology references and loads it into the ADK Model to be
	 * read by the treeviewer
	 */
	protected void updateOntologyReferences() {
		updateOntologyReferences(WizardData.getInstance().getOntologyModel());
	}

	private ImageDescriptor getImage(final String imageReference) {
		ImageDescriptor imageDesc = new ImageDescriptor() {
			@Override
			public ImageData getImageData() {
				ImageData imgData = null;
				try {
					// Obtain the URL
		 			PropertyLoader props = PropertyLoader.getInstance();
					String img = props.getProperty(imageReference);
					// get the bundle and its location
				    InputStream is = this.getClass().getResourceAsStream(img);
					imgData = new ImageData(is);
				} catch (Exception e) {
					logger.debug("An exception arised loading the image: " + e.toString());
					e.printStackTrace();
				}
				return imgData;
			}
		};
		
		return imageDesc;
	}
	
	/**
	 * Updates the ontology references in the ontology page
	 */
	private void updateOntologyReferences(OntologyModel ontModel) {
		// Updates
		//logger.debug("Updates ontology references in the tree");

		try {
			OntologyTreeContentProvider content = new OntologyTreeContentProvider(
					ontModel);
			ontologyTreeViewer
					.setContentProvider(new OntologyTreeContentProvider(
							ontModel));
			ontologyTreeViewer.setLabelProvider(new OntologyTreeLabelProvider(
					ontModel));
			ontologyTreeViewer.setInput(content.listRootClasses());
			ontologyTreeViewer.expandAll();
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("There was an error creating the TreeContentProvider");
		}
	}

	/**
	 * Returns whether this page's controls currently all contain valid values.
	 * 
	 * @return <code>true</code> if all controls are valid, and
	 *         <code>false</code> if at least one is invalid
	 */
	protected boolean validatePage() {

		// sets the error messages to null
		setErrorMessage(null);

		// validates the project reference
		if (!validateOntologySelection()) {
			return false;
		}

		// if the validation is correct, then return the default message
		setMessage(SmoolMessages.OntologyConceptSelectionPage_validationDefaultMessage);

		return true;
	}

	/**
	 * Validates the selection of a platform
	 */
	private boolean validateOntologySelection() {

		if (this.context.equals(PRODUCER)) {
			if (!((ModelGeneratorWizard) getWizard()).getModel()
					.hasSelectedProducerConcepts()) {
				setErrorMessage(null);
				setMessage(SmoolMessages.OntologyConceptSelectionPage_validationProducerConceptsNotSelected);
				return false;
			}
		} else if (this.context.equals(CONSUMER)) {
			if (!((ModelGeneratorWizard) getWizard()).getModel()
					.hasSelectedConsumerConcepts()) {
				setErrorMessage(null);
				setMessage(SmoolMessages.OntologyConceptSelectionPage_validationConsumerConceptsNotSelected);
				return false;
			}
		}

		return true;
	}

	/**
	 * Checks the parent smart environments (if exists)
	 * 
	 * @param element
	 *            the selected element
	 */
	public ArrayList<OntClass> setParentsChecked(Object element) {
		ArrayList<OntClass> ontClasses = new ArrayList<OntClass>();
		OntClass ontClass = (OntClass) element;
		ontClasses.add(ontClass);
		ontologyTreeViewer.setSelection(new StructuredSelection(element), true);

		while (ontClass.getSuperClass() != null) {
			ontClass = ontClass.getSuperClass();
			ontClasses.add(ontClass);
			ontologyTreeViewer.setGrayChecked(ontClass, true);
		}

		return ontClasses;
	}

	/**
	 * Sets the parent smart environments (if exists) to unchecked
	 * 
	 * @param element
	 *            the selected element
	 */
	public ArrayList<OntClass> setParentsUnchecked(Object element) {
		ArrayList<OntClass> ontClasses = new ArrayList<OntClass>();

		OntClass ontClass = (OntClass) element;
		ontClasses.add(ontClass);
		ontologyTreeViewer.setSelection(new StructuredSelection(element), true);
		boolean hasCheckedChildren = false;

		while (ontClass.getSuperClass() != null && !hasCheckedChildren) {

			ontClass = ontClass.getSuperClass();

			for (OntClass parent : ontClass.listSubClasses().toList()) {
				if (ontologyTreeViewer.getChecked(parent)) {
					hasCheckedChildren = true;
				}
			}

			if (!hasCheckedChildren) {
				ontClasses.add(ontClass);
				ontologyTreeViewer.setGrayChecked(ontClass, false);
			}
		}

		return ontClasses;
	}

	/**
	 * Implements the getNextPage method to obtain the following page in the
	 * wizard and update the ontology references
	 */
	@Override
	public IWizardPage getNextPage() {

		if (this.context.equals(PRODUCER)) {
			if (((ModelGeneratorWizard) getWizard()).getModel().getContext() == 0) {
				OntologyConceptSelectionPage page = ((ModelGeneratorWizard) getWizard())
						.getOntologyConsumerWizardPage();
				return page;
			} else {
				return null;
			}
		} else if (this.context.equals(CONSUMER)) {
			return super.getNextPage();
		} else {
			return null;
		}
	}
}
