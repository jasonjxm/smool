/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Alejandro Villamarin (Tecnalia Researh and Innovation - Software Systems Engineering) - bug fixing
 *******************************************************************************/ 

package org.smool.sdk.ui.wizard.model;

import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.smool.sdk.common.nls.SmoolMessages;
import org.smool.sdk.common.properties.PropertyLoader;
import org.smool.sdk.core.generator.kpi.model.ModelGenerator;
import org.smool.sdk.ui.wizard.model.data.WizardData;
import org.smool.sdk.ui.wizard.model.pages.NewModelLayerPage;
import org.smool.sdk.ui.wizard.model.pages.OntologyConceptSelectionPage;
import org.smool.sdk.ui.wizard.model.pages.frames.ProjectHelper;



/**
 * The ADKWizard class determines the Wizard that will be thrown
 * in the activation of this plugin.
 *
 * An implementation of a wizard. The ADKWizard must contain the following 
 * methods to configure the wizard: 
 * 
 * <ul><li>addPage</li>
 * <li>setHelpAvailable</li>
 * <li>setDefaultPageImageDescriptor</li>
 * <li>setDialogSettings</li>
 * <li>setNeedsProgressMonitor</li>
 * <li>setTitleBarColor</li>
 * <li>setWindowTitle</li></ul>
 *  
 * This class may override these methods if required:
 *  
 * <ul><li>createPageControls</li>
 * <li>performCancel</li>
 * <li>addPages</li>
 * <li>performFinish</li>
 * <li>dispose</li></ul>
 * 
 * @author Fran Ruiz (fran.ruiz@tecnalia.com), Alejandro Villamarin (alejandro.villamarin@tecnalia.com) Tecnalia
 *
 */
public class ModelGeneratorWizard extends Wizard implements INewWizard {

	private static final String WIZARD_IMAGE = "images.smool";	
	
	/** The new project page where we select the name and ontologies to use */
	private NewModelLayerPage newModelLayerPage;

	/** The ontology concepts selection page, based on the concepts already selected in the first page */
	private OntologyConceptSelectionPage ontProducerSelectionPage;
	
	/** The ontology concepts selection page, based on the concepts already selected in the first page */
	private OntologyConceptSelectionPage ontConsumerSelectionPage;
	
	/** the workbench instance */
	protected IWorkbench workbench;	

	/** workbench selection when the wizard was started */
	protected IStructuredSelection selection;	

	/** The logger */
	private static Logger logger = Logger.getLogger(ModelGeneratorWizard.class);
	
	/** The model that stores all data selected during the wizard **/
	protected WizardData model = null;
	
	/**
	 * Constructor
	 */
	public ModelGeneratorWizard() {
		super();
		
		// Creates a new ADK Model
		model = WizardData.getInstance();
		
		// sets the window page title
		setWindowTitle(SmoolMessages.Wizard_windowTitle);
		
		// allows the usage of the progress monitor
		setNeedsProgressMonitor(true);
		
	}
	
	/**
	 * Adds any wizard page to this wizard.
	 * This method is called just before the wizard becomes visible, 
	 * to give the wizard the opportunity to add any lazily created pages.
	 */
	public void addPages() {
		// Get the image descriptor to show it in the wizard
		ImageDescriptor imageDesc = new ImageDescriptor() {
			@Override
			public ImageData getImageData() {
				ImageData imgData = null;
				try {
					// Obtain the URL
		 			PropertyLoader props = PropertyLoader.getInstance();
					String smoolImg = props.getProperty(WIZARD_IMAGE);
					// get the bundle and its location
				    InputStream is = this.getClass().getResourceAsStream(smoolImg);
					imgData = new ImageData(is);
				} catch (Exception e) {
					logger.debug("An exception arised loading the image: " + e.toString());
					e.printStackTrace();
				}
				return imgData;
			}
		};
		
		// Creates a page where the location and selection of domain are selected
		if (newModelLayerPage == null) {
			newModelLayerPage = new NewModelLayerPage(SmoolMessages.NewModelLayerPage_windowTitle);
		}
		newModelLayerPage.setImageDescriptor(imageDesc);
		addPage(newModelLayerPage);

		// Creates a page where the ontology concepts are selected
		ontProducerSelectionPage = new OntologyConceptSelectionPage(OntologyConceptSelectionPage.PRODUCER);
		ontProducerSelectionPage.setImageDescriptor(imageDesc);
		addPage(ontProducerSelectionPage);

		// Creates a page where the ontology concepts are selected
		ontConsumerSelectionPage = new OntologyConceptSelectionPage(OntologyConceptSelectionPage.CONSUMER);
		ontConsumerSelectionPage.setImageDescriptor(imageDesc);
		addPage(ontConsumerSelectionPage);
	}
	
	/**
	 * Initializes this creation wizard using the passed workbench 
	 * and object selection.
	 * This method is called after the no argument constructor and before 
	 * other methods are called.
	 * 
	 *  @param workbench the current workbench
	 *  @param selection the current object selection
	 */
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.workbench = workbench;
		this.selection = selection;
		// Change made in order to cope with the new Singleton nature
		model = WizardData.getInstance();
		
		if (newModelLayerPage == null) {
			newModelLayerPage = new NewModelLayerPage(SmoolMessages.NewModelLayerPage_windowTitle);
		}
		newModelLayerPage.setInitialSelection(selection);
	}
	
	/**
	 * Subclasses must implement this Wizard method to perform any 
	 * special finish processing for their wizard.
	 * 
	 * @return true to indicate the finish request was accepted, 
	 * and false to indicate that the finish request was refused
	 *
	 */
	@Override
	public boolean performFinish() {
		
		final String packageName = newModelLayerPage.getPackageName();
		final IProject project = newModelLayerPage.getProjectHandle();

		// Starts the progress monitor
		IRunnableWithProgress op = new IRunnableWithProgress() {
			public void run(IProgressMonitor monitor) throws 
				InvocationTargetException, InterruptedException {
				
                try {
                	doFinish(packageName, project, monitor);
                } catch (CoreException e) {
                    throw new InvocationTargetException(e);
                } finally {
                    monitor.done();
                }				
			}
		};
		
        try {
            this.getContainer().run(true, false, op);
        } catch (InterruptedException e) {
            return false;
        } catch (InvocationTargetException e) {
            Throwable realException = e.getTargetException();
            MessageDialog.openError(getShell(), "Error", realException.getMessage());
            return false;
        } catch (Exception e) {
			logger.error("Cannot perform finish due to a Exception:" + e.getMessage());
			e.printStackTrace();
		}
        
        WizardData.getInstance().storeData();
        return true;		
	}

	/**
     * The worker method. It will find the container, create the
     * file if missing or just replace its contents, and open
     * the editor on the newly created file.
     */
    private void doFinish(String packageName, IProject project, IProgressMonitor monitor)
    		throws CoreException {
    	try {	
    		
    		if (existsModelLayer(ProjectHelper.getSMOOLProject(model.getProject()))) {
    			removeModelLayer(ProjectHelper.getSMOOLProject(model.getProject()));
    		}
    			
    		// Getting the information of the project
    		ModelGenerator generator = new ModelGenerator();
    		//Try to generate classes	
    		generator.generate(ProjectHelper.getSMOOLProject(model.getProject()),
    													model.getPackageName(), 
    													model.getOntologyModel().getOntModel(), 
    													model.getProducerOntologyConcepts(),
    													model.getConsumerOntologyConcepts());
    		
    		
    		
		} catch (Exception ex) {
			ex.printStackTrace();
		}
    }
 
	/**
	 * Gets the ontology page
	 * @return The Ontology selection WizardPage
	 */
    public OntologyConceptSelectionPage getOntologyProducerWizardPage() {
    	return ontProducerSelectionPage;
	}

	/**
	 * Gets the ontology page
	 * @return The Ontology selection WizardPage
	 */
    public OntologyConceptSelectionPage getOntologyConsumerWizardPage() {
    	return ontConsumerSelectionPage;
	}

	/**
	 * Gets the model containing the selections made during the wizard
	 * @return The ADKModel
	 */
    public WizardData getModel() {
		return model;
	}
    
    @Override
    public boolean canFinish() {
    	boolean newModelLayerPageComplete = newModelLayerPage.isPageComplete();
    	
    	boolean ontologySelectionComplete = false;
    	if (newModelLayerPage.getContext() == 0) { // PROSUMER
    		ontologySelectionComplete = 
    			ontConsumerSelectionPage.isPageComplete() && ontProducerSelectionPage.isPageComplete();
    	} else if (newModelLayerPage.getContext() == 1) { // PRODUCER
    		ontologySelectionComplete = 
    			ontProducerSelectionPage.isPageComplete();
    	} else if (newModelLayerPage.getContext() == 2) { // CONSUMER
    		ontologySelectionComplete = 
    			ontConsumerSelectionPage.isPageComplete();
    	}
    	
    	return newModelLayerPageComplete & ontologySelectionComplete;
    }

	/**
	 * Removes the path structure associated to the model layer
	 * @param smoolProject a IJavaProject
	 */
	private void removeModelLayer(IJavaProject smoolProject) {
        try {
			IProject project = (IProject) smoolProject.getResource();
			
			IFolder srcFolder = project.getFolder("src");

			IPackageFragmentRoot root = smoolProject.getPackageFragmentRoot(srcFolder);
			
			IJavaElement[] elements = root.getChildren();
			
			ArrayList<String> modelPackageNames = new ArrayList<String>();
			ArrayList<String> basePackageNames = new ArrayList<String>();
			
			for (IJavaElement javaElement : elements) {
				if (javaElement instanceof IPackageFragment) {
					IPackageFragment packageFragment = (IPackageFragment) javaElement;
					String packageFragmentName = packageFragment.getElementName();
					if (packageFragmentName.contains(".model")) {
						if (!modelPackageNames.contains(packageFragmentName)) {
							modelPackageNames.add(packageFragmentName);
						}
						String basePackageName = packageFragmentName.substring(0, packageFragmentName.indexOf(".model"));
						if (!basePackageNames.contains(basePackageName)) {
							basePackageNames.add(basePackageName);
						}
					}
				}
			}
			
			for(String modelPackageName : modelPackageNames) {
				IPackageFragment modelPackageFragment = root.getPackageFragment(modelPackageName);
				modelPackageFragment.getResource().delete(true, new NullProgressMonitor());
			}
			
			for(String basePackageName : basePackageNames) {
				IPackageFragment basePackageFragment = root.getPackageFragment(basePackageName);
				IResource resource = basePackageFragment.getResource();
				IPath folder = resource.getFullPath();
				
				if (folder.isEmpty()) {
					resource.delete(true, new NullProgressMonitor());
				}
			}			
			
		} catch (JavaModelException e) {
			logger.info("There are no package in the source folder");
		} catch (CoreException e) {
			logger.info("There are no package in the source folder");
		}
		
	}

	/**
	 * Determines if there is an existing model layer
	 * @param smoolProject an IJavaProject
	 * @return <code>true</code> if exists a model layer
	 */
	private boolean existsModelLayer(IJavaProject smoolProject) {
        try {
			IProject project = (IProject) smoolProject.getResource();
			
			IFolder srcFolder = project.getFolder("src");

			IPackageFragmentRoot root = smoolProject.getPackageFragmentRoot(srcFolder);
			
			IJavaElement[] elements = root.getChildren();
			
			for (IJavaElement javaElement : elements) {
				if (javaElement instanceof IPackageFragment) {
					IPackageFragment packageFragment = (IPackageFragment) javaElement;
					String packageFragmentName = packageFragment.getElementName();
					if (packageFragmentName.contains(".model")) {
						return true;
					}
				}
			}
		} catch (JavaModelException e) {
			logger.info("There are no package in the source folder");
		}
		
		return false;
	}

}
