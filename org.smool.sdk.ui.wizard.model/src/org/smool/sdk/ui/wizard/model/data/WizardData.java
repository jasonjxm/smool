/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Alejandro Villamarin (Tecnalia Researh and Innovation - Software Systems Engineering) - bug fixing
 *******************************************************************************/ 

package org.smool.sdk.ui.wizard.model.data;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.XMLMemento;
import org.smool.sdk.ui.wizard.model.pages.util.OntologyReader;
import org.smool.sdk.ui.wizard.model.pages.util.SmartEnvironmentHandler;

import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;



/**
 * Constains the intermediate information to create a new Knowledge Processor
 * project using the Wizard, Singleton class.
 * 
 * @author Fran Ruiz (fran.ruiz@tecnalia.com), Alejandro Villamarin (alejandro.villamarin@tecnalia.com) Tecnalia
 * 
 */
public class WizardData {
	
	
	private final static WizardData INSTANCE = new WizardData();
	
	/** Defines the key of smool.properties */
    private static final String LOCATOR = "smartenvironment.locator";
    
	private static SmartEnvironmentHandler h = null;
	
	private final String WIZARD_DATA_FILE = ".metadata/.plugins/org.smool.sdk.ui.wizard.model/data.xml";
	
	
	/**
	 * Constructor
	 */
	private WizardData(){
		//check if there were some ontologies cached, otherwise, initilize empty lists
		if (selectedOntologies == null) {selectedOntologies = new HashMap<String, OntModel>();}
		if (selectedProducerOntologyConcepts == null){selectedProducerOntologyConcepts = new ArrayList<OntClass>();}
		if (selectedConsumerOntologyConcepts == null) {	selectedConsumerOntologyConcepts = new ArrayList<OntClass>();}
		//load cache
		loadData();
	}
	/**
	 * GetInstanceOf method for this Singleton
	 * @return The only instance of WizardData
	 */
	public static WizardData getInstance(){
		return INSTANCE;
	}
	
	/** The selected ontologies to include its concepts */
	private HashMap<String, OntModel> selectedOntologies;

	/** The list of selected ontology concepts */
	private ArrayList<OntClass> selectedProducerOntologyConcepts;
	private ArrayList<OntClass> selectedConsumerOntologyConcepts;
	
	/** The Ontology Model */
	private OntologyModel model = null;

	/** The logger */
	private static Logger logger = Logger.getLogger(WizardData.class);

	/** The project name */
	private String projectName = null;
	
	/** The package name */
	private String packageName = null;
	
	/** The Project Path in where to located the project */
	private IPath projectPath = null;

	/** The execution context */
	private int context = 0;
	
	private IProject project = null;
	
	private boolean usesUserDefinedOntology = false;
	
	public boolean usesUserDefinedOntology() {
		return usesUserDefinedOntology;
	}

	public void setUsesUserDefinedOntology(boolean usesUserDefinedOntology) {
		this.usesUserDefinedOntology = usesUserDefinedOntology;
	}
	
	/**
	 * Used to store the custom ontology file path
	 */
	private String userOntologyPath = null;

	/**
	 * Constant fields used for storing data into the cache
	 */
	private final String ROOT = "modellayer";
	private final String PROJECT = "project";
	private final String PROJECT_NAME = "name";
	private final String PROJECT_PATH = "path";
	private final String PACKAGE = "package";
	private final String CONTEXT = "context";
	private final String IS_USER = "useront";
	private final String SELECTED_ONTOLOGIES = "ontologies";
	private final String ONTOLOGY = "ontology";
	private final String ONT_ID = "id";
	private final String ONT_PATH = "path";
	private final String PRODUCED_CONCEPTS = "produced_concepts";
	private final String CONSUMED_CONCEPTS = "consumed_concepts";
	private final String CONCEPT = "concept";
	private final String CONCEPT_ID = "id";


	
	/**
	 * Adds an smart environment the current smart environment list
	 * @param envList SmartEnvironments list
	 * @return TRUE if adding was successful
	 */
	public boolean addSmartEnvironments(Collection<SmartEnvironment> envList) {

		for (SmartEnvironment env : envList) {
		
			// Check if the ontology is already present in the ontology model
			if (!selectedOntologies.containsKey(env.getId())) {
				try {
					selectedOntologies.put(env.getId(), env.getOntologyModel());
				} 
				catch (Exception ex) {
					logger.error("A exception arised adding a smart environment");
					ex.printStackTrace();
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Adds an smart environment the current smart environment list
	 * @param envList SmartEnvironments list
	 * @return TRUE if adding was successful
	 */
	public boolean addSmartEnvironment(SmartEnvironment smartEnvironment) {
		
		// Check if the ontology is already present in the ontology model
		if (!selectedOntologies.containsKey(smartEnvironment.getId())) {
			try {
				selectedOntologies.put(smartEnvironment.getId(), smartEnvironment.getOntologyModel());
			} catch (Exception ex) {
				logger.error("A exception arised adding a smart environment");
				ex.printStackTrace();
				return false;
			}
		}
		return true;
	}	
	
	/**
	 * Removes a Smart Environment from the current smart environment list
	 * @param envList SmartEnvironments lists
	 * @return TRUE if removing was successfull
	 */
	public boolean removeSmartEnvironments(Collection<SmartEnvironment> envList) {
		
		for (SmartEnvironment env : envList) {
			logger.debug("Removing ontology " + env.getId());
	
			if (env.getId() != null && selectedOntologies.containsKey(env.getId())) {
				selectedOntologies.remove(env.getId());
				
				// Remove any selected concepts associated to the removed ontology
				ArrayList<OntClass> toRemove = new ArrayList<OntClass>();
				for (OntClass c : selectedConsumerOntologyConcepts) {
					if (c.getOntModel().equals(env.getOntologyModel())) {
						toRemove.add(c);
					}
				}
				selectedConsumerOntologyConcepts.removeAll(toRemove);
				
				toRemove.clear();
				for (OntClass c : selectedProducerOntologyConcepts) {
					if (c.getOntModel().equals(env.getOntologyModel())) {
						toRemove.add(c);
					}
				}
				selectedProducerOntologyConcepts.removeAll(toRemove);
			} else {
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Determines whether the ontology model has been loaded
	 * @return <code>true</code> if there is loaded the ontology model
	 */
	public boolean hasLoadedOntologyModel() {
		return selectedOntologies.size() > 0;
	}	
	
	/**
	 * Adds a ontology concept to the current ontology list
	 * @param element a ClassNode representing an ontology concept
	 */
	public void addConsumerOntologyConcept(OntClass element) {
		try {
			if (!selectedConsumerOntologyConcepts.contains(element)) {
				selectedConsumerOntologyConcepts.add(element);
			}
		} catch (Exception e) {
			logger.error("Error adding the ontology. " + e.getStackTrace().toString());
			e.printStackTrace();
		}
	}
	
	/**
	 * Removes an ontology concepts from the selected ontology concepts
	 * @param element an ClassNode element representing an ontology concept
	 */
	public void removeConsumerOntologyConcept(OntClass element) {

		try {
			if (selectedConsumerOntologyConcepts.contains(element)) {
				selectedConsumerOntologyConcepts.remove(element);
			}
		} catch (Exception e) {
			logger.error("Error removing the ontology. " + e.getStackTrace().toString());
			e.printStackTrace();
		}
	}

	/**
	 * Adds a ontology concept to the current ontology list
	 * @param element a ClassNode representing an ontology concept
	 */
	public void addProducerOntologyConcept(OntClass element) {
		try {
			if (!selectedProducerOntologyConcepts.contains(element)) {
				selectedProducerOntologyConcepts.add(element);
			}
		} catch (Exception e) {
			logger.error("Error adding the ontology. " + e.getStackTrace().toString());
			e.printStackTrace();
		}
	}
	
	/**
	 * Removes an ontology concepts from the selected ontology concepts
	 * @param element an ClassNode element representing an ontology concept
	 */
	public void removeProducerOntologyConcept(OntClass element) {
		try {
			if (selectedProducerOntologyConcepts.contains(element)) {
				selectedProducerOntologyConcepts.remove(element);
			}
		} catch (Exception e) {
			logger.error("Error removing the ontology. " + e.getStackTrace().toString());
			e.printStackTrace();
		}
	}

	
	/**
	 * Gets the list of selected ontology concepts
	 * @return a list of selected ontology concepts
	 */
	public Collection<OntClass> getProducerOntologyConcepts() {
		return selectedProducerOntologyConcepts;
	}	

	/**
	 * Gets the list of selected ontology concepts
	 * @return a list of selected ontology concepts
	 */
	public Collection<OntClass> getConsumerOntologyConcepts() {
		return selectedConsumerOntologyConcepts;
	}	

	/**
	 * Gets the ontology mapping
	 * @return the mapping
	 */
	public HashMap<String, OntModel> listOntologies() {
		return selectedOntologies;
	}
	
	/**
	 * Creates the ontology model.
	 * Taking as a reference the ontologies that are already in the model,
	 * it loads the concepts into a integrated model
	 * @param monitor a monitor to track the evolution
	 */
	public void createOntologyModel(IProgressMonitor monitor) {
		model = new OntologyModel();

		Iterator<String> it = selectedOntologies.keySet().iterator();
		
		while (it.hasNext()) {
			String id = it.next();
			OntModel ontModel = selectedOntologies.get(id);
			
			model.addOntology(ontModel);
		}
	}
	
	/**
	 * Gets the Ontology Model
	 * @return the Ontology Model
	 */
	public OntologyModel getOntologyModel() {
		return model;
	}
	
	/**
	 * Determines whether the user has selected some ontology concepts
	 */
	public boolean hasSelectedOntologyConcepts() {
		return (selectedConsumerOntologyConcepts.size() > 0 || selectedProducerOntologyConcepts.size() > 0);
	}

	/**
	 * Determines whether the user has selected some ontology concepts
	 */
	public boolean hasSelectedProducerConcepts() {
		return selectedProducerOntologyConcepts.size() > 0;
	}

	/**
	 * Determines whether the user has selected some ontology concepts
	 */
	public boolean hasSelectedConsumerConcepts() {
		return selectedConsumerOntologyConcepts.size() > 0;
	}

	/**
	 * @param projectName the projectName to set
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	/**
	 * @return the projectName
	 */
	public String getProjectName() {
		return projectName;
	}

	/**
	 * @param packageName the packageName to set
	 */
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	/**
	 * @return the packageName
	 */
	public String getPackageName() {
		return packageName;
	}

	/**
	 * Gets the base location for the project
	 * @return the base location for the project
	 */
	public IPath getPath() {
		return projectPath;
	}

	/**
	 * Establishes the context in what to create the context 
	 * 
	 * @param selectionIndex index
	 */
	public void setContext(int selectionIndex) {
		this.context  = selectionIndex;
	}
	
	/**
	 * Gets the project execution context.
	 * This context is codified with the following codes:
	 * <ul>
	 * <li>0 = Prosumer (Producer + Consumer)</li>
	 * <li>1 = Producer </li>
	 * <li>2 = Consumer </li>
	 * </ul> 
	 * 
	 * @return the context index
	 */
	public int getContext() {
		return context;
	}
	
	public void setProject(IProject project) {
		this.project = project;
	}	
	
	public IProject getProject() {
		return project;
	}

	public void clearSmartEnvironments() {
		selectedOntologies.clear();
	}

	public void clearOntologyClasses() {
		selectedProducerOntologyConcepts.clear();
		selectedConsumerOntologyConcepts.clear();
		
	}
	
	public String getUserOntologyPath() {
		return userOntologyPath;
	}

	public void setUserOntologyPath(String userOntologyPath) {
		this.userOntologyPath = userOntologyPath;
	}
	
	public HashMap<String, OntModel> getSelectedOntologies() {
		return selectedOntologies;
	}

	public void setSelectedOntologies(HashMap<String, OntModel> selectedOntologies) {
		this.selectedOntologies = selectedOntologies;
	}
	
	
	/**
	 * Method that loads the XML file with last execution of the Wizard into the
	 * memory cache, i.e., the selectedOntologies, selectedProducerOntologyConcepts
	 * and selectedConsumerOntologyConcepts maps.
	 * 
	 */
	private void loadData() {
		
		try {
			XMLMemento memento = XMLMemento.createReadRoot(new FileReader(ResourcesPlugin.getWorkspace().getRoot().getLocation().append(WIZARD_DATA_FILE).toOSString()));
			// Get the information from the Project
			IMemento p = memento.getChild(PROJECT);
			projectName = p.getString(PROJECT_NAME);
			project = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
			projectPath = new Path(p.getString(PROJECT_PATH));
			packageName = p.getString(PACKAGE);
			context = p.getInteger(CONTEXT);
			
			// Get selected ontologies
			IMemento so = p.getChild(SELECTED_ONTOLOGIES);
			usesUserDefinedOntology = so.getBoolean(IS_USER);
			
			if (!usesUserDefinedOntology) {
				// This is not the best way to go, but it works...MUST be optimized soon!!!
				// Loading all ontologies
				if (h == null) {
					h = new SmartEnvironmentHandler(LOCATOR);
				}
				for (IMemento ont : so.getChildren(ONTOLOGY)) {
					for (SmartEnvironment se : h.listSmartEnvironments()) {
						if (ont.getString(ONT_ID).equals(se.getId())) {
							selectedOntologies.put(se.getId(), se.getOntologyModel());
						}
					}
				}
			}
			else {
				for (IMemento ont : so.getChildren(ONTOLOGY)) {
					userOntologyPath = ont.getString(ONT_PATH);
					URI uri = new URI("file", null, ont.getString(ONT_PATH), null);
					OntologyReader reader = new OntologyReader(uri);
					selectedOntologies.put(reader.getSmartEnvironment().getId(), reader.getSmartEnvironment().getOntologyModel());
				}
			}
			
			// Get produced concepts
			IMemento pcpts = p.getChild(PRODUCED_CONCEPTS);
			
			if (pcpts != null) {
				for (IMemento cpt : pcpts.getChildren(CONCEPT)) {
					for (OntModel m : selectedOntologies.values()) {
						OntClass c = m.getOntClass(cpt.getString(CONCEPT_ID));
						if (c != null) {
							selectedProducerOntologyConcepts.add(c);
						}
						else {
							System.out.println("Couldn't find concept: " + cpt.getString(CONCEPT_ID)); // DEBUG info
						}
					}
				}
			}
			
			// Get consumer concepts
			IMemento ccpts = p.getChild(CONSUMED_CONCEPTS);
			
			if (ccpts != null) {
				for (IMemento cpt : ccpts.getChildren(CONCEPT)) {
					for (OntModel m : selectedOntologies.values()) {
						OntClass c = m.getOntClass(cpt.getString(CONCEPT_ID));
						if (c != null) {
							selectedConsumerOntologyConcepts.add(c);
						}
						else {
							System.out.println("Couldn't find concept: " + cpt.getString(CONCEPT_ID)); // DEBUG info
						}
					}
				}
			}
			
			createOntologyModel(null);
			
		} catch (Exception e) {
			//e.printStackTrace();
		}
	}
	
	/**
	 * Method used to store memory cache (the hashmaps) into file. This is used when Eclipse is shutdown, we need
	 * to store last execution into file so next time we have the last execution cached. It stores an XML file
	 * in ./.metadata/plugins/org.smool.sdk.ui.wizard.model.data
	 */
	public void storeData () {
		XMLMemento memento = XMLMemento.createWriteRoot(ROOT);
		IMemento p = memento.createChild(PROJECT);
		p.putString(PROJECT_NAME, projectName);
		p.putString(PROJECT_PATH, project.getLocation().toOSString());
		p.putInteger(CONTEXT, context);
		p.putString(PACKAGE, packageName);
		
		if (selectedOntologies.size() > 0) {
			IMemento so = p.createChild(SELECTED_ONTOLOGIES);
			so.putBoolean(IS_USER, usesUserDefinedOntology);
			
			for (java.util.Map.Entry<String, OntModel> e : selectedOntologies.entrySet()) {
				IMemento ont = so.createChild(ONTOLOGY);
				if (usesUserDefinedOntology) {
					ont.putString(ONT_PATH, userOntologyPath);
				}
				else {
					ont.putString(ONT_ID, e.getKey());
				}
			}
		}
		
		if (selectedProducerOntologyConcepts.size() > 0) {
			IMemento concepts = p.createChild(PRODUCED_CONCEPTS);
			
			for (OntClass c : selectedProducerOntologyConcepts) {
				IMemento cpt = concepts.createChild(CONCEPT);
				cpt.putString(CONCEPT_ID, c.getURI());
			}
		}
		
		if (selectedConsumerOntologyConcepts.size() > 0) {
			IMemento concepts = p.createChild(CONSUMED_CONCEPTS);
			
			for (OntClass c : selectedConsumerOntologyConcepts) {
				IMemento cpt = concepts.createChild(CONCEPT);
				cpt.putString(CONCEPT_ID, c.getURI());
			}
		}
		
		try {
			File f = new File(ResourcesPlugin.getWorkspace().getRoot().getLocation().append(".metadata/.plugins/org.smool.sdk.ui.wizard.model").toOSString());
			if (!f.exists()) {
				f.mkdir();
			}
			f = new File(ResourcesPlugin.getWorkspace().getRoot().getLocation().append(WIZARD_DATA_FILE).toOSString());
			if (!f.exists() && f.canWrite()) {
				f.createNewFile();
			}
			memento.save(new FileWriter(ResourcesPlugin.getWorkspace().getRoot().getLocation().append(WIZARD_DATA_FILE).toOSString()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public SmartEnvironmentHandler getHandler() {
		if (h == null) {
			h = new SmartEnvironmentHandler(LOCATOR);
		}
		return h;
	}
}
