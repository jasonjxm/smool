/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.wizard.model.pages.frames;

import java.util.ArrayList;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.Flags;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaModel;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.ITypeHierarchy;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jdt.ui.actions.OpenJavaPerspectiveAction;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.texteditor.ITextEditor;
import org.smool.sdk.ui.perspective.SmoolNature;


/**
 * This class is a helper class for getting info of the 
 * current workspace
 * @author Fran Ruiz, fran.ruiz@tecnalia.com, ESI
 *
 */
public class ProjectHelper {

	public static final String TEST_CLASS_OK = null;  
	
	/**
	 * Returns a list of source classpath for a specified project  
	 * @param javaProject  
	 * @return a list of path relative to the workspace root.  
	 */
	public static ArrayList<IPath> listSourceFolders(IJavaProject javaProject) {
		ArrayList<IPath> sourceList = new ArrayList<IPath>();
		
		// Getting the new class path
		IClasspathEntry[] entries = javaProject.readRawClasspath();
		if (entries != null) {  
			for (IClasspathEntry entry : entries) {
				// The entry indentifies a folder containing package fragments 
				// with source code to be compiled.
				if (entry.getEntryKind() == IClasspathEntry.CPE_SOURCE) {
					sourceList.add(entry.getPath());
				}
			}
		}
	    return sourceList;
	}
	
	/**
	 * Adds a marker to a file on a specific line
	 * @param file the file to be marked 
	 * @param markerId The id of the marker to add. 
	 * @param message the message associated with the mark  
	 * @param lineNumber the line number where to put the mark  
	 * @param severity the severity of the marker.  
	 * @return the IMarker that was added or null if it failed to add one.  
	 */
	 public final static IMarker addMarker(IResource file, 
			 String markerId, 
			 String message, 
			 int lineNumber, 
			 int severity) {
		 try {  
			 IMarker marker = file.createMarker(markerId);  
			 marker.setAttribute(IMarker.MESSAGE, message);  
			 marker.setAttribute(IMarker.SEVERITY, severity);  
			 if (lineNumber == -1) { 
				 lineNumber = 1; 
			 } 

			 marker.setAttribute(IMarker.LINE_NUMBER, lineNumber); 

			 // on Windows, when adding a marker to a project, 
			 // it takes a refresh for the marker to show. In order to fix this
			 // we're forcing a refresh of elements receiving 
			 // markers (and only the element, not its children), to force the marker display. 
			 file.refreshLocal(IResource.DEPTH_ZERO, new NullProgressMonitor());
			 
			 return marker; 
		 } catch (CoreException cex) {
			 cex.printStackTrace();
			 //SmoolLogging.log(cex, "Failed to add marker '%1$s' to '%2$s'", markerId, file.getFullPath());
		 }

		 return null; 
	 }
	 
	 /**
	  * Adds a marker to a resource. 
	  * @param resource the resource to be marked
	  * @param markerId The id of the marker to add. 
	  * @param message the message associated with the mark 
	  * @param severity the severity of the marker. 
	  * @return the IMarker that was added or null if it failed to add one. 
	  */ 
	 public final static IMarker addMarker(IResource resource, String markerId,
			 String message, int severity) { 
		 try {  
			 IMarker marker = resource.createMarker(markerId);  
			 marker.setAttribute(IMarker.MESSAGE, message);  
			 marker.setAttribute(IMarker.SEVERITY, severity);  

			 // on Windows, when adding a marker to a project, 
			 // it takes a refresh for the marker to show. In order to fix this
			 // we're forcing a refresh of elements receiving 
			 // markers (and only the element, not its children), to force the marker display. 
			 resource.refreshLocal(IResource.DEPTH_ZERO, new NullProgressMonitor());
			 
			 return marker; 
		 } catch (CoreException cex) {
			 cex.printStackTrace();
			 //SmoolLogging.log(cex, "Failed to add marker '%1$s' to '%2$s'", markerId, file.getFullPath());
		 }
		 return null;
	 }
		 
		 
	 /** 
	  * Tests that a class name is valid for usage in the manifest. 
	  * This tests the class existence, that it can be instantiated (ie it must not be abstract, 
	  * nor non static if enclosed), and that it extends the proper super class (not necessarily 
	  * directly) 
	  * @param javaProject the {@link IJavaProject} containing the class. 
	  * @param className the fully qualified name of the class to test.
	  * @param superClassName the fully qualified name of the expected super class. 
	  * @param testVisibility if <code>true</code>, the method will check the visibility of the class 
	  * or of its constructors. 
	  * @return {@link #TEST_CLASS_OK} or an error message. 
	  */ 
	 public final static String testClassForManifest(IJavaProject javaProject, 
			 String className, 
			 String superClassName, boolean testVisibility) { 

		 try {
			 String javaClassName = className.replaceAll("\\$", "\\."); //$NON-NLS-1$ //$NON-NLS-2$
			 
			 // look for the IType object for this class
			 IType type = javaProject.findType(javaClassName);
			 if (type != null && type.exists()) {
				// test that the class is not abstract
				 int flags = type.getFlags();
				 if (Flags.isAbstract(flags)) { 
					 return String.format("%1$s is abstract", className); 
				 }

				 // test whether the class is public or not. 
				 if (testVisibility && Flags.isPublic(flags) == false) { 
					 // if its not public, it may have a public default constructor, 
					 // which would then be fine. 
					 IMethod basicConstructor = type.getMethod(type.getElementName(), new String[0]); 
					 if (basicConstructor != null && basicConstructor.exists()) { 
						 int constructFlags = basicConstructor.getFlags(); 
						 if (Flags.isPublic(constructFlags) == false) { 
							 return String.format("%1$s or its default constructor must be public for the system to be able to instantiate it", 
									 className);
						 }
					 } else {
						 return String.format("%1$s must be public, or the system will not be able to instantiate it.", className);
					 }
				 }
				 
				 // If it's enclosed, test that it's static. If its declaring class is enclosed 
				 // as well, test that it is also static, and public. 
				 IType declaringType = type; 
				 do { 
					 IType tmpType = declaringType.getDeclaringType(); 
					 if (tmpType != null) { 
						 if (tmpType.exists()) { 
							 flags = declaringType.getFlags(); 
							 if (Flags.isStatic(flags) == false) { 
								 return String.format("%1$s is enclosed, but not static", declaringType.getFullyQualifiedName());
							 }
							 flags = tmpType.getFlags(); 
							 if (testVisibility && Flags.isPublic(flags) == false) { 
								 return String.format("%1$s is not public", 
										 tmpType.getFullyQualifiedName()); 
							 }
						 } else {
							 // if it doesn't exist, we need to exit so we may as well mark it null. 
							 tmpType = null; 
						 } 
					 } 
					 declaringType = tmpType; 
				 } while (declaringType != null); 

				 // test the class inherit from the specified super class. 

				 // get the type hierarchy 
				 ITypeHierarchy hierarchy = type.newSupertypeHierarchy(new NullProgressMonitor()); 

				 // if the super class is not the reference class, it may inherit from 
				 // it so we get its supertype. At some point it will be null and we
				 // will stop 
				 IType superType = type;                 
				 boolean foundProperSuperClass = false; 
				 while ((superType = hierarchy.getSuperclass(superType)) != null && 
						 superType.exists()) { 
					 if (superClassName.equals(superType.getFullyQualifiedName())) { 
						 foundProperSuperClass = true; 
					 }
				 } 

				 // didn't find the proper superclass? return false. 
				 if (foundProperSuperClass == false) { 
					 return String.format("%1$s does not extend %2$s", className, superClassName); 
				 } 
				 return TEST_CLASS_OK; 
			 } else { 
				 return String.format("Class %1$s does not exist", className); 
			 } 
		 } catch (JavaModelException e) { 
			 return String.format("%1$s: %2$s", className, e.getMessage());
		 }
	 }
	 
	 /**
	  * Returns the {@link IJavaProject} for a {@link IProject} object. 
	  * This checks if the project has the Java Nature first. 
	  * @param project
	  * @return the IJavaProject or null if the project couldn't be created or if the project 
	  * does not have the Java Nature. 
	  * @throws CoreException 
	  */ 
	 public static IJavaProject getSMOOLProject(IProject project) throws CoreException { 
		 if (project != null && project.hasNature(SmoolNature.NATURE_ID)) {
			 return JavaCore.create(project); 
		 } 
		 return null; 
	 }
	 
	 /** 
	  * Reveals a specific line in the source file defining a specified class, 
	  * for a specific project. 
	  * @param project the IProject element
	  * @param className the class name
	  * @param line the name to reveal
	  */ 
	 public static void revealSource(IProject project, String className, 
			 int line) { 
		 // in case the type is enclosed, we need to replace the $ with . 
		 className = className.replaceAll("\\$", "\\."); //$NON-NLS-1$ //$NON-NLS2$ 

		 // get the java project 
		 IJavaProject javaProject = JavaCore.create(project); 
         try { 
        	 // look for the IType matching the class name. 
        	 IType result = javaProject.findType(className); 
        	 if (result != null && result.exists()) { 
        		 // before we show the type in an editor window, we make sure the current 
        		 // workbench page has an editor area (typically the ddms perspective doesn't).
        		 IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(); 
                if (page.isEditorAreaVisible() == false) { 
                	// no editor area? we open the java perspective. 
                	new OpenJavaPerspectiveAction().run();
                }
                
                IEditorPart editor = JavaUI.openInEditor(result); 
                
                if (editor instanceof ITextEditor) { 
                	// get the text editor that was just opened. 
                	ITextEditor textEditor = (ITextEditor)editor;

                	// select and reveal the line. 
                	textEditor.selectAndReveal(0, 0); 

                } 
        	 } 
        } catch (JavaModelException ex) { 
        	 ex.printStackTrace();
        } catch (PartInitException ex) {
        	 ex.printStackTrace();
        }
	 }

	 /** 
	  * Returns the list of SMOOL-flagged projects for the specified javaModel. 
	  * This list contains projects that are opened in the workspace and that 
	  * are flagged as SMOOL project (through the SMOOL nature) 
	  * @param javaModel the Java Model object corresponding for the current workspace root. 
	  * @return an array of IJavaProject, which can be empty if no projects match. 
	  */ 
	 public static IJavaProject[] listSMOOLProjects(IJavaModel javaModel) {
		 // get the java projects 
		 IJavaProject[] javaProjectList = null; 

		 try { 
			 javaProjectList = javaModel.getJavaProjects(); 
		 } catch (JavaModelException jme) { 
			 return new IJavaProject[0]; 
		 } 

		 // temp list to build the android project array 
		 ArrayList<IJavaProject> smoolProjectList = new ArrayList<IJavaProject>(); 

		 // loop through the projects and add the smool flagged projects to the temp list. 
		 for (IJavaProject javaProject : javaProjectList) { 
			 // get the workspace project object 
			 IProject project = javaProject.getProject(); 
			 // check if it's a smool project based on its nature 
			 try { 
				 if (project.hasNature(SmoolNature.NATURE_ID)) { 
					 smoolProjectList.add(javaProject); 
				 }
			 } catch (CoreException e) { 
				 // this exception, thrown by IProject.hasNature(), means the project either doesn't 
				 // exist or isn't opened. So, in any case we just skip it (the exception will 
				 // bypass the ArrayList.add() 
			 } 
		 } 
		 // return the smool project list. 
		 return smoolProjectList.toArray(new IJavaProject[smoolProjectList.size()]); 
	 }
	  
	 /** 
	  * Returns the list of SMOOL-flagged projects for the specified javaModel. 
	  * This list contains projects that are opened in the workspace and that 
	  * are flagged as SMOOL project (through the SMOOL nature) 
	  * @return an array of IJavaProject, which can be empty if no projects match. 
	  */ 
	 public static IJavaProject[] listSMOOLProjects() {
		 IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		 IJavaModel javaModel = JavaCore.create(root);
		 
		 return listSMOOLProjects(javaModel);
	 }
	 
	 /** 
	  * Returns the {@link IFolder} representing the output for the project. 
	  * The project must be a java project and be opened, or the method will return null. 
	  * @param project the {@link IProject} 
      * @return an IFolder item or null. 
      */ 
	 public final static IFolder getOutputFolder(IProject project) {
		 try { 
			 if (project.isOpen() && project.hasNature(SmoolNature.NATURE_ID)) { 
				 // get a smool project from the normal project object 
				 IJavaProject sofiaProject = JavaCore.create(project); 
				 IPath path = sofiaProject.getOutputLocation(); 
				 IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot(); 
				 
				 IResource outputResource = root.findMember(path); 
				 if (outputResource != null && outputResource.getType() == IResource.FOLDER) { 
					 return (IFolder)outputResource;
				 } 
			 } 
		 } catch (JavaModelException e) { 
			 // Let's do nothing and return null 
		 } catch (CoreException e) { 
			 // Let's do nothing and return null 
		 } 
		 return null; 
	 }	  

	 public static IPackageFragmentRoot getSourceFolder(IJavaProject project) throws CoreException {
    
	        // find the first non-JAR package fragment root
	        IPackageFragmentRoot[] roots = project.getPackageFragmentRoots();

	        IPackageFragmentRoot srcFolder = null;

	        for (int i = 0; i < roots.length; i++) {
	            if (roots[i].isArchive() || roots[i].isReadOnly()
	                    || roots[i].isExternal()) {
	                continue;
	            } else {
	                srcFolder = roots[i];
	                break;
	            }
	        }

	        return srcFolder;
	 }

}
