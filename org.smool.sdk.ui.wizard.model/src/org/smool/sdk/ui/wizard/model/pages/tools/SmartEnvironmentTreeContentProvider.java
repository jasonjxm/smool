/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Alejandro Villamarin (Tecnalia Researh and Innovation - Software Systems Engineering) - bug fixing
 *******************************************************************************/ 

package org.smool.sdk.ui.wizard.model.pages.tools;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.smool.sdk.ui.wizard.model.data.SmartEnvironment;
import org.smool.sdk.ui.wizard.model.data.WizardData;
import org.smool.sdk.ui.wizard.model.pages.util.SmartEnvironmentHandler;


/**
 * This class represent the content for an Smart Environment Tree.
 * 
 * @author Fran Ruiz (fran.ruiz@tecnalia.com), Alejandro Villamarin (alejandro.villamarin@tecnalia.com) Tecnalia
 */
public class SmartEnvironmentTreeContentProvider implements ITreeContentProvider {

	/** The smart environments to be shown */
	private ArrayList<SmartEnvironment> smartEnvironments = new ArrayList<SmartEnvironment>();
	
	/**
	 * Constructor
	 */
	public SmartEnvironmentTreeContentProvider() {
		// Get the smart environments
		SmartEnvironmentHandler smartEnvironmentHandler = WizardData.getInstance().getHandler();
		
		// Adds the obtained smart environments
		this.smartEnvironments.addAll(smartEnvironmentHandler.listSmartEnvironments());
		
	}
	
	/**
	 * @see ITreeContentProvider#getChildren(Object)
	 */
	@Override
	public Object[] getChildren(Object parentElement) {
        // Return the nodelist
		SmartEnvironment parent = (SmartEnvironment) parentElement;
		
 	    Collection<SmartEnvironment> children = parent.getChildNodes();
 	   
 	    return children.toArray();
	}

	/**
	 * @see ITreeContentProvider#getParent(Object)
	 */
	@Override
	public Object getParent(Object element) {
		SmartEnvironment node = (SmartEnvironment) element;
		return node.getParent();
	}

	/**
	 * @see ITreeContentProvider#hasChildren(Object)
	 */
	@Override
	public boolean hasChildren(Object element) {
        // Get the children
		SmartEnvironment parent = (SmartEnvironment) element;
		
  	    Collection<SmartEnvironment> children = parent.getChildNodes();
		
        // Return whether the parent has children
        return children == null ? false : children.size() > 0;
	}

	/**
	 * @see ITreeContentProvider#getElements(Object)
	 */
	@Override
	public Object[] getElements(Object inputElement) {
       // These are the root elements of the tree
       // We don't care what inputElement is, because we just want all
       // the root nodes of smartEnvironment
 	   return smartEnvironments.toArray();
	}

	@Override
	public void dispose() {
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
	}
}
