/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.wizard.model.pages.util;

import org.apache.log4j.Logger;
import org.smool.sdk.common.properties.PropertyLoader;
import org.smool.sdk.common.sax.XMLParser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**
 * The <code>XMLHandler</code> class is responsible for getting the content
 * of a reference, and transforming it into a readable XML format.
 * 
 * @author Fran Ruiz, fran.ruiz@tecnalia.com, ESI
 *
 */
public abstract class XMLHandler {

    /** Remote suffix */
    private static final String REMOTE = ".remote";
    
    /** Local suffix */
    private static final String LOCAL = ".local";
	
    /** The logger */
	private static Logger logger = Logger.getLogger(XMLHandler.class);
	
	/**
	 * The constructor for generate the smart environment model
	 * @param key The key corresponding to smool properties in what is
	 * contained the URL
	 */
	public XMLHandler(String key) {
 		try {
			StringBuffer remoteReference = new StringBuffer();
			remoteReference.append(key).append(REMOTE);

			StringBuffer localReference = new StringBuffer();
			localReference.append(key).append(LOCAL);
			
			// Gets the property value for the remote reference
 			String remoteValue = PropertyLoader.getInstance().getProperty(remoteReference.toString());
			
			if (remoteValue == null) {
				logger.info("The remote reference " + remoteReference.toString() +
						"cannot be obtained  from the property file");
			}

			// Gets the property value for the local reference
 			String localValue = PropertyLoader.getInstance().getProperty(localReference.toString());
			
			if (localValue == null) {
				logger.info("The remote reference " + remoteReference.toString() +
						"cannot be obtained from the property file");
			}

			XMLParser parser = new XMLParser();
			parser.parse(remoteValue);
			
			Document dom = parser.getDocument();
			
			if (dom == null) { // trying the local resource
				logger.info("Cannot load remote file " + remoteReference.toString() +
						". Trying to get local reference");
				parser.parse(localValue);
				dom = parser.getDocument();
			} else {
				logger.debug("Using remote value");
			}
			
			if (dom == null) { // cannot read the xml
				logger.error("Cannot load local file " + localReference.toString() +
				". Trying to get local reference");
			}
			
			read(dom.getDocumentElement());
		} catch (Exception ex) {
			logger.error("An exception arised during XML processing: " + ex.getMessage());
			ex.printStackTrace();
		}
	}		
			
 	public XMLHandler() {
		// do nothing
	}

	public void readFile(String fileReference) {
 		try {
			XMLParser parser = new XMLParser();
			parser.parse(fileReference);
			
			Document dom = parser.getDocument();
			
			if (dom == null) { // trying the local resource
				logger.error("Cannot load file " + fileReference.toString());
			} else {
				read(dom.getDocumentElement());
			}
		} catch (Exception ex) {
			logger.error("An exception arised during XML processing: " + ex.getMessage());
			ex.printStackTrace();
		}
	}
	
	/**
	 * The methods that reads the XML document
	 */
	protected abstract void read(Element root);
}
