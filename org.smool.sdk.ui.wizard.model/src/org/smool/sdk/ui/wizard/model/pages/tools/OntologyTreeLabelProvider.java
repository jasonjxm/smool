/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.wizard.model.pages.tools;

import org.apache.log4j.Logger;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.swt.graphics.Image;
import org.smool.sdk.ui.wizard.model.data.OntologyModel;

import com.hp.hpl.jena.ontology.OntClass;


/**
 * This class reads ontologies using Jena engine given and implements
 * its to show in a TreeViewer widget.
 * 
 * @author Fran Ruiz, fran.ruiz@tecnalia.com, ESI
 */
public class OntologyTreeLabelProvider implements ILabelProvider {

	/** The ontology model */
	private OntologyModel ontologyModel = null;
	
	/** The logger */
	private static Logger logger = Logger.getLogger(OntologyTreeLabelProvider.class);
	
	/**
	 * Creates a ontology tree label provider
	 */
	public OntologyTreeLabelProvider() {
		this.ontologyModel = null;
	}	
	
	/**
	 * Reads the URL where is located the ontology
	 * @param ontModel The Ontology Model
	 */
	public OntologyTreeLabelProvider(OntologyModel ontModel) {
		if (ontModel != null) {
			this.ontologyModel = ontModel;
		}
	}
	
	@Override
	public Image getImage(Object element) {
		return null;
	}

	/**
	 * Gets the label for the current element
	 * @param element a ClassNode
	 */
	@Override
	public String getText(Object element) {
		OntClass ontologyClass = null; 
		String prefix = null;

		// Cast the element to JENA OntClass
		try {
			ontologyClass = (OntClass) element;
			prefix = ontologyModel.getPrefix(ontologyClass.getNameSpace());
		} catch (ClassCastException ex) {
			logger.error("Cannot cast the element into JENA class");
			logger.error(ex.getStackTrace().toString());
		}
		
		return prefix + ":" + ontologyClass.getLocalName();
	}
		
	@Override
	public void addListener(ILabelProviderListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isLabelProperty(Object element, String property) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
}
