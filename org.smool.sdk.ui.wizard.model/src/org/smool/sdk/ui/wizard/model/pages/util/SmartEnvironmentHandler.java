/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.wizard.model.pages.util;

import static org.w3c.dom.Node.ELEMENT_NODE;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.swt.graphics.Image;
import org.smool.sdk.common.sax.ResourceUtil;
import org.smool.sdk.ui.wizard.model.data.SmartEnvironment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.rdf.model.ModelFactory;


/**
 * This class contains the information about the available smart environments.
 * This information will be used by the ADK Wizard to be displayed for the user
 * 
 * @author Fran Ruiz, fran.ruiz@tecnalia.com, ESI
 *
 */
public class SmartEnvironmentHandler extends XMLHandler {

	private static ArrayList<SmartEnvironment> model = new ArrayList<SmartEnvironment>();
	
	private static String[] allowedNodes = {"foundational", "core", "domain", "application"}; 
	
	private static final int ELEMENT_TYPE = Node.ELEMENT_NODE;
	
	private static String ID_ELEMENT = "id";
	private static String NAME_ELEMENT = "name";
	private static String DESC_ELEMENT = "description";
	private static String ONTOLOGY_ELEMENT = "resource";
	private static String IMAGE_ELEMENT = "icon";
	
	// Define a static logger variable so that it references the
    // Logger instance named "SmartEnvironmentHandler".
	private static Logger logger = Logger.getLogger(SmartEnvironmentHandler.class);
	
	/**
	 * The constructor for generate the smart environment model
	 * @param key The key corresponding to smool properties in what is
	 * contained the URL
	 */
	public SmartEnvironmentHandler(String key) {
		super(key);
	}
	
	public SmartEnvironmentHandler() {
		super();
	}

	/**
	 * Creates the smart environment model
	 */
	protected void read(Element rootElement) {
		model = new ArrayList<SmartEnvironment>();
		logger.debug("Reading contents for element root node " + rootElement.getNodeName());
		model.addAll(createSmartEnvironmentModel(rootElement, null));
	}
	
	/**
	 * Determines if the element is an smart environment
	 * @param elementName the element name
	 * @return <code>true</code> if the element is an smart environment
	 */
	private boolean isSmartEnvironment(String elementName) {
		for (int i=0; i < allowedNodes.length; i++) {
			if ( elementName.equals(allowedNodes[i]) ) {
				return true;
			}
	    }
		return false;
	}
	
	/**
	 * Creates the smart environment model based on the root Element
	 * obtained after processing the XML
	 * @param parent The parent node 
	 * @return The array list of smart environments
	 */
	private ArrayList<SmartEnvironment> createSmartEnvironmentModel(Object parent, SmartEnvironment parentEnvironment) {
		// Create a list of Smart Environments
		ArrayList<SmartEnvironment> list = new ArrayList<SmartEnvironment>();
		
		// Obtain the node list from the root
		Node parentNode = (Node) parent;
		
		NodeList nl = parentNode.getChildNodes();
		
		// Visit all the nodes
		for (int i = 0; i < nl.getLength(); i++) {
			// Obtain the node
			Node node = nl.item(i);
 		   
			int type = node.getNodeType();
			
			if (type == ELEMENT_TYPE && isSmartEnvironment(node.getNodeName())) { 
				// Gets all possible parameters
				String id = getSmartEnvironmentElement(node, ID_ELEMENT).trim();
				String name = getSmartEnvironmentElement(node, NAME_ELEMENT).trim();
				String description = getSmartEnvironmentElement(node, DESC_ELEMENT).trim();
				String ontology = getSmartEnvironmentElement(node, ONTOLOGY_ELEMENT).trim();
				String image = getSmartEnvironmentElement(node, IMAGE_ELEMENT).trim();
				List<Node> children = getSmartEnvironmentChildren(node);

				// Create a smart environment object and populate it
				SmartEnvironment smartEnvironment = new SmartEnvironment();
				
				// Set the smart environment parent
				if (parent != null) {
					smartEnvironment.setParent(parentEnvironment);
				}
				
				// Set the smart environment identifier
				if (id != null) {
					smartEnvironment.setId(id);
				} else {
					logger.error("There is no identifier for this smart space");
					break;
				}
				
				// Set the smart environment name
				if (name != null) {
					smartEnvironment.setName(name);
				}
				
				// Set the smart environment description
				if (description != null) {
					smartEnvironment.setDescription(description);
				}
				
				// Set the smart environment ontology model
				if (ontology != null) {
					try {
						//logger.debug("Reading ontology " + ontology + "...");
						InputStream in = ResourceUtil.getInputStream(ontology);

						if (in != null) {
							OntModel owlModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM);
							owlModel.read(in, null);
							smartEnvironment.setOntologyModel(owlModel);
						} else {
							logger.error("Cannot load ontology");
						}
					} catch (Exception ex) {
						logger.error("Cannot load ontology from " + ontology + ". Reason: " + ex.getMessage());
						ex.printStackTrace();
					}
				}
				
				// Set the smart environment referenced image
				if (image != null) {
					InputStream in = ResourceUtil.getInputStream(image);

					if (in != null) {
						smartEnvironment.setImage(new Image(null, in));
					} else {
						logger.error("Cannot load image");
					}
				}
				
				// And finally, add the list of submodels
				if (children.size() > 0) {
					for (Node child : children) {
						if (child.getNodeType() == ELEMENT_NODE) {
							ArrayList<SmartEnvironment> childModels = 
								createSmartEnvironmentModel(child, smartEnvironment);
							if (childModels.size() > 0) {
								smartEnvironment.addChildNodes(childModels);
							}
						}
					}
				}
				
				// Adds the Smart Environment to the list
				list.add(smartEnvironment);
				
			} else { // if it is not an smart environment node
	   		   	if (node.getNodeType() == ELEMENT_NODE) {
	   		   		list.addAll(createSmartEnvironmentModel(node, null));
	   		   	}			
	   		}
		}
		return list;
	}


	/**
	 * Gets the list of the available smart environments
	 * @return a collection of Smart Environments
	 */
	public Collection<SmartEnvironment> listSmartEnvironments() {
		//logger.debug("Getting the list of smart environments -"+model.size()+"-");
		return model;
	}
	
	/**
	 * Gets the element string
	 * @param node A node
	 * @param element The element to query
	 * @return A string with the element. <code>null</code> if not found
	 */
	private String getSmartEnvironmentElement(Node node, String element) {
		String result = null;

		try {
			// Get the child node list
			NodeList nl = node.getChildNodes();
			
			for (int i = 0; i < nl.getLength(); i++) {
				Node child = nl.item(i);
				if (child.getNodeType() == ELEMENT_NODE && child.getNodeName() == element) {
					result = child.getTextContent();
				}
			}
		} catch (Exception ex) {
			logger.error("There is an error obtaining the text: " + ex.toString());
			ex.printStackTrace();
		} 
		return result;
	}
	
	/**
	 * Gets the list of node elements
	 * @param node A node
	 * @return A string with the name. An empty list if not found
	 */
	private List<Node> getSmartEnvironmentChildren(Node node) {
		List<Node> children = new ArrayList<Node> ();
		
		NodeList nl = node.getChildNodes();
		
		for (int i = 0; i < nl.getLength(); i++) {
			// Obtain the node
			Node child = nl.item(i);
			children.add(child);
		}
		
		return children;
	}
}
