/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Alejandro Villamarin (Tecnalia Researh and Innovation - Software Systems Engineering) - bug fixing
 *******************************************************************************/ 

package org.smool.sdk.ui.wizard.model.pages.frames;

import java.net.URI;
import java.util.ArrayList;
import org.eclipse.jface.viewers.CheckboxTreeViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.smool.sdk.common.nls.SmoolMessages;
import org.smool.sdk.ui.wizard.model.data.SmartEnvironment;
import org.smool.sdk.ui.wizard.model.pages.tools.SmartEnvironmentTreeContentProvider;
import org.smool.sdk.ui.wizard.model.pages.tools.SmartEnvironmentTreeLabelProvider;


/**
 * A class that gets from an URI the available smart spaces and shows it
 * in a tree viewer.
 * 
 * @author Fran Ruiz (fran.ruiz@tecnalia.com), Alejandro Villamarin (alejandro.villamarin@tecnalia.com) Tecnalia
 */
public class SmartEnvironmentSelectionArea {

	/** Widget definition */
	private CheckboxTreeViewer treeViewer;
	private Label treeLabel;
	
	/** The radio buttons associated to the ontology selection */
	private Button smoolDefinedSmartEnvironment = null;
	private Button userDefinedSmartEnvironment = null;	
	
	/** The User Ontology Selection Area */
	private UserOntologySelectionArea ontologySelectionArea;
	
	private StyledText descriptionText;
	
     /**
      * Create a new instance of a SmartEnvironmentSelectionArea.
      * 
      * @param composite The composite
      */
     public SmartEnvironmentSelectionArea(Composite composite) {
         // If it is a new project always start enabled
         createContents(composite);
     }
 
     /**
      * Create the area containing the check box tree viewer containing
      * the smart environment.
      * 
      * @param composite The composite
      */
     private void createContents(Composite composite) {
    	 
    	 Group smartEnvGroup = new Group(composite, SWT.SHADOW_ETCHED_IN);
 		 smartEnvGroup.setText(SmoolMessages.SmartEnvironmentSelectionArea_groupTitle);    	 
 		 GridLayout viewsLayout = new GridLayout();

 		 viewsLayout.numColumns = 2;
 		 viewsLayout.makeColumnsEqualWidth = true;

 		 smartEnvGroup.setLayout(viewsLayout);
 		 smartEnvGroup.setLayoutData(new GridData(GridData.FILL_BOTH)); 		 
 		 
         GridData labelData = new GridData();
         labelData.horizontalSpan = 2;
        
         smoolDefinedSmartEnvironment = new Button(smartEnvGroup, SWT.RADIO);
         smoolDefinedSmartEnvironment.setText(SmoolMessages.SmartEnvironmentSelectionArea_defaultSmartEnvironment);
         smoolDefinedSmartEnvironment.setSelection(true);
         smoolDefinedSmartEnvironment.setLayoutData(labelData);
         
         labelData = new GridData();
         labelData.horizontalSpan = 2;
         
    	 // The project name label
 		 treeLabel = new Label(smartEnvGroup, SWT.NONE);
 		 treeLabel.setText(SmoolMessages.SmartEnvironmentSelectionArea_treeLabel);
 		 treeLabel.setFont(smartEnvGroup.getFont());
         treeLabel.setLayoutData(labelData);
         
   	     // Create the tree viewer to display the file tree
   	     treeViewer = new CheckboxTreeViewer(smartEnvGroup);
   	     treeViewer.getTree().setLayoutData(new GridData(GridData.FILL_BOTH));
   	     treeViewer.setContentProvider(new SmartEnvironmentTreeContentProvider());
   	     treeViewer.setLabelProvider(new SmartEnvironmentTreeLabelProvider());
   	     treeViewer.setInput("smool");
   	     
   	     
   	     treeViewer.expandAll();
   	     
   	     // The associated text that describes the selected smart environment
 		 descriptionText = new StyledText(smartEnvGroup, SWT.BORDER | SWT.WRAP | SWT.MULTI);
		 GridData gridData = new GridData();
		 gridData.horizontalAlignment = SWT.FILL;
		 gridData.grabExcessHorizontalSpace = true;
		 gridData.verticalAlignment = SWT.FILL;
		 gridData.grabExcessVerticalSpace = true;
		 descriptionText.setLayoutData(gridData);
		 descriptionText.setText("");
		 descriptionText.setEditable(false);
		 descriptionText.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
	     treeViewer.addSelectionChangedListener(new ISelectionChangedListener() {
	    	 
	    	 // The selected smart environment
	    	 SmartEnvironment smartEnvironment = null;
	    	 
	    	 public void selectionChanged(SelectionChangedEvent event) {
				IStructuredSelection selection = (IStructuredSelection) event.getSelection();
        
				Object selectedElement = selection.getFirstElement();
				if (selectedElement == null) {
					smartEnvironment = null;
					descriptionText.setText("");
				} else {
					smartEnvironment = (SmartEnvironment) selectedElement;
					StringBuffer sb = new StringBuffer();
					sb.append("Identifier: ");
					sb.append(smartEnvironment.getId());
					int idText = sb.length();
					sb.append("\n");
					sb.append("Name: ");
					sb.append(smartEnvironment.getName());
					sb.append("\n");
					sb.append("Description: ");
					sb.append(smartEnvironment.getDescription());
					sb.append("\n");
					descriptionText.setText(sb.toString());
					StyleRange styleRange = new StyleRange();
					styleRange.start = 0;
					styleRange.length = idText;
					styleRange.fontStyle = SWT.BOLD;
					styleRange.foreground = descriptionText.getDisplay().getSystemColor(SWT.COLOR_BLUE);
					descriptionText.setStyleRange(styleRange);
					
				}
			}
	     });
	     
         labelData = new GridData();
         labelData.horizontalSpan = 2;
	     
         userDefinedSmartEnvironment = new Button(smartEnvGroup, SWT.RADIO);
         userDefinedSmartEnvironment.setText(SmoolMessages.SmartEnvironmentSelectionArea_userSmartEnvironment);
         userDefinedSmartEnvironment.setLayoutData(labelData);
         ontologySelectionArea = new UserOntologySelectionArea(smartEnvGroup);          
     }

	/**
      * Activates the user defined smart environment
      */
     public void activateUserDefinedSmartEnvironment() {
    	 ontologySelectionArea.activate();
     }

     /**
      * Activates the SMOOL defined smart environments
      */
     public void activateSMOOLDefinedSmartEnvironment() {
    	 treeLabel.setEnabled(true);
    	 treeViewer.getControl().setEnabled(true);
    	 descriptionText.setEnabled(true);
     }     
     
 	/**
      * Activates the user defined smart environment
      */
     public void desactivateUserDefinedSmartEnvironment() {
    	 ontologySelectionArea.desactivate();
     }

     /**
      * Activates the SMOOL defined smart environments
      */
     public void desactivateSMOOLDefinedSmartEnvironment() {
    	 treeLabel.setEnabled(false);
    	 treeViewer.getControl().setEnabled(false);
    	 descriptionText.setEnabled(false);
     }        
     /**
      * Sets a listener to the CheckboxTreeViewer
      */
     public void setListener(ICheckStateListener listener) {
    	 treeViewer.addCheckStateListener(listener);
     }
     
     /**
      * Sets a listener to the User Selection
      */
     public void setUserSmartEnvironmentSelectionListener(SelectionListener listener) {
    	 userDefinedSmartEnvironment.addSelectionListener(listener);
     }
     
     /**
      * Sets a listener to the User Selection
      */
     public void setSMOOLSmartEnvironmentSelectionListener(SelectionListener listener) {
    	 smoolDefinedSmartEnvironment.addSelectionListener(listener);
     }     
     
 	/**
 	 * Checks the parent smart environments (if exists) 
 	 * @param element the selected element
 	 */
     public ArrayList<SmartEnvironment> setParentsChecked(Object element) {
 		 ArrayList<SmartEnvironment> smartEnvironments = new ArrayList<SmartEnvironment>();
    	 SmartEnvironment env = (SmartEnvironment) element;
    	 smartEnvironments.add(env);
    	 treeViewer.setSelection(new StructuredSelection(element), true);
    	 
    	 while(env.getParent() != null) {
    		 env = env.getParent();
        	 smartEnvironments.add(env);
    		 treeViewer.setGrayChecked(env, true);
    	 }
    	 
    	 return smartEnvironments;
 	}
 	
  	/**
  	 * Sets the parent smart environments (if exists) to unchecked 
  	 * @param element the selected element
  	 */
 	public ArrayList<SmartEnvironment> setParentsUnchecked(Object element) {
 		ArrayList<SmartEnvironment> smartEnvironments = new ArrayList<SmartEnvironment>();
 		
 		SmartEnvironment env = (SmartEnvironment) element;
 		smartEnvironments.add(env);
 		treeViewer.setSelection(new StructuredSelection(element), true);
 		boolean hasCheckedChildren = false;
 		
 		while(env.getParent() != null && !hasCheckedChildren) {
	 		
 			env = env.getParent();
 			for (SmartEnvironment se : env.getChildNodes()) {
	 			if (treeViewer.getChecked(se)) {
	 				hasCheckedChildren = true;
	 			}
	 		}
	 		
	 		if (!hasCheckedChildren) {
	 	 		smartEnvironments.add(env);
	 			treeViewer.setGrayChecked(env, false);
	 			
	 		}
 		}
 		
 		return smartEnvironments;
 	} 	

	public Object[] getCheckedElements() {
		return treeViewer.getCheckedElements();
	}

	public URI getUserSelection() {
		if (ontologySelectionArea.getOntologyLocationURI() == null) {
			return null;
		} else {
			return ontologySelectionArea.getOntologyLocationURI();
		}
	}

	public void setUserOntologySelectionListener(ModifyListener listener) {
		ontologySelectionArea.setListener(listener);
	}
}
