/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.wizard.model.data;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.log4j.Logger;

import com.hp.hpl.jena.ontology.OntModel;

/**
 * This class contains the information about the available smart environments.
 * This information will be used by the ADK Wizard to be displayed for the user
 * 
 * @author Fran Ruiz, fran.ruiz@tecnalia.com, ESI
 *
 */
public class SmartEnvironment extends TreeNodeModel {

	/** The identifier of Smart Environment */
	private String id = null;
	
	/** The description of the smart environment */
	private String description = null;
	
	/** The ontology content */
	private OntModel model = null;
	
	/** The list of sub-smart environments */
	private ArrayList<SmartEnvironment> children = new ArrayList<SmartEnvironment>();
	
	/** The parent smart environment */
	private SmartEnvironment parent = null;

	/** The logger */
	private static Logger logger = Logger.getLogger(SmartEnvironment.class);
	
	//private HashMap<String,ArrayList<String>> annotations = new HashMap<String,ArrayList<String>>();
	
	/**
	 * Set the identifier of this smart space
	 * @param id the identifier
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the prefix of the current smart environment
	 * @return the prefix
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * Gets the description to the current smart environment
	 * @return a string with description of the content of the smart environment 
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Sets the description associated to the current smart environment
	 * @param description a string indicating the description of the smart environment
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Gets the OntModel that represents the ontology
	 * @return a OntModel representing the ontology
	 */
	public OntModel getOntologyModel() {
		return model;
	}
	
//	/**
//	 * Gets the OntModel that represents the ontology
//	 * @return a OntModel representing the ontology
//	 */
//	public HashMap<String, ArrayList<String>> getAnnotations() {
//		return annotations;
//	}	
	
	/**
	 * Sets the ontology
	 * @param ontology a OntModel representing the ontology
	 */
	public void setOntologyModel(OntModel ontology) {
		if (ontology != null) {
			this.model = ontology;
			//this.includeAnnotationProperties();
		}
	}

	/**
	 * Gets the collection of the child nodes descending from the 
	 * current node
	 * @return a collection containing child nodes
	 */
	public Collection<SmartEnvironment> getChildNodes() {
		return this.children;
	}
	
//	/**
//	 * Includes the annotation properties associated to the ontology model
//	 */
//	private String includeAnnotationProperties() {
//		StringBuffer sb = new StringBuffer();
//		ExtendedIterator<Ontology> ontologies = this.model.listOntologies();
//		
//		while(ontologies.hasNext()) {
//			Ontology ontology = ontologies.next();
//			System.out.println("Reading ontology " + ontology.toString());
//			
//			ExtendedIterator<String> versionInfoProps = ontology.listVersionInfo();
//			while(versionInfoProps.hasNext()) {
//				if (annotations.get("versionInfo") != null) {
//					annotations.get("versionInfo").add(versionInfoProps.next());
//				} else {
//					annotations.put("versionInfo", new ArrayList<String>());
//					annotations.get("versionInfo").add(versionInfoProps.next());
//				}
//			}
//			
//			ExtendedIterator<RDFNode> commentProps = ontology.listComments(null);
//			while(versionInfoProps.hasNext()) {
//				if (annotations.get("comment") != null) {
//					annotations.get("comment").add(commentProps.next().toString());
//				} else {
//					annotations.put("comment", new ArrayList<String>());
//					annotations.get("comment").add(commentProps.next().toString());
//				}
//			}
//			
//		}
//		return sb.toString();
//	}
	
	/**
	 * Adds a child to the list of children of this node
	 * @param child the tree node to add as child of current node
	 */
	public boolean addChildNode(SmartEnvironment child) {
		
		if (!this.children.contains(child )) {
			this.children.add(child);
			return true;
		} else {
			logger.error("The node " + this.name + " already contains the child node " + child.getName());
			return false;
		}
	}
	
	/**
	 * Adds a child to the list of children of this node
	 * @param child the tree node to add as child of current node
	 */
	public boolean removeChildNode(SmartEnvironment child) {
		
		if (this.children.contains(child)) {
			this.children.remove(child);
			return true;	
		} else {
			logger.error("The node " + this.name + " does not contain the child node " + child.getName());
			return false;
		}
	}

	/**
	 * Gets the parent node
	 * @return the parent node
	 */
	public SmartEnvironment getParent() {
		return this.parent;
	}
	
	/**
	 * Sets the parent node
	 * @param parent the parent node
	 */
	public void setParent(SmartEnvironment parent) {
		this.parent = parent;
	}	

	/**
	 * Adds a child to the list of children of this node
	 * @param children the list of children
	 */
	public boolean addChildNodes(ArrayList<SmartEnvironment> children) {
		boolean alreadyInModel = false;
		for (SmartEnvironment child : children) {
			boolean valid = this.addChildNode(child);
			if (!valid) {
				alreadyInModel = true;
			}
		}
		
		return alreadyInModel;
	}	
	
}
