/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Alejandro Villamarin (Tecnalia Researh and Innovation - Software Systems Engineering) - bug fixing
 *******************************************************************************/ 

package org.smool.sdk.ui.wizard.model.pages;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaConventions;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTreeViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.PlatformUI;
import org.smool.sdk.common.nls.SmoolMessages;
import org.smool.sdk.common.properties.PropertyLoader;
import org.smool.sdk.ui.perspective.SmoolNature;
import org.smool.sdk.ui.wizard.model.ModelGeneratorWizard;
import org.smool.sdk.ui.wizard.model.data.SmartEnvironment;
import org.smool.sdk.ui.wizard.model.data.WizardData;
import org.smool.sdk.ui.wizard.model.pages.frames.ProjectHelper;
import org.smool.sdk.ui.wizard.model.pages.frames.ProjectSelectionDialog;
import org.smool.sdk.ui.wizard.model.pages.tools.SmartEnvironmentTreeContentProvider;
import org.smool.sdk.ui.wizard.model.pages.tools.SmartEnvironmentTreeLabelProvider;
import org.smool.sdk.ui.wizard.model.pages.util.OntologyReader;


/**
 * This class implements the UI page of the ADK Wizard where
 * we can determine the application domain of the Knowledge Processor
 * 
 * @author Fran Ruiz (fran.ruiz@tecnalia.com), Alejandro Villamarin (alejandro.villamarin@tecnalia.com) Tecnalia
 * @see org.eclipse.jface.wizard.WizardPage
 */
public class NewModelLayerPage extends WizardPage {

	/** The SMOOL project */
	private IProject smoolProject = null;
	
	/** Widgets: The package name field */
	private Text projectNameField = null;

	/** Widgets: The package name field */
	private Text packageNameField = null;
	
	/** Widgets: The project browse button */
	private Button projectBrowseButton = null;
	
	/** Widgets: The project execution context combo */
	private Combo executionContextCombo = null;
	
	/** Widgets: The context description label */
	private Label contextDescriptionLabel = null;

	
	/** The Text Field width */
	private static final int SIZING_TEXT_FIELD_WIDTH = 250;

	private static final String HELP_BUNDLE = "bundle.help";
	
	private static final String HELP_CONTENT_KEY = "newproject";

	/** The project selector dialog */
	private ProjectSelectionDialog projectSelectionDialog;
	
	private IStructuredSelection selection;
	
	/** The logger */
	private static Logger logger = Logger.getLogger(NewModelLayerPage.class);
	
	private Button smoolDefinedSmartEnvironment;
	private Button userDefinedSmartEnvironment;
	private Label treeLabel;
	private CheckboxTreeViewer treeViewer;
	private StyledText descriptionText;
	private Label userOntologyLabel;
	private Text ontologyPathField;
	private Button browseOntologyButton;
	

	/**
	 * Constructor for DomainPage.
	 */
	public NewModelLayerPage(String windowTitle) {
		super(windowTitle);
		setTitle(SmoolMessages.NewModelLayerPage_pageTitle);
		setDescription(SmoolMessages.NewModelLayerPage_pageDescription);
	}	
	
    public void setInitialSelection(IStructuredSelection initialSelection) {
        selection = initialSelection;
    }
    
	/**
	 * Creates the wizard page components
	 * @param parent the parent composite
	 */
	@Override
	public void createControl(Composite parent) {
		// Creates a new composite
		Composite container = new Composite(parent, SWT.NULL);
		container.setFont(parent.getFont());
		
		// Initializes the computation of horizontal and vertical 
		// dialog units based on the size of current font. 
		initializeDialogUnits(parent);
		
		String helpBundle = PropertyLoader.getInstance().getProperty(HELP_BUNDLE);
		StringBuffer helpContents = new StringBuffer();
		helpContents.append(helpBundle).append(".").append(HELP_CONTENT_KEY);
		
		// Help system
		PlatformUI.getWorkbench().getHelpSystem().setHelp(container, helpContents.toString());

		// Creates a new grid layout
		container.setLayout(new GridLayout());
		container.setLayoutData(new GridData(GridData.FILL_BOTH));
		 
		// Creates the project and package name group
		createProjectSelectionGroup(container);

		// Creates the properties group
		createProjectConfigurationGroup(container);
		
		// Creates the smart environment selection area group
		createSmartEnvironmentSelectionGroup(container);
		
		// ANM. Update information in the page
		updatePage();

		// Validates the page and sets the completeness of the page
		setPageComplete(validatePage());

		// inits from selection
		initializeFromSelection(selection);

        // Show description on opening
        setErrorMessage(null);
        setMessage(null);
        setControl(container);
	}

	private void updatePage() {
		WizardData d = WizardData.getInstance();
		
		if (d.getProject() != null && d.getProjectName() != null) {
			projectNameField.setText(d.getProjectName());
		}
		
		if (d.getPackageName() != null && d.getPackageName().length() > 0) {
			packageNameField.setText(d.getPackageName());
		}
		
		executionContextCombo.select(d.getContext());
		
		treeLabel.setEnabled(!d.usesUserDefinedOntology());
		treeViewer.getTree().setEnabled(!d.usesUserDefinedOntology());
		descriptionText.setEnabled(!d.usesUserDefinedOntology());
		userOntologyLabel.setEnabled(d.usesUserDefinedOntology());
		ontologyPathField.setEnabled(d.usesUserDefinedOntology());
		browseOntologyButton.setEnabled(d.usesUserDefinedOntology());
		
		if (d.usesUserDefinedOntology()) {
			smoolDefinedSmartEnvironment.setSelection(false);
			userDefinedSmartEnvironment.setSelection(true);
			ontologyPathField.setText(WizardData.getInstance().getUserOntologyPath());
		}
		else {
			smoolDefinedSmartEnvironment.setSelection(true);
			userDefinedSmartEnvironment.setSelection(false);
			
			for (TreeItem ti : treeViewer.getTree().getItems()) {
				if (WizardData.getInstance().getSelectedOntologies().get(((SmartEnvironment)ti.getData()).getId()) != null) {
					ti.setChecked(true);
				}
				else {
					ti.setChecked(false);
				}
			}
		}
		
		
	}

	/**
	 * Creates the SMOOL project name and package name specification controls.
	 * @param parent the parent composite
	 */
	private final void createProjectSelectionGroup(Composite parent) {
		// project specification group
		Composite projectSelectionGroup = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		projectSelectionGroup.setLayout(layout);
		projectSelectionGroup.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		// The project name label
		Label projectSelectionLabel = new Label(projectSelectionGroup, SWT.NONE);
		projectSelectionLabel.setText(SmoolMessages.NewModelLayerPage_projectLabel);
		projectSelectionLabel.setFont(parent.getFont());
	
		// KP project name text field
		// ANM. Changed this field to non-editable
		projectNameField = new Text(projectSelectionGroup, SWT.BORDER);
		GridData data = new GridData(GridData.FILL_HORIZONTAL);
		data.widthHint = SIZING_TEXT_FIELD_WIDTH;
		projectNameField.setLayoutData(data);
		projectNameField.setFont(parent.getFont());
		//projectNameField.addListener(SWT.Modify, projectNameModifyListener);
		projectNameField.setEditable(false);

		projectBrowseButton = new Button(projectSelectionGroup, SWT.NONE);
		projectBrowseButton.setText(SmoolMessages.NewModelLayerPage_projectBrowseLabel);
		projectBrowseButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				openProjectSelectionDialog();
				setPageComplete(validatePage());
			}
		});
		projectSelectionDialog = new ProjectSelectionDialog(parent.getShell());
		
		Label packageLabel = new Label(projectSelectionGroup, SWT.NONE);
		packageLabel.setText(SmoolMessages.NewModelLayerPage_packageLabel);
        
		this.packageNameField = new Text(projectSelectionGroup, SWT.BORDER);
		GridData packageData = new GridData(GridData.FILL_HORIZONTAL);
		packageData.horizontalSpan = 2;
        packageNameField.setLayoutData(packageData);
		packageNameField.setFont(parent.getFont());
		packageNameField.addModifyListener(new ModifyListener() {
			
			@Override
			public void modifyText(ModifyEvent e) {
				WizardData.getInstance().setPackageName(((Text)e.widget).getText());
				setPageComplete(validatePage());
			}
		});	
			
	}

	/**
	 * Opens the selection dialog for the project selection
	 */
	private final void openProjectSelectionDialog() {
		IJavaProject project = projectSelectionDialog.selectSMOOLProject(projectNameField.getText());
	
		if (project != null) {
			// ANM. Corrected a bug with the cache
			smoolProject = project.getProject();
			WizardData cacheData = WizardData.getInstance();
			cacheData.setProject(smoolProject);
			cacheData.setProjectName(smoolProject.getName());
			projectNameField.setText(smoolProject.getName());
		}
	}
	
	/**
	 * Creates the SMOOL project configuration options controls.
	 * @param parent the parent composite
	 */
	private final void createProjectConfigurationGroup(Composite parent) {
		int columns = 2;
		
		// project specification group
		Group projectConfigurationGroup = new Group(parent, SWT.NONE);
		projectConfigurationGroup.setText(SmoolMessages.NewModelLayerPage_configurationGroupTitle);    	 

		GridLayout layout = new GridLayout();
		layout.numColumns = columns;
		projectConfigurationGroup.setLayout(layout);
		projectConfigurationGroup.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		// The execution context label
		Label executionContextLabel = new Label(projectConfigurationGroup, SWT.NONE);
		executionContextLabel.setText(SmoolMessages.NewModelLayerPage_executionContextLabel);
		executionContextLabel.setFont(parent.getFont());
	
		// Creation of the context combo
		executionContextCombo = new Combo(projectConfigurationGroup, SWT.READ_ONLY);
		GridData data = new GridData(GridData.FILL_HORIZONTAL);
		//data.widthHint = SIZING_TEXT_FIELD_WIDTH;
		executionContextCombo.setLayoutData(data);
		executionContextCombo.setFont(parent.getFont());
		
		// Add the options to the combo
		executionContextCombo.add(SmoolMessages.NewModelLayerPage_executionContextLabel_PROSUMER, 0);
		executionContextCombo.add(SmoolMessages.NewModelLayerPage_executionContextLabel_PRODUCER, 1);
		executionContextCombo.add(SmoolMessages.NewModelLayerPage_executionContextLabel_CONSUMER, 2);
		
		//executionContextCombo.addListener(SWT.Modify, projectContextModifyListener);
		executionContextCombo.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				// Clear the concept cache if context is changed
				if (WizardData.getInstance().getContext() != executionContextCombo.getSelectionIndex()) {
					WizardData.getInstance().getConsumerOntologyConcepts().clear();
					WizardData.getInstance().getProducerOntologyConcepts().clear();
				}
				
				WizardData.getInstance().setContext(executionContextCombo.getSelectionIndex());
				switch (executionContextCombo.getSelectionIndex()) {
				case 0:
					contextDescriptionLabel.setText(SmoolMessages.NewModelLayerPage_executionContextLabel_PROSUMER_desc);
					break;
				case 1:
					contextDescriptionLabel.setText(SmoolMessages.NewModelLayerPage_executionContextLabel_PRODUCER_desc);
					break;
				case 2:
					contextDescriptionLabel.setText(SmoolMessages.NewModelLayerPage_executionContextLabel_CONSUMER_desc);
					break;
				}
				setPageComplete(validatePage());
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			
			}
		});
		
		
		contextDescriptionLabel = new Label(projectConfigurationGroup, SWT.NONE);
		contextDescriptionLabel.setText(SmoolMessages.NewModelLayerPage_executionContextLabel_PROSUMER_desc);
		
		GridData contextDescriptionData = new GridData();
		contextDescriptionData.horizontalSpan = columns;
		contextDescriptionLabel.setLayoutData(contextDescriptionData);		
		
	}	
	
	/**
	 * Creates the SMOOL project location group controls.
	 * Due to the project location has been included in the frames package,
	 * it will create only the corresponding areas
	 * 
	 * @param parent the parent composite
	 */
	private final void createSmartEnvironmentSelectionGroup(Composite parent) {
		// Creates the smart environment selection area
		Group smartEnvGroup = new Group(parent, SWT.SHADOW_ETCHED_IN);
		smartEnvGroup.setText(SmoolMessages.SmartEnvironmentSelectionArea_groupTitle);    	 
		GridLayout viewsLayout = new GridLayout();

		viewsLayout.numColumns = 2;
		viewsLayout.makeColumnsEqualWidth = true;

		smartEnvGroup.setLayout(viewsLayout);
		smartEnvGroup.setLayoutData(new GridData(GridData.FILL_BOTH)); 		 

		GridData labelData = new GridData();
		labelData.horizontalSpan = 2;

		smoolDefinedSmartEnvironment = new Button(smartEnvGroup, SWT.RADIO);
		smoolDefinedSmartEnvironment.setText(SmoolMessages.SmartEnvironmentSelectionArea_defaultSmartEnvironment);
		//sofiaDefinedSmartEnvironment.setSelection(true);
		smoolDefinedSmartEnvironment.setLayoutData(labelData);
		smoolDefinedSmartEnvironment.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				// Clear the concept cache if we're switching mode
				if (((Button)e.widget).getSelection() == WizardData.getInstance().usesUserDefinedOntology()) {
					WizardData.getInstance().getConsumerOntologyConcepts().clear();
					WizardData.getInstance().getProducerOntologyConcepts().clear();
				}
				
				treeLabel.setEnabled(((Button)e.widget).getSelection());
				treeViewer.getTree().setEnabled(((Button)e.widget).getSelection());
				descriptionText.setEnabled(((Button)e.widget).getSelection());
				descriptionText.setText("");
				WizardData.getInstance().setUsesUserDefinedOntology(!((Button)e.widget).getSelection());
				
				// Remove all ontologies and reload any ontologies checked in the Tree
				WizardData.getInstance().getSelectedOntologies().clear();
				for (Object se : treeViewer.getCheckedElements()) {
					WizardData.getInstance().addSmartEnvironment((SmartEnvironment) se);
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// Not used
			}
		});

		labelData = new GridData();
		labelData.horizontalSpan = 2;

		// The project name label
		treeLabel = new Label(smartEnvGroup, SWT.NONE);
		treeLabel.setText(SmoolMessages.SmartEnvironmentSelectionArea_treeLabel);
		treeLabel.setFont(smartEnvGroup.getFont());
		treeLabel.setLayoutData(labelData);

		// Create the tree viewer to display the file tree
		treeViewer = new CheckboxTreeViewer(smartEnvGroup);
		treeViewer.getTree().setLayoutData(new GridData(GridData.FILL_BOTH));
		treeViewer.setContentProvider(new SmartEnvironmentTreeContentProvider());
		treeViewer.setLabelProvider(new SmartEnvironmentTreeLabelProvider());
		treeViewer.setInput("smool");


		treeViewer.expandAll();

		// The associated text that describes the selected smart environment
		descriptionText = new StyledText(smartEnvGroup, SWT.BORDER | SWT.WRAP | SWT.MULTI);
		GridData gridData = new GridData();
		gridData.horizontalAlignment = SWT.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.verticalAlignment = SWT.FILL;
		gridData.grabExcessVerticalSpace = true;
		descriptionText.setLayoutData(gridData);
		descriptionText.setText("");
		descriptionText.setEditable(false);
		descriptionText.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		
		
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			// The selected smart environment
			SmartEnvironment smartEnvironment = null;

			public void selectionChanged(SelectionChangedEvent event) {
				IStructuredSelection selection = (IStructuredSelection) event.getSelection();

				Object selectedElement = selection.getFirstElement();
				if (selectedElement == null || !(selectedElement instanceof SmartEnvironment)) {
					smartEnvironment = null;
					descriptionText.setText("");
				} else {
					smartEnvironment = (SmartEnvironment) selectedElement;
					StringBuffer sb = new StringBuffer();
					sb.append("Identifier: ");
					sb.append(smartEnvironment.getId());
					int idText = sb.length();
					sb.append("\n");
					sb.append("Name: ");
					sb.append(smartEnvironment.getName());
					sb.append("\n");
					sb.append("Description: ");
					sb.append(smartEnvironment.getDescription());
					sb.append("\n");
					descriptionText.setText(sb.toString());
					StyleRange styleRange = new StyleRange();
					styleRange.start = 0;
					styleRange.length = idText;
					styleRange.fontStyle = SWT.BOLD;
					styleRange.foreground = descriptionText.getDisplay().getSystemColor(SWT.COLOR_BLUE);
					descriptionText.setStyleRange(styleRange);

				}
			}
		});
		
		treeViewer.addCheckStateListener(new ICheckStateListener() {
			
			@Override
			public void checkStateChanged(CheckStateChangedEvent event) {
				// The user has checked some smart environment
				if (event.getChecked()) {
					// When the user checks the smart environment
					// it is checked the parents and added the smart environment
					// to the data model
					SmartEnvironment env = (SmartEnvironment) event.getElement();
					ArrayList<SmartEnvironment> smartEnvironments = new ArrayList<SmartEnvironment>();
					smartEnvironments.add(env);
					while (env.getParent() != null) {
						env = env.getParent();
						smartEnvironments.add(env);
						treeViewer.setGrayChecked(env, true);
					}
					WizardData.getInstance().addSmartEnvironments(smartEnvironments);   		
				} 
				else { // the item is removed
					SmartEnvironment env = (SmartEnvironment) event.getElement();
					ArrayList<SmartEnvironment> smartEnvironments = new ArrayList<SmartEnvironment>();
					
					smartEnvironments.add(env);
					boolean hasCheckedChildren = false;
					
					while(env.getParent() != null && !hasCheckedChildren) {

						env = env.getParent();
						for (SmartEnvironment se : env.getChildNodes()) {
							if (treeViewer.getChecked(se)) {
								hasCheckedChildren = true;
							}
						}

						if (!hasCheckedChildren) {
							smartEnvironments.add(env);
							treeViewer.setGrayChecked(env, false);

						}
					}
					
					WizardData.getInstance().removeSmartEnvironments(smartEnvironments);
				}

				// check the validity of the page content
				setPageComplete(validatePage());
			}
		});

		labelData = new GridData();
		labelData.horizontalSpan = 2;

		userDefinedSmartEnvironment = new Button(smartEnvGroup, SWT.RADIO);
		userDefinedSmartEnvironment.setText(SmoolMessages.SmartEnvironmentSelectionArea_userSmartEnvironment);
		//       userDefinedSmartEnvironment.setSelection(false);
		userDefinedSmartEnvironment.setLayoutData(labelData);
		userDefinedSmartEnvironment.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				userOntologyLabel.setEnabled(((Button)e.widget).getSelection());
				ontologyPathField.setEnabled(((Button)e.widget).getSelection());
				browseOntologyButton.setEnabled(((Button)e.widget).getSelection());
				WizardData.getInstance().setUsesUserDefinedOntology(((Button)e.widget).getSelection());
				
				// Delete the stored ontologies and load the user defined one (if there is one!)
				WizardData.getInstance().getSelectedOntologies().clear();
				if (WizardData.getInstance().getUserOntologyPath() != null) {
					SmartEnvironment se = loadOntology(WizardData.getInstance().getUserOntologyPath());
					if (se != null) {
						WizardData.getInstance().addSmartEnvironment(se);
					}
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// Not used
				
			}
		});


		Composite container = new Composite(smartEnvGroup, SWT.NULL);

		// In this frame we have to establish 3 columns:
		// One for the ontology label
		// Two for the text field
		// One for the browse button
		int columns = 4;

		// project location group
		//Composite location = new Composite(locationGroup, SWT.NONE);
		GridLayout layout = new GridLayout();

		layout.numColumns = columns;

		// make container wider
		GridData containerData = new GridData(GridData.FILL_HORIZONTAL);
		containerData.horizontalSpan = 2;    	 

		container.setLayout(layout);
		container.setLayoutData(containerData); 

		// Location path label
		userOntologyLabel = new Label(container, SWT.NONE);
		userOntologyLabel.setText(SmoolMessages.UserOntologySelectionArea_ontologyLabel);

		// project location path text field
		ontologyPathField = new Text(container, SWT.BORDER);
		GridData data = new GridData(GridData.FILL_HORIZONTAL);
		data.widthHint = SIZING_TEXT_FIELD_WIDTH;
		data.horizontalSpan = 2;
		ontologyPathField.setLayoutData(data);
		
		// Adds the browse button to locate the directory
		browseOntologyButton = new Button(container, SWT.PUSH);
		browseOntologyButton.setText(SmoolMessages.UserOntologySelectionArea_browseLabel);

		// Adds a listener for the button (action associated when clicked)
		browseOntologyButton.addSelectionListener(new SelectionAdapter() {
			// Throw the folder selection dialog
			public void widgetSelected(SelectionEvent event) {
				String selectedFile = null;

				// Create a directory dialog
				FileDialog dialog = new FileDialog(
						ontologyPathField.getShell());

				// Sets a message to the directory
				dialog.setText(SmoolMessages.UserOntologySelectionArea_fileDialogOntology);

				dialog.setFilterPath(System.getProperty("user.home"));

				String[] filterExt = { "*.owl", "*.rdf", ".n3", "*.*" };
				dialog.setFilterExtensions(filterExt);

				// If the directory name is null
				if (!ontologyPathField.getText().equals("")) {
					File path = new File(ontologyPathField.getText());
					if (path.exists() && path.isDirectory()) {
						dialog.setFilterPath((new Path(path.getAbsolutePath())).toOSString());
					}
				}

				selectedFile = dialog.open();

				if (selectedFile != null) {
					ontologyPathField.setText(selectedFile);
					WizardData.getInstance().setUserOntologyPath(selectedFile);

					SmartEnvironment se = loadOntology(selectedFile);
					
					if (se != null) {
						WizardData.getInstance().getSelectedOntologies().clear();
						WizardData.getInstance().getConsumerOntologyConcepts().clear();
						WizardData.getInstance().getProducerOntologyConcepts().clear();
						WizardData.getInstance().addSmartEnvironment(se);
					}
					
					setPageComplete(validatePage());
				}
			}
		});

	}
	
	private SmartEnvironment loadOntology(String path) {
		SmartEnvironment res = null;
		URI pathURI = null;

		try {
			if (ontologyPathField == null || ontologyPathField.getText().equals("")) {
				pathURI = null;
			} else {
				File file = new File(ontologyPathField.getText());
				if (file.isDirectory()) {
					pathURI = null;
				} else {
					pathURI = file.toURI();
				}
			}
		} catch (Exception ex) {
			System.out.println(ex.toString());
			return null;
		}
		
		if (pathURI != null) {
			OntologyReader reader = new OntologyReader(pathURI);
			res = reader.getSmartEnvironment();
		}
		
		return res;
	}
		
   /**
    * Returns whether this page's controls currently all contain valid 
    * values.
    *
    * @return <code>true</code> if all controls are valid, and
    * <code>false</code> if at least one is invalid
    */
	protected boolean validatePage() {
		// sets the error messages to null
    	setErrorMessage(null);
    	 
    	// validates the project reference
    	if (!validateProject()) {
    		return false;
    	}
    	 
    	// validates the package name
    	if (!validatePackageName()) {
    		return false;
    	}
    	 
    	// validates the smart environment selection
    	if (!validateSmartEnvironmentSelection()) {
    		return false;
    	}
    	 
        // if the validation is correct, then return the default message
        setMessage(SmoolMessages.NewModelLayerPage_validationDefaultMessage);
    	 
        return true;
    }
     
   /**
    * Validates the project reference
    * @return <code>true</code> if the user has specified some value in the project name
    */
    private boolean validateProject() {

    	// Obtains the current workspace
    	IWorkspace workspace = ResourcesPlugin.getWorkspace();
 
        String projectFieldContents = getProjectNameFieldValue();
        if (projectFieldContents.equals("")) { 
        	setErrorMessage(null);
            setMessage(SmoolMessages.NewModelLayerPage_validationProjectNotSpecified);
            return false;
        }
 
        IStatus nameStatus = workspace.validateName(projectFieldContents,
        		IResource.PROJECT);
        if (!nameStatus.isOK()) {
        	setErrorMessage(nameStatus.getMessage());
            return false;
        }

        smoolProject = workspace.getRoot().getProject(projectFieldContents);
        if (!smoolProject.exists()) {
        	setErrorMessage(SmoolMessages.NewModelLayerPage_errorProjectNotExist);
            return false;
        }
        
        IJavaProject project;
		try {
			project = ProjectHelper.getSMOOLProject(smoolProject);
	        if (project == null || !project.exists()) {
	        	setErrorMessage("The project is not a Smool project");
	        	return false;
	        }
		} catch (CoreException e) {
        	setErrorMessage("The project is not a Smool project - core exception");
			e.printStackTrace();
			return false;
		}
        
        return true;
     }

     /**
      * Validates the package name
      * @return <code>true</code> if the user has specified some value in the package name
      */
     private boolean validatePackageName() {
 
         String packageFieldContents = getPackageNameFieldValue();
         if (packageFieldContents.equals("")) { 
        	 setErrorMessage(null);
             setMessage(SmoolMessages.NewModelLayerPage_validationPackageNotSpecified);
             return false;
         }
 
         IStatus nameStatus = JavaConventions.validatePackageName(packageFieldContents,
        		 JavaCore.VERSION_1_3, JavaCore.VERSION_1_3); 
         if (!nameStatus.isOK()) {
        	 String message = SmoolMessages.NewModelLayerPage_errorInvalidPackageIdentifier + nameStatus.getMessage();
             setErrorMessage(message);
             return false;
         }
         return true;
     }

    /**
     * Validates the smart environment selection
     * @return <code>true</code> if the user has selected some smart environment
     */
     private boolean validateSmartEnvironmentSelection() {
    	 if (!WizardData.getInstance().hasLoadedOntologyModel()) { 
        	 setErrorMessage(null);
        	 setMessage(SmoolMessages.NewModelLayerPage_validationSmartEnvironmentNotSelected);
             return false;
         }
 
         return true;
     }
     
     /**
      * Returns the value of the project name field
      * with leading and trailing spaces removed.
      * 
      * @return the project name in the field
      */
     private String getProjectNameFieldValue() {
         if (projectNameField == null) {
             return "";
         } else {
        	 return projectNameField.getText().trim();
         }
     }
     
    /**
     * Returns the value of the project name field
     * with leading and trailing spaces removed.
     * 
     * @return the project name in the field
     */
     private String getPackageNameFieldValue() {
    	 if (packageNameField == null) {
             return "";
         } else {
        	 return packageNameField.getText().trim();
         }
     }
     
    /**
     * Creates a project resource handle for the current project name field
     * value. The project handle is created relative to the workspace root.
     * <p>
     * This method does not create the project resource; this is the
     * responsibility of <code>IProject::create</code> invoked by the new
     * project resource wizard.
     * </p>
     * 
     * @return the new project resource handle
     */
     public IProject getProjectHandle() {
         return smoolProject;
     }

    /**
     * Returns the current project name as entered by the user, or its anticipated
     * initial value.
     *
     * @return the project name, its anticipated initial value, or <code>null</code>
     * if no project name is known
     */
    public String getPackageName() {
        return getPackageNameFieldValue();
    }  
    
    /**
     * Gets the execution context.
     */
    public int getContext() {
        return executionContextCombo.getSelectionIndex();
    }      

	/**
	 * Implements the getNextPage method to obtain the following page in the
	 * wizard and update the ontology references
	 */
    @Override
	public IWizardPage getNextPage(){

    	if (isPageComplete()) {
    		WizardData.getInstance().createOntologyModel(null);
    		if (WizardData.getInstance().getContext() == 0 ||
    				WizardData.getInstance().getContext() == 1) {
    			OntologyConceptSelectionPage page = ((ModelGeneratorWizard)getWizard()).getOntologyProducerWizardPage();
    			page.updatePage();
    			OntologyConceptSelectionPage consumerPage = ((ModelGeneratorWizard)getWizard()).getOntologyConsumerWizardPage();
    			consumerPage.updatePage();
    			return page;
    		} else if (WizardData.getInstance().getContext() == 2) {
    			OntologyConceptSelectionPage page = ((ModelGeneratorWizard)getWizard()).getOntologyConsumerWizardPage();
    			page.updatePage();
    			return page;
    		} else {
    			return null;
    		}
    	}
    	else {
    		return null;
    	}
	}

    /**
     * Inits the Wizard Page from a selection
     * 
     * @param selection The selection when the wizard was initiated.
     */
    private void initializeFromSelection(IStructuredSelection selection) {
        if (selection == null) {
            return;
        }
        
        IProject targetProject = null;
        String targetPackage = null;
        int targetScore = 0;
        
        for (Object element : selection.toList()) {
            if (element instanceof IAdaptable) {
                IResource res = (IResource) ((IAdaptable) element).getAdapter(IResource.class);
                IProject project = res != null ? res.getProject() : null;
                
                // Is this a Smool project?
                try {
                    if (project == null || !project.hasNature(SmoolNature.NATURE_ID)) {
                        continue;
                    } else {
                    	targetProject = project;
                    	targetScore++;
                    }
					if (project.hasNature(JavaCore.NATURE_ID)) { // check if it is a Java Project
						IFolder folder = project.getFolder("src");
						
						IJavaProject javaProject = JavaCore.create(project);

						IPackageFragmentRoot root = javaProject.getPackageFragmentRoot(folder);
						
						IJavaElement[] elements = root.getChildren();
						
						for (IJavaElement javaElement : elements) {
							if (javaElement instanceof IPackageFragment) {
								IPackageFragment packageFragment = (IPackageFragment) javaElement;
								String packageFragmentName = packageFragment.getElementName();
								if (packageFragmentName.contains(".model")) {
									String basePackageName = packageFragmentName.substring(0, packageFragmentName.indexOf(".model"));
									if (targetPackage != null) {
										if (!targetPackage.equals(basePackageName)) {
											logger.error("More than one base package name: " + basePackageName);
										}
									} else {
										//logger.debug("Setting the base package to " + basePackageName);
										targetPackage = basePackageName;
									}
								}
							}
						}
					}
				} catch (JavaModelException e) {
					continue;
				} catch (CoreException e) {
					continue;
				}
            }
        }
        
        // Now set the UI accordingly
        if (targetScore > 0) {
        	if (targetProject != null) {
	        	String projectName = targetProject.getName();
	        	this.smoolProject = targetProject;
	        	WizardData.getInstance().setProject(targetProject);
	            projectNameField.setText(projectName);
	            if (targetPackage != null) {
	            	// Notice that here we first check whether we have some values within
	            	// the WizardData (cache), otherwise we keep the original
	            	WizardData data = WizardData.getInstance();
	            	if (data.getPackageName() != null){
	            		packageNameField.setText(data.getPackageName());} 
	            	else{ 
	            		packageNameField.setText("");  
	            	}
	            }
        	}
        }
    }


}
