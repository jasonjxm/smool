/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.wizard.model.pages.frames;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.core.runtime.Path;
import org.eclipse.osgi.util.TextProcessor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.smool.sdk.common.nls.SmoolMessages;
import org.smool.sdk.ui.wizard.model.data.WizardData;


/**
 * A class that implements the frame where an ontology location
 * is set.
 *  
 * @author Fran Ruiz, Fran.Ruiz@tecnalia.com, Tecnalia
 *
 */
public class UserOntologySelectionArea {

	/** Constants definition */
	private static final int SIZING_TEXT_FIELD_WIDTH = 250;

	/** Widget definition */
    private Label userOntologyLabel;
    private Text ontologyPathField;
    private Button browseButton;
    
  
     /**
      * Create a new instance of a ProjectContentsLocationArea.
      * @param composite The composite
      */
     public UserOntologySelectionArea(Composite composite) {
    	 // If it is a new project always start enabled
         createContents(composite);
     }
 
     /**
      * Create the project location components.
      * 
      * @param composite The composite
      * @param defaultEnabled Determines if the 'Use default location' check 
      * is enabled by default 
      */
     private void createContents(Composite composite) {
 
         createUserEntryArea(composite);
		 // sets the user area enable in function of the value of defaultEnabled
         setUserAreaEnabled(false);

     }
 
     /**
      * Create the project location input components: location path, browse, ...
      * 
      * @param composite The composite
      * @param enabled the initial enabled state of the widgets created 
      */
     private void createUserEntryArea(Composite composite) {
    	 
    	 Composite container = new Composite(composite, SWT.NULL);
    	 // In this frame we have to establish 3 columns:
    	 // One for the ontology label
    	 // Two for the text field
    	 // One for the browse button
    	 int columns = 4;
 
         // project location group
    	 //Composite location = new Composite(locationGroup, SWT.NONE);
    	 GridLayout layout = new GridLayout();

    	 layout.numColumns = columns;

    	 // make container wider
    	 GridData containerData = new GridData(GridData.FILL_HORIZONTAL);
    	 containerData.horizontalSpan = 2;    	 
    	 
    	 container.setLayout(layout);
      	 container.setLayoutData(containerData); 
    	 
    	 // Location path label
    	 userOntologyLabel = new Label(container, SWT.NONE);
    	 userOntologyLabel.setText(SmoolMessages.UserOntologySelectionArea_ontologyLabel);
 
         // project location path text field
    	 ontologyPathField = new Text(container, SWT.BORDER);
         GridData data = new GridData(GridData.FILL_HORIZONTAL);
         data.widthHint = SIZING_TEXT_FIELD_WIDTH;
         data.horizontalSpan = 2;
         ontologyPathField.setLayoutData(data);
         WizardData cacheData = WizardData.getInstance();       
         if (cacheData.getUserOntologyPath() != null) {
        	 ontologyPathField.setText(cacheData.getUserOntologyPath());
         }
         // Adds the browse button to locate the directory
         browseButton = new Button(container, SWT.PUSH);
         browseButton.setText(SmoolMessages.UserOntologySelectionArea_browseLabel);
         
         // Adds a listener for the button (action associated when clicked)
         browseButton.addSelectionListener(new SelectionAdapter() {
             // Throw the folder selection dialog
        	 public void widgetSelected(SelectionEvent event) {
                 handleOntologyBrowseButtonPressed();
             }
         });
     }
     
     /**
      * Associates the listener to the ontology path field
      * @param listener a modify listener
      */
     public void setListener (ModifyListener listener) {
    	 ontologyPathField.addModifyListener(listener);
     }
 
     /**
      * Sets if the path area is enabled.
      * 
      * @param enabled A boolean value indicating the enable of the user input area
      */
     private void setUserAreaEnabled(boolean enabled) {
 
    	 // Sets enable fields
    	 userOntologyLabel.setEnabled(enabled);
    	 ontologyPathField.setEnabled(enabled);
         browseButton.setEnabled(enabled);
     }
 
     /**
      * Return the browse button. Usually referenced in order to set the layout
      * data for a dialog.
      * 
      * @return Button The button component
      */
     public Button getBrowseButton() {
         return browseButton;
     }
 
     /**
      * Open an appropriate directory browser
      */
     private void handleOntologyBrowseButtonPressed() {

    	 // Variables
    	 String selectedFile = null;
         String dirName = getPathFromOntologyField();
    	 
         // Create a directory dialog
         FileDialog dialog = new FileDialog(
        		 ontologyPathField.getShell());
    	 
         // Sets a message to the directory
         dialog.setText(SmoolMessages.UserOntologySelectionArea_fileDialogOntology);
         
         dialog.setFilterPath(System.getProperty("user.home"));
         
         String[] filterExt = { "*.owl", "*.rdf", ".n3", "*.*" };
         dialog.setFilterExtensions(filterExt);
 
         // If the directory name is null
         if (!dirName.equals("")) {
			 File path = new File(dirName);
			 if (path.exists() && path.isDirectory()) {
				 dialog.setFilterPath((new Path(path.getAbsolutePath())).toOSString());
			 }
         }
         
         dialog.setFilterPath(dirName);
 
         selectedFile = dialog.open();

         if (selectedFile != null) {

             updateOntologyField(selectedFile);
         }
     }
 
     /**
      * Update the location path field based on the selected path.
      * 
      * @param selectedPath The selected path
      */
     private void updateOntologyField(String selectedOntology) {
         ontologyPathField.setText(TextProcessor.process(selectedOntology));
         WizardData.getInstance().setUserOntologyPath(selectedOntology); 
     }
 
     /**
      * Return the path on the location field.
      * 
      * @return String
      */
     private String getPathFromOntologyField() {
    	 // A URI containing the path selection
         URI fieldURI;
         try {
        	 if (ontologyPathField != null) {
        		 fieldURI = new URI (ontologyPathField.getText());
        	 } else {
        		 return null;
        	 }
         } catch (URISyntaxException  e) {
             return ontologyPathField.getText();
         }
         return fieldURI.getPath();
     }
 
     /**
      * Check if the entry in the widget location is valid. 
      * If it is valid return null. 
      * Otherwise return a string that indicates the problem.
      * 
      * @return String containing the error if the location cannot be reached or null if the location is correct.
      */
     public String checkValidLocation() {
 
         // Checks if the text field is empty
         String ontologyFieldContents = ontologyPathField.getText();
         if (ontologyFieldContents.length() == 0) {
             return (SmoolMessages.ProjectSelectionArea_validationLocationNotSpecified);
         }
 
         // The selected path
         URI newPath = getOntologyLocationURI();
         if (newPath == null) {
             return (SmoolMessages.UserOntologySelectionArea_errorInvalidOntology);
         }
 
         return null;
     }
 
     /**
      * Get the URI for the location field if possible.
      * 
      * @return URI or <code>null</code> if it is not valid.
      */
     public URI getOntologyLocationURI() {
 
    	 URI pathURI = null;
    	 
    	 try {
    		 if (ontologyPathField == null || ontologyPathField.getText().equals("")) {
    			 pathURI = null;
    		 } else {
    			 File file = new File(ontologyPathField.getText());
	    		 if (file.isDirectory()) {
	    			 pathURI = null;
	    		 } else {
	    			 pathURI = file.toURI();
	    		 }
    		 }
    	 } catch (Exception ex) {
    		 System.out.println(ex.toString());
    	 }
    	 
    	 return pathURI;
     }

	public void activate() {
		setUserAreaEnabled(true);
	}
     
	public void desactivate() {
		setUserAreaEnabled(false);
	}
	
	
}
