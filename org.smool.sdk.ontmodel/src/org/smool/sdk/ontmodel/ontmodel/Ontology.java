/**
 */
package org.smool.sdk.ontmodel.ontmodel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ontology</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.Ontology#getURI <em>URI</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.Ontology#getNameSpace <em>Name Space</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.Ontology#getClasses <em>Classes</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.Ontology#getProjectInfo <em>Project Info</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.Ontology#getImportedElements <em>Imported Elements</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.Ontology#getUnprocessedProperties <em>Unprocessed Properties</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.Ontology#getUnprocessableProperties <em>Unprocessable Properties</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getOntology()
 * @model annotation="gmf.diagram model.extension='ontmodel' diagram.extension='ontmodeldi'"
 * @generated
 */
public interface Ontology extends EObject {
	/**
	 * Returns the value of the '<em><b>URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>URI</em>' attribute.
	 * @see #setURI(String)
	 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getOntology_URI()
	 * @model
	 * @generated
	 */
	String getURI();

	/**
	 * Sets the value of the '{@link org.smool.sdk.ontmodel.ontmodel.Ontology#getURI <em>URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>URI</em>' attribute.
	 * @see #getURI()
	 * @generated
	 */
	void setURI(String value);

	/**
	 * Returns the value of the '<em><b>Name Space</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name Space</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name Space</em>' containment reference.
	 * @see #setNameSpace(NameSpace)
	 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getOntology_NameSpace()
	 * @model containment="true"
	 * @generated
	 */
	NameSpace getNameSpace();

	/**
	 * Sets the value of the '{@link org.smool.sdk.ontmodel.ontmodel.Ontology#getNameSpace <em>Name Space</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name Space</em>' containment reference.
	 * @see #getNameSpace()
	 * @generated
	 */
	void setNameSpace(NameSpace value);

	/**
	 * Returns the value of the '<em><b>Classes</b></em>' containment reference list.
	 * The list contents are of type {@link org.smool.sdk.ontmodel.ontmodel.Class}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Classes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Classes</em>' containment reference list.
	 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getOntology_Classes()
	 * @model containment="true"
	 * @generated
	 */
	EList<org.smool.sdk.ontmodel.ontmodel.Class> getClasses();

	/**
	 * Returns the value of the '<em><b>Project Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Project Info</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Project Info</em>' containment reference.
	 * @see #setProjectInfo(ProjectInfo)
	 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getOntology_ProjectInfo()
	 * @model containment="true"
	 * @generated
	 */
	ProjectInfo getProjectInfo();

	/**
	 * Sets the value of the '{@link org.smool.sdk.ontmodel.ontmodel.Ontology#getProjectInfo <em>Project Info</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Project Info</em>' containment reference.
	 * @see #getProjectInfo()
	 * @generated
	 */
	void setProjectInfo(ProjectInfo value);

	/**
	 * Returns the value of the '<em><b>Imported Elements</b></em>' containment reference list.
	 * The list contents are of type {@link org.smool.sdk.ontmodel.ontmodel.ImportedElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imported Elements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imported Elements</em>' containment reference list.
	 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getOntology_ImportedElements()
	 * @model containment="true"
	 * @generated
	 */
	EList<ImportedElement> getImportedElements();

	/**
	 * Returns the value of the '<em><b>Unprocessed Properties</b></em>' containment reference list.
	 * The list contents are of type {@link org.smool.sdk.ontmodel.ontmodel.UnprocessedProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unprocessed Properties</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unprocessed Properties</em>' containment reference list.
	 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getOntology_UnprocessedProperties()
	 * @model containment="true"
	 * @generated
	 */
	EList<UnprocessedProperty> getUnprocessedProperties();

	/**
	 * Returns the value of the '<em><b>Unprocessable Properties</b></em>' containment reference list.
	 * The list contents are of type {@link org.smool.sdk.ontmodel.ontmodel.UnprocessableProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unprocessable Properties</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unprocessable Properties</em>' containment reference list.
	 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getOntology_UnprocessableProperties()
	 * @model containment="true"
	 * @generated
	 */
	EList<UnprocessableProperty> getUnprocessableProperties();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	org.smool.sdk.ontmodel.ontmodel.Class getClassByName(String name);

} // Ontology
