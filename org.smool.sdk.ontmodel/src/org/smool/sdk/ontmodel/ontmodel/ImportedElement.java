/**
 */
package org.smool.sdk.ontmodel.ontmodel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Imported Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.ImportedElement#getOntName <em>Ont Name</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.ImportedElement#getClasses <em>Classes</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.ImportedElement#getOntURI <em>Ont URI</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getImportedElement()
 * @model
 * @generated
 */
public interface ImportedElement extends EObject {
	/**
	 * Returns the value of the '<em><b>Ont Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ont Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ont Name</em>' attribute.
	 * @see #setOntName(String)
	 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getImportedElement_OntName()
	 * @model required="true"
	 * @generated
	 */
	String getOntName();

	/**
	 * Sets the value of the '{@link org.smool.sdk.ontmodel.ontmodel.ImportedElement#getOntName <em>Ont Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ont Name</em>' attribute.
	 * @see #getOntName()
	 * @generated
	 */
	void setOntName(String value);

	/**
	 * Returns the value of the '<em><b>Classes</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Classes</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Classes</em>' attribute list.
	 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getImportedElement_Classes()
	 * @model
	 * @generated
	 */
	EList<String> getClasses();

	/**
	 * Returns the value of the '<em><b>Ont URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ont URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ont URI</em>' attribute.
	 * @see #setOntURI(String)
	 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getImportedElement_OntURI()
	 * @model required="true"
	 * @generated
	 */
	String getOntURI();

	/**
	 * Sets the value of the '{@link org.smool.sdk.ontmodel.ontmodel.ImportedElement#getOntURI <em>Ont URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ont URI</em>' attribute.
	 * @see #getOntURI()
	 * @generated
	 */
	void setOntURI(String value);

} // ImportedElement
