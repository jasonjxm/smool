/**
 */
package org.smool.sdk.ontmodel.ontmodel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unprocessable Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.UnprocessableProperty#getId <em>Id</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.UnprocessableProperty#getUri <em>Uri</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.UnprocessableProperty#getSuperProps <em>Super Props</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.UnprocessableProperty#getRange <em>Range</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.UnprocessableProperty#getRangeOnt <em>Range Ont</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getUnprocessableProperty()
 * @model
 * @generated
 */
public interface UnprocessableProperty extends EObject {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getUnprocessableProperty_Id()
	 * @model
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link org.smool.sdk.ontmodel.ontmodel.UnprocessableProperty#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Uri</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uri</em>' attribute.
	 * @see #setUri(String)
	 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getUnprocessableProperty_Uri()
	 * @model
	 * @generated
	 */
	String getUri();

	/**
	 * Sets the value of the '{@link org.smool.sdk.ontmodel.ontmodel.UnprocessableProperty#getUri <em>Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Uri</em>' attribute.
	 * @see #getUri()
	 * @generated
	 */
	void setUri(String value);

	/**
	 * Returns the value of the '<em><b>Super Props</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Super Props</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Super Props</em>' attribute list.
	 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getUnprocessableProperty_SuperProps()
	 * @model
	 * @generated
	 */
	EList<String> getSuperProps();

	/**
	 * Returns the value of the '<em><b>Range</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Range</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Range</em>' attribute.
	 * @see #setRange(String)
	 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getUnprocessableProperty_Range()
	 * @model
	 * @generated
	 */
	String getRange();

	/**
	 * Sets the value of the '{@link org.smool.sdk.ontmodel.ontmodel.UnprocessableProperty#getRange <em>Range</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Range</em>' attribute.
	 * @see #getRange()
	 * @generated
	 */
	void setRange(String value);

	/**
	 * Returns the value of the '<em><b>Range Ont</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Range Ont</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Range Ont</em>' attribute.
	 * @see #setRangeOnt(String)
	 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getUnprocessableProperty_RangeOnt()
	 * @model
	 * @generated
	 */
	String getRangeOnt();

	/**
	 * Sets the value of the '{@link org.smool.sdk.ontmodel.ontmodel.UnprocessableProperty#getRangeOnt <em>Range Ont</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Range Ont</em>' attribute.
	 * @see #getRangeOnt()
	 * @generated
	 */
	void setRangeOnt(String value);

} // UnprocessableProperty
