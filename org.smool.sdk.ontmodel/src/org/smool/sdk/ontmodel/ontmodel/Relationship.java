/**
 */
package org.smool.sdk.ontmodel.ontmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Relationship</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.Relationship#getType <em>Type</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.Relationship#getArg <em>Arg</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.Relationship#getReferenceOntology <em>Reference Ontology</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getRelationship()
 * @model
 * @generated
 */
public interface Relationship extends EObject {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.smool.sdk.ontmodel.ontmodel.RelationshipTypes}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see org.smool.sdk.ontmodel.ontmodel.RelationshipTypes
	 * @see #setType(RelationshipTypes)
	 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getRelationship_Type()
	 * @model
	 * @generated
	 */
	RelationshipTypes getType();

	/**
	 * Sets the value of the '{@link org.smool.sdk.ontmodel.ontmodel.Relationship#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see org.smool.sdk.ontmodel.ontmodel.RelationshipTypes
	 * @see #getType()
	 * @generated
	 */
	void setType(RelationshipTypes value);

	/**
	 * Returns the value of the '<em><b>Arg</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Arg</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Arg</em>' attribute.
	 * @see #setArg(String)
	 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getRelationship_Arg()
	 * @model
	 * @generated
	 */
	String getArg();

	/**
	 * Sets the value of the '{@link org.smool.sdk.ontmodel.ontmodel.Relationship#getArg <em>Arg</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Arg</em>' attribute.
	 * @see #getArg()
	 * @generated
	 */
	void setArg(String value);

	/**
	 * Returns the value of the '<em><b>Reference Ontology</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reference Ontology</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference Ontology</em>' attribute.
	 * @see #setReferenceOntology(String)
	 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getRelationship_ReferenceOntology()
	 * @model
	 * @generated
	 */
	String getReferenceOntology();

	/**
	 * Sets the value of the '{@link org.smool.sdk.ontmodel.ontmodel.Relationship#getReferenceOntology <em>Reference Ontology</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reference Ontology</em>' attribute.
	 * @see #getReferenceOntology()
	 * @generated
	 */
	void setReferenceOntology(String value);

} // Relationship
