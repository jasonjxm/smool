/**
 */
package org.smool.sdk.ontmodel.ontmodel.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

import org.smool.sdk.ontmodel.ontmodel.OntmodelPackage;
import org.smool.sdk.ontmodel.ontmodel.UnprocessedProperty;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Unprocessed Property</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.impl.UnprocessedPropertyImpl#getId <em>Id</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.impl.UnprocessedPropertyImpl#getUri <em>Uri</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.impl.UnprocessedPropertyImpl#getSuperProps <em>Super Props</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.impl.UnprocessedPropertyImpl#getRange <em>Range</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.impl.UnprocessedPropertyImpl#getRangeOnt <em>Range Ont</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UnprocessedPropertyImpl extends EObjectImpl implements UnprocessedProperty {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getUri() <em>Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUri()
	 * @generated
	 * @ordered
	 */
	protected static final String URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUri() <em>Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUri()
	 * @generated
	 * @ordered
	 */
	protected String uri = URI_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSuperProps() <em>Super Props</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSuperProps()
	 * @generated
	 * @ordered
	 */
	protected EList<String> superProps;

	/**
	 * The default value of the '{@link #getRange() <em>Range</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRange()
	 * @generated
	 * @ordered
	 */
	protected static final String RANGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRange() <em>Range</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRange()
	 * @generated
	 * @ordered
	 */
	protected String range = RANGE_EDEFAULT;

	/**
	 * The default value of the '{@link #getRangeOnt() <em>Range Ont</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRangeOnt()
	 * @generated
	 * @ordered
	 */
	protected static final String RANGE_ONT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRangeOnt() <em>Range Ont</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRangeOnt()
	 * @generated
	 * @ordered
	 */
	protected String rangeOnt = RANGE_ONT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UnprocessedPropertyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OntmodelPackage.Literals.UNPROCESSED_PROPERTY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OntmodelPackage.UNPROCESSED_PROPERTY__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getUri() {
		return uri;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setUri(String newUri) {
		String oldUri = uri;
		uri = newUri;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OntmodelPackage.UNPROCESSED_PROPERTY__URI, oldUri, uri));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<String> getSuperProps() {
		if (superProps == null) {
			superProps = new EDataTypeUniqueEList<String>(String.class, this, OntmodelPackage.UNPROCESSED_PROPERTY__SUPER_PROPS);
		}
		return superProps;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getRange() {
		return range;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRange(String newRange) {
		String oldRange = range;
		range = newRange;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OntmodelPackage.UNPROCESSED_PROPERTY__RANGE, oldRange, range));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getRangeOnt() {
		return rangeOnt;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRangeOnt(String newRangeOnt) {
		String oldRangeOnt = rangeOnt;
		rangeOnt = newRangeOnt;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OntmodelPackage.UNPROCESSED_PROPERTY__RANGE_ONT, oldRangeOnt, rangeOnt));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OntmodelPackage.UNPROCESSED_PROPERTY__ID:
				return getId();
			case OntmodelPackage.UNPROCESSED_PROPERTY__URI:
				return getUri();
			case OntmodelPackage.UNPROCESSED_PROPERTY__SUPER_PROPS:
				return getSuperProps();
			case OntmodelPackage.UNPROCESSED_PROPERTY__RANGE:
				return getRange();
			case OntmodelPackage.UNPROCESSED_PROPERTY__RANGE_ONT:
				return getRangeOnt();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OntmodelPackage.UNPROCESSED_PROPERTY__ID:
				setId((String)newValue);
				return;
			case OntmodelPackage.UNPROCESSED_PROPERTY__URI:
				setUri((String)newValue);
				return;
			case OntmodelPackage.UNPROCESSED_PROPERTY__SUPER_PROPS:
				getSuperProps().clear();
				getSuperProps().addAll((Collection<? extends String>)newValue);
				return;
			case OntmodelPackage.UNPROCESSED_PROPERTY__RANGE:
				setRange((String)newValue);
				return;
			case OntmodelPackage.UNPROCESSED_PROPERTY__RANGE_ONT:
				setRangeOnt((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OntmodelPackage.UNPROCESSED_PROPERTY__ID:
				setId(ID_EDEFAULT);
				return;
			case OntmodelPackage.UNPROCESSED_PROPERTY__URI:
				setUri(URI_EDEFAULT);
				return;
			case OntmodelPackage.UNPROCESSED_PROPERTY__SUPER_PROPS:
				getSuperProps().clear();
				return;
			case OntmodelPackage.UNPROCESSED_PROPERTY__RANGE:
				setRange(RANGE_EDEFAULT);
				return;
			case OntmodelPackage.UNPROCESSED_PROPERTY__RANGE_ONT:
				setRangeOnt(RANGE_ONT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OntmodelPackage.UNPROCESSED_PROPERTY__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case OntmodelPackage.UNPROCESSED_PROPERTY__URI:
				return URI_EDEFAULT == null ? uri != null : !URI_EDEFAULT.equals(uri);
			case OntmodelPackage.UNPROCESSED_PROPERTY__SUPER_PROPS:
				return superProps != null && !superProps.isEmpty();
			case OntmodelPackage.UNPROCESSED_PROPERTY__RANGE:
				return RANGE_EDEFAULT == null ? range != null : !RANGE_EDEFAULT.equals(range);
			case OntmodelPackage.UNPROCESSED_PROPERTY__RANGE_ONT:
				return RANGE_ONT_EDEFAULT == null ? rangeOnt != null : !RANGE_ONT_EDEFAULT.equals(rangeOnt);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", uri: ");
		result.append(uri);
		result.append(", superProps: ");
		result.append(superProps);
		result.append(", range: ");
		result.append(range);
		result.append(", rangeOnt: ");
		result.append(rangeOnt);
		result.append(')');
		return result.toString();
	}

} //UnprocessedPropertyImpl
