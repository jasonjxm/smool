/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.smool.sdk.ontmodel.ontmodel.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.smool.sdk.ontmodel.ontmodel.Class;
import org.smool.sdk.ontmodel.ontmodel.ImportedElement;
import org.smool.sdk.ontmodel.ontmodel.NameSpace;
import org.smool.sdk.ontmodel.ontmodel.OntmodelPackage;
import org.smool.sdk.ontmodel.ontmodel.Ontology;
import org.smool.sdk.ontmodel.ontmodel.ProjectInfo;
import org.smool.sdk.ontmodel.ontmodel.UnprocessableProperty;
import org.smool.sdk.ontmodel.ontmodel.UnprocessedProperty;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ontology</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.impl.OntologyImpl#getURI <em>URI</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.impl.OntologyImpl#getNameSpace <em>Name Space</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.impl.OntologyImpl#getClasses <em>Classes</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.impl.OntologyImpl#getProjectInfo <em>Project Info</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.impl.OntologyImpl#getImportedElements <em>Imported Elements</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.impl.OntologyImpl#getUnprocessedProperties <em>Unprocessed Properties</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.impl.OntologyImpl#getUnprocessableProperties <em>Unprocessable Properties</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OntologyImpl extends EObjectImpl implements Ontology {
	/**
	 * The default value of the '{@link #getURI() <em>URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getURI()
	 * @generated
	 * @ordered
	 */
	protected static final String URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getURI() <em>URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getURI()
	 * @generated
	 * @ordered
	 */
	protected String uri = URI_EDEFAULT;

	/**
	 * The cached value of the '{@link #getNameSpace() <em>Name Space</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNameSpace()
	 * @generated
	 * @ordered
	 */
	protected NameSpace nameSpace;

	/**
	 * The cached value of the '{@link #getClasses() <em>Classes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClasses()
	 * @generated
	 * @ordered
	 */
	protected EList<org.smool.sdk.ontmodel.ontmodel.Class> classes;

	/**
	 * The cached value of the '{@link #getProjectInfo() <em>Project Info</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProjectInfo()
	 * @generated
	 * @ordered
	 */
	protected ProjectInfo projectInfo;

	/**
	 * The cached value of the '{@link #getImportedElements() <em>Imported Elements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImportedElements()
	 * @generated
	 * @ordered
	 */
	protected EList<ImportedElement> importedElements;

	/**
	 * The cached value of the '{@link #getUnprocessedProperties() <em>Unprocessed Properties</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnprocessedProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<UnprocessedProperty> unprocessedProperties;

	/**
	 * The cached value of the '{@link #getUnprocessableProperties() <em>Unprocessable Properties</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnprocessableProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<UnprocessableProperty> unprocessableProperties;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OntologyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OntmodelPackage.Literals.ONTOLOGY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getURI() {
		return uri;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setURI(String newURI) {
		String oldURI = uri;
		uri = newURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OntmodelPackage.ONTOLOGY__URI, oldURI, uri));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NameSpace getNameSpace() {
		return nameSpace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNameSpace(NameSpace newNameSpace, NotificationChain msgs) {
		NameSpace oldNameSpace = nameSpace;
		nameSpace = newNameSpace;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OntmodelPackage.ONTOLOGY__NAME_SPACE, oldNameSpace, newNameSpace);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNameSpace(NameSpace newNameSpace) {
		if (newNameSpace != nameSpace) {
			NotificationChain msgs = null;
			if (nameSpace != null)
				msgs = ((InternalEObject)nameSpace).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OntmodelPackage.ONTOLOGY__NAME_SPACE, null, msgs);
			if (newNameSpace != null)
				msgs = ((InternalEObject)newNameSpace).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OntmodelPackage.ONTOLOGY__NAME_SPACE, null, msgs);
			msgs = basicSetNameSpace(newNameSpace, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OntmodelPackage.ONTOLOGY__NAME_SPACE, newNameSpace, newNameSpace));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<org.smool.sdk.ontmodel.ontmodel.Class> getClasses() {
		if (classes == null) {
			classes = new EObjectContainmentEList<org.smool.sdk.ontmodel.ontmodel.Class>(org.smool.sdk.ontmodel.ontmodel.Class.class, this, OntmodelPackage.ONTOLOGY__CLASSES);
		}
		return classes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ProjectInfo getProjectInfo() {
		return projectInfo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProjectInfo(ProjectInfo newProjectInfo, NotificationChain msgs) {
		ProjectInfo oldProjectInfo = projectInfo;
		projectInfo = newProjectInfo;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OntmodelPackage.ONTOLOGY__PROJECT_INFO, oldProjectInfo, newProjectInfo);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setProjectInfo(ProjectInfo newProjectInfo) {
		if (newProjectInfo != projectInfo) {
			NotificationChain msgs = null;
			if (projectInfo != null)
				msgs = ((InternalEObject)projectInfo).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OntmodelPackage.ONTOLOGY__PROJECT_INFO, null, msgs);
			if (newProjectInfo != null)
				msgs = ((InternalEObject)newProjectInfo).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OntmodelPackage.ONTOLOGY__PROJECT_INFO, null, msgs);
			msgs = basicSetProjectInfo(newProjectInfo, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OntmodelPackage.ONTOLOGY__PROJECT_INFO, newProjectInfo, newProjectInfo));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ImportedElement> getImportedElements() {
		if (importedElements == null) {
			importedElements = new EObjectContainmentEList<ImportedElement>(ImportedElement.class, this, OntmodelPackage.ONTOLOGY__IMPORTED_ELEMENTS);
		}
		return importedElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<UnprocessedProperty> getUnprocessedProperties() {
		if (unprocessedProperties == null) {
			unprocessedProperties = new EObjectContainmentEList<UnprocessedProperty>(UnprocessedProperty.class, this, OntmodelPackage.ONTOLOGY__UNPROCESSED_PROPERTIES);
		}
		return unprocessedProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<UnprocessableProperty> getUnprocessableProperties() {
		if (unprocessableProperties == null) {
			unprocessableProperties = new EObjectContainmentEList<UnprocessableProperty>(UnprocessableProperty.class, this, OntmodelPackage.ONTOLOGY__UNPROCESSABLE_PROPERTIES);
		}
		return unprocessableProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public org.smool.sdk.ontmodel.ontmodel.Class getClassByName(String name) {
		Class result = null;
		for (Class c : classes) {
			if (c.getURI().substring(c.getURI().lastIndexOf("#")+1).equals(name)) {
				result = c;
				break;
			}
		}
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OntmodelPackage.ONTOLOGY__NAME_SPACE:
				return basicSetNameSpace(null, msgs);
			case OntmodelPackage.ONTOLOGY__CLASSES:
				return ((InternalEList<?>)getClasses()).basicRemove(otherEnd, msgs);
			case OntmodelPackage.ONTOLOGY__PROJECT_INFO:
				return basicSetProjectInfo(null, msgs);
			case OntmodelPackage.ONTOLOGY__IMPORTED_ELEMENTS:
				return ((InternalEList<?>)getImportedElements()).basicRemove(otherEnd, msgs);
			case OntmodelPackage.ONTOLOGY__UNPROCESSED_PROPERTIES:
				return ((InternalEList<?>)getUnprocessedProperties()).basicRemove(otherEnd, msgs);
			case OntmodelPackage.ONTOLOGY__UNPROCESSABLE_PROPERTIES:
				return ((InternalEList<?>)getUnprocessableProperties()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OntmodelPackage.ONTOLOGY__URI:
				return getURI();
			case OntmodelPackage.ONTOLOGY__NAME_SPACE:
				return getNameSpace();
			case OntmodelPackage.ONTOLOGY__CLASSES:
				return getClasses();
			case OntmodelPackage.ONTOLOGY__PROJECT_INFO:
				return getProjectInfo();
			case OntmodelPackage.ONTOLOGY__IMPORTED_ELEMENTS:
				return getImportedElements();
			case OntmodelPackage.ONTOLOGY__UNPROCESSED_PROPERTIES:
				return getUnprocessedProperties();
			case OntmodelPackage.ONTOLOGY__UNPROCESSABLE_PROPERTIES:
				return getUnprocessableProperties();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OntmodelPackage.ONTOLOGY__URI:
				setURI((String)newValue);
				return;
			case OntmodelPackage.ONTOLOGY__NAME_SPACE:
				setNameSpace((NameSpace)newValue);
				return;
			case OntmodelPackage.ONTOLOGY__CLASSES:
				getClasses().clear();
				getClasses().addAll((Collection<? extends org.smool.sdk.ontmodel.ontmodel.Class>)newValue);
				return;
			case OntmodelPackage.ONTOLOGY__PROJECT_INFO:
				setProjectInfo((ProjectInfo)newValue);
				return;
			case OntmodelPackage.ONTOLOGY__IMPORTED_ELEMENTS:
				getImportedElements().clear();
				getImportedElements().addAll((Collection<? extends ImportedElement>)newValue);
				return;
			case OntmodelPackage.ONTOLOGY__UNPROCESSED_PROPERTIES:
				getUnprocessedProperties().clear();
				getUnprocessedProperties().addAll((Collection<? extends UnprocessedProperty>)newValue);
				return;
			case OntmodelPackage.ONTOLOGY__UNPROCESSABLE_PROPERTIES:
				getUnprocessableProperties().clear();
				getUnprocessableProperties().addAll((Collection<? extends UnprocessableProperty>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OntmodelPackage.ONTOLOGY__URI:
				setURI(URI_EDEFAULT);
				return;
			case OntmodelPackage.ONTOLOGY__NAME_SPACE:
				setNameSpace((NameSpace)null);
				return;
			case OntmodelPackage.ONTOLOGY__CLASSES:
				getClasses().clear();
				return;
			case OntmodelPackage.ONTOLOGY__PROJECT_INFO:
				setProjectInfo((ProjectInfo)null);
				return;
			case OntmodelPackage.ONTOLOGY__IMPORTED_ELEMENTS:
				getImportedElements().clear();
				return;
			case OntmodelPackage.ONTOLOGY__UNPROCESSED_PROPERTIES:
				getUnprocessedProperties().clear();
				return;
			case OntmodelPackage.ONTOLOGY__UNPROCESSABLE_PROPERTIES:
				getUnprocessableProperties().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OntmodelPackage.ONTOLOGY__URI:
				return URI_EDEFAULT == null ? uri != null : !URI_EDEFAULT.equals(uri);
			case OntmodelPackage.ONTOLOGY__NAME_SPACE:
				return nameSpace != null;
			case OntmodelPackage.ONTOLOGY__CLASSES:
				return classes != null && !classes.isEmpty();
			case OntmodelPackage.ONTOLOGY__PROJECT_INFO:
				return projectInfo != null;
			case OntmodelPackage.ONTOLOGY__IMPORTED_ELEMENTS:
				return importedElements != null && !importedElements.isEmpty();
			case OntmodelPackage.ONTOLOGY__UNPROCESSED_PROPERTIES:
				return unprocessedProperties != null && !unprocessedProperties.isEmpty();
			case OntmodelPackage.ONTOLOGY__UNPROCESSABLE_PROPERTIES:
				return unprocessableProperties != null && !unprocessableProperties.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (URI: ");
		result.append(uri);
		result.append(')');
		return result.toString();
	}

} //OntologyImpl
