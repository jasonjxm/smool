/**
 */
package org.smool.sdk.ontmodel.ontmodel.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.smool.sdk.ontmodel.ontmodel.OntmodelPackage;
import org.smool.sdk.ontmodel.ontmodel.ProjectInfo;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Project Info</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.impl.ProjectInfoImpl#getProjectName <em>Project Name</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.impl.ProjectInfoImpl#getSIBName <em>SIB Name</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.impl.ProjectInfoImpl#getAutoDetect <em>Auto Detect</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.impl.ProjectInfoImpl#getIpAddress <em>Ip Address</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.impl.ProjectInfoImpl#getPort <em>Port</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ProjectInfoImpl extends EObjectImpl implements ProjectInfo {
	/**
	 * The default value of the '{@link #getProjectName() <em>Project Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProjectName()
	 * @generated
	 * @ordered
	 */
	protected static final String PROJECT_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getProjectName() <em>Project Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProjectName()
	 * @generated
	 * @ordered
	 */
	protected String projectName = PROJECT_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getSIBName() <em>SIB Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSIBName()
	 * @generated
	 * @ordered
	 */
	protected static final String SIB_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSIBName() <em>SIB Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSIBName()
	 * @generated
	 * @ordered
	 */
	protected String sibName = SIB_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getAutoDetect() <em>Auto Detect</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAutoDetect()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean AUTO_DETECT_EDEFAULT = Boolean.TRUE;

	/**
	 * The cached value of the '{@link #getAutoDetect() <em>Auto Detect</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAutoDetect()
	 * @generated
	 * @ordered
	 */
	protected Boolean autoDetect = AUTO_DETECT_EDEFAULT;

	/**
	 * The default value of the '{@link #getIpAddress() <em>Ip Address</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIpAddress()
	 * @generated
	 * @ordered
	 */
	protected static final String IP_ADDRESS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getIpAddress() <em>Ip Address</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIpAddress()
	 * @generated
	 * @ordered
	 */
	protected String ipAddress = IP_ADDRESS_EDEFAULT;

	/**
	 * The default value of the '{@link #getPort() <em>Port</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPort()
	 * @generated
	 * @ordered
	 */
	protected static final String PORT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPort() <em>Port</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPort()
	 * @generated
	 * @ordered
	 */
	protected String port = PORT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProjectInfoImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OntmodelPackage.Literals.PROJECT_INFO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getProjectName() {
		return projectName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setProjectName(String newProjectName) {
		String oldProjectName = projectName;
		projectName = newProjectName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OntmodelPackage.PROJECT_INFO__PROJECT_NAME, oldProjectName, projectName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSIBName() {
		return sibName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSIBName(String newSIBName) {
		String oldSIBName = sibName;
		sibName = newSIBName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OntmodelPackage.PROJECT_INFO__SIB_NAME, oldSIBName, sibName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Boolean getAutoDetect() {
		return autoDetect;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAutoDetect(Boolean newAutoDetect) {
		Boolean oldAutoDetect = autoDetect;
		autoDetect = newAutoDetect;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OntmodelPackage.PROJECT_INFO__AUTO_DETECT, oldAutoDetect, autoDetect));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setIpAddress(String newIpAddress) {
		String oldIpAddress = ipAddress;
		ipAddress = newIpAddress;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OntmodelPackage.PROJECT_INFO__IP_ADDRESS, oldIpAddress, ipAddress));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getPort() {
		return port;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPort(String newPort) {
		String oldPort = port;
		port = newPort;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OntmodelPackage.PROJECT_INFO__PORT, oldPort, port));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OntmodelPackage.PROJECT_INFO__PROJECT_NAME:
				return getProjectName();
			case OntmodelPackage.PROJECT_INFO__SIB_NAME:
				return getSIBName();
			case OntmodelPackage.PROJECT_INFO__AUTO_DETECT:
				return getAutoDetect();
			case OntmodelPackage.PROJECT_INFO__IP_ADDRESS:
				return getIpAddress();
			case OntmodelPackage.PROJECT_INFO__PORT:
				return getPort();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OntmodelPackage.PROJECT_INFO__PROJECT_NAME:
				setProjectName((String)newValue);
				return;
			case OntmodelPackage.PROJECT_INFO__SIB_NAME:
				setSIBName((String)newValue);
				return;
			case OntmodelPackage.PROJECT_INFO__AUTO_DETECT:
				setAutoDetect((Boolean)newValue);
				return;
			case OntmodelPackage.PROJECT_INFO__IP_ADDRESS:
				setIpAddress((String)newValue);
				return;
			case OntmodelPackage.PROJECT_INFO__PORT:
				setPort((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OntmodelPackage.PROJECT_INFO__PROJECT_NAME:
				setProjectName(PROJECT_NAME_EDEFAULT);
				return;
			case OntmodelPackage.PROJECT_INFO__SIB_NAME:
				setSIBName(SIB_NAME_EDEFAULT);
				return;
			case OntmodelPackage.PROJECT_INFO__AUTO_DETECT:
				setAutoDetect(AUTO_DETECT_EDEFAULT);
				return;
			case OntmodelPackage.PROJECT_INFO__IP_ADDRESS:
				setIpAddress(IP_ADDRESS_EDEFAULT);
				return;
			case OntmodelPackage.PROJECT_INFO__PORT:
				setPort(PORT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OntmodelPackage.PROJECT_INFO__PROJECT_NAME:
				return PROJECT_NAME_EDEFAULT == null ? projectName != null : !PROJECT_NAME_EDEFAULT.equals(projectName);
			case OntmodelPackage.PROJECT_INFO__SIB_NAME:
				return SIB_NAME_EDEFAULT == null ? sibName != null : !SIB_NAME_EDEFAULT.equals(sibName);
			case OntmodelPackage.PROJECT_INFO__AUTO_DETECT:
				return AUTO_DETECT_EDEFAULT == null ? autoDetect != null : !AUTO_DETECT_EDEFAULT.equals(autoDetect);
			case OntmodelPackage.PROJECT_INFO__IP_ADDRESS:
				return IP_ADDRESS_EDEFAULT == null ? ipAddress != null : !IP_ADDRESS_EDEFAULT.equals(ipAddress);
			case OntmodelPackage.PROJECT_INFO__PORT:
				return PORT_EDEFAULT == null ? port != null : !PORT_EDEFAULT.equals(port);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (ProjectName: ");
		result.append(projectName);
		result.append(", SIBName: ");
		result.append(sibName);
		result.append(", autoDetect: ");
		result.append(autoDetect);
		result.append(", ipAddress: ");
		result.append(ipAddress);
		result.append(", port: ");
		result.append(port);
		result.append(')');
		return result.toString();
	}

} //ProjectInfoImpl
