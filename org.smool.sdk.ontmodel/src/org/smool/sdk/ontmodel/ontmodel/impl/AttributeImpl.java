/**
 */
package org.smool.sdk.ontmodel.ontmodel.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.smool.sdk.ontmodel.ontmodel.Attribute;
import org.smool.sdk.ontmodel.ontmodel.AttributeTypes;
import org.smool.sdk.ontmodel.ontmodel.CardinalityTypes;
import org.smool.sdk.ontmodel.ontmodel.OntmodelPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attribute</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.impl.AttributeImpl#getURI <em>URI</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.impl.AttributeImpl#getType <em>Type</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.impl.AttributeImpl#getRange <em>Range</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.impl.AttributeImpl#getRangeOntology <em>Range Ontology</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.impl.AttributeImpl#getCardinality <em>Cardinality</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AttributeImpl extends EObjectImpl implements Attribute {
	/**
	 * The default value of the '{@link #getURI() <em>URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getURI()
	 * @generated
	 * @ordered
	 */
	protected static final String URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getURI() <em>URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getURI()
	 * @generated
	 * @ordered
	 */
	protected String uri = URI_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final AttributeTypes TYPE_EDEFAULT = AttributeTypes.FUNCTIONAL_OBJECT;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected AttributeTypes type = TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getRange() <em>Range</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRange()
	 * @generated
	 * @ordered
	 */
	protected static final String RANGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRange() <em>Range</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRange()
	 * @generated
	 * @ordered
	 */
	protected String range = RANGE_EDEFAULT;

	/**
	 * The default value of the '{@link #getRangeOntology() <em>Range Ontology</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRangeOntology()
	 * @generated
	 * @ordered
	 */
	protected static final String RANGE_ONTOLOGY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRangeOntology() <em>Range Ontology</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRangeOntology()
	 * @generated
	 * @ordered
	 */
	protected String rangeOntology = RANGE_ONTOLOGY_EDEFAULT;

	/**
	 * The default value of the '{@link #getCardinality() <em>Cardinality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCardinality()
	 * @generated
	 * @ordered
	 */
	protected static final CardinalityTypes CARDINALITY_EDEFAULT = CardinalityTypes.SINGLE;

	/**
	 * The cached value of the '{@link #getCardinality() <em>Cardinality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCardinality()
	 * @generated
	 * @ordered
	 */
	protected CardinalityTypes cardinality = CARDINALITY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AttributeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OntmodelPackage.Literals.ATTRIBUTE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getURI() {
		return uri;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setURI(String newURI) {
		String oldURI = uri;
		uri = newURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OntmodelPackage.ATTRIBUTE__URI, oldURI, uri));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AttributeTypes getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setType(AttributeTypes newType) {
		AttributeTypes oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OntmodelPackage.ATTRIBUTE__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getRange() {
		return range;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRange(String newRange) {
		String oldRange = range;
		range = newRange;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OntmodelPackage.ATTRIBUTE__RANGE, oldRange, range));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getRangeOntology() {
		return rangeOntology;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRangeOntology(String newRangeOntology) {
		String oldRangeOntology = rangeOntology;
		rangeOntology = newRangeOntology;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OntmodelPackage.ATTRIBUTE__RANGE_ONTOLOGY, oldRangeOntology, rangeOntology));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CardinalityTypes getCardinality() {
		return cardinality;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCardinality(CardinalityTypes newCardinality) {
		CardinalityTypes oldCardinality = cardinality;
		cardinality = newCardinality == null ? CARDINALITY_EDEFAULT : newCardinality;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OntmodelPackage.ATTRIBUTE__CARDINALITY, oldCardinality, cardinality));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OntmodelPackage.ATTRIBUTE__URI:
				return getURI();
			case OntmodelPackage.ATTRIBUTE__TYPE:
				return getType();
			case OntmodelPackage.ATTRIBUTE__RANGE:
				return getRange();
			case OntmodelPackage.ATTRIBUTE__RANGE_ONTOLOGY:
				return getRangeOntology();
			case OntmodelPackage.ATTRIBUTE__CARDINALITY:
				return getCardinality();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OntmodelPackage.ATTRIBUTE__URI:
				setURI((String)newValue);
				return;
			case OntmodelPackage.ATTRIBUTE__TYPE:
				setType((AttributeTypes)newValue);
				return;
			case OntmodelPackage.ATTRIBUTE__RANGE:
				setRange((String)newValue);
				return;
			case OntmodelPackage.ATTRIBUTE__RANGE_ONTOLOGY:
				setRangeOntology((String)newValue);
				return;
			case OntmodelPackage.ATTRIBUTE__CARDINALITY:
				setCardinality((CardinalityTypes)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OntmodelPackage.ATTRIBUTE__URI:
				setURI(URI_EDEFAULT);
				return;
			case OntmodelPackage.ATTRIBUTE__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case OntmodelPackage.ATTRIBUTE__RANGE:
				setRange(RANGE_EDEFAULT);
				return;
			case OntmodelPackage.ATTRIBUTE__RANGE_ONTOLOGY:
				setRangeOntology(RANGE_ONTOLOGY_EDEFAULT);
				return;
			case OntmodelPackage.ATTRIBUTE__CARDINALITY:
				setCardinality(CARDINALITY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OntmodelPackage.ATTRIBUTE__URI:
				return URI_EDEFAULT == null ? uri != null : !URI_EDEFAULT.equals(uri);
			case OntmodelPackage.ATTRIBUTE__TYPE:
				return type != TYPE_EDEFAULT;
			case OntmodelPackage.ATTRIBUTE__RANGE:
				return RANGE_EDEFAULT == null ? range != null : !RANGE_EDEFAULT.equals(range);
			case OntmodelPackage.ATTRIBUTE__RANGE_ONTOLOGY:
				return RANGE_ONTOLOGY_EDEFAULT == null ? rangeOntology != null : !RANGE_ONTOLOGY_EDEFAULT.equals(rangeOntology);
			case OntmodelPackage.ATTRIBUTE__CARDINALITY:
				return cardinality != CARDINALITY_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (URI: ");
		result.append(uri);
		result.append(", Type: ");
		result.append(type);
		result.append(", Range: ");
		result.append(range);
		result.append(", RangeOntology: ");
		result.append(rangeOntology);
		result.append(", Cardinality: ");
		result.append(cardinality);
		result.append(')');
		return result.toString();
	}

} //AttributeImpl
