/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.smool.sdk.ontmodel.ontmodel.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.smool.sdk.ontmodel.ontmodel.Attribute;
import org.smool.sdk.ontmodel.ontmodel.GenerationTypes;
import org.smool.sdk.ontmodel.ontmodel.OntmodelPackage;
import org.smool.sdk.ontmodel.ontmodel.Relationship;
import org.smool.sdk.ontmodel.ontmodel.RelationshipTypes;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Class</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.impl.ClassImpl#getURI <em>URI</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.impl.ClassImpl#getAttributes <em>Attributes</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.impl.ClassImpl#getRelationships <em>Relationships</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.impl.ClassImpl#getGenerationType <em>Generation Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ClassImpl extends EObjectImpl implements org.smool.sdk.ontmodel.ontmodel.Class {
	/**
	 * The default value of the '{@link #getURI() <em>URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getURI()
	 * @generated
	 * @ordered
	 */
	protected static final String URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getURI() <em>URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getURI()
	 * @generated
	 * @ordered
	 */
	protected String uri = URI_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAttributes() <em>Attributes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttributes()
	 * @generated
	 * @ordered
	 */
	protected EList<Attribute> attributes;

	/**
	 * The cached value of the '{@link #getRelationships() <em>Relationships</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelationships()
	 * @generated
	 * @ordered
	 */
	protected EList<Relationship> relationships;

	/**
	 * The default value of the '{@link #getGenerationType() <em>Generation Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGenerationType()
	 * @generated
	 * @ordered
	 */
	protected static final GenerationTypes GENERATION_TYPE_EDEFAULT = GenerationTypes.NONE;

	/**
	 * The cached value of the '{@link #getGenerationType() <em>Generation Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGenerationType()
	 * @generated
	 * @ordered
	 */
	protected GenerationTypes generationType = GENERATION_TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClassImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OntmodelPackage.Literals.CLASS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getURI() {
		return uri;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setURI(String newURI) {
		String oldURI = uri;
		uri = newURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OntmodelPackage.CLASS__URI, oldURI, uri));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Attribute> getAttributes() {
		if (attributes == null) {
			attributes = new EObjectContainmentEList<Attribute>(Attribute.class, this, OntmodelPackage.CLASS__ATTRIBUTES);
		}
		return attributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Relationship> getRelationships() {
		if (relationships == null) {
			relationships = new EObjectContainmentEList<Relationship>(Relationship.class, this, OntmodelPackage.CLASS__RELATIONSHIPS);
		}
		return relationships;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public GenerationTypes getGenerationType() {
		return generationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Modified this method to add intrisic management of generation type:
	 * NONE + PRODUCER = PRODUCER
	 * NONE + CONSUMER = CONSUMER
	 * PRODUCER + CONSUMER = PROSUMER
	 * CONSUMER + PRODUCER = PROSUMER
	 * PROSUMER + CONSUMER = PROSUMER
	 * PROSUMER + PRODUCER = PROSUMER
	 * ANY + NONE = NONE
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public void setGenerationType(GenerationTypes newGenerationType) {
		GenerationTypes oldGenerationType = generationType;
		if (newGenerationType == null) {
			generationType = GENERATION_TYPE_EDEFAULT;
		}
		else {
			if (oldGenerationType == GenerationTypes.NONE) {
				generationType = newGenerationType;
			}
			else if (oldGenerationType == GenerationTypes.PRODUCER) {
				if (newGenerationType == GenerationTypes.CONSUMER) {
					generationType = GenerationTypes.PROSUMER;
				}
				else {
					generationType = newGenerationType;
				}
			}
			else if (oldGenerationType == GenerationTypes.CONSUMER) {
				if (newGenerationType == GenerationTypes.PRODUCER) {
					generationType = GenerationTypes.PROSUMER;
				}
				else {
					generationType = newGenerationType;
				}
			}
			else {
				if (newGenerationType == GenerationTypes.NONE) {
					generationType = newGenerationType;
				}
			}
		}
		//generationType = newGenerationType == null ? GENERATION_TYPE_EDEFAULT : newGenerationType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OntmodelPackage.CLASS__GENERATION_TYPE, oldGenerationType, generationType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * Sets the generation type to this class and all its ancestors
	 * @param type. The generation type.
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public void recursivelySetGenerationType(GenerationTypes type) {
		org.smool.sdk.ontmodel.ontmodel.Ontology ont = (org.smool.sdk.ontmodel.ontmodel.Ontology) this.eContainer;
		setGenerationType(type);
		java.util.Vector<org.smool.sdk.ontmodel.ontmodel.Class> remaining = new java.util.Vector<org.smool.sdk.ontmodel.ontmodel.Class>();
		remaining.add(this);
		while (remaining.size() > 0) {
			org.smool.sdk.ontmodel.ontmodel.Class[] array = remaining.toArray(new org.smool.sdk.ontmodel.ontmodel.Class[0]);
			remaining.clear();
			for (org.smool.sdk.ontmodel.ontmodel.Class c : array) {
				for (Relationship r : c.getRelationships()) {
					if (r.getType().equals(RelationshipTypes.SUB_CLASS_OF)) {
						org.smool.sdk.ontmodel.ontmodel.Class c2 = ont.getClassByName(r.getArg());
						if (c2 != null) {
							c2.setGenerationType(type);
							remaining.add(c2);
						}
					}
				}
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OntmodelPackage.CLASS__ATTRIBUTES:
				return ((InternalEList<?>)getAttributes()).basicRemove(otherEnd, msgs);
			case OntmodelPackage.CLASS__RELATIONSHIPS:
				return ((InternalEList<?>)getRelationships()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OntmodelPackage.CLASS__URI:
				return getURI();
			case OntmodelPackage.CLASS__ATTRIBUTES:
				return getAttributes();
			case OntmodelPackage.CLASS__RELATIONSHIPS:
				return getRelationships();
			case OntmodelPackage.CLASS__GENERATION_TYPE:
				return getGenerationType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OntmodelPackage.CLASS__URI:
				setURI((String)newValue);
				return;
			case OntmodelPackage.CLASS__ATTRIBUTES:
				getAttributes().clear();
				getAttributes().addAll((Collection<? extends Attribute>)newValue);
				return;
			case OntmodelPackage.CLASS__RELATIONSHIPS:
				getRelationships().clear();
				getRelationships().addAll((Collection<? extends Relationship>)newValue);
				return;
			case OntmodelPackage.CLASS__GENERATION_TYPE:
				setGenerationType((GenerationTypes)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OntmodelPackage.CLASS__URI:
				setURI(URI_EDEFAULT);
				return;
			case OntmodelPackage.CLASS__ATTRIBUTES:
				getAttributes().clear();
				return;
			case OntmodelPackage.CLASS__RELATIONSHIPS:
				getRelationships().clear();
				return;
			case OntmodelPackage.CLASS__GENERATION_TYPE:
				setGenerationType(GENERATION_TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OntmodelPackage.CLASS__URI:
				return URI_EDEFAULT == null ? uri != null : !URI_EDEFAULT.equals(uri);
			case OntmodelPackage.CLASS__ATTRIBUTES:
				return attributes != null && !attributes.isEmpty();
			case OntmodelPackage.CLASS__RELATIONSHIPS:
				return relationships != null && !relationships.isEmpty();
			case OntmodelPackage.CLASS__GENERATION_TYPE:
				return generationType != GENERATION_TYPE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (URI: ");
		result.append(uri);
		result.append(", GenerationType: ");
		result.append(generationType);
		result.append(')');
		return result.toString();
	}

} //ClassImpl
