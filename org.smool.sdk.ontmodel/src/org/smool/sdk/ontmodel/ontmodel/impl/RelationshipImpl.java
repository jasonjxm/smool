/**
 */
package org.smool.sdk.ontmodel.ontmodel.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.smool.sdk.ontmodel.ontmodel.OntmodelPackage;
import org.smool.sdk.ontmodel.ontmodel.Relationship;
import org.smool.sdk.ontmodel.ontmodel.RelationshipTypes;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Relationship</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.impl.RelationshipImpl#getType <em>Type</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.impl.RelationshipImpl#getArg <em>Arg</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.impl.RelationshipImpl#getReferenceOntology <em>Reference Ontology</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RelationshipImpl extends EObjectImpl implements Relationship {
	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final RelationshipTypes TYPE_EDEFAULT = RelationshipTypes.NONE;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected RelationshipTypes type = TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getArg() <em>Arg</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArg()
	 * @generated
	 * @ordered
	 */
	protected static final String ARG_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getArg() <em>Arg</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArg()
	 * @generated
	 * @ordered
	 */
	protected String arg = ARG_EDEFAULT;

	/**
	 * The default value of the '{@link #getReferenceOntology() <em>Reference Ontology</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferenceOntology()
	 * @generated
	 * @ordered
	 */
	protected static final String REFERENCE_ONTOLOGY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getReferenceOntology() <em>Reference Ontology</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferenceOntology()
	 * @generated
	 * @ordered
	 */
	protected String referenceOntology = REFERENCE_ONTOLOGY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RelationshipImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OntmodelPackage.Literals.RELATIONSHIP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RelationshipTypes getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setType(RelationshipTypes newType) {
		RelationshipTypes oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OntmodelPackage.RELATIONSHIP__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getArg() {
		return arg;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setArg(String newArg) {
		String oldArg = arg;
		arg = newArg;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OntmodelPackage.RELATIONSHIP__ARG, oldArg, arg));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getReferenceOntology() {
		return referenceOntology;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setReferenceOntology(String newReferenceOntology) {
		String oldReferenceOntology = referenceOntology;
		referenceOntology = newReferenceOntology;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OntmodelPackage.RELATIONSHIP__REFERENCE_ONTOLOGY, oldReferenceOntology, referenceOntology));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OntmodelPackage.RELATIONSHIP__TYPE:
				return getType();
			case OntmodelPackage.RELATIONSHIP__ARG:
				return getArg();
			case OntmodelPackage.RELATIONSHIP__REFERENCE_ONTOLOGY:
				return getReferenceOntology();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OntmodelPackage.RELATIONSHIP__TYPE:
				setType((RelationshipTypes)newValue);
				return;
			case OntmodelPackage.RELATIONSHIP__ARG:
				setArg((String)newValue);
				return;
			case OntmodelPackage.RELATIONSHIP__REFERENCE_ONTOLOGY:
				setReferenceOntology((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OntmodelPackage.RELATIONSHIP__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case OntmodelPackage.RELATIONSHIP__ARG:
				setArg(ARG_EDEFAULT);
				return;
			case OntmodelPackage.RELATIONSHIP__REFERENCE_ONTOLOGY:
				setReferenceOntology(REFERENCE_ONTOLOGY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OntmodelPackage.RELATIONSHIP__TYPE:
				return type != TYPE_EDEFAULT;
			case OntmodelPackage.RELATIONSHIP__ARG:
				return ARG_EDEFAULT == null ? arg != null : !ARG_EDEFAULT.equals(arg);
			case OntmodelPackage.RELATIONSHIP__REFERENCE_ONTOLOGY:
				return REFERENCE_ONTOLOGY_EDEFAULT == null ? referenceOntology != null : !REFERENCE_ONTOLOGY_EDEFAULT.equals(referenceOntology);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (Type: ");
		result.append(type);
		result.append(", Arg: ");
		result.append(arg);
		result.append(", ReferenceOntology: ");
		result.append(referenceOntology);
		result.append(')');
		return result.toString();
	}

} //RelationshipImpl
