/**
 */
package org.smool.sdk.ontmodel.ontmodel.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.smool.sdk.ontmodel.ontmodel.Attribute;
import org.smool.sdk.ontmodel.ontmodel.AttributeTypes;
import org.smool.sdk.ontmodel.ontmodel.CardinalityTypes;
import org.smool.sdk.ontmodel.ontmodel.GenerationTypes;
import org.smool.sdk.ontmodel.ontmodel.ImportedElement;
import org.smool.sdk.ontmodel.ontmodel.NameSpace;
import org.smool.sdk.ontmodel.ontmodel.OntmodelFactory;
import org.smool.sdk.ontmodel.ontmodel.OntmodelPackage;
import org.smool.sdk.ontmodel.ontmodel.Ontology;
import org.smool.sdk.ontmodel.ontmodel.ProjectInfo;
import org.smool.sdk.ontmodel.ontmodel.Relationship;
import org.smool.sdk.ontmodel.ontmodel.RelationshipTypes;
import org.smool.sdk.ontmodel.ontmodel.UnprocessableProperty;
import org.smool.sdk.ontmodel.ontmodel.UnprocessedProperty;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OntmodelFactoryImpl extends EFactoryImpl implements OntmodelFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static OntmodelFactory init() {
		try {
			OntmodelFactory theOntmodelFactory = (OntmodelFactory)EPackage.Registry.INSTANCE.getEFactory(OntmodelPackage.eNS_URI);
			if (theOntmodelFactory != null) {
				return theOntmodelFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new OntmodelFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OntmodelFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case OntmodelPackage.ONTOLOGY: return createOntology();
			case OntmodelPackage.CLASS: return createClass();
			case OntmodelPackage.RELATIONSHIP: return createRelationship();
			case OntmodelPackage.ATTRIBUTE: return createAttribute();
			case OntmodelPackage.NAME_SPACE: return createNameSpace();
			case OntmodelPackage.PROJECT_INFO: return createProjectInfo();
			case OntmodelPackage.IMPORTED_ELEMENT: return createImportedElement();
			case OntmodelPackage.UNPROCESSED_PROPERTY: return createUnprocessedProperty();
			case OntmodelPackage.UNPROCESSABLE_PROPERTY: return createUnprocessableProperty();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case OntmodelPackage.ATTRIBUTE_TYPES:
				return createAttributeTypesFromString(eDataType, initialValue);
			case OntmodelPackage.RELATIONSHIP_TYPES:
				return createRelationshipTypesFromString(eDataType, initialValue);
			case OntmodelPackage.CARDINALITY_TYPES:
				return createCardinalityTypesFromString(eDataType, initialValue);
			case OntmodelPackage.GENERATION_TYPES:
				return createGenerationTypesFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case OntmodelPackage.ATTRIBUTE_TYPES:
				return convertAttributeTypesToString(eDataType, instanceValue);
			case OntmodelPackage.RELATIONSHIP_TYPES:
				return convertRelationshipTypesToString(eDataType, instanceValue);
			case OntmodelPackage.CARDINALITY_TYPES:
				return convertCardinalityTypesToString(eDataType, instanceValue);
			case OntmodelPackage.GENERATION_TYPES:
				return convertGenerationTypesToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Ontology createOntology() {
		OntologyImpl ontology = new OntologyImpl();
		return ontology;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public org.smool.sdk.ontmodel.ontmodel.Class createClass() {
		ClassImpl class_ = new ClassImpl();
		return class_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Relationship createRelationship() {
		RelationshipImpl relationship = new RelationshipImpl();
		return relationship;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Attribute createAttribute() {
		AttributeImpl attribute = new AttributeImpl();
		return attribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NameSpace createNameSpace() {
		NameSpaceImpl nameSpace = new NameSpaceImpl();
		return nameSpace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ProjectInfo createProjectInfo() {
		ProjectInfoImpl projectInfo = new ProjectInfoImpl();
		return projectInfo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ImportedElement createImportedElement() {
		ImportedElementImpl importedElement = new ImportedElementImpl();
		return importedElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UnprocessedProperty createUnprocessedProperty() {
		UnprocessedPropertyImpl unprocessedProperty = new UnprocessedPropertyImpl();
		return unprocessedProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UnprocessableProperty createUnprocessableProperty() {
		UnprocessablePropertyImpl unprocessableProperty = new UnprocessablePropertyImpl();
		return unprocessableProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeTypes createAttributeTypesFromString(EDataType eDataType, String initialValue) {
		AttributeTypes result = AttributeTypes.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAttributeTypesToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RelationshipTypes createRelationshipTypesFromString(EDataType eDataType, String initialValue) {
		RelationshipTypes result = RelationshipTypes.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRelationshipTypesToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CardinalityTypes createCardinalityTypesFromString(EDataType eDataType, String initialValue) {
		CardinalityTypes result = CardinalityTypes.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCardinalityTypesToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GenerationTypes createGenerationTypesFromString(EDataType eDataType, String initialValue) {
		GenerationTypes result = GenerationTypes.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertGenerationTypesToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public OntmodelPackage getOntmodelPackage() {
		return (OntmodelPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static OntmodelPackage getPackage() {
		return OntmodelPackage.eINSTANCE;
	}

} //OntmodelFactoryImpl
