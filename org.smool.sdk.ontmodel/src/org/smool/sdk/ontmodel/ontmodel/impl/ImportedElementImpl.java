/**
 */
package org.smool.sdk.ontmodel.ontmodel.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

import org.smool.sdk.ontmodel.ontmodel.ImportedElement;
import org.smool.sdk.ontmodel.ontmodel.OntmodelPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Imported Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.impl.ImportedElementImpl#getOntName <em>Ont Name</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.impl.ImportedElementImpl#getClasses <em>Classes</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.impl.ImportedElementImpl#getOntURI <em>Ont URI</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ImportedElementImpl extends EObjectImpl implements ImportedElement {
	/**
	 * The default value of the '{@link #getOntName() <em>Ont Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOntName()
	 * @generated
	 * @ordered
	 */
	protected static final String ONT_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOntName() <em>Ont Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOntName()
	 * @generated
	 * @ordered
	 */
	protected String ontName = ONT_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getClasses() <em>Classes</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClasses()
	 * @generated
	 * @ordered
	 */
	protected EList<String> classes;

	/**
	 * The default value of the '{@link #getOntURI() <em>Ont URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOntURI()
	 * @generated
	 * @ordered
	 */
	protected static final String ONT_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOntURI() <em>Ont URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOntURI()
	 * @generated
	 * @ordered
	 */
	protected String ontURI = ONT_URI_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ImportedElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OntmodelPackage.Literals.IMPORTED_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOntName() {
		return ontName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOntName(String newOntName) {
		String oldOntName = ontName;
		ontName = newOntName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OntmodelPackage.IMPORTED_ELEMENT__ONT_NAME, oldOntName, ontName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<String> getClasses() {
		if (classes == null) {
			classes = new EDataTypeUniqueEList<String>(String.class, this, OntmodelPackage.IMPORTED_ELEMENT__CLASSES);
		}
		return classes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOntURI() {
		return ontURI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOntURI(String newOntURI) {
		String oldOntURI = ontURI;
		ontURI = newOntURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OntmodelPackage.IMPORTED_ELEMENT__ONT_URI, oldOntURI, ontURI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OntmodelPackage.IMPORTED_ELEMENT__ONT_NAME:
				return getOntName();
			case OntmodelPackage.IMPORTED_ELEMENT__CLASSES:
				return getClasses();
			case OntmodelPackage.IMPORTED_ELEMENT__ONT_URI:
				return getOntURI();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OntmodelPackage.IMPORTED_ELEMENT__ONT_NAME:
				setOntName((String)newValue);
				return;
			case OntmodelPackage.IMPORTED_ELEMENT__CLASSES:
				getClasses().clear();
				getClasses().addAll((Collection<? extends String>)newValue);
				return;
			case OntmodelPackage.IMPORTED_ELEMENT__ONT_URI:
				setOntURI((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OntmodelPackage.IMPORTED_ELEMENT__ONT_NAME:
				setOntName(ONT_NAME_EDEFAULT);
				return;
			case OntmodelPackage.IMPORTED_ELEMENT__CLASSES:
				getClasses().clear();
				return;
			case OntmodelPackage.IMPORTED_ELEMENT__ONT_URI:
				setOntURI(ONT_URI_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OntmodelPackage.IMPORTED_ELEMENT__ONT_NAME:
				return ONT_NAME_EDEFAULT == null ? ontName != null : !ONT_NAME_EDEFAULT.equals(ontName);
			case OntmodelPackage.IMPORTED_ELEMENT__CLASSES:
				return classes != null && !classes.isEmpty();
			case OntmodelPackage.IMPORTED_ELEMENT__ONT_URI:
				return ONT_URI_EDEFAULT == null ? ontURI != null : !ONT_URI_EDEFAULT.equals(ontURI);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (OntName: ");
		result.append(ontName);
		result.append(", Classes: ");
		result.append(classes);
		result.append(", OntURI: ");
		result.append(ontURI);
		result.append(')');
		return result.toString();
	}

} //ImportedElementImpl
