/**
 */
package org.smool.sdk.ontmodel.ontmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Project Info</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.ProjectInfo#getProjectName <em>Project Name</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.ProjectInfo#getSIBName <em>SIB Name</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.ProjectInfo#getAutoDetect <em>Auto Detect</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.ProjectInfo#getIpAddress <em>Ip Address</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.ProjectInfo#getPort <em>Port</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getProjectInfo()
 * @model
 * @generated
 */
public interface ProjectInfo extends EObject {
	/**
	 * Returns the value of the '<em><b>Project Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Project Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Project Name</em>' attribute.
	 * @see #setProjectName(String)
	 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getProjectInfo_ProjectName()
	 * @model
	 * @generated
	 */
	String getProjectName();

	/**
	 * Sets the value of the '{@link org.smool.sdk.ontmodel.ontmodel.ProjectInfo#getProjectName <em>Project Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Project Name</em>' attribute.
	 * @see #getProjectName()
	 * @generated
	 */
	void setProjectName(String value);

	/**
	 * Returns the value of the '<em><b>SIB Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SIB Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SIB Name</em>' attribute.
	 * @see #setSIBName(String)
	 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getProjectInfo_SIBName()
	 * @model
	 * @generated
	 */
	String getSIBName();

	/**
	 * Sets the value of the '{@link org.smool.sdk.ontmodel.ontmodel.ProjectInfo#getSIBName <em>SIB Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SIB Name</em>' attribute.
	 * @see #getSIBName()
	 * @generated
	 */
	void setSIBName(String value);

	/**
	 * Returns the value of the '<em><b>Auto Detect</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Auto Detect</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Auto Detect</em>' attribute.
	 * @see #setAutoDetect(Boolean)
	 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getProjectInfo_AutoDetect()
	 * @model default="true"
	 * @generated
	 */
	Boolean getAutoDetect();

	/**
	 * Sets the value of the '{@link org.smool.sdk.ontmodel.ontmodel.ProjectInfo#getAutoDetect <em>Auto Detect</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Auto Detect</em>' attribute.
	 * @see #getAutoDetect()
	 * @generated
	 */
	void setAutoDetect(Boolean value);

	/**
	 * Returns the value of the '<em><b>Ip Address</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ip Address</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ip Address</em>' attribute.
	 * @see #setIpAddress(String)
	 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getProjectInfo_IpAddress()
	 * @model
	 * @generated
	 */
	String getIpAddress();

	/**
	 * Sets the value of the '{@link org.smool.sdk.ontmodel.ontmodel.ProjectInfo#getIpAddress <em>Ip Address</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ip Address</em>' attribute.
	 * @see #getIpAddress()
	 * @generated
	 */
	void setIpAddress(String value);

	/**
	 * Returns the value of the '<em><b>Port</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port</em>' attribute.
	 * @see #setPort(String)
	 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getProjectInfo_Port()
	 * @model
	 * @generated
	 */
	String getPort();

	/**
	 * Sets the value of the '{@link org.smool.sdk.ontmodel.ontmodel.ProjectInfo#getPort <em>Port</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port</em>' attribute.
	 * @see #getPort()
	 * @generated
	 */
	void setPort(String value);

} // ProjectInfo
