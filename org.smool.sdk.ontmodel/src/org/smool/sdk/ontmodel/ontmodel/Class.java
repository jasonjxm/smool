/**
 */
package org.smool.sdk.ontmodel.ontmodel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.Class#getURI <em>URI</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.Class#getAttributes <em>Attributes</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.Class#getRelationships <em>Relationships</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.Class#getGenerationType <em>Generation Type</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getClass_()
 * @model
 * @generated
 */
public interface Class extends EObject {
	/**
	 * Returns the value of the '<em><b>URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>URI</em>' attribute.
	 * @see #setURI(String)
	 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getClass_URI()
	 * @model
	 * @generated
	 */
	String getURI();

	/**
	 * Sets the value of the '{@link org.smool.sdk.ontmodel.ontmodel.Class#getURI <em>URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>URI</em>' attribute.
	 * @see #getURI()
	 * @generated
	 */
	void setURI(String value);

	/**
	 * Returns the value of the '<em><b>Attributes</b></em>' containment reference list.
	 * The list contents are of type {@link org.smool.sdk.ontmodel.ontmodel.Attribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attributes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attributes</em>' containment reference list.
	 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getClass_Attributes()
	 * @model containment="true"
	 * @generated
	 */
	EList<Attribute> getAttributes();

	/**
	 * Returns the value of the '<em><b>Relationships</b></em>' containment reference list.
	 * The list contents are of type {@link org.smool.sdk.ontmodel.ontmodel.Relationship}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relationships</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relationships</em>' containment reference list.
	 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getClass_Relationships()
	 * @model containment="true"
	 * @generated
	 */
	EList<Relationship> getRelationships();

	/**
	 * Returns the value of the '<em><b>Generation Type</b></em>' attribute.
	 * The default value is <code>"None"</code>.
	 * The literals are from the enumeration {@link org.smool.sdk.ontmodel.ontmodel.GenerationTypes}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generation Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generation Type</em>' attribute.
	 * @see org.smool.sdk.ontmodel.ontmodel.GenerationTypes
	 * @see #setGenerationType(GenerationTypes)
	 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getClass_GenerationType()
	 * @model default="None"
	 * @generated
	 */
	GenerationTypes getGenerationType();

	/**
	 * Sets the value of the '{@link org.smool.sdk.ontmodel.ontmodel.Class#getGenerationType <em>Generation Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generation Type</em>' attribute.
	 * @see org.smool.sdk.ontmodel.ontmodel.GenerationTypes
	 * @see #getGenerationType()
	 * @generated
	 */
	void setGenerationType(GenerationTypes value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void recursivelySetGenerationType(GenerationTypes type);

} // Class
