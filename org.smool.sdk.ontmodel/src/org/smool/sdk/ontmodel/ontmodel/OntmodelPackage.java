/**
 */
package org.smool.sdk.ontmodel.ontmodel;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelFactory
 * @model kind="package"
 *        annotation="emf.gen basePackage='org.smool.sdk.ontmodel'"
 * @generated
 */
public interface OntmodelPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ontmodel";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.smool.org/sdk/ontmodel/Ontmodel";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ontmodel";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OntmodelPackage eINSTANCE = org.smool.sdk.ontmodel.ontmodel.impl.OntmodelPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.smool.sdk.ontmodel.ontmodel.impl.OntologyImpl <em>Ontology</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.smool.sdk.ontmodel.ontmodel.impl.OntologyImpl
	 * @see org.smool.sdk.ontmodel.ontmodel.impl.OntmodelPackageImpl#getOntology()
	 * @generated
	 */
	int ONTOLOGY = 0;

	/**
	 * The feature id for the '<em><b>URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__URI = 0;

	/**
	 * The feature id for the '<em><b>Name Space</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__NAME_SPACE = 1;

	/**
	 * The feature id for the '<em><b>Classes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__CLASSES = 2;

	/**
	 * The feature id for the '<em><b>Project Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__PROJECT_INFO = 3;

	/**
	 * The feature id for the '<em><b>Imported Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__IMPORTED_ELEMENTS = 4;

	/**
	 * The feature id for the '<em><b>Unprocessed Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__UNPROCESSED_PROPERTIES = 5;

	/**
	 * The feature id for the '<em><b>Unprocessable Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY__UNPROCESSABLE_PROPERTIES = 6;

	/**
	 * The number of structural features of the '<em>Ontology</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONTOLOGY_FEATURE_COUNT = 7;

	/**
	 * The meta object id for the '{@link org.smool.sdk.ontmodel.ontmodel.impl.ClassImpl <em>Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.smool.sdk.ontmodel.ontmodel.impl.ClassImpl
	 * @see org.smool.sdk.ontmodel.ontmodel.impl.OntmodelPackageImpl#getClass_()
	 * @generated
	 */
	int CLASS = 1;

	/**
	 * The feature id for the '<em><b>URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS__URI = 0;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS__ATTRIBUTES = 1;

	/**
	 * The feature id for the '<em><b>Relationships</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS__RELATIONSHIPS = 2;

	/**
	 * The feature id for the '<em><b>Generation Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS__GENERATION_TYPE = 3;

	/**
	 * The number of structural features of the '<em>Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link org.smool.sdk.ontmodel.ontmodel.impl.RelationshipImpl <em>Relationship</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.smool.sdk.ontmodel.ontmodel.impl.RelationshipImpl
	 * @see org.smool.sdk.ontmodel.ontmodel.impl.OntmodelPackageImpl#getRelationship()
	 * @generated
	 */
	int RELATIONSHIP = 2;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP__TYPE = 0;

	/**
	 * The feature id for the '<em><b>Arg</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP__ARG = 1;

	/**
	 * The feature id for the '<em><b>Reference Ontology</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP__REFERENCE_ONTOLOGY = 2;

	/**
	 * The number of structural features of the '<em>Relationship</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link org.smool.sdk.ontmodel.ontmodel.impl.AttributeImpl <em>Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.smool.sdk.ontmodel.ontmodel.impl.AttributeImpl
	 * @see org.smool.sdk.ontmodel.ontmodel.impl.OntmodelPackageImpl#getAttribute()
	 * @generated
	 */
	int ATTRIBUTE = 3;

	/**
	 * The feature id for the '<em><b>URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE__URI = 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE__TYPE = 1;

	/**
	 * The feature id for the '<em><b>Range</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE__RANGE = 2;

	/**
	 * The feature id for the '<em><b>Range Ontology</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE__RANGE_ONTOLOGY = 3;

	/**
	 * The feature id for the '<em><b>Cardinality</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE__CARDINALITY = 4;

	/**
	 * The number of structural features of the '<em>Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link org.smool.sdk.ontmodel.ontmodel.impl.NameSpaceImpl <em>Name Space</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.smool.sdk.ontmodel.ontmodel.impl.NameSpaceImpl
	 * @see org.smool.sdk.ontmodel.ontmodel.impl.OntmodelPackageImpl#getNameSpace()
	 * @generated
	 */
	int NAME_SPACE = 4;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE__ID = 0;

	/**
	 * The feature id for the '<em><b>URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE__URI = 1;

	/**
	 * The number of structural features of the '<em>Name Space</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.smool.sdk.ontmodel.ontmodel.impl.ProjectInfoImpl <em>Project Info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.smool.sdk.ontmodel.ontmodel.impl.ProjectInfoImpl
	 * @see org.smool.sdk.ontmodel.ontmodel.impl.OntmodelPackageImpl#getProjectInfo()
	 * @generated
	 */
	int PROJECT_INFO = 5;

	/**
	 * The feature id for the '<em><b>Project Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT_INFO__PROJECT_NAME = 0;

	/**
	 * The feature id for the '<em><b>SIB Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT_INFO__SIB_NAME = 1;

	/**
	 * The feature id for the '<em><b>Auto Detect</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT_INFO__AUTO_DETECT = 2;

	/**
	 * The feature id for the '<em><b>Ip Address</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT_INFO__IP_ADDRESS = 3;

	/**
	 * The feature id for the '<em><b>Port</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT_INFO__PORT = 4;

	/**
	 * The number of structural features of the '<em>Project Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT_INFO_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link org.smool.sdk.ontmodel.ontmodel.impl.ImportedElementImpl <em>Imported Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.smool.sdk.ontmodel.ontmodel.impl.ImportedElementImpl
	 * @see org.smool.sdk.ontmodel.ontmodel.impl.OntmodelPackageImpl#getImportedElement()
	 * @generated
	 */
	int IMPORTED_ELEMENT = 6;

	/**
	 * The feature id for the '<em><b>Ont Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORTED_ELEMENT__ONT_NAME = 0;

	/**
	 * The feature id for the '<em><b>Classes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORTED_ELEMENT__CLASSES = 1;

	/**
	 * The feature id for the '<em><b>Ont URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORTED_ELEMENT__ONT_URI = 2;

	/**
	 * The number of structural features of the '<em>Imported Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORTED_ELEMENT_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link org.smool.sdk.ontmodel.ontmodel.impl.UnprocessedPropertyImpl <em>Unprocessed Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.smool.sdk.ontmodel.ontmodel.impl.UnprocessedPropertyImpl
	 * @see org.smool.sdk.ontmodel.ontmodel.impl.OntmodelPackageImpl#getUnprocessedProperty()
	 * @generated
	 */
	int UNPROCESSED_PROPERTY = 7;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNPROCESSED_PROPERTY__ID = 0;

	/**
	 * The feature id for the '<em><b>Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNPROCESSED_PROPERTY__URI = 1;

	/**
	 * The feature id for the '<em><b>Super Props</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNPROCESSED_PROPERTY__SUPER_PROPS = 2;

	/**
	 * The feature id for the '<em><b>Range</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNPROCESSED_PROPERTY__RANGE = 3;

	/**
	 * The feature id for the '<em><b>Range Ont</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNPROCESSED_PROPERTY__RANGE_ONT = 4;

	/**
	 * The number of structural features of the '<em>Unprocessed Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNPROCESSED_PROPERTY_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link org.smool.sdk.ontmodel.ontmodel.impl.UnprocessablePropertyImpl <em>Unprocessable Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.smool.sdk.ontmodel.ontmodel.impl.UnprocessablePropertyImpl
	 * @see org.smool.sdk.ontmodel.ontmodel.impl.OntmodelPackageImpl#getUnprocessableProperty()
	 * @generated
	 */
	int UNPROCESSABLE_PROPERTY = 8;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNPROCESSABLE_PROPERTY__ID = 0;

	/**
	 * The feature id for the '<em><b>Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNPROCESSABLE_PROPERTY__URI = 1;

	/**
	 * The feature id for the '<em><b>Super Props</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNPROCESSABLE_PROPERTY__SUPER_PROPS = 2;

	/**
	 * The feature id for the '<em><b>Range</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNPROCESSABLE_PROPERTY__RANGE = 3;

	/**
	 * The feature id for the '<em><b>Range Ont</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNPROCESSABLE_PROPERTY__RANGE_ONT = 4;

	/**
	 * The number of structural features of the '<em>Unprocessable Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNPROCESSABLE_PROPERTY_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link org.smool.sdk.ontmodel.ontmodel.AttributeTypes <em>Attribute Types</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.smool.sdk.ontmodel.ontmodel.AttributeTypes
	 * @see org.smool.sdk.ontmodel.ontmodel.impl.OntmodelPackageImpl#getAttributeTypes()
	 * @generated
	 */
	int ATTRIBUTE_TYPES = 9;

	/**
	 * The meta object id for the '{@link org.smool.sdk.ontmodel.ontmodel.RelationshipTypes <em>Relationship Types</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.smool.sdk.ontmodel.ontmodel.RelationshipTypes
	 * @see org.smool.sdk.ontmodel.ontmodel.impl.OntmodelPackageImpl#getRelationshipTypes()
	 * @generated
	 */
	int RELATIONSHIP_TYPES = 10;

	/**
	 * The meta object id for the '{@link org.smool.sdk.ontmodel.ontmodel.CardinalityTypes <em>Cardinality Types</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.smool.sdk.ontmodel.ontmodel.CardinalityTypes
	 * @see org.smool.sdk.ontmodel.ontmodel.impl.OntmodelPackageImpl#getCardinalityTypes()
	 * @generated
	 */
	int CARDINALITY_TYPES = 11;

	/**
	 * The meta object id for the '{@link org.smool.sdk.ontmodel.ontmodel.GenerationTypes <em>Generation Types</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.smool.sdk.ontmodel.ontmodel.GenerationTypes
	 * @see org.smool.sdk.ontmodel.ontmodel.impl.OntmodelPackageImpl#getGenerationTypes()
	 * @generated
	 */
	int GENERATION_TYPES = 12;


	/**
	 * Returns the meta object for class '{@link org.smool.sdk.ontmodel.ontmodel.Ontology <em>Ontology</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ontology</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.Ontology
	 * @generated
	 */
	EClass getOntology();

	/**
	 * Returns the meta object for the attribute '{@link org.smool.sdk.ontmodel.ontmodel.Ontology#getURI <em>URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>URI</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.Ontology#getURI()
	 * @see #getOntology()
	 * @generated
	 */
	EAttribute getOntology_URI();

	/**
	 * Returns the meta object for the containment reference '{@link org.smool.sdk.ontmodel.ontmodel.Ontology#getNameSpace <em>Name Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Name Space</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.Ontology#getNameSpace()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_NameSpace();

	/**
	 * Returns the meta object for the containment reference list '{@link org.smool.sdk.ontmodel.ontmodel.Ontology#getClasses <em>Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Classes</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.Ontology#getClasses()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_Classes();

	/**
	 * Returns the meta object for the containment reference '{@link org.smool.sdk.ontmodel.ontmodel.Ontology#getProjectInfo <em>Project Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Project Info</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.Ontology#getProjectInfo()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_ProjectInfo();

	/**
	 * Returns the meta object for the containment reference list '{@link org.smool.sdk.ontmodel.ontmodel.Ontology#getImportedElements <em>Imported Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Imported Elements</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.Ontology#getImportedElements()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_ImportedElements();

	/**
	 * Returns the meta object for the containment reference list '{@link org.smool.sdk.ontmodel.ontmodel.Ontology#getUnprocessedProperties <em>Unprocessed Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Unprocessed Properties</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.Ontology#getUnprocessedProperties()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_UnprocessedProperties();

	/**
	 * Returns the meta object for the containment reference list '{@link org.smool.sdk.ontmodel.ontmodel.Ontology#getUnprocessableProperties <em>Unprocessable Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Unprocessable Properties</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.Ontology#getUnprocessableProperties()
	 * @see #getOntology()
	 * @generated
	 */
	EReference getOntology_UnprocessableProperties();

	/**
	 * Returns the meta object for class '{@link org.smool.sdk.ontmodel.ontmodel.Class <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Class</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.Class
	 * @generated
	 */
	EClass getClass_();

	/**
	 * Returns the meta object for the attribute '{@link org.smool.sdk.ontmodel.ontmodel.Class#getURI <em>URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>URI</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.Class#getURI()
	 * @see #getClass_()
	 * @generated
	 */
	EAttribute getClass_URI();

	/**
	 * Returns the meta object for the containment reference list '{@link org.smool.sdk.ontmodel.ontmodel.Class#getAttributes <em>Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Attributes</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.Class#getAttributes()
	 * @see #getClass_()
	 * @generated
	 */
	EReference getClass_Attributes();

	/**
	 * Returns the meta object for the containment reference list '{@link org.smool.sdk.ontmodel.ontmodel.Class#getRelationships <em>Relationships</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Relationships</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.Class#getRelationships()
	 * @see #getClass_()
	 * @generated
	 */
	EReference getClass_Relationships();

	/**
	 * Returns the meta object for the attribute '{@link org.smool.sdk.ontmodel.ontmodel.Class#getGenerationType <em>Generation Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Generation Type</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.Class#getGenerationType()
	 * @see #getClass_()
	 * @generated
	 */
	EAttribute getClass_GenerationType();

	/**
	 * Returns the meta object for class '{@link org.smool.sdk.ontmodel.ontmodel.Relationship <em>Relationship</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Relationship</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.Relationship
	 * @generated
	 */
	EClass getRelationship();

	/**
	 * Returns the meta object for the attribute '{@link org.smool.sdk.ontmodel.ontmodel.Relationship#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.Relationship#getType()
	 * @see #getRelationship()
	 * @generated
	 */
	EAttribute getRelationship_Type();

	/**
	 * Returns the meta object for the attribute '{@link org.smool.sdk.ontmodel.ontmodel.Relationship#getArg <em>Arg</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Arg</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.Relationship#getArg()
	 * @see #getRelationship()
	 * @generated
	 */
	EAttribute getRelationship_Arg();

	/**
	 * Returns the meta object for the attribute '{@link org.smool.sdk.ontmodel.ontmodel.Relationship#getReferenceOntology <em>Reference Ontology</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Reference Ontology</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.Relationship#getReferenceOntology()
	 * @see #getRelationship()
	 * @generated
	 */
	EAttribute getRelationship_ReferenceOntology();

	/**
	 * Returns the meta object for class '{@link org.smool.sdk.ontmodel.ontmodel.Attribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attribute</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.Attribute
	 * @generated
	 */
	EClass getAttribute();

	/**
	 * Returns the meta object for the attribute '{@link org.smool.sdk.ontmodel.ontmodel.Attribute#getURI <em>URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>URI</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.Attribute#getURI()
	 * @see #getAttribute()
	 * @generated
	 */
	EAttribute getAttribute_URI();

	/**
	 * Returns the meta object for the attribute '{@link org.smool.sdk.ontmodel.ontmodel.Attribute#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.Attribute#getType()
	 * @see #getAttribute()
	 * @generated
	 */
	EAttribute getAttribute_Type();

	/**
	 * Returns the meta object for the attribute '{@link org.smool.sdk.ontmodel.ontmodel.Attribute#getRange <em>Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Range</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.Attribute#getRange()
	 * @see #getAttribute()
	 * @generated
	 */
	EAttribute getAttribute_Range();

	/**
	 * Returns the meta object for the attribute '{@link org.smool.sdk.ontmodel.ontmodel.Attribute#getRangeOntology <em>Range Ontology</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Range Ontology</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.Attribute#getRangeOntology()
	 * @see #getAttribute()
	 * @generated
	 */
	EAttribute getAttribute_RangeOntology();

	/**
	 * Returns the meta object for the attribute '{@link org.smool.sdk.ontmodel.ontmodel.Attribute#getCardinality <em>Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cardinality</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.Attribute#getCardinality()
	 * @see #getAttribute()
	 * @generated
	 */
	EAttribute getAttribute_Cardinality();

	/**
	 * Returns the meta object for class '{@link org.smool.sdk.ontmodel.ontmodel.NameSpace <em>Name Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Name Space</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.NameSpace
	 * @generated
	 */
	EClass getNameSpace();

	/**
	 * Returns the meta object for the attribute '{@link org.smool.sdk.ontmodel.ontmodel.NameSpace#getID <em>ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ID</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.NameSpace#getID()
	 * @see #getNameSpace()
	 * @generated
	 */
	EAttribute getNameSpace_ID();

	/**
	 * Returns the meta object for the attribute '{@link org.smool.sdk.ontmodel.ontmodel.NameSpace#getURI <em>URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>URI</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.NameSpace#getURI()
	 * @see #getNameSpace()
	 * @generated
	 */
	EAttribute getNameSpace_URI();

	/**
	 * Returns the meta object for class '{@link org.smool.sdk.ontmodel.ontmodel.ProjectInfo <em>Project Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Project Info</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.ProjectInfo
	 * @generated
	 */
	EClass getProjectInfo();

	/**
	 * Returns the meta object for the attribute '{@link org.smool.sdk.ontmodel.ontmodel.ProjectInfo#getProjectName <em>Project Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Project Name</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.ProjectInfo#getProjectName()
	 * @see #getProjectInfo()
	 * @generated
	 */
	EAttribute getProjectInfo_ProjectName();

	/**
	 * Returns the meta object for the attribute '{@link org.smool.sdk.ontmodel.ontmodel.ProjectInfo#getSIBName <em>SIB Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>SIB Name</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.ProjectInfo#getSIBName()
	 * @see #getProjectInfo()
	 * @generated
	 */
	EAttribute getProjectInfo_SIBName();

	/**
	 * Returns the meta object for the attribute '{@link org.smool.sdk.ontmodel.ontmodel.ProjectInfo#getAutoDetect <em>Auto Detect</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Auto Detect</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.ProjectInfo#getAutoDetect()
	 * @see #getProjectInfo()
	 * @generated
	 */
	EAttribute getProjectInfo_AutoDetect();

	/**
	 * Returns the meta object for the attribute '{@link org.smool.sdk.ontmodel.ontmodel.ProjectInfo#getIpAddress <em>Ip Address</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ip Address</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.ProjectInfo#getIpAddress()
	 * @see #getProjectInfo()
	 * @generated
	 */
	EAttribute getProjectInfo_IpAddress();

	/**
	 * Returns the meta object for the attribute '{@link org.smool.sdk.ontmodel.ontmodel.ProjectInfo#getPort <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Port</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.ProjectInfo#getPort()
	 * @see #getProjectInfo()
	 * @generated
	 */
	EAttribute getProjectInfo_Port();

	/**
	 * Returns the meta object for class '{@link org.smool.sdk.ontmodel.ontmodel.ImportedElement <em>Imported Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Imported Element</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.ImportedElement
	 * @generated
	 */
	EClass getImportedElement();

	/**
	 * Returns the meta object for the attribute '{@link org.smool.sdk.ontmodel.ontmodel.ImportedElement#getOntName <em>Ont Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ont Name</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.ImportedElement#getOntName()
	 * @see #getImportedElement()
	 * @generated
	 */
	EAttribute getImportedElement_OntName();

	/**
	 * Returns the meta object for the attribute list '{@link org.smool.sdk.ontmodel.ontmodel.ImportedElement#getClasses <em>Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Classes</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.ImportedElement#getClasses()
	 * @see #getImportedElement()
	 * @generated
	 */
	EAttribute getImportedElement_Classes();

	/**
	 * Returns the meta object for the attribute '{@link org.smool.sdk.ontmodel.ontmodel.ImportedElement#getOntURI <em>Ont URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ont URI</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.ImportedElement#getOntURI()
	 * @see #getImportedElement()
	 * @generated
	 */
	EAttribute getImportedElement_OntURI();

	/**
	 * Returns the meta object for class '{@link org.smool.sdk.ontmodel.ontmodel.UnprocessedProperty <em>Unprocessed Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unprocessed Property</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.UnprocessedProperty
	 * @generated
	 */
	EClass getUnprocessedProperty();

	/**
	 * Returns the meta object for the attribute '{@link org.smool.sdk.ontmodel.ontmodel.UnprocessedProperty#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.UnprocessedProperty#getId()
	 * @see #getUnprocessedProperty()
	 * @generated
	 */
	EAttribute getUnprocessedProperty_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.smool.sdk.ontmodel.ontmodel.UnprocessedProperty#getUri <em>Uri</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Uri</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.UnprocessedProperty#getUri()
	 * @see #getUnprocessedProperty()
	 * @generated
	 */
	EAttribute getUnprocessedProperty_Uri();

	/**
	 * Returns the meta object for the attribute list '{@link org.smool.sdk.ontmodel.ontmodel.UnprocessedProperty#getSuperProps <em>Super Props</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Super Props</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.UnprocessedProperty#getSuperProps()
	 * @see #getUnprocessedProperty()
	 * @generated
	 */
	EAttribute getUnprocessedProperty_SuperProps();

	/**
	 * Returns the meta object for the attribute '{@link org.smool.sdk.ontmodel.ontmodel.UnprocessedProperty#getRange <em>Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Range</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.UnprocessedProperty#getRange()
	 * @see #getUnprocessedProperty()
	 * @generated
	 */
	EAttribute getUnprocessedProperty_Range();

	/**
	 * Returns the meta object for the attribute '{@link org.smool.sdk.ontmodel.ontmodel.UnprocessedProperty#getRangeOnt <em>Range Ont</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Range Ont</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.UnprocessedProperty#getRangeOnt()
	 * @see #getUnprocessedProperty()
	 * @generated
	 */
	EAttribute getUnprocessedProperty_RangeOnt();

	/**
	 * Returns the meta object for class '{@link org.smool.sdk.ontmodel.ontmodel.UnprocessableProperty <em>Unprocessable Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unprocessable Property</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.UnprocessableProperty
	 * @generated
	 */
	EClass getUnprocessableProperty();

	/**
	 * Returns the meta object for the attribute '{@link org.smool.sdk.ontmodel.ontmodel.UnprocessableProperty#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.UnprocessableProperty#getId()
	 * @see #getUnprocessableProperty()
	 * @generated
	 */
	EAttribute getUnprocessableProperty_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.smool.sdk.ontmodel.ontmodel.UnprocessableProperty#getUri <em>Uri</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Uri</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.UnprocessableProperty#getUri()
	 * @see #getUnprocessableProperty()
	 * @generated
	 */
	EAttribute getUnprocessableProperty_Uri();

	/**
	 * Returns the meta object for the attribute list '{@link org.smool.sdk.ontmodel.ontmodel.UnprocessableProperty#getSuperProps <em>Super Props</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Super Props</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.UnprocessableProperty#getSuperProps()
	 * @see #getUnprocessableProperty()
	 * @generated
	 */
	EAttribute getUnprocessableProperty_SuperProps();

	/**
	 * Returns the meta object for the attribute '{@link org.smool.sdk.ontmodel.ontmodel.UnprocessableProperty#getRange <em>Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Range</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.UnprocessableProperty#getRange()
	 * @see #getUnprocessableProperty()
	 * @generated
	 */
	EAttribute getUnprocessableProperty_Range();

	/**
	 * Returns the meta object for the attribute '{@link org.smool.sdk.ontmodel.ontmodel.UnprocessableProperty#getRangeOnt <em>Range Ont</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Range Ont</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.UnprocessableProperty#getRangeOnt()
	 * @see #getUnprocessableProperty()
	 * @generated
	 */
	EAttribute getUnprocessableProperty_RangeOnt();

	/**
	 * Returns the meta object for enum '{@link org.smool.sdk.ontmodel.ontmodel.AttributeTypes <em>Attribute Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Attribute Types</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.AttributeTypes
	 * @generated
	 */
	EEnum getAttributeTypes();

	/**
	 * Returns the meta object for enum '{@link org.smool.sdk.ontmodel.ontmodel.RelationshipTypes <em>Relationship Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Relationship Types</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.RelationshipTypes
	 * @generated
	 */
	EEnum getRelationshipTypes();

	/**
	 * Returns the meta object for enum '{@link org.smool.sdk.ontmodel.ontmodel.CardinalityTypes <em>Cardinality Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Cardinality Types</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.CardinalityTypes
	 * @generated
	 */
	EEnum getCardinalityTypes();

	/**
	 * Returns the meta object for enum '{@link org.smool.sdk.ontmodel.ontmodel.GenerationTypes <em>Generation Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Generation Types</em>'.
	 * @see org.smool.sdk.ontmodel.ontmodel.GenerationTypes
	 * @generated
	 */
	EEnum getGenerationTypes();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	OntmodelFactory getOntmodelFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.smool.sdk.ontmodel.ontmodel.impl.OntologyImpl <em>Ontology</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.smool.sdk.ontmodel.ontmodel.impl.OntologyImpl
		 * @see org.smool.sdk.ontmodel.ontmodel.impl.OntmodelPackageImpl#getOntology()
		 * @generated
		 */
		EClass ONTOLOGY = eINSTANCE.getOntology();

		/**
		 * The meta object literal for the '<em><b>URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ONTOLOGY__URI = eINSTANCE.getOntology_URI();

		/**
		 * The meta object literal for the '<em><b>Name Space</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ONTOLOGY__NAME_SPACE = eINSTANCE.getOntology_NameSpace();

		/**
		 * The meta object literal for the '<em><b>Classes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ONTOLOGY__CLASSES = eINSTANCE.getOntology_Classes();

		/**
		 * The meta object literal for the '<em><b>Project Info</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ONTOLOGY__PROJECT_INFO = eINSTANCE.getOntology_ProjectInfo();

		/**
		 * The meta object literal for the '<em><b>Imported Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ONTOLOGY__IMPORTED_ELEMENTS = eINSTANCE.getOntology_ImportedElements();

		/**
		 * The meta object literal for the '<em><b>Unprocessed Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ONTOLOGY__UNPROCESSED_PROPERTIES = eINSTANCE.getOntology_UnprocessedProperties();

		/**
		 * The meta object literal for the '<em><b>Unprocessable Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ONTOLOGY__UNPROCESSABLE_PROPERTIES = eINSTANCE.getOntology_UnprocessableProperties();

		/**
		 * The meta object literal for the '{@link org.smool.sdk.ontmodel.ontmodel.impl.ClassImpl <em>Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.smool.sdk.ontmodel.ontmodel.impl.ClassImpl
		 * @see org.smool.sdk.ontmodel.ontmodel.impl.OntmodelPackageImpl#getClass_()
		 * @generated
		 */
		EClass CLASS = eINSTANCE.getClass_();

		/**
		 * The meta object literal for the '<em><b>URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLASS__URI = eINSTANCE.getClass_URI();

		/**
		 * The meta object literal for the '<em><b>Attributes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS__ATTRIBUTES = eINSTANCE.getClass_Attributes();

		/**
		 * The meta object literal for the '<em><b>Relationships</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS__RELATIONSHIPS = eINSTANCE.getClass_Relationships();

		/**
		 * The meta object literal for the '<em><b>Generation Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLASS__GENERATION_TYPE = eINSTANCE.getClass_GenerationType();

		/**
		 * The meta object literal for the '{@link org.smool.sdk.ontmodel.ontmodel.impl.RelationshipImpl <em>Relationship</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.smool.sdk.ontmodel.ontmodel.impl.RelationshipImpl
		 * @see org.smool.sdk.ontmodel.ontmodel.impl.OntmodelPackageImpl#getRelationship()
		 * @generated
		 */
		EClass RELATIONSHIP = eINSTANCE.getRelationship();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RELATIONSHIP__TYPE = eINSTANCE.getRelationship_Type();

		/**
		 * The meta object literal for the '<em><b>Arg</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RELATIONSHIP__ARG = eINSTANCE.getRelationship_Arg();

		/**
		 * The meta object literal for the '<em><b>Reference Ontology</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RELATIONSHIP__REFERENCE_ONTOLOGY = eINSTANCE.getRelationship_ReferenceOntology();

		/**
		 * The meta object literal for the '{@link org.smool.sdk.ontmodel.ontmodel.impl.AttributeImpl <em>Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.smool.sdk.ontmodel.ontmodel.impl.AttributeImpl
		 * @see org.smool.sdk.ontmodel.ontmodel.impl.OntmodelPackageImpl#getAttribute()
		 * @generated
		 */
		EClass ATTRIBUTE = eINSTANCE.getAttribute();

		/**
		 * The meta object literal for the '<em><b>URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTE__URI = eINSTANCE.getAttribute_URI();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTE__TYPE = eINSTANCE.getAttribute_Type();

		/**
		 * The meta object literal for the '<em><b>Range</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTE__RANGE = eINSTANCE.getAttribute_Range();

		/**
		 * The meta object literal for the '<em><b>Range Ontology</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTE__RANGE_ONTOLOGY = eINSTANCE.getAttribute_RangeOntology();

		/**
		 * The meta object literal for the '<em><b>Cardinality</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTE__CARDINALITY = eINSTANCE.getAttribute_Cardinality();

		/**
		 * The meta object literal for the '{@link org.smool.sdk.ontmodel.ontmodel.impl.NameSpaceImpl <em>Name Space</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.smool.sdk.ontmodel.ontmodel.impl.NameSpaceImpl
		 * @see org.smool.sdk.ontmodel.ontmodel.impl.OntmodelPackageImpl#getNameSpace()
		 * @generated
		 */
		EClass NAME_SPACE = eINSTANCE.getNameSpace();

		/**
		 * The meta object literal for the '<em><b>ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAME_SPACE__ID = eINSTANCE.getNameSpace_ID();

		/**
		 * The meta object literal for the '<em><b>URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAME_SPACE__URI = eINSTANCE.getNameSpace_URI();

		/**
		 * The meta object literal for the '{@link org.smool.sdk.ontmodel.ontmodel.impl.ProjectInfoImpl <em>Project Info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.smool.sdk.ontmodel.ontmodel.impl.ProjectInfoImpl
		 * @see org.smool.sdk.ontmodel.ontmodel.impl.OntmodelPackageImpl#getProjectInfo()
		 * @generated
		 */
		EClass PROJECT_INFO = eINSTANCE.getProjectInfo();

		/**
		 * The meta object literal for the '<em><b>Project Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROJECT_INFO__PROJECT_NAME = eINSTANCE.getProjectInfo_ProjectName();

		/**
		 * The meta object literal for the '<em><b>SIB Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROJECT_INFO__SIB_NAME = eINSTANCE.getProjectInfo_SIBName();

		/**
		 * The meta object literal for the '<em><b>Auto Detect</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROJECT_INFO__AUTO_DETECT = eINSTANCE.getProjectInfo_AutoDetect();

		/**
		 * The meta object literal for the '<em><b>Ip Address</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROJECT_INFO__IP_ADDRESS = eINSTANCE.getProjectInfo_IpAddress();

		/**
		 * The meta object literal for the '<em><b>Port</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROJECT_INFO__PORT = eINSTANCE.getProjectInfo_Port();

		/**
		 * The meta object literal for the '{@link org.smool.sdk.ontmodel.ontmodel.impl.ImportedElementImpl <em>Imported Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.smool.sdk.ontmodel.ontmodel.impl.ImportedElementImpl
		 * @see org.smool.sdk.ontmodel.ontmodel.impl.OntmodelPackageImpl#getImportedElement()
		 * @generated
		 */
		EClass IMPORTED_ELEMENT = eINSTANCE.getImportedElement();

		/**
		 * The meta object literal for the '<em><b>Ont Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMPORTED_ELEMENT__ONT_NAME = eINSTANCE.getImportedElement_OntName();

		/**
		 * The meta object literal for the '<em><b>Classes</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMPORTED_ELEMENT__CLASSES = eINSTANCE.getImportedElement_Classes();

		/**
		 * The meta object literal for the '<em><b>Ont URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMPORTED_ELEMENT__ONT_URI = eINSTANCE.getImportedElement_OntURI();

		/**
		 * The meta object literal for the '{@link org.smool.sdk.ontmodel.ontmodel.impl.UnprocessedPropertyImpl <em>Unprocessed Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.smool.sdk.ontmodel.ontmodel.impl.UnprocessedPropertyImpl
		 * @see org.smool.sdk.ontmodel.ontmodel.impl.OntmodelPackageImpl#getUnprocessedProperty()
		 * @generated
		 */
		EClass UNPROCESSED_PROPERTY = eINSTANCE.getUnprocessedProperty();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNPROCESSED_PROPERTY__ID = eINSTANCE.getUnprocessedProperty_Id();

		/**
		 * The meta object literal for the '<em><b>Uri</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNPROCESSED_PROPERTY__URI = eINSTANCE.getUnprocessedProperty_Uri();

		/**
		 * The meta object literal for the '<em><b>Super Props</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNPROCESSED_PROPERTY__SUPER_PROPS = eINSTANCE.getUnprocessedProperty_SuperProps();

		/**
		 * The meta object literal for the '<em><b>Range</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNPROCESSED_PROPERTY__RANGE = eINSTANCE.getUnprocessedProperty_Range();

		/**
		 * The meta object literal for the '<em><b>Range Ont</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNPROCESSED_PROPERTY__RANGE_ONT = eINSTANCE.getUnprocessedProperty_RangeOnt();

		/**
		 * The meta object literal for the '{@link org.smool.sdk.ontmodel.ontmodel.impl.UnprocessablePropertyImpl <em>Unprocessable Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.smool.sdk.ontmodel.ontmodel.impl.UnprocessablePropertyImpl
		 * @see org.smool.sdk.ontmodel.ontmodel.impl.OntmodelPackageImpl#getUnprocessableProperty()
		 * @generated
		 */
		EClass UNPROCESSABLE_PROPERTY = eINSTANCE.getUnprocessableProperty();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNPROCESSABLE_PROPERTY__ID = eINSTANCE.getUnprocessableProperty_Id();

		/**
		 * The meta object literal for the '<em><b>Uri</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNPROCESSABLE_PROPERTY__URI = eINSTANCE.getUnprocessableProperty_Uri();

		/**
		 * The meta object literal for the '<em><b>Super Props</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNPROCESSABLE_PROPERTY__SUPER_PROPS = eINSTANCE.getUnprocessableProperty_SuperProps();

		/**
		 * The meta object literal for the '<em><b>Range</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNPROCESSABLE_PROPERTY__RANGE = eINSTANCE.getUnprocessableProperty_Range();

		/**
		 * The meta object literal for the '<em><b>Range Ont</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNPROCESSABLE_PROPERTY__RANGE_ONT = eINSTANCE.getUnprocessableProperty_RangeOnt();

		/**
		 * The meta object literal for the '{@link org.smool.sdk.ontmodel.ontmodel.AttributeTypes <em>Attribute Types</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.smool.sdk.ontmodel.ontmodel.AttributeTypes
		 * @see org.smool.sdk.ontmodel.ontmodel.impl.OntmodelPackageImpl#getAttributeTypes()
		 * @generated
		 */
		EEnum ATTRIBUTE_TYPES = eINSTANCE.getAttributeTypes();

		/**
		 * The meta object literal for the '{@link org.smool.sdk.ontmodel.ontmodel.RelationshipTypes <em>Relationship Types</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.smool.sdk.ontmodel.ontmodel.RelationshipTypes
		 * @see org.smool.sdk.ontmodel.ontmodel.impl.OntmodelPackageImpl#getRelationshipTypes()
		 * @generated
		 */
		EEnum RELATIONSHIP_TYPES = eINSTANCE.getRelationshipTypes();

		/**
		 * The meta object literal for the '{@link org.smool.sdk.ontmodel.ontmodel.CardinalityTypes <em>Cardinality Types</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.smool.sdk.ontmodel.ontmodel.CardinalityTypes
		 * @see org.smool.sdk.ontmodel.ontmodel.impl.OntmodelPackageImpl#getCardinalityTypes()
		 * @generated
		 */
		EEnum CARDINALITY_TYPES = eINSTANCE.getCardinalityTypes();

		/**
		 * The meta object literal for the '{@link org.smool.sdk.ontmodel.ontmodel.GenerationTypes <em>Generation Types</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.smool.sdk.ontmodel.ontmodel.GenerationTypes
		 * @see org.smool.sdk.ontmodel.ontmodel.impl.OntmodelPackageImpl#getGenerationTypes()
		 * @generated
		 */
		EEnum GENERATION_TYPES = eINSTANCE.getGenerationTypes();

	}

} //OntmodelPackage
