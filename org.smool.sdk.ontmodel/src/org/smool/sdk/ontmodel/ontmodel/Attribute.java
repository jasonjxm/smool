/**
 */
package org.smool.sdk.ontmodel.ontmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.Attribute#getURI <em>URI</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.Attribute#getType <em>Type</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.Attribute#getRange <em>Range</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.Attribute#getRangeOntology <em>Range Ontology</em>}</li>
 *   <li>{@link org.smool.sdk.ontmodel.ontmodel.Attribute#getCardinality <em>Cardinality</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getAttribute()
 * @model
 * @generated
 */
public interface Attribute extends EObject {
	/**
	 * Returns the value of the '<em><b>URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>URI</em>' attribute.
	 * @see #setURI(String)
	 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getAttribute_URI()
	 * @model
	 * @generated
	 */
	String getURI();

	/**
	 * Sets the value of the '{@link org.smool.sdk.ontmodel.ontmodel.Attribute#getURI <em>URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>URI</em>' attribute.
	 * @see #getURI()
	 * @generated
	 */
	void setURI(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.smool.sdk.ontmodel.ontmodel.AttributeTypes}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see org.smool.sdk.ontmodel.ontmodel.AttributeTypes
	 * @see #setType(AttributeTypes)
	 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getAttribute_Type()
	 * @model
	 * @generated
	 */
	AttributeTypes getType();

	/**
	 * Sets the value of the '{@link org.smool.sdk.ontmodel.ontmodel.Attribute#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see org.smool.sdk.ontmodel.ontmodel.AttributeTypes
	 * @see #getType()
	 * @generated
	 */
	void setType(AttributeTypes value);

	/**
	 * Returns the value of the '<em><b>Range</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Range</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Range</em>' attribute.
	 * @see #setRange(String)
	 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getAttribute_Range()
	 * @model
	 * @generated
	 */
	String getRange();

	/**
	 * Sets the value of the '{@link org.smool.sdk.ontmodel.ontmodel.Attribute#getRange <em>Range</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Range</em>' attribute.
	 * @see #getRange()
	 * @generated
	 */
	void setRange(String value);

	/**
	 * Returns the value of the '<em><b>Range Ontology</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Range Ontology</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Range Ontology</em>' attribute.
	 * @see #setRangeOntology(String)
	 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getAttribute_RangeOntology()
	 * @model
	 * @generated
	 */
	String getRangeOntology();

	/**
	 * Sets the value of the '{@link org.smool.sdk.ontmodel.ontmodel.Attribute#getRangeOntology <em>Range Ontology</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Range Ontology</em>' attribute.
	 * @see #getRangeOntology()
	 * @generated
	 */
	void setRangeOntology(String value);

	/**
	 * Returns the value of the '<em><b>Cardinality</b></em>' attribute.
	 * The default value is <code>"Single"</code>.
	 * The literals are from the enumeration {@link org.smool.sdk.ontmodel.ontmodel.CardinalityTypes}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cardinality</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cardinality</em>' attribute.
	 * @see org.smool.sdk.ontmodel.ontmodel.CardinalityTypes
	 * @see #setCardinality(CardinalityTypes)
	 * @see org.smool.sdk.ontmodel.ontmodel.OntmodelPackage#getAttribute_Cardinality()
	 * @model default="Single"
	 * @generated
	 */
	CardinalityTypes getCardinality();

	/**
	 * Sets the value of the '{@link org.smool.sdk.ontmodel.ontmodel.Attribute#getCardinality <em>Cardinality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cardinality</em>' attribute.
	 * @see org.smool.sdk.ontmodel.ontmodel.CardinalityTypes
	 * @see #getCardinality()
	 * @generated
	 */
	void setCardinality(CardinalityTypes value);

} // Attribute
