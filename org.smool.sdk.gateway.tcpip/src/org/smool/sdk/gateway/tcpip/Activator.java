/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.gateway.tcpip;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Properties;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;
import org.smool.sdk.gateway.service.IGatewayFactory;
import org.smool.sdk.sib.SIBActivator;
import org.smool.sdk.sib.service.ISIB;


/**
 * TCP/IP gateway manager to connect KP with tcp/ip connectors with the SIB.
 * @author Raul Otaolea, Raul.Otaolea@tecnalia.com, Tecnalia-Tecnalia
 */
public class Activator implements BundleActivator {


	
	/** The manager responsible for creating/destroying TCP/IP Gateway instances */
	private TCPIPGatewayFactory manager;

	public Activator() {

	}
	
	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void start(BundleContext context) throws Exception {
		Dictionary props = new Hashtable();
		props.put("Gateway", "TCP/IP");

		manager = new TCPIPGatewayFactory(context);
		context.registerService(IGatewayFactory.class.getName(), manager, props);
		System.out.println("Manager for TCP/IP registered");
		ServiceTracker tracker = new ServiceTracker(context, ISIB.class.getName(), manager);
		tracker.open();
		
		// Create any TCP/IP gateways defined in the configuration
		Properties configuration = SIBActivator.getConfiguration();
		String [] gws = configuration.getProperty("CREATE_TCPIP_GW") != null ? configuration.getProperty("CREATE_TCPIP_GW").split(",") : new String [0];
		String [] autostart = configuration.getProperty("AUTOSTART") != null ? configuration.getProperty("AUTOSTART").split(",") : new String [0];
		for (String gw : gws) {
			String [] gwProps = gw.split(":");
			if (gwProps.length == 4) {
				String sib = gwProps[0];
				String gwName = gwProps[1];
				String ip = gwProps[2];
				String port = gwProps[3];
				
				ISIB sibInstance = SIBActivator.getSIB(sib); 
				if (sibInstance != null) {
					Properties cp = manager.getConfigurableProperties();
					
					// Fill the properties
					ArrayList<String> ipAddresses = new ArrayList<String>();
					ipAddresses.add(ip);
					
					cp.put("IPADDRESS", ip);
					cp.put("ADDRESSES", ipAddresses);
					cp.put("PORT", port);
					
					int id = manager.create(gwName, sibInstance, cp);
					// Start the gw if autostart required
					for (String autosib : autostart) {
						if (autosib.equals(sib)) {
							manager.getGateway(id).start();
						}
					}
				}
			}
			
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		// stop the server.
		manager.shutdown();
		manager = null;
		System.out.println("Manager for TCP/IP unregistered");
	}

}
