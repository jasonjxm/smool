/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/

package org.smool.sdk.gateway.tcpip.core;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

/**
 * SocketChannelReader.java
 * 
 * Handles reading from all clients using a <code>Selector</code> and delegate
 * them to the appropriate listeners.
 * 
 * @author Raul Otaolea, Raul.Otaolea@tecnalia.com, Tecnalia-Tecnalia
 */
public class SocketChannelReader extends Thread {

	/**
	 * Holder of socket connections detected elsewhere when writing messages for
	 * instance
	 */
	// public static ConcurrentHashMap<Integer, SocketChannel> OBSOLETE_CHANNELS =
	// new ConcurrentHashMap<>();
	public static HashMap<Integer, SocketChannel> OBSOLETE_CHANNELS = new HashMap<>();

	/** Pending connections */
	private LinkedList<ClientConnection> newClients;

	/** Indicates if the thread is running */
	private boolean running;

	/** The selector, multiplexes access to client channels */
	private Selector selector;

	/** Read buffer */
	private ByteBuffer readBuffer = ByteBuffer.allocate(32768);

	/** Listener to inform that a new TCP/IP connection has been created. */
	private IConnectionListener connectionlistener;

	/** Time to sleep between acceptances */
	private long sleep = 5;

	private static Logger LOGGER = Logger.getLogger(SocketChannelReader.class.getName());

	private int counterConnections = 0;

	/**
	 * Constructor.
	 */
	public SocketChannelReader(ThreadGroup tg, TCPIPGatewayConfiguration config) {
		super(tg, "SocketChannelReader");
		this.newClients = new LinkedList<ClientConnection>();
		this.running = false;
		this.readBuffer = ByteBuffer.allocate(TCPIPGatewayConfiguration.BUFFER_SIZE);
		this.sleep = config.getLong(TCPIPGatewayConfiguration.CHANNEL_READER_SLEEP);
		this.setName(this.getClass().getSimpleName());
	}

	public void setConnectionListener(IConnectionListener listener) {
		this.connectionlistener = listener;
	}

	/**
	 * Adds a new connection to the list of pending clients to process them in a
	 * synchronized way.
	 */
	public void addNewClient(ClientConnection clientConnection) {
		synchronized (newClients) {
			newClients.addLast(clientConnection);
		}
		// Force selector to return so that the new connection can get in the loop right
		// away.
		selector.wakeup();
	}

	/**
	 * Loop forever, first doing our select() then check for new connections
	 */
	public void run() {

		running = true;
		try {
			System.out.println("SocketChannelReader started");
			selector = Selector.open();
			while (running) {
				try {

					// ARF, note that a SSAP message is split into several TCP segments
					// this means, when KP sends a message, select() is executed several times
					// and therefore, the rest of statements below
					select();
					if (!this.isInterrupted()) {
						boolean hasNewConnections = checkNewConnections();
						checkDisconnections(hasNewConnections);
						// patchARFSockets(); // disabled by now

						try {
							// System.out.println("[" + this.getDateTime() + "] waiting?...");

							// ARF:20-01-2021 (the last value I tested was 5ms and it was ok)do not use very
							// small values because a new incoming big message could be received in 2 o 3
							// tcp segments and this happens more when the kps and server are talking over
							// the Internet (the routers split to 4KB segments while pc card split at 12KB)
							// - if small sleep, this loop could be repeated on each segment->slow
							// - if big sleep, the buffer could be too small to handle several segments
							// - also, I do not known if real multiplexed channel and writing ops are not
							// affected by incoming segments
							Thread.sleep(30);

							// System.out.println("[" + this.getDateTime() + "] of course!");
						} catch (InterruptedException e) {
						}

					}
				} catch (Exception e) {
					System.out.println("Error while reading info");
				}
			}
		} catch (IOException e) {
			System.out.println("Exception while opening Selector");
		} finally {
			System.out.println("SocketChannelReader finished");
		}
	}

	private long last = System.currentTimeMillis();

	private void checkDisconnections(boolean hasNewConnections) {
		// ARF: this flag is needed because if start a socket conn,
		// select is invoked, but this method as well, and this method checks
		// that the socket is not still ready and closes it
		// you can test with >nc 192.168.1.xxx 2300 to see that
		// when connecting but not reading data, the inner read=-1 is raised
		if (hasNewConnections)
			return;

		// checkConn can be expensive, execute only every 10 seconds
		long now = System.currentTimeMillis();
		if (now - last < 10000)
			return;

		// check connections, remove obsolete ones and reset timestamp
		last = System.currentTimeMillis();
		connectionlistener.checkConnections();

	}

	/**
	 * Check for new connections and register them with the selector
	 */
	private boolean checkNewConnections() {
		boolean hasNewConnections = false;
		synchronized (newClients) {
			while (newClients.size() > 0) {
				try {
					ClientConnection client = (ClientConnection) newClients.removeFirst();
					connectionlistener.connectionCreated(client);
					client.registerAndConfigureChannel(selector);
					LOGGER.info("CLIENT CONNECTED:" + client.toString());
					hasNewConnections = true;
				} catch (ClosedChannelException cce) {
					System.out.println("Channel closed");
				} catch (IOException ioe) {
					System.out.println("IOException on clientChannel");
				}
			}
		}
		ARFPatchManyConnections(hasNewConnections);
		return hasNewConnections;
	}

	/**
	 * This is an all-weather method to detect if KPs are crashing, so they should
	 * be OK but not the server. In this case, close the server so when restarting
	 * the KPs will connect to a fresh SIB
	 */
	private void ARFPatchManyConnections(boolean hasNewConnections) {
		if (hasNewConnections)
			counterConnections++;
		if (counterConnections > 200) {
			String message = "ARF CRITICAL: THE SIB HAS BEEN ACCEPTING TOO MANY CONNECTIONS FROM KPS. Total of conn requests = "
					+ counterConnections;
			System.out.println(message); // To prevent logging invalid configuration
			LOGGER.severe(message);
			System.exit(10);
		}
	}

	/**
	 * Do Select, and read from the channels. This method also checks if a
	 * connection has closed.
	 */
	private void select() {
		try {
			// This is a blocking select that will return when new data arrive.
			// selector.select(5000);
			selector.select();
			Set<SelectionKey> readyKeys = selector.selectedKeys();
			Iterator<SelectionKey> i = readyKeys.iterator();
			while (i.hasNext()) {
				SelectionKey key = i.next();
				i.remove();
				SocketChannel channel = (SocketChannel) key.channel();
				ClientConnection client = (ClientConnection) key.attachment();
				// ByteBuffer readBuffer =
				// ByteBuffer.allocate(TCPIPGatewayConfiguration.BUFFER_SIZE);
				readBuffer.clear(); // reuse the buffer to improve performance
				try {

					// Read from the channel.

					long readBytes = channel.read(readBuffer);
					if (readBytes == 0) {
						// ARF 09-05-2020: from ReadableByteChannel docs, read() can return 0 if buffer
						// has no remaining bytes for filling
						System.out.println(
								"ARF: readBytes = 0, this should not be a problem but it indicates buffer has no remaining space. Check this trace if the socket is always disconnected (and then change the code to disconnect the client).  IsSocketOpen=="
										+ key.channel().isOpen());
					} else if (readBytes == -1) {
						// read=-1 means that the channel was closed/exited from the KP
						// most of the disconnections will be detected here (but not all)
						System.out.println("[" + client.toString() + "] connection closed");
						client.fireConnectionClosed();
						connectionlistener.connectionDestroyed(client);
						System.out.println("ARF: readbytes=-1 -> CLIENT DISCONNECTED:" + client.toString());
					} else {
						// Add read bytes to the attachment.
						// System.out.println("ARF: readBytes = " + readBytes);
						client.add(readBuffer);
					}
				} catch (MessageParseException mpe) { // ARF: MessageParseException | SSAPMessageParseException, but
														// they are in different bundles
					// ARF 26-04-2020 treat messageparseexception as critical and close socket if
					// socket is not a KP (ie: socket attack, http attack, etc)
					// (example. a telnet connection sending arbitrary data)
					String errMessage = mpe.getMessage() != null ? mpe.getMessage()
							: "Generic MessageParseException error";

//					if (errMessage.contains("SSAP_message")) {
//						KP_MessageException(client, errMessage);
//					} else {
//						NOT_a_KP_MessageException(client);
//					}

					// ARF 14-05-2020: some SSAP messages are incomplete but not containing "
					// SSAP_message" literal,
					// and since every reconnection, the information concepts are duplicated (this
					// is a bug in the SemanticModel class)
					// the less reconnections, the better. Otherwise new problems appears.
					KP_MessageException(client, errMessage);

					// trace the Error message
					byte[] b = new byte[(readBuffer.limit() / 8 + 1) * 8]; // in chunks of 8 bytes, otherwise it throws
																			// Buffer underflow later
					for (int r = 0; r < readBuffer.limit(); r++)
						b[r] = readBuffer.get(r);
					String s = new String(b, StandardCharsets.UTF_8);
					String humanMessage = "ATTACK: the message sent by client: " + client.toString()
							+ " is not a valid SSAP message. Message was: \n" + s;
					System.err.println(humanMessage); // ARF: Disable once invalid messages are analysed in production
				} catch (IOException ioe) {
					// The client has closed abruptly or the message was not a valid SSAP.
					System.out.println("Connection [" + client.toString() + "] closed");
					client.fireConnectionClosed();
					connectionlistener.connectionDestroyed(client);
					LOGGER.severe("CLIENT DISCONNECTED:" + client.toString());
				}
			}
		} catch (Exception e) {
			System.out.println("Exception during select(): " + e.toString());
			e.printStackTrace();
		}
	}

	/**
	 * A real KP socket connection `> ss host port` because it is sending SSAP, but
	 * the SSAP can be malformed This method will be debug later to see if problem
	 * is when parsing bytes here in the SIB. Note: check if ByteBuffer class is the
	 * problem.
	 */
	public void KP_MessageException(ClientConnection client, String message) throws Exception {
		// client.fireMessageReceived(("<SSAP_message>"+message+"</SSAP_message>").getBytes("UTF-8"));
		// //disabled because cliet object cannot send no-semantic data
		// connectionlistener.connectionDestroyed(client); do not stop by now but do not
		// close (the client is OK but the messege is not valid, unfortunately the smool
		// guys dind not implement error in the KP libs to resend the message so there
		// is no way by now to request resend ing the message again)
		LOGGER.warning("Invalid SSAP message: " + message + ". " + client.toString());
	}

	/**
	 * A normal socket connection `> ss host port` is accepted by the SIB, but when
	 * sending the first line, if the message is not SSAP, it is because that socket
	 * is a malicious one, so close the socket inmediately
	 */
	public void NOT_a_KP_MessageException(ClientConnection client) throws Exception {
		String msg = "{\"alert\":\"ATTACK: the message sent by client: " + client.toString()
				+ " is not a valid SSAP message" + "  \",\"id\":\"SMOOL_SERVER\",\"timestamp\":"
				+ Long.toString(System.currentTimeMillis()) + "}";
		// System.out.println(msg);
		LOGGER.severe(msg);
		LOGGER.severe("Disconnecting client to prevent further damage:" + client.toString());

		client.fireConnectionClosed();
		connectionlistener.connectionDestroyed(client);
	}

	/**
	 * This method solves the leakage in socket connections in the SIB Sessions are
	 * properly removed but not sockets.
	 * 
	 * When a KP is trying to reconnect, a new socket is acquired, but if the socket
	 * is closed or same kp with more sockets open or external rogue socket
	 * connections, then the selector.selectedKeys() shows an increasing amount of
	 * wasted channels
	 */
	private void patchARFSockets() {
		boolean mustUpdate = true;
		Map<String, List<SelectionKey>> map = new HashMap<>();

		// get the number of sockets with the same ip
		for (SelectionKey key : selector.keys()) {
			try {
				ClientConnection client = (ClientConnection) key.attachment();
				String IP;
				try {
					IP = ((InetSocketAddress) client.getChannel().getRemoteAddress()).getAddress().toString();
				} catch (ClosedChannelException e) {
					IP = "unknown";
				}
				if (!map.containsKey(IP))
					map.put(IP, new ArrayList<>());
				map.get(IP).add(key);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		// close socket connections if same IP has more than 100 conns
		// the closing socket will force the KP to crash (and reboot)
		for (String IP : map.keySet()) {
			List<SelectionKey> keys = map.get(IP);
			if (keys.size() > 20) {
				// mustUpdate = false;
				for (SelectionKey key : keys) {
					ClientConnection client = (ClientConnection) key.attachment();
					client.fireConnectionClosed();
					connectionlistener.connectionDestroyed(client);
					LOGGER.severe("This IP has too many connections and this is probably a socket leak. "
							+ client.toString());
				}
			}
		}

		// close also ALL the null or not connected channels
		// note that if a client exit abruptly, there is no FIN tcp segment sent to
		// server
		// So this method only removes clients when OS closes inactive sockets
		// In order to detect really closesd sockets use a PING (like in the
		// checkConnections() method)
		for (SelectionKey key : selector.keys()) {
			ClientConnection client = (ClientConnection) key.attachment();
			SocketChannel channel = client.getChannel();
			if (channel == null || channel.isConnected() == false) {
				client.fireConnectionClosed();
				connectionlistener.connectionDestroyed(client);
				LOGGER.severe("This channel is null or not connected. " + client.toString());
			}

		}
		// if (mustUpdate)
		// HEALTH.update(this.getClass().getName());
	}

	public void shutdown() {
		try {
			running = false;
			this.interrupt();
			selector.close();
		} catch (Exception e) {
		}
	}

}