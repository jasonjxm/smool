/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/

package org.smool.sdk.gateway.tcpip.core;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.LinkedList;

import org.smool.sdk.gateway.tcpip.parser.SSAPMessageParser;

/**
 * ClientConnectionAttachment.java
 *
 * This class is used as the attachment for each channel registered with the
 * Selector.
 * <p>
 * It holds the temporary incoming data and checks the completeness of each
 * message.
 *
 * @author Raul Otaolea, Raul.Otaolea@tecnalia.com, Tecnalia-Tecnalia
 */
public abstract class ClientConnectionAttachment {

	/** Temporary buffer before it is converted into a message. */
	private byte[] buffer;

	/** List of ClientMessages already decoded. */
	private LinkedList<ClientMessage> decodedMessages;

	private int alloc;
	private int used;

	public ClientConnectionAttachment() {
		// [ROM] TODO: Modify hard coded values with values read from
		// TCPIPServer.properties.
		buffer = new byte[TCPIPGatewayConfiguration.BUFFER_SIZE];
		alloc = TCPIPGatewayConfiguration.BUFFER_SIZE;
		used = 0;

		decodedMessages = new LinkedList<ClientMessage>();
	}

	/** Check if there are enough bytes to build message(s). */
	public boolean checkForMessages() throws MessageParseException {
		int[] indexes = parseMessages(buffer);
		if (indexes == null) {
			// ARF: indexes=null in 2 cases:
			// 1- message is big, so only a partial message is retrieved,
			// we should wait for more IP packets to finish the message.
			// This is OK so continue.
			// 2- rogue tcp connection sending arbitrary data.
			// We cannot detect this case safely because sometimes
			// the ssap sent by client is not ok and we patch it.
			// We can treat them as OK because the max damage is
			// a socket open and a 32K buffer filled.
			return false;
		} else {
			if (indexes.length % 2 != 0) {
				throw new MessageParseException("The length of the indexes is no even.");
			}
			try {
				// Create client messages.
				for (int i = 0; i < indexes.length; i += 2) {
					// int messageSize = indexes[i+1] - indexes[i] + 1;
					String s = new String(buffer, Charset.forName("UTF-8"));
					s = s.substring(indexes[i], indexes[i + 1] + 1);

					// ARF: PATCH when SSAP message is missing the first char '<'
					if (SSAPMessageParser.mustAddPatch(s)) {
						s = '<' + s;
					}

					byte[] payload = s.getBytes(Charset.forName("UTF-8"));
					ClientMessage cm = new ClientMessage(payload);
					decodedMessages.add(cm);
				}
				// Compact the buffer.
				int remaining = used - indexes[indexes.length - 1] - 1;
				int newLength = remaining;
				if (newLength < TCPIPGatewayConfiguration.BUFFER_SIZE) {
					newLength = TCPIPGatewayConfiguration.BUFFER_SIZE;
				}

				byte[] newBuffer = new byte[newLength];
				int pos = indexes.length > 0 ? indexes.length - 1 : 0;
				try {
					System.arraycopy(buffer, indexes[pos] + 1, newBuffer, 0, remaining);
				} catch (Exception e) {
					throw e;
				}

				buffer = newBuffer;
				alloc = newLength;
				used = remaining;
				return true;
			} catch (Exception e) {
				// e.printStackTrace();
				throw new MessageParseException(e.toString());
			}
		}
	}

	/**
	 * 
	 * @return int[] Returns pairs of indexes indicating the start index (included)
	 *         and the last index (included) of the detected messages.
	 */
	public abstract int[] parseMessages(byte[] buffer);

	/**
	 * Indicates if there are pending messages.
	 * 
	 * @return
	 */
	public boolean hasMessages() {
		return decodedMessages.size() > 0;
	}

	/**
	 * This method removes the messages returned each time is called.
	 * 
	 * @return Iterator with the parsed messages.
	 */
	public Iterator<ClientMessage> getMessages() {
		if (hasMessages()) {
			LinkedList<ClientMessage> messages = new LinkedList<ClientMessage>();
			Iterator<ClientMessage> i = decodedMessages.iterator();
			while (i.hasNext()) {
				ClientMessage cm = i.next();
				i.remove();
				messages.add(cm);
			}
			return messages.iterator();
		} else {
			return null;
		}
	}

	/**
	 * Clear this array
	 */
	public void reset() {
		used = 0;
	}

	/**
	 * Returns the lenght of this array
	 */
	public int length() {
		return used;
	}

	/**
	 * Add a ByteArray to this array
	 * 
	 * @param b ByteArray to add
	 */

	public void add(ByteBuffer bb) {
		bb.flip();
		if (used + bb.limit() >= alloc) {
			grow(bb.limit() + TCPIPGatewayConfiguration.BUFFER_GROW);
		}

		bb.get(buffer, used, bb.limit());
		used += bb.limit();
		// System.out.println("ARF: used = " + used);
	}

	private void grow(int newGap) {
		System.out.println(
				"ARF: one of the SSAPMessages seems to be too BIG, or at fast pace. Check for potential performance issues.");
		alloc += newGap;
		byte[] n = new byte[alloc];
		System.arraycopy(buffer, 0, n, 0, used);
		buffer = n;
	}
}
