/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/

package org.smool.sdk.gateway.tcpip.core;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;
import java.util.Collection;
import java.util.HashSet;
import java.util.Properties;

import org.smool.sdk.gateway.AbstractGateway;
import org.smool.sdk.gateway.exception.GatewayException;
import org.smool.sdk.gateway.session.impl.Session;
import org.smool.sdk.sib.service.ISIB;
import org.smool.sdk.sib.ssap.SIB;

/**
 * TCPIPGateway.java
 * 
 * Handles reading from clients and hands off events to the consumer.
 * 
 * @author Raul Otaolea, Raul.Otaolea@tecnalia.com, Tecnalia-Tecnalia
 */
public class TCPIPGateway extends AbstractGateway implements IConnectionListener {

	/**
	 * This class is responsible for accepting incoming client connections.
	 */
	private SocketChannelAcceptor channelAcceptor;
	/**
	 * This class is responsible for reading the information from the client
	 * channels.
	 */
	private SocketChannelReader channelReader;

	/**
	 * This class is responsible for writing the information to client channels.
	 */
	private SocketChannelWriter channelWriter;

	/**
	 * To read the messages that KP sends in order to discover SIBs.
	 */
	private MulticastReader multicastReader;
	/**
	 * The ThreadGroup where all threads related with TCP/IP gateway will run.
	 */
	private ThreadGroup tg;

	/**
	 * The configuration of the tcp/ip gateway.
	 */
	private TCPIPGatewayConfiguration config;

	/**
	 * The logger
	 */

	private volatile boolean running;

	/**
	 * Constructor
	 */
	public TCPIPGateway(String name, String type, ISIB sib, Properties properties) {
		super(name, type, sib, properties);
		this.config = new TCPIPGatewayConfiguration();
		config.setProperties(properties);

		// Create the ThreadGroup
		tg = new ThreadGroup("TCP/IP Gateway");
	}

	/**
	 * This method starts the gateway.
	 */
	public void startGateway() throws GatewayException {
		try {
			System.out.println("Starting the TCP/IP gateway...");
			channelWriter = new SocketChannelWriter(tg, config);
			channelWriter.setConnectionListener(this);
			channelWriter.start();

			channelReader = new SocketChannelReader(tg, config);
			channelReader.setConnectionListener(this);
			channelReader.start();

			channelAcceptor = new SocketChannelAcceptor(tg, channelReader, channelWriter, config);
			channelAcceptor.start();

			multicastReader = new MulticastReader(sib.getName(), config.getProperties());
			multicastReader.start();

			running = true;
		} catch (Exception e) {
			System.out.println("TCP/IP gateway failed");
			e.printStackTrace();

			try {
				stop();
			} catch (Exception e2) {
			}
			running = false;
			throw new GatewayException(e.getMessage(), e);
		}
	}

	/**
	 * Shutdown the gateway.
	 */
	public void stopGateway() throws GatewayException {
		if (!running) {
			return;
		}

		System.out.println("Stopping TCP/IP gateway...");
		try {
			if (channelAcceptor != null) {
				channelAcceptor.shutdown();
				channelAcceptor = null;
			}
			if (channelReader != null) {
				channelReader.shutdown();
				channelReader = null;
			}
			if (channelWriter != null) {
				channelWriter.shutdown();
				channelWriter = null;
			}
			if (multicastReader != null) {
				multicastReader.shutdown();
				multicastReader = null;
			}

			System.out.println("TCP/IP gateway stopped");
		} catch (Exception e) {
			System.out.println("TCP/IP gateway failed while stopping");
			throw new GatewayException(e.getMessage(), e);
		}
	}

	/**
	 * @return The configuration of the TCP/IP server
	 */
	public TCPIPGatewayConfiguration getConfig() {
		return this.config;
	}

	@Override
	public void connectionCreated(ClientConnection connection) {
		Session session = new Session();
		session.setSIB(this.sib);
		session.setConnector(connection);
		this.addSession(session);
		System.out.println("ARF: connection was created and added to new session:  " + session.getKpName() + " "
				+ connection.toString());
	}

	@Override
	public void connectionDestroyed(ClientConnection connection) {
		Session sessionToRemove = null;
		for (Session session : this.sessions.values()) {
			ClientConnection cc = (ClientConnection) session.getConnector();
			if (cc == connection) {
				sessionToRemove = session;
				System.out.println("ARF: connection was destroyed on KP:  " + sessionToRemove.getKpName()
						+ ".  Connection: " + connection.toString());
				break;
			}
		}
		if (sessionToRemove != null) {
			this.removeSession(sessionToRemove);
			if (sib instanceof org.smool.sdk.sib.ssap.SIB) {
				SIB currentSIB = (org.smool.sdk.sib.ssap.SIB) sib;
				org.smool.sdk.sib.data.Session session = new org.smool.sdk.sib.data.Session(
						sessionToRemove.getKpName());
				currentSIB.getViewerListenerManager().removeSession(session);
				// ARF 07-05-2020: (Note: see git changes in this file before any modification)
				// remove obsolete subscriptions
				Collection<Long> oldSubscriptions = currentSIB.getSubscriptionManager()
						.removeSubscriptions(sessionToRemove.getKpName());
				if (oldSubscriptions.size() > 0)
					currentSIB.getViewerListenerManager().removeSubscription(oldSubscriptions, null);
			}

		}
	}

	@Override
	public void checkConnections() {
		HashSet<ClientConnection> toRemove = new HashSet<>();
		synchronized (sessions) {
			if (sessions.size() > 50)
				System.err.println(
						"****WARNING: Total of sessions is too high (session leak?), sessions = " + sessions.size());
			for (Session s : sessions.values()) {
				ClientConnection client = (ClientConnection) s.getConnector();
				SocketChannel channel = client.getChannel();

				// a- channels that sent a disconnection TCP message (the KP sent TCP finish)
				boolean isDisconnected = channel.isConnected() ? false : true;

				// b- channels with duplicated KPNames same IP (Reconnection in the same KP)
				boolean isDuplicated = isDuplicatedKPName(sessions.values(), s);

				// c- channels that are detected as disconnected when writing them (KP killed or
				// client network failed)

				// d- maliciuos socket connections, non kp compliant
				boolean isNotKP = isNotKP(s);

				boolean isBrokenFromWrite = SocketChannelReader.OBSOLETE_CHANNELS
						.containsKey(System.identityHashCode(channel));

				if (isDisconnected || isDuplicated || isBrokenFromWrite || isNotKP) {
					toRemove.add((ClientConnection) s.getConnector());
					System.out.println("ARF: Removing obsolete connection id: " + s.getId() + ", KP: " + s.getKpName());
				}
			}
		}

		for (ClientConnection cc : toRemove) {
			SocketChannelReader.OBSOLETE_CHANNELS.remove(System.identityHashCode(cc.getChannel()));
			cc.fireConnectionClosed(); // ARF double-check to close the channel (because read==-1 can be raised when
										// socket is still connecting
			connectionDestroyed(cc);
		}
	}

	/**
	 * Arbitrary socket connections, i.e ` nc 192.168.1.10 23000` cannot closed
	 * promptly so we detect them here. Any KP can have a short time living with
	 * kpname=null when connection is ok but join message has not arrived yet. To
	 * detect invalid KPs, they always have null kpName so after 2-3 seconds, if
	 * still null, that is an invalid or malicious socket and should be discarded
	 * 
	 * @param session
	 * @return
	 */
	private boolean isNotKP(Session session) {
		long alive = System.currentTimeMillis() - session.creation;
		return session.getKpName() == null && alive > 5000;
	}

	/**
	 * Check if several KPs with same name and same IP. Only the one with the newest
	 * timestamp is valid.
	 * 
	 * Returns true only if two or more sessions with same kp-ip AND if the session
	 * is not the youngest of that group.
	 * 
	 * @param sessions
	 * @param session
	 * @param channel
	 * @return
	 */
	private boolean isDuplicatedKPName(Collection<Session> sessions, Session session) {
		// relevant parameters to match
		String name = session.getKpName();
		String ip = getIP(session);
		long creation = session.creation;

		// search to test if old kp
		for (Session s : sessions) {
			if (s.getKpName() == null)
				continue;// either valid kp but join not arrived yet or invalid KP
			if (s.getKpName().equals(name) && getIP(s).equals(ip) && s.creation > creation) {
				System.out.println("Detected several KPs with same name and same source IP (" + name
						+ "). Removing all but the youngest.");
				return true; // There is a newer KP, so mark obsolete
			}

		}
		return false;
	}

	private String getIP(Session s) {
		SocketChannel c = ((ClientConnection) s.getConnector()).getChannel();
		String ip = "";
		try {
			ip = ((InetSocketAddress) c.getRemoteAddress()).getAddress().toString();
		} catch (IOException ioe) {
			// add a nonce to prevent duplicated IPs when error
			ip = "IO Error! (nonce" + Long.toString(System.nanoTime()) + ")";
		}
		return ip;
	}

}
