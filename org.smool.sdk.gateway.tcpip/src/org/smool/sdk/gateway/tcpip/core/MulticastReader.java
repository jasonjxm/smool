/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.gateway.tcpip.core;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.ArrayList;
import java.util.Properties;
import java.util.StringTokenizer;

/**
 * MulticastReader.java
 *
 * @author Raul Otaolea, Raul.Otaolea@tecnalia.com, Tecnalia-Tecnalia
 */

public class MulticastReader extends Thread {

	private static final int PORT = 5555;
	private static final String MULTICAST_GROUP = "235.0.0.1";
	private static final String KP_REQUEST = "<KP>ping</KP>";
	
	/** Logger */

	private volatile boolean running;
	private InetAddress group;
	private MulticastSocket ms;
	private byte[] buffer;
	private String sibName;
	private Properties properties;
	
	public MulticastReader(String sibName, Properties properties) {
		this.sibName = sibName;
		this.properties = properties; 
	}
	
	private boolean init() {
		try {
			buffer = new byte[128];
			group = InetAddress.getByName(MulticastReader.MULTICAST_GROUP);
			ms = new MulticastSocket(MulticastReader.PORT);
			ms.joinGroup(group);
			return true;
		} catch(Exception e) {
			return false;
		}
	}
	public void run() {
		System.out.println("MulticastReader started");
		running = init();
		while (running) {
			try {
				DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
				ms.receive(packet);
				
				byte[] tmp = new byte[packet.getLength()];
				System.arraycopy(packet.getData(), 0, tmp, 0, tmp.length);
				String received = new String(tmp);
				System.out.println(received);
				if(received.equals(KP_REQUEST)) {
					tmp = getSIBPropertiesXML(packet.getAddress());
				} else {
					tmp = "Sorry, but you are not probably allowed in this multicast port...".getBytes();
				}
	
				// send the response to the client at "address" and "port"
				InetAddress address = packet.getAddress();
				int clientPort = packet.getPort();
				packet = new DatagramPacket(tmp, tmp.length, address, clientPort);
				ms.send(packet);
			} catch(Exception e) {
				
			}
		}
		
		try {
			ms.leaveGroup(group);
			ms.close();
		} catch(Exception e) {
		}
		System.out.println("MulticastReader finished");
	}
	
	public void shutdown() {
		running = false;
		ms.close();
	}
	
	/**
	 * When several nics in the SIB, send the best default_ip (same network) to the
	 * client
	 */
	private String guessPreferredSIBAddress(String rawDefaultIP, ArrayList<String> addresses, String clientAddress) {
		String defaultIP = rawDefaultIP != null ? rawDefaultIP : addresses.get(0);
		String clientNetwork = clientAddress.split("\\.")[0];
		if (addresses.size() == 1)
			return defaultIP;
		else {
			if (addresses.contains(clientAddress))
				return clientAddress;
			else return addresses.stream().filter(ip -> ip.startsWith(clientNetwork)).findFirst().orElse(defaultIP);
		}
	}

	/**
	 * Gets the properties of the SIB discovered, most important one should be the
	 * endpoints associated with this SIB (IP:PORT). If there are properties name
	 * ENDPOINT or ENDPOINTS, it will return those, otherwise, proceeds the all way,
	 * getting the default properties
	 * 
	 * @return The properties describing the SIB, as a byte array (better for
	 *         sending)
	 */
	@SuppressWarnings("unchecked")
	private byte[] getSIBPropertiesXML(InetAddress clientAddr) {
		StringBuilder sb = new StringBuilder();
		sb.append("<sib>");
		sb.append("<name>");
		sb.append(sibName);
		sb.append("</name>");
		
		//Check if we have some ENDPOINT or ENDPOINTS properties, if so, use only those
		if (this.properties.get(TCPIPGatewayConfiguration.ENDPOINT) != null){
			String endpoint = (String) this.properties.get(TCPIPGatewayConfiguration.ENDPOINT);
			if (!endpoint.isEmpty()){
				//Get the IP and PORT of this endpoint and use it to create the params DEFAULT_IPADDRESS
				//This is done to keep backwards compatibility with the TCPIPSIBDiscoveres in the 
				StringTokenizer tokenizer = new StringTokenizer(endpoint, ":");
				String ip = tokenizer.nextToken();
				String port = tokenizer.nextToken();
				sb.append("<parameter name=\""+ TCPIPGatewayConfiguration.DEFAULT_IPADDRESS +"\">");
				sb.append(ip);
				sb.append("</parameter>");
				sb.append("<parameter name=\""+ TCPIPGatewayConfiguration.PORT +"\">");
				sb.append(port);
				sb.append("</parameter>");
			}
		}
		else{
			if (this.properties.get(TCPIPGatewayConfiguration.ENDPOINTS) != null) {
				String endpoints = (String) this.properties.get(TCPIPGatewayConfiguration.ENDPOINTS);
				if (!endpoints.isEmpty()){
					
					//remove the brackets, if any
					if (endpoints.startsWith("{"))
						endpoints = endpoints.substring(1);
					if (endpoints.endsWith("}"))
						endpoints = endpoints.substring(0, endpoints.length() - 1);
					
					StringTokenizer tokenizer = new StringTokenizer(endpoints, ",");
					if (tokenizer.countTokens() > 0){
						//Get the IP and PORT of this endpoint and use it to create the params DEFAULT_IPADDRESS
						//This is done to keep backwards compatibility with the TCPIPSIBDiscoveres in the, in the case of
						//a list of ENDPOINTS, we use only the first one for DEFAULT_IPADDRESS and PORT 
						String endpoint = tokenizer.nextToken();
						StringTokenizer splitter = new StringTokenizer(endpoint, ":");
						String ip = splitter.nextToken();
						String port = splitter.nextToken();
						sb.append("<parameter name=\""+ TCPIPGatewayConfiguration.DEFAULT_IPADDRESS +"\">");
						sb.append(ip);
						sb.append("</parameter>");
						sb.append("<parameter name=\""+ TCPIPGatewayConfiguration.PORT +"\">");
						sb.append(port);
						sb.append("</parameter>");
					}
					
				}
			}
			//Proceed as the old way (without ENDPOINTS
			// Add the available IPs.
			else{
				ArrayList<String> ipAddresses = (ArrayList<String>)this.properties.get(TCPIPGatewayConfiguration.ADDRESSES);
				if(ipAddresses != null) {
					sb.append("<parameter name=\"" + TCPIPGatewayConfiguration.ADDRESSES + "\">");
					StringBuilder sb2 = new StringBuilder();
					for(String ip : ipAddresses) {
						sb2.append(ip);
						sb2.append(",");
					}
					sb.append(sb2.substring(0, sb2.length()-1));
					sb.append("</parameter>");
				}
				
				// Add the default IP.
				String rawDefaultIP = this.properties.getProperty(TCPIPGatewayConfiguration.DEFAULT_IPADDRESS);
				String defaultIP = guessPreferredSIBAddress(rawDefaultIP, ipAddresses, clientAddr.getHostAddress());
				if(defaultIP != null) {
					sb.append("<parameter name=\""+ TCPIPGatewayConfiguration.DEFAULT_IPADDRESS +"\">");
					sb.append(defaultIP);
					sb.append("</parameter>");
				}
				// Add the port.
				try {
					int port = Integer.parseInt(this.properties.getProperty(TCPIPGatewayConfiguration.PORT));
					if(port != 0) {
						sb.append("<parameter name=\""+ TCPIPGatewayConfiguration.PORT +"\">");
						sb.append(port);
						sb.append("</parameter>");
					}
				} 
				catch(NumberFormatException nfe) {
					System.out.println("ERROR: Could not parse the provided default port with value " + this.properties.getProperty(TCPIPGatewayConfiguration.PORT) + " . Reason is " + nfe.getMessage());
				} 
				catch(Exception ex) {
					System.out.println("ERROR: Could not parse the provided default port with value " + this.properties.getProperty(TCPIPGatewayConfiguration.PORT) + " . Reason is " + ex.getMessage());
				}
			}
			
		}
		sb.append("</sib>");
		return sb.toString().getBytes();
	}
}
