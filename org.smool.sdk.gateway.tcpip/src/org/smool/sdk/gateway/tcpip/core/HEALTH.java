package org.smool.sdk.gateway.tcpip.core;

import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

/**
 * IMPORTANT:
 * 
 * This is a status checker of several **HEALTH** parameters in the SIB If one
 * of the parameters is not valid, the system will **SUTHDOWN** to allow
 * RESTARTING in a clean way.
 * 
 * The class works as a forever secondary thread looking for problems.
 * 
 * DESCRIPTION:
 * 
 * The class is needed because there are several bugs in the socket thread
 * implamentation in the SIB, and they cannot be reproduced. So instead of
 * detecting and patching, it is better to centralize problems here.
 * 
 * The smart solution would be rewire the sockets in a non-thread, non blocking
 * code, but there is no time to put full effort on this.
 * 
 * For pilots, the socket problems are not critical, but when deploying SIB in
 * production, there are lots of unexpected connections, intermediate routers
 * firewalls, tcp attacks, and so on, and the list of KPs can be much higher.
 * 
 * @author ARF
 *
 */
public class HEALTH extends Thread {

	private static long LIMIT_MILLIS = 1000 * 60; // in 1 minute, at least one message should arrive
	private static Map<String, Long> params = new ConcurrentHashMap<>();


	public static void update(String key) {
		params.put(key, System.currentTimeMillis());
	}

	/**
	 * Check health parameters
	 */
	public void run() {
		try {
			while (true) {
				sleep2(1000 * 60);
				long now = System.currentTimeMillis();
				for (Entry<String, Long> entry : params.entrySet()) {
					if (entry.getValue() < now - LIMIT_MILLIS)
						System.err.println("[HEALTH] ******* " + entry.getKey() + " has reached timeout");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			sleep2(100);
			System.exit(1);
		}
	}


	private void sleep2(long millis) {
		try {
			Thread.sleep(millis);
		} catch (Exception e) {
			;
		}
	}
}
