<SSAP_message>
  <node_id>SMARTAPP1</node_id>
  <space_id>sib1</space_id>
  <transaction_id>3</transaction_id>
  <transaction_type>INSERT</transaction_type>
  <message_type>REQUEST</message_type>
  <parameter name='insert_graph' encoding='RDF-XML'>
    <?xml version='1.0'?>
    <rdf:RDF xmlns='http://www.sofia-project.eu/ontology/test/vCard#'
             xml:base='http://www.sofia-project.eu/ontology/test/vCard#'
             xmlns:rdfs='http://www.w3.org/2000/01/rdf-schema#'
             xmlns:owl='http://www.w3.org/2002/07/owl#'
             xmlns:xsd='http://www.w3.org/2001/XMLSchema#'
             xmlns:rdf='http://www.w3.org/1999/02/22-rdf-syntax-ns#'
             xmlns:vcard='http://www.w3.org/2001/vcard-rdf/3.0#'>
      <owl:Thing rdf:ID='FranRuiz'>
        <vcard:FN rdf:datatype='&xsd;string'>Fran Ruiz</vcard:FN>
        <vcard:N>
          <vcard:Given rdf:datatype='&xsd;string'>Fran</vcard:Given>
          <vcard:Family rdf:datatype='&xsd;string'>Ruiz</vcard:Family>
        </vcard:N>
      </owl:Thing>
    </rdf:RDF>
  </parameter>
  <parameter name='confirm'>TRUE</parameter>
</SSAP_message>