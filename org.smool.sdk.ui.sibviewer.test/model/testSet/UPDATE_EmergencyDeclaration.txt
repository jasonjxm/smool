<SSAP_message>
  <node_id>SMARTAPP1</node_id>
  <space_id>ESIB</space_id>
  <transaction_id>30</transaction_id>
  <transaction_type>UPDATE</transaction_type>
  <message_type>REQUEST</message_type>
  <parameter name='insert_graph' encoding='RDF-XML'>
    <?xml version='1.0'?>
    <!DOCTYPE rdf:RDF [
      <!ENTITY owl "http://www.w3.org/2002/07/owl#" >
      <!ENTITY xsd "http://www.w3.org/2001/XMLSchema#" >
      <!ENTITY rdfs "http://www.w3.org/2000/01/rdf-schema#" >
      <!ENTITY rdf "http://www.w3.org/1999/02/22-rdf-syntax-ns#" >
      <!ENTITY wmc "https://194.30.38.163/sofia/ontology/1.2/city/wmc.owl#" >
    ]>	
    <rdf:RDF xmlns='https://194.30.38.163/sofia/ontology/1.2/city/wmc.owl#'
             xml:base='https://194.30.38.163/sofia/ontology/1.2/city/wmc.owl#'
             xmlns:dc='http://purl.org/dc/elements/1.1/#'
             xmlns:rdfs='http://www.w3.org/2000/01/rdf-schema#'
             xmlns:owl='http://www.w3.org/2002/07/owl#'
             xmlns:xsd='http://www.w3.org/2001/XMLSchema#'
             xmlns:rdf='http://www.w3.org/1999/02/22-rdf-syntax-ns#'
             xmlns:wmc='https://194.30.38.163/sofia/ontology/1.2/city/wmc.owl#'
             xmlns:dcterms='http://purl.org/dc/terms/#'>
      <EmergencyDeclaration rdf:about='#EmergencyDeclared'>
		<expectedEnd rdf:datatype='&xsd;dateTime'>2010-05-17T14:00:00Z</expectedEnd>
      </EmergencyDeclaration>
    </rdf:RDF>
  </parameter>
  <parameter name='remove_graph' type='RDF-XML'>
    <?xml version='1.0'?>
    <!DOCTYPE rdf:RDF [
      <!ENTITY owl "http://www.w3.org/2002/07/owl#" >
      <!ENTITY xsd "http://www.w3.org/2001/XMLSchema#" >
      <!ENTITY rdfs "http://www.w3.org/2000/01/rdf-schema#" >
      <!ENTITY rdf "http://www.w3.org/1999/02/22-rdf-syntax-ns#" >
      <!ENTITY wmc "https://194.30.38.163/sofia/ontology/1.2/city/wmc.owl#" >
    ]>	
    <rdf:RDF xmlns='https://194.30.38.163/sofia/ontology/1.2/city/wmc.owl#'
             xml:base='https://194.30.38.163/sofia/ontology/1.2/city/wmc.owl#'
             xmlns:dc='http://purl.org/dc/elements/1.1/#'
             xmlns:rdfs='http://www.w3.org/2000/01/rdf-schema#'
             xmlns:owl='http://www.w3.org/2002/07/owl#'
             xmlns:xsd='http://www.w3.org/2001/XMLSchema#'
             xmlns:rdf='http://www.w3.org/1999/02/22-rdf-syntax-ns#'
             xmlns:wmc='https://194.30.38.163/sofia/ontology/1.2/city/wmc.owl#'
             xmlns:dcterms='http://purl.org/dc/terms/#'>
      <EmergencyDeclaration rdf:about='#EmergencyDeclared'>
		<expectedEnd rdf:datatype='&xsd;dateTime'>2010-05-17T13:30:00Z</expectedEnd>
      </EmergencyDeclaration>
    </rdf:RDF>
  </parameter>  
  <parameter name='CONFIRM'>TRUE</parameter>
</SSAP_message>