package org.smool.sdk.ui.sibviewer.test;

/*
 *    Copyright 2010 European Software Institute
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *    
 *      http://www.apache.org/licenses/LICENSE-2.0
 *    
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *    
 */

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;
import org.smool.sdk.sib.service.ISIB;
import org.smool.sdk.ui.sibviewer.test.sib.AsyncSSAPOSGiImpl;
import org.smool.sdk.ui.sibviewer.test.ui.MainWindow;


/**
 * The Activator class controls the plug-in life cycle.
 * This plug-in emulates the behavior of the Smart Application objects that are 
 * connected to a server. This emulation is performed by the creation of Smart Application
 * windows tests that can connect to a specific server and perform operations over it.
 * 
 * @author Cristina L�pez, cristina.lopez@tecnalia.com, ESI
 *
 */
public class Activator implements BundleActivator {

	/**
	 * Id of the plug-in
	 */
	public static final String PLUGIN_ID = "org.smool.m3.kp.osgi";

	/**
	 * Bundle Context
	 */
	private static BundleContext bc;
	
	/**
	 * Shared instance of the Activator
	 */
	private static Activator plugin;
	
	/**
	 * Service tracker customizer that detects the available SIB services
	 */
	private SIBServiceTrackerCustomizer sibstc;
	
	/**
	 * Hash map used internally to identify which SIB service corresponds to which server
	 * identifier.
	 */
	private HashMap<String,ISIB> serverMap= new HashMap<String, ISIB>();
	
	/**
	 * Hash map used internally to assign each SIB service with its corresponding
	 * Asynchronous connection.
	 */
	private HashMap<ISIB,AsyncSSAPOSGiImpl> servicesMap= new HashMap<ISIB,AsyncSSAPOSGiImpl>();
	
	/**
	 * Hash map used internally to identify how many test have been created over an
	 * specific server instance.
	 */
	private HashMap<String,List<MainWindow>> testUIMap= new HashMap<String,List<MainWindow>>();


	/**
	 * Default Constructor
	 */
	public Activator() {
	}
	
	/**
	 * Gets the internal server map
	 * @return HashMap<String, ISIB>
	 */
	public HashMap<String, ISIB> getServerMap() {
		return serverMap;
	}

	/**
	 * Gets the internal services map
	 * @return HashMap<ISIB, AsyncSSAPOSGiImpl>
	 */
	public HashMap<ISIB, AsyncSSAPOSGiImpl> getServicesMap() {
		return servicesMap;
	}

	/**
	 * Gets the internal Test user interface map
	 * @return HashMap<String, List<MainWindow>>
	 */
	public HashMap<String, List<MainWindow>> getTestUIMap() {
		return testUIMap;
	}
	
	/**
	 * Gets the shared instance of the Activator
	 *
	 * @return Activator
	 */
	public static Activator getDefault() {
		return plugin;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void start(BundleContext context) throws Exception {
		try {
			// Establish the context
			Activator.bc = context;
			plugin = this;

			// Service tracker customizer for the async SIB
			// in the current context
			sibstc = new SIBServiceTrackerCustomizer(bc);
			ServiceTracker tracker2 = new ServiceTracker(bc, ISIB.class.getName(), sibstc);
			tracker2.open();
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		Activator.bc = null;
		plugin=null;
	}

	/**
	 * This method creates a new Smart Application window test.
	 * The method is called when the Create Smart Application Test button/action has
	 * been pressed and after the server to which made the connection has been selected.
	 * 
	 * @param asynckpservice
	 * @param serverName
	 */
	public void registertSmartApp(AsyncSSAPOSGiImpl asynckpservice, String serverName){
		
		Dictionary<String,Object> dict = new Hashtable<String,Object>();			
		System.out.println("Registering Asynchronous KP Service...");
		bc.registerService(AsyncSSAPOSGiImpl.class.getName(), asynckpservice, dict);
		System.out.println("KP Service Asynchronous Registered.");
		String[] args= new String[1];
		args[0]=serverName;
		
		MainWindow ui= MainWindow.create(args, asynckpservice);
		List<MainWindow> uiList;
		if (testUIMap.get(serverName)!=null){
			uiList=testUIMap.get(serverName);
		}
		else{
			uiList= new ArrayList<MainWindow>();
		}
		uiList.add(ui);	
		testUIMap.put(serverName, uiList);	
	}

	/**
	 * This method updates the internal list of tests created when a window test has
	 * been closed manually by a user. 
	 * 
	 * @param server
	 * @param ui
	 */
	public void removeWindow (String server, MainWindow ui){
		List<MainWindow> uiList=this.testUIMap.get(server);
		uiList.remove(ui);
		this.testUIMap.put(server, uiList);
	}
}
