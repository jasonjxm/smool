package org.smool.sdk.ui.sibviewer.test.command.wizard;

/*
 *    Copyright 2010 European Software Institute
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *    
 *      http://www.apache.org/licenses/LICENSE-2.0
 *    
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *    
 */

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.smool.sdk.ui.sibviewer.test.Activator;


/**
 * Wizard page called by the New Smpart App Test Wizard which is used to display the
 * list of available servers
 * 
 * @author Cristina L�pez, cristina.lopez@tecnalia.com, ESI
 *
 */
public class ServerSelectionPage extends WizardPage{
	
	/**
	 * List of servers
	 */
	public Combo server;
	/**
	 * Boolean used by the wizard to check if page have been succesfully created
	 */
	boolean success=false;

	/**
	 * Default Constructor
	 * @param pageName
	 */
	protected ServerSelectionPage(String pageName) {
		super(pageName);
		this.setTitle("New Smart Application Test");
		this.setDescription("Please select the server to test with the Smart Application:");
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.wizard.WizardPage
	 */
	public void createControl(Composite parent) {
        Composite composite = new Composite(parent, SWT.NONE);
        GridLayout layout = new GridLayout();
         layout.numColumns = 2;
         composite.setLayout(layout);
         setControl(composite);
                
         Object[] objects=Activator.getDefault().getServerMap().keySet().toArray();  
         
         if (objects.length>0){
        	 new Label(composite,SWT.NONE).setText("Server: ");
	         String[] servers=new String[objects.length];
	         for(int i=0;i<objects.length;i++)
	        	 servers[i]=objects[i].toString();
	         
	         server= new Combo(composite,SWT.READ_ONLY);
	         server.setItems(servers);
	         server.select(0);
	         server.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
	         success=true;
         }
         else{
        	 this.setErrorMessage("Sorry, there are not active servers ");
        	 this.setPageComplete(true);
         }
        
	}


}
