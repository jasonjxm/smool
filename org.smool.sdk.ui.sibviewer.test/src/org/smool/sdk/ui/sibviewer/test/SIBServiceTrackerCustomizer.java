package org.smool.sdk.ui.sibviewer.test;

/*
 *    Copyright 2010 European Software Institute
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *    
 *      http://www.apache.org/licenses/LICENSE-2.0
 *    
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *    
 */

import java.util.List;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import org.smool.sdk.sib.service.ISIB;
import org.smool.sdk.ui.sibviewer.test.sib.AsyncSSAPOSGiImpl;
import org.smool.sdk.ui.sibviewer.test.ui.MainWindow;
import org.smool.sdk.ui.sibviewer.test.utils.Utils;


/**
 * This class is a service tracker customizer that detects the SIB servers that
 * have been registered in the OSGI Framework.
 * 
 * When a new server is encountered, an asynchronous connection to that server is created.
 * 
 * @author Cristina L�pez, cristina.lopez�si.es, ESI
 *
 */

@SuppressWarnings({ "rawtypes", "unchecked" })
public class SIBServiceTrackerCustomizer implements ServiceTrackerCustomizer {

	/**
	 * SIB service
	 */
	protected ISIB asib = null;
	/**
	 * Bundle Context
	 */
	private BundleContext bc;
		
	/**
	 * Default Constructor
	 * @param bc Bundle Context
	 */
	public SIBServiceTrackerCustomizer(BundleContext bc){
		this.bc = bc;
	}
	
	@Override
	/*
	 * (non-Javadoc)
	 * @see org.osgi.util.tracker.ServiceTrackerCustomizer
	 */
	public Object addingService(ServiceReference reference) {

		// Obtains the SIB
		Object obj = bc.getService(reference);

		if (obj instanceof ISIB) {
			asib = (ISIB) bc.getService(reference);
			// Creates a new object referring to the created service 
			String serverId= reference.getPropertyKeys()[0];
					
			AsyncSSAPOSGiImpl kpAsyncService = new AsyncSSAPOSGiImpl(asib);
			
			Activator.getDefault().getServerMap().put(serverId, asib);
			Activator.getDefault().getServicesMap().put(asib, kpAsyncService);
					
			return kpAsyncService;
		} else {
			return null;
		}

	}
	
	@Override
	/*
	 * (non-Javadoc)
	 * @see org.osgi.util.tracker.ServiceTrackerCustomizer
	 */
	public void modifiedService(ServiceReference reference, Object serviceObject) {
		// Obtains the SIB
		Object obj = bc.getService(reference);
		
		if (obj instanceof ISIB) {
			asib = (ISIB) bc.getService(reference);
			// Creates a new object referring to the created service 
			String serverId= reference.getPropertyKeys()[0];
			
			AsyncSSAPOSGiImpl kpAsyncService = new AsyncSSAPOSGiImpl(asib);
			
			Activator.getDefault().getServerMap().put(serverId, asib);
			Activator.getDefault().getServicesMap().put(asib, kpAsyncService);
		}

	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.osgi.util.tracker.ServiceTrackerCustomizer
	 */
	public void removedService(ServiceReference reference, Object service) {
		
		Object obj = bc.getService(reference);
		
		if (obj instanceof ISIB) {
			asib = (ISIB) bc.getService(reference);
			// Creates a new object referring to the created service 		
			String serverId= reference.getPropertyKeys()[0];
			
			Activator.getDefault().getServerMap().remove(serverId);
			Activator.getDefault().getServicesMap().remove(asib);
			
			//If a server is unregistered from the OSGI Framework, the Smart Application
			//windows tests that are connected to that server are closed in order to 
			//avoid errors.
			//A message advertising the error is shown before closing the windows tests.
			List<MainWindow> uiList=Activator.getDefault().getTestUIMap().get(serverId);
			if (uiList.size()>0){
				Utils.showErrorMessage("The connection with the server "+serverId+" has been interrupted. The related Smart Application tests will be closed after this message.");
				for(int i=0;i<uiList.size();i++){
					MainWindow current=uiList.get(i);
					if (current.isConnected()){
						current.leaveServer();
					}
					current.getFrame().dispose();
					Activator.getDefault().getTestUIMap().remove(serverId);
				}
			}
		}
	}
}
