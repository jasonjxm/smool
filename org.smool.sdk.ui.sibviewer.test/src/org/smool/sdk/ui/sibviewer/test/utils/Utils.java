package org.smool.sdk.ui.sibviewer.test.utils;

/*
 *    Copyright 2010 European Software Institute
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *    
 *      http://www.apache.org/licenses/LICENSE-2.0
 *    
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *    
 */

import java.util.Vector;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;
import org.smool.sdk.ssapmessage.parameter.SSAPMessageParameter;
import org.smool.sdk.ssapmessage.parameter.SSAPMessageRDFParameter;
import org.smool.sdk.ssapmessage.parameter.SSAPMessageParameter.NameAttribute;
import org.smool.sdk.ssapmessage.parameter.SSAPMessageRDFParameter.TypeAttribute;
import org.smool.sdk.ssapmessage.proxy.ClientProxy;
import org.smool.sdk.ui.sibviewer.test.sib.AsyncSSAPOSGiImpl;


/**
 * This is an utility auxiliary class
 * 
 * @author Cristina L�pez, cristina.lopez@tecnalia.com, ESI
 *
 */
public class Utils {
	
	
	/**
	 * This method converts an String parameters into a SSAPMessageParameter type
	 * @param operation String
	 * @param parameters String
	 * @return Vector<SSAPMessageParameter>
	 */
	public static Vector<SSAPMessageParameter> convertStringToSSAP2(String operation, String parameters){
		Vector<SSAPMessageParameter> _return= new Vector<SSAPMessageParameter>();
		Vector<String> paramList= separateParameters(parameters);
	
		for(int j=0; j<paramList.size();j++){	
			NameAttribute na = getNameAttribute(paramList.get(j));
			TypeAttribute ta;
			if (na != null && na == NameAttribute.QUERY || na == NameAttribute.INSERTGRAPH || 
					na == NameAttribute.REMOVEGRAPH) {
				ta = getTypeAttribute(paramList.get(j), true);
			} else {
				ta = getTypeAttribute(paramList.get(j), false);
			}
			SSAPMessageParameter parameter;
			if (na == NameAttribute.TYPE || na == NameAttribute.ENCODING || na == NameAttribute.FORMAT) {
				parameter= new SSAPMessageRDFParameter(getNameAttribute(paramList.get(j)),ta.getValue());
			} else {
				if (ta != null) {
					parameter= new SSAPMessageRDFParameter(getNameAttribute(paramList.get(j)),ta,null);
				} else {
					parameter= new SSAPMessageRDFParameter(getNameAttribute(paramList.get(j)),null);
				}
			}
			String[] result= (paramList.get(j)).split("\n");
			if (result.length > 0) {
				StringBuffer buffer=new StringBuffer();
				for(int i=1; i< result.length-1; i++) {				
					buffer.append(result[i]);				
				}
				
				if (buffer.length() > 0) {
					parameter.setContent(buffer.toString());
				}
			}
			_return.add(parameter);
		}
		return _return;
	}
	
	/**
	 * This method separates a string in lines
	 * @param parameters
	 * @return Vector<String>
	 */
	public static Vector<String> separateParameters (String parameters){
		Vector<String> _return= new Vector<String>();
		int tagIndex=0;
		String current= parameters.trim();

		while(current.length()>0) {			
			tagIndex=current.indexOf("</parameter>");
			_return.add(current.substring(current.indexOf("<parameter"), tagIndex+12).trim());
			current = current.substring(tagIndex+12, current.length());
			current.trim();
		}		
		return _return;
	}
	
	/**
	 * This method is called by the window parameters area to initialize its content
	 * with the default messages
	 * 
	 * @param text
	 * @return Vector<String>
	 */
	public static Vector<String> getDefaultText (String text){
		Vector<String> _return= new Vector<String>();
		
		if (text.equals("Insert")){
			_return= Messages.getDefaultInsertMessage();
		}
		else if (text.equals("Update")){
			_return= Messages.getDefaultUpdateMessage();
		}
		else if (text.equals("Query")){
			_return= Messages.getDefaultQueryMessage();
		}
		else if (text.equals("Subscribe")){
			_return= Messages.getDefaultSubscribeMessage();
		}
		else if (text.equals("Unsubscribe")){
			_return= Messages.getDefaultUnsubscribeMessage();
		}
		else if (text.equals("Remove")){
			_return= Messages.getDefaultRemoveMessage();
		}
		
		return _return;
	}
	
	/**
	 * Method that obtains the default model necessary to perform a join operation over
	 * the server
	 * 
	 * @param contents
	 * @return Vector<SSAPMessageParameter>
	 */
	public static Vector<SSAPMessageParameter> obtainDefaultModel(String contents) {
		Vector<SSAPMessageParameter> paramOperations= new Vector<SSAPMessageParameter>();
		SSAPMessageRDFParameter param = new SSAPMessageRDFParameter(NameAttribute.INSERTGRAPH, 
				SSAPMessageRDFParameter.TypeAttribute.RDFXML, null);
		StringBuffer sb = new StringBuffer();
		
		sb.append(contents);
		param.setContent(sb.toString());
		
		paramOperations.add(param);
		paramOperations.add(createSSAPMessageParameter(NameAttribute.CONFIRM, "TRUE"));
		
		return paramOperations;
	}
	
	/**
	 * This method converts a Vector<String> into a String
	 * @param vector Vector<String>
	 * @return String
	 */
	public static String convertVectorToString (Vector<String> vector){
		String _return="";
		
		for (int i=0;i<vector.size();i++){
			_return+=vector.get(i)+"\n";
		}
		
		return _return;
	}
	
	/**
	 * This method is called by the send button to perform an operation over the server
	 * @param operation
	 * @param parameters
	 * @param sibConnection
	 * @param nodeId
	 * @param spaceId
	 * @param transactionId
	 */
	public static void executeCommand (String operation, String parameters, AsyncSSAPOSGiImpl sibConnection, String nodeId, String spaceId, Long transactionId, ClientProxy proxy){
		Vector<SSAPMessageParameter> paramOperations= new Vector<SSAPMessageParameter>();
		
		if (!operation.equals("Unsubscribe")) {
			paramOperations= convertStringToSSAP2(operation,parameters);
		}
		
		if (operation.equals("Insert")){
			paramOperations.add(createSSAPMessageParameter(NameAttribute.CONFIRM, "TRUE"));
			sibConnection.insert(nodeId, spaceId, transactionId, paramOperations, proxy);
		} else if (operation.equals("Update")){
			paramOperations.add(createSSAPMessageParameter(NameAttribute.CONFIRM, "TRUE"));
			sibConnection.update(nodeId, spaceId, transactionId, paramOperations, proxy);
		} else if (operation.equals("Query")) {
			sibConnection.query(nodeId, spaceId, transactionId, paramOperations, proxy);
		} else if (operation.equals("Subscribe")){
			sibConnection.subscribe(nodeId, spaceId, transactionId, paramOperations, proxy);
		} else if (operation.equals("Unsubscribe")){
			String subscription= obtainUnsubscriptionId(parameters);			
			paramOperations.add(createSSAPMessageParameter(NameAttribute.SUBSCRIPTIONID, subscription));
			sibConnection.unsubscribe(nodeId, spaceId, transactionId, paramOperations, proxy);
		} else if (operation.equals("Remove")){
			paramOperations.add(createSSAPMessageParameter(NameAttribute.CONFIRM, "TRUE"));
			sibConnection.remove(nodeId, spaceId, transactionId, paramOperations, proxy);
		}
	}
	
	/**
	 * Method that obtains the name of am operation
	 * @param parameter
	 * @return
	 */
	public static NameAttribute getNameAttribute (String parameter){
		NameAttribute name;
			int index=parameter.indexOf("name");
			String text= parameter.substring(index+6, parameter.length());
			String type= text.substring(0, text.indexOf("'"));
			name= NameAttribute.findValue(type);			
		return name;
	}

	/**
	 * Method that obtains the name of am operation
	 * @param parameter
	 * @return
	 */
	public static TypeAttribute getTypeAttribute (String parameter, boolean additionalAttribute){
		TypeAttribute type;
		
		char searchChar;
		if (additionalAttribute) {
			searchChar = '\'';
		} else {
			searchChar = '<';
		}
		
		
		int index=parameter.indexOf("encoding");
		if (index != -1) {
			String text= parameter.substring(index+10, parameter.length());
			String otherType= text.substring(0, text.indexOf(searchChar));
			type= TypeAttribute.findValue(otherType);
		} else {
			index=parameter.indexOf("type");
			String text= parameter.substring(index+6, parameter.length());
			String otherType= text.substring(0, text.indexOf(searchChar));
			type= TypeAttribute.findValue(otherType);
		}
		return type;
	}	
	
	/**
	 * This method create a SSAPMessageParameter object
	 * @param name
	 * @param value
	 * @return SSAPMessageParameter
	 */
	public static SSAPMessageParameter createSSAPMessageParameter(NameAttribute name, String value) {
		
		SSAPMessageParameter parameter = 
			new SSAPMessageParameter(name, value);		
		return parameter;
	}
	
	/**
	 * This method parse a message to obtain the unsubscription id
	 * @param parameters
	 * @return
	 */
	public static String obtainUnsubscriptionId(String parameters)
	{
		int start=parameters.indexOf(">")+1;
		int end= parameters.indexOf("</parameter>");
		
		String _return=parameters.substring(start, end);
		
		return _return;
	}
	
	/**
	 * This method show an Error message dialog when required
	 * @param message
	 */
	public static void showErrorMessage(String message) {
		Shell shell= new Shell();
		MessageDialog.openError(
				shell,
			"Error",
			message);
	}
}
