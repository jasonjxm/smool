package org.smool.sdk.ui.sibviewer.test.ssap;

/*
 *    Copyright 2010 European Software Institute
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *    
 *      http://www.apache.org/licenses/LICENSE-2.0
 *    
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *    
 */

import java.util.Collection;

import org.smool.sdk.ssapmessage.parameter.SSAPMessageParameter;
import org.smool.sdk.ssapmessage.proxy.ClientProxy;


/**
 * Interface representing the operation that can be executed in a Sib server
 * 
 * @author Cristina L�pez, cristina.lopez@tecnalia.com, ESI
 *
 */
public interface AsyncSSAP {

	/**
	 * This method implements the SSAP JOIN REQUEST
	 * @param nodeId The node identifier
	 * @param spaceId Smart Space Id
	 * @param transactionId Node-generated transaction identifier
	 * @param parameters A vector of parameters containing the credentials
	 */
	public void join(String nodeId, String spaceId, long transactionId, Collection<SSAPMessageParameter> parameters, ClientProxy proxy);

	/**
	 * This method implements the SSAP LEAVE REQUEST
	 * @param nodeId The node identifier
	 * @param spaceId Smart Space Id
	 * @param transactionId Node-generated transaction identifier
	 */
	public void leave(String nodeId, String spaceId, long transactionId, ClientProxy proxy);

	/**
	 * This method implements the SSAP INSERT REQUEST
	 * @param nodeId The node identifier
	 * @param spaceId Smart Space Id
	 * @param transactionId Node-generated transaction identifier
	 * @param SSAPMessageParameters A vector of SSAPMessageParameters containing the insert triplets
	 */
	public void insert(String nodeId, String spaceId, long transactionId,  Collection<SSAPMessageParameter> parameters, ClientProxy proxy);
	
	/**
	 * This method implements the SSAP REMOVE REQUEST
	 * @param nodeId The node identifier
	 * @param spaceId Smart Space Id
	 * @param transactionId Node-generated transaction identifier
	 * @param SSAPMessageParameters A vector of SSAPMessageParameters containing the removed triplets
	 */
	public void remove(String nodeId, String spaceId, long transactionId, Collection<SSAPMessageParameter> parameters, ClientProxy proxy);
	
	/**
	 * This method implements the SSAP UPDATE REQUEST
	 * @param nodeId The node identifier
	 * @param spaceId Smart Space Id
	 * @param transactionId Node-generated transaction identifier
	 * @param SSAPMessageParameters A vector of SSAPMessageParameters containing the insert and removed triplets
	 */
	public void update(String nodeId, String spaceId, long transactionId, Collection<SSAPMessageParameter> parameters, ClientProxy proxy);
	
	/**
	 * This method implements the SSAP QUERY REQUEST
	 * @param nodeId The node identifier
	 * @param spaceId Smart Space Id
	 * @param transactionId Node-generated transaction identifier
	 * @param SSAPMessageParameters A vector of SSAPMessageParameters containing the query
	 */
	public void query(String nodeId, String spaceId, long transactionId, Collection<SSAPMessageParameter> parameters, ClientProxy proxy);
	
	/**
	 * This method implements the SSAP SUBSCRIBE REQUEST
	 * @param nodeId The node identifier
	 * @param spaceId Smart Space Id
	 * @param transactionId Node-generated transaction identifier
	 * @param SSAPMessageParameters A vector of SSAPMessageParameters containing the query
	 */
	public void subscribe(String nodeId, String spaceId, long transactionId, Collection<SSAPMessageParameter> parameters, ClientProxy proxy);
	
	/**
	 * This method implements the SSAP UNSUBSCRIBE REQUEST
	 * @param nodeId The node identifier
	 * @param spaceId Smart Space Id
	 * @param transactionId Node-generated transaction identifier
	 * @param SSAPMessageParameters A vector of SSAPMessageParameters containing the query
	 */
	public void unsubscribe(String nodeId, String spaceId, long transactionId, Collection<SSAPMessageParameter> parameters, ClientProxy proxy);

	
}
