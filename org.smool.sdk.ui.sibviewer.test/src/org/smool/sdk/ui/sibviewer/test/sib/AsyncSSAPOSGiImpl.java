package org.smool.sdk.ui.sibviewer.test.sib;

/*
 *    Copyright 2010 European Software Institute
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *    
 *      http://www.apache.org/licenses/LICENSE-2.0
 *    
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *    
 */

import java.util.Collection;

import org.smool.sdk.sib.service.ISIB;
import org.smool.sdk.ssapmessage.SSAPMessage;
import org.smool.sdk.ssapmessage.SSAPMessageRequest;
import org.smool.sdk.ssapmessage.parameter.SSAPMessageParameter;
import org.smool.sdk.ssapmessage.proxy.ClientProxy;
import org.smool.sdk.ui.sibviewer.test.ssap.AsyncSSAP;


/**
 * This class makes an asynchronous connection to a SIB server.
 * The class implements the operations that can be executed in the server.
 * 
 * @author Cristina L�pez, cristina.lopez@tecnalia.com, ESI
 *
 */
public class AsyncSSAPOSGiImpl implements AsyncSSAP {

	/**
	 * Object containing the SIB Service
	 */
	private ISIB asyncSIB = null;

	/**
	 * Default Constructor
	 */
	public AsyncSSAPOSGiImpl() {
	}
	
	/**
	 * Constructor 
	 * @param asyncSIB
	 */
	public AsyncSSAPOSGiImpl(ISIB asyncSIB) {
		this.asyncSIB = asyncSIB;
	}

	/**
	 * Sets the SIB service reference
	 * @param sib Asynchronous SIB service reference
	 */
	public void setSIB(ISIB sib) {
		this.asyncSIB = sib;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see eu.sofia.adk.ui.sibviewer.test.ssap.AsyncSSAP
	 */
	public void join(String nodeId, String spaceId, long transactionId,
			Collection<SSAPMessageParameter> parameters, ClientProxy proxy) {
		
		try {
			SSAPMessageRequest message = new SSAPMessageRequest(nodeId, spaceId, transactionId, 
				SSAPMessage.TransactionType.JOIN, parameters);
		
			// Calls to the SIB join (in async mode)
			message.setClientProxy(proxy);
			asyncSIB.connect(message);
		} catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see eu.sofia.adk.ui.sibviewer.test.ssap.AsyncSSAP
	 */
	public void leave(String nodeId, String spaceId, long transactionId, ClientProxy proxy) {
		
		try
		{
		SSAPMessageRequest message = new SSAPMessageRequest(nodeId, spaceId, transactionId, 
				SSAPMessage.TransactionType.LEAVE);

		// Calls to the SIB join (in async mode)
		message.setClientProxy(proxy);
		
		// Calls to the SIB join (in async mode)
		asyncSIB.disconnect(message);
		}
		catch (Exception e){e.printStackTrace();}

	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see eu.sofia.adk.ui.sibviewer.test.ssap.AsyncSSAP
	 */
	public void insert(String nodeId, String spaceId, long transactionId, 
			Collection<SSAPMessageParameter> parameters, ClientProxy proxy) {
		try
		{
		SSAPMessageRequest message = new SSAPMessageRequest(nodeId, spaceId, transactionId, 
				SSAPMessage.TransactionType.INSERT, parameters);
		
		message.setClientProxy(proxy);
		
		// Calls to the SIB join (in async mode)
		asyncSIB.process(message);
		}
		catch(Exception e){e.printStackTrace();}

	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see eu.sofia.adk.ui.sibviewer.test.ssap.AsyncSSAP
	 */
	public void remove(String nodeId, String spaceId, long transactionId, 
			Collection<SSAPMessageParameter> parameters, ClientProxy proxy) {
		try
		{
		SSAPMessageRequest message = new SSAPMessageRequest(nodeId, spaceId, transactionId, 
				SSAPMessage.TransactionType.REMOVE, parameters);
		
		message.setClientProxy(proxy);		
		// Calls to the SIB join (in async mode)
		asyncSIB.process(message);
		}
		catch(Exception e){e.printStackTrace();}
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see eu.sofia.adk.ui.sibviewer.test.ssap.AsyncSSAP
	 */
	public void update(String nodeId, String spaceId, long transactionId, 
			Collection<SSAPMessageParameter> parameters, ClientProxy proxy) {
		try {
			SSAPMessageRequest message = new SSAPMessageRequest(nodeId, spaceId, transactionId, 
					SSAPMessage.TransactionType.UPDATE, parameters);
			message.setClientProxy(proxy);
			// Calls to the SIB join (in async mode)
			asyncSIB.process(message);
		} catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see eu.sofia.adk.ui.sibviewer.test.ssap.AsyncSSAP
	 */
	public void subscribe(String nodeId, String spaceId, long transactionId,
			Collection<SSAPMessageParameter> parameters, ClientProxy proxy) {
		try
		{
		SSAPMessageRequest message = new SSAPMessageRequest(nodeId, spaceId, transactionId, 
				SSAPMessage.TransactionType.SUBSCRIBE, parameters);
		message.setClientProxy(proxy);
		// Calls to the SIB join (in async mode)
		asyncSIB.process(message);
		}
		catch(Exception e){ e.printStackTrace();}

	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see eu.sofia.adk.ui.sibviewer.test.ssap.AsyncSSAP
	 */
	public void unsubscribe(String nodeId, String spaceId, long transactionId, 
			Collection<SSAPMessageParameter> parameters, ClientProxy proxy) {
		try
		{
		SSAPMessageRequest message = new SSAPMessageRequest(nodeId, spaceId, transactionId, 
				SSAPMessage.TransactionType.UNSUBSCRIBE, parameters);
		message.setClientProxy(proxy);
		// Calls to the SIB join (in async mode)
		asyncSIB.process(message);
		}
		catch(Exception e){e.printStackTrace();}

	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see eu.sofia.adk.ui.sibviewer.test.ssap.AsyncSSAP
	 */
	public void query(String nodeId, String spaceId, long transactionId,
			Collection<SSAPMessageParameter> parameters, ClientProxy proxy) {
		try
		{
		SSAPMessageRequest message = new SSAPMessageRequest(nodeId, spaceId, transactionId, 
				SSAPMessage.TransactionType.QUERY, parameters);
		message.setClientProxy(proxy);
		// Calls to the SIB join (in async mode)
		asyncSIB.process(message);
		}
		catch(Exception e){e.printStackTrace();
		}

	}

}
