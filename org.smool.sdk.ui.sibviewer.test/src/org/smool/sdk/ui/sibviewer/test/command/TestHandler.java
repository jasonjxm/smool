package org.smool.sdk.ui.sibviewer.test.command;

/*
 *    Copyright 2010 European Software Institute
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *    
 *      http://www.apache.org/licenses/LICENSE-2.0
 *    
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *    
 */

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.smool.sdk.sib.service.ISIB;
import org.smool.sdk.ui.sibviewer.test.Activator;
import org.smool.sdk.ui.sibviewer.test.command.wizard.NewSmartAppTestWizard;
import org.smool.sdk.ui.sibviewer.test.sib.AsyncSSAPOSGiImpl;


/**
 * This class is used to control the behavior of the button/command that appears
 * in the Tool bar menu and that is used to execute the Smart Application Test
 * 
 * @author Cristina L�pez, cristina.lopez@tecnalia.com, ESI
 *
 */
public class TestHandler extends AbstractHandler {

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try{
			NewSmartAppTestWizard wizard= new NewSmartAppTestWizard();
			Shell shell= new Shell();
			WizardDialog dialog = new WizardDialog(shell, wizard);
	        dialog.create();
	        dialog.open();	       
			
			String serverName=wizard.getServer();
			if (serverName!=null){
				if (serverName.length()!=0 ){
					ISIB server=Activator.getDefault().getServerMap().get(serverName);
					AsyncSSAPOSGiImpl service= Activator.getDefault().getServicesMap().get(server);
					Activator.getDefault().registertSmartApp(service, serverName);
				}
				else{
					IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
					MessageDialog.openInformation(
							window.getShell(),
							"Command",
							"Please, select a valid server");
				}
			}
		}
		catch(Exception e){e.printStackTrace();}
		return null;
	}

}
