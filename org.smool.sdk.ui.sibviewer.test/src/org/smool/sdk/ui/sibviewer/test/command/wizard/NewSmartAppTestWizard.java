package org.smool.sdk.ui.sibviewer.test.command.wizard;

/*
 *    Copyright 2010 European Software Institute
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *    
 *      http://www.apache.org/licenses/LICENSE-2.0
 *    
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *    
 */

import org.eclipse.jface.wizard.Wizard;

/**
 * This is a Wizard that is called before creating the Smart Application tests and which 
 * displays the current list of active servers to which the tests can be connected 
 * 
 * @author Cristina L�pez, cristina.lopez@tecnalia.com, ESI
 *
 */
public class NewSmartAppTestWizard extends Wizard{
	/**
	 * Page of the wizard that shows the list of available servers
	 */
	private ServerSelectionPage page;
	
	/**
	 * It is used to store the server that has been selected using the wizard
	 */
	private String server;
	
	/**
	 * Default Constructor
	 */
	public NewSmartAppTestWizard(){
	 this.setWindowTitle("New Smart Application Test");
	}
	
	/**
	 * Gets the server selected.
	 * this method is used externally to obtain the server that has been selected using
	 * the wizard
	 * @return
	 */
	public String getServer() {
		return server;
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.wizard.Wizard
	 */
    public void addPages() {
    	page = new ServerSelectionPage("New Smart Application Test");
        addPage(page);
    }
    
	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.wizard.Wizard
	 */
	public boolean performFinish() {
			try{
				if (page.success==true){
					server= page.server.getText();
				}
			}
			catch(Exception e){e.printStackTrace();}
			return true;
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.wizard.Wizard
	 */
	public boolean performCancel() {		
		return true;
	}

}
