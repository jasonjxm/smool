package org.smool.sdk.ui.sibviewer.test.ui;

/*
 *    Copyright 2010 European Software Institute
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *    
 *      http://www.apache.org/licenses/LICENSE-2.0
 *    
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *    
 */

import javax.swing.JTabbedPane;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JButton;

import org.smool.sdk.ssapmessage.SSAPMessage;
import org.smool.sdk.ssapmessage.proxy.ClientProxy;
import org.smool.sdk.ssapmessage.proxy.SIBMessageListener;
import org.smool.sdk.ui.sibviewer.test.Activator;
import org.smool.sdk.ui.sibviewer.test.sib.AsyncSSAPOSGiImpl;
import org.smool.sdk.ui.sibviewer.test.utils.Utils;

import java.awt.*;
import java.awt.event.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;



/**
 * This class implements the Smart Application test window.
 * Before being able to perform the test operations its necessary to choose
 * a smart application id and join to the server.
 * 
 * @author Cristina L�pez, cristina.lopez@tecnalia.com, ESI
 *
 */
public class MainWindow extends JPanel implements SIBMessageListener{
	
	/**
	 * User Interface identifier
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Contains the asynchronous connection to the server
	 */
	private AsyncSSAPOSGiImpl async;
	/**
	 * Identifier of the server selected
	 */
	private String serverId;
	
	/**
	 * Frame used to implement the window
	 */
	private JFrame frame;
	/**
	 * Name of the smart application
	 */
	private TextField node;
	/**
	 * Space identifier
	 */
	private TextField space;
	/**
	 * Text area that displays the messages or errors receibed from the server
	 */
	private static TextArea textSib;
	/**
	 * Button used to join or leave the server
	 */
	private JButton joinButton;
	
	/**
	 * Tabs containing the operations that can be performed over the server
	 */
	private JTabbedPane tabsPanel;
	/**
	 * Message text area created to introduce the parameters that are send to the
	 * server when an operation is performed.
	 * This text area is initialized with a default message different for each kind
	 * of operation.
	 */
	private TextArea xmlText;
	/**
	 * Button that performs the selected operation and sends the parameters introduced in
	 * the text area created for this purpose
	 */
	private JButton sendButton;
	/**
	 * Button that clear the default messages that appear in the message text area
	 */
	private JButton clearButton;
	
	/**
	 * Internal variable used to count the number of operations made in the server
	 */
	private long transactionId;	
	/**
	 * Proxy used to connect to the server
	 */
	private ClientProxy proxy;
	/**
	 * Instantiation of the Window
	 */
	private MainWindow ui;
 
	/**
	 * Window constructor
	 * @param serverId
	 * @param async
	 */
	public MainWindow(String serverId,final AsyncSSAPOSGiImpl async ) {
		this.ui=this;
		this.serverId=serverId;
		initializeWindow();
		this.async=async;
		transactionId= new Long(0);
		proxy= new ClientProxy();
		proxy.addSIBMessageListener(this);	
		frame=new JFrame("Smart Application Test");
		frame.addWindowListener(new WindowAdapter() {

			public void windowClosing(WindowEvent e) {
				if (joinButton.getText().equals("Leave Sib Server")){
					transactionId++;
			    	async.leave(node.getText(), space.getText(), transactionId, proxy);	
				}	
				Activator.getDefault().removeWindow(ui.serverId,ui);
			}
		});
	}
	
	/**
	 * Gets the internal frame of the window
	 * @return JFrame
	 */
	public JFrame getFrame() {
		return frame;
	}	
	
	/**
	 * Method used externally to create new Window tests
	 * @param args
	 * @param async
	 * @return MainWindow
	 */
	public static MainWindow create(String[] args, AsyncSSAPOSGiImpl async) {
		
		MainWindow ui= new MainWindow(args[0], async); 	
		ui.frame.getContentPane().add(ui,BorderLayout.CENTER);
		ui.frame.setSize(550, 780);
		ui.frame.setVisible(true);
		
		return ui;
	}
	
	@Override
	/*
	 * (non-Javadoc)
	 * @see eu.sofia.adk.ssapmessage.proxy.SIBMessageListener
	 */
	public void messageReceived(SSAPMessage message) {
		System.out.println(message.toString());
		textSib.setText(message.toString());

		if (message.toString().indexOf("ERROR")!= -1){
			textSib.setBackground(Color.RED);
		}
		else{
		textSib.setBackground(Color.WHITE);
		}
	}
	
	/**
	 * Method both used internally by the join/leave button and externally
	 * to force the leaving of the server when necessary
	 */
	public void leaveServer(){
     	transactionId++;
    	async.leave(node.getText(), space.getText(), transactionId, proxy);
	}
	
	/**
	 * Method that checks if the smart application is connected or not to the server
	 * @return
	 */
	public boolean isConnected(){
		if (joinButton.getText().equals("Join Sib Server")){
			return false;
		}
		else{ 
			return true;
		}
	}
	
	/**
	 * Method that creates the structure of the window
	 */
	private void initializeWindow(){
		
		//SIB Panel
		JPanel sibPanel= new JPanel();
		sibPanel.setSize(550, 200);
		
		Label serverText= new Label ("Sib Id :");
		Label server= new Label(serverId);
		
		Label nodeText= new Label ("Node Id :");
		nodeText.setSize(200, 20);
		node= new TextField(50);
		node.setText("myKP");
		
		Label spaceText= new Label ("Space Id :");
		space= new TextField(50);
		space.setText("sib1");
		
		
		JPanel left= new JPanel();
		left.setLayout(new BorderLayout());
		left.add(serverText, BorderLayout.NORTH);
		left.add(nodeText, BorderLayout.CENTER);
		left.add(spaceText, BorderLayout.SOUTH);
		JPanel right= new JPanel();
		right.setLayout(new BorderLayout());
		right.add(server, BorderLayout.NORTH);
		right.add(node, BorderLayout.CENTER);
		right.add(space, BorderLayout.SOUTH);	
		JPanel info= new JPanel();
		info.setLayout(new BorderLayout());
		info.add(left, BorderLayout.WEST);
		info.add(right, BorderLayout.CENTER);
		
		
		JPanel buttonsPanel= new JPanel();		
		joinButton=new JButton("Join Sib Server");
		joinleaveListener();
		
		buttonsPanel.add(joinButton);
		
		JPanel responsePanel= new JPanel();
		responsePanel.setLayout(new BorderLayout());
		Label sibLabel= new Label ("Sib Response:");
		textSib=new TextArea(10,70);		
		responsePanel.add(sibLabel, BorderLayout.NORTH);
		responsePanel.add(textSib, BorderLayout.CENTER);
				
		
		sibPanel.setLayout(new FlowLayout());
		sibPanel.add(info);
		sibPanel.add(buttonsPanel);
		sibPanel.add(responsePanel);
		
		//Main Panel
		JPanel mainPanel= new JPanel();
		mainPanel.setLayout(new BorderLayout());
		tabsPanel = new JTabbedPane();
		String[] titles = {"Insert", "Update", "Query", "Subscribe", "Unsubscribe", "Remove"};
		for (int i=0;i<titles.length;i++)
		{
			JPanel innerPanel = createInnerPanel(titles[i]);
			tabsPanel.addTab(titles[i], innerPanel);	
		}
		tabsPanel.setSelectedIndex(0);
		tabsPanel.setSize(550, 350);
		tabsPanel.setEnabled(false);
		mainPanel.add(tabsPanel);
		
		JPanel buttons= new JPanel();
		sendButton=new JButton("Sent");
		sendButton.setEnabled(false);
		sendListener(sendButton);
		clearButton=new JButton("Clear");
		clearButton.setEnabled(false);
		clearListener(clearButton);

		buttons.setLayout(new FlowLayout());
		buttons.add(sendButton);
		buttons.add(clearButton);
		mainPanel.add(buttons, BorderLayout.SOUTH);

		// Add the tabbed pane to this panel.
		setLayout(new GridLayout(2,1));		
		add(sibPanel);
		add(mainPanel);
	}
	
	/**
	 * Method that creates the panel that contains operations
	 * @param text
	 * @return JPanel
	 */
	private JPanel createInnerPanel(String text) {
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout());	
		
		JPanel parametersPanel = new JPanel();
		parametersPanel.setLayout(new BorderLayout());
		Label parametersText= new Label ("Parameters :");
		xmlText=new TextArea(17,70);
		xmlText.setText(Utils.convertVectorToString(Utils.getDefaultText(text)));
		xmlText.setEnabled(false);
		parametersPanel.add(parametersText, BorderLayout.PAGE_START);
		parametersPanel.add(xmlText, BorderLayout.PAGE_END);
		panel.add(parametersPanel);
		return panel;
	}

	/**
	 * Method that disables the operations tab if the smart application is not
	 * connected to the server
	 */
	private void hideOperations()
	{
		tabsPanel.setEnabled(false);
		sendButton.setEnabled(false);
		clearButton.setEnabled(false);
		for (int i=0; i<tabsPanel.getComponents().length;i++){
			JPanel current=(JPanel) tabsPanel.getComponents()[i];
			TextArea text= (TextArea)((JPanel)current.getComponent(0)).getComponent(1);
			text.setEnabled(false);
			
		}
	}
	
	/**
	 * Method that enables the operations tab if the smart application is connected
	 * to the server
	 */
	private void unhideOperations()
	{
		
		tabsPanel.setEnabled(true);
		sendButton.setEnabled(true);
		clearButton.setEnabled(true);
		for (int i=0; i<tabsPanel.getComponents().length;i++){
			JPanel current=(JPanel) tabsPanel.getComponents()[i];
			TextArea text= (TextArea)((JPanel)current.getComponent(0)).getComponent(1);
			text.setEnabled(true);
			
		}
	}
	
	/**
	 * Method that disables the parameters are if the smart application is not
	 * connected to the server
	 */
	private void hideParameters()
	{
		node.setEnabled(false);
		space.setEnabled(false);
	}
	
	/**
	 * Method that enabled the parameters are if the smart application is 
	 * connected to the server
	 */
	private void unhideParameters()
	{
		node.setEnabled(true);
		space.setEnabled(true);
	}
	
	/**
	 * Method that implements the behavior of the join/leave button
	 */
	private void joinleaveListener(){
		joinButton.addActionListener(new ActionListener() {
            
            public void actionPerformed(ActionEvent e)
            {
                if (joinButton.getText().equals("Join Sib Server")){
                	joinButton.setText("Leave Sib Server");
                	joinServer();
                	unhideOperations();
                	hideParameters();
                }
                else if (joinButton.getText().equals("Leave Sib Server")){
                	joinButton.setText("Join Sib Server");
                	leaveServer();
                	hideOperations();
                	unhideParameters();
                }
            }
        });
	}
	
	/**
	 * Method called internally to join the server
	 */
	private void joinServer(){
    	transactionId++;
    	async.join(node.getText(), space.getText(), transactionId,null, proxy);
    	transactionId++;
    	async.insert(node.getText(), space.getText(), transactionId, Utils.obtainDefaultModel(getContents("/model/vCard/vCard.rdf")), proxy);
//    	async.insert(node.getText(), space.getText(), transactionId, Utils.obtainDefaultModel(getContents("/model/testSet/model.owl")), proxy);
	}
	
	/**
	 * Method that implements the behavior of the send button
	 * @param button
	 */
	private void sendListener(JButton button){
		button.addActionListener(new ActionListener() {
            
            public void actionPerformed(ActionEvent e)
            {
            	String operation=tabsPanel.getTitleAt(tabsPanel.getSelectedIndex());
            	System.out.println("Capturing "+operation.toUpperCase()+" message sent...");           	
            	JPanel selected=(JPanel) tabsPanel.getComponentAt(tabsPanel.getSelectedIndex());
            	TextArea text= (TextArea)((JPanel)selected.getComponents()[0]).getComponent(1);
            	System.out.println("Parameters "+ text.getText());
            	transactionId++;
            	Utils.executeCommand(operation,text.getText(),async, node.getText(), space.getText(), transactionId, proxy);
            }
        });
	}
	
	/**
	 * Method that implements the behavior of the clear button
	 * @param button
	 */
	private void clearListener (JButton button){
		button.addActionListener(new ActionListener() {            
            public void actionPerformed(ActionEvent e)
            {
            	System.out.println("Clearing Up "+tabsPanel.getTitleAt(tabsPanel.getSelectedIndex()).toUpperCase()+" parameters...");
            	JPanel selected=(JPanel) tabsPanel.getComponentAt(tabsPanel.getSelectedIndex());
            	TextArea text= (TextArea)((JPanel)selected.getComponents()[0]).getComponent(1);
            	text.setText("");
            }
        });
	}
	
	/**
	 * Method used to read a file containing a model.
	 * This method is used to set a default test model.
	 * @param fileName
	 * @return String
	 */
	private String getContents(String fileName) {
		//...checks on aFile are elided
		StringBuilder contents = new StringBuilder();
		
		InputStream is = null;
		
		try {
			is = getClass().getResourceAsStream(fileName);
			//is= window.getClass().getResourceAsStream(fileName);
			
			final char[] buffer = new char[0x10000]; 
			Reader in = new InputStreamReader(is, "UTF-8"); 
			int read; 
			do { 
			  read = in.read(buffer, 0, buffer.length); 
			  if (read>0) { 
			    contents.append(buffer, 0, read); 
			  } 
			} while (read>=0);			
			
		} catch (IOException ex){
			ex.printStackTrace();
	    } finally {
	    	try {
	    		is.close();
	    	} catch (IOException ex) {
	    		ex.printStackTrace();
	    	}
		}
		    
		return contents.toString();
	}

		
}
