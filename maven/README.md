# MAVEN PUBLIC REPO FOR SMOOL

## Create .m2 structure

Install into .m2 by creating the folders and the jar/pom files
 
`mvn install:install-file -Dfile=/home/ubuntu/SOFTWARE/SMOOL/smool/org.smool.sdk.wizard.j2segenerator/kp_libs/org.smool.kpi.model.smart_2.2.5.jar -DgroupId=org.smool.kpi.model -DartifactId=smart -Dversion=2.2.5 -Dpackaging=jar`

`mvn install:install-file -Dfile=/home/ubuntu/SOFTWARE/SMOOL/smool/org.smool.sdk.wizard.j2segenerator/kp_libs/org.smool.kpi.model.smart_2.2.5.jar -DgroupId=org.smool.kpi.model -DartifactId=smart -Dversion=2.2.5 -Dpackaging=jar`

`mvn install:install-file -Dfile=/home/ubuntu/SOFTWARE/SMOOL/smool/org.smool.sdk.wizard.j2segenerator/kp_libs/org.smool.kpi.connector_2.2.5.jar -DgroupId=org.smool.kpi -DartifactId=connector -Dversion=2.2.5 -Dpackaging=jar`

`mvn install:install-file -Dfile=/home/ubuntu/SOFTWARE/SMOOL/smool/org.smool.sdk.wizard.j2segenerator/kp_libs/org.smool.kpi.connector.tcpip_2.2.5.jar -DgroupId=org.smool.kpi.connector -DartifactId=tcpip -Dversion=2.2.5 -Dpackaging=jar`

`mvn install:install-file -Dfile=/home/ubuntu/SOFTWARE/SMOOL/smool/org.smool.sdk.wizard.j2segenerator/kp_libs/org.smool.kpi.model_2.2.5.jar -DgroupId=org.smool.kpi -DartifactId=model -Dversion=2.2.5 -Dpackaging=jar`

`mvn install:install-file -Dfile=/home/ubuntu/SOFTWARE/SMOOL/smool/org.smool.sdk.wizard.j2segenerator/kp_libs/org.smool.kpi.model.direct_2.2.5.jar -DgroupId=org.smool.kpi.model -DartifactId=direct -Dversion=2.2.5 -Dpackaging=jar`

`mvn install:install-file -Dfile=/home/ubuntu/SOFTWARE/SMOOL/smool/org.smool.sdk.wizard.j2segenerator/kp_libs/org.smool.kpi.ssap_2.2.5.jar -DgroupId=org.smool.kpi -DartifactId=ssap -Dversion=2.2.5 -Dpackaging=jar`




Test: add the dependency to your project-> it works

## Copy the folder structure into a "REPO" folder

Just add the folder structure and contens to the folder that will become the public repo.

Then delete the package in the .m2 folder, so to ensure the maven installation will take your repo (and not the .m2 installed previously)

## Add the new REPO to your projects

Either http://... or local folder at file://... can be used (http for public accesible repos and file:// for handy to use internal repos

```xml
<project>
  ...
  <repositories>
    <repository>
      <id>smool_remote</id><url>http://smool.tecnalia.com:8080</url>
    </repository>
    <repository>
      <id>smool_local</id><url>file:///home/ubuntu/SOFTWARE/SMOOL/smool/maven</url>
    </repository>
  </repositories>
  ...
</project>
```
