> **UPDATE (18/05/2023)**. This project is no longer maintained->**CLOSED**. Issue tracking has been closed as well. Thanks to developers contributing since the UE project [SOFIA (2009-2011)](https://cordis.europa.eu/project/id/100017) where OSGi was the basis for a modular approach and semantic technologies allowed to abstract the internals of IoT devices to a common language, travelling around regional public and private projects where auto-generation of clients, SDK tools and more device connectivity was added, and ending in another UE project [ENACT(2018-2021)](https://cordis.europa.eu/project/id/780351) focused on securing the IoT in different layers (application level, policies, etc.) and the SMOOL server was really put into production mode with continuous usage and exposed to real attacks from the Internet.

# SMOOL - Smartify the world #

Smool - Smart Tool, hopes to close the gap between the physical world and the virtual world by providing a set of tools that will make the creation of "Smart Spaces" a snap. A very easy and intuitive editor will allow non-techie users to interconnect devices, sensors and actuators with Internet Services, while a set of tools aimed for developers will be also deployed, based on Eclipse, that will also allow the use of smart applications using the Middleware "under the hood" of Smool.

More info, installation and running instructions at [Wiki](https://bitbucket.org/jasonjxm/smool/wiki/Home).

Some example videos can be played at the IoT team channel in [youtube](https://www.youtube.com/playlist?list=PLuc0lPfwaR5EQBHdUqR2AUXEYf4HTrPlr).

Powerpoint and other useful presentations are available in [google drive](https://drive.google.com/folderview?id=0B0mpCuAqjVNnRHdTQm90RV9xYnc&usp=sharing).



>Note: this project was migrated from google code. See [smool@google](https://code.google.com/p/smool) for old releases and history.