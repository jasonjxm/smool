This is just a plain folder to drop the generated files when exporting the SMOOL_ALL feature. This project was a real updatesite with site.xml before, but it is not needed anymore (this is just a folder to export the feature).

For building the updatesite read the `org.smool.sdk.features.all/README.md` file

> 	DO NOT FORGET TO ZIP ONLY THE CONTENTS, NOT THE PARENT FOLDER (see issue #76).
