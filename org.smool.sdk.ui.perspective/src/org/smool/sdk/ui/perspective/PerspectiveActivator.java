/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.perspective;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/** 
 * Customizes the starting and stopping of a bundle.
 * 
 * <code>BundleActivator</code> is an interface that may be implemented when a bundle is 
 * started or stopped. The Framework can create instances of a bundle's 
 * <code>BundleActivator</code> as required. If an instance's <code>BundleActivator.start</code> method 
 * executes successfully, it is guaranteed that the same instance's 
 * <code>BundleActivator.stop</code> method will be called when the bundle is to be stopped. 
 * The Framework must not concurrently call a <code>BundleActivator</code> object.
 * 
 * <code>BundleActivator<code> is specified through the <code>Bundle-Activator</code> Manifest header. 
 * A bundle can only specify a single <code>BundleActivator</code> in the Manifest file. 
 * Fragment bundles must not have a <code>BundleActivator</code>. The form of the Manifest 
 * header is:
 * 
 * <code>Bundle-Activator: class-name</code>
 * 
 * where <code>class-name</code> is a fully qualified Java classname.
 * 
 * The specified <code>BundleActivator</code> class must have a public constructor that 
 * takes no parameters so that a <code>BundleActivator</code> object can be created by 
 * <code>Class.newInstance()</code>. 
 * 
 * @author Fran Ruiz, fran.ruiz@tecnalia.com, ESI
 */
public class PerspectiveActivator implements BundleActivator {

	/** The plug-in id */
	public static final String PLUGIN_ID = "org.smool.sdk.ui.perspective";

	/** The shared instance */
	@SuppressWarnings("unused")
	private static PerspectiveActivator plugin;
	
	/** The bundle context */
	@SuppressWarnings("unused")
	private static BundleContext bc;
	
	/**
	 * The constructor
	 */
	public PerspectiveActivator() {
	}

	/**
	 * Starts up this plug-in.
	 * 
	 * This method overrides the Plugin class in subclasses that need to do 
	 * something when this plug-in is started. The start class calls the
	 * <code>super()</code> method to ensure that any system requirements 
	 * can be met.
	 * 
	 *  If this method throws an exception, it is taken as an indication that 
	 *  plug-in initialization has failed; as a result, the plug-in will not 
	 *  be activated; moreover, the plug-in will be marked as disabled and 
	 *  ineligible for activation for the duration.
	 *  
	 *  Plug-in startup code should be robust. In the event of a startup 
	 *  failure, the plug-in's shutdown method will be invoked automatically, 
	 *  in an attempt to close open files, etc.
	 *  
	 *  <code>Note 1</code>: This method is automatically invoked by the 
	 *  platform the first time any code in the plug-in is executed.
	 *  
	 *  <code>Note 2</code>: This method is intended to perform simple 
	 *  initialization of the plug-in environment. The platform may terminate 
	 *  initializers that do not complete in a timely fashion.
	 *  
	 *  <code>Note 3</code>: The class loader typically has monitors acquired 
	 *  during invocation of this method. It is strongly recommended that this 
	 *  method avoid synchronized blocks or other thread locking mechanisms, 
	 *  as this would lead to deadlock vulnerability.
	 *  
	 *  <code>Note 4</code>: The supplied bundle context represents the plug-in 
	 *  to the OSGi framework. For security reasons, it is strongly recommended 
	 *  that this object should not be divulged.
	 *  
	 *  <code>Note 5</code>: This method and the stop(BundleContext) may be 
	 *  called from separate threads, but the OSGi framework ensures that both 
	 *  methods will not be called simultaneously.
	 *  
	 *  <strong>Clients must never explicitly call this method.</strong>
	 *   
	 * @see org.eclipse.core.runtime.Plugin#start(BundleContext)
	 */
	@SuppressWarnings("static-access")
	public void start(BundleContext context) throws Exception {
		plugin = this;
		this.bc = context;
	}


	/**
	 * Stops this plug-in.
	 * 
	 * This method overrides the Plugin class in subclasses that need to do 
	 * something when this plug-in is shut down. The stop class calls the
	 * <code>super()</code> method to ensure that any system requirements 
	 * can be met.
	 * 
	 * Plug-in shutdown code should be robust. In particular, this method 
	 * should always make an effort to shut down the plug-in. 
	 * Furthermore, the code should not assume that the plug-in was started 
	 * successfully, as this method will be invoked in the event of a failure 
	 * during startup.
	 * 
	 * <code>Note 1</code>: If a plug-in has been automatically started, 
	 * this method will be automatically invoked by the platform when the 
	 * platform is shut down.
	 * 
	 * <code>Note 2<code>: This method is intended to perform simple 
	 * termination of the plug-in environment. The platform may terminate 
	 * invocations that do not complete in a timely fashion.
	 * 
	 * <code>Note 3</code>: The supplied bundle context represents the plug-in 
	 * to the OSGi framework. For security reasons, it is strongly recommended 
	 * that this object should not be divulged.
	 * 
	 * <code>Note 4</code>: This method and the start(BundleContext) may be 
	 * called from separate threads, but the OSGi framework ensures that both 
	 * methods will not be called simultaneously.
	 * 
	 * <strong>Clients must never explicitly call this method.</strong>
	 * 
	 * @see org.eclipse.core.runtime.Plugin#stop(BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
	}
}
