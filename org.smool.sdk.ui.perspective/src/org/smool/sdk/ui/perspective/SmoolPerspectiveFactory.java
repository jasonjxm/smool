/*
 *    Copyright 2010 European Software Institute
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *    
 *      http://www.apache.org/licenses/LICENSE-2.0
 *    
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *    
 */
package org.smool.sdk.ui.perspective;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

/**
 * A perspective factory generates the initial page layout and visible action 
 * set for a page.
 * 
 * When a new page is created in the workbench a perspective is used to define 
 * the initial page layout. If this is a predefined perspective (based on an 
 * extension to the workbench's perspective extension point) an 
 * <code>IPerspectiveFactory</code> is used to define the initial page layout.
 * 
 * The factory for the perspective is created and passed an IPageLayout where 
 * views can be added. The default layout consists of the editor area with no 
 * additional views. Additional views are added to the layout using the editor 
 * area as the initial point of reference. The factory is used only briefly 
 * while a new page is created; then discarded.
 * 
 * To define a perspective clients should implement this interface and include 
 * the name of their class in an extension to the workbench's perspective 
 * extension point (named "<code>org.eclipse.ui.perspectives</code>").
 * 
 * @author Cristina Lopez, cristina.lopez@tecnalia.com, ESI
 * @author Fran Ruiz, fran.ruiz@tecnalia.com, ESI
 *
 */
public class SmoolPerspectiveFactory implements IPerspectiveFactory {

	/** The page layout */
	private IPageLayout layout;
	
	/** The logger */
	//private static Logger logger = Logger.getLogger(SmoolPerspectiveFactory.class);
	
	/**
	 * Creates the initial layout for a page.
	 * Implementors of this method may add additional views to a perspective. 
	 * The perspective already contains an editor folder identified by the 
	 * result of IPageLayout.getEditorArea(). Additional views should be added 
	 * to the layout using this value as the initial point of reference. 
	 * @param layout the page layout
	 */
	public void createInitialLayout(IPageLayout layout) {
		//logger.debug("Creating initial layout...");
		System.out.println("Creating inital layout...");
		this.layout = layout;
		addViews();
		addActionSets();
		addNewWizardShortcuts();
		addPerspectiveShortcuts();
		addViewShortcuts();
		//logger.debug("Layout successfully created!");
		System.out.println("Created initial layout successfully!");

	}

	@SuppressWarnings("deprecation")
	private void addViews() {
		// Creates the overall folder layout. 
		// Note that each new Folder uses a percentage of the remaining EditorArea.
		//Top left layout
		IFolderLayout topLeft =
			layout.createFolder(
				"topLeft", //NON-NLS-1
				IPageLayout.LEFT,
				0.25f,
				layout.getEditorArea());
		topLeft.addView("org.eclipse.jdt.ui.PackageExplorer");
		topLeft.addView("org.eclipse.ui.views.ResourceNavigator");
		/*change deprecated addfastvies to addview */
		layout.addFastView("org.eclipse.team.ccvs.ui.RepositoriesView",0.50f); //NON-NLS-1
		layout.addFastView("org.eclipse.team.sync.views.SynchronizeView", 0.50f); //NON-NLS-1
		
        // Bottom left layout
        IFolderLayout bottomLeft =
        	layout.createFolder("bottomLeft", IPageLayout.BOTTOM, (float)0.50, "topLeft");
        //bottomLeft.addView("org.eclipse.wst.server.ui.ServersView");
        bottomLeft.addView("org.smool.sdk.ui.sib.view");
        bottomLeft.addView(IPageLayout.ID_OUTLINE);

        // Bottom Right layout
		IFolderLayout bottom =
			layout.createFolder(
				"bottomRight", //NON-NLS-1
				IPageLayout.BOTTOM,
				0.65f,
				layout.getEditorArea());
		

		bottom.addView("org.smool.sdk.ui.sibviewer.SibViewer");
		bottom.addView(IPageLayout.ID_PROP_SHEET);
		bottom.addView("org.eclipse.ui.console.ConsoleView");
		bottom.addView(IPageLayout.ID_PROBLEM_VIEW);
		bottom.addView(IPageLayout.ID_TASK_LIST);
		bottom.addView(IPageLayout.ID_EDITOR_AREA);
		
	}

	private void addActionSets() {
		//factory.addActionSet("org.eclipse.debug.ui.launchActionSet"); //NON-NLS-1
		layout.addActionSet("org.eclipse.debug.ui.debugActionSet"); //NON-NLS-1
		layout.addActionSet("org.eclipse.debug.ui.profileActionSet"); //NON-NLS-1
		layout.addActionSet("org.eclipse.jdt.debug.ui.JDTDebugActionSet"); //NON-NLS-1
		layout.addActionSet("org.eclipse.jdt.junit.JUnitActionSet"); //NON-NLS-1
		//factory.addActionSet("org.eclipse.team.ui.actionSet"); //NON-NLS-1
		//factory.addActionSet("org.eclipse.team.cvs.ui.CVSActionSet"); //NON-NLS-1
		layout.addActionSet("org.eclipse.ant.ui.actionSet.presentation"); //NON-NLS-1
		//factory.addActionSet(JavaUI.ID_ACTION_SET);
		//factory.addActionSet(JavaUI.ID_ELEMENT_CREATION_ACTION_SET);
		layout.addActionSet(IPageLayout.ID_NAVIGATE_ACTION_SET); //NON-NLS-1
	}

	private void addPerspectiveShortcuts() {
		layout.addPerspectiveShortcut("org.smool.plugins.ui.perspective");
		layout.addPerspectiveShortcut("org.eclipse.debug.ui.DebugPerspective");
		layout.addPerspectiveShortcut("org.eclipse.pde.ui.PDEPerspective");
		layout.addPerspectiveShortcut("org.eclipse.jdt.ui.JavaPerspective");
	}

	private void addNewWizardShortcuts() {
		layout.addNewWizardShortcut("org.smool.sdk.ui.wizard.CreateNewKPWizard");//NON-NLS-1
		//layout.addNewWizardShortcut("NewModelLayerWizard");//NON-NLS-1
		layout.addNewWizardShortcut("org.eclipse.ui.wizards.new.folder");//NON-NLS-1
		layout.addNewWizardShortcut("org.eclipse.ui.wizards.new.file");//NON-NLS-1
		//layout.addNewWizardShortcut("org.smool.sdk.ui.projectplugin.Project");
		
	}

	private void addViewShortcuts() {
		layout.addShowViewShortcut("org.smool.plugins.sibviewer.ui.views.SIBSessions");	
		layout.addShowViewShortcut("org.smool.plugins.sibviewer.ui.views.SIBSubscriptions");
		layout.addShowViewShortcut("org.smool.plugins.sibviewer.ui.views.SIBTriples");
		layout.addShowViewShortcut("org.eclipse.wst.server.ui.ServersView");
		layout.addShowViewShortcut("org.eclipse.pde.ui.DependenciesView"); //NON-NLS-1
		layout.addShowViewShortcut("org.eclipse.team.ui.GenericHistoryView"); //NON-NLS-1
		layout.addShowViewShortcut("org.eclipse.ui.console.ConsoleView");
		layout.addShowViewShortcut(IPageLayout.ID_PROBLEM_VIEW);
		layout.addShowViewShortcut(IPageLayout.ID_OUTLINE);
	}

}
