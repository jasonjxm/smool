/**
 */
package org.smool.sdk.owlparser.owlmodel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sub Type Of</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.SubTypeOf#isResourceBased <em>Resource Based</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.SubTypeOf#getClassRef <em>Class Ref</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.SubTypeOf#getModifiers <em>Modifiers</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getSubTypeOf()
 * @model
 * @generated
 */
public interface SubTypeOf extends EObject
{
  /**
   * Returns the value of the '<em><b>Resource Based</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Resource Based</em>' attribute.
   * @see #setResourceBased(boolean)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getSubTypeOf_ResourceBased()
   * @model
   * @generated
   */
  boolean isResourceBased();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.SubTypeOf#isResourceBased <em>Resource Based</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Resource Based</em>' attribute.
   * @see #isResourceBased()
   * @generated
   */
  void setResourceBased(boolean value);

  /**
   * Returns the value of the '<em><b>Class Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Class Ref</em>' containment reference.
   * @see #setClassRef(OwlRef)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getSubTypeOf_ClassRef()
   * @model containment="true"
   * @generated
   */
  OwlRef getClassRef();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.SubTypeOf#getClassRef <em>Class Ref</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Class Ref</em>' containment reference.
   * @see #getClassRef()
   * @generated
   */
  void setClassRef(OwlRef value);

  /**
   * Returns the value of the '<em><b>Modifiers</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.ClassModifier}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Modifiers</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getSubTypeOf_Modifiers()
   * @model containment="true"
   * @generated
   */
  EList<ClassModifier> getModifiers();

} // SubTypeOf
