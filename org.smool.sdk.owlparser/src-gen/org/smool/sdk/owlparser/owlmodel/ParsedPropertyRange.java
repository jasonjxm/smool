/**
 */
package org.smool.sdk.owlparser.owlmodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parsed Property Range</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.ParsedPropertyRange#getParseType <em>Parse Type</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.ParsedPropertyRange#getModifiers <em>Modifiers</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getParsedPropertyRange()
 * @model
 * @generated
 */
public interface ParsedPropertyRange extends RDFSRange
{
  /**
   * Returns the value of the '<em><b>Parse Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Parse Type</em>' containment reference.
   * @see #setParseType(ParseType)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getParsedPropertyRange_ParseType()
   * @model containment="true"
   * @generated
   */
  ParseType getParseType();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.ParsedPropertyRange#getParseType <em>Parse Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Parse Type</em>' containment reference.
   * @see #getParseType()
   * @generated
   */
  void setParseType(ParseType value);

  /**
   * Returns the value of the '<em><b>Modifiers</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.ClassModifier}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Modifiers</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getParsedPropertyRange_Modifiers()
   * @model containment="true"
   * @generated
   */
  EList<ClassModifier> getModifiers();

} // ParsedPropertyRange
