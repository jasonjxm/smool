/**
 */
package org.smool.sdk.owlparser.owlmodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simple RDFS Range</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.SimpleRDFSRange#getRange <em>Range</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getSimpleRDFSRange()
 * @model
 * @generated
 */
public interface SimpleRDFSRange extends RDFSRange
{
  /**
   * Returns the value of the '<em><b>Range</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Range</em>' containment reference.
   * @see #setRange(OwlRef)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getSimpleRDFSRange_Range()
   * @model containment="true"
   * @generated
   */
  OwlRef getRange();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.SimpleRDFSRange#getRange <em>Range</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Range</em>' containment reference.
   * @see #getRange()
   * @generated
   */
  void setRange(OwlRef value);

} // SimpleRDFSRange
