/**
 */
package org.smool.sdk.owlparser.owlmodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Datatype Property</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getDatatypeProperty()
 * @model
 * @generated
 */
public interface DatatypeProperty extends Property
{
} // DatatypeProperty
