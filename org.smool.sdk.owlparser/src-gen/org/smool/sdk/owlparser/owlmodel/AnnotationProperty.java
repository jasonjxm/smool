/**
 */
package org.smool.sdk.owlparser.owlmodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Annotation Property</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getAnnotationProperty()
 * @model
 * @generated
 */
public interface AnnotationProperty extends Property
{
} // AnnotationProperty
