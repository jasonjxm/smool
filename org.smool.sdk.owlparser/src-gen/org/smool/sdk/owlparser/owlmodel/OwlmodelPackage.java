/**
 */
package org.smool.sdk.owlparser.owlmodel;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelFactory
 * @model kind="package"
 * @generated
 */
public interface OwlmodelPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "owlmodel";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.smool.org/sdk/owlparser/OWLModel";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "owlmodel";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  OwlmodelPackage eINSTANCE = org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl.init();

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.OWLModelImpl <em>OWL Model</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.OWLModelImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getOWLModel()
   * @generated
   */
  int OWL_MODEL = 0;

  /**
   * The feature id for the '<em><b>Header</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_MODEL__HEADER = 0;

  /**
   * The feature id for the '<em><b>Doc Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_MODEL__DOC_TYPE = 1;

  /**
   * The feature id for the '<em><b>Rdf</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_MODEL__RDF = 2;

  /**
   * The number of structural features of the '<em>OWL Model</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_MODEL_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.DocTypeImpl <em>Doc Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.DocTypeImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getDocType()
   * @generated
   */
  int DOC_TYPE = 1;

  /**
   * The feature id for the '<em><b>Doc ID</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOC_TYPE__DOC_ID = 0;

  /**
   * The feature id for the '<em><b>Entities</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOC_TYPE__ENTITIES = 1;

  /**
   * The number of structural features of the '<em>Doc Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOC_TYPE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.DocEntityImpl <em>Doc Entity</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.DocEntityImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getDocEntity()
   * @generated
   */
  int DOC_ENTITY = 2;

  /**
   * The feature id for the '<em><b>Entity ID</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOC_ENTITY__ENTITY_ID = 0;

  /**
   * The feature id for the '<em><b>Entity URI</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOC_ENTITY__ENTITY_URI = 1;

  /**
   * The number of structural features of the '<em>Doc Entity</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOC_ENTITY_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.RDFElementImpl <em>RDF Element</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.RDFElementImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getRDFElement()
   * @generated
   */
  int RDF_ELEMENT = 3;

  /**
   * The feature id for the '<em><b>Imported Ns</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RDF_ELEMENT__IMPORTED_NS = 0;

  /**
   * The feature id for the '<em><b>Base Ont</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RDF_ELEMENT__BASE_ONT = 1;

  /**
   * The feature id for the '<em><b>Ontology Elements</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RDF_ELEMENT__ONTOLOGY_ELEMENTS = 2;

  /**
   * The number of structural features of the '<em>RDF Element</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RDF_ELEMENT_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.XmlNsImpl <em>Xml Ns</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.XmlNsImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getXmlNs()
   * @generated
   */
  int XML_NS = 4;

  /**
   * The feature id for the '<em><b>Nsid</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int XML_NS__NSID = 0;

  /**
   * The feature id for the '<em><b>Nsuri</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int XML_NS__NSURI = 1;

  /**
   * The feature id for the '<em><b>Nsname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int XML_NS__NSNAME = 2;

  /**
   * The number of structural features of the '<em>Xml Ns</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int XML_NS_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.OwlRefImpl <em>Owl Ref</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlRefImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getOwlRef()
   * @generated
   */
  int OWL_REF = 5;

  /**
   * The feature id for the '<em><b>Ont URI</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_REF__ONT_URI = 0;

  /**
   * The feature id for the '<em><b>Ont Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_REF__ONT_NAME = 1;

  /**
   * The feature id for the '<em><b>Elem ID</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_REF__ELEM_ID = 2;

  /**
   * The feature id for the '<em><b>Full URI</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_REF__FULL_URI = 3;

  /**
   * The number of structural features of the '<em>Owl Ref</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_REF_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.OwlIDImpl <em>Owl ID</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlIDImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getOwlID()
   * @generated
   */
  int OWL_ID = 6;

  /**
   * The feature id for the '<em><b>Elem ID</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_ID__ELEM_ID = 0;

  /**
   * The number of structural features of the '<em>Owl ID</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_ID_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.OntologyIDImpl <em>Ontology ID</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.OntologyIDImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getOntologyID()
   * @generated
   */
  int ONTOLOGY_ID = 7;

  /**
   * The feature id for the '<em><b>Super Ont</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ONTOLOGY_ID__SUPER_ONT = 0;

  /**
   * The feature id for the '<em><b>Ont URI</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ONTOLOGY_ID__ONT_URI = 1;

  /**
   * The number of structural features of the '<em>Ontology ID</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ONTOLOGY_ID_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.ParseTypeImpl <em>Parse Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.ParseTypeImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getParseType()
   * @generated
   */
  int PARSE_TYPE = 8;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARSE_TYPE__TYPE = 0;

  /**
   * The number of structural features of the '<em>Parse Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARSE_TYPE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.OntologyElementImpl <em>Ontology Element</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.OntologyElementImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getOntologyElement()
   * @generated
   */
  int ONTOLOGY_ELEMENT = 9;

  /**
   * The number of structural features of the '<em>Ontology Element</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ONTOLOGY_ELEMENT_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.NamedIndividualImpl <em>Named Individual</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.NamedIndividualImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getNamedIndividual()
   * @generated
   */
  int NAMED_INDIVIDUAL = 10;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAMED_INDIVIDUAL__REF = ONTOLOGY_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Types</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAMED_INDIVIDUAL__TYPES = ONTOLOGY_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAMED_INDIVIDUAL__DESC = ONTOLOGY_ELEMENT_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Named Individual</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAMED_INDIVIDUAL_FEATURE_COUNT = ONTOLOGY_ELEMENT_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.OntologyImpl <em>Ontology</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.OntologyImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getOntology()
   * @generated
   */
  int ONTOLOGY = 11;

  /**
   * The feature id for the '<em><b>About</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ONTOLOGY__ABOUT = ONTOLOGY_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Resource</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ONTOLOGY__RESOURCE = ONTOLOGY_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Labels</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ONTOLOGY__LABELS = ONTOLOGY_ELEMENT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Version Info</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ONTOLOGY__VERSION_INFO = ONTOLOGY_ELEMENT_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>See Also</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ONTOLOGY__SEE_ALSO = ONTOLOGY_ELEMENT_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>PVers</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ONTOLOGY__PVERS = ONTOLOGY_ELEMENT_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Back Comp</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ONTOLOGY__BACK_COMP = ONTOLOGY_ELEMENT_FEATURE_COUNT + 6;

  /**
   * The feature id for the '<em><b>Imported Ontologies</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ONTOLOGY__IMPORTED_ONTOLOGIES = ONTOLOGY_ELEMENT_FEATURE_COUNT + 7;

  /**
   * The feature id for the '<em><b>Others</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ONTOLOGY__OTHERS = ONTOLOGY_ELEMENT_FEATURE_COUNT + 8;

  /**
   * The number of structural features of the '<em>Ontology</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ONTOLOGY_FEATURE_COUNT = ONTOLOGY_ELEMENT_FEATURE_COUNT + 9;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.SeeAlsoImpl <em>See Also</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.SeeAlsoImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getSeeAlso()
   * @generated
   */
  int SEE_ALSO = 12;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEE_ALSO__REF = 0;

  /**
   * The number of structural features of the '<em>See Also</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEE_ALSO_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.PriorVersionImpl <em>Prior Version</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.PriorVersionImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getPriorVersion()
   * @generated
   */
  int PRIOR_VERSION = 13;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRIOR_VERSION__REF = 0;

  /**
   * The number of structural features of the '<em>Prior Version</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRIOR_VERSION_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.BackwardsCompImpl <em>Backwards Comp</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.BackwardsCompImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getBackwardsComp()
   * @generated
   */
  int BACKWARDS_COMP = 14;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BACKWARDS_COMP__REF = 0;

  /**
   * The number of structural features of the '<em>Backwards Comp</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BACKWARDS_COMP_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.OwlImportImpl <em>Owl Import</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlImportImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getOwlImport()
   * @generated
   */
  int OWL_IMPORT = 15;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_IMPORT__REF = 0;

  /**
   * The feature id for the '<em><b>Onts</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_IMPORT__ONTS = 1;

  /**
   * The number of structural features of the '<em>Owl Import</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_IMPORT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.OwlClassImpl <em>Owl Class</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlClassImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getOwlClass()
   * @generated
   */
  int OWL_CLASS = 16;

  /**
   * The feature id for the '<em><b>Id</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_CLASS__ID = ONTOLOGY_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Reference</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_CLASS__REFERENCE = ONTOLOGY_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_CLASS__REF = ONTOLOGY_ELEMENT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Labels</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_CLASS__LABELS = ONTOLOGY_ELEMENT_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Version Info</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_CLASS__VERSION_INFO = ONTOLOGY_ELEMENT_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Super Types</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_CLASS__SUPER_TYPES = ONTOLOGY_ELEMENT_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Axioms</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_CLASS__AXIOMS = ONTOLOGY_ELEMENT_FEATURE_COUNT + 6;

  /**
   * The feature id for the '<em><b>Onts</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_CLASS__ONTS = ONTOLOGY_ELEMENT_FEATURE_COUNT + 7;

  /**
   * The feature id for the '<em><b>Others</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_CLASS__OTHERS = ONTOLOGY_ELEMENT_FEATURE_COUNT + 8;

  /**
   * The number of structural features of the '<em>Owl Class</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_CLASS_FEATURE_COUNT = ONTOLOGY_ELEMENT_FEATURE_COUNT + 9;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.ClassModifierImpl <em>Class Modifier</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.ClassModifierImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getClassModifier()
   * @generated
   */
  int CLASS_MODIFIER = 29;

  /**
   * The number of structural features of the '<em>Class Modifier</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLASS_MODIFIER_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.MinimumClassImpl <em>Minimum Class</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.MinimumClassImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getMinimumClass()
   * @generated
   */
  int MINIMUM_CLASS = 17;

  /**
   * The feature id for the '<em><b>Labels</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MINIMUM_CLASS__LABELS = CLASS_MODIFIER_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Axioms</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MINIMUM_CLASS__AXIOMS = CLASS_MODIFIER_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Others</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MINIMUM_CLASS__OTHERS = CLASS_MODIFIER_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Minimum Class</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MINIMUM_CLASS_FEATURE_COUNT = CLASS_MODIFIER_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.ClassAxiomImpl <em>Class Axiom</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.ClassAxiomImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getClassAxiom()
   * @generated
   */
  int CLASS_AXIOM = 18;

  /**
   * The feature id for the '<em><b>Parse Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLASS_AXIOM__PARSE_TYPE = 0;

  /**
   * The number of structural features of the '<em>Class Axiom</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLASS_AXIOM_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.ComplementAxiomImpl <em>Complement Axiom</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.ComplementAxiomImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getComplementAxiom()
   * @generated
   */
  int COMPLEMENT_AXIOM = 19;

  /**
   * The feature id for the '<em><b>Parse Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPLEMENT_AXIOM__PARSE_TYPE = CLASS_AXIOM__PARSE_TYPE;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPLEMENT_AXIOM__REF = CLASS_AXIOM_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Classes</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPLEMENT_AXIOM__CLASSES = CLASS_AXIOM_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Complement Axiom</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPLEMENT_AXIOM_FEATURE_COUNT = CLASS_AXIOM_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.IncompatibleWithAxiomImpl <em>Incompatible With Axiom</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.IncompatibleWithAxiomImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getIncompatibleWithAxiom()
   * @generated
   */
  int INCOMPATIBLE_WITH_AXIOM = 20;

  /**
   * The feature id for the '<em><b>Parse Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INCOMPATIBLE_WITH_AXIOM__PARSE_TYPE = CLASS_AXIOM__PARSE_TYPE;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INCOMPATIBLE_WITH_AXIOM__REF = CLASS_AXIOM_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Classes</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INCOMPATIBLE_WITH_AXIOM__CLASSES = CLASS_AXIOM_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Incompatible With Axiom</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INCOMPATIBLE_WITH_AXIOM_FEATURE_COUNT = CLASS_AXIOM_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.DisjointAxiomImpl <em>Disjoint Axiom</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.DisjointAxiomImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getDisjointAxiom()
   * @generated
   */
  int DISJOINT_AXIOM = 21;

  /**
   * The feature id for the '<em><b>Parse Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DISJOINT_AXIOM__PARSE_TYPE = CLASS_AXIOM__PARSE_TYPE;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DISJOINT_AXIOM__REF = CLASS_AXIOM_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Classes</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DISJOINT_AXIOM__CLASSES = CLASS_AXIOM_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Disjoint Axiom</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DISJOINT_AXIOM_FEATURE_COUNT = CLASS_AXIOM_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.EquivalentAxiomImpl <em>Equivalent Axiom</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.EquivalentAxiomImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getEquivalentAxiom()
   * @generated
   */
  int EQUIVALENT_AXIOM = 22;

  /**
   * The feature id for the '<em><b>Parse Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQUIVALENT_AXIOM__PARSE_TYPE = CLASS_AXIOM__PARSE_TYPE;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQUIVALENT_AXIOM__REF = CLASS_AXIOM_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Classes</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQUIVALENT_AXIOM__CLASSES = CLASS_AXIOM_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Equivalent Axiom</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQUIVALENT_AXIOM_FEATURE_COUNT = CLASS_AXIOM_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.UnionAxiomImpl <em>Union Axiom</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.UnionAxiomImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getUnionAxiom()
   * @generated
   */
  int UNION_AXIOM = 23;

  /**
   * The feature id for the '<em><b>Parse Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNION_AXIOM__PARSE_TYPE = CLASS_AXIOM__PARSE_TYPE;

  /**
   * The feature id for the '<em><b>Classes</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNION_AXIOM__CLASSES = CLASS_AXIOM_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Union Axiom</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNION_AXIOM_FEATURE_COUNT = CLASS_AXIOM_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.IntersectionAxiomImpl <em>Intersection Axiom</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.IntersectionAxiomImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getIntersectionAxiom()
   * @generated
   */
  int INTERSECTION_AXIOM = 24;

  /**
   * The feature id for the '<em><b>Parse Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTERSECTION_AXIOM__PARSE_TYPE = CLASS_AXIOM__PARSE_TYPE;

  /**
   * The feature id for the '<em><b>Classes</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTERSECTION_AXIOM__CLASSES = CLASS_AXIOM_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Intersection Axiom</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTERSECTION_AXIOM_FEATURE_COUNT = CLASS_AXIOM_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.OneOfAxiomImpl <em>One Of Axiom</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.OneOfAxiomImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getOneOfAxiom()
   * @generated
   */
  int ONE_OF_AXIOM = 25;

  /**
   * The feature id for the '<em><b>Parse Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ONE_OF_AXIOM__PARSE_TYPE = CLASS_AXIOM__PARSE_TYPE;

  /**
   * The feature id for the '<em><b>Values</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ONE_OF_AXIOM__VALUES = CLASS_AXIOM_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Tag Names</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ONE_OF_AXIOM__TAG_NAMES = CLASS_AXIOM_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Tagids</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ONE_OF_AXIOM__TAGIDS = CLASS_AXIOM_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>First</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ONE_OF_AXIOM__FIRST = CLASS_AXIOM_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Rest</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ONE_OF_AXIOM__REST = CLASS_AXIOM_FEATURE_COUNT + 4;

  /**
   * The number of structural features of the '<em>One Of Axiom</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ONE_OF_AXIOM_FEATURE_COUNT = CLASS_AXIOM_FEATURE_COUNT + 5;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.OwlLabelImpl <em>Owl Label</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlLabelImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getOwlLabel()
   * @generated
   */
  int OWL_LABEL = 26;

  /**
   * The feature id for the '<em><b>Lang</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_LABEL__LANG = 0;

  /**
   * The feature id for the '<em><b>Datatype</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_LABEL__DATATYPE = 1;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_LABEL__VALUE = 2;

  /**
   * The number of structural features of the '<em>Owl Label</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_LABEL_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.OwlVersionImpl <em>Owl Version</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlVersionImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getOwlVersion()
   * @generated
   */
  int OWL_VERSION = 27;

  /**
   * The feature id for the '<em><b>Lang</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_VERSION__LANG = 0;

  /**
   * The feature id for the '<em><b>Datatype</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_VERSION__DATATYPE = 1;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_VERSION__VALUE = 2;

  /**
   * The number of structural features of the '<em>Owl Version</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_VERSION_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.SubTypeOfImpl <em>Sub Type Of</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.SubTypeOfImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getSubTypeOf()
   * @generated
   */
  int SUB_TYPE_OF = 28;

  /**
   * The feature id for the '<em><b>Resource Based</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUB_TYPE_OF__RESOURCE_BASED = 0;

  /**
   * The feature id for the '<em><b>Class Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUB_TYPE_OF__CLASS_REF = 1;

  /**
   * The feature id for the '<em><b>Modifiers</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUB_TYPE_OF__MODIFIERS = 2;

  /**
   * The number of structural features of the '<em>Sub Type Of</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUB_TYPE_OF_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.OwlRestrictionImpl <em>Owl Restriction</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlRestrictionImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getOwlRestriction()
   * @generated
   */
  int OWL_RESTRICTION = 30;

  /**
   * The feature id for the '<em><b>Restriction Bodies</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_RESTRICTION__RESTRICTION_BODIES = CLASS_MODIFIER_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Prop</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_RESTRICTION__PROP = CLASS_MODIFIER_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Property</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_RESTRICTION__PROPERTY = CLASS_MODIFIER_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Class</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_RESTRICTION__CLASS = CLASS_MODIFIER_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Classes</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_RESTRICTION__CLASSES = CLASS_MODIFIER_FEATURE_COUNT + 4;

  /**
   * The number of structural features of the '<em>Owl Restriction</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_RESTRICTION_FEATURE_COUNT = CLASS_MODIFIER_FEATURE_COUNT + 5;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.RestrictionBodyImpl <em>Restriction Body</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.RestrictionBodyImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getRestrictionBody()
   * @generated
   */
  int RESTRICTION_BODY = 31;

  /**
   * The number of structural features of the '<em>Restriction Body</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RESTRICTION_BODY_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.MinCardinalityImpl <em>Min Cardinality</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.MinCardinalityImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getMinCardinality()
   * @generated
   */
  int MIN_CARDINALITY = 32;

  /**
   * The feature id for the '<em><b>Datatype</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MIN_CARDINALITY__DATATYPE = RESTRICTION_BODY_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MIN_CARDINALITY__VALUE = RESTRICTION_BODY_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Min Cardinality</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MIN_CARDINALITY_FEATURE_COUNT = RESTRICTION_BODY_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.MaxCardinalityImpl <em>Max Cardinality</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.MaxCardinalityImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getMaxCardinality()
   * @generated
   */
  int MAX_CARDINALITY = 33;

  /**
   * The feature id for the '<em><b>Datatype</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAX_CARDINALITY__DATATYPE = RESTRICTION_BODY_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAX_CARDINALITY__VALUE = RESTRICTION_BODY_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Max Cardinality</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAX_CARDINALITY_FEATURE_COUNT = RESTRICTION_BODY_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.CardinalityImpl <em>Cardinality</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.CardinalityImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getCardinality()
   * @generated
   */
  int CARDINALITY = 34;

  /**
   * The feature id for the '<em><b>Datatype</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CARDINALITY__DATATYPE = RESTRICTION_BODY_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CARDINALITY__VALUE = RESTRICTION_BODY_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Cardinality</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CARDINALITY_FEATURE_COUNT = RESTRICTION_BODY_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.QualifiedCardinalityImpl <em>Qualified Cardinality</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.QualifiedCardinalityImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getQualifiedCardinality()
   * @generated
   */
  int QUALIFIED_CARDINALITY = 35;

  /**
   * The feature id for the '<em><b>Datatype</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUALIFIED_CARDINALITY__DATATYPE = RESTRICTION_BODY_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUALIFIED_CARDINALITY__VALUE = RESTRICTION_BODY_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Qualified Cardinality</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUALIFIED_CARDINALITY_FEATURE_COUNT = RESTRICTION_BODY_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.MinQualifiedCardinalityImpl <em>Min Qualified Cardinality</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.MinQualifiedCardinalityImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getMinQualifiedCardinality()
   * @generated
   */
  int MIN_QUALIFIED_CARDINALITY = 36;

  /**
   * The feature id for the '<em><b>Datatype</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MIN_QUALIFIED_CARDINALITY__DATATYPE = RESTRICTION_BODY_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MIN_QUALIFIED_CARDINALITY__VALUE = RESTRICTION_BODY_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Min Qualified Cardinality</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MIN_QUALIFIED_CARDINALITY_FEATURE_COUNT = RESTRICTION_BODY_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.MaxQualifiedCardinalityImpl <em>Max Qualified Cardinality</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.MaxQualifiedCardinalityImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getMaxQualifiedCardinality()
   * @generated
   */
  int MAX_QUALIFIED_CARDINALITY = 37;

  /**
   * The feature id for the '<em><b>Datatype</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAX_QUALIFIED_CARDINALITY__DATATYPE = RESTRICTION_BODY_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAX_QUALIFIED_CARDINALITY__VALUE = RESTRICTION_BODY_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Max Qualified Cardinality</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAX_QUALIFIED_CARDINALITY_FEATURE_COUNT = RESTRICTION_BODY_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.SomeValuesImpl <em>Some Values</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.SomeValuesImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getSomeValues()
   * @generated
   */
  int SOME_VALUES = 38;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOME_VALUES__REF = RESTRICTION_BODY_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Modifiers</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOME_VALUES__MODIFIERS = RESTRICTION_BODY_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Some Values</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOME_VALUES_FEATURE_COUNT = RESTRICTION_BODY_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.AllValuesImpl <em>All Values</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.AllValuesImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getAllValues()
   * @generated
   */
  int ALL_VALUES = 39;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALL_VALUES__REF = RESTRICTION_BODY_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Modifiers</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALL_VALUES__MODIFIERS = RESTRICTION_BODY_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>All Values</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALL_VALUES_FEATURE_COUNT = RESTRICTION_BODY_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.HasValueImpl <em>Has Value</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.HasValueImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getHasValue()
   * @generated
   */
  int HAS_VALUE = 40;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HAS_VALUE__REF = RESTRICTION_BODY_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HAS_VALUE__TYPE = RESTRICTION_BODY_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HAS_VALUE__VALUE = RESTRICTION_BODY_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Has Value</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HAS_VALUE_FEATURE_COUNT = RESTRICTION_BODY_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.PropertyImpl <em>Property</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.PropertyImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getProperty()
   * @generated
   */
  int PROPERTY = 41;

  /**
   * The feature id for the '<em><b>Id</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY__ID = ONTOLOGY_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Reference</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY__REFERENCE = ONTOLOGY_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY__REF = ONTOLOGY_ELEMENT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Others</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY__OTHERS = ONTOLOGY_ELEMENT_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Labels</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY__LABELS = ONTOLOGY_ELEMENT_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Version Info</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY__VERSION_INFO = ONTOLOGY_ELEMENT_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Ranges</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY__RANGES = ONTOLOGY_ELEMENT_FEATURE_COUNT + 6;

  /**
   * The feature id for the '<em><b>Types</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY__TYPES = ONTOLOGY_ELEMENT_FEATURE_COUNT + 7;

  /**
   * The feature id for the '<em><b>Domains</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY__DOMAINS = ONTOLOGY_ELEMENT_FEATURE_COUNT + 8;

  /**
   * The feature id for the '<em><b>Modifiers</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY__MODIFIERS = ONTOLOGY_ELEMENT_FEATURE_COUNT + 9;

  /**
   * The number of structural features of the '<em>Property</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY_FEATURE_COUNT = ONTOLOGY_ELEMENT_FEATURE_COUNT + 10;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.AnnotationPropertyImpl <em>Annotation Property</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.AnnotationPropertyImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getAnnotationProperty()
   * @generated
   */
  int ANNOTATION_PROPERTY = 42;

  /**
   * The feature id for the '<em><b>Id</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATION_PROPERTY__ID = PROPERTY__ID;

  /**
   * The feature id for the '<em><b>Reference</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATION_PROPERTY__REFERENCE = PROPERTY__REFERENCE;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATION_PROPERTY__REF = PROPERTY__REF;

  /**
   * The feature id for the '<em><b>Others</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATION_PROPERTY__OTHERS = PROPERTY__OTHERS;

  /**
   * The feature id for the '<em><b>Labels</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATION_PROPERTY__LABELS = PROPERTY__LABELS;

  /**
   * The feature id for the '<em><b>Version Info</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATION_PROPERTY__VERSION_INFO = PROPERTY__VERSION_INFO;

  /**
   * The feature id for the '<em><b>Ranges</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATION_PROPERTY__RANGES = PROPERTY__RANGES;

  /**
   * The feature id for the '<em><b>Types</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATION_PROPERTY__TYPES = PROPERTY__TYPES;

  /**
   * The feature id for the '<em><b>Domains</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATION_PROPERTY__DOMAINS = PROPERTY__DOMAINS;

  /**
   * The feature id for the '<em><b>Modifiers</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATION_PROPERTY__MODIFIERS = PROPERTY__MODIFIERS;

  /**
   * The number of structural features of the '<em>Annotation Property</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATION_PROPERTY_FEATURE_COUNT = PROPERTY_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.ObjectPropertyImpl <em>Object Property</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.ObjectPropertyImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getObjectProperty()
   * @generated
   */
  int OBJECT_PROPERTY = 43;

  /**
   * The feature id for the '<em><b>Id</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBJECT_PROPERTY__ID = PROPERTY__ID;

  /**
   * The feature id for the '<em><b>Reference</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBJECT_PROPERTY__REFERENCE = PROPERTY__REFERENCE;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBJECT_PROPERTY__REF = PROPERTY__REF;

  /**
   * The feature id for the '<em><b>Others</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBJECT_PROPERTY__OTHERS = PROPERTY__OTHERS;

  /**
   * The feature id for the '<em><b>Labels</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBJECT_PROPERTY__LABELS = PROPERTY__LABELS;

  /**
   * The feature id for the '<em><b>Version Info</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBJECT_PROPERTY__VERSION_INFO = PROPERTY__VERSION_INFO;

  /**
   * The feature id for the '<em><b>Ranges</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBJECT_PROPERTY__RANGES = PROPERTY__RANGES;

  /**
   * The feature id for the '<em><b>Types</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBJECT_PROPERTY__TYPES = PROPERTY__TYPES;

  /**
   * The feature id for the '<em><b>Domains</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBJECT_PROPERTY__DOMAINS = PROPERTY__DOMAINS;

  /**
   * The feature id for the '<em><b>Modifiers</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBJECT_PROPERTY__MODIFIERS = PROPERTY__MODIFIERS;

  /**
   * The number of structural features of the '<em>Object Property</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBJECT_PROPERTY_FEATURE_COUNT = PROPERTY_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.DatatypePropertyImpl <em>Datatype Property</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.DatatypePropertyImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getDatatypeProperty()
   * @generated
   */
  int DATATYPE_PROPERTY = 44;

  /**
   * The feature id for the '<em><b>Id</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATATYPE_PROPERTY__ID = PROPERTY__ID;

  /**
   * The feature id for the '<em><b>Reference</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATATYPE_PROPERTY__REFERENCE = PROPERTY__REFERENCE;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATATYPE_PROPERTY__REF = PROPERTY__REF;

  /**
   * The feature id for the '<em><b>Others</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATATYPE_PROPERTY__OTHERS = PROPERTY__OTHERS;

  /**
   * The feature id for the '<em><b>Labels</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATATYPE_PROPERTY__LABELS = PROPERTY__LABELS;

  /**
   * The feature id for the '<em><b>Version Info</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATATYPE_PROPERTY__VERSION_INFO = PROPERTY__VERSION_INFO;

  /**
   * The feature id for the '<em><b>Ranges</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATATYPE_PROPERTY__RANGES = PROPERTY__RANGES;

  /**
   * The feature id for the '<em><b>Types</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATATYPE_PROPERTY__TYPES = PROPERTY__TYPES;

  /**
   * The feature id for the '<em><b>Domains</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATATYPE_PROPERTY__DOMAINS = PROPERTY__DOMAINS;

  /**
   * The feature id for the '<em><b>Modifiers</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATATYPE_PROPERTY__MODIFIERS = PROPERTY__MODIFIERS;

  /**
   * The number of structural features of the '<em>Datatype Property</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATATYPE_PROPERTY_FEATURE_COUNT = PROPERTY_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.FunctionalPropertyImpl <em>Functional Property</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.FunctionalPropertyImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getFunctionalProperty()
   * @generated
   */
  int FUNCTIONAL_PROPERTY = 45;

  /**
   * The feature id for the '<em><b>Id</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTIONAL_PROPERTY__ID = PROPERTY__ID;

  /**
   * The feature id for the '<em><b>Reference</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTIONAL_PROPERTY__REFERENCE = PROPERTY__REFERENCE;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTIONAL_PROPERTY__REF = PROPERTY__REF;

  /**
   * The feature id for the '<em><b>Others</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTIONAL_PROPERTY__OTHERS = PROPERTY__OTHERS;

  /**
   * The feature id for the '<em><b>Labels</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTIONAL_PROPERTY__LABELS = PROPERTY__LABELS;

  /**
   * The feature id for the '<em><b>Version Info</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTIONAL_PROPERTY__VERSION_INFO = PROPERTY__VERSION_INFO;

  /**
   * The feature id for the '<em><b>Ranges</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTIONAL_PROPERTY__RANGES = PROPERTY__RANGES;

  /**
   * The feature id for the '<em><b>Types</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTIONAL_PROPERTY__TYPES = PROPERTY__TYPES;

  /**
   * The feature id for the '<em><b>Domains</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTIONAL_PROPERTY__DOMAINS = PROPERTY__DOMAINS;

  /**
   * The feature id for the '<em><b>Modifiers</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTIONAL_PROPERTY__MODIFIERS = PROPERTY__MODIFIERS;

  /**
   * The number of structural features of the '<em>Functional Property</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTIONAL_PROPERTY_FEATURE_COUNT = PROPERTY_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.InverseFunctionalPropertyImpl <em>Inverse Functional Property</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.InverseFunctionalPropertyImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getInverseFunctionalProperty()
   * @generated
   */
  int INVERSE_FUNCTIONAL_PROPERTY = 46;

  /**
   * The feature id for the '<em><b>Id</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INVERSE_FUNCTIONAL_PROPERTY__ID = PROPERTY__ID;

  /**
   * The feature id for the '<em><b>Reference</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INVERSE_FUNCTIONAL_PROPERTY__REFERENCE = PROPERTY__REFERENCE;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INVERSE_FUNCTIONAL_PROPERTY__REF = PROPERTY__REF;

  /**
   * The feature id for the '<em><b>Others</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INVERSE_FUNCTIONAL_PROPERTY__OTHERS = PROPERTY__OTHERS;

  /**
   * The feature id for the '<em><b>Labels</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INVERSE_FUNCTIONAL_PROPERTY__LABELS = PROPERTY__LABELS;

  /**
   * The feature id for the '<em><b>Version Info</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INVERSE_FUNCTIONAL_PROPERTY__VERSION_INFO = PROPERTY__VERSION_INFO;

  /**
   * The feature id for the '<em><b>Ranges</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INVERSE_FUNCTIONAL_PROPERTY__RANGES = PROPERTY__RANGES;

  /**
   * The feature id for the '<em><b>Types</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INVERSE_FUNCTIONAL_PROPERTY__TYPES = PROPERTY__TYPES;

  /**
   * The feature id for the '<em><b>Domains</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INVERSE_FUNCTIONAL_PROPERTY__DOMAINS = PROPERTY__DOMAINS;

  /**
   * The feature id for the '<em><b>Modifiers</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INVERSE_FUNCTIONAL_PROPERTY__MODIFIERS = PROPERTY__MODIFIERS;

  /**
   * The number of structural features of the '<em>Inverse Functional Property</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INVERSE_FUNCTIONAL_PROPERTY_FEATURE_COUNT = PROPERTY_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.SymmetricPropertyImpl <em>Symmetric Property</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.SymmetricPropertyImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getSymmetricProperty()
   * @generated
   */
  int SYMMETRIC_PROPERTY = 47;

  /**
   * The feature id for the '<em><b>Id</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYMMETRIC_PROPERTY__ID = PROPERTY__ID;

  /**
   * The feature id for the '<em><b>Reference</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYMMETRIC_PROPERTY__REFERENCE = PROPERTY__REFERENCE;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYMMETRIC_PROPERTY__REF = PROPERTY__REF;

  /**
   * The feature id for the '<em><b>Others</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYMMETRIC_PROPERTY__OTHERS = PROPERTY__OTHERS;

  /**
   * The feature id for the '<em><b>Labels</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYMMETRIC_PROPERTY__LABELS = PROPERTY__LABELS;

  /**
   * The feature id for the '<em><b>Version Info</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYMMETRIC_PROPERTY__VERSION_INFO = PROPERTY__VERSION_INFO;

  /**
   * The feature id for the '<em><b>Ranges</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYMMETRIC_PROPERTY__RANGES = PROPERTY__RANGES;

  /**
   * The feature id for the '<em><b>Types</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYMMETRIC_PROPERTY__TYPES = PROPERTY__TYPES;

  /**
   * The feature id for the '<em><b>Domains</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYMMETRIC_PROPERTY__DOMAINS = PROPERTY__DOMAINS;

  /**
   * The feature id for the '<em><b>Modifiers</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYMMETRIC_PROPERTY__MODIFIERS = PROPERTY__MODIFIERS;

  /**
   * The number of structural features of the '<em>Symmetric Property</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYMMETRIC_PROPERTY_FEATURE_COUNT = PROPERTY_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.TransitivePropertyImpl <em>Transitive Property</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.TransitivePropertyImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getTransitiveProperty()
   * @generated
   */
  int TRANSITIVE_PROPERTY = 48;

  /**
   * The feature id for the '<em><b>Id</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRANSITIVE_PROPERTY__ID = PROPERTY__ID;

  /**
   * The feature id for the '<em><b>Reference</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRANSITIVE_PROPERTY__REFERENCE = PROPERTY__REFERENCE;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRANSITIVE_PROPERTY__REF = PROPERTY__REF;

  /**
   * The feature id for the '<em><b>Others</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRANSITIVE_PROPERTY__OTHERS = PROPERTY__OTHERS;

  /**
   * The feature id for the '<em><b>Labels</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRANSITIVE_PROPERTY__LABELS = PROPERTY__LABELS;

  /**
   * The feature id for the '<em><b>Version Info</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRANSITIVE_PROPERTY__VERSION_INFO = PROPERTY__VERSION_INFO;

  /**
   * The feature id for the '<em><b>Ranges</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRANSITIVE_PROPERTY__RANGES = PROPERTY__RANGES;

  /**
   * The feature id for the '<em><b>Types</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRANSITIVE_PROPERTY__TYPES = PROPERTY__TYPES;

  /**
   * The feature id for the '<em><b>Domains</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRANSITIVE_PROPERTY__DOMAINS = PROPERTY__DOMAINS;

  /**
   * The feature id for the '<em><b>Modifiers</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRANSITIVE_PROPERTY__MODIFIERS = PROPERTY__MODIFIERS;

  /**
   * The number of structural features of the '<em>Transitive Property</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRANSITIVE_PROPERTY_FEATURE_COUNT = PROPERTY_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.PropertyModifierImpl <em>Property Modifier</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.PropertyModifierImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getPropertyModifier()
   * @generated
   */
  int PROPERTY_MODIFIER = 49;

  /**
   * The feature id for the '<em><b>Props</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY_MODIFIER__PROPS = 0;

  /**
   * The number of structural features of the '<em>Property Modifier</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY_MODIFIER_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.InverseTypeImpl <em>Inverse Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.InverseTypeImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getInverseType()
   * @generated
   */
  int INVERSE_TYPE = 50;

  /**
   * The feature id for the '<em><b>Props</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INVERSE_TYPE__PROPS = PROPERTY_MODIFIER__PROPS;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INVERSE_TYPE__REF = PROPERTY_MODIFIER_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Inverse Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INVERSE_TYPE_FEATURE_COUNT = PROPERTY_MODIFIER_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.EquivalentTypeImpl <em>Equivalent Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.EquivalentTypeImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getEquivalentType()
   * @generated
   */
  int EQUIVALENT_TYPE = 51;

  /**
   * The feature id for the '<em><b>Props</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQUIVALENT_TYPE__PROPS = PROPERTY_MODIFIER__PROPS;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQUIVALENT_TYPE__REF = PROPERTY_MODIFIER_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Equivalent Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQUIVALENT_TYPE_FEATURE_COUNT = PROPERTY_MODIFIER_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.RDFSDomainImpl <em>RDFS Domain</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.RDFSDomainImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getRDFSDomain()
   * @generated
   */
  int RDFS_DOMAIN = 52;

  /**
   * The number of structural features of the '<em>RDFS Domain</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RDFS_DOMAIN_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.RDFSRangeImpl <em>RDFS Range</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.RDFSRangeImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getRDFSRange()
   * @generated
   */
  int RDFS_RANGE = 53;

  /**
   * The number of structural features of the '<em>RDFS Range</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RDFS_RANGE_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.OwlDataRangeImpl <em>Owl Data Range</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlDataRangeImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getOwlDataRange()
   * @generated
   */
  int OWL_DATA_RANGE = 54;

  /**
   * The feature id for the '<em><b>Owl Range</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_DATA_RANGE__OWL_RANGE = RDFS_RANGE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Union</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_DATA_RANGE__UNION = RDFS_RANGE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>One Of</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_DATA_RANGE__ONE_OF = RDFS_RANGE_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Intersection</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_DATA_RANGE__INTERSECTION = RDFS_RANGE_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>Owl Data Range</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_DATA_RANGE_FEATURE_COUNT = RDFS_RANGE_FEATURE_COUNT + 4;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.RDFTypeImpl <em>RDF Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.RDFTypeImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getRDFType()
   * @generated
   */
  int RDF_TYPE = 55;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RDF_TYPE__TYPE = 0;

  /**
   * The number of structural features of the '<em>RDF Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RDF_TYPE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.SubPropertyOfImpl <em>Sub Property Of</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.SubPropertyOfImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getSubPropertyOf()
   * @generated
   */
  int SUB_PROPERTY_OF = 56;

  /**
   * The feature id for the '<em><b>Props</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUB_PROPERTY_OF__PROPS = PROPERTY_MODIFIER__PROPS;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUB_PROPERTY_OF__TYPE = PROPERTY_MODIFIER_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Sub Property Of</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUB_PROPERTY_OF_FEATURE_COUNT = PROPERTY_MODIFIER_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.DescriptionImpl <em>Description</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.DescriptionImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getDescription()
   * @generated
   */
  int DESCRIPTION = 57;

  /**
   * The feature id for the '<em><b>About</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DESCRIPTION__ABOUT = ONTOLOGY_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>First</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DESCRIPTION__FIRST = ONTOLOGY_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DESCRIPTION__TYPE = ONTOLOGY_ELEMENT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Domain</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DESCRIPTION__DOMAIN = ONTOLOGY_ELEMENT_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Range</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DESCRIPTION__RANGE = ONTOLOGY_ELEMENT_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Prop Modifiers</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DESCRIPTION__PROP_MODIFIERS = ONTOLOGY_ELEMENT_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Labels</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DESCRIPTION__LABELS = ONTOLOGY_ELEMENT_FEATURE_COUNT + 6;

  /**
   * The feature id for the '<em><b>Axioms</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DESCRIPTION__AXIOMS = ONTOLOGY_ELEMENT_FEATURE_COUNT + 7;

  /**
   * The number of structural features of the '<em>Description</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DESCRIPTION_FEATURE_COUNT = ONTOLOGY_ELEMENT_FEATURE_COUNT + 8;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.OwlFirstImpl <em>Owl First</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlFirstImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getOwlFirst()
   * @generated
   */
  int OWL_FIRST = 58;

  /**
   * The feature id for the '<em><b>Ref Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_FIRST__REF_TYPE = 0;

  /**
   * The feature id for the '<em><b>Data Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_FIRST__DATA_TYPE = 1;

  /**
   * The feature id for the '<em><b>Data Type Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_FIRST__DATA_TYPE_VALUE = 2;

  /**
   * The number of structural features of the '<em>Owl First</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_FIRST_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.RDFRestImpl <em>RDF Rest</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.RDFRestImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getRDFRest()
   * @generated
   */
  int RDF_REST = 59;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RDF_REST__REF = 0;

  /**
   * The feature id for the '<em><b>Parse Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RDF_REST__PARSE_TYPE = 1;

  /**
   * The feature id for the '<em><b>First</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RDF_REST__FIRST = 2;

  /**
   * The feature id for the '<em><b>Rest</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RDF_REST__REST = 3;

  /**
   * The number of structural features of the '<em>RDF Rest</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RDF_REST_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.OwlTypeImpl <em>Owl Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlTypeImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getOwlType()
   * @generated
   */
  int OWL_TYPE = 60;

  /**
   * The feature id for the '<em><b>Ref Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_TYPE__REF_TYPE = 0;

  /**
   * The feature id for the '<em><b>Members</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_TYPE__MEMBERS = 1;

  /**
   * The number of structural features of the '<em>Owl Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_TYPE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.OwlMembersImpl <em>Owl Members</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlMembersImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getOwlMembers()
   * @generated
   */
  int OWL_MEMBERS = 61;

  /**
   * The feature id for the '<em><b>Parse Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_MEMBERS__PARSE_TYPE = 0;

  /**
   * The feature id for the '<em><b>Descriptions</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_MEMBERS__DESCRIPTIONS = 1;

  /**
   * The number of structural features of the '<em>Owl Members</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OWL_MEMBERS_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.DatatypeImpl <em>Datatype</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.DatatypeImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getDatatype()
   * @generated
   */
  int DATATYPE = 62;

  /**
   * The feature id for the '<em><b>Id</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATATYPE__ID = ONTOLOGY_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Reference</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATATYPE__REFERENCE = ONTOLOGY_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATATYPE__REF = ONTOLOGY_ELEMENT_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Datatype</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATATYPE_FEATURE_COUNT = ONTOLOGY_ELEMENT_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.SimpleRDFSDomainImpl <em>Simple RDFS Domain</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.SimpleRDFSDomainImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getSimpleRDFSDomain()
   * @generated
   */
  int SIMPLE_RDFS_DOMAIN = 63;

  /**
   * The feature id for the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_RDFS_DOMAIN__REF = RDFS_DOMAIN_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Simple RDFS Domain</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_RDFS_DOMAIN_FEATURE_COUNT = RDFS_DOMAIN_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.ParsedRDFSDomainImpl <em>Parsed RDFS Domain</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.ParsedRDFSDomainImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getParsedRDFSDomain()
   * @generated
   */
  int PARSED_RDFS_DOMAIN = 64;

  /**
   * The feature id for the '<em><b>Parse Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARSED_RDFS_DOMAIN__PARSE_TYPE = RDFS_DOMAIN_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Modifiers</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARSED_RDFS_DOMAIN__MODIFIERS = RDFS_DOMAIN_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Parsed RDFS Domain</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARSED_RDFS_DOMAIN_FEATURE_COUNT = RDFS_DOMAIN_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.SimpleRDFSRangeImpl <em>Simple RDFS Range</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.SimpleRDFSRangeImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getSimpleRDFSRange()
   * @generated
   */
  int SIMPLE_RDFS_RANGE = 65;

  /**
   * The feature id for the '<em><b>Range</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_RDFS_RANGE__RANGE = RDFS_RANGE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Simple RDFS Range</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_RDFS_RANGE_FEATURE_COUNT = RDFS_RANGE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.smool.sdk.owlparser.owlmodel.impl.ParsedPropertyRangeImpl <em>Parsed Property Range</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.smool.sdk.owlparser.owlmodel.impl.ParsedPropertyRangeImpl
   * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getParsedPropertyRange()
   * @generated
   */
  int PARSED_PROPERTY_RANGE = 66;

  /**
   * The feature id for the '<em><b>Parse Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARSED_PROPERTY_RANGE__PARSE_TYPE = RDFS_RANGE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Modifiers</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARSED_PROPERTY_RANGE__MODIFIERS = RDFS_RANGE_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Parsed Property Range</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARSED_PROPERTY_RANGE_FEATURE_COUNT = RDFS_RANGE_FEATURE_COUNT + 2;


  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.OWLModel <em>OWL Model</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>OWL Model</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OWLModel
   * @generated
   */
  EClass getOWLModel();

  /**
   * Returns the meta object for the attribute '{@link org.smool.sdk.owlparser.owlmodel.OWLModel#getHeader <em>Header</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Header</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OWLModel#getHeader()
   * @see #getOWLModel()
   * @generated
   */
  EAttribute getOWLModel_Header();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.OWLModel#getDocType <em>Doc Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Doc Type</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OWLModel#getDocType()
   * @see #getOWLModel()
   * @generated
   */
  EReference getOWLModel_DocType();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.OWLModel#getRdf <em>Rdf</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Rdf</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OWLModel#getRdf()
   * @see #getOWLModel()
   * @generated
   */
  EReference getOWLModel_Rdf();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.DocType <em>Doc Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Doc Type</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.DocType
   * @generated
   */
  EClass getDocType();

  /**
   * Returns the meta object for the attribute '{@link org.smool.sdk.owlparser.owlmodel.DocType#getDocID <em>Doc ID</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Doc ID</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.DocType#getDocID()
   * @see #getDocType()
   * @generated
   */
  EAttribute getDocType_DocID();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.DocType#getEntities <em>Entities</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Entities</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.DocType#getEntities()
   * @see #getDocType()
   * @generated
   */
  EReference getDocType_Entities();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.DocEntity <em>Doc Entity</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Doc Entity</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.DocEntity
   * @generated
   */
  EClass getDocEntity();

  /**
   * Returns the meta object for the attribute '{@link org.smool.sdk.owlparser.owlmodel.DocEntity#getEntityID <em>Entity ID</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Entity ID</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.DocEntity#getEntityID()
   * @see #getDocEntity()
   * @generated
   */
  EAttribute getDocEntity_EntityID();

  /**
   * Returns the meta object for the attribute '{@link org.smool.sdk.owlparser.owlmodel.DocEntity#getEntityURI <em>Entity URI</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Entity URI</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.DocEntity#getEntityURI()
   * @see #getDocEntity()
   * @generated
   */
  EAttribute getDocEntity_EntityURI();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.RDFElement <em>RDF Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>RDF Element</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.RDFElement
   * @generated
   */
  EClass getRDFElement();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.RDFElement#getImportedNs <em>Imported Ns</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Imported Ns</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.RDFElement#getImportedNs()
   * @see #getRDFElement()
   * @generated
   */
  EReference getRDFElement_ImportedNs();

  /**
   * Returns the meta object for the attribute '{@link org.smool.sdk.owlparser.owlmodel.RDFElement#getBaseOnt <em>Base Ont</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Base Ont</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.RDFElement#getBaseOnt()
   * @see #getRDFElement()
   * @generated
   */
  EAttribute getRDFElement_BaseOnt();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.RDFElement#getOntologyElements <em>Ontology Elements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Ontology Elements</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.RDFElement#getOntologyElements()
   * @see #getRDFElement()
   * @generated
   */
  EReference getRDFElement_OntologyElements();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.XmlNs <em>Xml Ns</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Xml Ns</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.XmlNs
   * @generated
   */
  EClass getXmlNs();

  /**
   * Returns the meta object for the attribute '{@link org.smool.sdk.owlparser.owlmodel.XmlNs#getNsid <em>Nsid</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Nsid</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.XmlNs#getNsid()
   * @see #getXmlNs()
   * @generated
   */
  EAttribute getXmlNs_Nsid();

  /**
   * Returns the meta object for the attribute '{@link org.smool.sdk.owlparser.owlmodel.XmlNs#getNsuri <em>Nsuri</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Nsuri</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.XmlNs#getNsuri()
   * @see #getXmlNs()
   * @generated
   */
  EAttribute getXmlNs_Nsuri();

  /**
   * Returns the meta object for the attribute '{@link org.smool.sdk.owlparser.owlmodel.XmlNs#getNsname <em>Nsname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Nsname</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.XmlNs#getNsname()
   * @see #getXmlNs()
   * @generated
   */
  EAttribute getXmlNs_Nsname();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.OwlRef <em>Owl Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Owl Ref</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlRef
   * @generated
   */
  EClass getOwlRef();

  /**
   * Returns the meta object for the attribute '{@link org.smool.sdk.owlparser.owlmodel.OwlRef#getOntURI <em>Ont URI</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ont URI</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlRef#getOntURI()
   * @see #getOwlRef()
   * @generated
   */
  EAttribute getOwlRef_OntURI();

  /**
   * Returns the meta object for the attribute '{@link org.smool.sdk.owlparser.owlmodel.OwlRef#getOntName <em>Ont Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ont Name</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlRef#getOntName()
   * @see #getOwlRef()
   * @generated
   */
  EAttribute getOwlRef_OntName();

  /**
   * Returns the meta object for the attribute '{@link org.smool.sdk.owlparser.owlmodel.OwlRef#getElemID <em>Elem ID</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Elem ID</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlRef#getElemID()
   * @see #getOwlRef()
   * @generated
   */
  EAttribute getOwlRef_ElemID();

  /**
   * Returns the meta object for the attribute '{@link org.smool.sdk.owlparser.owlmodel.OwlRef#getFullURI <em>Full URI</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Full URI</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlRef#getFullURI()
   * @see #getOwlRef()
   * @generated
   */
  EAttribute getOwlRef_FullURI();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.OwlID <em>Owl ID</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Owl ID</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlID
   * @generated
   */
  EClass getOwlID();

  /**
   * Returns the meta object for the attribute '{@link org.smool.sdk.owlparser.owlmodel.OwlID#getElemID <em>Elem ID</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Elem ID</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlID#getElemID()
   * @see #getOwlID()
   * @generated
   */
  EAttribute getOwlID_ElemID();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.OntologyID <em>Ontology ID</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Ontology ID</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OntologyID
   * @generated
   */
  EClass getOntologyID();

  /**
   * Returns the meta object for the attribute '{@link org.smool.sdk.owlparser.owlmodel.OntologyID#getSuperOnt <em>Super Ont</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Super Ont</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OntologyID#getSuperOnt()
   * @see #getOntologyID()
   * @generated
   */
  EAttribute getOntologyID_SuperOnt();

  /**
   * Returns the meta object for the attribute '{@link org.smool.sdk.owlparser.owlmodel.OntologyID#getOntURI <em>Ont URI</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ont URI</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OntologyID#getOntURI()
   * @see #getOntologyID()
   * @generated
   */
  EAttribute getOntologyID_OntURI();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.ParseType <em>Parse Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Parse Type</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.ParseType
   * @generated
   */
  EClass getParseType();

  /**
   * Returns the meta object for the attribute '{@link org.smool.sdk.owlparser.owlmodel.ParseType#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.ParseType#getType()
   * @see #getParseType()
   * @generated
   */
  EAttribute getParseType_Type();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.OntologyElement <em>Ontology Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Ontology Element</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OntologyElement
   * @generated
   */
  EClass getOntologyElement();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.NamedIndividual <em>Named Individual</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Named Individual</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.NamedIndividual
   * @generated
   */
  EClass getNamedIndividual();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.NamedIndividual#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ref</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.NamedIndividual#getRef()
   * @see #getNamedIndividual()
   * @generated
   */
  EReference getNamedIndividual_Ref();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.NamedIndividual#getTypes <em>Types</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Types</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.NamedIndividual#getTypes()
   * @see #getNamedIndividual()
   * @generated
   */
  EReference getNamedIndividual_Types();

  /**
   * Returns the meta object for the attribute '{@link org.smool.sdk.owlparser.owlmodel.NamedIndividual#getDesc <em>Desc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Desc</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.NamedIndividual#getDesc()
   * @see #getNamedIndividual()
   * @generated
   */
  EAttribute getNamedIndividual_Desc();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.Ontology <em>Ontology</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Ontology</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Ontology
   * @generated
   */
  EClass getOntology();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.Ontology#getAbout <em>About</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>About</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Ontology#getAbout()
   * @see #getOntology()
   * @generated
   */
  EReference getOntology_About();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.Ontology#getResource <em>Resource</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Resource</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Ontology#getResource()
   * @see #getOntology()
   * @generated
   */
  EReference getOntology_Resource();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.Ontology#getLabels <em>Labels</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Labels</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Ontology#getLabels()
   * @see #getOntology()
   * @generated
   */
  EReference getOntology_Labels();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.Ontology#getVersionInfo <em>Version Info</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Version Info</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Ontology#getVersionInfo()
   * @see #getOntology()
   * @generated
   */
  EReference getOntology_VersionInfo();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.Ontology#getSeeAlso <em>See Also</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>See Also</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Ontology#getSeeAlso()
   * @see #getOntology()
   * @generated
   */
  EReference getOntology_SeeAlso();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.Ontology#getPVers <em>PVers</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>PVers</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Ontology#getPVers()
   * @see #getOntology()
   * @generated
   */
  EReference getOntology_PVers();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.Ontology#getBackComp <em>Back Comp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Back Comp</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Ontology#getBackComp()
   * @see #getOntology()
   * @generated
   */
  EReference getOntology_BackComp();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.Ontology#getImportedOntologies <em>Imported Ontologies</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Imported Ontologies</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Ontology#getImportedOntologies()
   * @see #getOntology()
   * @generated
   */
  EReference getOntology_ImportedOntologies();

  /**
   * Returns the meta object for the attribute list '{@link org.smool.sdk.owlparser.owlmodel.Ontology#getOthers <em>Others</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Others</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Ontology#getOthers()
   * @see #getOntology()
   * @generated
   */
  EAttribute getOntology_Others();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.SeeAlso <em>See Also</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>See Also</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.SeeAlso
   * @generated
   */
  EClass getSeeAlso();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.SeeAlso#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ref</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.SeeAlso#getRef()
   * @see #getSeeAlso()
   * @generated
   */
  EReference getSeeAlso_Ref();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.PriorVersion <em>Prior Version</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Prior Version</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.PriorVersion
   * @generated
   */
  EClass getPriorVersion();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.PriorVersion#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ref</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.PriorVersion#getRef()
   * @see #getPriorVersion()
   * @generated
   */
  EReference getPriorVersion_Ref();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.BackwardsComp <em>Backwards Comp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Backwards Comp</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.BackwardsComp
   * @generated
   */
  EClass getBackwardsComp();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.BackwardsComp#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ref</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.BackwardsComp#getRef()
   * @see #getBackwardsComp()
   * @generated
   */
  EReference getBackwardsComp_Ref();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.OwlImport <em>Owl Import</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Owl Import</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlImport
   * @generated
   */
  EClass getOwlImport();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.OwlImport#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ref</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlImport#getRef()
   * @see #getOwlImport()
   * @generated
   */
  EReference getOwlImport_Ref();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.OwlImport#getOnts <em>Onts</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Onts</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlImport#getOnts()
   * @see #getOwlImport()
   * @generated
   */
  EReference getOwlImport_Onts();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.OwlClass <em>Owl Class</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Owl Class</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlClass
   * @generated
   */
  EClass getOwlClass();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.OwlClass#getId <em>Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Id</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlClass#getId()
   * @see #getOwlClass()
   * @generated
   */
  EReference getOwlClass_Id();

  /**
   * Returns the meta object for the attribute '{@link org.smool.sdk.owlparser.owlmodel.OwlClass#isReference <em>Reference</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Reference</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlClass#isReference()
   * @see #getOwlClass()
   * @generated
   */
  EAttribute getOwlClass_Reference();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.OwlClass#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ref</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlClass#getRef()
   * @see #getOwlClass()
   * @generated
   */
  EReference getOwlClass_Ref();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.OwlClass#getLabels <em>Labels</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Labels</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlClass#getLabels()
   * @see #getOwlClass()
   * @generated
   */
  EReference getOwlClass_Labels();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.OwlClass#getVersionInfo <em>Version Info</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Version Info</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlClass#getVersionInfo()
   * @see #getOwlClass()
   * @generated
   */
  EReference getOwlClass_VersionInfo();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.OwlClass#getSuperTypes <em>Super Types</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Super Types</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlClass#getSuperTypes()
   * @see #getOwlClass()
   * @generated
   */
  EReference getOwlClass_SuperTypes();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.OwlClass#getAxioms <em>Axioms</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Axioms</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlClass#getAxioms()
   * @see #getOwlClass()
   * @generated
   */
  EReference getOwlClass_Axioms();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.OwlClass#getOnts <em>Onts</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Onts</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlClass#getOnts()
   * @see #getOwlClass()
   * @generated
   */
  EReference getOwlClass_Onts();

  /**
   * Returns the meta object for the attribute list '{@link org.smool.sdk.owlparser.owlmodel.OwlClass#getOthers <em>Others</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Others</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlClass#getOthers()
   * @see #getOwlClass()
   * @generated
   */
  EAttribute getOwlClass_Others();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.MinimumClass <em>Minimum Class</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Minimum Class</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.MinimumClass
   * @generated
   */
  EClass getMinimumClass();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.MinimumClass#getLabels <em>Labels</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Labels</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.MinimumClass#getLabels()
   * @see #getMinimumClass()
   * @generated
   */
  EReference getMinimumClass_Labels();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.MinimumClass#getAxioms <em>Axioms</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Axioms</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.MinimumClass#getAxioms()
   * @see #getMinimumClass()
   * @generated
   */
  EReference getMinimumClass_Axioms();

  /**
   * Returns the meta object for the attribute list '{@link org.smool.sdk.owlparser.owlmodel.MinimumClass#getOthers <em>Others</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Others</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.MinimumClass#getOthers()
   * @see #getMinimumClass()
   * @generated
   */
  EAttribute getMinimumClass_Others();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.ClassAxiom <em>Class Axiom</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Class Axiom</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.ClassAxiom
   * @generated
   */
  EClass getClassAxiom();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.ClassAxiom#getParseType <em>Parse Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Parse Type</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.ClassAxiom#getParseType()
   * @see #getClassAxiom()
   * @generated
   */
  EReference getClassAxiom_ParseType();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.ComplementAxiom <em>Complement Axiom</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Complement Axiom</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.ComplementAxiom
   * @generated
   */
  EClass getComplementAxiom();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.ComplementAxiom#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ref</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.ComplementAxiom#getRef()
   * @see #getComplementAxiom()
   * @generated
   */
  EReference getComplementAxiom_Ref();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.ComplementAxiom#getClasses <em>Classes</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Classes</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.ComplementAxiom#getClasses()
   * @see #getComplementAxiom()
   * @generated
   */
  EReference getComplementAxiom_Classes();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.IncompatibleWithAxiom <em>Incompatible With Axiom</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Incompatible With Axiom</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.IncompatibleWithAxiom
   * @generated
   */
  EClass getIncompatibleWithAxiom();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.IncompatibleWithAxiom#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ref</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.IncompatibleWithAxiom#getRef()
   * @see #getIncompatibleWithAxiom()
   * @generated
   */
  EReference getIncompatibleWithAxiom_Ref();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.IncompatibleWithAxiom#getClasses <em>Classes</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Classes</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.IncompatibleWithAxiom#getClasses()
   * @see #getIncompatibleWithAxiom()
   * @generated
   */
  EReference getIncompatibleWithAxiom_Classes();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.DisjointAxiom <em>Disjoint Axiom</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Disjoint Axiom</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.DisjointAxiom
   * @generated
   */
  EClass getDisjointAxiom();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.DisjointAxiom#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ref</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.DisjointAxiom#getRef()
   * @see #getDisjointAxiom()
   * @generated
   */
  EReference getDisjointAxiom_Ref();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.DisjointAxiom#getClasses <em>Classes</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Classes</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.DisjointAxiom#getClasses()
   * @see #getDisjointAxiom()
   * @generated
   */
  EReference getDisjointAxiom_Classes();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.EquivalentAxiom <em>Equivalent Axiom</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Equivalent Axiom</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.EquivalentAxiom
   * @generated
   */
  EClass getEquivalentAxiom();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.EquivalentAxiom#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ref</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.EquivalentAxiom#getRef()
   * @see #getEquivalentAxiom()
   * @generated
   */
  EReference getEquivalentAxiom_Ref();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.EquivalentAxiom#getClasses <em>Classes</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Classes</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.EquivalentAxiom#getClasses()
   * @see #getEquivalentAxiom()
   * @generated
   */
  EReference getEquivalentAxiom_Classes();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.UnionAxiom <em>Union Axiom</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Union Axiom</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.UnionAxiom
   * @generated
   */
  EClass getUnionAxiom();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.UnionAxiom#getClasses <em>Classes</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Classes</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.UnionAxiom#getClasses()
   * @see #getUnionAxiom()
   * @generated
   */
  EReference getUnionAxiom_Classes();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.IntersectionAxiom <em>Intersection Axiom</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Intersection Axiom</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.IntersectionAxiom
   * @generated
   */
  EClass getIntersectionAxiom();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.IntersectionAxiom#getClasses <em>Classes</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Classes</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.IntersectionAxiom#getClasses()
   * @see #getIntersectionAxiom()
   * @generated
   */
  EReference getIntersectionAxiom_Classes();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.OneOfAxiom <em>One Of Axiom</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>One Of Axiom</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OneOfAxiom
   * @generated
   */
  EClass getOneOfAxiom();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.OneOfAxiom#getValues <em>Values</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Values</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OneOfAxiom#getValues()
   * @see #getOneOfAxiom()
   * @generated
   */
  EReference getOneOfAxiom_Values();

  /**
   * Returns the meta object for the attribute list '{@link org.smool.sdk.owlparser.owlmodel.OneOfAxiom#getTagNames <em>Tag Names</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Tag Names</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OneOfAxiom#getTagNames()
   * @see #getOneOfAxiom()
   * @generated
   */
  EAttribute getOneOfAxiom_TagNames();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.OneOfAxiom#getTagids <em>Tagids</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Tagids</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OneOfAxiom#getTagids()
   * @see #getOneOfAxiom()
   * @generated
   */
  EReference getOneOfAxiom_Tagids();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.OneOfAxiom#getFirst <em>First</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>First</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OneOfAxiom#getFirst()
   * @see #getOneOfAxiom()
   * @generated
   */
  EReference getOneOfAxiom_First();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.OneOfAxiom#getRest <em>Rest</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Rest</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OneOfAxiom#getRest()
   * @see #getOneOfAxiom()
   * @generated
   */
  EReference getOneOfAxiom_Rest();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.OwlLabel <em>Owl Label</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Owl Label</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlLabel
   * @generated
   */
  EClass getOwlLabel();

  /**
   * Returns the meta object for the attribute '{@link org.smool.sdk.owlparser.owlmodel.OwlLabel#getLang <em>Lang</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Lang</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlLabel#getLang()
   * @see #getOwlLabel()
   * @generated
   */
  EAttribute getOwlLabel_Lang();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.OwlLabel#getDatatype <em>Datatype</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Datatype</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlLabel#getDatatype()
   * @see #getOwlLabel()
   * @generated
   */
  EReference getOwlLabel_Datatype();

  /**
   * Returns the meta object for the attribute '{@link org.smool.sdk.owlparser.owlmodel.OwlLabel#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlLabel#getValue()
   * @see #getOwlLabel()
   * @generated
   */
  EAttribute getOwlLabel_Value();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.OwlVersion <em>Owl Version</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Owl Version</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlVersion
   * @generated
   */
  EClass getOwlVersion();

  /**
   * Returns the meta object for the attribute '{@link org.smool.sdk.owlparser.owlmodel.OwlVersion#getLang <em>Lang</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Lang</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlVersion#getLang()
   * @see #getOwlVersion()
   * @generated
   */
  EAttribute getOwlVersion_Lang();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.OwlVersion#getDatatype <em>Datatype</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Datatype</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlVersion#getDatatype()
   * @see #getOwlVersion()
   * @generated
   */
  EReference getOwlVersion_Datatype();

  /**
   * Returns the meta object for the attribute '{@link org.smool.sdk.owlparser.owlmodel.OwlVersion#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlVersion#getValue()
   * @see #getOwlVersion()
   * @generated
   */
  EAttribute getOwlVersion_Value();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.SubTypeOf <em>Sub Type Of</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Sub Type Of</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.SubTypeOf
   * @generated
   */
  EClass getSubTypeOf();

  /**
   * Returns the meta object for the attribute '{@link org.smool.sdk.owlparser.owlmodel.SubTypeOf#isResourceBased <em>Resource Based</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Resource Based</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.SubTypeOf#isResourceBased()
   * @see #getSubTypeOf()
   * @generated
   */
  EAttribute getSubTypeOf_ResourceBased();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.SubTypeOf#getClassRef <em>Class Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Class Ref</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.SubTypeOf#getClassRef()
   * @see #getSubTypeOf()
   * @generated
   */
  EReference getSubTypeOf_ClassRef();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.SubTypeOf#getModifiers <em>Modifiers</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Modifiers</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.SubTypeOf#getModifiers()
   * @see #getSubTypeOf()
   * @generated
   */
  EReference getSubTypeOf_Modifiers();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.ClassModifier <em>Class Modifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Class Modifier</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.ClassModifier
   * @generated
   */
  EClass getClassModifier();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.OwlRestriction <em>Owl Restriction</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Owl Restriction</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlRestriction
   * @generated
   */
  EClass getOwlRestriction();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.OwlRestriction#getRestrictionBodies <em>Restriction Bodies</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Restriction Bodies</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlRestriction#getRestrictionBodies()
   * @see #getOwlRestriction()
   * @generated
   */
  EReference getOwlRestriction_RestrictionBodies();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.OwlRestriction#getProp <em>Prop</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Prop</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlRestriction#getProp()
   * @see #getOwlRestriction()
   * @generated
   */
  EReference getOwlRestriction_Prop();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.OwlRestriction#getProperty <em>Property</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Property</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlRestriction#getProperty()
   * @see #getOwlRestriction()
   * @generated
   */
  EReference getOwlRestriction_Property();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.OwlRestriction#getClass_ <em>Class</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Class</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlRestriction#getClass_()
   * @see #getOwlRestriction()
   * @generated
   */
  EReference getOwlRestriction_Class();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.OwlRestriction#getClasses <em>Classes</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Classes</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlRestriction#getClasses()
   * @see #getOwlRestriction()
   * @generated
   */
  EReference getOwlRestriction_Classes();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.RestrictionBody <em>Restriction Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Restriction Body</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.RestrictionBody
   * @generated
   */
  EClass getRestrictionBody();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.MinCardinality <em>Min Cardinality</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Min Cardinality</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.MinCardinality
   * @generated
   */
  EClass getMinCardinality();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.MinCardinality#getDatatype <em>Datatype</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Datatype</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.MinCardinality#getDatatype()
   * @see #getMinCardinality()
   * @generated
   */
  EReference getMinCardinality_Datatype();

  /**
   * Returns the meta object for the attribute '{@link org.smool.sdk.owlparser.owlmodel.MinCardinality#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.MinCardinality#getValue()
   * @see #getMinCardinality()
   * @generated
   */
  EAttribute getMinCardinality_Value();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.MaxCardinality <em>Max Cardinality</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Max Cardinality</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.MaxCardinality
   * @generated
   */
  EClass getMaxCardinality();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.MaxCardinality#getDatatype <em>Datatype</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Datatype</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.MaxCardinality#getDatatype()
   * @see #getMaxCardinality()
   * @generated
   */
  EReference getMaxCardinality_Datatype();

  /**
   * Returns the meta object for the attribute '{@link org.smool.sdk.owlparser.owlmodel.MaxCardinality#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.MaxCardinality#getValue()
   * @see #getMaxCardinality()
   * @generated
   */
  EAttribute getMaxCardinality_Value();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.Cardinality <em>Cardinality</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Cardinality</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Cardinality
   * @generated
   */
  EClass getCardinality();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.Cardinality#getDatatype <em>Datatype</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Datatype</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Cardinality#getDatatype()
   * @see #getCardinality()
   * @generated
   */
  EReference getCardinality_Datatype();

  /**
   * Returns the meta object for the attribute '{@link org.smool.sdk.owlparser.owlmodel.Cardinality#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Cardinality#getValue()
   * @see #getCardinality()
   * @generated
   */
  EAttribute getCardinality_Value();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.QualifiedCardinality <em>Qualified Cardinality</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Qualified Cardinality</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.QualifiedCardinality
   * @generated
   */
  EClass getQualifiedCardinality();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.QualifiedCardinality#getDatatype <em>Datatype</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Datatype</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.QualifiedCardinality#getDatatype()
   * @see #getQualifiedCardinality()
   * @generated
   */
  EReference getQualifiedCardinality_Datatype();

  /**
   * Returns the meta object for the attribute '{@link org.smool.sdk.owlparser.owlmodel.QualifiedCardinality#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.QualifiedCardinality#getValue()
   * @see #getQualifiedCardinality()
   * @generated
   */
  EAttribute getQualifiedCardinality_Value();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.MinQualifiedCardinality <em>Min Qualified Cardinality</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Min Qualified Cardinality</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.MinQualifiedCardinality
   * @generated
   */
  EClass getMinQualifiedCardinality();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.MinQualifiedCardinality#getDatatype <em>Datatype</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Datatype</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.MinQualifiedCardinality#getDatatype()
   * @see #getMinQualifiedCardinality()
   * @generated
   */
  EReference getMinQualifiedCardinality_Datatype();

  /**
   * Returns the meta object for the attribute '{@link org.smool.sdk.owlparser.owlmodel.MinQualifiedCardinality#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.MinQualifiedCardinality#getValue()
   * @see #getMinQualifiedCardinality()
   * @generated
   */
  EAttribute getMinQualifiedCardinality_Value();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.MaxQualifiedCardinality <em>Max Qualified Cardinality</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Max Qualified Cardinality</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.MaxQualifiedCardinality
   * @generated
   */
  EClass getMaxQualifiedCardinality();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.MaxQualifiedCardinality#getDatatype <em>Datatype</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Datatype</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.MaxQualifiedCardinality#getDatatype()
   * @see #getMaxQualifiedCardinality()
   * @generated
   */
  EReference getMaxQualifiedCardinality_Datatype();

  /**
   * Returns the meta object for the attribute '{@link org.smool.sdk.owlparser.owlmodel.MaxQualifiedCardinality#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.MaxQualifiedCardinality#getValue()
   * @see #getMaxQualifiedCardinality()
   * @generated
   */
  EAttribute getMaxQualifiedCardinality_Value();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.SomeValues <em>Some Values</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Some Values</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.SomeValues
   * @generated
   */
  EClass getSomeValues();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.SomeValues#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ref</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.SomeValues#getRef()
   * @see #getSomeValues()
   * @generated
   */
  EReference getSomeValues_Ref();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.SomeValues#getModifiers <em>Modifiers</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Modifiers</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.SomeValues#getModifiers()
   * @see #getSomeValues()
   * @generated
   */
  EReference getSomeValues_Modifiers();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.AllValues <em>All Values</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>All Values</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.AllValues
   * @generated
   */
  EClass getAllValues();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.AllValues#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ref</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.AllValues#getRef()
   * @see #getAllValues()
   * @generated
   */
  EReference getAllValues_Ref();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.AllValues#getModifiers <em>Modifiers</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Modifiers</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.AllValues#getModifiers()
   * @see #getAllValues()
   * @generated
   */
  EReference getAllValues_Modifiers();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.HasValue <em>Has Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Has Value</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.HasValue
   * @generated
   */
  EClass getHasValue();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.HasValue#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ref</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.HasValue#getRef()
   * @see #getHasValue()
   * @generated
   */
  EReference getHasValue_Ref();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.HasValue#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.HasValue#getType()
   * @see #getHasValue()
   * @generated
   */
  EReference getHasValue_Type();

  /**
   * Returns the meta object for the attribute '{@link org.smool.sdk.owlparser.owlmodel.HasValue#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.HasValue#getValue()
   * @see #getHasValue()
   * @generated
   */
  EAttribute getHasValue_Value();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.Property <em>Property</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Property</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Property
   * @generated
   */
  EClass getProperty();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.Property#getId <em>Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Id</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Property#getId()
   * @see #getProperty()
   * @generated
   */
  EReference getProperty_Id();

  /**
   * Returns the meta object for the attribute '{@link org.smool.sdk.owlparser.owlmodel.Property#isReference <em>Reference</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Reference</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Property#isReference()
   * @see #getProperty()
   * @generated
   */
  EAttribute getProperty_Reference();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.Property#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ref</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Property#getRef()
   * @see #getProperty()
   * @generated
   */
  EReference getProperty_Ref();

  /**
   * Returns the meta object for the attribute list '{@link org.smool.sdk.owlparser.owlmodel.Property#getOthers <em>Others</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Others</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Property#getOthers()
   * @see #getProperty()
   * @generated
   */
  EAttribute getProperty_Others();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.Property#getLabels <em>Labels</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Labels</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Property#getLabels()
   * @see #getProperty()
   * @generated
   */
  EReference getProperty_Labels();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.Property#getVersionInfo <em>Version Info</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Version Info</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Property#getVersionInfo()
   * @see #getProperty()
   * @generated
   */
  EReference getProperty_VersionInfo();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.Property#getRanges <em>Ranges</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Ranges</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Property#getRanges()
   * @see #getProperty()
   * @generated
   */
  EReference getProperty_Ranges();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.Property#getTypes <em>Types</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Types</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Property#getTypes()
   * @see #getProperty()
   * @generated
   */
  EReference getProperty_Types();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.Property#getDomains <em>Domains</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Domains</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Property#getDomains()
   * @see #getProperty()
   * @generated
   */
  EReference getProperty_Domains();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.Property#getModifiers <em>Modifiers</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Modifiers</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Property#getModifiers()
   * @see #getProperty()
   * @generated
   */
  EReference getProperty_Modifiers();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.AnnotationProperty <em>Annotation Property</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Annotation Property</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.AnnotationProperty
   * @generated
   */
  EClass getAnnotationProperty();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.ObjectProperty <em>Object Property</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Object Property</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.ObjectProperty
   * @generated
   */
  EClass getObjectProperty();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.DatatypeProperty <em>Datatype Property</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Datatype Property</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.DatatypeProperty
   * @generated
   */
  EClass getDatatypeProperty();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.FunctionalProperty <em>Functional Property</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Functional Property</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.FunctionalProperty
   * @generated
   */
  EClass getFunctionalProperty();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.InverseFunctionalProperty <em>Inverse Functional Property</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Inverse Functional Property</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.InverseFunctionalProperty
   * @generated
   */
  EClass getInverseFunctionalProperty();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.SymmetricProperty <em>Symmetric Property</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Symmetric Property</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.SymmetricProperty
   * @generated
   */
  EClass getSymmetricProperty();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.TransitiveProperty <em>Transitive Property</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Transitive Property</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.TransitiveProperty
   * @generated
   */
  EClass getTransitiveProperty();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.PropertyModifier <em>Property Modifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Property Modifier</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.PropertyModifier
   * @generated
   */
  EClass getPropertyModifier();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.PropertyModifier#getProps <em>Props</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Props</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.PropertyModifier#getProps()
   * @see #getPropertyModifier()
   * @generated
   */
  EReference getPropertyModifier_Props();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.InverseType <em>Inverse Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Inverse Type</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.InverseType
   * @generated
   */
  EClass getInverseType();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.InverseType#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ref</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.InverseType#getRef()
   * @see #getInverseType()
   * @generated
   */
  EReference getInverseType_Ref();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.EquivalentType <em>Equivalent Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Equivalent Type</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.EquivalentType
   * @generated
   */
  EClass getEquivalentType();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.EquivalentType#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ref</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.EquivalentType#getRef()
   * @see #getEquivalentType()
   * @generated
   */
  EReference getEquivalentType_Ref();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.RDFSDomain <em>RDFS Domain</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>RDFS Domain</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.RDFSDomain
   * @generated
   */
  EClass getRDFSDomain();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.RDFSRange <em>RDFS Range</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>RDFS Range</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.RDFSRange
   * @generated
   */
  EClass getRDFSRange();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.OwlDataRange <em>Owl Data Range</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Owl Data Range</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlDataRange
   * @generated
   */
  EClass getOwlDataRange();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.OwlDataRange#getOwlRange <em>Owl Range</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Owl Range</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlDataRange#getOwlRange()
   * @see #getOwlDataRange()
   * @generated
   */
  EReference getOwlDataRange_OwlRange();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.OwlDataRange#getUnion <em>Union</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Union</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlDataRange#getUnion()
   * @see #getOwlDataRange()
   * @generated
   */
  EReference getOwlDataRange_Union();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.OwlDataRange#getOneOf <em>One Of</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>One Of</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlDataRange#getOneOf()
   * @see #getOwlDataRange()
   * @generated
   */
  EReference getOwlDataRange_OneOf();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.OwlDataRange#getIntersection <em>Intersection</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Intersection</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlDataRange#getIntersection()
   * @see #getOwlDataRange()
   * @generated
   */
  EReference getOwlDataRange_Intersection();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.RDFType <em>RDF Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>RDF Type</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.RDFType
   * @generated
   */
  EClass getRDFType();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.RDFType#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.RDFType#getType()
   * @see #getRDFType()
   * @generated
   */
  EReference getRDFType_Type();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.SubPropertyOf <em>Sub Property Of</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Sub Property Of</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.SubPropertyOf
   * @generated
   */
  EClass getSubPropertyOf();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.SubPropertyOf#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.SubPropertyOf#getType()
   * @see #getSubPropertyOf()
   * @generated
   */
  EReference getSubPropertyOf_Type();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.Description <em>Description</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Description</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Description
   * @generated
   */
  EClass getDescription();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.Description#getAbout <em>About</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>About</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Description#getAbout()
   * @see #getDescription()
   * @generated
   */
  EReference getDescription_About();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.Description#getFirst <em>First</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>First</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Description#getFirst()
   * @see #getDescription()
   * @generated
   */
  EReference getDescription_First();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.Description#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Type</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Description#getType()
   * @see #getDescription()
   * @generated
   */
  EReference getDescription_Type();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.Description#getDomain <em>Domain</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Domain</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Description#getDomain()
   * @see #getDescription()
   * @generated
   */
  EReference getDescription_Domain();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.Description#getRange <em>Range</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Range</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Description#getRange()
   * @see #getDescription()
   * @generated
   */
  EReference getDescription_Range();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.Description#getPropModifiers <em>Prop Modifiers</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Prop Modifiers</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Description#getPropModifiers()
   * @see #getDescription()
   * @generated
   */
  EReference getDescription_PropModifiers();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.Description#getLabels <em>Labels</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Labels</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Description#getLabels()
   * @see #getDescription()
   * @generated
   */
  EReference getDescription_Labels();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.Description#getAxioms <em>Axioms</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Axioms</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Description#getAxioms()
   * @see #getDescription()
   * @generated
   */
  EReference getDescription_Axioms();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.OwlFirst <em>Owl First</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Owl First</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlFirst
   * @generated
   */
  EClass getOwlFirst();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.OwlFirst#getRefType <em>Ref Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ref Type</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlFirst#getRefType()
   * @see #getOwlFirst()
   * @generated
   */
  EReference getOwlFirst_RefType();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.OwlFirst#getDataType <em>Data Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Data Type</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlFirst#getDataType()
   * @see #getOwlFirst()
   * @generated
   */
  EReference getOwlFirst_DataType();

  /**
   * Returns the meta object for the attribute '{@link org.smool.sdk.owlparser.owlmodel.OwlFirst#getDataTypeValue <em>Data Type Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Data Type Value</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlFirst#getDataTypeValue()
   * @see #getOwlFirst()
   * @generated
   */
  EAttribute getOwlFirst_DataTypeValue();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.RDFRest <em>RDF Rest</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>RDF Rest</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.RDFRest
   * @generated
   */
  EClass getRDFRest();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.RDFRest#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ref</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.RDFRest#getRef()
   * @see #getRDFRest()
   * @generated
   */
  EReference getRDFRest_Ref();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.RDFRest#getParseType <em>Parse Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Parse Type</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.RDFRest#getParseType()
   * @see #getRDFRest()
   * @generated
   */
  EReference getRDFRest_ParseType();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.RDFRest#getFirst <em>First</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>First</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.RDFRest#getFirst()
   * @see #getRDFRest()
   * @generated
   */
  EReference getRDFRest_First();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.RDFRest#getRest <em>Rest</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Rest</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.RDFRest#getRest()
   * @see #getRDFRest()
   * @generated
   */
  EReference getRDFRest_Rest();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.OwlType <em>Owl Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Owl Type</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlType
   * @generated
   */
  EClass getOwlType();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.OwlType#getRefType <em>Ref Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ref Type</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlType#getRefType()
   * @see #getOwlType()
   * @generated
   */
  EReference getOwlType_RefType();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.OwlType#getMembers <em>Members</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Members</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlType#getMembers()
   * @see #getOwlType()
   * @generated
   */
  EReference getOwlType_Members();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.OwlMembers <em>Owl Members</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Owl Members</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlMembers
   * @generated
   */
  EClass getOwlMembers();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.OwlMembers#getParseType <em>Parse Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Parse Type</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlMembers#getParseType()
   * @see #getOwlMembers()
   * @generated
   */
  EReference getOwlMembers_ParseType();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.OwlMembers#getDescriptions <em>Descriptions</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Descriptions</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.OwlMembers#getDescriptions()
   * @see #getOwlMembers()
   * @generated
   */
  EReference getOwlMembers_Descriptions();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.Datatype <em>Datatype</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Datatype</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Datatype
   * @generated
   */
  EClass getDatatype();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.Datatype#getId <em>Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Id</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Datatype#getId()
   * @see #getDatatype()
   * @generated
   */
  EReference getDatatype_Id();

  /**
   * Returns the meta object for the attribute '{@link org.smool.sdk.owlparser.owlmodel.Datatype#isReference <em>Reference</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Reference</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Datatype#isReference()
   * @see #getDatatype()
   * @generated
   */
  EAttribute getDatatype_Reference();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.Datatype#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ref</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.Datatype#getRef()
   * @see #getDatatype()
   * @generated
   */
  EReference getDatatype_Ref();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.SimpleRDFSDomain <em>Simple RDFS Domain</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Simple RDFS Domain</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.SimpleRDFSDomain
   * @generated
   */
  EClass getSimpleRDFSDomain();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.SimpleRDFSDomain#getRef <em>Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ref</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.SimpleRDFSDomain#getRef()
   * @see #getSimpleRDFSDomain()
   * @generated
   */
  EReference getSimpleRDFSDomain_Ref();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.ParsedRDFSDomain <em>Parsed RDFS Domain</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Parsed RDFS Domain</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.ParsedRDFSDomain
   * @generated
   */
  EClass getParsedRDFSDomain();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.ParsedRDFSDomain#getParseType <em>Parse Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Parse Type</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.ParsedRDFSDomain#getParseType()
   * @see #getParsedRDFSDomain()
   * @generated
   */
  EReference getParsedRDFSDomain_ParseType();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.ParsedRDFSDomain#getModifiers <em>Modifiers</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Modifiers</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.ParsedRDFSDomain#getModifiers()
   * @see #getParsedRDFSDomain()
   * @generated
   */
  EReference getParsedRDFSDomain_Modifiers();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.SimpleRDFSRange <em>Simple RDFS Range</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Simple RDFS Range</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.SimpleRDFSRange
   * @generated
   */
  EClass getSimpleRDFSRange();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.SimpleRDFSRange#getRange <em>Range</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Range</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.SimpleRDFSRange#getRange()
   * @see #getSimpleRDFSRange()
   * @generated
   */
  EReference getSimpleRDFSRange_Range();

  /**
   * Returns the meta object for class '{@link org.smool.sdk.owlparser.owlmodel.ParsedPropertyRange <em>Parsed Property Range</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Parsed Property Range</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.ParsedPropertyRange
   * @generated
   */
  EClass getParsedPropertyRange();

  /**
   * Returns the meta object for the containment reference '{@link org.smool.sdk.owlparser.owlmodel.ParsedPropertyRange#getParseType <em>Parse Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Parse Type</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.ParsedPropertyRange#getParseType()
   * @see #getParsedPropertyRange()
   * @generated
   */
  EReference getParsedPropertyRange_ParseType();

  /**
   * Returns the meta object for the containment reference list '{@link org.smool.sdk.owlparser.owlmodel.ParsedPropertyRange#getModifiers <em>Modifiers</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Modifiers</em>'.
   * @see org.smool.sdk.owlparser.owlmodel.ParsedPropertyRange#getModifiers()
   * @see #getParsedPropertyRange()
   * @generated
   */
  EReference getParsedPropertyRange_Modifiers();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  OwlmodelFactory getOwlmodelFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.OWLModelImpl <em>OWL Model</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.OWLModelImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getOWLModel()
     * @generated
     */
    EClass OWL_MODEL = eINSTANCE.getOWLModel();

    /**
     * The meta object literal for the '<em><b>Header</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OWL_MODEL__HEADER = eINSTANCE.getOWLModel_Header();

    /**
     * The meta object literal for the '<em><b>Doc Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OWL_MODEL__DOC_TYPE = eINSTANCE.getOWLModel_DocType();

    /**
     * The meta object literal for the '<em><b>Rdf</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OWL_MODEL__RDF = eINSTANCE.getOWLModel_Rdf();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.DocTypeImpl <em>Doc Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.DocTypeImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getDocType()
     * @generated
     */
    EClass DOC_TYPE = eINSTANCE.getDocType();

    /**
     * The meta object literal for the '<em><b>Doc ID</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DOC_TYPE__DOC_ID = eINSTANCE.getDocType_DocID();

    /**
     * The meta object literal for the '<em><b>Entities</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DOC_TYPE__ENTITIES = eINSTANCE.getDocType_Entities();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.DocEntityImpl <em>Doc Entity</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.DocEntityImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getDocEntity()
     * @generated
     */
    EClass DOC_ENTITY = eINSTANCE.getDocEntity();

    /**
     * The meta object literal for the '<em><b>Entity ID</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DOC_ENTITY__ENTITY_ID = eINSTANCE.getDocEntity_EntityID();

    /**
     * The meta object literal for the '<em><b>Entity URI</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DOC_ENTITY__ENTITY_URI = eINSTANCE.getDocEntity_EntityURI();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.RDFElementImpl <em>RDF Element</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.RDFElementImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getRDFElement()
     * @generated
     */
    EClass RDF_ELEMENT = eINSTANCE.getRDFElement();

    /**
     * The meta object literal for the '<em><b>Imported Ns</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RDF_ELEMENT__IMPORTED_NS = eINSTANCE.getRDFElement_ImportedNs();

    /**
     * The meta object literal for the '<em><b>Base Ont</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RDF_ELEMENT__BASE_ONT = eINSTANCE.getRDFElement_BaseOnt();

    /**
     * The meta object literal for the '<em><b>Ontology Elements</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RDF_ELEMENT__ONTOLOGY_ELEMENTS = eINSTANCE.getRDFElement_OntologyElements();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.XmlNsImpl <em>Xml Ns</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.XmlNsImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getXmlNs()
     * @generated
     */
    EClass XML_NS = eINSTANCE.getXmlNs();

    /**
     * The meta object literal for the '<em><b>Nsid</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute XML_NS__NSID = eINSTANCE.getXmlNs_Nsid();

    /**
     * The meta object literal for the '<em><b>Nsuri</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute XML_NS__NSURI = eINSTANCE.getXmlNs_Nsuri();

    /**
     * The meta object literal for the '<em><b>Nsname</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute XML_NS__NSNAME = eINSTANCE.getXmlNs_Nsname();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.OwlRefImpl <em>Owl Ref</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlRefImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getOwlRef()
     * @generated
     */
    EClass OWL_REF = eINSTANCE.getOwlRef();

    /**
     * The meta object literal for the '<em><b>Ont URI</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OWL_REF__ONT_URI = eINSTANCE.getOwlRef_OntURI();

    /**
     * The meta object literal for the '<em><b>Ont Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OWL_REF__ONT_NAME = eINSTANCE.getOwlRef_OntName();

    /**
     * The meta object literal for the '<em><b>Elem ID</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OWL_REF__ELEM_ID = eINSTANCE.getOwlRef_ElemID();

    /**
     * The meta object literal for the '<em><b>Full URI</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OWL_REF__FULL_URI = eINSTANCE.getOwlRef_FullURI();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.OwlIDImpl <em>Owl ID</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlIDImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getOwlID()
     * @generated
     */
    EClass OWL_ID = eINSTANCE.getOwlID();

    /**
     * The meta object literal for the '<em><b>Elem ID</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OWL_ID__ELEM_ID = eINSTANCE.getOwlID_ElemID();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.OntologyIDImpl <em>Ontology ID</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.OntologyIDImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getOntologyID()
     * @generated
     */
    EClass ONTOLOGY_ID = eINSTANCE.getOntologyID();

    /**
     * The meta object literal for the '<em><b>Super Ont</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ONTOLOGY_ID__SUPER_ONT = eINSTANCE.getOntologyID_SuperOnt();

    /**
     * The meta object literal for the '<em><b>Ont URI</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ONTOLOGY_ID__ONT_URI = eINSTANCE.getOntologyID_OntURI();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.ParseTypeImpl <em>Parse Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.ParseTypeImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getParseType()
     * @generated
     */
    EClass PARSE_TYPE = eINSTANCE.getParseType();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PARSE_TYPE__TYPE = eINSTANCE.getParseType_Type();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.OntologyElementImpl <em>Ontology Element</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.OntologyElementImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getOntologyElement()
     * @generated
     */
    EClass ONTOLOGY_ELEMENT = eINSTANCE.getOntologyElement();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.NamedIndividualImpl <em>Named Individual</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.NamedIndividualImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getNamedIndividual()
     * @generated
     */
    EClass NAMED_INDIVIDUAL = eINSTANCE.getNamedIndividual();

    /**
     * The meta object literal for the '<em><b>Ref</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NAMED_INDIVIDUAL__REF = eINSTANCE.getNamedIndividual_Ref();

    /**
     * The meta object literal for the '<em><b>Types</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NAMED_INDIVIDUAL__TYPES = eINSTANCE.getNamedIndividual_Types();

    /**
     * The meta object literal for the '<em><b>Desc</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute NAMED_INDIVIDUAL__DESC = eINSTANCE.getNamedIndividual_Desc();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.OntologyImpl <em>Ontology</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.OntologyImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getOntology()
     * @generated
     */
    EClass ONTOLOGY = eINSTANCE.getOntology();

    /**
     * The meta object literal for the '<em><b>About</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ONTOLOGY__ABOUT = eINSTANCE.getOntology_About();

    /**
     * The meta object literal for the '<em><b>Resource</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ONTOLOGY__RESOURCE = eINSTANCE.getOntology_Resource();

    /**
     * The meta object literal for the '<em><b>Labels</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ONTOLOGY__LABELS = eINSTANCE.getOntology_Labels();

    /**
     * The meta object literal for the '<em><b>Version Info</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ONTOLOGY__VERSION_INFO = eINSTANCE.getOntology_VersionInfo();

    /**
     * The meta object literal for the '<em><b>See Also</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ONTOLOGY__SEE_ALSO = eINSTANCE.getOntology_SeeAlso();

    /**
     * The meta object literal for the '<em><b>PVers</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ONTOLOGY__PVERS = eINSTANCE.getOntology_PVers();

    /**
     * The meta object literal for the '<em><b>Back Comp</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ONTOLOGY__BACK_COMP = eINSTANCE.getOntology_BackComp();

    /**
     * The meta object literal for the '<em><b>Imported Ontologies</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ONTOLOGY__IMPORTED_ONTOLOGIES = eINSTANCE.getOntology_ImportedOntologies();

    /**
     * The meta object literal for the '<em><b>Others</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ONTOLOGY__OTHERS = eINSTANCE.getOntology_Others();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.SeeAlsoImpl <em>See Also</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.SeeAlsoImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getSeeAlso()
     * @generated
     */
    EClass SEE_ALSO = eINSTANCE.getSeeAlso();

    /**
     * The meta object literal for the '<em><b>Ref</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SEE_ALSO__REF = eINSTANCE.getSeeAlso_Ref();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.PriorVersionImpl <em>Prior Version</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.PriorVersionImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getPriorVersion()
     * @generated
     */
    EClass PRIOR_VERSION = eINSTANCE.getPriorVersion();

    /**
     * The meta object literal for the '<em><b>Ref</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PRIOR_VERSION__REF = eINSTANCE.getPriorVersion_Ref();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.BackwardsCompImpl <em>Backwards Comp</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.BackwardsCompImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getBackwardsComp()
     * @generated
     */
    EClass BACKWARDS_COMP = eINSTANCE.getBackwardsComp();

    /**
     * The meta object literal for the '<em><b>Ref</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BACKWARDS_COMP__REF = eINSTANCE.getBackwardsComp_Ref();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.OwlImportImpl <em>Owl Import</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlImportImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getOwlImport()
     * @generated
     */
    EClass OWL_IMPORT = eINSTANCE.getOwlImport();

    /**
     * The meta object literal for the '<em><b>Ref</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OWL_IMPORT__REF = eINSTANCE.getOwlImport_Ref();

    /**
     * The meta object literal for the '<em><b>Onts</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OWL_IMPORT__ONTS = eINSTANCE.getOwlImport_Onts();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.OwlClassImpl <em>Owl Class</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlClassImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getOwlClass()
     * @generated
     */
    EClass OWL_CLASS = eINSTANCE.getOwlClass();

    /**
     * The meta object literal for the '<em><b>Id</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OWL_CLASS__ID = eINSTANCE.getOwlClass_Id();

    /**
     * The meta object literal for the '<em><b>Reference</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OWL_CLASS__REFERENCE = eINSTANCE.getOwlClass_Reference();

    /**
     * The meta object literal for the '<em><b>Ref</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OWL_CLASS__REF = eINSTANCE.getOwlClass_Ref();

    /**
     * The meta object literal for the '<em><b>Labels</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OWL_CLASS__LABELS = eINSTANCE.getOwlClass_Labels();

    /**
     * The meta object literal for the '<em><b>Version Info</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OWL_CLASS__VERSION_INFO = eINSTANCE.getOwlClass_VersionInfo();

    /**
     * The meta object literal for the '<em><b>Super Types</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OWL_CLASS__SUPER_TYPES = eINSTANCE.getOwlClass_SuperTypes();

    /**
     * The meta object literal for the '<em><b>Axioms</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OWL_CLASS__AXIOMS = eINSTANCE.getOwlClass_Axioms();

    /**
     * The meta object literal for the '<em><b>Onts</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OWL_CLASS__ONTS = eINSTANCE.getOwlClass_Onts();

    /**
     * The meta object literal for the '<em><b>Others</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OWL_CLASS__OTHERS = eINSTANCE.getOwlClass_Others();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.MinimumClassImpl <em>Minimum Class</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.MinimumClassImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getMinimumClass()
     * @generated
     */
    EClass MINIMUM_CLASS = eINSTANCE.getMinimumClass();

    /**
     * The meta object literal for the '<em><b>Labels</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MINIMUM_CLASS__LABELS = eINSTANCE.getMinimumClass_Labels();

    /**
     * The meta object literal for the '<em><b>Axioms</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MINIMUM_CLASS__AXIOMS = eINSTANCE.getMinimumClass_Axioms();

    /**
     * The meta object literal for the '<em><b>Others</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MINIMUM_CLASS__OTHERS = eINSTANCE.getMinimumClass_Others();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.ClassAxiomImpl <em>Class Axiom</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.ClassAxiomImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getClassAxiom()
     * @generated
     */
    EClass CLASS_AXIOM = eINSTANCE.getClassAxiom();

    /**
     * The meta object literal for the '<em><b>Parse Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CLASS_AXIOM__PARSE_TYPE = eINSTANCE.getClassAxiom_ParseType();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.ComplementAxiomImpl <em>Complement Axiom</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.ComplementAxiomImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getComplementAxiom()
     * @generated
     */
    EClass COMPLEMENT_AXIOM = eINSTANCE.getComplementAxiom();

    /**
     * The meta object literal for the '<em><b>Ref</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference COMPLEMENT_AXIOM__REF = eINSTANCE.getComplementAxiom_Ref();

    /**
     * The meta object literal for the '<em><b>Classes</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference COMPLEMENT_AXIOM__CLASSES = eINSTANCE.getComplementAxiom_Classes();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.IncompatibleWithAxiomImpl <em>Incompatible With Axiom</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.IncompatibleWithAxiomImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getIncompatibleWithAxiom()
     * @generated
     */
    EClass INCOMPATIBLE_WITH_AXIOM = eINSTANCE.getIncompatibleWithAxiom();

    /**
     * The meta object literal for the '<em><b>Ref</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INCOMPATIBLE_WITH_AXIOM__REF = eINSTANCE.getIncompatibleWithAxiom_Ref();

    /**
     * The meta object literal for the '<em><b>Classes</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INCOMPATIBLE_WITH_AXIOM__CLASSES = eINSTANCE.getIncompatibleWithAxiom_Classes();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.DisjointAxiomImpl <em>Disjoint Axiom</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.DisjointAxiomImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getDisjointAxiom()
     * @generated
     */
    EClass DISJOINT_AXIOM = eINSTANCE.getDisjointAxiom();

    /**
     * The meta object literal for the '<em><b>Ref</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DISJOINT_AXIOM__REF = eINSTANCE.getDisjointAxiom_Ref();

    /**
     * The meta object literal for the '<em><b>Classes</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DISJOINT_AXIOM__CLASSES = eINSTANCE.getDisjointAxiom_Classes();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.EquivalentAxiomImpl <em>Equivalent Axiom</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.EquivalentAxiomImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getEquivalentAxiom()
     * @generated
     */
    EClass EQUIVALENT_AXIOM = eINSTANCE.getEquivalentAxiom();

    /**
     * The meta object literal for the '<em><b>Ref</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EQUIVALENT_AXIOM__REF = eINSTANCE.getEquivalentAxiom_Ref();

    /**
     * The meta object literal for the '<em><b>Classes</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EQUIVALENT_AXIOM__CLASSES = eINSTANCE.getEquivalentAxiom_Classes();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.UnionAxiomImpl <em>Union Axiom</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.UnionAxiomImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getUnionAxiom()
     * @generated
     */
    EClass UNION_AXIOM = eINSTANCE.getUnionAxiom();

    /**
     * The meta object literal for the '<em><b>Classes</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference UNION_AXIOM__CLASSES = eINSTANCE.getUnionAxiom_Classes();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.IntersectionAxiomImpl <em>Intersection Axiom</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.IntersectionAxiomImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getIntersectionAxiom()
     * @generated
     */
    EClass INTERSECTION_AXIOM = eINSTANCE.getIntersectionAxiom();

    /**
     * The meta object literal for the '<em><b>Classes</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INTERSECTION_AXIOM__CLASSES = eINSTANCE.getIntersectionAxiom_Classes();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.OneOfAxiomImpl <em>One Of Axiom</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.OneOfAxiomImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getOneOfAxiom()
     * @generated
     */
    EClass ONE_OF_AXIOM = eINSTANCE.getOneOfAxiom();

    /**
     * The meta object literal for the '<em><b>Values</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ONE_OF_AXIOM__VALUES = eINSTANCE.getOneOfAxiom_Values();

    /**
     * The meta object literal for the '<em><b>Tag Names</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ONE_OF_AXIOM__TAG_NAMES = eINSTANCE.getOneOfAxiom_TagNames();

    /**
     * The meta object literal for the '<em><b>Tagids</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ONE_OF_AXIOM__TAGIDS = eINSTANCE.getOneOfAxiom_Tagids();

    /**
     * The meta object literal for the '<em><b>First</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ONE_OF_AXIOM__FIRST = eINSTANCE.getOneOfAxiom_First();

    /**
     * The meta object literal for the '<em><b>Rest</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ONE_OF_AXIOM__REST = eINSTANCE.getOneOfAxiom_Rest();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.OwlLabelImpl <em>Owl Label</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlLabelImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getOwlLabel()
     * @generated
     */
    EClass OWL_LABEL = eINSTANCE.getOwlLabel();

    /**
     * The meta object literal for the '<em><b>Lang</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OWL_LABEL__LANG = eINSTANCE.getOwlLabel_Lang();

    /**
     * The meta object literal for the '<em><b>Datatype</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OWL_LABEL__DATATYPE = eINSTANCE.getOwlLabel_Datatype();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OWL_LABEL__VALUE = eINSTANCE.getOwlLabel_Value();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.OwlVersionImpl <em>Owl Version</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlVersionImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getOwlVersion()
     * @generated
     */
    EClass OWL_VERSION = eINSTANCE.getOwlVersion();

    /**
     * The meta object literal for the '<em><b>Lang</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OWL_VERSION__LANG = eINSTANCE.getOwlVersion_Lang();

    /**
     * The meta object literal for the '<em><b>Datatype</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OWL_VERSION__DATATYPE = eINSTANCE.getOwlVersion_Datatype();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OWL_VERSION__VALUE = eINSTANCE.getOwlVersion_Value();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.SubTypeOfImpl <em>Sub Type Of</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.SubTypeOfImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getSubTypeOf()
     * @generated
     */
    EClass SUB_TYPE_OF = eINSTANCE.getSubTypeOf();

    /**
     * The meta object literal for the '<em><b>Resource Based</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SUB_TYPE_OF__RESOURCE_BASED = eINSTANCE.getSubTypeOf_ResourceBased();

    /**
     * The meta object literal for the '<em><b>Class Ref</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SUB_TYPE_OF__CLASS_REF = eINSTANCE.getSubTypeOf_ClassRef();

    /**
     * The meta object literal for the '<em><b>Modifiers</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SUB_TYPE_OF__MODIFIERS = eINSTANCE.getSubTypeOf_Modifiers();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.ClassModifierImpl <em>Class Modifier</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.ClassModifierImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getClassModifier()
     * @generated
     */
    EClass CLASS_MODIFIER = eINSTANCE.getClassModifier();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.OwlRestrictionImpl <em>Owl Restriction</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlRestrictionImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getOwlRestriction()
     * @generated
     */
    EClass OWL_RESTRICTION = eINSTANCE.getOwlRestriction();

    /**
     * The meta object literal for the '<em><b>Restriction Bodies</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OWL_RESTRICTION__RESTRICTION_BODIES = eINSTANCE.getOwlRestriction_RestrictionBodies();

    /**
     * The meta object literal for the '<em><b>Prop</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OWL_RESTRICTION__PROP = eINSTANCE.getOwlRestriction_Prop();

    /**
     * The meta object literal for the '<em><b>Property</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OWL_RESTRICTION__PROPERTY = eINSTANCE.getOwlRestriction_Property();

    /**
     * The meta object literal for the '<em><b>Class</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OWL_RESTRICTION__CLASS = eINSTANCE.getOwlRestriction_Class();

    /**
     * The meta object literal for the '<em><b>Classes</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OWL_RESTRICTION__CLASSES = eINSTANCE.getOwlRestriction_Classes();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.RestrictionBodyImpl <em>Restriction Body</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.RestrictionBodyImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getRestrictionBody()
     * @generated
     */
    EClass RESTRICTION_BODY = eINSTANCE.getRestrictionBody();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.MinCardinalityImpl <em>Min Cardinality</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.MinCardinalityImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getMinCardinality()
     * @generated
     */
    EClass MIN_CARDINALITY = eINSTANCE.getMinCardinality();

    /**
     * The meta object literal for the '<em><b>Datatype</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MIN_CARDINALITY__DATATYPE = eINSTANCE.getMinCardinality_Datatype();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MIN_CARDINALITY__VALUE = eINSTANCE.getMinCardinality_Value();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.MaxCardinalityImpl <em>Max Cardinality</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.MaxCardinalityImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getMaxCardinality()
     * @generated
     */
    EClass MAX_CARDINALITY = eINSTANCE.getMaxCardinality();

    /**
     * The meta object literal for the '<em><b>Datatype</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MAX_CARDINALITY__DATATYPE = eINSTANCE.getMaxCardinality_Datatype();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MAX_CARDINALITY__VALUE = eINSTANCE.getMaxCardinality_Value();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.CardinalityImpl <em>Cardinality</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.CardinalityImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getCardinality()
     * @generated
     */
    EClass CARDINALITY = eINSTANCE.getCardinality();

    /**
     * The meta object literal for the '<em><b>Datatype</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CARDINALITY__DATATYPE = eINSTANCE.getCardinality_Datatype();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CARDINALITY__VALUE = eINSTANCE.getCardinality_Value();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.QualifiedCardinalityImpl <em>Qualified Cardinality</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.QualifiedCardinalityImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getQualifiedCardinality()
     * @generated
     */
    EClass QUALIFIED_CARDINALITY = eINSTANCE.getQualifiedCardinality();

    /**
     * The meta object literal for the '<em><b>Datatype</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference QUALIFIED_CARDINALITY__DATATYPE = eINSTANCE.getQualifiedCardinality_Datatype();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute QUALIFIED_CARDINALITY__VALUE = eINSTANCE.getQualifiedCardinality_Value();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.MinQualifiedCardinalityImpl <em>Min Qualified Cardinality</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.MinQualifiedCardinalityImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getMinQualifiedCardinality()
     * @generated
     */
    EClass MIN_QUALIFIED_CARDINALITY = eINSTANCE.getMinQualifiedCardinality();

    /**
     * The meta object literal for the '<em><b>Datatype</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MIN_QUALIFIED_CARDINALITY__DATATYPE = eINSTANCE.getMinQualifiedCardinality_Datatype();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MIN_QUALIFIED_CARDINALITY__VALUE = eINSTANCE.getMinQualifiedCardinality_Value();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.MaxQualifiedCardinalityImpl <em>Max Qualified Cardinality</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.MaxQualifiedCardinalityImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getMaxQualifiedCardinality()
     * @generated
     */
    EClass MAX_QUALIFIED_CARDINALITY = eINSTANCE.getMaxQualifiedCardinality();

    /**
     * The meta object literal for the '<em><b>Datatype</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MAX_QUALIFIED_CARDINALITY__DATATYPE = eINSTANCE.getMaxQualifiedCardinality_Datatype();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MAX_QUALIFIED_CARDINALITY__VALUE = eINSTANCE.getMaxQualifiedCardinality_Value();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.SomeValuesImpl <em>Some Values</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.SomeValuesImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getSomeValues()
     * @generated
     */
    EClass SOME_VALUES = eINSTANCE.getSomeValues();

    /**
     * The meta object literal for the '<em><b>Ref</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SOME_VALUES__REF = eINSTANCE.getSomeValues_Ref();

    /**
     * The meta object literal for the '<em><b>Modifiers</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SOME_VALUES__MODIFIERS = eINSTANCE.getSomeValues_Modifiers();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.AllValuesImpl <em>All Values</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.AllValuesImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getAllValues()
     * @generated
     */
    EClass ALL_VALUES = eINSTANCE.getAllValues();

    /**
     * The meta object literal for the '<em><b>Ref</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ALL_VALUES__REF = eINSTANCE.getAllValues_Ref();

    /**
     * The meta object literal for the '<em><b>Modifiers</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ALL_VALUES__MODIFIERS = eINSTANCE.getAllValues_Modifiers();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.HasValueImpl <em>Has Value</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.HasValueImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getHasValue()
     * @generated
     */
    EClass HAS_VALUE = eINSTANCE.getHasValue();

    /**
     * The meta object literal for the '<em><b>Ref</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference HAS_VALUE__REF = eINSTANCE.getHasValue_Ref();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference HAS_VALUE__TYPE = eINSTANCE.getHasValue_Type();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute HAS_VALUE__VALUE = eINSTANCE.getHasValue_Value();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.PropertyImpl <em>Property</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.PropertyImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getProperty()
     * @generated
     */
    EClass PROPERTY = eINSTANCE.getProperty();

    /**
     * The meta object literal for the '<em><b>Id</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROPERTY__ID = eINSTANCE.getProperty_Id();

    /**
     * The meta object literal for the '<em><b>Reference</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PROPERTY__REFERENCE = eINSTANCE.getProperty_Reference();

    /**
     * The meta object literal for the '<em><b>Ref</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROPERTY__REF = eINSTANCE.getProperty_Ref();

    /**
     * The meta object literal for the '<em><b>Others</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PROPERTY__OTHERS = eINSTANCE.getProperty_Others();

    /**
     * The meta object literal for the '<em><b>Labels</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROPERTY__LABELS = eINSTANCE.getProperty_Labels();

    /**
     * The meta object literal for the '<em><b>Version Info</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROPERTY__VERSION_INFO = eINSTANCE.getProperty_VersionInfo();

    /**
     * The meta object literal for the '<em><b>Ranges</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROPERTY__RANGES = eINSTANCE.getProperty_Ranges();

    /**
     * The meta object literal for the '<em><b>Types</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROPERTY__TYPES = eINSTANCE.getProperty_Types();

    /**
     * The meta object literal for the '<em><b>Domains</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROPERTY__DOMAINS = eINSTANCE.getProperty_Domains();

    /**
     * The meta object literal for the '<em><b>Modifiers</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROPERTY__MODIFIERS = eINSTANCE.getProperty_Modifiers();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.AnnotationPropertyImpl <em>Annotation Property</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.AnnotationPropertyImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getAnnotationProperty()
     * @generated
     */
    EClass ANNOTATION_PROPERTY = eINSTANCE.getAnnotationProperty();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.ObjectPropertyImpl <em>Object Property</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.ObjectPropertyImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getObjectProperty()
     * @generated
     */
    EClass OBJECT_PROPERTY = eINSTANCE.getObjectProperty();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.DatatypePropertyImpl <em>Datatype Property</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.DatatypePropertyImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getDatatypeProperty()
     * @generated
     */
    EClass DATATYPE_PROPERTY = eINSTANCE.getDatatypeProperty();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.FunctionalPropertyImpl <em>Functional Property</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.FunctionalPropertyImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getFunctionalProperty()
     * @generated
     */
    EClass FUNCTIONAL_PROPERTY = eINSTANCE.getFunctionalProperty();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.InverseFunctionalPropertyImpl <em>Inverse Functional Property</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.InverseFunctionalPropertyImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getInverseFunctionalProperty()
     * @generated
     */
    EClass INVERSE_FUNCTIONAL_PROPERTY = eINSTANCE.getInverseFunctionalProperty();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.SymmetricPropertyImpl <em>Symmetric Property</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.SymmetricPropertyImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getSymmetricProperty()
     * @generated
     */
    EClass SYMMETRIC_PROPERTY = eINSTANCE.getSymmetricProperty();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.TransitivePropertyImpl <em>Transitive Property</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.TransitivePropertyImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getTransitiveProperty()
     * @generated
     */
    EClass TRANSITIVE_PROPERTY = eINSTANCE.getTransitiveProperty();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.PropertyModifierImpl <em>Property Modifier</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.PropertyModifierImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getPropertyModifier()
     * @generated
     */
    EClass PROPERTY_MODIFIER = eINSTANCE.getPropertyModifier();

    /**
     * The meta object literal for the '<em><b>Props</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROPERTY_MODIFIER__PROPS = eINSTANCE.getPropertyModifier_Props();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.InverseTypeImpl <em>Inverse Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.InverseTypeImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getInverseType()
     * @generated
     */
    EClass INVERSE_TYPE = eINSTANCE.getInverseType();

    /**
     * The meta object literal for the '<em><b>Ref</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INVERSE_TYPE__REF = eINSTANCE.getInverseType_Ref();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.EquivalentTypeImpl <em>Equivalent Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.EquivalentTypeImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getEquivalentType()
     * @generated
     */
    EClass EQUIVALENT_TYPE = eINSTANCE.getEquivalentType();

    /**
     * The meta object literal for the '<em><b>Ref</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EQUIVALENT_TYPE__REF = eINSTANCE.getEquivalentType_Ref();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.RDFSDomainImpl <em>RDFS Domain</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.RDFSDomainImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getRDFSDomain()
     * @generated
     */
    EClass RDFS_DOMAIN = eINSTANCE.getRDFSDomain();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.RDFSRangeImpl <em>RDFS Range</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.RDFSRangeImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getRDFSRange()
     * @generated
     */
    EClass RDFS_RANGE = eINSTANCE.getRDFSRange();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.OwlDataRangeImpl <em>Owl Data Range</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlDataRangeImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getOwlDataRange()
     * @generated
     */
    EClass OWL_DATA_RANGE = eINSTANCE.getOwlDataRange();

    /**
     * The meta object literal for the '<em><b>Owl Range</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OWL_DATA_RANGE__OWL_RANGE = eINSTANCE.getOwlDataRange_OwlRange();

    /**
     * The meta object literal for the '<em><b>Union</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OWL_DATA_RANGE__UNION = eINSTANCE.getOwlDataRange_Union();

    /**
     * The meta object literal for the '<em><b>One Of</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OWL_DATA_RANGE__ONE_OF = eINSTANCE.getOwlDataRange_OneOf();

    /**
     * The meta object literal for the '<em><b>Intersection</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OWL_DATA_RANGE__INTERSECTION = eINSTANCE.getOwlDataRange_Intersection();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.RDFTypeImpl <em>RDF Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.RDFTypeImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getRDFType()
     * @generated
     */
    EClass RDF_TYPE = eINSTANCE.getRDFType();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RDF_TYPE__TYPE = eINSTANCE.getRDFType_Type();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.SubPropertyOfImpl <em>Sub Property Of</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.SubPropertyOfImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getSubPropertyOf()
     * @generated
     */
    EClass SUB_PROPERTY_OF = eINSTANCE.getSubPropertyOf();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SUB_PROPERTY_OF__TYPE = eINSTANCE.getSubPropertyOf_Type();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.DescriptionImpl <em>Description</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.DescriptionImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getDescription()
     * @generated
     */
    EClass DESCRIPTION = eINSTANCE.getDescription();

    /**
     * The meta object literal for the '<em><b>About</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DESCRIPTION__ABOUT = eINSTANCE.getDescription_About();

    /**
     * The meta object literal for the '<em><b>First</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DESCRIPTION__FIRST = eINSTANCE.getDescription_First();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DESCRIPTION__TYPE = eINSTANCE.getDescription_Type();

    /**
     * The meta object literal for the '<em><b>Domain</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DESCRIPTION__DOMAIN = eINSTANCE.getDescription_Domain();

    /**
     * The meta object literal for the '<em><b>Range</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DESCRIPTION__RANGE = eINSTANCE.getDescription_Range();

    /**
     * The meta object literal for the '<em><b>Prop Modifiers</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DESCRIPTION__PROP_MODIFIERS = eINSTANCE.getDescription_PropModifiers();

    /**
     * The meta object literal for the '<em><b>Labels</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DESCRIPTION__LABELS = eINSTANCE.getDescription_Labels();

    /**
     * The meta object literal for the '<em><b>Axioms</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DESCRIPTION__AXIOMS = eINSTANCE.getDescription_Axioms();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.OwlFirstImpl <em>Owl First</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlFirstImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getOwlFirst()
     * @generated
     */
    EClass OWL_FIRST = eINSTANCE.getOwlFirst();

    /**
     * The meta object literal for the '<em><b>Ref Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OWL_FIRST__REF_TYPE = eINSTANCE.getOwlFirst_RefType();

    /**
     * The meta object literal for the '<em><b>Data Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OWL_FIRST__DATA_TYPE = eINSTANCE.getOwlFirst_DataType();

    /**
     * The meta object literal for the '<em><b>Data Type Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OWL_FIRST__DATA_TYPE_VALUE = eINSTANCE.getOwlFirst_DataTypeValue();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.RDFRestImpl <em>RDF Rest</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.RDFRestImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getRDFRest()
     * @generated
     */
    EClass RDF_REST = eINSTANCE.getRDFRest();

    /**
     * The meta object literal for the '<em><b>Ref</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RDF_REST__REF = eINSTANCE.getRDFRest_Ref();

    /**
     * The meta object literal for the '<em><b>Parse Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RDF_REST__PARSE_TYPE = eINSTANCE.getRDFRest_ParseType();

    /**
     * The meta object literal for the '<em><b>First</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RDF_REST__FIRST = eINSTANCE.getRDFRest_First();

    /**
     * The meta object literal for the '<em><b>Rest</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RDF_REST__REST = eINSTANCE.getRDFRest_Rest();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.OwlTypeImpl <em>Owl Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlTypeImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getOwlType()
     * @generated
     */
    EClass OWL_TYPE = eINSTANCE.getOwlType();

    /**
     * The meta object literal for the '<em><b>Ref Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OWL_TYPE__REF_TYPE = eINSTANCE.getOwlType_RefType();

    /**
     * The meta object literal for the '<em><b>Members</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OWL_TYPE__MEMBERS = eINSTANCE.getOwlType_Members();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.OwlMembersImpl <em>Owl Members</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlMembersImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getOwlMembers()
     * @generated
     */
    EClass OWL_MEMBERS = eINSTANCE.getOwlMembers();

    /**
     * The meta object literal for the '<em><b>Parse Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OWL_MEMBERS__PARSE_TYPE = eINSTANCE.getOwlMembers_ParseType();

    /**
     * The meta object literal for the '<em><b>Descriptions</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OWL_MEMBERS__DESCRIPTIONS = eINSTANCE.getOwlMembers_Descriptions();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.DatatypeImpl <em>Datatype</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.DatatypeImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getDatatype()
     * @generated
     */
    EClass DATATYPE = eINSTANCE.getDatatype();

    /**
     * The meta object literal for the '<em><b>Id</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DATATYPE__ID = eINSTANCE.getDatatype_Id();

    /**
     * The meta object literal for the '<em><b>Reference</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DATATYPE__REFERENCE = eINSTANCE.getDatatype_Reference();

    /**
     * The meta object literal for the '<em><b>Ref</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DATATYPE__REF = eINSTANCE.getDatatype_Ref();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.SimpleRDFSDomainImpl <em>Simple RDFS Domain</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.SimpleRDFSDomainImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getSimpleRDFSDomain()
     * @generated
     */
    EClass SIMPLE_RDFS_DOMAIN = eINSTANCE.getSimpleRDFSDomain();

    /**
     * The meta object literal for the '<em><b>Ref</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SIMPLE_RDFS_DOMAIN__REF = eINSTANCE.getSimpleRDFSDomain_Ref();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.ParsedRDFSDomainImpl <em>Parsed RDFS Domain</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.ParsedRDFSDomainImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getParsedRDFSDomain()
     * @generated
     */
    EClass PARSED_RDFS_DOMAIN = eINSTANCE.getParsedRDFSDomain();

    /**
     * The meta object literal for the '<em><b>Parse Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PARSED_RDFS_DOMAIN__PARSE_TYPE = eINSTANCE.getParsedRDFSDomain_ParseType();

    /**
     * The meta object literal for the '<em><b>Modifiers</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PARSED_RDFS_DOMAIN__MODIFIERS = eINSTANCE.getParsedRDFSDomain_Modifiers();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.SimpleRDFSRangeImpl <em>Simple RDFS Range</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.SimpleRDFSRangeImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getSimpleRDFSRange()
     * @generated
     */
    EClass SIMPLE_RDFS_RANGE = eINSTANCE.getSimpleRDFSRange();

    /**
     * The meta object literal for the '<em><b>Range</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SIMPLE_RDFS_RANGE__RANGE = eINSTANCE.getSimpleRDFSRange_Range();

    /**
     * The meta object literal for the '{@link org.smool.sdk.owlparser.owlmodel.impl.ParsedPropertyRangeImpl <em>Parsed Property Range</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.smool.sdk.owlparser.owlmodel.impl.ParsedPropertyRangeImpl
     * @see org.smool.sdk.owlparser.owlmodel.impl.OwlmodelPackageImpl#getParsedPropertyRange()
     * @generated
     */
    EClass PARSED_PROPERTY_RANGE = eINSTANCE.getParsedPropertyRange();

    /**
     * The meta object literal for the '<em><b>Parse Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PARSED_PROPERTY_RANGE__PARSE_TYPE = eINSTANCE.getParsedPropertyRange_ParseType();

    /**
     * The meta object literal for the '<em><b>Modifiers</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PARSED_PROPERTY_RANGE__MODIFIERS = eINSTANCE.getParsedPropertyRange_Modifiers();

  }

} //OwlmodelPackage
