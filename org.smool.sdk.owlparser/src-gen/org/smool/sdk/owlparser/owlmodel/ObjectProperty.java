/**
 */
package org.smool.sdk.owlparser.owlmodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Object Property</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getObjectProperty()
 * @model
 * @generated
 */
public interface ObjectProperty extends Property
{
} // ObjectProperty
