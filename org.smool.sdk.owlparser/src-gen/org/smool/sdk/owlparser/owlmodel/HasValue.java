/**
 */
package org.smool.sdk.owlparser.owlmodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Has Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.HasValue#getRef <em>Ref</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.HasValue#getType <em>Type</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.HasValue#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getHasValue()
 * @model
 * @generated
 */
public interface HasValue extends RestrictionBody
{
  /**
   * Returns the value of the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ref</em>' containment reference.
   * @see #setRef(OwlRef)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getHasValue_Ref()
   * @model containment="true"
   * @generated
   */
  OwlRef getRef();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.HasValue#getRef <em>Ref</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ref</em>' containment reference.
   * @see #getRef()
   * @generated
   */
  void setRef(OwlRef value);

  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference.
   * @see #setType(OwlRef)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getHasValue_Type()
   * @model containment="true"
   * @generated
   */
  OwlRef getType();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.HasValue#getType <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' containment reference.
   * @see #getType()
   * @generated
   */
  void setType(OwlRef value);

  /**
   * Returns the value of the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' attribute.
   * @see #setValue(String)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getHasValue_Value()
   * @model
   * @generated
   */
  String getValue();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.HasValue#getValue <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' attribute.
   * @see #getValue()
   * @generated
   */
  void setValue(String value);

} // HasValue
