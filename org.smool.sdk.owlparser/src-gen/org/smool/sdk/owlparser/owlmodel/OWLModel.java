/**
 */
package org.smool.sdk.owlparser.owlmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>OWL Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OWLModel#getHeader <em>Header</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OWLModel#getDocType <em>Doc Type</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OWLModel#getRdf <em>Rdf</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOWLModel()
 * @model
 * @generated
 */
public interface OWLModel extends EObject
{
  /**
   * Returns the value of the '<em><b>Header</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Header</em>' attribute.
   * @see #setHeader(String)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOWLModel_Header()
   * @model
   * @generated
   */
  String getHeader();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.OWLModel#getHeader <em>Header</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Header</em>' attribute.
   * @see #getHeader()
   * @generated
   */
  void setHeader(String value);

  /**
   * Returns the value of the '<em><b>Doc Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Doc Type</em>' containment reference.
   * @see #setDocType(DocType)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOWLModel_DocType()
   * @model containment="true"
   * @generated
   */
  DocType getDocType();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.OWLModel#getDocType <em>Doc Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Doc Type</em>' containment reference.
   * @see #getDocType()
   * @generated
   */
  void setDocType(DocType value);

  /**
   * Returns the value of the '<em><b>Rdf</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Rdf</em>' containment reference.
   * @see #setRdf(RDFElement)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOWLModel_Rdf()
   * @model containment="true"
   * @generated
   */
  RDFElement getRdf();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.OWLModel#getRdf <em>Rdf</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Rdf</em>' containment reference.
   * @see #getRdf()
   * @generated
   */
  void setRdf(RDFElement value);

} // OWLModel
