/**
 */
package org.smool.sdk.owlparser.owlmodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transitive Property</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getTransitiveProperty()
 * @model
 * @generated
 */
public interface TransitiveProperty extends Property
{
} // TransitiveProperty
