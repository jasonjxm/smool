/**
 */
package org.smool.sdk.owlparser.owlmodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Minimum Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.MinimumClass#getLabels <em>Labels</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.MinimumClass#getAxioms <em>Axioms</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.MinimumClass#getOthers <em>Others</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getMinimumClass()
 * @model
 * @generated
 */
public interface MinimumClass extends ClassModifier
{
  /**
   * Returns the value of the '<em><b>Labels</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.OwlLabel}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Labels</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getMinimumClass_Labels()
   * @model containment="true"
   * @generated
   */
  EList<OwlLabel> getLabels();

  /**
   * Returns the value of the '<em><b>Axioms</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.ClassAxiom}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Axioms</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getMinimumClass_Axioms()
   * @model containment="true"
   * @generated
   */
  EList<ClassAxiom> getAxioms();

  /**
   * Returns the value of the '<em><b>Others</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Others</em>' attribute list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getMinimumClass_Others()
   * @model unique="false"
   * @generated
   */
  EList<String> getOthers();

} // MinimumClass
