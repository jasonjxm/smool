/**
 */
package org.smool.sdk.owlparser.owlmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Owl First</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OwlFirst#getRefType <em>Ref Type</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OwlFirst#getDataType <em>Data Type</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OwlFirst#getDataTypeValue <em>Data Type Value</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlFirst()
 * @model
 * @generated
 */
public interface OwlFirst extends EObject
{
  /**
   * Returns the value of the '<em><b>Ref Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ref Type</em>' containment reference.
   * @see #setRefType(OwlRef)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlFirst_RefType()
   * @model containment="true"
   * @generated
   */
  OwlRef getRefType();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.OwlFirst#getRefType <em>Ref Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ref Type</em>' containment reference.
   * @see #getRefType()
   * @generated
   */
  void setRefType(OwlRef value);

  /**
   * Returns the value of the '<em><b>Data Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Data Type</em>' containment reference.
   * @see #setDataType(OwlRef)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlFirst_DataType()
   * @model containment="true"
   * @generated
   */
  OwlRef getDataType();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.OwlFirst#getDataType <em>Data Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Data Type</em>' containment reference.
   * @see #getDataType()
   * @generated
   */
  void setDataType(OwlRef value);

  /**
   * Returns the value of the '<em><b>Data Type Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Data Type Value</em>' attribute.
   * @see #setDataTypeValue(String)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlFirst_DataTypeValue()
   * @model
   * @generated
   */
  String getDataTypeValue();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.OwlFirst#getDataTypeValue <em>Data Type Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Data Type Value</em>' attribute.
   * @see #getDataTypeValue()
   * @generated
   */
  void setDataTypeValue(String value);

} // OwlFirst
