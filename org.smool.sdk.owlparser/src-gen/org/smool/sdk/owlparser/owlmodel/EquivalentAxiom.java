/**
 */
package org.smool.sdk.owlparser.owlmodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Equivalent Axiom</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.EquivalentAxiom#getRef <em>Ref</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.EquivalentAxiom#getClasses <em>Classes</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getEquivalentAxiom()
 * @model
 * @generated
 */
public interface EquivalentAxiom extends ClassAxiom
{
  /**
   * Returns the value of the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ref</em>' containment reference.
   * @see #setRef(OwlRef)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getEquivalentAxiom_Ref()
   * @model containment="true"
   * @generated
   */
  OwlRef getRef();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.EquivalentAxiom#getRef <em>Ref</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ref</em>' containment reference.
   * @see #getRef()
   * @generated
   */
  void setRef(OwlRef value);

  /**
   * Returns the value of the '<em><b>Classes</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.ClassModifier}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Classes</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getEquivalentAxiom_Classes()
   * @model containment="true"
   * @generated
   */
  EList<ClassModifier> getClasses();

} // EquivalentAxiom
