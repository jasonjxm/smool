/**
 */
package org.smool.sdk.owlparser.owlmodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Max Cardinality</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.MaxCardinality#getDatatype <em>Datatype</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.MaxCardinality#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getMaxCardinality()
 * @model
 * @generated
 */
public interface MaxCardinality extends RestrictionBody
{
  /**
   * Returns the value of the '<em><b>Datatype</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Datatype</em>' containment reference.
   * @see #setDatatype(OwlRef)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getMaxCardinality_Datatype()
   * @model containment="true"
   * @generated
   */
  OwlRef getDatatype();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.MaxCardinality#getDatatype <em>Datatype</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Datatype</em>' containment reference.
   * @see #getDatatype()
   * @generated
   */
  void setDatatype(OwlRef value);

  /**
   * Returns the value of the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' attribute.
   * @see #setValue(int)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getMaxCardinality_Value()
   * @model
   * @generated
   */
  int getValue();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.MaxCardinality#getValue <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' attribute.
   * @see #getValue()
   * @generated
   */
  void setValue(int value);

} // MaxCardinality
