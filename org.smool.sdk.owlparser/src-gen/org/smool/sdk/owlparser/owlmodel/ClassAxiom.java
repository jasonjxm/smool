/**
 */
package org.smool.sdk.owlparser.owlmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class Axiom</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.ClassAxiom#getParseType <em>Parse Type</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getClassAxiom()
 * @model
 * @generated
 */
public interface ClassAxiom extends EObject
{
  /**
   * Returns the value of the '<em><b>Parse Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Parse Type</em>' containment reference.
   * @see #setParseType(ParseType)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getClassAxiom_ParseType()
   * @model containment="true"
   * @generated
   */
  ParseType getParseType();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.ClassAxiom#getParseType <em>Parse Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Parse Type</em>' containment reference.
   * @see #getParseType()
   * @generated
   */
  void setParseType(ParseType value);

} // ClassAxiom
