/**
 */
package org.smool.sdk.owlparser.owlmodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Owl Data Range</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OwlDataRange#getOwlRange <em>Owl Range</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OwlDataRange#getUnion <em>Union</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OwlDataRange#getOneOf <em>One Of</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OwlDataRange#getIntersection <em>Intersection</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlDataRange()
 * @model
 * @generated
 */
public interface OwlDataRange extends RDFSRange
{
  /**
   * Returns the value of the '<em><b>Owl Range</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Owl Range</em>' containment reference.
   * @see #setOwlRange(OwlDataRange)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlDataRange_OwlRange()
   * @model containment="true"
   * @generated
   */
  OwlDataRange getOwlRange();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.OwlDataRange#getOwlRange <em>Owl Range</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Owl Range</em>' containment reference.
   * @see #getOwlRange()
   * @generated
   */
  void setOwlRange(OwlDataRange value);

  /**
   * Returns the value of the '<em><b>Union</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Union</em>' containment reference.
   * @see #setUnion(UnionAxiom)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlDataRange_Union()
   * @model containment="true"
   * @generated
   */
  UnionAxiom getUnion();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.OwlDataRange#getUnion <em>Union</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Union</em>' containment reference.
   * @see #getUnion()
   * @generated
   */
  void setUnion(UnionAxiom value);

  /**
   * Returns the value of the '<em><b>One Of</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>One Of</em>' containment reference.
   * @see #setOneOf(OneOfAxiom)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlDataRange_OneOf()
   * @model containment="true"
   * @generated
   */
  OneOfAxiom getOneOf();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.OwlDataRange#getOneOf <em>One Of</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>One Of</em>' containment reference.
   * @see #getOneOf()
   * @generated
   */
  void setOneOf(OneOfAxiom value);

  /**
   * Returns the value of the '<em><b>Intersection</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Intersection</em>' containment reference.
   * @see #setIntersection(IntersectionAxiom)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlDataRange_Intersection()
   * @model containment="true"
   * @generated
   */
  IntersectionAxiom getIntersection();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.OwlDataRange#getIntersection <em>Intersection</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Intersection</em>' containment reference.
   * @see #getIntersection()
   * @generated
   */
  void setIntersection(IntersectionAxiom value);

} // OwlDataRange
