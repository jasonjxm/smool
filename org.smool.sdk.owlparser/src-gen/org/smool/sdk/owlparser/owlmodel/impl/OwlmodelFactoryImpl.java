/**
 */
package org.smool.sdk.owlparser.owlmodel.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.smool.sdk.owlparser.owlmodel.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OwlmodelFactoryImpl extends EFactoryImpl implements OwlmodelFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static OwlmodelFactory init()
  {
    try
    {
      OwlmodelFactory theOwlmodelFactory = (OwlmodelFactory)EPackage.Registry.INSTANCE.getEFactory(OwlmodelPackage.eNS_URI);
      if (theOwlmodelFactory != null)
      {
        return theOwlmodelFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new OwlmodelFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OwlmodelFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case OwlmodelPackage.OWL_MODEL: return createOWLModel();
      case OwlmodelPackage.DOC_TYPE: return createDocType();
      case OwlmodelPackage.DOC_ENTITY: return createDocEntity();
      case OwlmodelPackage.RDF_ELEMENT: return createRDFElement();
      case OwlmodelPackage.XML_NS: return createXmlNs();
      case OwlmodelPackage.OWL_REF: return createOwlRef();
      case OwlmodelPackage.OWL_ID: return createOwlID();
      case OwlmodelPackage.ONTOLOGY_ID: return createOntologyID();
      case OwlmodelPackage.PARSE_TYPE: return createParseType();
      case OwlmodelPackage.ONTOLOGY_ELEMENT: return createOntologyElement();
      case OwlmodelPackage.NAMED_INDIVIDUAL: return createNamedIndividual();
      case OwlmodelPackage.ONTOLOGY: return createOntology();
      case OwlmodelPackage.SEE_ALSO: return createSeeAlso();
      case OwlmodelPackage.PRIOR_VERSION: return createPriorVersion();
      case OwlmodelPackage.BACKWARDS_COMP: return createBackwardsComp();
      case OwlmodelPackage.OWL_IMPORT: return createOwlImport();
      case OwlmodelPackage.OWL_CLASS: return createOwlClass();
      case OwlmodelPackage.MINIMUM_CLASS: return createMinimumClass();
      case OwlmodelPackage.CLASS_AXIOM: return createClassAxiom();
      case OwlmodelPackage.COMPLEMENT_AXIOM: return createComplementAxiom();
      case OwlmodelPackage.INCOMPATIBLE_WITH_AXIOM: return createIncompatibleWithAxiom();
      case OwlmodelPackage.DISJOINT_AXIOM: return createDisjointAxiom();
      case OwlmodelPackage.EQUIVALENT_AXIOM: return createEquivalentAxiom();
      case OwlmodelPackage.UNION_AXIOM: return createUnionAxiom();
      case OwlmodelPackage.INTERSECTION_AXIOM: return createIntersectionAxiom();
      case OwlmodelPackage.ONE_OF_AXIOM: return createOneOfAxiom();
      case OwlmodelPackage.OWL_LABEL: return createOwlLabel();
      case OwlmodelPackage.OWL_VERSION: return createOwlVersion();
      case OwlmodelPackage.SUB_TYPE_OF: return createSubTypeOf();
      case OwlmodelPackage.CLASS_MODIFIER: return createClassModifier();
      case OwlmodelPackage.OWL_RESTRICTION: return createOwlRestriction();
      case OwlmodelPackage.RESTRICTION_BODY: return createRestrictionBody();
      case OwlmodelPackage.MIN_CARDINALITY: return createMinCardinality();
      case OwlmodelPackage.MAX_CARDINALITY: return createMaxCardinality();
      case OwlmodelPackage.CARDINALITY: return createCardinality();
      case OwlmodelPackage.QUALIFIED_CARDINALITY: return createQualifiedCardinality();
      case OwlmodelPackage.MIN_QUALIFIED_CARDINALITY: return createMinQualifiedCardinality();
      case OwlmodelPackage.MAX_QUALIFIED_CARDINALITY: return createMaxQualifiedCardinality();
      case OwlmodelPackage.SOME_VALUES: return createSomeValues();
      case OwlmodelPackage.ALL_VALUES: return createAllValues();
      case OwlmodelPackage.HAS_VALUE: return createHasValue();
      case OwlmodelPackage.PROPERTY: return createProperty();
      case OwlmodelPackage.ANNOTATION_PROPERTY: return createAnnotationProperty();
      case OwlmodelPackage.OBJECT_PROPERTY: return createObjectProperty();
      case OwlmodelPackage.DATATYPE_PROPERTY: return createDatatypeProperty();
      case OwlmodelPackage.FUNCTIONAL_PROPERTY: return createFunctionalProperty();
      case OwlmodelPackage.INVERSE_FUNCTIONAL_PROPERTY: return createInverseFunctionalProperty();
      case OwlmodelPackage.SYMMETRIC_PROPERTY: return createSymmetricProperty();
      case OwlmodelPackage.TRANSITIVE_PROPERTY: return createTransitiveProperty();
      case OwlmodelPackage.PROPERTY_MODIFIER: return createPropertyModifier();
      case OwlmodelPackage.INVERSE_TYPE: return createInverseType();
      case OwlmodelPackage.EQUIVALENT_TYPE: return createEquivalentType();
      case OwlmodelPackage.RDFS_DOMAIN: return createRDFSDomain();
      case OwlmodelPackage.RDFS_RANGE: return createRDFSRange();
      case OwlmodelPackage.OWL_DATA_RANGE: return createOwlDataRange();
      case OwlmodelPackage.RDF_TYPE: return createRDFType();
      case OwlmodelPackage.SUB_PROPERTY_OF: return createSubPropertyOf();
      case OwlmodelPackage.DESCRIPTION: return createDescription();
      case OwlmodelPackage.OWL_FIRST: return createOwlFirst();
      case OwlmodelPackage.RDF_REST: return createRDFRest();
      case OwlmodelPackage.OWL_TYPE: return createOwlType();
      case OwlmodelPackage.OWL_MEMBERS: return createOwlMembers();
      case OwlmodelPackage.DATATYPE: return createDatatype();
      case OwlmodelPackage.SIMPLE_RDFS_DOMAIN: return createSimpleRDFSDomain();
      case OwlmodelPackage.PARSED_RDFS_DOMAIN: return createParsedRDFSDomain();
      case OwlmodelPackage.SIMPLE_RDFS_RANGE: return createSimpleRDFSRange();
      case OwlmodelPackage.PARSED_PROPERTY_RANGE: return createParsedPropertyRange();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OWLModel createOWLModel()
  {
    OWLModelImpl owlModel = new OWLModelImpl();
    return owlModel;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public DocType createDocType()
  {
    DocTypeImpl docType = new DocTypeImpl();
    return docType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public DocEntity createDocEntity()
  {
    DocEntityImpl docEntity = new DocEntityImpl();
    return docEntity;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public RDFElement createRDFElement()
  {
    RDFElementImpl rdfElement = new RDFElementImpl();
    return rdfElement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public XmlNs createXmlNs()
  {
    XmlNsImpl xmlNs = new XmlNsImpl();
    return xmlNs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OwlRef createOwlRef()
  {
    OwlRefImpl owlRef = new OwlRefImpl();
    return owlRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OwlID createOwlID()
  {
    OwlIDImpl owlID = new OwlIDImpl();
    return owlID;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OntologyID createOntologyID()
  {
    OntologyIDImpl ontologyID = new OntologyIDImpl();
    return ontologyID;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public ParseType createParseType()
  {
    ParseTypeImpl parseType = new ParseTypeImpl();
    return parseType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OntologyElement createOntologyElement()
  {
    OntologyElementImpl ontologyElement = new OntologyElementImpl();
    return ontologyElement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NamedIndividual createNamedIndividual()
  {
    NamedIndividualImpl namedIndividual = new NamedIndividualImpl();
    return namedIndividual;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Ontology createOntology()
  {
    OntologyImpl ontology = new OntologyImpl();
    return ontology;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public SeeAlso createSeeAlso()
  {
    SeeAlsoImpl seeAlso = new SeeAlsoImpl();
    return seeAlso;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public PriorVersion createPriorVersion()
  {
    PriorVersionImpl priorVersion = new PriorVersionImpl();
    return priorVersion;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public BackwardsComp createBackwardsComp()
  {
    BackwardsCompImpl backwardsComp = new BackwardsCompImpl();
    return backwardsComp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OwlImport createOwlImport()
  {
    OwlImportImpl owlImport = new OwlImportImpl();
    return owlImport;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OwlClass createOwlClass()
  {
    OwlClassImpl owlClass = new OwlClassImpl();
    return owlClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public MinimumClass createMinimumClass()
  {
    MinimumClassImpl minimumClass = new MinimumClassImpl();
    return minimumClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public ClassAxiom createClassAxiom()
  {
    ClassAxiomImpl classAxiom = new ClassAxiomImpl();
    return classAxiom;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public ComplementAxiom createComplementAxiom()
  {
    ComplementAxiomImpl complementAxiom = new ComplementAxiomImpl();
    return complementAxiom;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public IncompatibleWithAxiom createIncompatibleWithAxiom()
  {
    IncompatibleWithAxiomImpl incompatibleWithAxiom = new IncompatibleWithAxiomImpl();
    return incompatibleWithAxiom;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public DisjointAxiom createDisjointAxiom()
  {
    DisjointAxiomImpl disjointAxiom = new DisjointAxiomImpl();
    return disjointAxiom;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EquivalentAxiom createEquivalentAxiom()
  {
    EquivalentAxiomImpl equivalentAxiom = new EquivalentAxiomImpl();
    return equivalentAxiom;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public UnionAxiom createUnionAxiom()
  {
    UnionAxiomImpl unionAxiom = new UnionAxiomImpl();
    return unionAxiom;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public IntersectionAxiom createIntersectionAxiom()
  {
    IntersectionAxiomImpl intersectionAxiom = new IntersectionAxiomImpl();
    return intersectionAxiom;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OneOfAxiom createOneOfAxiom()
  {
    OneOfAxiomImpl oneOfAxiom = new OneOfAxiomImpl();
    return oneOfAxiom;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OwlLabel createOwlLabel()
  {
    OwlLabelImpl owlLabel = new OwlLabelImpl();
    return owlLabel;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OwlVersion createOwlVersion()
  {
    OwlVersionImpl owlVersion = new OwlVersionImpl();
    return owlVersion;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public SubTypeOf createSubTypeOf()
  {
    SubTypeOfImpl subTypeOf = new SubTypeOfImpl();
    return subTypeOf;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public ClassModifier createClassModifier()
  {
    ClassModifierImpl classModifier = new ClassModifierImpl();
    return classModifier;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OwlRestriction createOwlRestriction()
  {
    OwlRestrictionImpl owlRestriction = new OwlRestrictionImpl();
    return owlRestriction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public RestrictionBody createRestrictionBody()
  {
    RestrictionBodyImpl restrictionBody = new RestrictionBodyImpl();
    return restrictionBody;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public MinCardinality createMinCardinality()
  {
    MinCardinalityImpl minCardinality = new MinCardinalityImpl();
    return minCardinality;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public MaxCardinality createMaxCardinality()
  {
    MaxCardinalityImpl maxCardinality = new MaxCardinalityImpl();
    return maxCardinality;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Cardinality createCardinality()
  {
    CardinalityImpl cardinality = new CardinalityImpl();
    return cardinality;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public QualifiedCardinality createQualifiedCardinality()
  {
    QualifiedCardinalityImpl qualifiedCardinality = new QualifiedCardinalityImpl();
    return qualifiedCardinality;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public MinQualifiedCardinality createMinQualifiedCardinality()
  {
    MinQualifiedCardinalityImpl minQualifiedCardinality = new MinQualifiedCardinalityImpl();
    return minQualifiedCardinality;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public MaxQualifiedCardinality createMaxQualifiedCardinality()
  {
    MaxQualifiedCardinalityImpl maxQualifiedCardinality = new MaxQualifiedCardinalityImpl();
    return maxQualifiedCardinality;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public SomeValues createSomeValues()
  {
    SomeValuesImpl someValues = new SomeValuesImpl();
    return someValues;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public AllValues createAllValues()
  {
    AllValuesImpl allValues = new AllValuesImpl();
    return allValues;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public HasValue createHasValue()
  {
    HasValueImpl hasValue = new HasValueImpl();
    return hasValue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Property createProperty()
  {
    PropertyImpl property = new PropertyImpl();
    return property;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public AnnotationProperty createAnnotationProperty()
  {
    AnnotationPropertyImpl annotationProperty = new AnnotationPropertyImpl();
    return annotationProperty;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public ObjectProperty createObjectProperty()
  {
    ObjectPropertyImpl objectProperty = new ObjectPropertyImpl();
    return objectProperty;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public DatatypeProperty createDatatypeProperty()
  {
    DatatypePropertyImpl datatypeProperty = new DatatypePropertyImpl();
    return datatypeProperty;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public FunctionalProperty createFunctionalProperty()
  {
    FunctionalPropertyImpl functionalProperty = new FunctionalPropertyImpl();
    return functionalProperty;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public InverseFunctionalProperty createInverseFunctionalProperty()
  {
    InverseFunctionalPropertyImpl inverseFunctionalProperty = new InverseFunctionalPropertyImpl();
    return inverseFunctionalProperty;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public SymmetricProperty createSymmetricProperty()
  {
    SymmetricPropertyImpl symmetricProperty = new SymmetricPropertyImpl();
    return symmetricProperty;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public TransitiveProperty createTransitiveProperty()
  {
    TransitivePropertyImpl transitiveProperty = new TransitivePropertyImpl();
    return transitiveProperty;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public PropertyModifier createPropertyModifier()
  {
    PropertyModifierImpl propertyModifier = new PropertyModifierImpl();
    return propertyModifier;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public InverseType createInverseType()
  {
    InverseTypeImpl inverseType = new InverseTypeImpl();
    return inverseType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EquivalentType createEquivalentType()
  {
    EquivalentTypeImpl equivalentType = new EquivalentTypeImpl();
    return equivalentType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public RDFSDomain createRDFSDomain()
  {
    RDFSDomainImpl rdfsDomain = new RDFSDomainImpl();
    return rdfsDomain;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public RDFSRange createRDFSRange()
  {
    RDFSRangeImpl rdfsRange = new RDFSRangeImpl();
    return rdfsRange;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OwlDataRange createOwlDataRange()
  {
    OwlDataRangeImpl owlDataRange = new OwlDataRangeImpl();
    return owlDataRange;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public RDFType createRDFType()
  {
    RDFTypeImpl rdfType = new RDFTypeImpl();
    return rdfType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public SubPropertyOf createSubPropertyOf()
  {
    SubPropertyOfImpl subPropertyOf = new SubPropertyOfImpl();
    return subPropertyOf;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Description createDescription()
  {
    DescriptionImpl description = new DescriptionImpl();
    return description;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OwlFirst createOwlFirst()
  {
    OwlFirstImpl owlFirst = new OwlFirstImpl();
    return owlFirst;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public RDFRest createRDFRest()
  {
    RDFRestImpl rdfRest = new RDFRestImpl();
    return rdfRest;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OwlType createOwlType()
  {
    OwlTypeImpl owlType = new OwlTypeImpl();
    return owlType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OwlMembers createOwlMembers()
  {
    OwlMembersImpl owlMembers = new OwlMembersImpl();
    return owlMembers;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Datatype createDatatype()
  {
    DatatypeImpl datatype = new DatatypeImpl();
    return datatype;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public SimpleRDFSDomain createSimpleRDFSDomain()
  {
    SimpleRDFSDomainImpl simpleRDFSDomain = new SimpleRDFSDomainImpl();
    return simpleRDFSDomain;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public ParsedRDFSDomain createParsedRDFSDomain()
  {
    ParsedRDFSDomainImpl parsedRDFSDomain = new ParsedRDFSDomainImpl();
    return parsedRDFSDomain;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public SimpleRDFSRange createSimpleRDFSRange()
  {
    SimpleRDFSRangeImpl simpleRDFSRange = new SimpleRDFSRangeImpl();
    return simpleRDFSRange;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public ParsedPropertyRange createParsedPropertyRange()
  {
    ParsedPropertyRangeImpl parsedPropertyRange = new ParsedPropertyRangeImpl();
    return parsedPropertyRange;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OwlmodelPackage getOwlmodelPackage()
  {
    return (OwlmodelPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static OwlmodelPackage getPackage()
  {
    return OwlmodelPackage.eINSTANCE;
  }

} //OwlmodelFactoryImpl
