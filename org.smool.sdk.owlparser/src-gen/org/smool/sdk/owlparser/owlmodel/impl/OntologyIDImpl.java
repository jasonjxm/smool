/**
 */
package org.smool.sdk.owlparser.owlmodel.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.smool.sdk.owlparser.owlmodel.OntologyID;
import org.smool.sdk.owlparser.owlmodel.OwlmodelPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ontology ID</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OntologyIDImpl#getSuperOnt <em>Super Ont</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OntologyIDImpl#getOntURI <em>Ont URI</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OntologyIDImpl extends MinimalEObjectImpl.Container implements OntologyID
{
  /**
   * The default value of the '{@link #getSuperOnt() <em>Super Ont</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSuperOnt()
   * @generated
   * @ordered
   */
  protected static final String SUPER_ONT_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getSuperOnt() <em>Super Ont</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSuperOnt()
   * @generated
   * @ordered
   */
  protected String superOnt = SUPER_ONT_EDEFAULT;

  /**
   * The default value of the '{@link #getOntURI() <em>Ont URI</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOntURI()
   * @generated
   * @ordered
   */
  protected static final String ONT_URI_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getOntURI() <em>Ont URI</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOntURI()
   * @generated
   * @ordered
   */
  protected String ontURI = ONT_URI_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected OntologyIDImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return OwlmodelPackage.Literals.ONTOLOGY_ID;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getSuperOnt()
  {
    return superOnt;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setSuperOnt(String newSuperOnt)
  {
    String oldSuperOnt = superOnt;
    superOnt = newSuperOnt;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.ONTOLOGY_ID__SUPER_ONT, oldSuperOnt, superOnt));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getOntURI()
  {
    return ontURI;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setOntURI(String newOntURI)
  {
    String oldOntURI = ontURI;
    ontURI = newOntURI;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.ONTOLOGY_ID__ONT_URI, oldOntURI, ontURI));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case OwlmodelPackage.ONTOLOGY_ID__SUPER_ONT:
        return getSuperOnt();
      case OwlmodelPackage.ONTOLOGY_ID__ONT_URI:
        return getOntURI();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case OwlmodelPackage.ONTOLOGY_ID__SUPER_ONT:
        setSuperOnt((String)newValue);
        return;
      case OwlmodelPackage.ONTOLOGY_ID__ONT_URI:
        setOntURI((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.ONTOLOGY_ID__SUPER_ONT:
        setSuperOnt(SUPER_ONT_EDEFAULT);
        return;
      case OwlmodelPackage.ONTOLOGY_ID__ONT_URI:
        setOntURI(ONT_URI_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.ONTOLOGY_ID__SUPER_ONT:
        return SUPER_ONT_EDEFAULT == null ? superOnt != null : !SUPER_ONT_EDEFAULT.equals(superOnt);
      case OwlmodelPackage.ONTOLOGY_ID__ONT_URI:
        return ONT_URI_EDEFAULT == null ? ontURI != null : !ONT_URI_EDEFAULT.equals(ontURI);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (superOnt: ");
    result.append(superOnt);
    result.append(", ontURI: ");
    result.append(ontURI);
    result.append(')');
    return result.toString();
  }

} //OntologyIDImpl
