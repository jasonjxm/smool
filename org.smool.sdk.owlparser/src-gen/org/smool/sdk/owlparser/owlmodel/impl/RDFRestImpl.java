/**
 */
package org.smool.sdk.owlparser.owlmodel.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.smool.sdk.owlparser.owlmodel.OwlFirst;
import org.smool.sdk.owlparser.owlmodel.OwlRef;
import org.smool.sdk.owlparser.owlmodel.OwlmodelPackage;
import org.smool.sdk.owlparser.owlmodel.ParseType;
import org.smool.sdk.owlparser.owlmodel.RDFRest;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>RDF Rest</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.RDFRestImpl#getRef <em>Ref</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.RDFRestImpl#getParseType <em>Parse Type</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.RDFRestImpl#getFirst <em>First</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.RDFRestImpl#getRest <em>Rest</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RDFRestImpl extends MinimalEObjectImpl.Container implements RDFRest
{
  /**
   * The cached value of the '{@link #getRef() <em>Ref</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRef()
   * @generated
   * @ordered
   */
  protected OwlRef ref;

  /**
   * The cached value of the '{@link #getParseType() <em>Parse Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParseType()
   * @generated
   * @ordered
   */
  protected ParseType parseType;

  /**
   * The cached value of the '{@link #getFirst() <em>First</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFirst()
   * @generated
   * @ordered
   */
  protected EList<OwlFirst> first;

  /**
   * The cached value of the '{@link #getRest() <em>Rest</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRest()
   * @generated
   * @ordered
   */
  protected EList<RDFRest> rest;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected RDFRestImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return OwlmodelPackage.Literals.RDF_REST;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OwlRef getRef()
  {
    return ref;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetRef(OwlRef newRef, NotificationChain msgs)
  {
    OwlRef oldRef = ref;
    ref = newRef;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlmodelPackage.RDF_REST__REF, oldRef, newRef);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setRef(OwlRef newRef)
  {
    if (newRef != ref)
    {
      NotificationChain msgs = null;
      if (ref != null)
        msgs = ((InternalEObject)ref).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.RDF_REST__REF, null, msgs);
      if (newRef != null)
        msgs = ((InternalEObject)newRef).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.RDF_REST__REF, null, msgs);
      msgs = basicSetRef(newRef, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.RDF_REST__REF, newRef, newRef));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public ParseType getParseType()
  {
    return parseType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetParseType(ParseType newParseType, NotificationChain msgs)
  {
    ParseType oldParseType = parseType;
    parseType = newParseType;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlmodelPackage.RDF_REST__PARSE_TYPE, oldParseType, newParseType);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setParseType(ParseType newParseType)
  {
    if (newParseType != parseType)
    {
      NotificationChain msgs = null;
      if (parseType != null)
        msgs = ((InternalEObject)parseType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.RDF_REST__PARSE_TYPE, null, msgs);
      if (newParseType != null)
        msgs = ((InternalEObject)newParseType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.RDF_REST__PARSE_TYPE, null, msgs);
      msgs = basicSetParseType(newParseType, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.RDF_REST__PARSE_TYPE, newParseType, newParseType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<OwlFirst> getFirst()
  {
    if (first == null)
    {
      first = new EObjectContainmentEList<OwlFirst>(OwlFirst.class, this, OwlmodelPackage.RDF_REST__FIRST);
    }
    return first;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<RDFRest> getRest()
  {
    if (rest == null)
    {
      rest = new EObjectContainmentEList<RDFRest>(RDFRest.class, this, OwlmodelPackage.RDF_REST__REST);
    }
    return rest;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case OwlmodelPackage.RDF_REST__REF:
        return basicSetRef(null, msgs);
      case OwlmodelPackage.RDF_REST__PARSE_TYPE:
        return basicSetParseType(null, msgs);
      case OwlmodelPackage.RDF_REST__FIRST:
        return ((InternalEList<?>)getFirst()).basicRemove(otherEnd, msgs);
      case OwlmodelPackage.RDF_REST__REST:
        return ((InternalEList<?>)getRest()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case OwlmodelPackage.RDF_REST__REF:
        return getRef();
      case OwlmodelPackage.RDF_REST__PARSE_TYPE:
        return getParseType();
      case OwlmodelPackage.RDF_REST__FIRST:
        return getFirst();
      case OwlmodelPackage.RDF_REST__REST:
        return getRest();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case OwlmodelPackage.RDF_REST__REF:
        setRef((OwlRef)newValue);
        return;
      case OwlmodelPackage.RDF_REST__PARSE_TYPE:
        setParseType((ParseType)newValue);
        return;
      case OwlmodelPackage.RDF_REST__FIRST:
        getFirst().clear();
        getFirst().addAll((Collection<? extends OwlFirst>)newValue);
        return;
      case OwlmodelPackage.RDF_REST__REST:
        getRest().clear();
        getRest().addAll((Collection<? extends RDFRest>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.RDF_REST__REF:
        setRef((OwlRef)null);
        return;
      case OwlmodelPackage.RDF_REST__PARSE_TYPE:
        setParseType((ParseType)null);
        return;
      case OwlmodelPackage.RDF_REST__FIRST:
        getFirst().clear();
        return;
      case OwlmodelPackage.RDF_REST__REST:
        getRest().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.RDF_REST__REF:
        return ref != null;
      case OwlmodelPackage.RDF_REST__PARSE_TYPE:
        return parseType != null;
      case OwlmodelPackage.RDF_REST__FIRST:
        return first != null && !first.isEmpty();
      case OwlmodelPackage.RDF_REST__REST:
        return rest != null && !rest.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //RDFRestImpl
