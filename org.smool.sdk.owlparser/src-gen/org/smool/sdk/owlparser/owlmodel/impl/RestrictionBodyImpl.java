/**
 */
package org.smool.sdk.owlparser.owlmodel.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.smool.sdk.owlparser.owlmodel.OwlmodelPackage;
import org.smool.sdk.owlparser.owlmodel.RestrictionBody;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Restriction Body</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RestrictionBodyImpl extends MinimalEObjectImpl.Container implements RestrictionBody
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected RestrictionBodyImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return OwlmodelPackage.Literals.RESTRICTION_BODY;
  }

} //RestrictionBodyImpl
