/**
 */
package org.smool.sdk.owlparser.owlmodel.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.smool.sdk.owlparser.owlmodel.DocEntity;
import org.smool.sdk.owlparser.owlmodel.OwlmodelPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Doc Entity</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.DocEntityImpl#getEntityID <em>Entity ID</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.DocEntityImpl#getEntityURI <em>Entity URI</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DocEntityImpl extends MinimalEObjectImpl.Container implements DocEntity
{
  /**
   * The default value of the '{@link #getEntityID() <em>Entity ID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEntityID()
   * @generated
   * @ordered
   */
  protected static final String ENTITY_ID_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getEntityID() <em>Entity ID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEntityID()
   * @generated
   * @ordered
   */
  protected String entityID = ENTITY_ID_EDEFAULT;

  /**
   * The default value of the '{@link #getEntityURI() <em>Entity URI</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEntityURI()
   * @generated
   * @ordered
   */
  protected static final String ENTITY_URI_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getEntityURI() <em>Entity URI</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEntityURI()
   * @generated
   * @ordered
   */
  protected String entityURI = ENTITY_URI_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected DocEntityImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return OwlmodelPackage.Literals.DOC_ENTITY;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getEntityID()
  {
    return entityID;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setEntityID(String newEntityID)
  {
    String oldEntityID = entityID;
    entityID = newEntityID;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.DOC_ENTITY__ENTITY_ID, oldEntityID, entityID));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getEntityURI()
  {
    return entityURI;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setEntityURI(String newEntityURI)
  {
    String oldEntityURI = entityURI;
    entityURI = newEntityURI;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.DOC_ENTITY__ENTITY_URI, oldEntityURI, entityURI));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case OwlmodelPackage.DOC_ENTITY__ENTITY_ID:
        return getEntityID();
      case OwlmodelPackage.DOC_ENTITY__ENTITY_URI:
        return getEntityURI();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case OwlmodelPackage.DOC_ENTITY__ENTITY_ID:
        setEntityID((String)newValue);
        return;
      case OwlmodelPackage.DOC_ENTITY__ENTITY_URI:
        setEntityURI((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.DOC_ENTITY__ENTITY_ID:
        setEntityID(ENTITY_ID_EDEFAULT);
        return;
      case OwlmodelPackage.DOC_ENTITY__ENTITY_URI:
        setEntityURI(ENTITY_URI_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.DOC_ENTITY__ENTITY_ID:
        return ENTITY_ID_EDEFAULT == null ? entityID != null : !ENTITY_ID_EDEFAULT.equals(entityID);
      case OwlmodelPackage.DOC_ENTITY__ENTITY_URI:
        return ENTITY_URI_EDEFAULT == null ? entityURI != null : !ENTITY_URI_EDEFAULT.equals(entityURI);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (entityID: ");
    result.append(entityID);
    result.append(", entityURI: ");
    result.append(entityURI);
    result.append(')');
    return result.toString();
  }

} //DocEntityImpl
