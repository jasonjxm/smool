/**
 */
package org.smool.sdk.owlparser.owlmodel.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.smool.sdk.owlparser.owlmodel.Ontology;
import org.smool.sdk.owlparser.owlmodel.OntologyID;
import org.smool.sdk.owlparser.owlmodel.OwlImport;
import org.smool.sdk.owlparser.owlmodel.OwlmodelPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Owl Import</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OwlImportImpl#getRef <em>Ref</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OwlImportImpl#getOnts <em>Onts</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OwlImportImpl extends MinimalEObjectImpl.Container implements OwlImport
{
  /**
   * The cached value of the '{@link #getRef() <em>Ref</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRef()
   * @generated
   * @ordered
   */
  protected OntologyID ref;

  /**
   * The cached value of the '{@link #getOnts() <em>Onts</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOnts()
   * @generated
   * @ordered
   */
  protected EList<Ontology> onts;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected OwlImportImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return OwlmodelPackage.Literals.OWL_IMPORT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OntologyID getRef()
  {
    return ref;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetRef(OntologyID newRef, NotificationChain msgs)
  {
    OntologyID oldRef = ref;
    ref = newRef;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_IMPORT__REF, oldRef, newRef);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setRef(OntologyID newRef)
  {
    if (newRef != ref)
    {
      NotificationChain msgs = null;
      if (ref != null)
        msgs = ((InternalEObject)ref).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.OWL_IMPORT__REF, null, msgs);
      if (newRef != null)
        msgs = ((InternalEObject)newRef).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.OWL_IMPORT__REF, null, msgs);
      msgs = basicSetRef(newRef, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_IMPORT__REF, newRef, newRef));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<Ontology> getOnts()
  {
    if (onts == null)
    {
      onts = new EObjectContainmentEList<Ontology>(Ontology.class, this, OwlmodelPackage.OWL_IMPORT__ONTS);
    }
    return onts;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_IMPORT__REF:
        return basicSetRef(null, msgs);
      case OwlmodelPackage.OWL_IMPORT__ONTS:
        return ((InternalEList<?>)getOnts()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_IMPORT__REF:
        return getRef();
      case OwlmodelPackage.OWL_IMPORT__ONTS:
        return getOnts();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_IMPORT__REF:
        setRef((OntologyID)newValue);
        return;
      case OwlmodelPackage.OWL_IMPORT__ONTS:
        getOnts().clear();
        getOnts().addAll((Collection<? extends Ontology>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_IMPORT__REF:
        setRef((OntologyID)null);
        return;
      case OwlmodelPackage.OWL_IMPORT__ONTS:
        getOnts().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_IMPORT__REF:
        return ref != null;
      case OwlmodelPackage.OWL_IMPORT__ONTS:
        return onts != null && !onts.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //OwlImportImpl
