/**
 */
package org.smool.sdk.owlparser.owlmodel.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.smool.sdk.owlparser.owlmodel.ClassModifier;
import org.smool.sdk.owlparser.owlmodel.OwlRef;
import org.smool.sdk.owlparser.owlmodel.OwlRestriction;
import org.smool.sdk.owlparser.owlmodel.OwlmodelPackage;
import org.smool.sdk.owlparser.owlmodel.Property;
import org.smool.sdk.owlparser.owlmodel.RestrictionBody;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Owl Restriction</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OwlRestrictionImpl#getRestrictionBodies <em>Restriction Bodies</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OwlRestrictionImpl#getProp <em>Prop</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OwlRestrictionImpl#getProperty <em>Property</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OwlRestrictionImpl#getClass_ <em>Class</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OwlRestrictionImpl#getClasses <em>Classes</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OwlRestrictionImpl extends ClassModifierImpl implements OwlRestriction
{
  /**
   * The cached value of the '{@link #getRestrictionBodies() <em>Restriction Bodies</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRestrictionBodies()
   * @generated
   * @ordered
   */
  protected EList<RestrictionBody> restrictionBodies;

  /**
   * The cached value of the '{@link #getProp() <em>Prop</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getProp()
   * @generated
   * @ordered
   */
  protected OwlRef prop;

  /**
   * The cached value of the '{@link #getProperty() <em>Property</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getProperty()
   * @generated
   * @ordered
   */
  protected Property property;

  /**
   * The cached value of the '{@link #getClass_() <em>Class</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getClass_()
   * @generated
   * @ordered
   */
  protected OwlRef class_;

  /**
   * The cached value of the '{@link #getClasses() <em>Classes</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getClasses()
   * @generated
   * @ordered
   */
  protected EList<ClassModifier> classes;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected OwlRestrictionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return OwlmodelPackage.Literals.OWL_RESTRICTION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<RestrictionBody> getRestrictionBodies()
  {
    if (restrictionBodies == null)
    {
      restrictionBodies = new EObjectContainmentEList<RestrictionBody>(RestrictionBody.class, this, OwlmodelPackage.OWL_RESTRICTION__RESTRICTION_BODIES);
    }
    return restrictionBodies;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OwlRef getProp()
  {
    return prop;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetProp(OwlRef newProp, NotificationChain msgs)
  {
    OwlRef oldProp = prop;
    prop = newProp;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_RESTRICTION__PROP, oldProp, newProp);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setProp(OwlRef newProp)
  {
    if (newProp != prop)
    {
      NotificationChain msgs = null;
      if (prop != null)
        msgs = ((InternalEObject)prop).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.OWL_RESTRICTION__PROP, null, msgs);
      if (newProp != null)
        msgs = ((InternalEObject)newProp).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.OWL_RESTRICTION__PROP, null, msgs);
      msgs = basicSetProp(newProp, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_RESTRICTION__PROP, newProp, newProp));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Property getProperty()
  {
    return property;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetProperty(Property newProperty, NotificationChain msgs)
  {
    Property oldProperty = property;
    property = newProperty;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_RESTRICTION__PROPERTY, oldProperty, newProperty);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setProperty(Property newProperty)
  {
    if (newProperty != property)
    {
      NotificationChain msgs = null;
      if (property != null)
        msgs = ((InternalEObject)property).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.OWL_RESTRICTION__PROPERTY, null, msgs);
      if (newProperty != null)
        msgs = ((InternalEObject)newProperty).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.OWL_RESTRICTION__PROPERTY, null, msgs);
      msgs = basicSetProperty(newProperty, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_RESTRICTION__PROPERTY, newProperty, newProperty));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OwlRef getClass_()
  {
    return class_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetClass(OwlRef newClass, NotificationChain msgs)
  {
    OwlRef oldClass = class_;
    class_ = newClass;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_RESTRICTION__CLASS, oldClass, newClass);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setClass(OwlRef newClass)
  {
    if (newClass != class_)
    {
      NotificationChain msgs = null;
      if (class_ != null)
        msgs = ((InternalEObject)class_).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.OWL_RESTRICTION__CLASS, null, msgs);
      if (newClass != null)
        msgs = ((InternalEObject)newClass).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.OWL_RESTRICTION__CLASS, null, msgs);
      msgs = basicSetClass(newClass, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_RESTRICTION__CLASS, newClass, newClass));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<ClassModifier> getClasses()
  {
    if (classes == null)
    {
      classes = new EObjectContainmentEList<ClassModifier>(ClassModifier.class, this, OwlmodelPackage.OWL_RESTRICTION__CLASSES);
    }
    return classes;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_RESTRICTION__RESTRICTION_BODIES:
        return ((InternalEList<?>)getRestrictionBodies()).basicRemove(otherEnd, msgs);
      case OwlmodelPackage.OWL_RESTRICTION__PROP:
        return basicSetProp(null, msgs);
      case OwlmodelPackage.OWL_RESTRICTION__PROPERTY:
        return basicSetProperty(null, msgs);
      case OwlmodelPackage.OWL_RESTRICTION__CLASS:
        return basicSetClass(null, msgs);
      case OwlmodelPackage.OWL_RESTRICTION__CLASSES:
        return ((InternalEList<?>)getClasses()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_RESTRICTION__RESTRICTION_BODIES:
        return getRestrictionBodies();
      case OwlmodelPackage.OWL_RESTRICTION__PROP:
        return getProp();
      case OwlmodelPackage.OWL_RESTRICTION__PROPERTY:
        return getProperty();
      case OwlmodelPackage.OWL_RESTRICTION__CLASS:
        return getClass_();
      case OwlmodelPackage.OWL_RESTRICTION__CLASSES:
        return getClasses();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_RESTRICTION__RESTRICTION_BODIES:
        getRestrictionBodies().clear();
        getRestrictionBodies().addAll((Collection<? extends RestrictionBody>)newValue);
        return;
      case OwlmodelPackage.OWL_RESTRICTION__PROP:
        setProp((OwlRef)newValue);
        return;
      case OwlmodelPackage.OWL_RESTRICTION__PROPERTY:
        setProperty((Property)newValue);
        return;
      case OwlmodelPackage.OWL_RESTRICTION__CLASS:
        setClass((OwlRef)newValue);
        return;
      case OwlmodelPackage.OWL_RESTRICTION__CLASSES:
        getClasses().clear();
        getClasses().addAll((Collection<? extends ClassModifier>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_RESTRICTION__RESTRICTION_BODIES:
        getRestrictionBodies().clear();
        return;
      case OwlmodelPackage.OWL_RESTRICTION__PROP:
        setProp((OwlRef)null);
        return;
      case OwlmodelPackage.OWL_RESTRICTION__PROPERTY:
        setProperty((Property)null);
        return;
      case OwlmodelPackage.OWL_RESTRICTION__CLASS:
        setClass((OwlRef)null);
        return;
      case OwlmodelPackage.OWL_RESTRICTION__CLASSES:
        getClasses().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_RESTRICTION__RESTRICTION_BODIES:
        return restrictionBodies != null && !restrictionBodies.isEmpty();
      case OwlmodelPackage.OWL_RESTRICTION__PROP:
        return prop != null;
      case OwlmodelPackage.OWL_RESTRICTION__PROPERTY:
        return property != null;
      case OwlmodelPackage.OWL_RESTRICTION__CLASS:
        return class_ != null;
      case OwlmodelPackage.OWL_RESTRICTION__CLASSES:
        return classes != null && !classes.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //OwlRestrictionImpl
