/**
 */
package org.smool.sdk.owlparser.owlmodel.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.smool.sdk.owlparser.owlmodel.DocType;
import org.smool.sdk.owlparser.owlmodel.OWLModel;
import org.smool.sdk.owlparser.owlmodel.OwlmodelPackage;
import org.smool.sdk.owlparser.owlmodel.RDFElement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>OWL Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OWLModelImpl#getHeader <em>Header</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OWLModelImpl#getDocType <em>Doc Type</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OWLModelImpl#getRdf <em>Rdf</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OWLModelImpl extends MinimalEObjectImpl.Container implements OWLModel
{
  /**
   * The default value of the '{@link #getHeader() <em>Header</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getHeader()
   * @generated
   * @ordered
   */
  protected static final String HEADER_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getHeader() <em>Header</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getHeader()
   * @generated
   * @ordered
   */
  protected String header = HEADER_EDEFAULT;

  /**
   * The cached value of the '{@link #getDocType() <em>Doc Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDocType()
   * @generated
   * @ordered
   */
  protected DocType docType;

  /**
   * The cached value of the '{@link #getRdf() <em>Rdf</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRdf()
   * @generated
   * @ordered
   */
  protected RDFElement rdf;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected OWLModelImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return OwlmodelPackage.Literals.OWL_MODEL;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getHeader()
  {
    return header;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setHeader(String newHeader)
  {
    String oldHeader = header;
    header = newHeader;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_MODEL__HEADER, oldHeader, header));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public DocType getDocType()
  {
    return docType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDocType(DocType newDocType, NotificationChain msgs)
  {
    DocType oldDocType = docType;
    docType = newDocType;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_MODEL__DOC_TYPE, oldDocType, newDocType);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setDocType(DocType newDocType)
  {
    if (newDocType != docType)
    {
      NotificationChain msgs = null;
      if (docType != null)
        msgs = ((InternalEObject)docType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.OWL_MODEL__DOC_TYPE, null, msgs);
      if (newDocType != null)
        msgs = ((InternalEObject)newDocType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.OWL_MODEL__DOC_TYPE, null, msgs);
      msgs = basicSetDocType(newDocType, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_MODEL__DOC_TYPE, newDocType, newDocType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public RDFElement getRdf()
  {
    return rdf;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetRdf(RDFElement newRdf, NotificationChain msgs)
  {
    RDFElement oldRdf = rdf;
    rdf = newRdf;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_MODEL__RDF, oldRdf, newRdf);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setRdf(RDFElement newRdf)
  {
    if (newRdf != rdf)
    {
      NotificationChain msgs = null;
      if (rdf != null)
        msgs = ((InternalEObject)rdf).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.OWL_MODEL__RDF, null, msgs);
      if (newRdf != null)
        msgs = ((InternalEObject)newRdf).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.OWL_MODEL__RDF, null, msgs);
      msgs = basicSetRdf(newRdf, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_MODEL__RDF, newRdf, newRdf));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_MODEL__DOC_TYPE:
        return basicSetDocType(null, msgs);
      case OwlmodelPackage.OWL_MODEL__RDF:
        return basicSetRdf(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_MODEL__HEADER:
        return getHeader();
      case OwlmodelPackage.OWL_MODEL__DOC_TYPE:
        return getDocType();
      case OwlmodelPackage.OWL_MODEL__RDF:
        return getRdf();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_MODEL__HEADER:
        setHeader((String)newValue);
        return;
      case OwlmodelPackage.OWL_MODEL__DOC_TYPE:
        setDocType((DocType)newValue);
        return;
      case OwlmodelPackage.OWL_MODEL__RDF:
        setRdf((RDFElement)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_MODEL__HEADER:
        setHeader(HEADER_EDEFAULT);
        return;
      case OwlmodelPackage.OWL_MODEL__DOC_TYPE:
        setDocType((DocType)null);
        return;
      case OwlmodelPackage.OWL_MODEL__RDF:
        setRdf((RDFElement)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_MODEL__HEADER:
        return HEADER_EDEFAULT == null ? header != null : !HEADER_EDEFAULT.equals(header);
      case OwlmodelPackage.OWL_MODEL__DOC_TYPE:
        return docType != null;
      case OwlmodelPackage.OWL_MODEL__RDF:
        return rdf != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (header: ");
    result.append(header);
    result.append(')');
    return result.toString();
  }

} //OWLModelImpl
