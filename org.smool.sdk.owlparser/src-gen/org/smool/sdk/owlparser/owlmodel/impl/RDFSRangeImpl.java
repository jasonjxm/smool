/**
 */
package org.smool.sdk.owlparser.owlmodel.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.smool.sdk.owlparser.owlmodel.OwlmodelPackage;
import org.smool.sdk.owlparser.owlmodel.RDFSRange;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>RDFS Range</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RDFSRangeImpl extends MinimalEObjectImpl.Container implements RDFSRange
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected RDFSRangeImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return OwlmodelPackage.Literals.RDFS_RANGE;
  }

} //RDFSRangeImpl
