/**
 */
package org.smool.sdk.owlparser.owlmodel.impl;

import org.eclipse.emf.ecore.EClass;

import org.smool.sdk.owlparser.owlmodel.FunctionalProperty;
import org.smool.sdk.owlparser.owlmodel.OwlmodelPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Functional Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FunctionalPropertyImpl extends PropertyImpl implements FunctionalProperty
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected FunctionalPropertyImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return OwlmodelPackage.Literals.FUNCTIONAL_PROPERTY;
  }

} //FunctionalPropertyImpl
