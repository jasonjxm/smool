/**
 */
package org.smool.sdk.owlparser.owlmodel.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.smool.sdk.owlparser.owlmodel.OwlmodelPackage;
import org.smool.sdk.owlparser.owlmodel.XmlNs;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Xml Ns</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.XmlNsImpl#getNsid <em>Nsid</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.XmlNsImpl#getNsuri <em>Nsuri</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.XmlNsImpl#getNsname <em>Nsname</em>}</li>
 * </ul>
 *
 * @generated
 */
public class XmlNsImpl extends MinimalEObjectImpl.Container implements XmlNs
{
  /**
   * The default value of the '{@link #getNsid() <em>Nsid</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNsid()
   * @generated
   * @ordered
   */
  protected static final String NSID_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getNsid() <em>Nsid</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNsid()
   * @generated
   * @ordered
   */
  protected String nsid = NSID_EDEFAULT;

  /**
   * The default value of the '{@link #getNsuri() <em>Nsuri</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNsuri()
   * @generated
   * @ordered
   */
  protected static final String NSURI_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getNsuri() <em>Nsuri</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNsuri()
   * @generated
   * @ordered
   */
  protected String nsuri = NSURI_EDEFAULT;

  /**
   * The default value of the '{@link #getNsname() <em>Nsname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNsname()
   * @generated
   * @ordered
   */
  protected static final String NSNAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getNsname() <em>Nsname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNsname()
   * @generated
   * @ordered
   */
  protected String nsname = NSNAME_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected XmlNsImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return OwlmodelPackage.Literals.XML_NS;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getNsid()
  {
    return nsid;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setNsid(String newNsid)
  {
    String oldNsid = nsid;
    nsid = newNsid;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.XML_NS__NSID, oldNsid, nsid));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getNsuri()
  {
    return nsuri;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setNsuri(String newNsuri)
  {
    String oldNsuri = nsuri;
    nsuri = newNsuri;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.XML_NS__NSURI, oldNsuri, nsuri));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getNsname()
  {
    return nsname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setNsname(String newNsname)
  {
    String oldNsname = nsname;
    nsname = newNsname;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.XML_NS__NSNAME, oldNsname, nsname));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case OwlmodelPackage.XML_NS__NSID:
        return getNsid();
      case OwlmodelPackage.XML_NS__NSURI:
        return getNsuri();
      case OwlmodelPackage.XML_NS__NSNAME:
        return getNsname();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case OwlmodelPackage.XML_NS__NSID:
        setNsid((String)newValue);
        return;
      case OwlmodelPackage.XML_NS__NSURI:
        setNsuri((String)newValue);
        return;
      case OwlmodelPackage.XML_NS__NSNAME:
        setNsname((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.XML_NS__NSID:
        setNsid(NSID_EDEFAULT);
        return;
      case OwlmodelPackage.XML_NS__NSURI:
        setNsuri(NSURI_EDEFAULT);
        return;
      case OwlmodelPackage.XML_NS__NSNAME:
        setNsname(NSNAME_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.XML_NS__NSID:
        return NSID_EDEFAULT == null ? nsid != null : !NSID_EDEFAULT.equals(nsid);
      case OwlmodelPackage.XML_NS__NSURI:
        return NSURI_EDEFAULT == null ? nsuri != null : !NSURI_EDEFAULT.equals(nsuri);
      case OwlmodelPackage.XML_NS__NSNAME:
        return NSNAME_EDEFAULT == null ? nsname != null : !NSNAME_EDEFAULT.equals(nsname);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (nsid: ");
    result.append(nsid);
    result.append(", nsuri: ");
    result.append(nsuri);
    result.append(", nsname: ");
    result.append(nsname);
    result.append(')');
    return result.toString();
  }

} //XmlNsImpl
