/**
 */
package org.smool.sdk.owlparser.owlmodel.impl;

import org.eclipse.emf.ecore.EClass;

import org.smool.sdk.owlparser.owlmodel.OwlmodelPackage;
import org.smool.sdk.owlparser.owlmodel.TransitiveProperty;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Transitive Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TransitivePropertyImpl extends PropertyImpl implements TransitiveProperty
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TransitivePropertyImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return OwlmodelPackage.Literals.TRANSITIVE_PROPERTY;
  }

} //TransitivePropertyImpl
