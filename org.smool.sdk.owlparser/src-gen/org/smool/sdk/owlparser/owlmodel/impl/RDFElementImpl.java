/**
 */
package org.smool.sdk.owlparser.owlmodel.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.smool.sdk.owlparser.owlmodel.OntologyElement;
import org.smool.sdk.owlparser.owlmodel.OwlmodelPackage;
import org.smool.sdk.owlparser.owlmodel.RDFElement;
import org.smool.sdk.owlparser.owlmodel.XmlNs;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>RDF Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.RDFElementImpl#getImportedNs <em>Imported Ns</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.RDFElementImpl#getBaseOnt <em>Base Ont</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.RDFElementImpl#getOntologyElements <em>Ontology Elements</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RDFElementImpl extends MinimalEObjectImpl.Container implements RDFElement
{
  /**
   * The cached value of the '{@link #getImportedNs() <em>Imported Ns</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getImportedNs()
   * @generated
   * @ordered
   */
  protected EList<XmlNs> importedNs;

  /**
   * The default value of the '{@link #getBaseOnt() <em>Base Ont</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBaseOnt()
   * @generated
   * @ordered
   */
  protected static final String BASE_ONT_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBaseOnt() <em>Base Ont</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBaseOnt()
   * @generated
   * @ordered
   */
  protected String baseOnt = BASE_ONT_EDEFAULT;

  /**
   * The cached value of the '{@link #getOntologyElements() <em>Ontology Elements</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOntologyElements()
   * @generated
   * @ordered
   */
  protected EList<OntologyElement> ontologyElements;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected RDFElementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return OwlmodelPackage.Literals.RDF_ELEMENT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<XmlNs> getImportedNs()
  {
    if (importedNs == null)
    {
      importedNs = new EObjectContainmentEList<XmlNs>(XmlNs.class, this, OwlmodelPackage.RDF_ELEMENT__IMPORTED_NS);
    }
    return importedNs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getBaseOnt()
  {
    return baseOnt;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setBaseOnt(String newBaseOnt)
  {
    String oldBaseOnt = baseOnt;
    baseOnt = newBaseOnt;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.RDF_ELEMENT__BASE_ONT, oldBaseOnt, baseOnt));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<OntologyElement> getOntologyElements()
  {
    if (ontologyElements == null)
    {
      ontologyElements = new EObjectContainmentEList<OntologyElement>(OntologyElement.class, this, OwlmodelPackage.RDF_ELEMENT__ONTOLOGY_ELEMENTS);
    }
    return ontologyElements;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case OwlmodelPackage.RDF_ELEMENT__IMPORTED_NS:
        return ((InternalEList<?>)getImportedNs()).basicRemove(otherEnd, msgs);
      case OwlmodelPackage.RDF_ELEMENT__ONTOLOGY_ELEMENTS:
        return ((InternalEList<?>)getOntologyElements()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case OwlmodelPackage.RDF_ELEMENT__IMPORTED_NS:
        return getImportedNs();
      case OwlmodelPackage.RDF_ELEMENT__BASE_ONT:
        return getBaseOnt();
      case OwlmodelPackage.RDF_ELEMENT__ONTOLOGY_ELEMENTS:
        return getOntologyElements();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case OwlmodelPackage.RDF_ELEMENT__IMPORTED_NS:
        getImportedNs().clear();
        getImportedNs().addAll((Collection<? extends XmlNs>)newValue);
        return;
      case OwlmodelPackage.RDF_ELEMENT__BASE_ONT:
        setBaseOnt((String)newValue);
        return;
      case OwlmodelPackage.RDF_ELEMENT__ONTOLOGY_ELEMENTS:
        getOntologyElements().clear();
        getOntologyElements().addAll((Collection<? extends OntologyElement>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.RDF_ELEMENT__IMPORTED_NS:
        getImportedNs().clear();
        return;
      case OwlmodelPackage.RDF_ELEMENT__BASE_ONT:
        setBaseOnt(BASE_ONT_EDEFAULT);
        return;
      case OwlmodelPackage.RDF_ELEMENT__ONTOLOGY_ELEMENTS:
        getOntologyElements().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.RDF_ELEMENT__IMPORTED_NS:
        return importedNs != null && !importedNs.isEmpty();
      case OwlmodelPackage.RDF_ELEMENT__BASE_ONT:
        return BASE_ONT_EDEFAULT == null ? baseOnt != null : !BASE_ONT_EDEFAULT.equals(baseOnt);
      case OwlmodelPackage.RDF_ELEMENT__ONTOLOGY_ELEMENTS:
        return ontologyElements != null && !ontologyElements.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (baseOnt: ");
    result.append(baseOnt);
    result.append(')');
    return result.toString();
  }

} //RDFElementImpl
