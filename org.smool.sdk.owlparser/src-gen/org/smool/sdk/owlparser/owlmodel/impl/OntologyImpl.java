/**
 */
package org.smool.sdk.owlparser.owlmodel.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.smool.sdk.owlparser.owlmodel.BackwardsComp;
import org.smool.sdk.owlparser.owlmodel.Ontology;
import org.smool.sdk.owlparser.owlmodel.OntologyID;
import org.smool.sdk.owlparser.owlmodel.OwlImport;
import org.smool.sdk.owlparser.owlmodel.OwlLabel;
import org.smool.sdk.owlparser.owlmodel.OwlRef;
import org.smool.sdk.owlparser.owlmodel.OwlVersion;
import org.smool.sdk.owlparser.owlmodel.OwlmodelPackage;
import org.smool.sdk.owlparser.owlmodel.PriorVersion;
import org.smool.sdk.owlparser.owlmodel.SeeAlso;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ontology</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OntologyImpl#getAbout <em>About</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OntologyImpl#getResource <em>Resource</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OntologyImpl#getLabels <em>Labels</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OntologyImpl#getVersionInfo <em>Version Info</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OntologyImpl#getSeeAlso <em>See Also</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OntologyImpl#getPVers <em>PVers</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OntologyImpl#getBackComp <em>Back Comp</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OntologyImpl#getImportedOntologies <em>Imported Ontologies</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OntologyImpl#getOthers <em>Others</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OntologyImpl extends OntologyElementImpl implements Ontology
{
  /**
   * The cached value of the '{@link #getAbout() <em>About</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAbout()
   * @generated
   * @ordered
   */
  protected OntologyID about;

  /**
   * The cached value of the '{@link #getResource() <em>Resource</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getResource()
   * @generated
   * @ordered
   */
  protected OwlRef resource;

  /**
   * The cached value of the '{@link #getLabels() <em>Labels</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLabels()
   * @generated
   * @ordered
   */
  protected EList<OwlLabel> labels;

  /**
   * The cached value of the '{@link #getVersionInfo() <em>Version Info</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVersionInfo()
   * @generated
   * @ordered
   */
  protected EList<OwlVersion> versionInfo;

  /**
   * The cached value of the '{@link #getSeeAlso() <em>See Also</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSeeAlso()
   * @generated
   * @ordered
   */
  protected EList<SeeAlso> seeAlso;

  /**
   * The cached value of the '{@link #getPVers() <em>PVers</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPVers()
   * @generated
   * @ordered
   */
  protected EList<PriorVersion> pVers;

  /**
   * The cached value of the '{@link #getBackComp() <em>Back Comp</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBackComp()
   * @generated
   * @ordered
   */
  protected EList<BackwardsComp> backComp;

  /**
   * The cached value of the '{@link #getImportedOntologies() <em>Imported Ontologies</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getImportedOntologies()
   * @generated
   * @ordered
   */
  protected EList<OwlImport> importedOntologies;

  /**
   * The cached value of the '{@link #getOthers() <em>Others</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOthers()
   * @generated
   * @ordered
   */
  protected EList<String> others;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected OntologyImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return OwlmodelPackage.Literals.ONTOLOGY;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OntologyID getAbout()
  {
    return about;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAbout(OntologyID newAbout, NotificationChain msgs)
  {
    OntologyID oldAbout = about;
    about = newAbout;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlmodelPackage.ONTOLOGY__ABOUT, oldAbout, newAbout);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setAbout(OntologyID newAbout)
  {
    if (newAbout != about)
    {
      NotificationChain msgs = null;
      if (about != null)
        msgs = ((InternalEObject)about).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.ONTOLOGY__ABOUT, null, msgs);
      if (newAbout != null)
        msgs = ((InternalEObject)newAbout).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.ONTOLOGY__ABOUT, null, msgs);
      msgs = basicSetAbout(newAbout, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.ONTOLOGY__ABOUT, newAbout, newAbout));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OwlRef getResource()
  {
    return resource;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetResource(OwlRef newResource, NotificationChain msgs)
  {
    OwlRef oldResource = resource;
    resource = newResource;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlmodelPackage.ONTOLOGY__RESOURCE, oldResource, newResource);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setResource(OwlRef newResource)
  {
    if (newResource != resource)
    {
      NotificationChain msgs = null;
      if (resource != null)
        msgs = ((InternalEObject)resource).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.ONTOLOGY__RESOURCE, null, msgs);
      if (newResource != null)
        msgs = ((InternalEObject)newResource).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.ONTOLOGY__RESOURCE, null, msgs);
      msgs = basicSetResource(newResource, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.ONTOLOGY__RESOURCE, newResource, newResource));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<OwlLabel> getLabels()
  {
    if (labels == null)
    {
      labels = new EObjectContainmentEList<OwlLabel>(OwlLabel.class, this, OwlmodelPackage.ONTOLOGY__LABELS);
    }
    return labels;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<OwlVersion> getVersionInfo()
  {
    if (versionInfo == null)
    {
      versionInfo = new EObjectContainmentEList<OwlVersion>(OwlVersion.class, this, OwlmodelPackage.ONTOLOGY__VERSION_INFO);
    }
    return versionInfo;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<SeeAlso> getSeeAlso()
  {
    if (seeAlso == null)
    {
      seeAlso = new EObjectContainmentEList<SeeAlso>(SeeAlso.class, this, OwlmodelPackage.ONTOLOGY__SEE_ALSO);
    }
    return seeAlso;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<PriorVersion> getPVers()
  {
    if (pVers == null)
    {
      pVers = new EObjectContainmentEList<PriorVersion>(PriorVersion.class, this, OwlmodelPackage.ONTOLOGY__PVERS);
    }
    return pVers;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<BackwardsComp> getBackComp()
  {
    if (backComp == null)
    {
      backComp = new EObjectContainmentEList<BackwardsComp>(BackwardsComp.class, this, OwlmodelPackage.ONTOLOGY__BACK_COMP);
    }
    return backComp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<OwlImport> getImportedOntologies()
  {
    if (importedOntologies == null)
    {
      importedOntologies = new EObjectContainmentEList<OwlImport>(OwlImport.class, this, OwlmodelPackage.ONTOLOGY__IMPORTED_ONTOLOGIES);
    }
    return importedOntologies;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<String> getOthers()
  {
    if (others == null)
    {
      others = new EDataTypeEList<String>(String.class, this, OwlmodelPackage.ONTOLOGY__OTHERS);
    }
    return others;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case OwlmodelPackage.ONTOLOGY__ABOUT:
        return basicSetAbout(null, msgs);
      case OwlmodelPackage.ONTOLOGY__RESOURCE:
        return basicSetResource(null, msgs);
      case OwlmodelPackage.ONTOLOGY__LABELS:
        return ((InternalEList<?>)getLabels()).basicRemove(otherEnd, msgs);
      case OwlmodelPackage.ONTOLOGY__VERSION_INFO:
        return ((InternalEList<?>)getVersionInfo()).basicRemove(otherEnd, msgs);
      case OwlmodelPackage.ONTOLOGY__SEE_ALSO:
        return ((InternalEList<?>)getSeeAlso()).basicRemove(otherEnd, msgs);
      case OwlmodelPackage.ONTOLOGY__PVERS:
        return ((InternalEList<?>)getPVers()).basicRemove(otherEnd, msgs);
      case OwlmodelPackage.ONTOLOGY__BACK_COMP:
        return ((InternalEList<?>)getBackComp()).basicRemove(otherEnd, msgs);
      case OwlmodelPackage.ONTOLOGY__IMPORTED_ONTOLOGIES:
        return ((InternalEList<?>)getImportedOntologies()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case OwlmodelPackage.ONTOLOGY__ABOUT:
        return getAbout();
      case OwlmodelPackage.ONTOLOGY__RESOURCE:
        return getResource();
      case OwlmodelPackage.ONTOLOGY__LABELS:
        return getLabels();
      case OwlmodelPackage.ONTOLOGY__VERSION_INFO:
        return getVersionInfo();
      case OwlmodelPackage.ONTOLOGY__SEE_ALSO:
        return getSeeAlso();
      case OwlmodelPackage.ONTOLOGY__PVERS:
        return getPVers();
      case OwlmodelPackage.ONTOLOGY__BACK_COMP:
        return getBackComp();
      case OwlmodelPackage.ONTOLOGY__IMPORTED_ONTOLOGIES:
        return getImportedOntologies();
      case OwlmodelPackage.ONTOLOGY__OTHERS:
        return getOthers();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case OwlmodelPackage.ONTOLOGY__ABOUT:
        setAbout((OntologyID)newValue);
        return;
      case OwlmodelPackage.ONTOLOGY__RESOURCE:
        setResource((OwlRef)newValue);
        return;
      case OwlmodelPackage.ONTOLOGY__LABELS:
        getLabels().clear();
        getLabels().addAll((Collection<? extends OwlLabel>)newValue);
        return;
      case OwlmodelPackage.ONTOLOGY__VERSION_INFO:
        getVersionInfo().clear();
        getVersionInfo().addAll((Collection<? extends OwlVersion>)newValue);
        return;
      case OwlmodelPackage.ONTOLOGY__SEE_ALSO:
        getSeeAlso().clear();
        getSeeAlso().addAll((Collection<? extends SeeAlso>)newValue);
        return;
      case OwlmodelPackage.ONTOLOGY__PVERS:
        getPVers().clear();
        getPVers().addAll((Collection<? extends PriorVersion>)newValue);
        return;
      case OwlmodelPackage.ONTOLOGY__BACK_COMP:
        getBackComp().clear();
        getBackComp().addAll((Collection<? extends BackwardsComp>)newValue);
        return;
      case OwlmodelPackage.ONTOLOGY__IMPORTED_ONTOLOGIES:
        getImportedOntologies().clear();
        getImportedOntologies().addAll((Collection<? extends OwlImport>)newValue);
        return;
      case OwlmodelPackage.ONTOLOGY__OTHERS:
        getOthers().clear();
        getOthers().addAll((Collection<? extends String>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.ONTOLOGY__ABOUT:
        setAbout((OntologyID)null);
        return;
      case OwlmodelPackage.ONTOLOGY__RESOURCE:
        setResource((OwlRef)null);
        return;
      case OwlmodelPackage.ONTOLOGY__LABELS:
        getLabels().clear();
        return;
      case OwlmodelPackage.ONTOLOGY__VERSION_INFO:
        getVersionInfo().clear();
        return;
      case OwlmodelPackage.ONTOLOGY__SEE_ALSO:
        getSeeAlso().clear();
        return;
      case OwlmodelPackage.ONTOLOGY__PVERS:
        getPVers().clear();
        return;
      case OwlmodelPackage.ONTOLOGY__BACK_COMP:
        getBackComp().clear();
        return;
      case OwlmodelPackage.ONTOLOGY__IMPORTED_ONTOLOGIES:
        getImportedOntologies().clear();
        return;
      case OwlmodelPackage.ONTOLOGY__OTHERS:
        getOthers().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.ONTOLOGY__ABOUT:
        return about != null;
      case OwlmodelPackage.ONTOLOGY__RESOURCE:
        return resource != null;
      case OwlmodelPackage.ONTOLOGY__LABELS:
        return labels != null && !labels.isEmpty();
      case OwlmodelPackage.ONTOLOGY__VERSION_INFO:
        return versionInfo != null && !versionInfo.isEmpty();
      case OwlmodelPackage.ONTOLOGY__SEE_ALSO:
        return seeAlso != null && !seeAlso.isEmpty();
      case OwlmodelPackage.ONTOLOGY__PVERS:
        return pVers != null && !pVers.isEmpty();
      case OwlmodelPackage.ONTOLOGY__BACK_COMP:
        return backComp != null && !backComp.isEmpty();
      case OwlmodelPackage.ONTOLOGY__IMPORTED_ONTOLOGIES:
        return importedOntologies != null && !importedOntologies.isEmpty();
      case OwlmodelPackage.ONTOLOGY__OTHERS:
        return others != null && !others.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (others: ");
    result.append(others);
    result.append(')');
    return result.toString();
  }

} //OntologyImpl
