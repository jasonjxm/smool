/**
 */
package org.smool.sdk.owlparser.owlmodel.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.smool.sdk.owlparser.owlmodel.ClassAxiom;
import org.smool.sdk.owlparser.owlmodel.MinimumClass;
import org.smool.sdk.owlparser.owlmodel.OwlLabel;
import org.smool.sdk.owlparser.owlmodel.OwlmodelPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Minimum Class</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.MinimumClassImpl#getLabels <em>Labels</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.MinimumClassImpl#getAxioms <em>Axioms</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.MinimumClassImpl#getOthers <em>Others</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MinimumClassImpl extends ClassModifierImpl implements MinimumClass
{
  /**
   * The cached value of the '{@link #getLabels() <em>Labels</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLabels()
   * @generated
   * @ordered
   */
  protected EList<OwlLabel> labels;

  /**
   * The cached value of the '{@link #getAxioms() <em>Axioms</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAxioms()
   * @generated
   * @ordered
   */
  protected EList<ClassAxiom> axioms;

  /**
   * The cached value of the '{@link #getOthers() <em>Others</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOthers()
   * @generated
   * @ordered
   */
  protected EList<String> others;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected MinimumClassImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return OwlmodelPackage.Literals.MINIMUM_CLASS;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<OwlLabel> getLabels()
  {
    if (labels == null)
    {
      labels = new EObjectContainmentEList<OwlLabel>(OwlLabel.class, this, OwlmodelPackage.MINIMUM_CLASS__LABELS);
    }
    return labels;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<ClassAxiom> getAxioms()
  {
    if (axioms == null)
    {
      axioms = new EObjectContainmentEList<ClassAxiom>(ClassAxiom.class, this, OwlmodelPackage.MINIMUM_CLASS__AXIOMS);
    }
    return axioms;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<String> getOthers()
  {
    if (others == null)
    {
      others = new EDataTypeEList<String>(String.class, this, OwlmodelPackage.MINIMUM_CLASS__OTHERS);
    }
    return others;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case OwlmodelPackage.MINIMUM_CLASS__LABELS:
        return ((InternalEList<?>)getLabels()).basicRemove(otherEnd, msgs);
      case OwlmodelPackage.MINIMUM_CLASS__AXIOMS:
        return ((InternalEList<?>)getAxioms()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case OwlmodelPackage.MINIMUM_CLASS__LABELS:
        return getLabels();
      case OwlmodelPackage.MINIMUM_CLASS__AXIOMS:
        return getAxioms();
      case OwlmodelPackage.MINIMUM_CLASS__OTHERS:
        return getOthers();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case OwlmodelPackage.MINIMUM_CLASS__LABELS:
        getLabels().clear();
        getLabels().addAll((Collection<? extends OwlLabel>)newValue);
        return;
      case OwlmodelPackage.MINIMUM_CLASS__AXIOMS:
        getAxioms().clear();
        getAxioms().addAll((Collection<? extends ClassAxiom>)newValue);
        return;
      case OwlmodelPackage.MINIMUM_CLASS__OTHERS:
        getOthers().clear();
        getOthers().addAll((Collection<? extends String>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.MINIMUM_CLASS__LABELS:
        getLabels().clear();
        return;
      case OwlmodelPackage.MINIMUM_CLASS__AXIOMS:
        getAxioms().clear();
        return;
      case OwlmodelPackage.MINIMUM_CLASS__OTHERS:
        getOthers().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.MINIMUM_CLASS__LABELS:
        return labels != null && !labels.isEmpty();
      case OwlmodelPackage.MINIMUM_CLASS__AXIOMS:
        return axioms != null && !axioms.isEmpty();
      case OwlmodelPackage.MINIMUM_CLASS__OTHERS:
        return others != null && !others.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (others: ");
    result.append(others);
    result.append(')');
    return result.toString();
  }

} //MinimumClassImpl
