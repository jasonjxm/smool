/**
 */
package org.smool.sdk.owlparser.owlmodel.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.smool.sdk.owlparser.owlmodel.Description;
import org.smool.sdk.owlparser.owlmodel.OwlMembers;
import org.smool.sdk.owlparser.owlmodel.OwlmodelPackage;
import org.smool.sdk.owlparser.owlmodel.ParseType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Owl Members</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OwlMembersImpl#getParseType <em>Parse Type</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OwlMembersImpl#getDescriptions <em>Descriptions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OwlMembersImpl extends MinimalEObjectImpl.Container implements OwlMembers
{
  /**
   * The cached value of the '{@link #getParseType() <em>Parse Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParseType()
   * @generated
   * @ordered
   */
  protected ParseType parseType;

  /**
   * The cached value of the '{@link #getDescriptions() <em>Descriptions</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDescriptions()
   * @generated
   * @ordered
   */
  protected EList<Description> descriptions;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected OwlMembersImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return OwlmodelPackage.Literals.OWL_MEMBERS;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public ParseType getParseType()
  {
    return parseType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetParseType(ParseType newParseType, NotificationChain msgs)
  {
    ParseType oldParseType = parseType;
    parseType = newParseType;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_MEMBERS__PARSE_TYPE, oldParseType, newParseType);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setParseType(ParseType newParseType)
  {
    if (newParseType != parseType)
    {
      NotificationChain msgs = null;
      if (parseType != null)
        msgs = ((InternalEObject)parseType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.OWL_MEMBERS__PARSE_TYPE, null, msgs);
      if (newParseType != null)
        msgs = ((InternalEObject)newParseType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.OWL_MEMBERS__PARSE_TYPE, null, msgs);
      msgs = basicSetParseType(newParseType, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_MEMBERS__PARSE_TYPE, newParseType, newParseType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<Description> getDescriptions()
  {
    if (descriptions == null)
    {
      descriptions = new EObjectContainmentEList<Description>(Description.class, this, OwlmodelPackage.OWL_MEMBERS__DESCRIPTIONS);
    }
    return descriptions;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_MEMBERS__PARSE_TYPE:
        return basicSetParseType(null, msgs);
      case OwlmodelPackage.OWL_MEMBERS__DESCRIPTIONS:
        return ((InternalEList<?>)getDescriptions()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_MEMBERS__PARSE_TYPE:
        return getParseType();
      case OwlmodelPackage.OWL_MEMBERS__DESCRIPTIONS:
        return getDescriptions();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_MEMBERS__PARSE_TYPE:
        setParseType((ParseType)newValue);
        return;
      case OwlmodelPackage.OWL_MEMBERS__DESCRIPTIONS:
        getDescriptions().clear();
        getDescriptions().addAll((Collection<? extends Description>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_MEMBERS__PARSE_TYPE:
        setParseType((ParseType)null);
        return;
      case OwlmodelPackage.OWL_MEMBERS__DESCRIPTIONS:
        getDescriptions().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_MEMBERS__PARSE_TYPE:
        return parseType != null;
      case OwlmodelPackage.OWL_MEMBERS__DESCRIPTIONS:
        return descriptions != null && !descriptions.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //OwlMembersImpl
