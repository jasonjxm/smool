/**
 */
package org.smool.sdk.owlparser.owlmodel.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.smool.sdk.owlparser.owlmodel.ClassModifier;
import org.smool.sdk.owlparser.owlmodel.OwlmodelPackage;
import org.smool.sdk.owlparser.owlmodel.ParseType;
import org.smool.sdk.owlparser.owlmodel.ParsedPropertyRange;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Parsed Property Range</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.ParsedPropertyRangeImpl#getParseType <em>Parse Type</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.ParsedPropertyRangeImpl#getModifiers <em>Modifiers</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ParsedPropertyRangeImpl extends RDFSRangeImpl implements ParsedPropertyRange
{
  /**
   * The cached value of the '{@link #getParseType() <em>Parse Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParseType()
   * @generated
   * @ordered
   */
  protected ParseType parseType;

  /**
   * The cached value of the '{@link #getModifiers() <em>Modifiers</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getModifiers()
   * @generated
   * @ordered
   */
  protected EList<ClassModifier> modifiers;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ParsedPropertyRangeImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return OwlmodelPackage.Literals.PARSED_PROPERTY_RANGE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public ParseType getParseType()
  {
    return parseType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetParseType(ParseType newParseType, NotificationChain msgs)
  {
    ParseType oldParseType = parseType;
    parseType = newParseType;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlmodelPackage.PARSED_PROPERTY_RANGE__PARSE_TYPE, oldParseType, newParseType);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setParseType(ParseType newParseType)
  {
    if (newParseType != parseType)
    {
      NotificationChain msgs = null;
      if (parseType != null)
        msgs = ((InternalEObject)parseType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.PARSED_PROPERTY_RANGE__PARSE_TYPE, null, msgs);
      if (newParseType != null)
        msgs = ((InternalEObject)newParseType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.PARSED_PROPERTY_RANGE__PARSE_TYPE, null, msgs);
      msgs = basicSetParseType(newParseType, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.PARSED_PROPERTY_RANGE__PARSE_TYPE, newParseType, newParseType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<ClassModifier> getModifiers()
  {
    if (modifiers == null)
    {
      modifiers = new EObjectContainmentEList<ClassModifier>(ClassModifier.class, this, OwlmodelPackage.PARSED_PROPERTY_RANGE__MODIFIERS);
    }
    return modifiers;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case OwlmodelPackage.PARSED_PROPERTY_RANGE__PARSE_TYPE:
        return basicSetParseType(null, msgs);
      case OwlmodelPackage.PARSED_PROPERTY_RANGE__MODIFIERS:
        return ((InternalEList<?>)getModifiers()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case OwlmodelPackage.PARSED_PROPERTY_RANGE__PARSE_TYPE:
        return getParseType();
      case OwlmodelPackage.PARSED_PROPERTY_RANGE__MODIFIERS:
        return getModifiers();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case OwlmodelPackage.PARSED_PROPERTY_RANGE__PARSE_TYPE:
        setParseType((ParseType)newValue);
        return;
      case OwlmodelPackage.PARSED_PROPERTY_RANGE__MODIFIERS:
        getModifiers().clear();
        getModifiers().addAll((Collection<? extends ClassModifier>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.PARSED_PROPERTY_RANGE__PARSE_TYPE:
        setParseType((ParseType)null);
        return;
      case OwlmodelPackage.PARSED_PROPERTY_RANGE__MODIFIERS:
        getModifiers().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.PARSED_PROPERTY_RANGE__PARSE_TYPE:
        return parseType != null;
      case OwlmodelPackage.PARSED_PROPERTY_RANGE__MODIFIERS:
        return modifiers != null && !modifiers.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //ParsedPropertyRangeImpl
