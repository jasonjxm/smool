/**
 */
package org.smool.sdk.owlparser.owlmodel.impl;

import org.eclipse.emf.ecore.EClass;

import org.smool.sdk.owlparser.owlmodel.OwlmodelPackage;
import org.smool.sdk.owlparser.owlmodel.SymmetricProperty;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Symmetric Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SymmetricPropertyImpl extends PropertyImpl implements SymmetricProperty
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SymmetricPropertyImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return OwlmodelPackage.Literals.SYMMETRIC_PROPERTY;
  }

} //SymmetricPropertyImpl
