/**
 */
package org.smool.sdk.owlparser.owlmodel.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.smool.sdk.owlparser.owlmodel.IntersectionAxiom;
import org.smool.sdk.owlparser.owlmodel.OneOfAxiom;
import org.smool.sdk.owlparser.owlmodel.OwlDataRange;
import org.smool.sdk.owlparser.owlmodel.OwlmodelPackage;
import org.smool.sdk.owlparser.owlmodel.UnionAxiom;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Owl Data Range</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OwlDataRangeImpl#getOwlRange <em>Owl Range</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OwlDataRangeImpl#getUnion <em>Union</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OwlDataRangeImpl#getOneOf <em>One Of</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OwlDataRangeImpl#getIntersection <em>Intersection</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OwlDataRangeImpl extends RDFSRangeImpl implements OwlDataRange
{
  /**
   * The cached value of the '{@link #getOwlRange() <em>Owl Range</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOwlRange()
   * @generated
   * @ordered
   */
  protected OwlDataRange owlRange;

  /**
   * The cached value of the '{@link #getUnion() <em>Union</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUnion()
   * @generated
   * @ordered
   */
  protected UnionAxiom union;

  /**
   * The cached value of the '{@link #getOneOf() <em>One Of</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOneOf()
   * @generated
   * @ordered
   */
  protected OneOfAxiom oneOf;

  /**
   * The cached value of the '{@link #getIntersection() <em>Intersection</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIntersection()
   * @generated
   * @ordered
   */
  protected IntersectionAxiom intersection;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected OwlDataRangeImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return OwlmodelPackage.Literals.OWL_DATA_RANGE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OwlDataRange getOwlRange()
  {
    return owlRange;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetOwlRange(OwlDataRange newOwlRange, NotificationChain msgs)
  {
    OwlDataRange oldOwlRange = owlRange;
    owlRange = newOwlRange;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_DATA_RANGE__OWL_RANGE, oldOwlRange, newOwlRange);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setOwlRange(OwlDataRange newOwlRange)
  {
    if (newOwlRange != owlRange)
    {
      NotificationChain msgs = null;
      if (owlRange != null)
        msgs = ((InternalEObject)owlRange).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.OWL_DATA_RANGE__OWL_RANGE, null, msgs);
      if (newOwlRange != null)
        msgs = ((InternalEObject)newOwlRange).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.OWL_DATA_RANGE__OWL_RANGE, null, msgs);
      msgs = basicSetOwlRange(newOwlRange, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_DATA_RANGE__OWL_RANGE, newOwlRange, newOwlRange));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public UnionAxiom getUnion()
  {
    return union;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetUnion(UnionAxiom newUnion, NotificationChain msgs)
  {
    UnionAxiom oldUnion = union;
    union = newUnion;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_DATA_RANGE__UNION, oldUnion, newUnion);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setUnion(UnionAxiom newUnion)
  {
    if (newUnion != union)
    {
      NotificationChain msgs = null;
      if (union != null)
        msgs = ((InternalEObject)union).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.OWL_DATA_RANGE__UNION, null, msgs);
      if (newUnion != null)
        msgs = ((InternalEObject)newUnion).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.OWL_DATA_RANGE__UNION, null, msgs);
      msgs = basicSetUnion(newUnion, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_DATA_RANGE__UNION, newUnion, newUnion));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OneOfAxiom getOneOf()
  {
    return oneOf;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetOneOf(OneOfAxiom newOneOf, NotificationChain msgs)
  {
    OneOfAxiom oldOneOf = oneOf;
    oneOf = newOneOf;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_DATA_RANGE__ONE_OF, oldOneOf, newOneOf);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setOneOf(OneOfAxiom newOneOf)
  {
    if (newOneOf != oneOf)
    {
      NotificationChain msgs = null;
      if (oneOf != null)
        msgs = ((InternalEObject)oneOf).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.OWL_DATA_RANGE__ONE_OF, null, msgs);
      if (newOneOf != null)
        msgs = ((InternalEObject)newOneOf).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.OWL_DATA_RANGE__ONE_OF, null, msgs);
      msgs = basicSetOneOf(newOneOf, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_DATA_RANGE__ONE_OF, newOneOf, newOneOf));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public IntersectionAxiom getIntersection()
  {
    return intersection;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetIntersection(IntersectionAxiom newIntersection, NotificationChain msgs)
  {
    IntersectionAxiom oldIntersection = intersection;
    intersection = newIntersection;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_DATA_RANGE__INTERSECTION, oldIntersection, newIntersection);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setIntersection(IntersectionAxiom newIntersection)
  {
    if (newIntersection != intersection)
    {
      NotificationChain msgs = null;
      if (intersection != null)
        msgs = ((InternalEObject)intersection).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.OWL_DATA_RANGE__INTERSECTION, null, msgs);
      if (newIntersection != null)
        msgs = ((InternalEObject)newIntersection).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.OWL_DATA_RANGE__INTERSECTION, null, msgs);
      msgs = basicSetIntersection(newIntersection, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_DATA_RANGE__INTERSECTION, newIntersection, newIntersection));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_DATA_RANGE__OWL_RANGE:
        return basicSetOwlRange(null, msgs);
      case OwlmodelPackage.OWL_DATA_RANGE__UNION:
        return basicSetUnion(null, msgs);
      case OwlmodelPackage.OWL_DATA_RANGE__ONE_OF:
        return basicSetOneOf(null, msgs);
      case OwlmodelPackage.OWL_DATA_RANGE__INTERSECTION:
        return basicSetIntersection(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_DATA_RANGE__OWL_RANGE:
        return getOwlRange();
      case OwlmodelPackage.OWL_DATA_RANGE__UNION:
        return getUnion();
      case OwlmodelPackage.OWL_DATA_RANGE__ONE_OF:
        return getOneOf();
      case OwlmodelPackage.OWL_DATA_RANGE__INTERSECTION:
        return getIntersection();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_DATA_RANGE__OWL_RANGE:
        setOwlRange((OwlDataRange)newValue);
        return;
      case OwlmodelPackage.OWL_DATA_RANGE__UNION:
        setUnion((UnionAxiom)newValue);
        return;
      case OwlmodelPackage.OWL_DATA_RANGE__ONE_OF:
        setOneOf((OneOfAxiom)newValue);
        return;
      case OwlmodelPackage.OWL_DATA_RANGE__INTERSECTION:
        setIntersection((IntersectionAxiom)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_DATA_RANGE__OWL_RANGE:
        setOwlRange((OwlDataRange)null);
        return;
      case OwlmodelPackage.OWL_DATA_RANGE__UNION:
        setUnion((UnionAxiom)null);
        return;
      case OwlmodelPackage.OWL_DATA_RANGE__ONE_OF:
        setOneOf((OneOfAxiom)null);
        return;
      case OwlmodelPackage.OWL_DATA_RANGE__INTERSECTION:
        setIntersection((IntersectionAxiom)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_DATA_RANGE__OWL_RANGE:
        return owlRange != null;
      case OwlmodelPackage.OWL_DATA_RANGE__UNION:
        return union != null;
      case OwlmodelPackage.OWL_DATA_RANGE__ONE_OF:
        return oneOf != null;
      case OwlmodelPackage.OWL_DATA_RANGE__INTERSECTION:
        return intersection != null;
    }
    return super.eIsSet(featureID);
  }

} //OwlDataRangeImpl
