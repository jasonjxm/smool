/**
 */
package org.smool.sdk.owlparser.owlmodel.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.smool.sdk.owlparser.owlmodel.ClassModifier;
import org.smool.sdk.owlparser.owlmodel.OwlRef;
import org.smool.sdk.owlparser.owlmodel.OwlmodelPackage;
import org.smool.sdk.owlparser.owlmodel.SubTypeOf;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sub Type Of</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.SubTypeOfImpl#isResourceBased <em>Resource Based</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.SubTypeOfImpl#getClassRef <em>Class Ref</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.SubTypeOfImpl#getModifiers <em>Modifiers</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SubTypeOfImpl extends MinimalEObjectImpl.Container implements SubTypeOf
{
  /**
   * The default value of the '{@link #isResourceBased() <em>Resource Based</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isResourceBased()
   * @generated
   * @ordered
   */
  protected static final boolean RESOURCE_BASED_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isResourceBased() <em>Resource Based</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isResourceBased()
   * @generated
   * @ordered
   */
  protected boolean resourceBased = RESOURCE_BASED_EDEFAULT;

  /**
   * The cached value of the '{@link #getClassRef() <em>Class Ref</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getClassRef()
   * @generated
   * @ordered
   */
  protected OwlRef classRef;

  /**
   * The cached value of the '{@link #getModifiers() <em>Modifiers</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getModifiers()
   * @generated
   * @ordered
   */
  protected EList<ClassModifier> modifiers;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SubTypeOfImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return OwlmodelPackage.Literals.SUB_TYPE_OF;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean isResourceBased()
  {
    return resourceBased;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setResourceBased(boolean newResourceBased)
  {
    boolean oldResourceBased = resourceBased;
    resourceBased = newResourceBased;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.SUB_TYPE_OF__RESOURCE_BASED, oldResourceBased, resourceBased));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OwlRef getClassRef()
  {
    return classRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetClassRef(OwlRef newClassRef, NotificationChain msgs)
  {
    OwlRef oldClassRef = classRef;
    classRef = newClassRef;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlmodelPackage.SUB_TYPE_OF__CLASS_REF, oldClassRef, newClassRef);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setClassRef(OwlRef newClassRef)
  {
    if (newClassRef != classRef)
    {
      NotificationChain msgs = null;
      if (classRef != null)
        msgs = ((InternalEObject)classRef).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.SUB_TYPE_OF__CLASS_REF, null, msgs);
      if (newClassRef != null)
        msgs = ((InternalEObject)newClassRef).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.SUB_TYPE_OF__CLASS_REF, null, msgs);
      msgs = basicSetClassRef(newClassRef, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.SUB_TYPE_OF__CLASS_REF, newClassRef, newClassRef));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<ClassModifier> getModifiers()
  {
    if (modifiers == null)
    {
      modifiers = new EObjectContainmentEList<ClassModifier>(ClassModifier.class, this, OwlmodelPackage.SUB_TYPE_OF__MODIFIERS);
    }
    return modifiers;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case OwlmodelPackage.SUB_TYPE_OF__CLASS_REF:
        return basicSetClassRef(null, msgs);
      case OwlmodelPackage.SUB_TYPE_OF__MODIFIERS:
        return ((InternalEList<?>)getModifiers()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case OwlmodelPackage.SUB_TYPE_OF__RESOURCE_BASED:
        return isResourceBased();
      case OwlmodelPackage.SUB_TYPE_OF__CLASS_REF:
        return getClassRef();
      case OwlmodelPackage.SUB_TYPE_OF__MODIFIERS:
        return getModifiers();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case OwlmodelPackage.SUB_TYPE_OF__RESOURCE_BASED:
        setResourceBased((Boolean)newValue);
        return;
      case OwlmodelPackage.SUB_TYPE_OF__CLASS_REF:
        setClassRef((OwlRef)newValue);
        return;
      case OwlmodelPackage.SUB_TYPE_OF__MODIFIERS:
        getModifiers().clear();
        getModifiers().addAll((Collection<? extends ClassModifier>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.SUB_TYPE_OF__RESOURCE_BASED:
        setResourceBased(RESOURCE_BASED_EDEFAULT);
        return;
      case OwlmodelPackage.SUB_TYPE_OF__CLASS_REF:
        setClassRef((OwlRef)null);
        return;
      case OwlmodelPackage.SUB_TYPE_OF__MODIFIERS:
        getModifiers().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.SUB_TYPE_OF__RESOURCE_BASED:
        return resourceBased != RESOURCE_BASED_EDEFAULT;
      case OwlmodelPackage.SUB_TYPE_OF__CLASS_REF:
        return classRef != null;
      case OwlmodelPackage.SUB_TYPE_OF__MODIFIERS:
        return modifiers != null && !modifiers.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (resourceBased: ");
    result.append(resourceBased);
    result.append(')');
    return result.toString();
  }

} //SubTypeOfImpl
