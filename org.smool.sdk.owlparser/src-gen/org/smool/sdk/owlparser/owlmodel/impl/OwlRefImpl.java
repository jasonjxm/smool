/**
 */
package org.smool.sdk.owlparser.owlmodel.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.smool.sdk.owlparser.owlmodel.OwlRef;
import org.smool.sdk.owlparser.owlmodel.OwlmodelPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Owl Ref</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OwlRefImpl#getOntURI <em>Ont URI</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OwlRefImpl#getOntName <em>Ont Name</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OwlRefImpl#getElemID <em>Elem ID</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OwlRefImpl#getFullURI <em>Full URI</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OwlRefImpl extends MinimalEObjectImpl.Container implements OwlRef
{
  /**
   * The default value of the '{@link #getOntURI() <em>Ont URI</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOntURI()
   * @generated
   * @ordered
   */
  protected static final String ONT_URI_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getOntURI() <em>Ont URI</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOntURI()
   * @generated
   * @ordered
   */
  protected String ontURI = ONT_URI_EDEFAULT;

  /**
   * The default value of the '{@link #getOntName() <em>Ont Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOntName()
   * @generated
   * @ordered
   */
  protected static final String ONT_NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getOntName() <em>Ont Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOntName()
   * @generated
   * @ordered
   */
  protected String ontName = ONT_NAME_EDEFAULT;

  /**
   * The default value of the '{@link #getElemID() <em>Elem ID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getElemID()
   * @generated
   * @ordered
   */
  protected static final String ELEM_ID_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getElemID() <em>Elem ID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getElemID()
   * @generated
   * @ordered
   */
  protected String elemID = ELEM_ID_EDEFAULT;

  /**
   * The default value of the '{@link #getFullURI() <em>Full URI</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFullURI()
   * @generated
   * @ordered
   */
  protected static final String FULL_URI_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getFullURI() <em>Full URI</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFullURI()
   * @generated
   * @ordered
   */
  protected String fullURI = FULL_URI_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected OwlRefImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return OwlmodelPackage.Literals.OWL_REF;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getOntURI()
  {
    return ontURI;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setOntURI(String newOntURI)
  {
    String oldOntURI = ontURI;
    ontURI = newOntURI;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_REF__ONT_URI, oldOntURI, ontURI));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getOntName()
  {
    return ontName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setOntName(String newOntName)
  {
    String oldOntName = ontName;
    ontName = newOntName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_REF__ONT_NAME, oldOntName, ontName));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getElemID()
  {
    return elemID;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setElemID(String newElemID)
  {
    String oldElemID = elemID;
    elemID = newElemID;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_REF__ELEM_ID, oldElemID, elemID));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getFullURI()
  {
    return fullURI;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setFullURI(String newFullURI)
  {
    String oldFullURI = fullURI;
    fullURI = newFullURI;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_REF__FULL_URI, oldFullURI, fullURI));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_REF__ONT_URI:
        return getOntURI();
      case OwlmodelPackage.OWL_REF__ONT_NAME:
        return getOntName();
      case OwlmodelPackage.OWL_REF__ELEM_ID:
        return getElemID();
      case OwlmodelPackage.OWL_REF__FULL_URI:
        return getFullURI();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_REF__ONT_URI:
        setOntURI((String)newValue);
        return;
      case OwlmodelPackage.OWL_REF__ONT_NAME:
        setOntName((String)newValue);
        return;
      case OwlmodelPackage.OWL_REF__ELEM_ID:
        setElemID((String)newValue);
        return;
      case OwlmodelPackage.OWL_REF__FULL_URI:
        setFullURI((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_REF__ONT_URI:
        setOntURI(ONT_URI_EDEFAULT);
        return;
      case OwlmodelPackage.OWL_REF__ONT_NAME:
        setOntName(ONT_NAME_EDEFAULT);
        return;
      case OwlmodelPackage.OWL_REF__ELEM_ID:
        setElemID(ELEM_ID_EDEFAULT);
        return;
      case OwlmodelPackage.OWL_REF__FULL_URI:
        setFullURI(FULL_URI_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_REF__ONT_URI:
        return ONT_URI_EDEFAULT == null ? ontURI != null : !ONT_URI_EDEFAULT.equals(ontURI);
      case OwlmodelPackage.OWL_REF__ONT_NAME:
        return ONT_NAME_EDEFAULT == null ? ontName != null : !ONT_NAME_EDEFAULT.equals(ontName);
      case OwlmodelPackage.OWL_REF__ELEM_ID:
        return ELEM_ID_EDEFAULT == null ? elemID != null : !ELEM_ID_EDEFAULT.equals(elemID);
      case OwlmodelPackage.OWL_REF__FULL_URI:
        return FULL_URI_EDEFAULT == null ? fullURI != null : !FULL_URI_EDEFAULT.equals(fullURI);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (ontURI: ");
    result.append(ontURI);
    result.append(", ontName: ");
    result.append(ontName);
    result.append(", elemID: ");
    result.append(elemID);
    result.append(", fullURI: ");
    result.append(fullURI);
    result.append(')');
    return result.toString();
  }

} //OwlRefImpl
