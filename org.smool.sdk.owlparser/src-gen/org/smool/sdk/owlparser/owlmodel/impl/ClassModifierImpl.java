/**
 */
package org.smool.sdk.owlparser.owlmodel.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.smool.sdk.owlparser.owlmodel.ClassModifier;
import org.smool.sdk.owlparser.owlmodel.OwlmodelPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Class Modifier</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ClassModifierImpl extends MinimalEObjectImpl.Container implements ClassModifier
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ClassModifierImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return OwlmodelPackage.Literals.CLASS_MODIFIER;
  }

} //ClassModifierImpl
