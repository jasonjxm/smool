/**
 */
package org.smool.sdk.owlparser.owlmodel.impl;

import org.eclipse.emf.ecore.EClass;

import org.smool.sdk.owlparser.owlmodel.DatatypeProperty;
import org.smool.sdk.owlparser.owlmodel.OwlmodelPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Datatype Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DatatypePropertyImpl extends PropertyImpl implements DatatypeProperty
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected DatatypePropertyImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return OwlmodelPackage.Literals.DATATYPE_PROPERTY;
  }

} //DatatypePropertyImpl
