/**
 */
package org.smool.sdk.owlparser.owlmodel.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.smool.sdk.owlparser.owlmodel.OwlmodelPackage;
import org.smool.sdk.owlparser.owlmodel.RDFSDomain;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>RDFS Domain</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RDFSDomainImpl extends MinimalEObjectImpl.Container implements RDFSDomain
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected RDFSDomainImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return OwlmodelPackage.Literals.RDFS_DOMAIN;
  }

} //RDFSDomainImpl
