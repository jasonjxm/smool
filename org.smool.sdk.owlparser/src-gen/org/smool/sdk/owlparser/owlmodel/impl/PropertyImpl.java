/**
 */
package org.smool.sdk.owlparser.owlmodel.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.smool.sdk.owlparser.owlmodel.OwlID;
import org.smool.sdk.owlparser.owlmodel.OwlLabel;
import org.smool.sdk.owlparser.owlmodel.OwlRef;
import org.smool.sdk.owlparser.owlmodel.OwlVersion;
import org.smool.sdk.owlparser.owlmodel.OwlmodelPackage;
import org.smool.sdk.owlparser.owlmodel.Property;
import org.smool.sdk.owlparser.owlmodel.PropertyModifier;
import org.smool.sdk.owlparser.owlmodel.RDFSDomain;
import org.smool.sdk.owlparser.owlmodel.RDFSRange;
import org.smool.sdk.owlparser.owlmodel.RDFType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Property</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.PropertyImpl#getId <em>Id</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.PropertyImpl#isReference <em>Reference</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.PropertyImpl#getRef <em>Ref</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.PropertyImpl#getOthers <em>Others</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.PropertyImpl#getLabels <em>Labels</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.PropertyImpl#getVersionInfo <em>Version Info</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.PropertyImpl#getRanges <em>Ranges</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.PropertyImpl#getTypes <em>Types</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.PropertyImpl#getDomains <em>Domains</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.PropertyImpl#getModifiers <em>Modifiers</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PropertyImpl extends OntologyElementImpl implements Property
{
  /**
   * The cached value of the '{@link #getId() <em>Id</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getId()
   * @generated
   * @ordered
   */
  protected OwlID id;

  /**
   * The default value of the '{@link #isReference() <em>Reference</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isReference()
   * @generated
   * @ordered
   */
  protected static final boolean REFERENCE_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isReference() <em>Reference</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isReference()
   * @generated
   * @ordered
   */
  protected boolean reference = REFERENCE_EDEFAULT;

  /**
   * The cached value of the '{@link #getRef() <em>Ref</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRef()
   * @generated
   * @ordered
   */
  protected OwlRef ref;

  /**
   * The cached value of the '{@link #getOthers() <em>Others</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOthers()
   * @generated
   * @ordered
   */
  protected EList<String> others;

  /**
   * The cached value of the '{@link #getLabels() <em>Labels</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLabels()
   * @generated
   * @ordered
   */
  protected EList<OwlLabel> labels;

  /**
   * The cached value of the '{@link #getVersionInfo() <em>Version Info</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVersionInfo()
   * @generated
   * @ordered
   */
  protected EList<OwlVersion> versionInfo;

  /**
   * The cached value of the '{@link #getRanges() <em>Ranges</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRanges()
   * @generated
   * @ordered
   */
  protected EList<RDFSRange> ranges;

  /**
   * The cached value of the '{@link #getTypes() <em>Types</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTypes()
   * @generated
   * @ordered
   */
  protected EList<RDFType> types;

  /**
   * The cached value of the '{@link #getDomains() <em>Domains</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDomains()
   * @generated
   * @ordered
   */
  protected EList<RDFSDomain> domains;

  /**
   * The cached value of the '{@link #getModifiers() <em>Modifiers</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getModifiers()
   * @generated
   * @ordered
   */
  protected EList<PropertyModifier> modifiers;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected PropertyImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return OwlmodelPackage.Literals.PROPERTY;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OwlID getId()
  {
    return id;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetId(OwlID newId, NotificationChain msgs)
  {
    OwlID oldId = id;
    id = newId;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlmodelPackage.PROPERTY__ID, oldId, newId);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setId(OwlID newId)
  {
    if (newId != id)
    {
      NotificationChain msgs = null;
      if (id != null)
        msgs = ((InternalEObject)id).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.PROPERTY__ID, null, msgs);
      if (newId != null)
        msgs = ((InternalEObject)newId).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.PROPERTY__ID, null, msgs);
      msgs = basicSetId(newId, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.PROPERTY__ID, newId, newId));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean isReference()
  {
    return reference;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setReference(boolean newReference)
  {
    boolean oldReference = reference;
    reference = newReference;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.PROPERTY__REFERENCE, oldReference, reference));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OwlRef getRef()
  {
    return ref;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetRef(OwlRef newRef, NotificationChain msgs)
  {
    OwlRef oldRef = ref;
    ref = newRef;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlmodelPackage.PROPERTY__REF, oldRef, newRef);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setRef(OwlRef newRef)
  {
    if (newRef != ref)
    {
      NotificationChain msgs = null;
      if (ref != null)
        msgs = ((InternalEObject)ref).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.PROPERTY__REF, null, msgs);
      if (newRef != null)
        msgs = ((InternalEObject)newRef).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.PROPERTY__REF, null, msgs);
      msgs = basicSetRef(newRef, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.PROPERTY__REF, newRef, newRef));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<String> getOthers()
  {
    if (others == null)
    {
      others = new EDataTypeEList<String>(String.class, this, OwlmodelPackage.PROPERTY__OTHERS);
    }
    return others;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<OwlLabel> getLabels()
  {
    if (labels == null)
    {
      labels = new EObjectContainmentEList<OwlLabel>(OwlLabel.class, this, OwlmodelPackage.PROPERTY__LABELS);
    }
    return labels;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<OwlVersion> getVersionInfo()
  {
    if (versionInfo == null)
    {
      versionInfo = new EObjectContainmentEList<OwlVersion>(OwlVersion.class, this, OwlmodelPackage.PROPERTY__VERSION_INFO);
    }
    return versionInfo;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<RDFSRange> getRanges()
  {
    if (ranges == null)
    {
      ranges = new EObjectContainmentEList<RDFSRange>(RDFSRange.class, this, OwlmodelPackage.PROPERTY__RANGES);
    }
    return ranges;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<RDFType> getTypes()
  {
    if (types == null)
    {
      types = new EObjectContainmentEList<RDFType>(RDFType.class, this, OwlmodelPackage.PROPERTY__TYPES);
    }
    return types;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<RDFSDomain> getDomains()
  {
    if (domains == null)
    {
      domains = new EObjectContainmentEList<RDFSDomain>(RDFSDomain.class, this, OwlmodelPackage.PROPERTY__DOMAINS);
    }
    return domains;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<PropertyModifier> getModifiers()
  {
    if (modifiers == null)
    {
      modifiers = new EObjectContainmentEList<PropertyModifier>(PropertyModifier.class, this, OwlmodelPackage.PROPERTY__MODIFIERS);
    }
    return modifiers;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case OwlmodelPackage.PROPERTY__ID:
        return basicSetId(null, msgs);
      case OwlmodelPackage.PROPERTY__REF:
        return basicSetRef(null, msgs);
      case OwlmodelPackage.PROPERTY__LABELS:
        return ((InternalEList<?>)getLabels()).basicRemove(otherEnd, msgs);
      case OwlmodelPackage.PROPERTY__VERSION_INFO:
        return ((InternalEList<?>)getVersionInfo()).basicRemove(otherEnd, msgs);
      case OwlmodelPackage.PROPERTY__RANGES:
        return ((InternalEList<?>)getRanges()).basicRemove(otherEnd, msgs);
      case OwlmodelPackage.PROPERTY__TYPES:
        return ((InternalEList<?>)getTypes()).basicRemove(otherEnd, msgs);
      case OwlmodelPackage.PROPERTY__DOMAINS:
        return ((InternalEList<?>)getDomains()).basicRemove(otherEnd, msgs);
      case OwlmodelPackage.PROPERTY__MODIFIERS:
        return ((InternalEList<?>)getModifiers()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case OwlmodelPackage.PROPERTY__ID:
        return getId();
      case OwlmodelPackage.PROPERTY__REFERENCE:
        return isReference();
      case OwlmodelPackage.PROPERTY__REF:
        return getRef();
      case OwlmodelPackage.PROPERTY__OTHERS:
        return getOthers();
      case OwlmodelPackage.PROPERTY__LABELS:
        return getLabels();
      case OwlmodelPackage.PROPERTY__VERSION_INFO:
        return getVersionInfo();
      case OwlmodelPackage.PROPERTY__RANGES:
        return getRanges();
      case OwlmodelPackage.PROPERTY__TYPES:
        return getTypes();
      case OwlmodelPackage.PROPERTY__DOMAINS:
        return getDomains();
      case OwlmodelPackage.PROPERTY__MODIFIERS:
        return getModifiers();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case OwlmodelPackage.PROPERTY__ID:
        setId((OwlID)newValue);
        return;
      case OwlmodelPackage.PROPERTY__REFERENCE:
        setReference((Boolean)newValue);
        return;
      case OwlmodelPackage.PROPERTY__REF:
        setRef((OwlRef)newValue);
        return;
      case OwlmodelPackage.PROPERTY__OTHERS:
        getOthers().clear();
        getOthers().addAll((Collection<? extends String>)newValue);
        return;
      case OwlmodelPackage.PROPERTY__LABELS:
        getLabels().clear();
        getLabels().addAll((Collection<? extends OwlLabel>)newValue);
        return;
      case OwlmodelPackage.PROPERTY__VERSION_INFO:
        getVersionInfo().clear();
        getVersionInfo().addAll((Collection<? extends OwlVersion>)newValue);
        return;
      case OwlmodelPackage.PROPERTY__RANGES:
        getRanges().clear();
        getRanges().addAll((Collection<? extends RDFSRange>)newValue);
        return;
      case OwlmodelPackage.PROPERTY__TYPES:
        getTypes().clear();
        getTypes().addAll((Collection<? extends RDFType>)newValue);
        return;
      case OwlmodelPackage.PROPERTY__DOMAINS:
        getDomains().clear();
        getDomains().addAll((Collection<? extends RDFSDomain>)newValue);
        return;
      case OwlmodelPackage.PROPERTY__MODIFIERS:
        getModifiers().clear();
        getModifiers().addAll((Collection<? extends PropertyModifier>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.PROPERTY__ID:
        setId((OwlID)null);
        return;
      case OwlmodelPackage.PROPERTY__REFERENCE:
        setReference(REFERENCE_EDEFAULT);
        return;
      case OwlmodelPackage.PROPERTY__REF:
        setRef((OwlRef)null);
        return;
      case OwlmodelPackage.PROPERTY__OTHERS:
        getOthers().clear();
        return;
      case OwlmodelPackage.PROPERTY__LABELS:
        getLabels().clear();
        return;
      case OwlmodelPackage.PROPERTY__VERSION_INFO:
        getVersionInfo().clear();
        return;
      case OwlmodelPackage.PROPERTY__RANGES:
        getRanges().clear();
        return;
      case OwlmodelPackage.PROPERTY__TYPES:
        getTypes().clear();
        return;
      case OwlmodelPackage.PROPERTY__DOMAINS:
        getDomains().clear();
        return;
      case OwlmodelPackage.PROPERTY__MODIFIERS:
        getModifiers().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.PROPERTY__ID:
        return id != null;
      case OwlmodelPackage.PROPERTY__REFERENCE:
        return reference != REFERENCE_EDEFAULT;
      case OwlmodelPackage.PROPERTY__REF:
        return ref != null;
      case OwlmodelPackage.PROPERTY__OTHERS:
        return others != null && !others.isEmpty();
      case OwlmodelPackage.PROPERTY__LABELS:
        return labels != null && !labels.isEmpty();
      case OwlmodelPackage.PROPERTY__VERSION_INFO:
        return versionInfo != null && !versionInfo.isEmpty();
      case OwlmodelPackage.PROPERTY__RANGES:
        return ranges != null && !ranges.isEmpty();
      case OwlmodelPackage.PROPERTY__TYPES:
        return types != null && !types.isEmpty();
      case OwlmodelPackage.PROPERTY__DOMAINS:
        return domains != null && !domains.isEmpty();
      case OwlmodelPackage.PROPERTY__MODIFIERS:
        return modifiers != null && !modifiers.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (reference: ");
    result.append(reference);
    result.append(", others: ");
    result.append(others);
    result.append(')');
    return result.toString();
  }

} //PropertyImpl
