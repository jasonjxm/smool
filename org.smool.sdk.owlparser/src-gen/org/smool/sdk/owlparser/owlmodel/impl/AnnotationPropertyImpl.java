/**
 */
package org.smool.sdk.owlparser.owlmodel.impl;

import org.eclipse.emf.ecore.EClass;

import org.smool.sdk.owlparser.owlmodel.AnnotationProperty;
import org.smool.sdk.owlparser.owlmodel.OwlmodelPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Annotation Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AnnotationPropertyImpl extends PropertyImpl implements AnnotationProperty
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected AnnotationPropertyImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return OwlmodelPackage.Literals.ANNOTATION_PROPERTY;
  }

} //AnnotationPropertyImpl
