/**
 */
package org.smool.sdk.owlparser.owlmodel.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.smool.sdk.owlparser.owlmodel.AllValues;
import org.smool.sdk.owlparser.owlmodel.AnnotationProperty;
import org.smool.sdk.owlparser.owlmodel.BackwardsComp;
import org.smool.sdk.owlparser.owlmodel.Cardinality;
import org.smool.sdk.owlparser.owlmodel.ClassAxiom;
import org.smool.sdk.owlparser.owlmodel.ClassModifier;
import org.smool.sdk.owlparser.owlmodel.ComplementAxiom;
import org.smool.sdk.owlparser.owlmodel.Datatype;
import org.smool.sdk.owlparser.owlmodel.DatatypeProperty;
import org.smool.sdk.owlparser.owlmodel.Description;
import org.smool.sdk.owlparser.owlmodel.DisjointAxiom;
import org.smool.sdk.owlparser.owlmodel.DocEntity;
import org.smool.sdk.owlparser.owlmodel.DocType;
import org.smool.sdk.owlparser.owlmodel.EquivalentAxiom;
import org.smool.sdk.owlparser.owlmodel.EquivalentType;
import org.smool.sdk.owlparser.owlmodel.FunctionalProperty;
import org.smool.sdk.owlparser.owlmodel.HasValue;
import org.smool.sdk.owlparser.owlmodel.IncompatibleWithAxiom;
import org.smool.sdk.owlparser.owlmodel.IntersectionAxiom;
import org.smool.sdk.owlparser.owlmodel.InverseFunctionalProperty;
import org.smool.sdk.owlparser.owlmodel.InverseType;
import org.smool.sdk.owlparser.owlmodel.MaxCardinality;
import org.smool.sdk.owlparser.owlmodel.MaxQualifiedCardinality;
import org.smool.sdk.owlparser.owlmodel.MinCardinality;
import org.smool.sdk.owlparser.owlmodel.MinQualifiedCardinality;
import org.smool.sdk.owlparser.owlmodel.MinimumClass;
import org.smool.sdk.owlparser.owlmodel.NamedIndividual;
import org.smool.sdk.owlparser.owlmodel.OWLModel;
import org.smool.sdk.owlparser.owlmodel.ObjectProperty;
import org.smool.sdk.owlparser.owlmodel.OneOfAxiom;
import org.smool.sdk.owlparser.owlmodel.Ontology;
import org.smool.sdk.owlparser.owlmodel.OntologyElement;
import org.smool.sdk.owlparser.owlmodel.OntologyID;
import org.smool.sdk.owlparser.owlmodel.OwlClass;
import org.smool.sdk.owlparser.owlmodel.OwlDataRange;
import org.smool.sdk.owlparser.owlmodel.OwlFirst;
import org.smool.sdk.owlparser.owlmodel.OwlID;
import org.smool.sdk.owlparser.owlmodel.OwlImport;
import org.smool.sdk.owlparser.owlmodel.OwlLabel;
import org.smool.sdk.owlparser.owlmodel.OwlMembers;
import org.smool.sdk.owlparser.owlmodel.OwlRef;
import org.smool.sdk.owlparser.owlmodel.OwlRestriction;
import org.smool.sdk.owlparser.owlmodel.OwlType;
import org.smool.sdk.owlparser.owlmodel.OwlVersion;
import org.smool.sdk.owlparser.owlmodel.OwlmodelFactory;
import org.smool.sdk.owlparser.owlmodel.OwlmodelPackage;
import org.smool.sdk.owlparser.owlmodel.ParseType;
import org.smool.sdk.owlparser.owlmodel.ParsedPropertyRange;
import org.smool.sdk.owlparser.owlmodel.ParsedRDFSDomain;
import org.smool.sdk.owlparser.owlmodel.PriorVersion;
import org.smool.sdk.owlparser.owlmodel.Property;
import org.smool.sdk.owlparser.owlmodel.PropertyModifier;
import org.smool.sdk.owlparser.owlmodel.QualifiedCardinality;
import org.smool.sdk.owlparser.owlmodel.RDFElement;
import org.smool.sdk.owlparser.owlmodel.RDFRest;
import org.smool.sdk.owlparser.owlmodel.RDFSDomain;
import org.smool.sdk.owlparser.owlmodel.RDFSRange;
import org.smool.sdk.owlparser.owlmodel.RDFType;
import org.smool.sdk.owlparser.owlmodel.RestrictionBody;
import org.smool.sdk.owlparser.owlmodel.SeeAlso;
import org.smool.sdk.owlparser.owlmodel.SimpleRDFSDomain;
import org.smool.sdk.owlparser.owlmodel.SimpleRDFSRange;
import org.smool.sdk.owlparser.owlmodel.SomeValues;
import org.smool.sdk.owlparser.owlmodel.SubPropertyOf;
import org.smool.sdk.owlparser.owlmodel.SubTypeOf;
import org.smool.sdk.owlparser.owlmodel.SymmetricProperty;
import org.smool.sdk.owlparser.owlmodel.TransitiveProperty;
import org.smool.sdk.owlparser.owlmodel.UnionAxiom;
import org.smool.sdk.owlparser.owlmodel.XmlNs;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OwlmodelPackageImpl extends EPackageImpl implements OwlmodelPackage
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass owlModelEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass docTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass docEntityEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass rdfElementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass xmlNsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass owlRefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass owlIDEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ontologyIDEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass parseTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ontologyElementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass namedIndividualEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ontologyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass seeAlsoEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass priorVersionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass backwardsCompEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass owlImportEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass owlClassEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass minimumClassEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass classAxiomEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass complementAxiomEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass incompatibleWithAxiomEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass disjointAxiomEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass equivalentAxiomEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass unionAxiomEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass intersectionAxiomEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass oneOfAxiomEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass owlLabelEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass owlVersionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass subTypeOfEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass classModifierEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass owlRestrictionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass restrictionBodyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass minCardinalityEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass maxCardinalityEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass cardinalityEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass qualifiedCardinalityEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass minQualifiedCardinalityEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass maxQualifiedCardinalityEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass someValuesEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass allValuesEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass hasValueEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass propertyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass annotationPropertyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass objectPropertyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass datatypePropertyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass functionalPropertyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass inverseFunctionalPropertyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass symmetricPropertyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass transitivePropertyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass propertyModifierEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass inverseTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass equivalentTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass rdfsDomainEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass rdfsRangeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass owlDataRangeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass rdfTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass subPropertyOfEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass descriptionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass owlFirstEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass rdfRestEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass owlTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass owlMembersEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass datatypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass simpleRDFSDomainEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass parsedRDFSDomainEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass simpleRDFSRangeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass parsedPropertyRangeEClass = null;

  /**
   * Creates an instance of the model <b>Package</b>, registered with
   * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
   * package URI value.
   * <p>Note: the correct way to create the package is via the static
   * factory method {@link #init init()}, which also performs
   * initialization of the package, or returns the registered package,
   * if one already exists.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EPackage.Registry
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#eNS_URI
   * @see #init()
   * @generated
   */
  private OwlmodelPackageImpl()
  {
    super(eNS_URI, OwlmodelFactory.eINSTANCE);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static boolean isInited = false;

  /**
   * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
   *
   * <p>This method is used to initialize {@link OwlmodelPackage#eINSTANCE} when that field is accessed.
   * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #eNS_URI
   * @see #createPackageContents()
   * @see #initializePackageContents()
   * @generated
   */
  public static OwlmodelPackage init()
  {
    if (isInited) return (OwlmodelPackage)EPackage.Registry.INSTANCE.getEPackage(OwlmodelPackage.eNS_URI);

    // Obtain or create and register package
    Object registeredOwlmodelPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
    OwlmodelPackageImpl theOwlmodelPackage = registeredOwlmodelPackage instanceof OwlmodelPackageImpl ? (OwlmodelPackageImpl)registeredOwlmodelPackage : new OwlmodelPackageImpl();

    isInited = true;

    // Create package meta-data objects
    theOwlmodelPackage.createPackageContents();

    // Initialize created meta-data
    theOwlmodelPackage.initializePackageContents();

    // Mark meta-data to indicate it can't be changed
    theOwlmodelPackage.freeze();

    // Update the registry and return the package
    EPackage.Registry.INSTANCE.put(OwlmodelPackage.eNS_URI, theOwlmodelPackage);
    return theOwlmodelPackage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getOWLModel()
  {
    return owlModelEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getOWLModel_Header()
  {
    return (EAttribute)owlModelEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOWLModel_DocType()
  {
    return (EReference)owlModelEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOWLModel_Rdf()
  {
    return (EReference)owlModelEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getDocType()
  {
    return docTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getDocType_DocID()
  {
    return (EAttribute)docTypeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getDocType_Entities()
  {
    return (EReference)docTypeEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getDocEntity()
  {
    return docEntityEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getDocEntity_EntityID()
  {
    return (EAttribute)docEntityEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getDocEntity_EntityURI()
  {
    return (EAttribute)docEntityEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getRDFElement()
  {
    return rdfElementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getRDFElement_ImportedNs()
  {
    return (EReference)rdfElementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getRDFElement_BaseOnt()
  {
    return (EAttribute)rdfElementEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getRDFElement_OntologyElements()
  {
    return (EReference)rdfElementEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getXmlNs()
  {
    return xmlNsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getXmlNs_Nsid()
  {
    return (EAttribute)xmlNsEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getXmlNs_Nsuri()
  {
    return (EAttribute)xmlNsEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getXmlNs_Nsname()
  {
    return (EAttribute)xmlNsEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getOwlRef()
  {
    return owlRefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getOwlRef_OntURI()
  {
    return (EAttribute)owlRefEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getOwlRef_OntName()
  {
    return (EAttribute)owlRefEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getOwlRef_ElemID()
  {
    return (EAttribute)owlRefEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getOwlRef_FullURI()
  {
    return (EAttribute)owlRefEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getOwlID()
  {
    return owlIDEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getOwlID_ElemID()
  {
    return (EAttribute)owlIDEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getOntologyID()
  {
    return ontologyIDEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getOntologyID_SuperOnt()
  {
    return (EAttribute)ontologyIDEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getOntologyID_OntURI()
  {
    return (EAttribute)ontologyIDEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getParseType()
  {
    return parseTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getParseType_Type()
  {
    return (EAttribute)parseTypeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getOntologyElement()
  {
    return ontologyElementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getNamedIndividual()
  {
    return namedIndividualEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getNamedIndividual_Ref()
  {
    return (EReference)namedIndividualEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getNamedIndividual_Types()
  {
    return (EReference)namedIndividualEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getNamedIndividual_Desc()
  {
    return (EAttribute)namedIndividualEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getOntology()
  {
    return ontologyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOntology_About()
  {
    return (EReference)ontologyEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOntology_Resource()
  {
    return (EReference)ontologyEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOntology_Labels()
  {
    return (EReference)ontologyEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOntology_VersionInfo()
  {
    return (EReference)ontologyEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOntology_SeeAlso()
  {
    return (EReference)ontologyEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOntology_PVers()
  {
    return (EReference)ontologyEClass.getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOntology_BackComp()
  {
    return (EReference)ontologyEClass.getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOntology_ImportedOntologies()
  {
    return (EReference)ontologyEClass.getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getOntology_Others()
  {
    return (EAttribute)ontologyEClass.getEStructuralFeatures().get(8);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getSeeAlso()
  {
    return seeAlsoEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getSeeAlso_Ref()
  {
    return (EReference)seeAlsoEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getPriorVersion()
  {
    return priorVersionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getPriorVersion_Ref()
  {
    return (EReference)priorVersionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getBackwardsComp()
  {
    return backwardsCompEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getBackwardsComp_Ref()
  {
    return (EReference)backwardsCompEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getOwlImport()
  {
    return owlImportEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOwlImport_Ref()
  {
    return (EReference)owlImportEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOwlImport_Onts()
  {
    return (EReference)owlImportEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getOwlClass()
  {
    return owlClassEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOwlClass_Id()
  {
    return (EReference)owlClassEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getOwlClass_Reference()
  {
    return (EAttribute)owlClassEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOwlClass_Ref()
  {
    return (EReference)owlClassEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOwlClass_Labels()
  {
    return (EReference)owlClassEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOwlClass_VersionInfo()
  {
    return (EReference)owlClassEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOwlClass_SuperTypes()
  {
    return (EReference)owlClassEClass.getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOwlClass_Axioms()
  {
    return (EReference)owlClassEClass.getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOwlClass_Onts()
  {
    return (EReference)owlClassEClass.getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getOwlClass_Others()
  {
    return (EAttribute)owlClassEClass.getEStructuralFeatures().get(8);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getMinimumClass()
  {
    return minimumClassEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getMinimumClass_Labels()
  {
    return (EReference)minimumClassEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getMinimumClass_Axioms()
  {
    return (EReference)minimumClassEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getMinimumClass_Others()
  {
    return (EAttribute)minimumClassEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getClassAxiom()
  {
    return classAxiomEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getClassAxiom_ParseType()
  {
    return (EReference)classAxiomEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getComplementAxiom()
  {
    return complementAxiomEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getComplementAxiom_Ref()
  {
    return (EReference)complementAxiomEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getComplementAxiom_Classes()
  {
    return (EReference)complementAxiomEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getIncompatibleWithAxiom()
  {
    return incompatibleWithAxiomEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getIncompatibleWithAxiom_Ref()
  {
    return (EReference)incompatibleWithAxiomEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getIncompatibleWithAxiom_Classes()
  {
    return (EReference)incompatibleWithAxiomEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getDisjointAxiom()
  {
    return disjointAxiomEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getDisjointAxiom_Ref()
  {
    return (EReference)disjointAxiomEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getDisjointAxiom_Classes()
  {
    return (EReference)disjointAxiomEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getEquivalentAxiom()
  {
    return equivalentAxiomEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getEquivalentAxiom_Ref()
  {
    return (EReference)equivalentAxiomEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getEquivalentAxiom_Classes()
  {
    return (EReference)equivalentAxiomEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getUnionAxiom()
  {
    return unionAxiomEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getUnionAxiom_Classes()
  {
    return (EReference)unionAxiomEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getIntersectionAxiom()
  {
    return intersectionAxiomEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getIntersectionAxiom_Classes()
  {
    return (EReference)intersectionAxiomEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getOneOfAxiom()
  {
    return oneOfAxiomEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOneOfAxiom_Values()
  {
    return (EReference)oneOfAxiomEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getOneOfAxiom_TagNames()
  {
    return (EAttribute)oneOfAxiomEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOneOfAxiom_Tagids()
  {
    return (EReference)oneOfAxiomEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOneOfAxiom_First()
  {
    return (EReference)oneOfAxiomEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOneOfAxiom_Rest()
  {
    return (EReference)oneOfAxiomEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getOwlLabel()
  {
    return owlLabelEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getOwlLabel_Lang()
  {
    return (EAttribute)owlLabelEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOwlLabel_Datatype()
  {
    return (EReference)owlLabelEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getOwlLabel_Value()
  {
    return (EAttribute)owlLabelEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getOwlVersion()
  {
    return owlVersionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getOwlVersion_Lang()
  {
    return (EAttribute)owlVersionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOwlVersion_Datatype()
  {
    return (EReference)owlVersionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getOwlVersion_Value()
  {
    return (EAttribute)owlVersionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getSubTypeOf()
  {
    return subTypeOfEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getSubTypeOf_ResourceBased()
  {
    return (EAttribute)subTypeOfEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getSubTypeOf_ClassRef()
  {
    return (EReference)subTypeOfEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getSubTypeOf_Modifiers()
  {
    return (EReference)subTypeOfEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getClassModifier()
  {
    return classModifierEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getOwlRestriction()
  {
    return owlRestrictionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOwlRestriction_RestrictionBodies()
  {
    return (EReference)owlRestrictionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOwlRestriction_Prop()
  {
    return (EReference)owlRestrictionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOwlRestriction_Property()
  {
    return (EReference)owlRestrictionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOwlRestriction_Class()
  {
    return (EReference)owlRestrictionEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOwlRestriction_Classes()
  {
    return (EReference)owlRestrictionEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getRestrictionBody()
  {
    return restrictionBodyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getMinCardinality()
  {
    return minCardinalityEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getMinCardinality_Datatype()
  {
    return (EReference)minCardinalityEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getMinCardinality_Value()
  {
    return (EAttribute)minCardinalityEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getMaxCardinality()
  {
    return maxCardinalityEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getMaxCardinality_Datatype()
  {
    return (EReference)maxCardinalityEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getMaxCardinality_Value()
  {
    return (EAttribute)maxCardinalityEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getCardinality()
  {
    return cardinalityEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getCardinality_Datatype()
  {
    return (EReference)cardinalityEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getCardinality_Value()
  {
    return (EAttribute)cardinalityEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getQualifiedCardinality()
  {
    return qualifiedCardinalityEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getQualifiedCardinality_Datatype()
  {
    return (EReference)qualifiedCardinalityEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getQualifiedCardinality_Value()
  {
    return (EAttribute)qualifiedCardinalityEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getMinQualifiedCardinality()
  {
    return minQualifiedCardinalityEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getMinQualifiedCardinality_Datatype()
  {
    return (EReference)minQualifiedCardinalityEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getMinQualifiedCardinality_Value()
  {
    return (EAttribute)minQualifiedCardinalityEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getMaxQualifiedCardinality()
  {
    return maxQualifiedCardinalityEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getMaxQualifiedCardinality_Datatype()
  {
    return (EReference)maxQualifiedCardinalityEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getMaxQualifiedCardinality_Value()
  {
    return (EAttribute)maxQualifiedCardinalityEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getSomeValues()
  {
    return someValuesEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getSomeValues_Ref()
  {
    return (EReference)someValuesEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getSomeValues_Modifiers()
  {
    return (EReference)someValuesEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getAllValues()
  {
    return allValuesEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getAllValues_Ref()
  {
    return (EReference)allValuesEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getAllValues_Modifiers()
  {
    return (EReference)allValuesEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getHasValue()
  {
    return hasValueEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getHasValue_Ref()
  {
    return (EReference)hasValueEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getHasValue_Type()
  {
    return (EReference)hasValueEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getHasValue_Value()
  {
    return (EAttribute)hasValueEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getProperty()
  {
    return propertyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getProperty_Id()
  {
    return (EReference)propertyEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getProperty_Reference()
  {
    return (EAttribute)propertyEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getProperty_Ref()
  {
    return (EReference)propertyEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getProperty_Others()
  {
    return (EAttribute)propertyEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getProperty_Labels()
  {
    return (EReference)propertyEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getProperty_VersionInfo()
  {
    return (EReference)propertyEClass.getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getProperty_Ranges()
  {
    return (EReference)propertyEClass.getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getProperty_Types()
  {
    return (EReference)propertyEClass.getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getProperty_Domains()
  {
    return (EReference)propertyEClass.getEStructuralFeatures().get(8);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getProperty_Modifiers()
  {
    return (EReference)propertyEClass.getEStructuralFeatures().get(9);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getAnnotationProperty()
  {
    return annotationPropertyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getObjectProperty()
  {
    return objectPropertyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getDatatypeProperty()
  {
    return datatypePropertyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getFunctionalProperty()
  {
    return functionalPropertyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getInverseFunctionalProperty()
  {
    return inverseFunctionalPropertyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getSymmetricProperty()
  {
    return symmetricPropertyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getTransitiveProperty()
  {
    return transitivePropertyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getPropertyModifier()
  {
    return propertyModifierEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getPropertyModifier_Props()
  {
    return (EReference)propertyModifierEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getInverseType()
  {
    return inverseTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getInverseType_Ref()
  {
    return (EReference)inverseTypeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getEquivalentType()
  {
    return equivalentTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getEquivalentType_Ref()
  {
    return (EReference)equivalentTypeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getRDFSDomain()
  {
    return rdfsDomainEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getRDFSRange()
  {
    return rdfsRangeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getOwlDataRange()
  {
    return owlDataRangeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOwlDataRange_OwlRange()
  {
    return (EReference)owlDataRangeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOwlDataRange_Union()
  {
    return (EReference)owlDataRangeEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOwlDataRange_OneOf()
  {
    return (EReference)owlDataRangeEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOwlDataRange_Intersection()
  {
    return (EReference)owlDataRangeEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getRDFType()
  {
    return rdfTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getRDFType_Type()
  {
    return (EReference)rdfTypeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getSubPropertyOf()
  {
    return subPropertyOfEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getSubPropertyOf_Type()
  {
    return (EReference)subPropertyOfEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getDescription()
  {
    return descriptionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getDescription_About()
  {
    return (EReference)descriptionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getDescription_First()
  {
    return (EReference)descriptionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getDescription_Type()
  {
    return (EReference)descriptionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getDescription_Domain()
  {
    return (EReference)descriptionEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getDescription_Range()
  {
    return (EReference)descriptionEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getDescription_PropModifiers()
  {
    return (EReference)descriptionEClass.getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getDescription_Labels()
  {
    return (EReference)descriptionEClass.getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getDescription_Axioms()
  {
    return (EReference)descriptionEClass.getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getOwlFirst()
  {
    return owlFirstEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOwlFirst_RefType()
  {
    return (EReference)owlFirstEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOwlFirst_DataType()
  {
    return (EReference)owlFirstEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getOwlFirst_DataTypeValue()
  {
    return (EAttribute)owlFirstEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getRDFRest()
  {
    return rdfRestEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getRDFRest_Ref()
  {
    return (EReference)rdfRestEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getRDFRest_ParseType()
  {
    return (EReference)rdfRestEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getRDFRest_First()
  {
    return (EReference)rdfRestEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getRDFRest_Rest()
  {
    return (EReference)rdfRestEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getOwlType()
  {
    return owlTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOwlType_RefType()
  {
    return (EReference)owlTypeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOwlType_Members()
  {
    return (EReference)owlTypeEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getOwlMembers()
  {
    return owlMembersEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOwlMembers_ParseType()
  {
    return (EReference)owlMembersEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getOwlMembers_Descriptions()
  {
    return (EReference)owlMembersEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getDatatype()
  {
    return datatypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getDatatype_Id()
  {
    return (EReference)datatypeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EAttribute getDatatype_Reference()
  {
    return (EAttribute)datatypeEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getDatatype_Ref()
  {
    return (EReference)datatypeEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getSimpleRDFSDomain()
  {
    return simpleRDFSDomainEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getSimpleRDFSDomain_Ref()
  {
    return (EReference)simpleRDFSDomainEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getParsedRDFSDomain()
  {
    return parsedRDFSDomainEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getParsedRDFSDomain_ParseType()
  {
    return (EReference)parsedRDFSDomainEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getParsedRDFSDomain_Modifiers()
  {
    return (EReference)parsedRDFSDomainEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getSimpleRDFSRange()
  {
    return simpleRDFSRangeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getSimpleRDFSRange_Range()
  {
    return (EReference)simpleRDFSRangeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EClass getParsedPropertyRange()
  {
    return parsedPropertyRangeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getParsedPropertyRange_ParseType()
  {
    return (EReference)parsedPropertyRangeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EReference getParsedPropertyRange_Modifiers()
  {
    return (EReference)parsedPropertyRangeEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OwlmodelFactory getOwlmodelFactory()
  {
    return (OwlmodelFactory)getEFactoryInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isCreated = false;

  /**
   * Creates the meta-model objects for the package.  This method is
   * guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void createPackageContents()
  {
    if (isCreated) return;
    isCreated = true;

    // Create classes and their features
    owlModelEClass = createEClass(OWL_MODEL);
    createEAttribute(owlModelEClass, OWL_MODEL__HEADER);
    createEReference(owlModelEClass, OWL_MODEL__DOC_TYPE);
    createEReference(owlModelEClass, OWL_MODEL__RDF);

    docTypeEClass = createEClass(DOC_TYPE);
    createEAttribute(docTypeEClass, DOC_TYPE__DOC_ID);
    createEReference(docTypeEClass, DOC_TYPE__ENTITIES);

    docEntityEClass = createEClass(DOC_ENTITY);
    createEAttribute(docEntityEClass, DOC_ENTITY__ENTITY_ID);
    createEAttribute(docEntityEClass, DOC_ENTITY__ENTITY_URI);

    rdfElementEClass = createEClass(RDF_ELEMENT);
    createEReference(rdfElementEClass, RDF_ELEMENT__IMPORTED_NS);
    createEAttribute(rdfElementEClass, RDF_ELEMENT__BASE_ONT);
    createEReference(rdfElementEClass, RDF_ELEMENT__ONTOLOGY_ELEMENTS);

    xmlNsEClass = createEClass(XML_NS);
    createEAttribute(xmlNsEClass, XML_NS__NSID);
    createEAttribute(xmlNsEClass, XML_NS__NSURI);
    createEAttribute(xmlNsEClass, XML_NS__NSNAME);

    owlRefEClass = createEClass(OWL_REF);
    createEAttribute(owlRefEClass, OWL_REF__ONT_URI);
    createEAttribute(owlRefEClass, OWL_REF__ONT_NAME);
    createEAttribute(owlRefEClass, OWL_REF__ELEM_ID);
    createEAttribute(owlRefEClass, OWL_REF__FULL_URI);

    owlIDEClass = createEClass(OWL_ID);
    createEAttribute(owlIDEClass, OWL_ID__ELEM_ID);

    ontologyIDEClass = createEClass(ONTOLOGY_ID);
    createEAttribute(ontologyIDEClass, ONTOLOGY_ID__SUPER_ONT);
    createEAttribute(ontologyIDEClass, ONTOLOGY_ID__ONT_URI);

    parseTypeEClass = createEClass(PARSE_TYPE);
    createEAttribute(parseTypeEClass, PARSE_TYPE__TYPE);

    ontologyElementEClass = createEClass(ONTOLOGY_ELEMENT);

    namedIndividualEClass = createEClass(NAMED_INDIVIDUAL);
    createEReference(namedIndividualEClass, NAMED_INDIVIDUAL__REF);
    createEReference(namedIndividualEClass, NAMED_INDIVIDUAL__TYPES);
    createEAttribute(namedIndividualEClass, NAMED_INDIVIDUAL__DESC);

    ontologyEClass = createEClass(ONTOLOGY);
    createEReference(ontologyEClass, ONTOLOGY__ABOUT);
    createEReference(ontologyEClass, ONTOLOGY__RESOURCE);
    createEReference(ontologyEClass, ONTOLOGY__LABELS);
    createEReference(ontologyEClass, ONTOLOGY__VERSION_INFO);
    createEReference(ontologyEClass, ONTOLOGY__SEE_ALSO);
    createEReference(ontologyEClass, ONTOLOGY__PVERS);
    createEReference(ontologyEClass, ONTOLOGY__BACK_COMP);
    createEReference(ontologyEClass, ONTOLOGY__IMPORTED_ONTOLOGIES);
    createEAttribute(ontologyEClass, ONTOLOGY__OTHERS);

    seeAlsoEClass = createEClass(SEE_ALSO);
    createEReference(seeAlsoEClass, SEE_ALSO__REF);

    priorVersionEClass = createEClass(PRIOR_VERSION);
    createEReference(priorVersionEClass, PRIOR_VERSION__REF);

    backwardsCompEClass = createEClass(BACKWARDS_COMP);
    createEReference(backwardsCompEClass, BACKWARDS_COMP__REF);

    owlImportEClass = createEClass(OWL_IMPORT);
    createEReference(owlImportEClass, OWL_IMPORT__REF);
    createEReference(owlImportEClass, OWL_IMPORT__ONTS);

    owlClassEClass = createEClass(OWL_CLASS);
    createEReference(owlClassEClass, OWL_CLASS__ID);
    createEAttribute(owlClassEClass, OWL_CLASS__REFERENCE);
    createEReference(owlClassEClass, OWL_CLASS__REF);
    createEReference(owlClassEClass, OWL_CLASS__LABELS);
    createEReference(owlClassEClass, OWL_CLASS__VERSION_INFO);
    createEReference(owlClassEClass, OWL_CLASS__SUPER_TYPES);
    createEReference(owlClassEClass, OWL_CLASS__AXIOMS);
    createEReference(owlClassEClass, OWL_CLASS__ONTS);
    createEAttribute(owlClassEClass, OWL_CLASS__OTHERS);

    minimumClassEClass = createEClass(MINIMUM_CLASS);
    createEReference(minimumClassEClass, MINIMUM_CLASS__LABELS);
    createEReference(minimumClassEClass, MINIMUM_CLASS__AXIOMS);
    createEAttribute(minimumClassEClass, MINIMUM_CLASS__OTHERS);

    classAxiomEClass = createEClass(CLASS_AXIOM);
    createEReference(classAxiomEClass, CLASS_AXIOM__PARSE_TYPE);

    complementAxiomEClass = createEClass(COMPLEMENT_AXIOM);
    createEReference(complementAxiomEClass, COMPLEMENT_AXIOM__REF);
    createEReference(complementAxiomEClass, COMPLEMENT_AXIOM__CLASSES);

    incompatibleWithAxiomEClass = createEClass(INCOMPATIBLE_WITH_AXIOM);
    createEReference(incompatibleWithAxiomEClass, INCOMPATIBLE_WITH_AXIOM__REF);
    createEReference(incompatibleWithAxiomEClass, INCOMPATIBLE_WITH_AXIOM__CLASSES);

    disjointAxiomEClass = createEClass(DISJOINT_AXIOM);
    createEReference(disjointAxiomEClass, DISJOINT_AXIOM__REF);
    createEReference(disjointAxiomEClass, DISJOINT_AXIOM__CLASSES);

    equivalentAxiomEClass = createEClass(EQUIVALENT_AXIOM);
    createEReference(equivalentAxiomEClass, EQUIVALENT_AXIOM__REF);
    createEReference(equivalentAxiomEClass, EQUIVALENT_AXIOM__CLASSES);

    unionAxiomEClass = createEClass(UNION_AXIOM);
    createEReference(unionAxiomEClass, UNION_AXIOM__CLASSES);

    intersectionAxiomEClass = createEClass(INTERSECTION_AXIOM);
    createEReference(intersectionAxiomEClass, INTERSECTION_AXIOM__CLASSES);

    oneOfAxiomEClass = createEClass(ONE_OF_AXIOM);
    createEReference(oneOfAxiomEClass, ONE_OF_AXIOM__VALUES);
    createEAttribute(oneOfAxiomEClass, ONE_OF_AXIOM__TAG_NAMES);
    createEReference(oneOfAxiomEClass, ONE_OF_AXIOM__TAGIDS);
    createEReference(oneOfAxiomEClass, ONE_OF_AXIOM__FIRST);
    createEReference(oneOfAxiomEClass, ONE_OF_AXIOM__REST);

    owlLabelEClass = createEClass(OWL_LABEL);
    createEAttribute(owlLabelEClass, OWL_LABEL__LANG);
    createEReference(owlLabelEClass, OWL_LABEL__DATATYPE);
    createEAttribute(owlLabelEClass, OWL_LABEL__VALUE);

    owlVersionEClass = createEClass(OWL_VERSION);
    createEAttribute(owlVersionEClass, OWL_VERSION__LANG);
    createEReference(owlVersionEClass, OWL_VERSION__DATATYPE);
    createEAttribute(owlVersionEClass, OWL_VERSION__VALUE);

    subTypeOfEClass = createEClass(SUB_TYPE_OF);
    createEAttribute(subTypeOfEClass, SUB_TYPE_OF__RESOURCE_BASED);
    createEReference(subTypeOfEClass, SUB_TYPE_OF__CLASS_REF);
    createEReference(subTypeOfEClass, SUB_TYPE_OF__MODIFIERS);

    classModifierEClass = createEClass(CLASS_MODIFIER);

    owlRestrictionEClass = createEClass(OWL_RESTRICTION);
    createEReference(owlRestrictionEClass, OWL_RESTRICTION__RESTRICTION_BODIES);
    createEReference(owlRestrictionEClass, OWL_RESTRICTION__PROP);
    createEReference(owlRestrictionEClass, OWL_RESTRICTION__PROPERTY);
    createEReference(owlRestrictionEClass, OWL_RESTRICTION__CLASS);
    createEReference(owlRestrictionEClass, OWL_RESTRICTION__CLASSES);

    restrictionBodyEClass = createEClass(RESTRICTION_BODY);

    minCardinalityEClass = createEClass(MIN_CARDINALITY);
    createEReference(minCardinalityEClass, MIN_CARDINALITY__DATATYPE);
    createEAttribute(minCardinalityEClass, MIN_CARDINALITY__VALUE);

    maxCardinalityEClass = createEClass(MAX_CARDINALITY);
    createEReference(maxCardinalityEClass, MAX_CARDINALITY__DATATYPE);
    createEAttribute(maxCardinalityEClass, MAX_CARDINALITY__VALUE);

    cardinalityEClass = createEClass(CARDINALITY);
    createEReference(cardinalityEClass, CARDINALITY__DATATYPE);
    createEAttribute(cardinalityEClass, CARDINALITY__VALUE);

    qualifiedCardinalityEClass = createEClass(QUALIFIED_CARDINALITY);
    createEReference(qualifiedCardinalityEClass, QUALIFIED_CARDINALITY__DATATYPE);
    createEAttribute(qualifiedCardinalityEClass, QUALIFIED_CARDINALITY__VALUE);

    minQualifiedCardinalityEClass = createEClass(MIN_QUALIFIED_CARDINALITY);
    createEReference(minQualifiedCardinalityEClass, MIN_QUALIFIED_CARDINALITY__DATATYPE);
    createEAttribute(minQualifiedCardinalityEClass, MIN_QUALIFIED_CARDINALITY__VALUE);

    maxQualifiedCardinalityEClass = createEClass(MAX_QUALIFIED_CARDINALITY);
    createEReference(maxQualifiedCardinalityEClass, MAX_QUALIFIED_CARDINALITY__DATATYPE);
    createEAttribute(maxQualifiedCardinalityEClass, MAX_QUALIFIED_CARDINALITY__VALUE);

    someValuesEClass = createEClass(SOME_VALUES);
    createEReference(someValuesEClass, SOME_VALUES__REF);
    createEReference(someValuesEClass, SOME_VALUES__MODIFIERS);

    allValuesEClass = createEClass(ALL_VALUES);
    createEReference(allValuesEClass, ALL_VALUES__REF);
    createEReference(allValuesEClass, ALL_VALUES__MODIFIERS);

    hasValueEClass = createEClass(HAS_VALUE);
    createEReference(hasValueEClass, HAS_VALUE__REF);
    createEReference(hasValueEClass, HAS_VALUE__TYPE);
    createEAttribute(hasValueEClass, HAS_VALUE__VALUE);

    propertyEClass = createEClass(PROPERTY);
    createEReference(propertyEClass, PROPERTY__ID);
    createEAttribute(propertyEClass, PROPERTY__REFERENCE);
    createEReference(propertyEClass, PROPERTY__REF);
    createEAttribute(propertyEClass, PROPERTY__OTHERS);
    createEReference(propertyEClass, PROPERTY__LABELS);
    createEReference(propertyEClass, PROPERTY__VERSION_INFO);
    createEReference(propertyEClass, PROPERTY__RANGES);
    createEReference(propertyEClass, PROPERTY__TYPES);
    createEReference(propertyEClass, PROPERTY__DOMAINS);
    createEReference(propertyEClass, PROPERTY__MODIFIERS);

    annotationPropertyEClass = createEClass(ANNOTATION_PROPERTY);

    objectPropertyEClass = createEClass(OBJECT_PROPERTY);

    datatypePropertyEClass = createEClass(DATATYPE_PROPERTY);

    functionalPropertyEClass = createEClass(FUNCTIONAL_PROPERTY);

    inverseFunctionalPropertyEClass = createEClass(INVERSE_FUNCTIONAL_PROPERTY);

    symmetricPropertyEClass = createEClass(SYMMETRIC_PROPERTY);

    transitivePropertyEClass = createEClass(TRANSITIVE_PROPERTY);

    propertyModifierEClass = createEClass(PROPERTY_MODIFIER);
    createEReference(propertyModifierEClass, PROPERTY_MODIFIER__PROPS);

    inverseTypeEClass = createEClass(INVERSE_TYPE);
    createEReference(inverseTypeEClass, INVERSE_TYPE__REF);

    equivalentTypeEClass = createEClass(EQUIVALENT_TYPE);
    createEReference(equivalentTypeEClass, EQUIVALENT_TYPE__REF);

    rdfsDomainEClass = createEClass(RDFS_DOMAIN);

    rdfsRangeEClass = createEClass(RDFS_RANGE);

    owlDataRangeEClass = createEClass(OWL_DATA_RANGE);
    createEReference(owlDataRangeEClass, OWL_DATA_RANGE__OWL_RANGE);
    createEReference(owlDataRangeEClass, OWL_DATA_RANGE__UNION);
    createEReference(owlDataRangeEClass, OWL_DATA_RANGE__ONE_OF);
    createEReference(owlDataRangeEClass, OWL_DATA_RANGE__INTERSECTION);

    rdfTypeEClass = createEClass(RDF_TYPE);
    createEReference(rdfTypeEClass, RDF_TYPE__TYPE);

    subPropertyOfEClass = createEClass(SUB_PROPERTY_OF);
    createEReference(subPropertyOfEClass, SUB_PROPERTY_OF__TYPE);

    descriptionEClass = createEClass(DESCRIPTION);
    createEReference(descriptionEClass, DESCRIPTION__ABOUT);
    createEReference(descriptionEClass, DESCRIPTION__FIRST);
    createEReference(descriptionEClass, DESCRIPTION__TYPE);
    createEReference(descriptionEClass, DESCRIPTION__DOMAIN);
    createEReference(descriptionEClass, DESCRIPTION__RANGE);
    createEReference(descriptionEClass, DESCRIPTION__PROP_MODIFIERS);
    createEReference(descriptionEClass, DESCRIPTION__LABELS);
    createEReference(descriptionEClass, DESCRIPTION__AXIOMS);

    owlFirstEClass = createEClass(OWL_FIRST);
    createEReference(owlFirstEClass, OWL_FIRST__REF_TYPE);
    createEReference(owlFirstEClass, OWL_FIRST__DATA_TYPE);
    createEAttribute(owlFirstEClass, OWL_FIRST__DATA_TYPE_VALUE);

    rdfRestEClass = createEClass(RDF_REST);
    createEReference(rdfRestEClass, RDF_REST__REF);
    createEReference(rdfRestEClass, RDF_REST__PARSE_TYPE);
    createEReference(rdfRestEClass, RDF_REST__FIRST);
    createEReference(rdfRestEClass, RDF_REST__REST);

    owlTypeEClass = createEClass(OWL_TYPE);
    createEReference(owlTypeEClass, OWL_TYPE__REF_TYPE);
    createEReference(owlTypeEClass, OWL_TYPE__MEMBERS);

    owlMembersEClass = createEClass(OWL_MEMBERS);
    createEReference(owlMembersEClass, OWL_MEMBERS__PARSE_TYPE);
    createEReference(owlMembersEClass, OWL_MEMBERS__DESCRIPTIONS);

    datatypeEClass = createEClass(DATATYPE);
    createEReference(datatypeEClass, DATATYPE__ID);
    createEAttribute(datatypeEClass, DATATYPE__REFERENCE);
    createEReference(datatypeEClass, DATATYPE__REF);

    simpleRDFSDomainEClass = createEClass(SIMPLE_RDFS_DOMAIN);
    createEReference(simpleRDFSDomainEClass, SIMPLE_RDFS_DOMAIN__REF);

    parsedRDFSDomainEClass = createEClass(PARSED_RDFS_DOMAIN);
    createEReference(parsedRDFSDomainEClass, PARSED_RDFS_DOMAIN__PARSE_TYPE);
    createEReference(parsedRDFSDomainEClass, PARSED_RDFS_DOMAIN__MODIFIERS);

    simpleRDFSRangeEClass = createEClass(SIMPLE_RDFS_RANGE);
    createEReference(simpleRDFSRangeEClass, SIMPLE_RDFS_RANGE__RANGE);

    parsedPropertyRangeEClass = createEClass(PARSED_PROPERTY_RANGE);
    createEReference(parsedPropertyRangeEClass, PARSED_PROPERTY_RANGE__PARSE_TYPE);
    createEReference(parsedPropertyRangeEClass, PARSED_PROPERTY_RANGE__MODIFIERS);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isInitialized = false;

  /**
   * Complete the initialization of the package and its meta-model.  This
   * method is guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void initializePackageContents()
  {
    if (isInitialized) return;
    isInitialized = true;

    // Initialize package
    setName(eNAME);
    setNsPrefix(eNS_PREFIX);
    setNsURI(eNS_URI);

    // Create type parameters

    // Set bounds for type parameters

    // Add supertypes to classes
    namedIndividualEClass.getESuperTypes().add(this.getOntologyElement());
    ontologyEClass.getESuperTypes().add(this.getOntologyElement());
    owlClassEClass.getESuperTypes().add(this.getOntologyElement());
    owlClassEClass.getESuperTypes().add(this.getClassModifier());
    minimumClassEClass.getESuperTypes().add(this.getClassModifier());
    complementAxiomEClass.getESuperTypes().add(this.getClassAxiom());
    incompatibleWithAxiomEClass.getESuperTypes().add(this.getClassAxiom());
    disjointAxiomEClass.getESuperTypes().add(this.getClassAxiom());
    equivalentAxiomEClass.getESuperTypes().add(this.getClassAxiom());
    unionAxiomEClass.getESuperTypes().add(this.getClassAxiom());
    intersectionAxiomEClass.getESuperTypes().add(this.getClassAxiom());
    oneOfAxiomEClass.getESuperTypes().add(this.getClassAxiom());
    owlRestrictionEClass.getESuperTypes().add(this.getClassModifier());
    minCardinalityEClass.getESuperTypes().add(this.getRestrictionBody());
    maxCardinalityEClass.getESuperTypes().add(this.getRestrictionBody());
    cardinalityEClass.getESuperTypes().add(this.getRestrictionBody());
    qualifiedCardinalityEClass.getESuperTypes().add(this.getRestrictionBody());
    minQualifiedCardinalityEClass.getESuperTypes().add(this.getRestrictionBody());
    maxQualifiedCardinalityEClass.getESuperTypes().add(this.getRestrictionBody());
    someValuesEClass.getESuperTypes().add(this.getRestrictionBody());
    allValuesEClass.getESuperTypes().add(this.getRestrictionBody());
    hasValueEClass.getESuperTypes().add(this.getRestrictionBody());
    propertyEClass.getESuperTypes().add(this.getOntologyElement());
    annotationPropertyEClass.getESuperTypes().add(this.getProperty());
    objectPropertyEClass.getESuperTypes().add(this.getProperty());
    datatypePropertyEClass.getESuperTypes().add(this.getProperty());
    functionalPropertyEClass.getESuperTypes().add(this.getProperty());
    inverseFunctionalPropertyEClass.getESuperTypes().add(this.getProperty());
    symmetricPropertyEClass.getESuperTypes().add(this.getProperty());
    transitivePropertyEClass.getESuperTypes().add(this.getProperty());
    inverseTypeEClass.getESuperTypes().add(this.getPropertyModifier());
    equivalentTypeEClass.getESuperTypes().add(this.getPropertyModifier());
    owlDataRangeEClass.getESuperTypes().add(this.getRDFSRange());
    subPropertyOfEClass.getESuperTypes().add(this.getPropertyModifier());
    descriptionEClass.getESuperTypes().add(this.getOntologyElement());
    descriptionEClass.getESuperTypes().add(this.getClassModifier());
    datatypeEClass.getESuperTypes().add(this.getOntologyElement());
    simpleRDFSDomainEClass.getESuperTypes().add(this.getRDFSDomain());
    parsedRDFSDomainEClass.getESuperTypes().add(this.getRDFSDomain());
    simpleRDFSRangeEClass.getESuperTypes().add(this.getRDFSRange());
    parsedPropertyRangeEClass.getESuperTypes().add(this.getRDFSRange());

    // Initialize classes and features; add operations and parameters
    initEClass(owlModelEClass, OWLModel.class, "OWLModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getOWLModel_Header(), ecorePackage.getEString(), "header", null, 0, 1, OWLModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOWLModel_DocType(), this.getDocType(), null, "docType", null, 0, 1, OWLModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOWLModel_Rdf(), this.getRDFElement(), null, "rdf", null, 0, 1, OWLModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(docTypeEClass, DocType.class, "DocType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getDocType_DocID(), ecorePackage.getEString(), "docID", null, 0, 1, DocType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDocType_Entities(), this.getDocEntity(), null, "entities", null, 0, -1, DocType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(docEntityEClass, DocEntity.class, "DocEntity", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getDocEntity_EntityID(), ecorePackage.getEString(), "entityID", null, 0, 1, DocEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getDocEntity_EntityURI(), ecorePackage.getEString(), "entityURI", null, 0, 1, DocEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(rdfElementEClass, RDFElement.class, "RDFElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getRDFElement_ImportedNs(), this.getXmlNs(), null, "importedNs", null, 0, -1, RDFElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getRDFElement_BaseOnt(), ecorePackage.getEString(), "baseOnt", null, 0, 1, RDFElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getRDFElement_OntologyElements(), this.getOntologyElement(), null, "ontologyElements", null, 0, -1, RDFElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(xmlNsEClass, XmlNs.class, "XmlNs", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getXmlNs_Nsid(), ecorePackage.getEString(), "nsid", null, 0, 1, XmlNs.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getXmlNs_Nsuri(), ecorePackage.getEString(), "nsuri", null, 0, 1, XmlNs.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getXmlNs_Nsname(), ecorePackage.getEString(), "nsname", null, 0, 1, XmlNs.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(owlRefEClass, OwlRef.class, "OwlRef", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getOwlRef_OntURI(), ecorePackage.getEString(), "ontURI", null, 0, 1, OwlRef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getOwlRef_OntName(), ecorePackage.getEString(), "ontName", null, 0, 1, OwlRef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getOwlRef_ElemID(), ecorePackage.getEString(), "elemID", null, 0, 1, OwlRef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getOwlRef_FullURI(), ecorePackage.getEString(), "fullURI", null, 0, 1, OwlRef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(owlIDEClass, OwlID.class, "OwlID", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getOwlID_ElemID(), ecorePackage.getEString(), "elemID", null, 0, 1, OwlID.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(ontologyIDEClass, OntologyID.class, "OntologyID", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getOntologyID_SuperOnt(), ecorePackage.getEString(), "superOnt", null, 0, 1, OntologyID.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getOntologyID_OntURI(), ecorePackage.getEString(), "ontURI", null, 0, 1, OntologyID.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(parseTypeEClass, ParseType.class, "ParseType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getParseType_Type(), ecorePackage.getEString(), "type", null, 0, 1, ParseType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(ontologyElementEClass, OntologyElement.class, "OntologyElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(namedIndividualEClass, NamedIndividual.class, "NamedIndividual", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getNamedIndividual_Ref(), this.getOwlRef(), null, "ref", null, 0, 1, NamedIndividual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getNamedIndividual_Types(), this.getRDFType(), null, "types", null, 0, -1, NamedIndividual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getNamedIndividual_Desc(), ecorePackage.getEString(), "desc", null, 0, 1, NamedIndividual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(ontologyEClass, Ontology.class, "Ontology", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getOntology_About(), this.getOntologyID(), null, "about", null, 0, 1, Ontology.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOntology_Resource(), this.getOwlRef(), null, "resource", null, 0, 1, Ontology.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOntology_Labels(), this.getOwlLabel(), null, "labels", null, 0, -1, Ontology.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOntology_VersionInfo(), this.getOwlVersion(), null, "versionInfo", null, 0, -1, Ontology.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOntology_SeeAlso(), this.getSeeAlso(), null, "seeAlso", null, 0, -1, Ontology.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOntology_PVers(), this.getPriorVersion(), null, "pVers", null, 0, -1, Ontology.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOntology_BackComp(), this.getBackwardsComp(), null, "backComp", null, 0, -1, Ontology.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOntology_ImportedOntologies(), this.getOwlImport(), null, "importedOntologies", null, 0, -1, Ontology.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getOntology_Others(), ecorePackage.getEString(), "others", null, 0, -1, Ontology.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(seeAlsoEClass, SeeAlso.class, "SeeAlso", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getSeeAlso_Ref(), this.getOntologyID(), null, "ref", null, 0, 1, SeeAlso.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(priorVersionEClass, PriorVersion.class, "PriorVersion", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getPriorVersion_Ref(), this.getOntologyID(), null, "ref", null, 0, 1, PriorVersion.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(backwardsCompEClass, BackwardsComp.class, "BackwardsComp", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getBackwardsComp_Ref(), this.getOntologyID(), null, "ref", null, 0, 1, BackwardsComp.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(owlImportEClass, OwlImport.class, "OwlImport", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getOwlImport_Ref(), this.getOntologyID(), null, "ref", null, 0, 1, OwlImport.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOwlImport_Onts(), this.getOntology(), null, "onts", null, 0, -1, OwlImport.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(owlClassEClass, OwlClass.class, "OwlClass", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getOwlClass_Id(), this.getOwlID(), null, "id", null, 0, 1, OwlClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getOwlClass_Reference(), ecorePackage.getEBoolean(), "reference", null, 0, 1, OwlClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOwlClass_Ref(), this.getOwlRef(), null, "ref", null, 0, 1, OwlClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOwlClass_Labels(), this.getOwlLabel(), null, "labels", null, 0, -1, OwlClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOwlClass_VersionInfo(), this.getOwlVersion(), null, "versionInfo", null, 0, -1, OwlClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOwlClass_SuperTypes(), this.getSubTypeOf(), null, "superTypes", null, 0, -1, OwlClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOwlClass_Axioms(), this.getClassAxiom(), null, "axioms", null, 0, -1, OwlClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOwlClass_Onts(), this.getOntology(), null, "onts", null, 0, -1, OwlClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getOwlClass_Others(), ecorePackage.getEString(), "others", null, 0, -1, OwlClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(minimumClassEClass, MinimumClass.class, "MinimumClass", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getMinimumClass_Labels(), this.getOwlLabel(), null, "labels", null, 0, -1, MinimumClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getMinimumClass_Axioms(), this.getClassAxiom(), null, "axioms", null, 0, -1, MinimumClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getMinimumClass_Others(), ecorePackage.getEString(), "others", null, 0, -1, MinimumClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(classAxiomEClass, ClassAxiom.class, "ClassAxiom", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getClassAxiom_ParseType(), this.getParseType(), null, "parseType", null, 0, 1, ClassAxiom.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(complementAxiomEClass, ComplementAxiom.class, "ComplementAxiom", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getComplementAxiom_Ref(), this.getOwlRef(), null, "ref", null, 0, 1, ComplementAxiom.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getComplementAxiom_Classes(), this.getClassModifier(), null, "classes", null, 0, -1, ComplementAxiom.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(incompatibleWithAxiomEClass, IncompatibleWithAxiom.class, "IncompatibleWithAxiom", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getIncompatibleWithAxiom_Ref(), this.getOwlRef(), null, "ref", null, 0, 1, IncompatibleWithAxiom.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getIncompatibleWithAxiom_Classes(), this.getClassModifier(), null, "classes", null, 0, -1, IncompatibleWithAxiom.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(disjointAxiomEClass, DisjointAxiom.class, "DisjointAxiom", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getDisjointAxiom_Ref(), this.getOwlRef(), null, "ref", null, 0, 1, DisjointAxiom.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDisjointAxiom_Classes(), this.getClassModifier(), null, "classes", null, 0, -1, DisjointAxiom.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(equivalentAxiomEClass, EquivalentAxiom.class, "EquivalentAxiom", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getEquivalentAxiom_Ref(), this.getOwlRef(), null, "ref", null, 0, 1, EquivalentAxiom.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getEquivalentAxiom_Classes(), this.getClassModifier(), null, "classes", null, 0, -1, EquivalentAxiom.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(unionAxiomEClass, UnionAxiom.class, "UnionAxiom", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getUnionAxiom_Classes(), this.getClassModifier(), null, "classes", null, 0, -1, UnionAxiom.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(intersectionAxiomEClass, IntersectionAxiom.class, "IntersectionAxiom", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getIntersectionAxiom_Classes(), this.getClassModifier(), null, "classes", null, 0, -1, IntersectionAxiom.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(oneOfAxiomEClass, OneOfAxiom.class, "OneOfAxiom", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getOneOfAxiom_Values(), ecorePackage.getEObject(), null, "values", null, 0, -1, OneOfAxiom.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getOneOfAxiom_TagNames(), ecorePackage.getEString(), "tagNames", null, 0, -1, OneOfAxiom.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOneOfAxiom_Tagids(), ecorePackage.getEObject(), null, "tagids", null, 0, -1, OneOfAxiom.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOneOfAxiom_First(), this.getOwlFirst(), null, "first", null, 0, -1, OneOfAxiom.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOneOfAxiom_Rest(), this.getRDFRest(), null, "rest", null, 0, -1, OneOfAxiom.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(owlLabelEClass, OwlLabel.class, "OwlLabel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getOwlLabel_Lang(), ecorePackage.getEString(), "lang", null, 0, 1, OwlLabel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOwlLabel_Datatype(), this.getOwlRef(), null, "datatype", null, 0, 1, OwlLabel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getOwlLabel_Value(), ecorePackage.getEString(), "value", null, 0, 1, OwlLabel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(owlVersionEClass, OwlVersion.class, "OwlVersion", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getOwlVersion_Lang(), ecorePackage.getEString(), "lang", null, 0, 1, OwlVersion.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOwlVersion_Datatype(), this.getOwlRef(), null, "datatype", null, 0, 1, OwlVersion.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getOwlVersion_Value(), ecorePackage.getEString(), "value", null, 0, 1, OwlVersion.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(subTypeOfEClass, SubTypeOf.class, "SubTypeOf", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getSubTypeOf_ResourceBased(), ecorePackage.getEBoolean(), "resourceBased", null, 0, 1, SubTypeOf.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSubTypeOf_ClassRef(), this.getOwlRef(), null, "classRef", null, 0, 1, SubTypeOf.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSubTypeOf_Modifiers(), this.getClassModifier(), null, "modifiers", null, 0, -1, SubTypeOf.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(classModifierEClass, ClassModifier.class, "ClassModifier", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(owlRestrictionEClass, OwlRestriction.class, "OwlRestriction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getOwlRestriction_RestrictionBodies(), this.getRestrictionBody(), null, "restrictionBodies", null, 0, -1, OwlRestriction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOwlRestriction_Prop(), this.getOwlRef(), null, "prop", null, 0, 1, OwlRestriction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOwlRestriction_Property(), this.getProperty(), null, "property", null, 0, 1, OwlRestriction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOwlRestriction_Class(), this.getOwlRef(), null, "class", null, 0, 1, OwlRestriction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOwlRestriction_Classes(), this.getClassModifier(), null, "classes", null, 0, -1, OwlRestriction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(restrictionBodyEClass, RestrictionBody.class, "RestrictionBody", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(minCardinalityEClass, MinCardinality.class, "MinCardinality", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getMinCardinality_Datatype(), this.getOwlRef(), null, "datatype", null, 0, 1, MinCardinality.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getMinCardinality_Value(), ecorePackage.getEInt(), "value", null, 0, 1, MinCardinality.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(maxCardinalityEClass, MaxCardinality.class, "MaxCardinality", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getMaxCardinality_Datatype(), this.getOwlRef(), null, "datatype", null, 0, 1, MaxCardinality.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getMaxCardinality_Value(), ecorePackage.getEInt(), "value", null, 0, 1, MaxCardinality.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(cardinalityEClass, Cardinality.class, "Cardinality", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getCardinality_Datatype(), this.getOwlRef(), null, "datatype", null, 0, 1, Cardinality.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getCardinality_Value(), ecorePackage.getEInt(), "value", null, 0, 1, Cardinality.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(qualifiedCardinalityEClass, QualifiedCardinality.class, "QualifiedCardinality", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getQualifiedCardinality_Datatype(), this.getOwlRef(), null, "datatype", null, 0, 1, QualifiedCardinality.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getQualifiedCardinality_Value(), ecorePackage.getEInt(), "value", null, 0, 1, QualifiedCardinality.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(minQualifiedCardinalityEClass, MinQualifiedCardinality.class, "MinQualifiedCardinality", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getMinQualifiedCardinality_Datatype(), this.getOwlRef(), null, "datatype", null, 0, 1, MinQualifiedCardinality.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getMinQualifiedCardinality_Value(), ecorePackage.getEInt(), "value", null, 0, 1, MinQualifiedCardinality.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(maxQualifiedCardinalityEClass, MaxQualifiedCardinality.class, "MaxQualifiedCardinality", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getMaxQualifiedCardinality_Datatype(), this.getOwlRef(), null, "datatype", null, 0, 1, MaxQualifiedCardinality.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getMaxQualifiedCardinality_Value(), ecorePackage.getEInt(), "value", null, 0, 1, MaxQualifiedCardinality.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(someValuesEClass, SomeValues.class, "SomeValues", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getSomeValues_Ref(), this.getOwlRef(), null, "ref", null, 0, 1, SomeValues.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSomeValues_Modifiers(), this.getClassModifier(), null, "modifiers", null, 0, -1, SomeValues.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(allValuesEClass, AllValues.class, "AllValues", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getAllValues_Ref(), this.getOwlRef(), null, "ref", null, 0, 1, AllValues.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getAllValues_Modifiers(), this.getClassModifier(), null, "modifiers", null, 0, -1, AllValues.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(hasValueEClass, HasValue.class, "HasValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getHasValue_Ref(), this.getOwlRef(), null, "ref", null, 0, 1, HasValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getHasValue_Type(), this.getOwlRef(), null, "type", null, 0, 1, HasValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getHasValue_Value(), ecorePackage.getEString(), "value", null, 0, 1, HasValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(propertyEClass, Property.class, "Property", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getProperty_Id(), this.getOwlID(), null, "id", null, 0, 1, Property.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getProperty_Reference(), ecorePackage.getEBoolean(), "reference", null, 0, 1, Property.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getProperty_Ref(), this.getOwlRef(), null, "ref", null, 0, 1, Property.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getProperty_Others(), ecorePackage.getEString(), "others", null, 0, -1, Property.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getProperty_Labels(), this.getOwlLabel(), null, "labels", null, 0, -1, Property.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getProperty_VersionInfo(), this.getOwlVersion(), null, "versionInfo", null, 0, -1, Property.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getProperty_Ranges(), this.getRDFSRange(), null, "ranges", null, 0, -1, Property.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getProperty_Types(), this.getRDFType(), null, "types", null, 0, -1, Property.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getProperty_Domains(), this.getRDFSDomain(), null, "domains", null, 0, -1, Property.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getProperty_Modifiers(), this.getPropertyModifier(), null, "modifiers", null, 0, -1, Property.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(annotationPropertyEClass, AnnotationProperty.class, "AnnotationProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(objectPropertyEClass, ObjectProperty.class, "ObjectProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(datatypePropertyEClass, DatatypeProperty.class, "DatatypeProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(functionalPropertyEClass, FunctionalProperty.class, "FunctionalProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(inverseFunctionalPropertyEClass, InverseFunctionalProperty.class, "InverseFunctionalProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(symmetricPropertyEClass, SymmetricProperty.class, "SymmetricProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(transitivePropertyEClass, TransitiveProperty.class, "TransitiveProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(propertyModifierEClass, PropertyModifier.class, "PropertyModifier", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getPropertyModifier_Props(), this.getProperty(), null, "props", null, 0, -1, PropertyModifier.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(inverseTypeEClass, InverseType.class, "InverseType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getInverseType_Ref(), this.getOwlRef(), null, "ref", null, 0, 1, InverseType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(equivalentTypeEClass, EquivalentType.class, "EquivalentType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getEquivalentType_Ref(), this.getOwlRef(), null, "ref", null, 0, 1, EquivalentType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(rdfsDomainEClass, RDFSDomain.class, "RDFSDomain", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(rdfsRangeEClass, RDFSRange.class, "RDFSRange", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(owlDataRangeEClass, OwlDataRange.class, "OwlDataRange", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getOwlDataRange_OwlRange(), this.getOwlDataRange(), null, "owlRange", null, 0, 1, OwlDataRange.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOwlDataRange_Union(), this.getUnionAxiom(), null, "union", null, 0, 1, OwlDataRange.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOwlDataRange_OneOf(), this.getOneOfAxiom(), null, "oneOf", null, 0, 1, OwlDataRange.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOwlDataRange_Intersection(), this.getIntersectionAxiom(), null, "intersection", null, 0, 1, OwlDataRange.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(rdfTypeEClass, RDFType.class, "RDFType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getRDFType_Type(), this.getOwlRef(), null, "type", null, 0, 1, RDFType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(subPropertyOfEClass, SubPropertyOf.class, "SubPropertyOf", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getSubPropertyOf_Type(), this.getOwlRef(), null, "type", null, 0, 1, SubPropertyOf.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(descriptionEClass, Description.class, "Description", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getDescription_About(), this.getOwlRef(), null, "about", null, 0, 1, Description.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDescription_First(), this.getOwlFirst(), null, "first", null, 0, -1, Description.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDescription_Type(), this.getOwlType(), null, "type", null, 0, -1, Description.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDescription_Domain(), this.getRDFSDomain(), null, "domain", null, 0, -1, Description.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDescription_Range(), this.getRDFSRange(), null, "range", null, 0, -1, Description.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDescription_PropModifiers(), this.getPropertyModifier(), null, "propModifiers", null, 0, -1, Description.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDescription_Labels(), this.getOwlLabel(), null, "labels", null, 0, -1, Description.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDescription_Axioms(), this.getClassAxiom(), null, "axioms", null, 0, -1, Description.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(owlFirstEClass, OwlFirst.class, "OwlFirst", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getOwlFirst_RefType(), this.getOwlRef(), null, "refType", null, 0, 1, OwlFirst.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOwlFirst_DataType(), this.getOwlRef(), null, "dataType", null, 0, 1, OwlFirst.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getOwlFirst_DataTypeValue(), ecorePackage.getEString(), "dataTypeValue", null, 0, 1, OwlFirst.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(rdfRestEClass, RDFRest.class, "RDFRest", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getRDFRest_Ref(), this.getOwlRef(), null, "ref", null, 0, 1, RDFRest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getRDFRest_ParseType(), this.getParseType(), null, "parseType", null, 0, 1, RDFRest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getRDFRest_First(), this.getOwlFirst(), null, "first", null, 0, -1, RDFRest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getRDFRest_Rest(), this.getRDFRest(), null, "rest", null, 0, -1, RDFRest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(owlTypeEClass, OwlType.class, "OwlType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getOwlType_RefType(), this.getOwlRef(), null, "refType", null, 0, 1, OwlType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOwlType_Members(), this.getOwlMembers(), null, "members", null, 0, 1, OwlType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(owlMembersEClass, OwlMembers.class, "OwlMembers", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getOwlMembers_ParseType(), this.getParseType(), null, "parseType", null, 0, 1, OwlMembers.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOwlMembers_Descriptions(), this.getDescription(), null, "descriptions", null, 0, -1, OwlMembers.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(datatypeEClass, Datatype.class, "Datatype", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getDatatype_Id(), this.getOwlID(), null, "id", null, 0, 1, Datatype.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getDatatype_Reference(), ecorePackage.getEBoolean(), "reference", null, 0, 1, Datatype.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDatatype_Ref(), this.getOwlRef(), null, "ref", null, 0, 1, Datatype.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(simpleRDFSDomainEClass, SimpleRDFSDomain.class, "SimpleRDFSDomain", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getSimpleRDFSDomain_Ref(), this.getOwlRef(), null, "ref", null, 0, 1, SimpleRDFSDomain.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(parsedRDFSDomainEClass, ParsedRDFSDomain.class, "ParsedRDFSDomain", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getParsedRDFSDomain_ParseType(), this.getParseType(), null, "parseType", null, 0, 1, ParsedRDFSDomain.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getParsedRDFSDomain_Modifiers(), this.getClassModifier(), null, "modifiers", null, 0, -1, ParsedRDFSDomain.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(simpleRDFSRangeEClass, SimpleRDFSRange.class, "SimpleRDFSRange", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getSimpleRDFSRange_Range(), this.getOwlRef(), null, "range", null, 0, 1, SimpleRDFSRange.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(parsedPropertyRangeEClass, ParsedPropertyRange.class, "ParsedPropertyRange", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getParsedPropertyRange_ParseType(), this.getParseType(), null, "parseType", null, 0, 1, ParsedPropertyRange.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getParsedPropertyRange_Modifiers(), this.getClassModifier(), null, "modifiers", null, 0, -1, ParsedPropertyRange.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    // Create resource
    createResource(eNS_URI);
  }

} //OwlmodelPackageImpl
