/**
 */
package org.smool.sdk.owlparser.owlmodel.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.smool.sdk.owlparser.owlmodel.ClassAxiom;
import org.smool.sdk.owlparser.owlmodel.OwlmodelPackage;
import org.smool.sdk.owlparser.owlmodel.ParseType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Class Axiom</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.ClassAxiomImpl#getParseType <em>Parse Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ClassAxiomImpl extends MinimalEObjectImpl.Container implements ClassAxiom
{
  /**
   * The cached value of the '{@link #getParseType() <em>Parse Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParseType()
   * @generated
   * @ordered
   */
  protected ParseType parseType;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ClassAxiomImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return OwlmodelPackage.Literals.CLASS_AXIOM;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public ParseType getParseType()
  {
    return parseType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetParseType(ParseType newParseType, NotificationChain msgs)
  {
    ParseType oldParseType = parseType;
    parseType = newParseType;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlmodelPackage.CLASS_AXIOM__PARSE_TYPE, oldParseType, newParseType);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setParseType(ParseType newParseType)
  {
    if (newParseType != parseType)
    {
      NotificationChain msgs = null;
      if (parseType != null)
        msgs = ((InternalEObject)parseType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.CLASS_AXIOM__PARSE_TYPE, null, msgs);
      if (newParseType != null)
        msgs = ((InternalEObject)newParseType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.CLASS_AXIOM__PARSE_TYPE, null, msgs);
      msgs = basicSetParseType(newParseType, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.CLASS_AXIOM__PARSE_TYPE, newParseType, newParseType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case OwlmodelPackage.CLASS_AXIOM__PARSE_TYPE:
        return basicSetParseType(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case OwlmodelPackage.CLASS_AXIOM__PARSE_TYPE:
        return getParseType();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case OwlmodelPackage.CLASS_AXIOM__PARSE_TYPE:
        setParseType((ParseType)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.CLASS_AXIOM__PARSE_TYPE:
        setParseType((ParseType)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.CLASS_AXIOM__PARSE_TYPE:
        return parseType != null;
    }
    return super.eIsSet(featureID);
  }

} //ClassAxiomImpl
