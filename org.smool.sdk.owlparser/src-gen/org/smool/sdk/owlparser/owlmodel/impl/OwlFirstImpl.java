/**
 */
package org.smool.sdk.owlparser.owlmodel.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.smool.sdk.owlparser.owlmodel.OwlFirst;
import org.smool.sdk.owlparser.owlmodel.OwlRef;
import org.smool.sdk.owlparser.owlmodel.OwlmodelPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Owl First</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OwlFirstImpl#getRefType <em>Ref Type</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OwlFirstImpl#getDataType <em>Data Type</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OwlFirstImpl#getDataTypeValue <em>Data Type Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OwlFirstImpl extends MinimalEObjectImpl.Container implements OwlFirst
{
  /**
   * The cached value of the '{@link #getRefType() <em>Ref Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRefType()
   * @generated
   * @ordered
   */
  protected OwlRef refType;

  /**
   * The cached value of the '{@link #getDataType() <em>Data Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDataType()
   * @generated
   * @ordered
   */
  protected OwlRef dataType;

  /**
   * The default value of the '{@link #getDataTypeValue() <em>Data Type Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDataTypeValue()
   * @generated
   * @ordered
   */
  protected static final String DATA_TYPE_VALUE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getDataTypeValue() <em>Data Type Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDataTypeValue()
   * @generated
   * @ordered
   */
  protected String dataTypeValue = DATA_TYPE_VALUE_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected OwlFirstImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return OwlmodelPackage.Literals.OWL_FIRST;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OwlRef getRefType()
  {
    return refType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetRefType(OwlRef newRefType, NotificationChain msgs)
  {
    OwlRef oldRefType = refType;
    refType = newRefType;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_FIRST__REF_TYPE, oldRefType, newRefType);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setRefType(OwlRef newRefType)
  {
    if (newRefType != refType)
    {
      NotificationChain msgs = null;
      if (refType != null)
        msgs = ((InternalEObject)refType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.OWL_FIRST__REF_TYPE, null, msgs);
      if (newRefType != null)
        msgs = ((InternalEObject)newRefType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.OWL_FIRST__REF_TYPE, null, msgs);
      msgs = basicSetRefType(newRefType, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_FIRST__REF_TYPE, newRefType, newRefType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OwlRef getDataType()
  {
    return dataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDataType(OwlRef newDataType, NotificationChain msgs)
  {
    OwlRef oldDataType = dataType;
    dataType = newDataType;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_FIRST__DATA_TYPE, oldDataType, newDataType);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setDataType(OwlRef newDataType)
  {
    if (newDataType != dataType)
    {
      NotificationChain msgs = null;
      if (dataType != null)
        msgs = ((InternalEObject)dataType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.OWL_FIRST__DATA_TYPE, null, msgs);
      if (newDataType != null)
        msgs = ((InternalEObject)newDataType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.OWL_FIRST__DATA_TYPE, null, msgs);
      msgs = basicSetDataType(newDataType, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_FIRST__DATA_TYPE, newDataType, newDataType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getDataTypeValue()
  {
    return dataTypeValue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setDataTypeValue(String newDataTypeValue)
  {
    String oldDataTypeValue = dataTypeValue;
    dataTypeValue = newDataTypeValue;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_FIRST__DATA_TYPE_VALUE, oldDataTypeValue, dataTypeValue));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_FIRST__REF_TYPE:
        return basicSetRefType(null, msgs);
      case OwlmodelPackage.OWL_FIRST__DATA_TYPE:
        return basicSetDataType(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_FIRST__REF_TYPE:
        return getRefType();
      case OwlmodelPackage.OWL_FIRST__DATA_TYPE:
        return getDataType();
      case OwlmodelPackage.OWL_FIRST__DATA_TYPE_VALUE:
        return getDataTypeValue();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_FIRST__REF_TYPE:
        setRefType((OwlRef)newValue);
        return;
      case OwlmodelPackage.OWL_FIRST__DATA_TYPE:
        setDataType((OwlRef)newValue);
        return;
      case OwlmodelPackage.OWL_FIRST__DATA_TYPE_VALUE:
        setDataTypeValue((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_FIRST__REF_TYPE:
        setRefType((OwlRef)null);
        return;
      case OwlmodelPackage.OWL_FIRST__DATA_TYPE:
        setDataType((OwlRef)null);
        return;
      case OwlmodelPackage.OWL_FIRST__DATA_TYPE_VALUE:
        setDataTypeValue(DATA_TYPE_VALUE_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_FIRST__REF_TYPE:
        return refType != null;
      case OwlmodelPackage.OWL_FIRST__DATA_TYPE:
        return dataType != null;
      case OwlmodelPackage.OWL_FIRST__DATA_TYPE_VALUE:
        return DATA_TYPE_VALUE_EDEFAULT == null ? dataTypeValue != null : !DATA_TYPE_VALUE_EDEFAULT.equals(dataTypeValue);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (dataTypeValue: ");
    result.append(dataTypeValue);
    result.append(')');
    return result.toString();
  }

} //OwlFirstImpl
