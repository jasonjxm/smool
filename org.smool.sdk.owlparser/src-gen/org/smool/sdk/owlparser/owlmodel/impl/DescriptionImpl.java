/**
 */
package org.smool.sdk.owlparser.owlmodel.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.smool.sdk.owlparser.owlmodel.ClassAxiom;
import org.smool.sdk.owlparser.owlmodel.Description;
import org.smool.sdk.owlparser.owlmodel.OwlFirst;
import org.smool.sdk.owlparser.owlmodel.OwlLabel;
import org.smool.sdk.owlparser.owlmodel.OwlRef;
import org.smool.sdk.owlparser.owlmodel.OwlType;
import org.smool.sdk.owlparser.owlmodel.OwlmodelPackage;
import org.smool.sdk.owlparser.owlmodel.PropertyModifier;
import org.smool.sdk.owlparser.owlmodel.RDFSDomain;
import org.smool.sdk.owlparser.owlmodel.RDFSRange;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Description</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.DescriptionImpl#getAbout <em>About</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.DescriptionImpl#getFirst <em>First</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.DescriptionImpl#getType <em>Type</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.DescriptionImpl#getDomain <em>Domain</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.DescriptionImpl#getRange <em>Range</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.DescriptionImpl#getPropModifiers <em>Prop Modifiers</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.DescriptionImpl#getLabels <em>Labels</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.DescriptionImpl#getAxioms <em>Axioms</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DescriptionImpl extends OntologyElementImpl implements Description
{
  /**
   * The cached value of the '{@link #getAbout() <em>About</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAbout()
   * @generated
   * @ordered
   */
  protected OwlRef about;

  /**
   * The cached value of the '{@link #getFirst() <em>First</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFirst()
   * @generated
   * @ordered
   */
  protected EList<OwlFirst> first;

  /**
   * The cached value of the '{@link #getType() <em>Type</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getType()
   * @generated
   * @ordered
   */
  protected EList<OwlType> type;

  /**
   * The cached value of the '{@link #getDomain() <em>Domain</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDomain()
   * @generated
   * @ordered
   */
  protected EList<RDFSDomain> domain;

  /**
   * The cached value of the '{@link #getRange() <em>Range</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRange()
   * @generated
   * @ordered
   */
  protected EList<RDFSRange> range;

  /**
   * The cached value of the '{@link #getPropModifiers() <em>Prop Modifiers</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPropModifiers()
   * @generated
   * @ordered
   */
  protected EList<PropertyModifier> propModifiers;

  /**
   * The cached value of the '{@link #getLabels() <em>Labels</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLabels()
   * @generated
   * @ordered
   */
  protected EList<OwlLabel> labels;

  /**
   * The cached value of the '{@link #getAxioms() <em>Axioms</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAxioms()
   * @generated
   * @ordered
   */
  protected EList<ClassAxiom> axioms;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected DescriptionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return OwlmodelPackage.Literals.DESCRIPTION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OwlRef getAbout()
  {
    return about;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAbout(OwlRef newAbout, NotificationChain msgs)
  {
    OwlRef oldAbout = about;
    about = newAbout;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlmodelPackage.DESCRIPTION__ABOUT, oldAbout, newAbout);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setAbout(OwlRef newAbout)
  {
    if (newAbout != about)
    {
      NotificationChain msgs = null;
      if (about != null)
        msgs = ((InternalEObject)about).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.DESCRIPTION__ABOUT, null, msgs);
      if (newAbout != null)
        msgs = ((InternalEObject)newAbout).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.DESCRIPTION__ABOUT, null, msgs);
      msgs = basicSetAbout(newAbout, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.DESCRIPTION__ABOUT, newAbout, newAbout));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<OwlFirst> getFirst()
  {
    if (first == null)
    {
      first = new EObjectContainmentEList<OwlFirst>(OwlFirst.class, this, OwlmodelPackage.DESCRIPTION__FIRST);
    }
    return first;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<OwlType> getType()
  {
    if (type == null)
    {
      type = new EObjectContainmentEList<OwlType>(OwlType.class, this, OwlmodelPackage.DESCRIPTION__TYPE);
    }
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<RDFSDomain> getDomain()
  {
    if (domain == null)
    {
      domain = new EObjectContainmentEList<RDFSDomain>(RDFSDomain.class, this, OwlmodelPackage.DESCRIPTION__DOMAIN);
    }
    return domain;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<RDFSRange> getRange()
  {
    if (range == null)
    {
      range = new EObjectContainmentEList<RDFSRange>(RDFSRange.class, this, OwlmodelPackage.DESCRIPTION__RANGE);
    }
    return range;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<PropertyModifier> getPropModifiers()
  {
    if (propModifiers == null)
    {
      propModifiers = new EObjectContainmentEList<PropertyModifier>(PropertyModifier.class, this, OwlmodelPackage.DESCRIPTION__PROP_MODIFIERS);
    }
    return propModifiers;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<OwlLabel> getLabels()
  {
    if (labels == null)
    {
      labels = new EObjectContainmentEList<OwlLabel>(OwlLabel.class, this, OwlmodelPackage.DESCRIPTION__LABELS);
    }
    return labels;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<ClassAxiom> getAxioms()
  {
    if (axioms == null)
    {
      axioms = new EObjectContainmentEList<ClassAxiom>(ClassAxiom.class, this, OwlmodelPackage.DESCRIPTION__AXIOMS);
    }
    return axioms;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case OwlmodelPackage.DESCRIPTION__ABOUT:
        return basicSetAbout(null, msgs);
      case OwlmodelPackage.DESCRIPTION__FIRST:
        return ((InternalEList<?>)getFirst()).basicRemove(otherEnd, msgs);
      case OwlmodelPackage.DESCRIPTION__TYPE:
        return ((InternalEList<?>)getType()).basicRemove(otherEnd, msgs);
      case OwlmodelPackage.DESCRIPTION__DOMAIN:
        return ((InternalEList<?>)getDomain()).basicRemove(otherEnd, msgs);
      case OwlmodelPackage.DESCRIPTION__RANGE:
        return ((InternalEList<?>)getRange()).basicRemove(otherEnd, msgs);
      case OwlmodelPackage.DESCRIPTION__PROP_MODIFIERS:
        return ((InternalEList<?>)getPropModifiers()).basicRemove(otherEnd, msgs);
      case OwlmodelPackage.DESCRIPTION__LABELS:
        return ((InternalEList<?>)getLabels()).basicRemove(otherEnd, msgs);
      case OwlmodelPackage.DESCRIPTION__AXIOMS:
        return ((InternalEList<?>)getAxioms()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case OwlmodelPackage.DESCRIPTION__ABOUT:
        return getAbout();
      case OwlmodelPackage.DESCRIPTION__FIRST:
        return getFirst();
      case OwlmodelPackage.DESCRIPTION__TYPE:
        return getType();
      case OwlmodelPackage.DESCRIPTION__DOMAIN:
        return getDomain();
      case OwlmodelPackage.DESCRIPTION__RANGE:
        return getRange();
      case OwlmodelPackage.DESCRIPTION__PROP_MODIFIERS:
        return getPropModifiers();
      case OwlmodelPackage.DESCRIPTION__LABELS:
        return getLabels();
      case OwlmodelPackage.DESCRIPTION__AXIOMS:
        return getAxioms();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case OwlmodelPackage.DESCRIPTION__ABOUT:
        setAbout((OwlRef)newValue);
        return;
      case OwlmodelPackage.DESCRIPTION__FIRST:
        getFirst().clear();
        getFirst().addAll((Collection<? extends OwlFirst>)newValue);
        return;
      case OwlmodelPackage.DESCRIPTION__TYPE:
        getType().clear();
        getType().addAll((Collection<? extends OwlType>)newValue);
        return;
      case OwlmodelPackage.DESCRIPTION__DOMAIN:
        getDomain().clear();
        getDomain().addAll((Collection<? extends RDFSDomain>)newValue);
        return;
      case OwlmodelPackage.DESCRIPTION__RANGE:
        getRange().clear();
        getRange().addAll((Collection<? extends RDFSRange>)newValue);
        return;
      case OwlmodelPackage.DESCRIPTION__PROP_MODIFIERS:
        getPropModifiers().clear();
        getPropModifiers().addAll((Collection<? extends PropertyModifier>)newValue);
        return;
      case OwlmodelPackage.DESCRIPTION__LABELS:
        getLabels().clear();
        getLabels().addAll((Collection<? extends OwlLabel>)newValue);
        return;
      case OwlmodelPackage.DESCRIPTION__AXIOMS:
        getAxioms().clear();
        getAxioms().addAll((Collection<? extends ClassAxiom>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.DESCRIPTION__ABOUT:
        setAbout((OwlRef)null);
        return;
      case OwlmodelPackage.DESCRIPTION__FIRST:
        getFirst().clear();
        return;
      case OwlmodelPackage.DESCRIPTION__TYPE:
        getType().clear();
        return;
      case OwlmodelPackage.DESCRIPTION__DOMAIN:
        getDomain().clear();
        return;
      case OwlmodelPackage.DESCRIPTION__RANGE:
        getRange().clear();
        return;
      case OwlmodelPackage.DESCRIPTION__PROP_MODIFIERS:
        getPropModifiers().clear();
        return;
      case OwlmodelPackage.DESCRIPTION__LABELS:
        getLabels().clear();
        return;
      case OwlmodelPackage.DESCRIPTION__AXIOMS:
        getAxioms().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.DESCRIPTION__ABOUT:
        return about != null;
      case OwlmodelPackage.DESCRIPTION__FIRST:
        return first != null && !first.isEmpty();
      case OwlmodelPackage.DESCRIPTION__TYPE:
        return type != null && !type.isEmpty();
      case OwlmodelPackage.DESCRIPTION__DOMAIN:
        return domain != null && !domain.isEmpty();
      case OwlmodelPackage.DESCRIPTION__RANGE:
        return range != null && !range.isEmpty();
      case OwlmodelPackage.DESCRIPTION__PROP_MODIFIERS:
        return propModifiers != null && !propModifiers.isEmpty();
      case OwlmodelPackage.DESCRIPTION__LABELS:
        return labels != null && !labels.isEmpty();
      case OwlmodelPackage.DESCRIPTION__AXIOMS:
        return axioms != null && !axioms.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //DescriptionImpl
