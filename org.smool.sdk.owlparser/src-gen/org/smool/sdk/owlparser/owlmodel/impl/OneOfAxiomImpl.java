/**
 */
package org.smool.sdk.owlparser.owlmodel.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.smool.sdk.owlparser.owlmodel.OneOfAxiom;
import org.smool.sdk.owlparser.owlmodel.OwlFirst;
import org.smool.sdk.owlparser.owlmodel.OwlmodelPackage;
import org.smool.sdk.owlparser.owlmodel.RDFRest;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>One Of Axiom</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OneOfAxiomImpl#getValues <em>Values</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OneOfAxiomImpl#getTagNames <em>Tag Names</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OneOfAxiomImpl#getTagids <em>Tagids</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OneOfAxiomImpl#getFirst <em>First</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OneOfAxiomImpl#getRest <em>Rest</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OneOfAxiomImpl extends ClassAxiomImpl implements OneOfAxiom
{
  /**
   * The cached value of the '{@link #getValues() <em>Values</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getValues()
   * @generated
   * @ordered
   */
  protected EList<EObject> values;

  /**
   * The cached value of the '{@link #getTagNames() <em>Tag Names</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTagNames()
   * @generated
   * @ordered
   */
  protected EList<String> tagNames;

  /**
   * The cached value of the '{@link #getTagids() <em>Tagids</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTagids()
   * @generated
   * @ordered
   */
  protected EList<EObject> tagids;

  /**
   * The cached value of the '{@link #getFirst() <em>First</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFirst()
   * @generated
   * @ordered
   */
  protected EList<OwlFirst> first;

  /**
   * The cached value of the '{@link #getRest() <em>Rest</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRest()
   * @generated
   * @ordered
   */
  protected EList<RDFRest> rest;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected OneOfAxiomImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return OwlmodelPackage.Literals.ONE_OF_AXIOM;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<EObject> getValues()
  {
    if (values == null)
    {
      values = new EObjectContainmentEList<EObject>(EObject.class, this, OwlmodelPackage.ONE_OF_AXIOM__VALUES);
    }
    return values;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<String> getTagNames()
  {
    if (tagNames == null)
    {
      tagNames = new EDataTypeEList<String>(String.class, this, OwlmodelPackage.ONE_OF_AXIOM__TAG_NAMES);
    }
    return tagNames;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<EObject> getTagids()
  {
    if (tagids == null)
    {
      tagids = new EObjectContainmentEList<EObject>(EObject.class, this, OwlmodelPackage.ONE_OF_AXIOM__TAGIDS);
    }
    return tagids;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<OwlFirst> getFirst()
  {
    if (first == null)
    {
      first = new EObjectContainmentEList<OwlFirst>(OwlFirst.class, this, OwlmodelPackage.ONE_OF_AXIOM__FIRST);
    }
    return first;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<RDFRest> getRest()
  {
    if (rest == null)
    {
      rest = new EObjectContainmentEList<RDFRest>(RDFRest.class, this, OwlmodelPackage.ONE_OF_AXIOM__REST);
    }
    return rest;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case OwlmodelPackage.ONE_OF_AXIOM__VALUES:
        return ((InternalEList<?>)getValues()).basicRemove(otherEnd, msgs);
      case OwlmodelPackage.ONE_OF_AXIOM__TAGIDS:
        return ((InternalEList<?>)getTagids()).basicRemove(otherEnd, msgs);
      case OwlmodelPackage.ONE_OF_AXIOM__FIRST:
        return ((InternalEList<?>)getFirst()).basicRemove(otherEnd, msgs);
      case OwlmodelPackage.ONE_OF_AXIOM__REST:
        return ((InternalEList<?>)getRest()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case OwlmodelPackage.ONE_OF_AXIOM__VALUES:
        return getValues();
      case OwlmodelPackage.ONE_OF_AXIOM__TAG_NAMES:
        return getTagNames();
      case OwlmodelPackage.ONE_OF_AXIOM__TAGIDS:
        return getTagids();
      case OwlmodelPackage.ONE_OF_AXIOM__FIRST:
        return getFirst();
      case OwlmodelPackage.ONE_OF_AXIOM__REST:
        return getRest();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case OwlmodelPackage.ONE_OF_AXIOM__VALUES:
        getValues().clear();
        getValues().addAll((Collection<? extends EObject>)newValue);
        return;
      case OwlmodelPackage.ONE_OF_AXIOM__TAG_NAMES:
        getTagNames().clear();
        getTagNames().addAll((Collection<? extends String>)newValue);
        return;
      case OwlmodelPackage.ONE_OF_AXIOM__TAGIDS:
        getTagids().clear();
        getTagids().addAll((Collection<? extends EObject>)newValue);
        return;
      case OwlmodelPackage.ONE_OF_AXIOM__FIRST:
        getFirst().clear();
        getFirst().addAll((Collection<? extends OwlFirst>)newValue);
        return;
      case OwlmodelPackage.ONE_OF_AXIOM__REST:
        getRest().clear();
        getRest().addAll((Collection<? extends RDFRest>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.ONE_OF_AXIOM__VALUES:
        getValues().clear();
        return;
      case OwlmodelPackage.ONE_OF_AXIOM__TAG_NAMES:
        getTagNames().clear();
        return;
      case OwlmodelPackage.ONE_OF_AXIOM__TAGIDS:
        getTagids().clear();
        return;
      case OwlmodelPackage.ONE_OF_AXIOM__FIRST:
        getFirst().clear();
        return;
      case OwlmodelPackage.ONE_OF_AXIOM__REST:
        getRest().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.ONE_OF_AXIOM__VALUES:
        return values != null && !values.isEmpty();
      case OwlmodelPackage.ONE_OF_AXIOM__TAG_NAMES:
        return tagNames != null && !tagNames.isEmpty();
      case OwlmodelPackage.ONE_OF_AXIOM__TAGIDS:
        return tagids != null && !tagids.isEmpty();
      case OwlmodelPackage.ONE_OF_AXIOM__FIRST:
        return first != null && !first.isEmpty();
      case OwlmodelPackage.ONE_OF_AXIOM__REST:
        return rest != null && !rest.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (tagNames: ");
    result.append(tagNames);
    result.append(')');
    return result.toString();
  }

} //OneOfAxiomImpl
