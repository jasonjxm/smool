/**
 */
package org.smool.sdk.owlparser.owlmodel.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.smool.sdk.owlparser.owlmodel.ClassAxiom;
import org.smool.sdk.owlparser.owlmodel.Ontology;
import org.smool.sdk.owlparser.owlmodel.OwlClass;
import org.smool.sdk.owlparser.owlmodel.OwlID;
import org.smool.sdk.owlparser.owlmodel.OwlLabel;
import org.smool.sdk.owlparser.owlmodel.OwlRef;
import org.smool.sdk.owlparser.owlmodel.OwlVersion;
import org.smool.sdk.owlparser.owlmodel.OwlmodelPackage;
import org.smool.sdk.owlparser.owlmodel.SubTypeOf;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Owl Class</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OwlClassImpl#getId <em>Id</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OwlClassImpl#isReference <em>Reference</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OwlClassImpl#getRef <em>Ref</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OwlClassImpl#getLabels <em>Labels</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OwlClassImpl#getVersionInfo <em>Version Info</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OwlClassImpl#getSuperTypes <em>Super Types</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OwlClassImpl#getAxioms <em>Axioms</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OwlClassImpl#getOnts <em>Onts</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.impl.OwlClassImpl#getOthers <em>Others</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OwlClassImpl extends OntologyElementImpl implements OwlClass
{
  /**
   * The cached value of the '{@link #getId() <em>Id</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getId()
   * @generated
   * @ordered
   */
  protected OwlID id;

  /**
   * The default value of the '{@link #isReference() <em>Reference</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isReference()
   * @generated
   * @ordered
   */
  protected static final boolean REFERENCE_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isReference() <em>Reference</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isReference()
   * @generated
   * @ordered
   */
  protected boolean reference = REFERENCE_EDEFAULT;

  /**
   * The cached value of the '{@link #getRef() <em>Ref</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRef()
   * @generated
   * @ordered
   */
  protected OwlRef ref;

  /**
   * The cached value of the '{@link #getLabels() <em>Labels</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLabels()
   * @generated
   * @ordered
   */
  protected EList<OwlLabel> labels;

  /**
   * The cached value of the '{@link #getVersionInfo() <em>Version Info</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVersionInfo()
   * @generated
   * @ordered
   */
  protected EList<OwlVersion> versionInfo;

  /**
   * The cached value of the '{@link #getSuperTypes() <em>Super Types</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSuperTypes()
   * @generated
   * @ordered
   */
  protected EList<SubTypeOf> superTypes;

  /**
   * The cached value of the '{@link #getAxioms() <em>Axioms</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAxioms()
   * @generated
   * @ordered
   */
  protected EList<ClassAxiom> axioms;

  /**
   * The cached value of the '{@link #getOnts() <em>Onts</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOnts()
   * @generated
   * @ordered
   */
  protected EList<Ontology> onts;

  /**
   * The cached value of the '{@link #getOthers() <em>Others</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOthers()
   * @generated
   * @ordered
   */
  protected EList<String> others;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected OwlClassImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return OwlmodelPackage.Literals.OWL_CLASS;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OwlID getId()
  {
    return id;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetId(OwlID newId, NotificationChain msgs)
  {
    OwlID oldId = id;
    id = newId;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_CLASS__ID, oldId, newId);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setId(OwlID newId)
  {
    if (newId != id)
    {
      NotificationChain msgs = null;
      if (id != null)
        msgs = ((InternalEObject)id).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.OWL_CLASS__ID, null, msgs);
      if (newId != null)
        msgs = ((InternalEObject)newId).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.OWL_CLASS__ID, null, msgs);
      msgs = basicSetId(newId, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_CLASS__ID, newId, newId));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean isReference()
  {
    return reference;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setReference(boolean newReference)
  {
    boolean oldReference = reference;
    reference = newReference;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_CLASS__REFERENCE, oldReference, reference));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OwlRef getRef()
  {
    return ref;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetRef(OwlRef newRef, NotificationChain msgs)
  {
    OwlRef oldRef = ref;
    ref = newRef;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_CLASS__REF, oldRef, newRef);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setRef(OwlRef newRef)
  {
    if (newRef != ref)
    {
      NotificationChain msgs = null;
      if (ref != null)
        msgs = ((InternalEObject)ref).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.OWL_CLASS__REF, null, msgs);
      if (newRef != null)
        msgs = ((InternalEObject)newRef).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OwlmodelPackage.OWL_CLASS__REF, null, msgs);
      msgs = basicSetRef(newRef, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OwlmodelPackage.OWL_CLASS__REF, newRef, newRef));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<OwlLabel> getLabels()
  {
    if (labels == null)
    {
      labels = new EObjectContainmentEList<OwlLabel>(OwlLabel.class, this, OwlmodelPackage.OWL_CLASS__LABELS);
    }
    return labels;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<OwlVersion> getVersionInfo()
  {
    if (versionInfo == null)
    {
      versionInfo = new EObjectContainmentEList<OwlVersion>(OwlVersion.class, this, OwlmodelPackage.OWL_CLASS__VERSION_INFO);
    }
    return versionInfo;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<SubTypeOf> getSuperTypes()
  {
    if (superTypes == null)
    {
      superTypes = new EObjectContainmentEList<SubTypeOf>(SubTypeOf.class, this, OwlmodelPackage.OWL_CLASS__SUPER_TYPES);
    }
    return superTypes;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<ClassAxiom> getAxioms()
  {
    if (axioms == null)
    {
      axioms = new EObjectContainmentEList<ClassAxiom>(ClassAxiom.class, this, OwlmodelPackage.OWL_CLASS__AXIOMS);
    }
    return axioms;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<Ontology> getOnts()
  {
    if (onts == null)
    {
      onts = new EObjectContainmentEList<Ontology>(Ontology.class, this, OwlmodelPackage.OWL_CLASS__ONTS);
    }
    return onts;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<String> getOthers()
  {
    if (others == null)
    {
      others = new EDataTypeEList<String>(String.class, this, OwlmodelPackage.OWL_CLASS__OTHERS);
    }
    return others;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_CLASS__ID:
        return basicSetId(null, msgs);
      case OwlmodelPackage.OWL_CLASS__REF:
        return basicSetRef(null, msgs);
      case OwlmodelPackage.OWL_CLASS__LABELS:
        return ((InternalEList<?>)getLabels()).basicRemove(otherEnd, msgs);
      case OwlmodelPackage.OWL_CLASS__VERSION_INFO:
        return ((InternalEList<?>)getVersionInfo()).basicRemove(otherEnd, msgs);
      case OwlmodelPackage.OWL_CLASS__SUPER_TYPES:
        return ((InternalEList<?>)getSuperTypes()).basicRemove(otherEnd, msgs);
      case OwlmodelPackage.OWL_CLASS__AXIOMS:
        return ((InternalEList<?>)getAxioms()).basicRemove(otherEnd, msgs);
      case OwlmodelPackage.OWL_CLASS__ONTS:
        return ((InternalEList<?>)getOnts()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_CLASS__ID:
        return getId();
      case OwlmodelPackage.OWL_CLASS__REFERENCE:
        return isReference();
      case OwlmodelPackage.OWL_CLASS__REF:
        return getRef();
      case OwlmodelPackage.OWL_CLASS__LABELS:
        return getLabels();
      case OwlmodelPackage.OWL_CLASS__VERSION_INFO:
        return getVersionInfo();
      case OwlmodelPackage.OWL_CLASS__SUPER_TYPES:
        return getSuperTypes();
      case OwlmodelPackage.OWL_CLASS__AXIOMS:
        return getAxioms();
      case OwlmodelPackage.OWL_CLASS__ONTS:
        return getOnts();
      case OwlmodelPackage.OWL_CLASS__OTHERS:
        return getOthers();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_CLASS__ID:
        setId((OwlID)newValue);
        return;
      case OwlmodelPackage.OWL_CLASS__REFERENCE:
        setReference((Boolean)newValue);
        return;
      case OwlmodelPackage.OWL_CLASS__REF:
        setRef((OwlRef)newValue);
        return;
      case OwlmodelPackage.OWL_CLASS__LABELS:
        getLabels().clear();
        getLabels().addAll((Collection<? extends OwlLabel>)newValue);
        return;
      case OwlmodelPackage.OWL_CLASS__VERSION_INFO:
        getVersionInfo().clear();
        getVersionInfo().addAll((Collection<? extends OwlVersion>)newValue);
        return;
      case OwlmodelPackage.OWL_CLASS__SUPER_TYPES:
        getSuperTypes().clear();
        getSuperTypes().addAll((Collection<? extends SubTypeOf>)newValue);
        return;
      case OwlmodelPackage.OWL_CLASS__AXIOMS:
        getAxioms().clear();
        getAxioms().addAll((Collection<? extends ClassAxiom>)newValue);
        return;
      case OwlmodelPackage.OWL_CLASS__ONTS:
        getOnts().clear();
        getOnts().addAll((Collection<? extends Ontology>)newValue);
        return;
      case OwlmodelPackage.OWL_CLASS__OTHERS:
        getOthers().clear();
        getOthers().addAll((Collection<? extends String>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_CLASS__ID:
        setId((OwlID)null);
        return;
      case OwlmodelPackage.OWL_CLASS__REFERENCE:
        setReference(REFERENCE_EDEFAULT);
        return;
      case OwlmodelPackage.OWL_CLASS__REF:
        setRef((OwlRef)null);
        return;
      case OwlmodelPackage.OWL_CLASS__LABELS:
        getLabels().clear();
        return;
      case OwlmodelPackage.OWL_CLASS__VERSION_INFO:
        getVersionInfo().clear();
        return;
      case OwlmodelPackage.OWL_CLASS__SUPER_TYPES:
        getSuperTypes().clear();
        return;
      case OwlmodelPackage.OWL_CLASS__AXIOMS:
        getAxioms().clear();
        return;
      case OwlmodelPackage.OWL_CLASS__ONTS:
        getOnts().clear();
        return;
      case OwlmodelPackage.OWL_CLASS__OTHERS:
        getOthers().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case OwlmodelPackage.OWL_CLASS__ID:
        return id != null;
      case OwlmodelPackage.OWL_CLASS__REFERENCE:
        return reference != REFERENCE_EDEFAULT;
      case OwlmodelPackage.OWL_CLASS__REF:
        return ref != null;
      case OwlmodelPackage.OWL_CLASS__LABELS:
        return labels != null && !labels.isEmpty();
      case OwlmodelPackage.OWL_CLASS__VERSION_INFO:
        return versionInfo != null && !versionInfo.isEmpty();
      case OwlmodelPackage.OWL_CLASS__SUPER_TYPES:
        return superTypes != null && !superTypes.isEmpty();
      case OwlmodelPackage.OWL_CLASS__AXIOMS:
        return axioms != null && !axioms.isEmpty();
      case OwlmodelPackage.OWL_CLASS__ONTS:
        return onts != null && !onts.isEmpty();
      case OwlmodelPackage.OWL_CLASS__OTHERS:
        return others != null && !others.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (reference: ");
    result.append(reference);
    result.append(", others: ");
    result.append(others);
    result.append(')');
    return result.toString();
  }

} //OwlClassImpl
