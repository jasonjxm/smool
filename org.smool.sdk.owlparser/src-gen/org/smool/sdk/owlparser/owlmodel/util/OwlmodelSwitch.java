/**
 */
package org.smool.sdk.owlparser.owlmodel.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.smool.sdk.owlparser.owlmodel.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage
 * @generated
 */
public class OwlmodelSwitch<T> extends Switch<T>
{
  /**
   * The cached model package
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static OwlmodelPackage modelPackage;

  /**
   * Creates an instance of the switch.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OwlmodelSwitch()
  {
    if (modelPackage == null)
    {
      modelPackage = OwlmodelPackage.eINSTANCE;
    }
  }

  /**
   * Checks whether this is a switch for the given package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param ePackage the package in question.
   * @return whether this is a switch for the given package.
   * @generated
   */
  @Override
  protected boolean isSwitchFor(EPackage ePackage)
  {
    return ePackage == modelPackage;
  }

  /**
   * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the first non-null result returned by a <code>caseXXX</code> call.
   * @generated
   */
  @Override
  protected T doSwitch(int classifierID, EObject theEObject)
  {
    switch (classifierID)
    {
      case OwlmodelPackage.OWL_MODEL:
      {
        OWLModel owlModel = (OWLModel)theEObject;
        T result = caseOWLModel(owlModel);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.DOC_TYPE:
      {
        DocType docType = (DocType)theEObject;
        T result = caseDocType(docType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.DOC_ENTITY:
      {
        DocEntity docEntity = (DocEntity)theEObject;
        T result = caseDocEntity(docEntity);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.RDF_ELEMENT:
      {
        RDFElement rdfElement = (RDFElement)theEObject;
        T result = caseRDFElement(rdfElement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.XML_NS:
      {
        XmlNs xmlNs = (XmlNs)theEObject;
        T result = caseXmlNs(xmlNs);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.OWL_REF:
      {
        OwlRef owlRef = (OwlRef)theEObject;
        T result = caseOwlRef(owlRef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.OWL_ID:
      {
        OwlID owlID = (OwlID)theEObject;
        T result = caseOwlID(owlID);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.ONTOLOGY_ID:
      {
        OntologyID ontologyID = (OntologyID)theEObject;
        T result = caseOntologyID(ontologyID);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.PARSE_TYPE:
      {
        ParseType parseType = (ParseType)theEObject;
        T result = caseParseType(parseType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.ONTOLOGY_ELEMENT:
      {
        OntologyElement ontologyElement = (OntologyElement)theEObject;
        T result = caseOntologyElement(ontologyElement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.NAMED_INDIVIDUAL:
      {
        NamedIndividual namedIndividual = (NamedIndividual)theEObject;
        T result = caseNamedIndividual(namedIndividual);
        if (result == null) result = caseOntologyElement(namedIndividual);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.ONTOLOGY:
      {
        Ontology ontology = (Ontology)theEObject;
        T result = caseOntology(ontology);
        if (result == null) result = caseOntologyElement(ontology);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.SEE_ALSO:
      {
        SeeAlso seeAlso = (SeeAlso)theEObject;
        T result = caseSeeAlso(seeAlso);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.PRIOR_VERSION:
      {
        PriorVersion priorVersion = (PriorVersion)theEObject;
        T result = casePriorVersion(priorVersion);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.BACKWARDS_COMP:
      {
        BackwardsComp backwardsComp = (BackwardsComp)theEObject;
        T result = caseBackwardsComp(backwardsComp);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.OWL_IMPORT:
      {
        OwlImport owlImport = (OwlImport)theEObject;
        T result = caseOwlImport(owlImport);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.OWL_CLASS:
      {
        OwlClass owlClass = (OwlClass)theEObject;
        T result = caseOwlClass(owlClass);
        if (result == null) result = caseOntologyElement(owlClass);
        if (result == null) result = caseClassModifier(owlClass);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.MINIMUM_CLASS:
      {
        MinimumClass minimumClass = (MinimumClass)theEObject;
        T result = caseMinimumClass(minimumClass);
        if (result == null) result = caseClassModifier(minimumClass);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.CLASS_AXIOM:
      {
        ClassAxiom classAxiom = (ClassAxiom)theEObject;
        T result = caseClassAxiom(classAxiom);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.COMPLEMENT_AXIOM:
      {
        ComplementAxiom complementAxiom = (ComplementAxiom)theEObject;
        T result = caseComplementAxiom(complementAxiom);
        if (result == null) result = caseClassAxiom(complementAxiom);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.INCOMPATIBLE_WITH_AXIOM:
      {
        IncompatibleWithAxiom incompatibleWithAxiom = (IncompatibleWithAxiom)theEObject;
        T result = caseIncompatibleWithAxiom(incompatibleWithAxiom);
        if (result == null) result = caseClassAxiom(incompatibleWithAxiom);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.DISJOINT_AXIOM:
      {
        DisjointAxiom disjointAxiom = (DisjointAxiom)theEObject;
        T result = caseDisjointAxiom(disjointAxiom);
        if (result == null) result = caseClassAxiom(disjointAxiom);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.EQUIVALENT_AXIOM:
      {
        EquivalentAxiom equivalentAxiom = (EquivalentAxiom)theEObject;
        T result = caseEquivalentAxiom(equivalentAxiom);
        if (result == null) result = caseClassAxiom(equivalentAxiom);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.UNION_AXIOM:
      {
        UnionAxiom unionAxiom = (UnionAxiom)theEObject;
        T result = caseUnionAxiom(unionAxiom);
        if (result == null) result = caseClassAxiom(unionAxiom);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.INTERSECTION_AXIOM:
      {
        IntersectionAxiom intersectionAxiom = (IntersectionAxiom)theEObject;
        T result = caseIntersectionAxiom(intersectionAxiom);
        if (result == null) result = caseClassAxiom(intersectionAxiom);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.ONE_OF_AXIOM:
      {
        OneOfAxiom oneOfAxiom = (OneOfAxiom)theEObject;
        T result = caseOneOfAxiom(oneOfAxiom);
        if (result == null) result = caseClassAxiom(oneOfAxiom);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.OWL_LABEL:
      {
        OwlLabel owlLabel = (OwlLabel)theEObject;
        T result = caseOwlLabel(owlLabel);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.OWL_VERSION:
      {
        OwlVersion owlVersion = (OwlVersion)theEObject;
        T result = caseOwlVersion(owlVersion);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.SUB_TYPE_OF:
      {
        SubTypeOf subTypeOf = (SubTypeOf)theEObject;
        T result = caseSubTypeOf(subTypeOf);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.CLASS_MODIFIER:
      {
        ClassModifier classModifier = (ClassModifier)theEObject;
        T result = caseClassModifier(classModifier);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.OWL_RESTRICTION:
      {
        OwlRestriction owlRestriction = (OwlRestriction)theEObject;
        T result = caseOwlRestriction(owlRestriction);
        if (result == null) result = caseClassModifier(owlRestriction);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.RESTRICTION_BODY:
      {
        RestrictionBody restrictionBody = (RestrictionBody)theEObject;
        T result = caseRestrictionBody(restrictionBody);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.MIN_CARDINALITY:
      {
        MinCardinality minCardinality = (MinCardinality)theEObject;
        T result = caseMinCardinality(minCardinality);
        if (result == null) result = caseRestrictionBody(minCardinality);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.MAX_CARDINALITY:
      {
        MaxCardinality maxCardinality = (MaxCardinality)theEObject;
        T result = caseMaxCardinality(maxCardinality);
        if (result == null) result = caseRestrictionBody(maxCardinality);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.CARDINALITY:
      {
        Cardinality cardinality = (Cardinality)theEObject;
        T result = caseCardinality(cardinality);
        if (result == null) result = caseRestrictionBody(cardinality);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.QUALIFIED_CARDINALITY:
      {
        QualifiedCardinality qualifiedCardinality = (QualifiedCardinality)theEObject;
        T result = caseQualifiedCardinality(qualifiedCardinality);
        if (result == null) result = caseRestrictionBody(qualifiedCardinality);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.MIN_QUALIFIED_CARDINALITY:
      {
        MinQualifiedCardinality minQualifiedCardinality = (MinQualifiedCardinality)theEObject;
        T result = caseMinQualifiedCardinality(minQualifiedCardinality);
        if (result == null) result = caseRestrictionBody(minQualifiedCardinality);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.MAX_QUALIFIED_CARDINALITY:
      {
        MaxQualifiedCardinality maxQualifiedCardinality = (MaxQualifiedCardinality)theEObject;
        T result = caseMaxQualifiedCardinality(maxQualifiedCardinality);
        if (result == null) result = caseRestrictionBody(maxQualifiedCardinality);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.SOME_VALUES:
      {
        SomeValues someValues = (SomeValues)theEObject;
        T result = caseSomeValues(someValues);
        if (result == null) result = caseRestrictionBody(someValues);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.ALL_VALUES:
      {
        AllValues allValues = (AllValues)theEObject;
        T result = caseAllValues(allValues);
        if (result == null) result = caseRestrictionBody(allValues);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.HAS_VALUE:
      {
        HasValue hasValue = (HasValue)theEObject;
        T result = caseHasValue(hasValue);
        if (result == null) result = caseRestrictionBody(hasValue);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.PROPERTY:
      {
        Property property = (Property)theEObject;
        T result = caseProperty(property);
        if (result == null) result = caseOntologyElement(property);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.ANNOTATION_PROPERTY:
      {
        AnnotationProperty annotationProperty = (AnnotationProperty)theEObject;
        T result = caseAnnotationProperty(annotationProperty);
        if (result == null) result = caseProperty(annotationProperty);
        if (result == null) result = caseOntologyElement(annotationProperty);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.OBJECT_PROPERTY:
      {
        ObjectProperty objectProperty = (ObjectProperty)theEObject;
        T result = caseObjectProperty(objectProperty);
        if (result == null) result = caseProperty(objectProperty);
        if (result == null) result = caseOntologyElement(objectProperty);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.DATATYPE_PROPERTY:
      {
        DatatypeProperty datatypeProperty = (DatatypeProperty)theEObject;
        T result = caseDatatypeProperty(datatypeProperty);
        if (result == null) result = caseProperty(datatypeProperty);
        if (result == null) result = caseOntologyElement(datatypeProperty);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.FUNCTIONAL_PROPERTY:
      {
        FunctionalProperty functionalProperty = (FunctionalProperty)theEObject;
        T result = caseFunctionalProperty(functionalProperty);
        if (result == null) result = caseProperty(functionalProperty);
        if (result == null) result = caseOntologyElement(functionalProperty);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.INVERSE_FUNCTIONAL_PROPERTY:
      {
        InverseFunctionalProperty inverseFunctionalProperty = (InverseFunctionalProperty)theEObject;
        T result = caseInverseFunctionalProperty(inverseFunctionalProperty);
        if (result == null) result = caseProperty(inverseFunctionalProperty);
        if (result == null) result = caseOntologyElement(inverseFunctionalProperty);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.SYMMETRIC_PROPERTY:
      {
        SymmetricProperty symmetricProperty = (SymmetricProperty)theEObject;
        T result = caseSymmetricProperty(symmetricProperty);
        if (result == null) result = caseProperty(symmetricProperty);
        if (result == null) result = caseOntologyElement(symmetricProperty);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.TRANSITIVE_PROPERTY:
      {
        TransitiveProperty transitiveProperty = (TransitiveProperty)theEObject;
        T result = caseTransitiveProperty(transitiveProperty);
        if (result == null) result = caseProperty(transitiveProperty);
        if (result == null) result = caseOntologyElement(transitiveProperty);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.PROPERTY_MODIFIER:
      {
        PropertyModifier propertyModifier = (PropertyModifier)theEObject;
        T result = casePropertyModifier(propertyModifier);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.INVERSE_TYPE:
      {
        InverseType inverseType = (InverseType)theEObject;
        T result = caseInverseType(inverseType);
        if (result == null) result = casePropertyModifier(inverseType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.EQUIVALENT_TYPE:
      {
        EquivalentType equivalentType = (EquivalentType)theEObject;
        T result = caseEquivalentType(equivalentType);
        if (result == null) result = casePropertyModifier(equivalentType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.RDFS_DOMAIN:
      {
        RDFSDomain rdfsDomain = (RDFSDomain)theEObject;
        T result = caseRDFSDomain(rdfsDomain);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.RDFS_RANGE:
      {
        RDFSRange rdfsRange = (RDFSRange)theEObject;
        T result = caseRDFSRange(rdfsRange);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.OWL_DATA_RANGE:
      {
        OwlDataRange owlDataRange = (OwlDataRange)theEObject;
        T result = caseOwlDataRange(owlDataRange);
        if (result == null) result = caseRDFSRange(owlDataRange);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.RDF_TYPE:
      {
        RDFType rdfType = (RDFType)theEObject;
        T result = caseRDFType(rdfType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.SUB_PROPERTY_OF:
      {
        SubPropertyOf subPropertyOf = (SubPropertyOf)theEObject;
        T result = caseSubPropertyOf(subPropertyOf);
        if (result == null) result = casePropertyModifier(subPropertyOf);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.DESCRIPTION:
      {
        Description description = (Description)theEObject;
        T result = caseDescription(description);
        if (result == null) result = caseOntologyElement(description);
        if (result == null) result = caseClassModifier(description);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.OWL_FIRST:
      {
        OwlFirst owlFirst = (OwlFirst)theEObject;
        T result = caseOwlFirst(owlFirst);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.RDF_REST:
      {
        RDFRest rdfRest = (RDFRest)theEObject;
        T result = caseRDFRest(rdfRest);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.OWL_TYPE:
      {
        OwlType owlType = (OwlType)theEObject;
        T result = caseOwlType(owlType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.OWL_MEMBERS:
      {
        OwlMembers owlMembers = (OwlMembers)theEObject;
        T result = caseOwlMembers(owlMembers);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.DATATYPE:
      {
        Datatype datatype = (Datatype)theEObject;
        T result = caseDatatype(datatype);
        if (result == null) result = caseOntologyElement(datatype);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.SIMPLE_RDFS_DOMAIN:
      {
        SimpleRDFSDomain simpleRDFSDomain = (SimpleRDFSDomain)theEObject;
        T result = caseSimpleRDFSDomain(simpleRDFSDomain);
        if (result == null) result = caseRDFSDomain(simpleRDFSDomain);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.PARSED_RDFS_DOMAIN:
      {
        ParsedRDFSDomain parsedRDFSDomain = (ParsedRDFSDomain)theEObject;
        T result = caseParsedRDFSDomain(parsedRDFSDomain);
        if (result == null) result = caseRDFSDomain(parsedRDFSDomain);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.SIMPLE_RDFS_RANGE:
      {
        SimpleRDFSRange simpleRDFSRange = (SimpleRDFSRange)theEObject;
        T result = caseSimpleRDFSRange(simpleRDFSRange);
        if (result == null) result = caseRDFSRange(simpleRDFSRange);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case OwlmodelPackage.PARSED_PROPERTY_RANGE:
      {
        ParsedPropertyRange parsedPropertyRange = (ParsedPropertyRange)theEObject;
        T result = caseParsedPropertyRange(parsedPropertyRange);
        if (result == null) result = caseRDFSRange(parsedPropertyRange);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      default: return defaultCase(theEObject);
    }
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>OWL Model</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>OWL Model</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseOWLModel(OWLModel object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Doc Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Doc Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDocType(DocType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Doc Entity</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Doc Entity</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDocEntity(DocEntity object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>RDF Element</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>RDF Element</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRDFElement(RDFElement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Xml Ns</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Xml Ns</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseXmlNs(XmlNs object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Owl Ref</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Owl Ref</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseOwlRef(OwlRef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Owl ID</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Owl ID</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseOwlID(OwlID object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Ontology ID</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Ontology ID</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseOntologyID(OntologyID object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Parse Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Parse Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseParseType(ParseType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Ontology Element</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Ontology Element</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseOntologyElement(OntologyElement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Named Individual</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Named Individual</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNamedIndividual(NamedIndividual object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Ontology</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Ontology</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseOntology(Ontology object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>See Also</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>See Also</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSeeAlso(SeeAlso object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Prior Version</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Prior Version</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePriorVersion(PriorVersion object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Backwards Comp</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Backwards Comp</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBackwardsComp(BackwardsComp object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Owl Import</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Owl Import</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseOwlImport(OwlImport object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Owl Class</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Owl Class</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseOwlClass(OwlClass object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Minimum Class</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Minimum Class</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMinimumClass(MinimumClass object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Class Axiom</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Class Axiom</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseClassAxiom(ClassAxiom object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Complement Axiom</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Complement Axiom</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseComplementAxiom(ComplementAxiom object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Incompatible With Axiom</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Incompatible With Axiom</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseIncompatibleWithAxiom(IncompatibleWithAxiom object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Disjoint Axiom</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Disjoint Axiom</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDisjointAxiom(DisjointAxiom object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Equivalent Axiom</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Equivalent Axiom</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseEquivalentAxiom(EquivalentAxiom object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Union Axiom</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Union Axiom</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseUnionAxiom(UnionAxiom object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Intersection Axiom</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Intersection Axiom</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseIntersectionAxiom(IntersectionAxiom object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>One Of Axiom</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>One Of Axiom</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseOneOfAxiom(OneOfAxiom object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Owl Label</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Owl Label</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseOwlLabel(OwlLabel object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Owl Version</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Owl Version</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseOwlVersion(OwlVersion object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Sub Type Of</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Sub Type Of</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSubTypeOf(SubTypeOf object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Class Modifier</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Class Modifier</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseClassModifier(ClassModifier object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Owl Restriction</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Owl Restriction</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseOwlRestriction(OwlRestriction object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Restriction Body</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Restriction Body</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRestrictionBody(RestrictionBody object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Min Cardinality</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Min Cardinality</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMinCardinality(MinCardinality object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Max Cardinality</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Max Cardinality</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMaxCardinality(MaxCardinality object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Cardinality</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Cardinality</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCardinality(Cardinality object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Qualified Cardinality</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Qualified Cardinality</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseQualifiedCardinality(QualifiedCardinality object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Min Qualified Cardinality</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Min Qualified Cardinality</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMinQualifiedCardinality(MinQualifiedCardinality object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Max Qualified Cardinality</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Max Qualified Cardinality</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMaxQualifiedCardinality(MaxQualifiedCardinality object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Some Values</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Some Values</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSomeValues(SomeValues object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>All Values</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>All Values</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAllValues(AllValues object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Has Value</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Has Value</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseHasValue(HasValue object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Property</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Property</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseProperty(Property object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Annotation Property</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Annotation Property</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAnnotationProperty(AnnotationProperty object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Object Property</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Object Property</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseObjectProperty(ObjectProperty object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Datatype Property</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Datatype Property</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDatatypeProperty(DatatypeProperty object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Functional Property</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Functional Property</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFunctionalProperty(FunctionalProperty object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Inverse Functional Property</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Inverse Functional Property</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseInverseFunctionalProperty(InverseFunctionalProperty object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Symmetric Property</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Symmetric Property</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSymmetricProperty(SymmetricProperty object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Transitive Property</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Transitive Property</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTransitiveProperty(TransitiveProperty object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Property Modifier</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Property Modifier</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePropertyModifier(PropertyModifier object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Inverse Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Inverse Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseInverseType(InverseType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Equivalent Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Equivalent Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseEquivalentType(EquivalentType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>RDFS Domain</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>RDFS Domain</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRDFSDomain(RDFSDomain object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>RDFS Range</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>RDFS Range</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRDFSRange(RDFSRange object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Owl Data Range</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Owl Data Range</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseOwlDataRange(OwlDataRange object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>RDF Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>RDF Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRDFType(RDFType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Sub Property Of</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Sub Property Of</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSubPropertyOf(SubPropertyOf object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Description</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Description</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDescription(Description object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Owl First</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Owl First</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseOwlFirst(OwlFirst object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>RDF Rest</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>RDF Rest</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRDFRest(RDFRest object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Owl Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Owl Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseOwlType(OwlType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Owl Members</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Owl Members</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseOwlMembers(OwlMembers object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Datatype</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Datatype</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDatatype(Datatype object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Simple RDFS Domain</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Simple RDFS Domain</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSimpleRDFSDomain(SimpleRDFSDomain object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Parsed RDFS Domain</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Parsed RDFS Domain</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseParsedRDFSDomain(ParsedRDFSDomain object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Simple RDFS Range</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Simple RDFS Range</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSimpleRDFSRange(SimpleRDFSRange object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Parsed Property Range</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Parsed Property Range</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseParsedPropertyRange(ParsedPropertyRange object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch, but this is the last case anyway.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject)
   * @generated
   */
  @Override
  public T defaultCase(EObject object)
  {
    return null;
  }

} //OwlmodelSwitch
