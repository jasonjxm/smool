/**
 */
package org.smool.sdk.owlparser.owlmodel.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import org.smool.sdk.owlparser.owlmodel.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage
 * @generated
 */
public class OwlmodelAdapterFactory extends AdapterFactoryImpl
{
  /**
   * The cached model package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static OwlmodelPackage modelPackage;

  /**
   * Creates an instance of the adapter factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OwlmodelAdapterFactory()
  {
    if (modelPackage == null)
    {
      modelPackage = OwlmodelPackage.eINSTANCE;
    }
  }

  /**
   * Returns whether this factory is applicable for the type of the object.
   * <!-- begin-user-doc -->
   * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
   * <!-- end-user-doc -->
   * @return whether this factory is applicable for the type of the object.
   * @generated
   */
  @Override
  public boolean isFactoryForType(Object object)
  {
    if (object == modelPackage)
    {
      return true;
    }
    if (object instanceof EObject)
    {
      return ((EObject)object).eClass().getEPackage() == modelPackage;
    }
    return false;
  }

  /**
   * The switch that delegates to the <code>createXXX</code> methods.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected OwlmodelSwitch<Adapter> modelSwitch =
    new OwlmodelSwitch<Adapter>()
    {
      @Override
      public Adapter caseOWLModel(OWLModel object)
      {
        return createOWLModelAdapter();
      }
      @Override
      public Adapter caseDocType(DocType object)
      {
        return createDocTypeAdapter();
      }
      @Override
      public Adapter caseDocEntity(DocEntity object)
      {
        return createDocEntityAdapter();
      }
      @Override
      public Adapter caseRDFElement(RDFElement object)
      {
        return createRDFElementAdapter();
      }
      @Override
      public Adapter caseXmlNs(XmlNs object)
      {
        return createXmlNsAdapter();
      }
      @Override
      public Adapter caseOwlRef(OwlRef object)
      {
        return createOwlRefAdapter();
      }
      @Override
      public Adapter caseOwlID(OwlID object)
      {
        return createOwlIDAdapter();
      }
      @Override
      public Adapter caseOntologyID(OntologyID object)
      {
        return createOntologyIDAdapter();
      }
      @Override
      public Adapter caseParseType(ParseType object)
      {
        return createParseTypeAdapter();
      }
      @Override
      public Adapter caseOntologyElement(OntologyElement object)
      {
        return createOntologyElementAdapter();
      }
      @Override
      public Adapter caseNamedIndividual(NamedIndividual object)
      {
        return createNamedIndividualAdapter();
      }
      @Override
      public Adapter caseOntology(Ontology object)
      {
        return createOntologyAdapter();
      }
      @Override
      public Adapter caseSeeAlso(SeeAlso object)
      {
        return createSeeAlsoAdapter();
      }
      @Override
      public Adapter casePriorVersion(PriorVersion object)
      {
        return createPriorVersionAdapter();
      }
      @Override
      public Adapter caseBackwardsComp(BackwardsComp object)
      {
        return createBackwardsCompAdapter();
      }
      @Override
      public Adapter caseOwlImport(OwlImport object)
      {
        return createOwlImportAdapter();
      }
      @Override
      public Adapter caseOwlClass(OwlClass object)
      {
        return createOwlClassAdapter();
      }
      @Override
      public Adapter caseMinimumClass(MinimumClass object)
      {
        return createMinimumClassAdapter();
      }
      @Override
      public Adapter caseClassAxiom(ClassAxiom object)
      {
        return createClassAxiomAdapter();
      }
      @Override
      public Adapter caseComplementAxiom(ComplementAxiom object)
      {
        return createComplementAxiomAdapter();
      }
      @Override
      public Adapter caseIncompatibleWithAxiom(IncompatibleWithAxiom object)
      {
        return createIncompatibleWithAxiomAdapter();
      }
      @Override
      public Adapter caseDisjointAxiom(DisjointAxiom object)
      {
        return createDisjointAxiomAdapter();
      }
      @Override
      public Adapter caseEquivalentAxiom(EquivalentAxiom object)
      {
        return createEquivalentAxiomAdapter();
      }
      @Override
      public Adapter caseUnionAxiom(UnionAxiom object)
      {
        return createUnionAxiomAdapter();
      }
      @Override
      public Adapter caseIntersectionAxiom(IntersectionAxiom object)
      {
        return createIntersectionAxiomAdapter();
      }
      @Override
      public Adapter caseOneOfAxiom(OneOfAxiom object)
      {
        return createOneOfAxiomAdapter();
      }
      @Override
      public Adapter caseOwlLabel(OwlLabel object)
      {
        return createOwlLabelAdapter();
      }
      @Override
      public Adapter caseOwlVersion(OwlVersion object)
      {
        return createOwlVersionAdapter();
      }
      @Override
      public Adapter caseSubTypeOf(SubTypeOf object)
      {
        return createSubTypeOfAdapter();
      }
      @Override
      public Adapter caseClassModifier(ClassModifier object)
      {
        return createClassModifierAdapter();
      }
      @Override
      public Adapter caseOwlRestriction(OwlRestriction object)
      {
        return createOwlRestrictionAdapter();
      }
      @Override
      public Adapter caseRestrictionBody(RestrictionBody object)
      {
        return createRestrictionBodyAdapter();
      }
      @Override
      public Adapter caseMinCardinality(MinCardinality object)
      {
        return createMinCardinalityAdapter();
      }
      @Override
      public Adapter caseMaxCardinality(MaxCardinality object)
      {
        return createMaxCardinalityAdapter();
      }
      @Override
      public Adapter caseCardinality(Cardinality object)
      {
        return createCardinalityAdapter();
      }
      @Override
      public Adapter caseQualifiedCardinality(QualifiedCardinality object)
      {
        return createQualifiedCardinalityAdapter();
      }
      @Override
      public Adapter caseMinQualifiedCardinality(MinQualifiedCardinality object)
      {
        return createMinQualifiedCardinalityAdapter();
      }
      @Override
      public Adapter caseMaxQualifiedCardinality(MaxQualifiedCardinality object)
      {
        return createMaxQualifiedCardinalityAdapter();
      }
      @Override
      public Adapter caseSomeValues(SomeValues object)
      {
        return createSomeValuesAdapter();
      }
      @Override
      public Adapter caseAllValues(AllValues object)
      {
        return createAllValuesAdapter();
      }
      @Override
      public Adapter caseHasValue(HasValue object)
      {
        return createHasValueAdapter();
      }
      @Override
      public Adapter caseProperty(Property object)
      {
        return createPropertyAdapter();
      }
      @Override
      public Adapter caseAnnotationProperty(AnnotationProperty object)
      {
        return createAnnotationPropertyAdapter();
      }
      @Override
      public Adapter caseObjectProperty(ObjectProperty object)
      {
        return createObjectPropertyAdapter();
      }
      @Override
      public Adapter caseDatatypeProperty(DatatypeProperty object)
      {
        return createDatatypePropertyAdapter();
      }
      @Override
      public Adapter caseFunctionalProperty(FunctionalProperty object)
      {
        return createFunctionalPropertyAdapter();
      }
      @Override
      public Adapter caseInverseFunctionalProperty(InverseFunctionalProperty object)
      {
        return createInverseFunctionalPropertyAdapter();
      }
      @Override
      public Adapter caseSymmetricProperty(SymmetricProperty object)
      {
        return createSymmetricPropertyAdapter();
      }
      @Override
      public Adapter caseTransitiveProperty(TransitiveProperty object)
      {
        return createTransitivePropertyAdapter();
      }
      @Override
      public Adapter casePropertyModifier(PropertyModifier object)
      {
        return createPropertyModifierAdapter();
      }
      @Override
      public Adapter caseInverseType(InverseType object)
      {
        return createInverseTypeAdapter();
      }
      @Override
      public Adapter caseEquivalentType(EquivalentType object)
      {
        return createEquivalentTypeAdapter();
      }
      @Override
      public Adapter caseRDFSDomain(RDFSDomain object)
      {
        return createRDFSDomainAdapter();
      }
      @Override
      public Adapter caseRDFSRange(RDFSRange object)
      {
        return createRDFSRangeAdapter();
      }
      @Override
      public Adapter caseOwlDataRange(OwlDataRange object)
      {
        return createOwlDataRangeAdapter();
      }
      @Override
      public Adapter caseRDFType(RDFType object)
      {
        return createRDFTypeAdapter();
      }
      @Override
      public Adapter caseSubPropertyOf(SubPropertyOf object)
      {
        return createSubPropertyOfAdapter();
      }
      @Override
      public Adapter caseDescription(Description object)
      {
        return createDescriptionAdapter();
      }
      @Override
      public Adapter caseOwlFirst(OwlFirst object)
      {
        return createOwlFirstAdapter();
      }
      @Override
      public Adapter caseRDFRest(RDFRest object)
      {
        return createRDFRestAdapter();
      }
      @Override
      public Adapter caseOwlType(OwlType object)
      {
        return createOwlTypeAdapter();
      }
      @Override
      public Adapter caseOwlMembers(OwlMembers object)
      {
        return createOwlMembersAdapter();
      }
      @Override
      public Adapter caseDatatype(Datatype object)
      {
        return createDatatypeAdapter();
      }
      @Override
      public Adapter caseSimpleRDFSDomain(SimpleRDFSDomain object)
      {
        return createSimpleRDFSDomainAdapter();
      }
      @Override
      public Adapter caseParsedRDFSDomain(ParsedRDFSDomain object)
      {
        return createParsedRDFSDomainAdapter();
      }
      @Override
      public Adapter caseSimpleRDFSRange(SimpleRDFSRange object)
      {
        return createSimpleRDFSRangeAdapter();
      }
      @Override
      public Adapter caseParsedPropertyRange(ParsedPropertyRange object)
      {
        return createParsedPropertyRangeAdapter();
      }
      @Override
      public Adapter defaultCase(EObject object)
      {
        return createEObjectAdapter();
      }
    };

  /**
   * Creates an adapter for the <code>target</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param target the object to adapt.
   * @return the adapter for the <code>target</code>.
   * @generated
   */
  @Override
  public Adapter createAdapter(Notifier target)
  {
    return modelSwitch.doSwitch((EObject)target);
  }


  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.OWLModel <em>OWL Model</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.OWLModel
   * @generated
   */
  public Adapter createOWLModelAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.DocType <em>Doc Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.DocType
   * @generated
   */
  public Adapter createDocTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.DocEntity <em>Doc Entity</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.DocEntity
   * @generated
   */
  public Adapter createDocEntityAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.RDFElement <em>RDF Element</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.RDFElement
   * @generated
   */
  public Adapter createRDFElementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.XmlNs <em>Xml Ns</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.XmlNs
   * @generated
   */
  public Adapter createXmlNsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.OwlRef <em>Owl Ref</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.OwlRef
   * @generated
   */
  public Adapter createOwlRefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.OwlID <em>Owl ID</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.OwlID
   * @generated
   */
  public Adapter createOwlIDAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.OntologyID <em>Ontology ID</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.OntologyID
   * @generated
   */
  public Adapter createOntologyIDAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.ParseType <em>Parse Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.ParseType
   * @generated
   */
  public Adapter createParseTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.OntologyElement <em>Ontology Element</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.OntologyElement
   * @generated
   */
  public Adapter createOntologyElementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.NamedIndividual <em>Named Individual</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.NamedIndividual
   * @generated
   */
  public Adapter createNamedIndividualAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.Ontology <em>Ontology</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.Ontology
   * @generated
   */
  public Adapter createOntologyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.SeeAlso <em>See Also</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.SeeAlso
   * @generated
   */
  public Adapter createSeeAlsoAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.PriorVersion <em>Prior Version</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.PriorVersion
   * @generated
   */
  public Adapter createPriorVersionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.BackwardsComp <em>Backwards Comp</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.BackwardsComp
   * @generated
   */
  public Adapter createBackwardsCompAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.OwlImport <em>Owl Import</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.OwlImport
   * @generated
   */
  public Adapter createOwlImportAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.OwlClass <em>Owl Class</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.OwlClass
   * @generated
   */
  public Adapter createOwlClassAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.MinimumClass <em>Minimum Class</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.MinimumClass
   * @generated
   */
  public Adapter createMinimumClassAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.ClassAxiom <em>Class Axiom</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.ClassAxiom
   * @generated
   */
  public Adapter createClassAxiomAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.ComplementAxiom <em>Complement Axiom</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.ComplementAxiom
   * @generated
   */
  public Adapter createComplementAxiomAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.IncompatibleWithAxiom <em>Incompatible With Axiom</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.IncompatibleWithAxiom
   * @generated
   */
  public Adapter createIncompatibleWithAxiomAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.DisjointAxiom <em>Disjoint Axiom</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.DisjointAxiom
   * @generated
   */
  public Adapter createDisjointAxiomAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.EquivalentAxiom <em>Equivalent Axiom</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.EquivalentAxiom
   * @generated
   */
  public Adapter createEquivalentAxiomAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.UnionAxiom <em>Union Axiom</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.UnionAxiom
   * @generated
   */
  public Adapter createUnionAxiomAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.IntersectionAxiom <em>Intersection Axiom</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.IntersectionAxiom
   * @generated
   */
  public Adapter createIntersectionAxiomAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.OneOfAxiom <em>One Of Axiom</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.OneOfAxiom
   * @generated
   */
  public Adapter createOneOfAxiomAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.OwlLabel <em>Owl Label</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.OwlLabel
   * @generated
   */
  public Adapter createOwlLabelAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.OwlVersion <em>Owl Version</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.OwlVersion
   * @generated
   */
  public Adapter createOwlVersionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.SubTypeOf <em>Sub Type Of</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.SubTypeOf
   * @generated
   */
  public Adapter createSubTypeOfAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.ClassModifier <em>Class Modifier</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.ClassModifier
   * @generated
   */
  public Adapter createClassModifierAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.OwlRestriction <em>Owl Restriction</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.OwlRestriction
   * @generated
   */
  public Adapter createOwlRestrictionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.RestrictionBody <em>Restriction Body</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.RestrictionBody
   * @generated
   */
  public Adapter createRestrictionBodyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.MinCardinality <em>Min Cardinality</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.MinCardinality
   * @generated
   */
  public Adapter createMinCardinalityAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.MaxCardinality <em>Max Cardinality</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.MaxCardinality
   * @generated
   */
  public Adapter createMaxCardinalityAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.Cardinality <em>Cardinality</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.Cardinality
   * @generated
   */
  public Adapter createCardinalityAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.QualifiedCardinality <em>Qualified Cardinality</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.QualifiedCardinality
   * @generated
   */
  public Adapter createQualifiedCardinalityAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.MinQualifiedCardinality <em>Min Qualified Cardinality</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.MinQualifiedCardinality
   * @generated
   */
  public Adapter createMinQualifiedCardinalityAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.MaxQualifiedCardinality <em>Max Qualified Cardinality</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.MaxQualifiedCardinality
   * @generated
   */
  public Adapter createMaxQualifiedCardinalityAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.SomeValues <em>Some Values</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.SomeValues
   * @generated
   */
  public Adapter createSomeValuesAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.AllValues <em>All Values</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.AllValues
   * @generated
   */
  public Adapter createAllValuesAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.HasValue <em>Has Value</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.HasValue
   * @generated
   */
  public Adapter createHasValueAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.Property <em>Property</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.Property
   * @generated
   */
  public Adapter createPropertyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.AnnotationProperty <em>Annotation Property</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.AnnotationProperty
   * @generated
   */
  public Adapter createAnnotationPropertyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.ObjectProperty <em>Object Property</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.ObjectProperty
   * @generated
   */
  public Adapter createObjectPropertyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.DatatypeProperty <em>Datatype Property</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.DatatypeProperty
   * @generated
   */
  public Adapter createDatatypePropertyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.FunctionalProperty <em>Functional Property</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.FunctionalProperty
   * @generated
   */
  public Adapter createFunctionalPropertyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.InverseFunctionalProperty <em>Inverse Functional Property</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.InverseFunctionalProperty
   * @generated
   */
  public Adapter createInverseFunctionalPropertyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.SymmetricProperty <em>Symmetric Property</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.SymmetricProperty
   * @generated
   */
  public Adapter createSymmetricPropertyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.TransitiveProperty <em>Transitive Property</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.TransitiveProperty
   * @generated
   */
  public Adapter createTransitivePropertyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.PropertyModifier <em>Property Modifier</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.PropertyModifier
   * @generated
   */
  public Adapter createPropertyModifierAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.InverseType <em>Inverse Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.InverseType
   * @generated
   */
  public Adapter createInverseTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.EquivalentType <em>Equivalent Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.EquivalentType
   * @generated
   */
  public Adapter createEquivalentTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.RDFSDomain <em>RDFS Domain</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.RDFSDomain
   * @generated
   */
  public Adapter createRDFSDomainAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.RDFSRange <em>RDFS Range</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.RDFSRange
   * @generated
   */
  public Adapter createRDFSRangeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.OwlDataRange <em>Owl Data Range</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.OwlDataRange
   * @generated
   */
  public Adapter createOwlDataRangeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.RDFType <em>RDF Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.RDFType
   * @generated
   */
  public Adapter createRDFTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.SubPropertyOf <em>Sub Property Of</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.SubPropertyOf
   * @generated
   */
  public Adapter createSubPropertyOfAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.Description <em>Description</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.Description
   * @generated
   */
  public Adapter createDescriptionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.OwlFirst <em>Owl First</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.OwlFirst
   * @generated
   */
  public Adapter createOwlFirstAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.RDFRest <em>RDF Rest</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.RDFRest
   * @generated
   */
  public Adapter createRDFRestAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.OwlType <em>Owl Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.OwlType
   * @generated
   */
  public Adapter createOwlTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.OwlMembers <em>Owl Members</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.OwlMembers
   * @generated
   */
  public Adapter createOwlMembersAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.Datatype <em>Datatype</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.Datatype
   * @generated
   */
  public Adapter createDatatypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.SimpleRDFSDomain <em>Simple RDFS Domain</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.SimpleRDFSDomain
   * @generated
   */
  public Adapter createSimpleRDFSDomainAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.ParsedRDFSDomain <em>Parsed RDFS Domain</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.ParsedRDFSDomain
   * @generated
   */
  public Adapter createParsedRDFSDomainAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.SimpleRDFSRange <em>Simple RDFS Range</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.SimpleRDFSRange
   * @generated
   */
  public Adapter createSimpleRDFSRangeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.smool.sdk.owlparser.owlmodel.ParsedPropertyRange <em>Parsed Property Range</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.smool.sdk.owlparser.owlmodel.ParsedPropertyRange
   * @generated
   */
  public Adapter createParsedPropertyRangeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for the default case.
   * <!-- begin-user-doc -->
   * This default implementation returns null.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @generated
   */
  public Adapter createEObjectAdapter()
  {
    return null;
  }

} //OwlmodelAdapterFactory
