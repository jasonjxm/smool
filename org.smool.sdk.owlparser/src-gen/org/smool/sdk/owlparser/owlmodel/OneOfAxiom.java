/**
 */
package org.smool.sdk.owlparser.owlmodel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>One Of Axiom</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OneOfAxiom#getValues <em>Values</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OneOfAxiom#getTagNames <em>Tag Names</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OneOfAxiom#getTagids <em>Tagids</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OneOfAxiom#getFirst <em>First</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OneOfAxiom#getRest <em>Rest</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOneOfAxiom()
 * @model
 * @generated
 */
public interface OneOfAxiom extends ClassAxiom
{
  /**
   * Returns the value of the '<em><b>Values</b></em>' containment reference list.
   * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Values</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOneOfAxiom_Values()
   * @model containment="true"
   * @generated
   */
  EList<EObject> getValues();

  /**
   * Returns the value of the '<em><b>Tag Names</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Tag Names</em>' attribute list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOneOfAxiom_TagNames()
   * @model unique="false"
   * @generated
   */
  EList<String> getTagNames();

  /**
   * Returns the value of the '<em><b>Tagids</b></em>' containment reference list.
   * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Tagids</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOneOfAxiom_Tagids()
   * @model containment="true"
   * @generated
   */
  EList<EObject> getTagids();

  /**
   * Returns the value of the '<em><b>First</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.OwlFirst}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>First</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOneOfAxiom_First()
   * @model containment="true"
   * @generated
   */
  EList<OwlFirst> getFirst();

  /**
   * Returns the value of the '<em><b>Rest</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.RDFRest}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Rest</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOneOfAxiom_Rest()
   * @model containment="true"
   * @generated
   */
  EList<RDFRest> getRest();

} // OneOfAxiom
