/**
 */
package org.smool.sdk.owlparser.owlmodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Description</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.Description#getAbout <em>About</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.Description#getFirst <em>First</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.Description#getType <em>Type</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.Description#getDomain <em>Domain</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.Description#getRange <em>Range</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.Description#getPropModifiers <em>Prop Modifiers</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.Description#getLabels <em>Labels</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.Description#getAxioms <em>Axioms</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getDescription()
 * @model
 * @generated
 */
public interface Description extends OntologyElement, ClassModifier
{
  /**
   * Returns the value of the '<em><b>About</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>About</em>' containment reference.
   * @see #setAbout(OwlRef)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getDescription_About()
   * @model containment="true"
   * @generated
   */
  OwlRef getAbout();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.Description#getAbout <em>About</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>About</em>' containment reference.
   * @see #getAbout()
   * @generated
   */
  void setAbout(OwlRef value);

  /**
   * Returns the value of the '<em><b>First</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.OwlFirst}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>First</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getDescription_First()
   * @model containment="true"
   * @generated
   */
  EList<OwlFirst> getFirst();

  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.OwlType}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getDescription_Type()
   * @model containment="true"
   * @generated
   */
  EList<OwlType> getType();

  /**
   * Returns the value of the '<em><b>Domain</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.RDFSDomain}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Domain</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getDescription_Domain()
   * @model containment="true"
   * @generated
   */
  EList<RDFSDomain> getDomain();

  /**
   * Returns the value of the '<em><b>Range</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.RDFSRange}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Range</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getDescription_Range()
   * @model containment="true"
   * @generated
   */
  EList<RDFSRange> getRange();

  /**
   * Returns the value of the '<em><b>Prop Modifiers</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.PropertyModifier}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Prop Modifiers</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getDescription_PropModifiers()
   * @model containment="true"
   * @generated
   */
  EList<PropertyModifier> getPropModifiers();

  /**
   * Returns the value of the '<em><b>Labels</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.OwlLabel}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Labels</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getDescription_Labels()
   * @model containment="true"
   * @generated
   */
  EList<OwlLabel> getLabels();

  /**
   * Returns the value of the '<em><b>Axioms</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.ClassAxiom}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Axioms</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getDescription_Axioms()
   * @model containment="true"
   * @generated
   */
  EList<ClassAxiom> getAxioms();

} // Description
