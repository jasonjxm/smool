/**
 */
package org.smool.sdk.owlparser.owlmodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.Property#getId <em>Id</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.Property#isReference <em>Reference</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.Property#getRef <em>Ref</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.Property#getOthers <em>Others</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.Property#getLabels <em>Labels</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.Property#getVersionInfo <em>Version Info</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.Property#getRanges <em>Ranges</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.Property#getTypes <em>Types</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.Property#getDomains <em>Domains</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.Property#getModifiers <em>Modifiers</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getProperty()
 * @model
 * @generated
 */
public interface Property extends OntologyElement
{
  /**
   * Returns the value of the '<em><b>Id</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Id</em>' containment reference.
   * @see #setId(OwlID)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getProperty_Id()
   * @model containment="true"
   * @generated
   */
  OwlID getId();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.Property#getId <em>Id</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Id</em>' containment reference.
   * @see #getId()
   * @generated
   */
  void setId(OwlID value);

  /**
   * Returns the value of the '<em><b>Reference</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Reference</em>' attribute.
   * @see #setReference(boolean)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getProperty_Reference()
   * @model
   * @generated
   */
  boolean isReference();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.Property#isReference <em>Reference</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Reference</em>' attribute.
   * @see #isReference()
   * @generated
   */
  void setReference(boolean value);

  /**
   * Returns the value of the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ref</em>' containment reference.
   * @see #setRef(OwlRef)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getProperty_Ref()
   * @model containment="true"
   * @generated
   */
  OwlRef getRef();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.Property#getRef <em>Ref</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ref</em>' containment reference.
   * @see #getRef()
   * @generated
   */
  void setRef(OwlRef value);

  /**
   * Returns the value of the '<em><b>Others</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Others</em>' attribute list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getProperty_Others()
   * @model unique="false"
   * @generated
   */
  EList<String> getOthers();

  /**
   * Returns the value of the '<em><b>Labels</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.OwlLabel}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Labels</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getProperty_Labels()
   * @model containment="true"
   * @generated
   */
  EList<OwlLabel> getLabels();

  /**
   * Returns the value of the '<em><b>Version Info</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.OwlVersion}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Version Info</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getProperty_VersionInfo()
   * @model containment="true"
   * @generated
   */
  EList<OwlVersion> getVersionInfo();

  /**
   * Returns the value of the '<em><b>Ranges</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.RDFSRange}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ranges</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getProperty_Ranges()
   * @model containment="true"
   * @generated
   */
  EList<RDFSRange> getRanges();

  /**
   * Returns the value of the '<em><b>Types</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.RDFType}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Types</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getProperty_Types()
   * @model containment="true"
   * @generated
   */
  EList<RDFType> getTypes();

  /**
   * Returns the value of the '<em><b>Domains</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.RDFSDomain}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Domains</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getProperty_Domains()
   * @model containment="true"
   * @generated
   */
  EList<RDFSDomain> getDomains();

  /**
   * Returns the value of the '<em><b>Modifiers</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.PropertyModifier}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Modifiers</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getProperty_Modifiers()
   * @model containment="true"
   * @generated
   */
  EList<PropertyModifier> getModifiers();

} // Property
