/**
 */
package org.smool.sdk.owlparser.owlmodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Symmetric Property</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getSymmetricProperty()
 * @model
 * @generated
 */
public interface SymmetricProperty extends Property
{
} // SymmetricProperty
