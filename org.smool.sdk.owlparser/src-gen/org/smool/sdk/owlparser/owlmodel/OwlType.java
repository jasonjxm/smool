/**
 */
package org.smool.sdk.owlparser.owlmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Owl Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OwlType#getRefType <em>Ref Type</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OwlType#getMembers <em>Members</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlType()
 * @model
 * @generated
 */
public interface OwlType extends EObject
{
  /**
   * Returns the value of the '<em><b>Ref Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ref Type</em>' containment reference.
   * @see #setRefType(OwlRef)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlType_RefType()
   * @model containment="true"
   * @generated
   */
  OwlRef getRefType();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.OwlType#getRefType <em>Ref Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ref Type</em>' containment reference.
   * @see #getRefType()
   * @generated
   */
  void setRefType(OwlRef value);

  /**
   * Returns the value of the '<em><b>Members</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Members</em>' containment reference.
   * @see #setMembers(OwlMembers)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlType_Members()
   * @model containment="true"
   * @generated
   */
  OwlMembers getMembers();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.OwlType#getMembers <em>Members</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Members</em>' containment reference.
   * @see #getMembers()
   * @generated
   */
  void setMembers(OwlMembers value);

} // OwlType
