/**
 */
package org.smool.sdk.owlparser.owlmodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Min Cardinality</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.MinCardinality#getDatatype <em>Datatype</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.MinCardinality#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getMinCardinality()
 * @model
 * @generated
 */
public interface MinCardinality extends RestrictionBody
{
  /**
   * Returns the value of the '<em><b>Datatype</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Datatype</em>' containment reference.
   * @see #setDatatype(OwlRef)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getMinCardinality_Datatype()
   * @model containment="true"
   * @generated
   */
  OwlRef getDatatype();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.MinCardinality#getDatatype <em>Datatype</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Datatype</em>' containment reference.
   * @see #getDatatype()
   * @generated
   */
  void setDatatype(OwlRef value);

  /**
   * Returns the value of the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' attribute.
   * @see #setValue(int)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getMinCardinality_Value()
   * @model
   * @generated
   */
  int getValue();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.MinCardinality#getValue <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' attribute.
   * @see #getValue()
   * @generated
   */
  void setValue(int value);

} // MinCardinality
