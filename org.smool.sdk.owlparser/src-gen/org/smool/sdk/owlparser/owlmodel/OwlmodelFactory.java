/**
 */
package org.smool.sdk.owlparser.owlmodel;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage
 * @generated
 */
public interface OwlmodelFactory extends EFactory
{
  /**
   * The singleton instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  OwlmodelFactory eINSTANCE = org.smool.sdk.owlparser.owlmodel.impl.OwlmodelFactoryImpl.init();

  /**
   * Returns a new object of class '<em>OWL Model</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>OWL Model</em>'.
   * @generated
   */
  OWLModel createOWLModel();

  /**
   * Returns a new object of class '<em>Doc Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Doc Type</em>'.
   * @generated
   */
  DocType createDocType();

  /**
   * Returns a new object of class '<em>Doc Entity</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Doc Entity</em>'.
   * @generated
   */
  DocEntity createDocEntity();

  /**
   * Returns a new object of class '<em>RDF Element</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>RDF Element</em>'.
   * @generated
   */
  RDFElement createRDFElement();

  /**
   * Returns a new object of class '<em>Xml Ns</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Xml Ns</em>'.
   * @generated
   */
  XmlNs createXmlNs();

  /**
   * Returns a new object of class '<em>Owl Ref</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Owl Ref</em>'.
   * @generated
   */
  OwlRef createOwlRef();

  /**
   * Returns a new object of class '<em>Owl ID</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Owl ID</em>'.
   * @generated
   */
  OwlID createOwlID();

  /**
   * Returns a new object of class '<em>Ontology ID</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Ontology ID</em>'.
   * @generated
   */
  OntologyID createOntologyID();

  /**
   * Returns a new object of class '<em>Parse Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Parse Type</em>'.
   * @generated
   */
  ParseType createParseType();

  /**
   * Returns a new object of class '<em>Ontology Element</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Ontology Element</em>'.
   * @generated
   */
  OntologyElement createOntologyElement();

  /**
   * Returns a new object of class '<em>Named Individual</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Named Individual</em>'.
   * @generated
   */
  NamedIndividual createNamedIndividual();

  /**
   * Returns a new object of class '<em>Ontology</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Ontology</em>'.
   * @generated
   */
  Ontology createOntology();

  /**
   * Returns a new object of class '<em>See Also</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>See Also</em>'.
   * @generated
   */
  SeeAlso createSeeAlso();

  /**
   * Returns a new object of class '<em>Prior Version</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Prior Version</em>'.
   * @generated
   */
  PriorVersion createPriorVersion();

  /**
   * Returns a new object of class '<em>Backwards Comp</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Backwards Comp</em>'.
   * @generated
   */
  BackwardsComp createBackwardsComp();

  /**
   * Returns a new object of class '<em>Owl Import</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Owl Import</em>'.
   * @generated
   */
  OwlImport createOwlImport();

  /**
   * Returns a new object of class '<em>Owl Class</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Owl Class</em>'.
   * @generated
   */
  OwlClass createOwlClass();

  /**
   * Returns a new object of class '<em>Minimum Class</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Minimum Class</em>'.
   * @generated
   */
  MinimumClass createMinimumClass();

  /**
   * Returns a new object of class '<em>Class Axiom</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Class Axiom</em>'.
   * @generated
   */
  ClassAxiom createClassAxiom();

  /**
   * Returns a new object of class '<em>Complement Axiom</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Complement Axiom</em>'.
   * @generated
   */
  ComplementAxiom createComplementAxiom();

  /**
   * Returns a new object of class '<em>Incompatible With Axiom</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Incompatible With Axiom</em>'.
   * @generated
   */
  IncompatibleWithAxiom createIncompatibleWithAxiom();

  /**
   * Returns a new object of class '<em>Disjoint Axiom</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Disjoint Axiom</em>'.
   * @generated
   */
  DisjointAxiom createDisjointAxiom();

  /**
   * Returns a new object of class '<em>Equivalent Axiom</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Equivalent Axiom</em>'.
   * @generated
   */
  EquivalentAxiom createEquivalentAxiom();

  /**
   * Returns a new object of class '<em>Union Axiom</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Union Axiom</em>'.
   * @generated
   */
  UnionAxiom createUnionAxiom();

  /**
   * Returns a new object of class '<em>Intersection Axiom</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Intersection Axiom</em>'.
   * @generated
   */
  IntersectionAxiom createIntersectionAxiom();

  /**
   * Returns a new object of class '<em>One Of Axiom</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>One Of Axiom</em>'.
   * @generated
   */
  OneOfAxiom createOneOfAxiom();

  /**
   * Returns a new object of class '<em>Owl Label</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Owl Label</em>'.
   * @generated
   */
  OwlLabel createOwlLabel();

  /**
   * Returns a new object of class '<em>Owl Version</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Owl Version</em>'.
   * @generated
   */
  OwlVersion createOwlVersion();

  /**
   * Returns a new object of class '<em>Sub Type Of</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Sub Type Of</em>'.
   * @generated
   */
  SubTypeOf createSubTypeOf();

  /**
   * Returns a new object of class '<em>Class Modifier</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Class Modifier</em>'.
   * @generated
   */
  ClassModifier createClassModifier();

  /**
   * Returns a new object of class '<em>Owl Restriction</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Owl Restriction</em>'.
   * @generated
   */
  OwlRestriction createOwlRestriction();

  /**
   * Returns a new object of class '<em>Restriction Body</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Restriction Body</em>'.
   * @generated
   */
  RestrictionBody createRestrictionBody();

  /**
   * Returns a new object of class '<em>Min Cardinality</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Min Cardinality</em>'.
   * @generated
   */
  MinCardinality createMinCardinality();

  /**
   * Returns a new object of class '<em>Max Cardinality</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Max Cardinality</em>'.
   * @generated
   */
  MaxCardinality createMaxCardinality();

  /**
   * Returns a new object of class '<em>Cardinality</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Cardinality</em>'.
   * @generated
   */
  Cardinality createCardinality();

  /**
   * Returns a new object of class '<em>Qualified Cardinality</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Qualified Cardinality</em>'.
   * @generated
   */
  QualifiedCardinality createQualifiedCardinality();

  /**
   * Returns a new object of class '<em>Min Qualified Cardinality</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Min Qualified Cardinality</em>'.
   * @generated
   */
  MinQualifiedCardinality createMinQualifiedCardinality();

  /**
   * Returns a new object of class '<em>Max Qualified Cardinality</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Max Qualified Cardinality</em>'.
   * @generated
   */
  MaxQualifiedCardinality createMaxQualifiedCardinality();

  /**
   * Returns a new object of class '<em>Some Values</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Some Values</em>'.
   * @generated
   */
  SomeValues createSomeValues();

  /**
   * Returns a new object of class '<em>All Values</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>All Values</em>'.
   * @generated
   */
  AllValues createAllValues();

  /**
   * Returns a new object of class '<em>Has Value</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Has Value</em>'.
   * @generated
   */
  HasValue createHasValue();

  /**
   * Returns a new object of class '<em>Property</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Property</em>'.
   * @generated
   */
  Property createProperty();

  /**
   * Returns a new object of class '<em>Annotation Property</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Annotation Property</em>'.
   * @generated
   */
  AnnotationProperty createAnnotationProperty();

  /**
   * Returns a new object of class '<em>Object Property</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Object Property</em>'.
   * @generated
   */
  ObjectProperty createObjectProperty();

  /**
   * Returns a new object of class '<em>Datatype Property</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Datatype Property</em>'.
   * @generated
   */
  DatatypeProperty createDatatypeProperty();

  /**
   * Returns a new object of class '<em>Functional Property</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Functional Property</em>'.
   * @generated
   */
  FunctionalProperty createFunctionalProperty();

  /**
   * Returns a new object of class '<em>Inverse Functional Property</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Inverse Functional Property</em>'.
   * @generated
   */
  InverseFunctionalProperty createInverseFunctionalProperty();

  /**
   * Returns a new object of class '<em>Symmetric Property</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Symmetric Property</em>'.
   * @generated
   */
  SymmetricProperty createSymmetricProperty();

  /**
   * Returns a new object of class '<em>Transitive Property</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Transitive Property</em>'.
   * @generated
   */
  TransitiveProperty createTransitiveProperty();

  /**
   * Returns a new object of class '<em>Property Modifier</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Property Modifier</em>'.
   * @generated
   */
  PropertyModifier createPropertyModifier();

  /**
   * Returns a new object of class '<em>Inverse Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Inverse Type</em>'.
   * @generated
   */
  InverseType createInverseType();

  /**
   * Returns a new object of class '<em>Equivalent Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Equivalent Type</em>'.
   * @generated
   */
  EquivalentType createEquivalentType();

  /**
   * Returns a new object of class '<em>RDFS Domain</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>RDFS Domain</em>'.
   * @generated
   */
  RDFSDomain createRDFSDomain();

  /**
   * Returns a new object of class '<em>RDFS Range</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>RDFS Range</em>'.
   * @generated
   */
  RDFSRange createRDFSRange();

  /**
   * Returns a new object of class '<em>Owl Data Range</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Owl Data Range</em>'.
   * @generated
   */
  OwlDataRange createOwlDataRange();

  /**
   * Returns a new object of class '<em>RDF Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>RDF Type</em>'.
   * @generated
   */
  RDFType createRDFType();

  /**
   * Returns a new object of class '<em>Sub Property Of</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Sub Property Of</em>'.
   * @generated
   */
  SubPropertyOf createSubPropertyOf();

  /**
   * Returns a new object of class '<em>Description</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Description</em>'.
   * @generated
   */
  Description createDescription();

  /**
   * Returns a new object of class '<em>Owl First</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Owl First</em>'.
   * @generated
   */
  OwlFirst createOwlFirst();

  /**
   * Returns a new object of class '<em>RDF Rest</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>RDF Rest</em>'.
   * @generated
   */
  RDFRest createRDFRest();

  /**
   * Returns a new object of class '<em>Owl Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Owl Type</em>'.
   * @generated
   */
  OwlType createOwlType();

  /**
   * Returns a new object of class '<em>Owl Members</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Owl Members</em>'.
   * @generated
   */
  OwlMembers createOwlMembers();

  /**
   * Returns a new object of class '<em>Datatype</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Datatype</em>'.
   * @generated
   */
  Datatype createDatatype();

  /**
   * Returns a new object of class '<em>Simple RDFS Domain</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Simple RDFS Domain</em>'.
   * @generated
   */
  SimpleRDFSDomain createSimpleRDFSDomain();

  /**
   * Returns a new object of class '<em>Parsed RDFS Domain</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Parsed RDFS Domain</em>'.
   * @generated
   */
  ParsedRDFSDomain createParsedRDFSDomain();

  /**
   * Returns a new object of class '<em>Simple RDFS Range</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Simple RDFS Range</em>'.
   * @generated
   */
  SimpleRDFSRange createSimpleRDFSRange();

  /**
   * Returns a new object of class '<em>Parsed Property Range</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Parsed Property Range</em>'.
   * @generated
   */
  ParsedPropertyRange createParsedPropertyRange();

  /**
   * Returns the package supported by this factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the package supported by this factory.
   * @generated
   */
  OwlmodelPackage getOwlmodelPackage();

} //OwlmodelFactory
