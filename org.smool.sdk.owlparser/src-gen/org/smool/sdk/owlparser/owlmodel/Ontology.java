/**
 */
package org.smool.sdk.owlparser.owlmodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ontology</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.Ontology#getAbout <em>About</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.Ontology#getResource <em>Resource</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.Ontology#getLabels <em>Labels</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.Ontology#getVersionInfo <em>Version Info</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.Ontology#getSeeAlso <em>See Also</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.Ontology#getPVers <em>PVers</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.Ontology#getBackComp <em>Back Comp</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.Ontology#getImportedOntologies <em>Imported Ontologies</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.Ontology#getOthers <em>Others</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOntology()
 * @model
 * @generated
 */
public interface Ontology extends OntologyElement
{
  /**
   * Returns the value of the '<em><b>About</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>About</em>' containment reference.
   * @see #setAbout(OntologyID)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOntology_About()
   * @model containment="true"
   * @generated
   */
  OntologyID getAbout();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.Ontology#getAbout <em>About</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>About</em>' containment reference.
   * @see #getAbout()
   * @generated
   */
  void setAbout(OntologyID value);

  /**
   * Returns the value of the '<em><b>Resource</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Resource</em>' containment reference.
   * @see #setResource(OwlRef)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOntology_Resource()
   * @model containment="true"
   * @generated
   */
  OwlRef getResource();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.Ontology#getResource <em>Resource</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Resource</em>' containment reference.
   * @see #getResource()
   * @generated
   */
  void setResource(OwlRef value);

  /**
   * Returns the value of the '<em><b>Labels</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.OwlLabel}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Labels</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOntology_Labels()
   * @model containment="true"
   * @generated
   */
  EList<OwlLabel> getLabels();

  /**
   * Returns the value of the '<em><b>Version Info</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.OwlVersion}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Version Info</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOntology_VersionInfo()
   * @model containment="true"
   * @generated
   */
  EList<OwlVersion> getVersionInfo();

  /**
   * Returns the value of the '<em><b>See Also</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.SeeAlso}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>See Also</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOntology_SeeAlso()
   * @model containment="true"
   * @generated
   */
  EList<SeeAlso> getSeeAlso();

  /**
   * Returns the value of the '<em><b>PVers</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.PriorVersion}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>PVers</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOntology_PVers()
   * @model containment="true"
   * @generated
   */
  EList<PriorVersion> getPVers();

  /**
   * Returns the value of the '<em><b>Back Comp</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.BackwardsComp}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Back Comp</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOntology_BackComp()
   * @model containment="true"
   * @generated
   */
  EList<BackwardsComp> getBackComp();

  /**
   * Returns the value of the '<em><b>Imported Ontologies</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.OwlImport}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Imported Ontologies</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOntology_ImportedOntologies()
   * @model containment="true"
   * @generated
   */
  EList<OwlImport> getImportedOntologies();

  /**
   * Returns the value of the '<em><b>Others</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Others</em>' attribute list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOntology_Others()
   * @model unique="false"
   * @generated
   */
  EList<String> getOthers();

} // Ontology
