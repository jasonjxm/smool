/**
 */
package org.smool.sdk.owlparser.owlmodel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Owl Members</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OwlMembers#getParseType <em>Parse Type</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OwlMembers#getDescriptions <em>Descriptions</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlMembers()
 * @model
 * @generated
 */
public interface OwlMembers extends EObject
{
  /**
   * Returns the value of the '<em><b>Parse Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Parse Type</em>' containment reference.
   * @see #setParseType(ParseType)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlMembers_ParseType()
   * @model containment="true"
   * @generated
   */
  ParseType getParseType();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.OwlMembers#getParseType <em>Parse Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Parse Type</em>' containment reference.
   * @see #getParseType()
   * @generated
   */
  void setParseType(ParseType value);

  /**
   * Returns the value of the '<em><b>Descriptions</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.Description}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Descriptions</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlMembers_Descriptions()
   * @model containment="true"
   * @generated
   */
  EList<Description> getDescriptions();

} // OwlMembers
