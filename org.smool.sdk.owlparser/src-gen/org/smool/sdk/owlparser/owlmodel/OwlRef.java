/**
 */
package org.smool.sdk.owlparser.owlmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Owl Ref</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OwlRef#getOntURI <em>Ont URI</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OwlRef#getOntName <em>Ont Name</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OwlRef#getElemID <em>Elem ID</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OwlRef#getFullURI <em>Full URI</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlRef()
 * @model
 * @generated
 */
public interface OwlRef extends EObject
{
  /**
   * Returns the value of the '<em><b>Ont URI</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ont URI</em>' attribute.
   * @see #setOntURI(String)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlRef_OntURI()
   * @model
   * @generated
   */
  String getOntURI();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.OwlRef#getOntURI <em>Ont URI</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ont URI</em>' attribute.
   * @see #getOntURI()
   * @generated
   */
  void setOntURI(String value);

  /**
   * Returns the value of the '<em><b>Ont Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ont Name</em>' attribute.
   * @see #setOntName(String)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlRef_OntName()
   * @model
   * @generated
   */
  String getOntName();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.OwlRef#getOntName <em>Ont Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ont Name</em>' attribute.
   * @see #getOntName()
   * @generated
   */
  void setOntName(String value);

  /**
   * Returns the value of the '<em><b>Elem ID</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Elem ID</em>' attribute.
   * @see #setElemID(String)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlRef_ElemID()
   * @model
   * @generated
   */
  String getElemID();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.OwlRef#getElemID <em>Elem ID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Elem ID</em>' attribute.
   * @see #getElemID()
   * @generated
   */
  void setElemID(String value);

  /**
   * Returns the value of the '<em><b>Full URI</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Full URI</em>' attribute.
   * @see #setFullURI(String)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlRef_FullURI()
   * @model
   * @generated
   */
  String getFullURI();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.OwlRef#getFullURI <em>Full URI</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Full URI</em>' attribute.
   * @see #getFullURI()
   * @generated
   */
  void setFullURI(String value);

} // OwlRef
