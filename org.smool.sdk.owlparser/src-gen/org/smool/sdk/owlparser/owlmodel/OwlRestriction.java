/**
 */
package org.smool.sdk.owlparser.owlmodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Owl Restriction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OwlRestriction#getRestrictionBodies <em>Restriction Bodies</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OwlRestriction#getProp <em>Prop</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OwlRestriction#getProperty <em>Property</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OwlRestriction#getClass_ <em>Class</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OwlRestriction#getClasses <em>Classes</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlRestriction()
 * @model
 * @generated
 */
public interface OwlRestriction extends ClassModifier
{
  /**
   * Returns the value of the '<em><b>Restriction Bodies</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.RestrictionBody}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Restriction Bodies</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlRestriction_RestrictionBodies()
   * @model containment="true"
   * @generated
   */
  EList<RestrictionBody> getRestrictionBodies();

  /**
   * Returns the value of the '<em><b>Prop</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Prop</em>' containment reference.
   * @see #setProp(OwlRef)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlRestriction_Prop()
   * @model containment="true"
   * @generated
   */
  OwlRef getProp();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.OwlRestriction#getProp <em>Prop</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Prop</em>' containment reference.
   * @see #getProp()
   * @generated
   */
  void setProp(OwlRef value);

  /**
   * Returns the value of the '<em><b>Property</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Property</em>' containment reference.
   * @see #setProperty(Property)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlRestriction_Property()
   * @model containment="true"
   * @generated
   */
  Property getProperty();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.OwlRestriction#getProperty <em>Property</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Property</em>' containment reference.
   * @see #getProperty()
   * @generated
   */
  void setProperty(Property value);

  /**
   * Returns the value of the '<em><b>Class</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Class</em>' containment reference.
   * @see #setClass(OwlRef)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlRestriction_Class()
   * @model containment="true"
   * @generated
   */
  OwlRef getClass_();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.OwlRestriction#getClass_ <em>Class</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Class</em>' containment reference.
   * @see #getClass_()
   * @generated
   */
  void setClass(OwlRef value);

  /**
   * Returns the value of the '<em><b>Classes</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.ClassModifier}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Classes</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlRestriction_Classes()
   * @model containment="true"
   * @generated
   */
  EList<ClassModifier> getClasses();

} // OwlRestriction
