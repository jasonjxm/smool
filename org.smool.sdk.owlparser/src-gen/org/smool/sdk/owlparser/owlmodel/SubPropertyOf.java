/**
 */
package org.smool.sdk.owlparser.owlmodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sub Property Of</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.SubPropertyOf#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getSubPropertyOf()
 * @model
 * @generated
 */
public interface SubPropertyOf extends PropertyModifier
{
  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference.
   * @see #setType(OwlRef)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getSubPropertyOf_Type()
   * @model containment="true"
   * @generated
   */
  OwlRef getType();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.SubPropertyOf#getType <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' containment reference.
   * @see #getType()
   * @generated
   */
  void setType(OwlRef value);

} // SubPropertyOf
