/**
 */
package org.smool.sdk.owlparser.owlmodel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Doc Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.DocType#getDocID <em>Doc ID</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.DocType#getEntities <em>Entities</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getDocType()
 * @model
 * @generated
 */
public interface DocType extends EObject
{
  /**
   * Returns the value of the '<em><b>Doc ID</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Doc ID</em>' attribute.
   * @see #setDocID(String)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getDocType_DocID()
   * @model
   * @generated
   */
  String getDocID();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.DocType#getDocID <em>Doc ID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Doc ID</em>' attribute.
   * @see #getDocID()
   * @generated
   */
  void setDocID(String value);

  /**
   * Returns the value of the '<em><b>Entities</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.DocEntity}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Entities</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getDocType_Entities()
   * @model containment="true"
   * @generated
   */
  EList<DocEntity> getEntities();

} // DocType
