/**
 */
package org.smool.sdk.owlparser.owlmodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Inverse Functional Property</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getInverseFunctionalProperty()
 * @model
 * @generated
 */
public interface InverseFunctionalProperty extends Property
{
} // InverseFunctionalProperty
