/**
 */
package org.smool.sdk.owlparser.owlmodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Equivalent Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.EquivalentType#getRef <em>Ref</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getEquivalentType()
 * @model
 * @generated
 */
public interface EquivalentType extends PropertyModifier
{
  /**
   * Returns the value of the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ref</em>' containment reference.
   * @see #setRef(OwlRef)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getEquivalentType_Ref()
   * @model containment="true"
   * @generated
   */
  OwlRef getRef();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.EquivalentType#getRef <em>Ref</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ref</em>' containment reference.
   * @see #getRef()
   * @generated
   */
  void setRef(OwlRef value);

} // EquivalentType
