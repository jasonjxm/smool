/**
 */
package org.smool.sdk.owlparser.owlmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ontology Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOntologyElement()
 * @model
 * @generated
 */
public interface OntologyElement extends EObject
{
} // OntologyElement
