/**
 */
package org.smool.sdk.owlparser.owlmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Owl ID</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OwlID#getElemID <em>Elem ID</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlID()
 * @model
 * @generated
 */
public interface OwlID extends EObject
{
  /**
   * Returns the value of the '<em><b>Elem ID</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Elem ID</em>' attribute.
   * @see #setElemID(String)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlID_ElemID()
   * @model
   * @generated
   */
  String getElemID();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.OwlID#getElemID <em>Elem ID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Elem ID</em>' attribute.
   * @see #getElemID()
   * @generated
   */
  void setElemID(String value);

} // OwlID
