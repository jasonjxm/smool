/**
 */
package org.smool.sdk.owlparser.owlmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Xml Ns</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.XmlNs#getNsid <em>Nsid</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.XmlNs#getNsuri <em>Nsuri</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.XmlNs#getNsname <em>Nsname</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getXmlNs()
 * @model
 * @generated
 */
public interface XmlNs extends EObject
{
  /**
   * Returns the value of the '<em><b>Nsid</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Nsid</em>' attribute.
   * @see #setNsid(String)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getXmlNs_Nsid()
   * @model
   * @generated
   */
  String getNsid();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.XmlNs#getNsid <em>Nsid</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Nsid</em>' attribute.
   * @see #getNsid()
   * @generated
   */
  void setNsid(String value);

  /**
   * Returns the value of the '<em><b>Nsuri</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Nsuri</em>' attribute.
   * @see #setNsuri(String)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getXmlNs_Nsuri()
   * @model
   * @generated
   */
  String getNsuri();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.XmlNs#getNsuri <em>Nsuri</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Nsuri</em>' attribute.
   * @see #getNsuri()
   * @generated
   */
  void setNsuri(String value);

  /**
   * Returns the value of the '<em><b>Nsname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Nsname</em>' attribute.
   * @see #setNsname(String)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getXmlNs_Nsname()
   * @model
   * @generated
   */
  String getNsname();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.XmlNs#getNsname <em>Nsname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Nsname</em>' attribute.
   * @see #getNsname()
   * @generated
   */
  void setNsname(String value);

} // XmlNs
