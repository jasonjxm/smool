/**
 */
package org.smool.sdk.owlparser.owlmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Doc Entity</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.DocEntity#getEntityID <em>Entity ID</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.DocEntity#getEntityURI <em>Entity URI</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getDocEntity()
 * @model
 * @generated
 */
public interface DocEntity extends EObject
{
  /**
   * Returns the value of the '<em><b>Entity ID</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Entity ID</em>' attribute.
   * @see #setEntityID(String)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getDocEntity_EntityID()
   * @model
   * @generated
   */
  String getEntityID();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.DocEntity#getEntityID <em>Entity ID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Entity ID</em>' attribute.
   * @see #getEntityID()
   * @generated
   */
  void setEntityID(String value);

  /**
   * Returns the value of the '<em><b>Entity URI</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Entity URI</em>' attribute.
   * @see #setEntityURI(String)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getDocEntity_EntityURI()
   * @model
   * @generated
   */
  String getEntityURI();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.DocEntity#getEntityURI <em>Entity URI</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Entity URI</em>' attribute.
   * @see #getEntityURI()
   * @generated
   */
  void setEntityURI(String value);

} // DocEntity
