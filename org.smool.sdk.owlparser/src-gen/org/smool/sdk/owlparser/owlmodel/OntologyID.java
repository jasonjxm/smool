/**
 */
package org.smool.sdk.owlparser.owlmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ontology ID</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OntologyID#getSuperOnt <em>Super Ont</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OntologyID#getOntURI <em>Ont URI</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOntologyID()
 * @model
 * @generated
 */
public interface OntologyID extends EObject
{
  /**
   * Returns the value of the '<em><b>Super Ont</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Super Ont</em>' attribute.
   * @see #setSuperOnt(String)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOntologyID_SuperOnt()
   * @model
   * @generated
   */
  String getSuperOnt();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.OntologyID#getSuperOnt <em>Super Ont</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Super Ont</em>' attribute.
   * @see #getSuperOnt()
   * @generated
   */
  void setSuperOnt(String value);

  /**
   * Returns the value of the '<em><b>Ont URI</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ont URI</em>' attribute.
   * @see #setOntURI(String)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOntologyID_OntURI()
   * @model
   * @generated
   */
  String getOntURI();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.OntologyID#getOntURI <em>Ont URI</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ont URI</em>' attribute.
   * @see #getOntURI()
   * @generated
   */
  void setOntURI(String value);

} // OntologyID
