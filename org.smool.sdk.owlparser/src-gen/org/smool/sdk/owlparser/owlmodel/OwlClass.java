/**
 */
package org.smool.sdk.owlparser.owlmodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Owl Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OwlClass#getId <em>Id</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OwlClass#isReference <em>Reference</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OwlClass#getRef <em>Ref</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OwlClass#getLabels <em>Labels</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OwlClass#getVersionInfo <em>Version Info</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OwlClass#getSuperTypes <em>Super Types</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OwlClass#getAxioms <em>Axioms</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OwlClass#getOnts <em>Onts</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.OwlClass#getOthers <em>Others</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlClass()
 * @model
 * @generated
 */
public interface OwlClass extends OntologyElement, ClassModifier
{
  /**
   * Returns the value of the '<em><b>Id</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Id</em>' containment reference.
   * @see #setId(OwlID)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlClass_Id()
   * @model containment="true"
   * @generated
   */
  OwlID getId();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.OwlClass#getId <em>Id</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Id</em>' containment reference.
   * @see #getId()
   * @generated
   */
  void setId(OwlID value);

  /**
   * Returns the value of the '<em><b>Reference</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Reference</em>' attribute.
   * @see #setReference(boolean)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlClass_Reference()
   * @model
   * @generated
   */
  boolean isReference();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.OwlClass#isReference <em>Reference</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Reference</em>' attribute.
   * @see #isReference()
   * @generated
   */
  void setReference(boolean value);

  /**
   * Returns the value of the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ref</em>' containment reference.
   * @see #setRef(OwlRef)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlClass_Ref()
   * @model containment="true"
   * @generated
   */
  OwlRef getRef();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.OwlClass#getRef <em>Ref</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ref</em>' containment reference.
   * @see #getRef()
   * @generated
   */
  void setRef(OwlRef value);

  /**
   * Returns the value of the '<em><b>Labels</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.OwlLabel}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Labels</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlClass_Labels()
   * @model containment="true"
   * @generated
   */
  EList<OwlLabel> getLabels();

  /**
   * Returns the value of the '<em><b>Version Info</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.OwlVersion}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Version Info</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlClass_VersionInfo()
   * @model containment="true"
   * @generated
   */
  EList<OwlVersion> getVersionInfo();

  /**
   * Returns the value of the '<em><b>Super Types</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.SubTypeOf}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Super Types</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlClass_SuperTypes()
   * @model containment="true"
   * @generated
   */
  EList<SubTypeOf> getSuperTypes();

  /**
   * Returns the value of the '<em><b>Axioms</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.ClassAxiom}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Axioms</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlClass_Axioms()
   * @model containment="true"
   * @generated
   */
  EList<ClassAxiom> getAxioms();

  /**
   * Returns the value of the '<em><b>Onts</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.Ontology}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Onts</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlClass_Onts()
   * @model containment="true"
   * @generated
   */
  EList<Ontology> getOnts();

  /**
   * Returns the value of the '<em><b>Others</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Others</em>' attribute list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getOwlClass_Others()
   * @model unique="false"
   * @generated
   */
  EList<String> getOthers();

} // OwlClass
