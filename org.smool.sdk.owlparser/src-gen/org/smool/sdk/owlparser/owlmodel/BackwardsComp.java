/**
 */
package org.smool.sdk.owlparser.owlmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Backwards Comp</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.BackwardsComp#getRef <em>Ref</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getBackwardsComp()
 * @model
 * @generated
 */
public interface BackwardsComp extends EObject
{
  /**
   * Returns the value of the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ref</em>' containment reference.
   * @see #setRef(OntologyID)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getBackwardsComp_Ref()
   * @model containment="true"
   * @generated
   */
  OntologyID getRef();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.BackwardsComp#getRef <em>Ref</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ref</em>' containment reference.
   * @see #getRef()
   * @generated
   */
  void setRef(OntologyID value);

} // BackwardsComp
