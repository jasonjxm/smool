/**
 */
package org.smool.sdk.owlparser.owlmodel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>RDF Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.RDFElement#getImportedNs <em>Imported Ns</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.RDFElement#getBaseOnt <em>Base Ont</em>}</li>
 *   <li>{@link org.smool.sdk.owlparser.owlmodel.RDFElement#getOntologyElements <em>Ontology Elements</em>}</li>
 * </ul>
 *
 * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getRDFElement()
 * @model
 * @generated
 */
public interface RDFElement extends EObject
{
  /**
   * Returns the value of the '<em><b>Imported Ns</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.XmlNs}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Imported Ns</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getRDFElement_ImportedNs()
   * @model containment="true"
   * @generated
   */
  EList<XmlNs> getImportedNs();

  /**
   * Returns the value of the '<em><b>Base Ont</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Base Ont</em>' attribute.
   * @see #setBaseOnt(String)
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getRDFElement_BaseOnt()
   * @model
   * @generated
   */
  String getBaseOnt();

  /**
   * Sets the value of the '{@link org.smool.sdk.owlparser.owlmodel.RDFElement#getBaseOnt <em>Base Ont</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Base Ont</em>' attribute.
   * @see #getBaseOnt()
   * @generated
   */
  void setBaseOnt(String value);

  /**
   * Returns the value of the '<em><b>Ontology Elements</b></em>' containment reference list.
   * The list contents are of type {@link org.smool.sdk.owlparser.owlmodel.OntologyElement}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ontology Elements</em>' containment reference list.
   * @see org.smool.sdk.owlparser.owlmodel.OwlmodelPackage#getRDFElement_OntologyElements()
   * @model containment="true"
   * @generated
   */
  EList<OntologyElement> getOntologyElements();

} // RDFElement
