package org.smool.sdk.owlparser.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.smool.sdk.owlparser.services.OWLModelGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalOWLModelParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_XMI_HEADER", "RULE_NSID", "RULE_ID", "RULE_STR_DELIMITER", "RULE_URI", "RULE_HASHTAG", "RULE_AMPERSAND", "RULE_SEMICOLON", "RULE_NSNAME", "RULE_INT", "RULE_ANY_OTHER", "RULE_ML_COMMENT", "RULE_RDFS_COMMENT", "RULE_WS", "'<!DOCTYPE'", "'['", "']'", "'>'", "'<!ENTITY'", "'<rdf:RDF'", "'xml:base'", "'='", "'</rdf:RDF'", "'<owl:NamedIndividual'", "'rdf:about'", "'/>'", "'</owl:NamedIndividual'", "'<owl:Ontology'", "'rdf:resource'", "'</owl:Ontology'", "'<rdfs:seeAlso'", "'<owl:priorVersion'", "'<owl:backwardCompatibleWith'", "'<owl:imports'", "'</owl:imports'", "'<owl:Class'", "'rdf:ID'", "'</owl:Class'", "'<owl:complementOf'", "'</owl:complementOf'", "'rdf:parseType'", "'<owl:incompatibleWith'", "'</owl:incompatibleWith'", "'<owl:disjointWith'", "'</owl:disjointWith'", "'<owl:equivalentClass'", "'</owl:equivalentClass'", "'<owl:unionOf'", "'</owl:unionOf'", "'<owl:intersectionOf'", "'</owl:intersectionOf'", "'<owl:oneOf'", "'<owl:Thing'", "'</owl:Thing'", "'<'", "'</owl:oneOf'", "'<rdfs:label'", "'xml:lang'", "'rdf:datatype'", "'</rdfs:label'", "'<owl:versionInfo'", "'</owl:versionInfo'", "'<rdfs:subClassOf'", "'</rdfs:subClassOf'", "'<owl:subClassOf'", "'</owl:subClassOf'", "'<owl:Restriction'", "'<owl:onProperty'", "'</owl:onProperty'", "'<owl:onClass'", "'</owl:onClass'", "'</owl:Restriction'", "'<owl:minCardinality'", "'</owl:minCardinality'", "'<owl:maxCardinality'", "'</owl:maxCardinality'", "'<owl:cardinality'", "'</owl:cardinality'", "'<owl:qualifiedCardinality'", "'</owl:qualifiedCardinality'", "'<owl:minQualifiedCardinality'", "'</owl:minQualifiedCardinality'", "'<owl:maxQualifiedCardinality'", "'</owl:maxQualifiedCardinality'", "'<owl:someValuesFrom'", "'</owl:someValuesFrom'", "'<owl:allValuesFrom'", "'</owl:allValuesFrom'", "'<owl:hasValue'", "'</owl:hasValue'", "'<owl:AnnotationProperty'", "'</owl:AnnotationProperty'", "'<owl:ObjectProperty'", "'</owl:ObjectProperty'", "'<owl:DatatypeProperty'", "'</owl:DatatypeProperty'", "'<owl:FunctionalProperty'", "'</owl:FunctionalProperty'", "'<owl:InverseFunctionalProperty'", "'</owl:InverseFunctionalProperty'", "'<owl:SymmetricProperty'", "'</owl:SymmetricProperty'", "'<owl:TransitiveProperty'", "'</owl:TransitiveProperty'", "'<owl:inverseOf'", "'</owl:inverseOf'", "'<owl:equivalentProperty'", "'</owl:equivalentProperty'", "'<rdfs:domain'", "'</rdfs:domain'", "'<rdfs:range'", "'</rdfs:range'", "'<owl:DataRange'", "'</owl:DataRange'", "'<rdf:type'", "'</rdf:type'", "'<rdfs:subPropertyOf'", "'</rdfs:subPropertyOf'", "'<rdf:Description'", "'</rdf:Description'", "'<rdf:first'", "'</rdf:first'", "'<rdf:rest'", "'</rdf:rest'", "'<owl:members'", "'</owl:members'", "'<rdfs:Datatype'", "'</rdfs:Datatype'", "'rdfs:'", "'owl:'", "'rdf:'", "'</'"
    };
    public static final int T__50=50;
    public static final int T__59=59;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__133=133;
    public static final int T__132=132;
    public static final int T__60=60;
    public static final int T__135=135;
    public static final int T__61=61;
    public static final int T__134=134;
    public static final int RULE_ID=6;
    public static final int RULE_NSID=5;
    public static final int T__131=131;
    public static final int T__130=130;
    public static final int RULE_INT=13;
    public static final int T__66=66;
    public static final int RULE_ML_COMMENT=15;
    public static final int T__67=67;
    public static final int T__129=129;
    public static final int T__68=68;
    public static final int T__69=69;
    public static final int T__62=62;
    public static final int T__126=126;
    public static final int T__63=63;
    public static final int T__125=125;
    public static final int T__64=64;
    public static final int T__128=128;
    public static final int T__65=65;
    public static final int T__127=127;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_STR_DELIMITER=7;
    public static final int RULE_HASHTAG=9;
    public static final int RULE_AMPERSAND=10;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__91=91;
    public static final int T__100=100;
    public static final int T__92=92;
    public static final int T__93=93;
    public static final int T__102=102;
    public static final int T__94=94;
    public static final int T__101=101;
    public static final int T__90=90;
    public static final int RULE_XMI_HEADER=4;
    public static final int T__19=19;
    public static final int RULE_NSNAME=12;
    public static final int T__18=18;
    public static final int T__99=99;
    public static final int T__95=95;
    public static final int T__96=96;
    public static final int T__97=97;
    public static final int T__98=98;
    public static final int RULE_RDFS_COMMENT=16;
    public static final int RULE_URI=8;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int RULE_SEMICOLON=11;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__122=122;
    public static final int T__70=70;
    public static final int T__121=121;
    public static final int T__71=71;
    public static final int T__124=124;
    public static final int T__72=72;
    public static final int T__123=123;
    public static final int T__120=120;
    public static final int T__77=77;
    public static final int T__119=119;
    public static final int T__78=78;
    public static final int T__118=118;
    public static final int T__79=79;
    public static final int T__73=73;
    public static final int T__115=115;
    public static final int EOF=-1;
    public static final int T__74=74;
    public static final int T__114=114;
    public static final int T__75=75;
    public static final int T__117=117;
    public static final int T__76=76;
    public static final int T__116=116;
    public static final int T__80=80;
    public static final int T__111=111;
    public static final int T__81=81;
    public static final int T__110=110;
    public static final int T__82=82;
    public static final int T__113=113;
    public static final int T__83=83;
    public static final int T__112=112;
    public static final int RULE_WS=17;
    public static final int RULE_ANY_OTHER=14;
    public static final int T__88=88;
    public static final int T__108=108;
    public static final int T__89=89;
    public static final int T__107=107;
    public static final int T__109=109;
    public static final int T__84=84;
    public static final int T__104=104;
    public static final int T__85=85;
    public static final int T__103=103;
    public static final int T__86=86;
    public static final int T__106=106;
    public static final int T__87=87;
    public static final int T__105=105;

    // delegates
    // delegators


        public InternalOWLModelParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalOWLModelParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalOWLModelParser.tokenNames; }
    public String getGrammarFileName() { return "InternalOWLModel.g"; }



     	private OWLModelGrammarAccess grammarAccess;
     	
        public InternalOWLModelParser(TokenStream input, OWLModelGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "OWLModel";	
       	}
       	
       	@Override
       	protected OWLModelGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleOWLModel"
    // InternalOWLModel.g:67:1: entryRuleOWLModel returns [EObject current=null] : iv_ruleOWLModel= ruleOWLModel EOF ;
    public final EObject entryRuleOWLModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOWLModel = null;


        try {
            // InternalOWLModel.g:68:2: (iv_ruleOWLModel= ruleOWLModel EOF )
            // InternalOWLModel.g:69:2: iv_ruleOWLModel= ruleOWLModel EOF
            {
             newCompositeNode(grammarAccess.getOWLModelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOWLModel=ruleOWLModel();

            state._fsp--;

             current =iv_ruleOWLModel; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOWLModel"


    // $ANTLR start "ruleOWLModel"
    // InternalOWLModel.g:76:1: ruleOWLModel returns [EObject current=null] : ( ( (lv_header_0_0= RULE_XMI_HEADER ) )? ( (lv_docType_1_0= ruleDocType ) )? ( (lv_rdf_2_0= ruleRDFElement ) ) ) ;
    public final EObject ruleOWLModel() throws RecognitionException {
        EObject current = null;

        Token lv_header_0_0=null;
        EObject lv_docType_1_0 = null;

        EObject lv_rdf_2_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:79:28: ( ( ( (lv_header_0_0= RULE_XMI_HEADER ) )? ( (lv_docType_1_0= ruleDocType ) )? ( (lv_rdf_2_0= ruleRDFElement ) ) ) )
            // InternalOWLModel.g:80:1: ( ( (lv_header_0_0= RULE_XMI_HEADER ) )? ( (lv_docType_1_0= ruleDocType ) )? ( (lv_rdf_2_0= ruleRDFElement ) ) )
            {
            // InternalOWLModel.g:80:1: ( ( (lv_header_0_0= RULE_XMI_HEADER ) )? ( (lv_docType_1_0= ruleDocType ) )? ( (lv_rdf_2_0= ruleRDFElement ) ) )
            // InternalOWLModel.g:80:2: ( (lv_header_0_0= RULE_XMI_HEADER ) )? ( (lv_docType_1_0= ruleDocType ) )? ( (lv_rdf_2_0= ruleRDFElement ) )
            {
            // InternalOWLModel.g:80:2: ( (lv_header_0_0= RULE_XMI_HEADER ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==RULE_XMI_HEADER) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalOWLModel.g:81:1: (lv_header_0_0= RULE_XMI_HEADER )
                    {
                    // InternalOWLModel.g:81:1: (lv_header_0_0= RULE_XMI_HEADER )
                    // InternalOWLModel.g:82:3: lv_header_0_0= RULE_XMI_HEADER
                    {
                    lv_header_0_0=(Token)match(input,RULE_XMI_HEADER,FOLLOW_3); 

                    			newLeafNode(lv_header_0_0, grammarAccess.getOWLModelAccess().getHeaderXMI_HEADERTerminalRuleCall_0_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getOWLModelRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"header",
                            		lv_header_0_0, 
                            		"org.smool.sdk.owlparser.OWLModel.XMI_HEADER");
                    	    

                    }


                    }
                    break;

            }

            // InternalOWLModel.g:98:3: ( (lv_docType_1_0= ruleDocType ) )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==18) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // InternalOWLModel.g:99:1: (lv_docType_1_0= ruleDocType )
                    {
                    // InternalOWLModel.g:99:1: (lv_docType_1_0= ruleDocType )
                    // InternalOWLModel.g:100:3: lv_docType_1_0= ruleDocType
                    {
                     
                    	        newCompositeNode(grammarAccess.getOWLModelAccess().getDocTypeDocTypeParserRuleCall_1_0()); 
                    	    
                    pushFollow(FOLLOW_3);
                    lv_docType_1_0=ruleDocType();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getOWLModelRule());
                    	        }
                           		set(
                           			current, 
                           			"docType",
                            		lv_docType_1_0, 
                            		"org.smool.sdk.owlparser.OWLModel.DocType");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }

            // InternalOWLModel.g:116:3: ( (lv_rdf_2_0= ruleRDFElement ) )
            // InternalOWLModel.g:117:1: (lv_rdf_2_0= ruleRDFElement )
            {
            // InternalOWLModel.g:117:1: (lv_rdf_2_0= ruleRDFElement )
            // InternalOWLModel.g:118:3: lv_rdf_2_0= ruleRDFElement
            {
             
            	        newCompositeNode(grammarAccess.getOWLModelAccess().getRdfRDFElementParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_2);
            lv_rdf_2_0=ruleRDFElement();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getOWLModelRule());
            	        }
                   		set(
                   			current, 
                   			"rdf",
                    		lv_rdf_2_0, 
                    		"org.smool.sdk.owlparser.OWLModel.RDFElement");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOWLModel"


    // $ANTLR start "entryRuleDocType"
    // InternalOWLModel.g:142:1: entryRuleDocType returns [EObject current=null] : iv_ruleDocType= ruleDocType EOF ;
    public final EObject entryRuleDocType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDocType = null;


        try {
            // InternalOWLModel.g:143:2: (iv_ruleDocType= ruleDocType EOF )
            // InternalOWLModel.g:144:2: iv_ruleDocType= ruleDocType EOF
            {
             newCompositeNode(grammarAccess.getDocTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDocType=ruleDocType();

            state._fsp--;

             current =iv_ruleDocType; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDocType"


    // $ANTLR start "ruleDocType"
    // InternalOWLModel.g:151:1: ruleDocType returns [EObject current=null] : ( () otherlv_1= '<!DOCTYPE' ( ( ( (lv_docID_2_1= RULE_NSID | lv_docID_2_2= RULE_ID ) ) ) otherlv_3= '[' ( (lv_entities_4_0= ruleDocEntity ) )* otherlv_5= ']' )? otherlv_6= '>' ) ;
    public final EObject ruleDocType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_docID_2_1=null;
        Token lv_docID_2_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        EObject lv_entities_4_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:154:28: ( ( () otherlv_1= '<!DOCTYPE' ( ( ( (lv_docID_2_1= RULE_NSID | lv_docID_2_2= RULE_ID ) ) ) otherlv_3= '[' ( (lv_entities_4_0= ruleDocEntity ) )* otherlv_5= ']' )? otherlv_6= '>' ) )
            // InternalOWLModel.g:155:1: ( () otherlv_1= '<!DOCTYPE' ( ( ( (lv_docID_2_1= RULE_NSID | lv_docID_2_2= RULE_ID ) ) ) otherlv_3= '[' ( (lv_entities_4_0= ruleDocEntity ) )* otherlv_5= ']' )? otherlv_6= '>' )
            {
            // InternalOWLModel.g:155:1: ( () otherlv_1= '<!DOCTYPE' ( ( ( (lv_docID_2_1= RULE_NSID | lv_docID_2_2= RULE_ID ) ) ) otherlv_3= '[' ( (lv_entities_4_0= ruleDocEntity ) )* otherlv_5= ']' )? otherlv_6= '>' )
            // InternalOWLModel.g:155:2: () otherlv_1= '<!DOCTYPE' ( ( ( (lv_docID_2_1= RULE_NSID | lv_docID_2_2= RULE_ID ) ) ) otherlv_3= '[' ( (lv_entities_4_0= ruleDocEntity ) )* otherlv_5= ']' )? otherlv_6= '>'
            {
            // InternalOWLModel.g:155:2: ()
            // InternalOWLModel.g:156:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getDocTypeAccess().getDocTypeAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,18,FOLLOW_4); 

                	newLeafNode(otherlv_1, grammarAccess.getDocTypeAccess().getDOCTYPEKeyword_1());
                
            // InternalOWLModel.g:165:1: ( ( ( (lv_docID_2_1= RULE_NSID | lv_docID_2_2= RULE_ID ) ) ) otherlv_3= '[' ( (lv_entities_4_0= ruleDocEntity ) )* otherlv_5= ']' )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( ((LA5_0>=RULE_NSID && LA5_0<=RULE_ID)) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalOWLModel.g:165:2: ( ( (lv_docID_2_1= RULE_NSID | lv_docID_2_2= RULE_ID ) ) ) otherlv_3= '[' ( (lv_entities_4_0= ruleDocEntity ) )* otherlv_5= ']'
                    {
                    // InternalOWLModel.g:165:2: ( ( (lv_docID_2_1= RULE_NSID | lv_docID_2_2= RULE_ID ) ) )
                    // InternalOWLModel.g:166:1: ( (lv_docID_2_1= RULE_NSID | lv_docID_2_2= RULE_ID ) )
                    {
                    // InternalOWLModel.g:166:1: ( (lv_docID_2_1= RULE_NSID | lv_docID_2_2= RULE_ID ) )
                    // InternalOWLModel.g:167:1: (lv_docID_2_1= RULE_NSID | lv_docID_2_2= RULE_ID )
                    {
                    // InternalOWLModel.g:167:1: (lv_docID_2_1= RULE_NSID | lv_docID_2_2= RULE_ID )
                    int alt3=2;
                    int LA3_0 = input.LA(1);

                    if ( (LA3_0==RULE_NSID) ) {
                        alt3=1;
                    }
                    else if ( (LA3_0==RULE_ID) ) {
                        alt3=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 3, 0, input);

                        throw nvae;
                    }
                    switch (alt3) {
                        case 1 :
                            // InternalOWLModel.g:168:3: lv_docID_2_1= RULE_NSID
                            {
                            lv_docID_2_1=(Token)match(input,RULE_NSID,FOLLOW_5); 

                            			newLeafNode(lv_docID_2_1, grammarAccess.getDocTypeAccess().getDocIDNSIDTerminalRuleCall_2_0_0_0()); 
                            		

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getDocTypeRule());
                            	        }
                                   		setWithLastConsumed(
                                   			current, 
                                   			"docID",
                                    		lv_docID_2_1, 
                                    		"org.smool.sdk.owlparser.OWLModel.NSID");
                            	    

                            }
                            break;
                        case 2 :
                            // InternalOWLModel.g:183:8: lv_docID_2_2= RULE_ID
                            {
                            lv_docID_2_2=(Token)match(input,RULE_ID,FOLLOW_5); 

                            			newLeafNode(lv_docID_2_2, grammarAccess.getDocTypeAccess().getDocIDIDTerminalRuleCall_2_0_0_1()); 
                            		

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getDocTypeRule());
                            	        }
                                   		setWithLastConsumed(
                                   			current, 
                                   			"docID",
                                    		lv_docID_2_2, 
                                    		"org.smool.sdk.owlparser.OWLModel.ID");
                            	    

                            }
                            break;

                    }


                    }


                    }

                    otherlv_3=(Token)match(input,19,FOLLOW_6); 

                        	newLeafNode(otherlv_3, grammarAccess.getDocTypeAccess().getLeftSquareBracketKeyword_2_1());
                        
                    // InternalOWLModel.g:205:1: ( (lv_entities_4_0= ruleDocEntity ) )*
                    loop4:
                    do {
                        int alt4=2;
                        int LA4_0 = input.LA(1);

                        if ( (LA4_0==22) ) {
                            alt4=1;
                        }


                        switch (alt4) {
                    	case 1 :
                    	    // InternalOWLModel.g:206:1: (lv_entities_4_0= ruleDocEntity )
                    	    {
                    	    // InternalOWLModel.g:206:1: (lv_entities_4_0= ruleDocEntity )
                    	    // InternalOWLModel.g:207:3: lv_entities_4_0= ruleDocEntity
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getDocTypeAccess().getEntitiesDocEntityParserRuleCall_2_2_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_6);
                    	    lv_entities_4_0=ruleDocEntity();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getDocTypeRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"entities",
                    	            		lv_entities_4_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.DocEntity");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop4;
                        }
                    } while (true);

                    otherlv_5=(Token)match(input,20,FOLLOW_7); 

                        	newLeafNode(otherlv_5, grammarAccess.getDocTypeAccess().getRightSquareBracketKeyword_2_3());
                        

                    }
                    break;

            }

            otherlv_6=(Token)match(input,21,FOLLOW_2); 

                	newLeafNode(otherlv_6, grammarAccess.getDocTypeAccess().getGreaterThanSignKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDocType"


    // $ANTLR start "entryRuleDocEntity"
    // InternalOWLModel.g:239:1: entryRuleDocEntity returns [EObject current=null] : iv_ruleDocEntity= ruleDocEntity EOF ;
    public final EObject entryRuleDocEntity() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDocEntity = null;


        try {
            // InternalOWLModel.g:240:2: (iv_ruleDocEntity= ruleDocEntity EOF )
            // InternalOWLModel.g:241:2: iv_ruleDocEntity= ruleDocEntity EOF
            {
             newCompositeNode(grammarAccess.getDocEntityRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDocEntity=ruleDocEntity();

            state._fsp--;

             current =iv_ruleDocEntity; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDocEntity"


    // $ANTLR start "ruleDocEntity"
    // InternalOWLModel.g:248:1: ruleDocEntity returns [EObject current=null] : (otherlv_0= '<!ENTITY' ( (lv_entityID_1_0= RULE_ID ) ) this_STR_DELIMITER_2= RULE_STR_DELIMITER ( (lv_entityURI_3_0= RULE_URI ) ) (this_HASHTAG_4= RULE_HASHTAG )? this_STR_DELIMITER_5= RULE_STR_DELIMITER otherlv_6= '>' ) ;
    public final EObject ruleDocEntity() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_entityID_1_0=null;
        Token this_STR_DELIMITER_2=null;
        Token lv_entityURI_3_0=null;
        Token this_HASHTAG_4=null;
        Token this_STR_DELIMITER_5=null;
        Token otherlv_6=null;

         enterRule(); 
            
        try {
            // InternalOWLModel.g:251:28: ( (otherlv_0= '<!ENTITY' ( (lv_entityID_1_0= RULE_ID ) ) this_STR_DELIMITER_2= RULE_STR_DELIMITER ( (lv_entityURI_3_0= RULE_URI ) ) (this_HASHTAG_4= RULE_HASHTAG )? this_STR_DELIMITER_5= RULE_STR_DELIMITER otherlv_6= '>' ) )
            // InternalOWLModel.g:252:1: (otherlv_0= '<!ENTITY' ( (lv_entityID_1_0= RULE_ID ) ) this_STR_DELIMITER_2= RULE_STR_DELIMITER ( (lv_entityURI_3_0= RULE_URI ) ) (this_HASHTAG_4= RULE_HASHTAG )? this_STR_DELIMITER_5= RULE_STR_DELIMITER otherlv_6= '>' )
            {
            // InternalOWLModel.g:252:1: (otherlv_0= '<!ENTITY' ( (lv_entityID_1_0= RULE_ID ) ) this_STR_DELIMITER_2= RULE_STR_DELIMITER ( (lv_entityURI_3_0= RULE_URI ) ) (this_HASHTAG_4= RULE_HASHTAG )? this_STR_DELIMITER_5= RULE_STR_DELIMITER otherlv_6= '>' )
            // InternalOWLModel.g:252:3: otherlv_0= '<!ENTITY' ( (lv_entityID_1_0= RULE_ID ) ) this_STR_DELIMITER_2= RULE_STR_DELIMITER ( (lv_entityURI_3_0= RULE_URI ) ) (this_HASHTAG_4= RULE_HASHTAG )? this_STR_DELIMITER_5= RULE_STR_DELIMITER otherlv_6= '>'
            {
            otherlv_0=(Token)match(input,22,FOLLOW_8); 

                	newLeafNode(otherlv_0, grammarAccess.getDocEntityAccess().getENTITYKeyword_0());
                
            // InternalOWLModel.g:256:1: ( (lv_entityID_1_0= RULE_ID ) )
            // InternalOWLModel.g:257:1: (lv_entityID_1_0= RULE_ID )
            {
            // InternalOWLModel.g:257:1: (lv_entityID_1_0= RULE_ID )
            // InternalOWLModel.g:258:3: lv_entityID_1_0= RULE_ID
            {
            lv_entityID_1_0=(Token)match(input,RULE_ID,FOLLOW_9); 

            			newLeafNode(lv_entityID_1_0, grammarAccess.getDocEntityAccess().getEntityIDIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getDocEntityRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"entityID",
                    		lv_entityID_1_0, 
                    		"org.smool.sdk.owlparser.OWLModel.ID");
            	    

            }


            }

            this_STR_DELIMITER_2=(Token)match(input,RULE_STR_DELIMITER,FOLLOW_10); 
             
                newLeafNode(this_STR_DELIMITER_2, grammarAccess.getDocEntityAccess().getSTR_DELIMITERTerminalRuleCall_2()); 
                
            // InternalOWLModel.g:278:1: ( (lv_entityURI_3_0= RULE_URI ) )
            // InternalOWLModel.g:279:1: (lv_entityURI_3_0= RULE_URI )
            {
            // InternalOWLModel.g:279:1: (lv_entityURI_3_0= RULE_URI )
            // InternalOWLModel.g:280:3: lv_entityURI_3_0= RULE_URI
            {
            lv_entityURI_3_0=(Token)match(input,RULE_URI,FOLLOW_11); 

            			newLeafNode(lv_entityURI_3_0, grammarAccess.getDocEntityAccess().getEntityURIURITerminalRuleCall_3_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getDocEntityRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"entityURI",
                    		lv_entityURI_3_0, 
                    		"org.smool.sdk.owlparser.OWLModel.URI");
            	    

            }


            }

            // InternalOWLModel.g:296:2: (this_HASHTAG_4= RULE_HASHTAG )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==RULE_HASHTAG) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalOWLModel.g:296:3: this_HASHTAG_4= RULE_HASHTAG
                    {
                    this_HASHTAG_4=(Token)match(input,RULE_HASHTAG,FOLLOW_9); 
                     
                        newLeafNode(this_HASHTAG_4, grammarAccess.getDocEntityAccess().getHASHTAGTerminalRuleCall_4()); 
                        

                    }
                    break;

            }

            this_STR_DELIMITER_5=(Token)match(input,RULE_STR_DELIMITER,FOLLOW_7); 
             
                newLeafNode(this_STR_DELIMITER_5, grammarAccess.getDocEntityAccess().getSTR_DELIMITERTerminalRuleCall_5()); 
                
            otherlv_6=(Token)match(input,21,FOLLOW_2); 

                	newLeafNode(otherlv_6, grammarAccess.getDocEntityAccess().getGreaterThanSignKeyword_6());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDocEntity"


    // $ANTLR start "entryRuleRDFElement"
    // InternalOWLModel.g:316:1: entryRuleRDFElement returns [EObject current=null] : iv_ruleRDFElement= ruleRDFElement EOF ;
    public final EObject entryRuleRDFElement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRDFElement = null;


        try {
            // InternalOWLModel.g:317:2: (iv_ruleRDFElement= ruleRDFElement EOF )
            // InternalOWLModel.g:318:2: iv_ruleRDFElement= ruleRDFElement EOF
            {
             newCompositeNode(grammarAccess.getRDFElementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRDFElement=ruleRDFElement();

            state._fsp--;

             current =iv_ruleRDFElement; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRDFElement"


    // $ANTLR start "ruleRDFElement"
    // InternalOWLModel.g:325:1: ruleRDFElement returns [EObject current=null] : (otherlv_0= '<rdf:RDF' ( (lv_importedNs_1_0= ruleXmlNs ) )* (otherlv_2= 'xml:base' otherlv_3= '=' this_STR_DELIMITER_4= RULE_STR_DELIMITER ( ( ( (lv_baseOnt_5_0= RULE_URI ) ) (this_HASHTAG_6= RULE_HASHTAG )? ) | (this_AMPERSAND_7= RULE_AMPERSAND ( (lv_baseOnt_8_0= RULE_ID ) ) this_SEMICOLON_9= RULE_SEMICOLON ) ) this_STR_DELIMITER_10= RULE_STR_DELIMITER ( (lv_importedNs_11_0= ruleXmlNs ) )* )? otherlv_12= '>' ( (lv_ontologyElements_13_0= ruleOntologyElement ) )+ otherlv_14= '</rdf:RDF' otherlv_15= '>' ) ;
    public final EObject ruleRDFElement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token this_STR_DELIMITER_4=null;
        Token lv_baseOnt_5_0=null;
        Token this_HASHTAG_6=null;
        Token this_AMPERSAND_7=null;
        Token lv_baseOnt_8_0=null;
        Token this_SEMICOLON_9=null;
        Token this_STR_DELIMITER_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        EObject lv_importedNs_1_0 = null;

        EObject lv_importedNs_11_0 = null;

        EObject lv_ontologyElements_13_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:328:28: ( (otherlv_0= '<rdf:RDF' ( (lv_importedNs_1_0= ruleXmlNs ) )* (otherlv_2= 'xml:base' otherlv_3= '=' this_STR_DELIMITER_4= RULE_STR_DELIMITER ( ( ( (lv_baseOnt_5_0= RULE_URI ) ) (this_HASHTAG_6= RULE_HASHTAG )? ) | (this_AMPERSAND_7= RULE_AMPERSAND ( (lv_baseOnt_8_0= RULE_ID ) ) this_SEMICOLON_9= RULE_SEMICOLON ) ) this_STR_DELIMITER_10= RULE_STR_DELIMITER ( (lv_importedNs_11_0= ruleXmlNs ) )* )? otherlv_12= '>' ( (lv_ontologyElements_13_0= ruleOntologyElement ) )+ otherlv_14= '</rdf:RDF' otherlv_15= '>' ) )
            // InternalOWLModel.g:329:1: (otherlv_0= '<rdf:RDF' ( (lv_importedNs_1_0= ruleXmlNs ) )* (otherlv_2= 'xml:base' otherlv_3= '=' this_STR_DELIMITER_4= RULE_STR_DELIMITER ( ( ( (lv_baseOnt_5_0= RULE_URI ) ) (this_HASHTAG_6= RULE_HASHTAG )? ) | (this_AMPERSAND_7= RULE_AMPERSAND ( (lv_baseOnt_8_0= RULE_ID ) ) this_SEMICOLON_9= RULE_SEMICOLON ) ) this_STR_DELIMITER_10= RULE_STR_DELIMITER ( (lv_importedNs_11_0= ruleXmlNs ) )* )? otherlv_12= '>' ( (lv_ontologyElements_13_0= ruleOntologyElement ) )+ otherlv_14= '</rdf:RDF' otherlv_15= '>' )
            {
            // InternalOWLModel.g:329:1: (otherlv_0= '<rdf:RDF' ( (lv_importedNs_1_0= ruleXmlNs ) )* (otherlv_2= 'xml:base' otherlv_3= '=' this_STR_DELIMITER_4= RULE_STR_DELIMITER ( ( ( (lv_baseOnt_5_0= RULE_URI ) ) (this_HASHTAG_6= RULE_HASHTAG )? ) | (this_AMPERSAND_7= RULE_AMPERSAND ( (lv_baseOnt_8_0= RULE_ID ) ) this_SEMICOLON_9= RULE_SEMICOLON ) ) this_STR_DELIMITER_10= RULE_STR_DELIMITER ( (lv_importedNs_11_0= ruleXmlNs ) )* )? otherlv_12= '>' ( (lv_ontologyElements_13_0= ruleOntologyElement ) )+ otherlv_14= '</rdf:RDF' otherlv_15= '>' )
            // InternalOWLModel.g:329:3: otherlv_0= '<rdf:RDF' ( (lv_importedNs_1_0= ruleXmlNs ) )* (otherlv_2= 'xml:base' otherlv_3= '=' this_STR_DELIMITER_4= RULE_STR_DELIMITER ( ( ( (lv_baseOnt_5_0= RULE_URI ) ) (this_HASHTAG_6= RULE_HASHTAG )? ) | (this_AMPERSAND_7= RULE_AMPERSAND ( (lv_baseOnt_8_0= RULE_ID ) ) this_SEMICOLON_9= RULE_SEMICOLON ) ) this_STR_DELIMITER_10= RULE_STR_DELIMITER ( (lv_importedNs_11_0= ruleXmlNs ) )* )? otherlv_12= '>' ( (lv_ontologyElements_13_0= ruleOntologyElement ) )+ otherlv_14= '</rdf:RDF' otherlv_15= '>'
            {
            otherlv_0=(Token)match(input,23,FOLLOW_12); 

                	newLeafNode(otherlv_0, grammarAccess.getRDFElementAccess().getRdfRDFKeyword_0());
                
            // InternalOWLModel.g:333:1: ( (lv_importedNs_1_0= ruleXmlNs ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==RULE_NSNAME) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalOWLModel.g:334:1: (lv_importedNs_1_0= ruleXmlNs )
            	    {
            	    // InternalOWLModel.g:334:1: (lv_importedNs_1_0= ruleXmlNs )
            	    // InternalOWLModel.g:335:3: lv_importedNs_1_0= ruleXmlNs
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getRDFElementAccess().getImportedNsXmlNsParserRuleCall_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_12);
            	    lv_importedNs_1_0=ruleXmlNs();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getRDFElementRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"importedNs",
            	            		lv_importedNs_1_0, 
            	            		"org.smool.sdk.owlparser.OWLModel.XmlNs");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

            // InternalOWLModel.g:351:3: (otherlv_2= 'xml:base' otherlv_3= '=' this_STR_DELIMITER_4= RULE_STR_DELIMITER ( ( ( (lv_baseOnt_5_0= RULE_URI ) ) (this_HASHTAG_6= RULE_HASHTAG )? ) | (this_AMPERSAND_7= RULE_AMPERSAND ( (lv_baseOnt_8_0= RULE_ID ) ) this_SEMICOLON_9= RULE_SEMICOLON ) ) this_STR_DELIMITER_10= RULE_STR_DELIMITER ( (lv_importedNs_11_0= ruleXmlNs ) )* )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==24) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalOWLModel.g:351:5: otherlv_2= 'xml:base' otherlv_3= '=' this_STR_DELIMITER_4= RULE_STR_DELIMITER ( ( ( (lv_baseOnt_5_0= RULE_URI ) ) (this_HASHTAG_6= RULE_HASHTAG )? ) | (this_AMPERSAND_7= RULE_AMPERSAND ( (lv_baseOnt_8_0= RULE_ID ) ) this_SEMICOLON_9= RULE_SEMICOLON ) ) this_STR_DELIMITER_10= RULE_STR_DELIMITER ( (lv_importedNs_11_0= ruleXmlNs ) )*
                    {
                    otherlv_2=(Token)match(input,24,FOLLOW_13); 

                        	newLeafNode(otherlv_2, grammarAccess.getRDFElementAccess().getXmlBaseKeyword_2_0());
                        
                    otherlv_3=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_3, grammarAccess.getRDFElementAccess().getEqualsSignKeyword_2_1());
                        
                    this_STR_DELIMITER_4=(Token)match(input,RULE_STR_DELIMITER,FOLLOW_14); 
                     
                        newLeafNode(this_STR_DELIMITER_4, grammarAccess.getRDFElementAccess().getSTR_DELIMITERTerminalRuleCall_2_2()); 
                        
                    // InternalOWLModel.g:363:1: ( ( ( (lv_baseOnt_5_0= RULE_URI ) ) (this_HASHTAG_6= RULE_HASHTAG )? ) | (this_AMPERSAND_7= RULE_AMPERSAND ( (lv_baseOnt_8_0= RULE_ID ) ) this_SEMICOLON_9= RULE_SEMICOLON ) )
                    int alt9=2;
                    int LA9_0 = input.LA(1);

                    if ( (LA9_0==RULE_URI) ) {
                        alt9=1;
                    }
                    else if ( (LA9_0==RULE_AMPERSAND) ) {
                        alt9=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 9, 0, input);

                        throw nvae;
                    }
                    switch (alt9) {
                        case 1 :
                            // InternalOWLModel.g:363:2: ( ( (lv_baseOnt_5_0= RULE_URI ) ) (this_HASHTAG_6= RULE_HASHTAG )? )
                            {
                            // InternalOWLModel.g:363:2: ( ( (lv_baseOnt_5_0= RULE_URI ) ) (this_HASHTAG_6= RULE_HASHTAG )? )
                            // InternalOWLModel.g:363:3: ( (lv_baseOnt_5_0= RULE_URI ) ) (this_HASHTAG_6= RULE_HASHTAG )?
                            {
                            // InternalOWLModel.g:363:3: ( (lv_baseOnt_5_0= RULE_URI ) )
                            // InternalOWLModel.g:364:1: (lv_baseOnt_5_0= RULE_URI )
                            {
                            // InternalOWLModel.g:364:1: (lv_baseOnt_5_0= RULE_URI )
                            // InternalOWLModel.g:365:3: lv_baseOnt_5_0= RULE_URI
                            {
                            lv_baseOnt_5_0=(Token)match(input,RULE_URI,FOLLOW_11); 

                            			newLeafNode(lv_baseOnt_5_0, grammarAccess.getRDFElementAccess().getBaseOntURITerminalRuleCall_2_3_0_0_0()); 
                            		

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getRDFElementRule());
                            	        }
                                   		setWithLastConsumed(
                                   			current, 
                                   			"baseOnt",
                                    		lv_baseOnt_5_0, 
                                    		"org.smool.sdk.owlparser.OWLModel.URI");
                            	    

                            }


                            }

                            // InternalOWLModel.g:381:2: (this_HASHTAG_6= RULE_HASHTAG )?
                            int alt8=2;
                            int LA8_0 = input.LA(1);

                            if ( (LA8_0==RULE_HASHTAG) ) {
                                alt8=1;
                            }
                            switch (alt8) {
                                case 1 :
                                    // InternalOWLModel.g:381:3: this_HASHTAG_6= RULE_HASHTAG
                                    {
                                    this_HASHTAG_6=(Token)match(input,RULE_HASHTAG,FOLLOW_9); 
                                     
                                        newLeafNode(this_HASHTAG_6, grammarAccess.getRDFElementAccess().getHASHTAGTerminalRuleCall_2_3_0_1()); 
                                        

                                    }
                                    break;

                            }


                            }


                            }
                            break;
                        case 2 :
                            // InternalOWLModel.g:386:6: (this_AMPERSAND_7= RULE_AMPERSAND ( (lv_baseOnt_8_0= RULE_ID ) ) this_SEMICOLON_9= RULE_SEMICOLON )
                            {
                            // InternalOWLModel.g:386:6: (this_AMPERSAND_7= RULE_AMPERSAND ( (lv_baseOnt_8_0= RULE_ID ) ) this_SEMICOLON_9= RULE_SEMICOLON )
                            // InternalOWLModel.g:386:7: this_AMPERSAND_7= RULE_AMPERSAND ( (lv_baseOnt_8_0= RULE_ID ) ) this_SEMICOLON_9= RULE_SEMICOLON
                            {
                            this_AMPERSAND_7=(Token)match(input,RULE_AMPERSAND,FOLLOW_8); 
                             
                                newLeafNode(this_AMPERSAND_7, grammarAccess.getRDFElementAccess().getAMPERSANDTerminalRuleCall_2_3_1_0()); 
                                
                            // InternalOWLModel.g:390:1: ( (lv_baseOnt_8_0= RULE_ID ) )
                            // InternalOWLModel.g:391:1: (lv_baseOnt_8_0= RULE_ID )
                            {
                            // InternalOWLModel.g:391:1: (lv_baseOnt_8_0= RULE_ID )
                            // InternalOWLModel.g:392:3: lv_baseOnt_8_0= RULE_ID
                            {
                            lv_baseOnt_8_0=(Token)match(input,RULE_ID,FOLLOW_15); 

                            			newLeafNode(lv_baseOnt_8_0, grammarAccess.getRDFElementAccess().getBaseOntIDTerminalRuleCall_2_3_1_1_0()); 
                            		

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getRDFElementRule());
                            	        }
                                   		setWithLastConsumed(
                                   			current, 
                                   			"baseOnt",
                                    		lv_baseOnt_8_0, 
                                    		"org.smool.sdk.owlparser.OWLModel.ID");
                            	    

                            }


                            }

                            this_SEMICOLON_9=(Token)match(input,RULE_SEMICOLON,FOLLOW_9); 
                             
                                newLeafNode(this_SEMICOLON_9, grammarAccess.getRDFElementAccess().getSEMICOLONTerminalRuleCall_2_3_1_2()); 
                                

                            }


                            }
                            break;

                    }

                    this_STR_DELIMITER_10=(Token)match(input,RULE_STR_DELIMITER,FOLLOW_16); 
                     
                        newLeafNode(this_STR_DELIMITER_10, grammarAccess.getRDFElementAccess().getSTR_DELIMITERTerminalRuleCall_2_4()); 
                        
                    // InternalOWLModel.g:416:1: ( (lv_importedNs_11_0= ruleXmlNs ) )*
                    loop10:
                    do {
                        int alt10=2;
                        int LA10_0 = input.LA(1);

                        if ( (LA10_0==RULE_NSNAME) ) {
                            alt10=1;
                        }


                        switch (alt10) {
                    	case 1 :
                    	    // InternalOWLModel.g:417:1: (lv_importedNs_11_0= ruleXmlNs )
                    	    {
                    	    // InternalOWLModel.g:417:1: (lv_importedNs_11_0= ruleXmlNs )
                    	    // InternalOWLModel.g:418:3: lv_importedNs_11_0= ruleXmlNs
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getRDFElementAccess().getImportedNsXmlNsParserRuleCall_2_5_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_16);
                    	    lv_importedNs_11_0=ruleXmlNs();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getRDFElementRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"importedNs",
                    	            		lv_importedNs_11_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.XmlNs");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop10;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_12=(Token)match(input,21,FOLLOW_17); 

                	newLeafNode(otherlv_12, grammarAccess.getRDFElementAccess().getGreaterThanSignKeyword_3());
                
            // InternalOWLModel.g:438:1: ( (lv_ontologyElements_13_0= ruleOntologyElement ) )+
            int cnt12=0;
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==27||LA12_0==31||LA12_0==39||LA12_0==58||LA12_0==94||LA12_0==96||LA12_0==98||LA12_0==100||LA12_0==102||LA12_0==104||LA12_0==106||LA12_0==122||LA12_0==130) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalOWLModel.g:439:1: (lv_ontologyElements_13_0= ruleOntologyElement )
            	    {
            	    // InternalOWLModel.g:439:1: (lv_ontologyElements_13_0= ruleOntologyElement )
            	    // InternalOWLModel.g:440:3: lv_ontologyElements_13_0= ruleOntologyElement
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getRDFElementAccess().getOntologyElementsOntologyElementParserRuleCall_4_0()); 
            	    	    
            	    pushFollow(FOLLOW_18);
            	    lv_ontologyElements_13_0=ruleOntologyElement();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getRDFElementRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"ontologyElements",
            	            		lv_ontologyElements_13_0, 
            	            		"org.smool.sdk.owlparser.OWLModel.OntologyElement");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt12 >= 1 ) break loop12;
                        EarlyExitException eee =
                            new EarlyExitException(12, input);
                        throw eee;
                }
                cnt12++;
            } while (true);

            otherlv_14=(Token)match(input,26,FOLLOW_7); 

                	newLeafNode(otherlv_14, grammarAccess.getRDFElementAccess().getRdfRDFKeyword_5());
                
            otherlv_15=(Token)match(input,21,FOLLOW_2); 

                	newLeafNode(otherlv_15, grammarAccess.getRDFElementAccess().getGreaterThanSignKeyword_6());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRDFElement"


    // $ANTLR start "entryRuleXmlNs"
    // InternalOWLModel.g:472:1: entryRuleXmlNs returns [EObject current=null] : iv_ruleXmlNs= ruleXmlNs EOF ;
    public final EObject entryRuleXmlNs() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXmlNs = null;


        try {
            // InternalOWLModel.g:473:2: (iv_ruleXmlNs= ruleXmlNs EOF )
            // InternalOWLModel.g:474:2: iv_ruleXmlNs= ruleXmlNs EOF
            {
             newCompositeNode(grammarAccess.getXmlNsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleXmlNs=ruleXmlNs();

            state._fsp--;

             current =iv_ruleXmlNs; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXmlNs"


    // $ANTLR start "ruleXmlNs"
    // InternalOWLModel.g:481:1: ruleXmlNs returns [EObject current=null] : ( ( (lv_nsid_0_0= RULE_NSNAME ) ) otherlv_1= '=' this_STR_DELIMITER_2= RULE_STR_DELIMITER ( ( ( (lv_nsuri_3_0= RULE_URI ) ) (this_HASHTAG_4= RULE_HASHTAG )? ) | (this_AMPERSAND_5= RULE_AMPERSAND ( (lv_nsname_6_0= RULE_ID ) ) this_SEMICOLON_7= RULE_SEMICOLON ) ) this_STR_DELIMITER_8= RULE_STR_DELIMITER ) ;
    public final EObject ruleXmlNs() throws RecognitionException {
        EObject current = null;

        Token lv_nsid_0_0=null;
        Token otherlv_1=null;
        Token this_STR_DELIMITER_2=null;
        Token lv_nsuri_3_0=null;
        Token this_HASHTAG_4=null;
        Token this_AMPERSAND_5=null;
        Token lv_nsname_6_0=null;
        Token this_SEMICOLON_7=null;
        Token this_STR_DELIMITER_8=null;

         enterRule(); 
            
        try {
            // InternalOWLModel.g:484:28: ( ( ( (lv_nsid_0_0= RULE_NSNAME ) ) otherlv_1= '=' this_STR_DELIMITER_2= RULE_STR_DELIMITER ( ( ( (lv_nsuri_3_0= RULE_URI ) ) (this_HASHTAG_4= RULE_HASHTAG )? ) | (this_AMPERSAND_5= RULE_AMPERSAND ( (lv_nsname_6_0= RULE_ID ) ) this_SEMICOLON_7= RULE_SEMICOLON ) ) this_STR_DELIMITER_8= RULE_STR_DELIMITER ) )
            // InternalOWLModel.g:485:1: ( ( (lv_nsid_0_0= RULE_NSNAME ) ) otherlv_1= '=' this_STR_DELIMITER_2= RULE_STR_DELIMITER ( ( ( (lv_nsuri_3_0= RULE_URI ) ) (this_HASHTAG_4= RULE_HASHTAG )? ) | (this_AMPERSAND_5= RULE_AMPERSAND ( (lv_nsname_6_0= RULE_ID ) ) this_SEMICOLON_7= RULE_SEMICOLON ) ) this_STR_DELIMITER_8= RULE_STR_DELIMITER )
            {
            // InternalOWLModel.g:485:1: ( ( (lv_nsid_0_0= RULE_NSNAME ) ) otherlv_1= '=' this_STR_DELIMITER_2= RULE_STR_DELIMITER ( ( ( (lv_nsuri_3_0= RULE_URI ) ) (this_HASHTAG_4= RULE_HASHTAG )? ) | (this_AMPERSAND_5= RULE_AMPERSAND ( (lv_nsname_6_0= RULE_ID ) ) this_SEMICOLON_7= RULE_SEMICOLON ) ) this_STR_DELIMITER_8= RULE_STR_DELIMITER )
            // InternalOWLModel.g:485:2: ( (lv_nsid_0_0= RULE_NSNAME ) ) otherlv_1= '=' this_STR_DELIMITER_2= RULE_STR_DELIMITER ( ( ( (lv_nsuri_3_0= RULE_URI ) ) (this_HASHTAG_4= RULE_HASHTAG )? ) | (this_AMPERSAND_5= RULE_AMPERSAND ( (lv_nsname_6_0= RULE_ID ) ) this_SEMICOLON_7= RULE_SEMICOLON ) ) this_STR_DELIMITER_8= RULE_STR_DELIMITER
            {
            // InternalOWLModel.g:485:2: ( (lv_nsid_0_0= RULE_NSNAME ) )
            // InternalOWLModel.g:486:1: (lv_nsid_0_0= RULE_NSNAME )
            {
            // InternalOWLModel.g:486:1: (lv_nsid_0_0= RULE_NSNAME )
            // InternalOWLModel.g:487:3: lv_nsid_0_0= RULE_NSNAME
            {
            lv_nsid_0_0=(Token)match(input,RULE_NSNAME,FOLLOW_13); 

            			newLeafNode(lv_nsid_0_0, grammarAccess.getXmlNsAccess().getNsidNSNAMETerminalRuleCall_0_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getXmlNsRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"nsid",
                    		lv_nsid_0_0, 
                    		"org.smool.sdk.owlparser.OWLModel.NSNAME");
            	    

            }


            }

            otherlv_1=(Token)match(input,25,FOLLOW_9); 

                	newLeafNode(otherlv_1, grammarAccess.getXmlNsAccess().getEqualsSignKeyword_1());
                
            this_STR_DELIMITER_2=(Token)match(input,RULE_STR_DELIMITER,FOLLOW_14); 
             
                newLeafNode(this_STR_DELIMITER_2, grammarAccess.getXmlNsAccess().getSTR_DELIMITERTerminalRuleCall_2()); 
                
            // InternalOWLModel.g:511:1: ( ( ( (lv_nsuri_3_0= RULE_URI ) ) (this_HASHTAG_4= RULE_HASHTAG )? ) | (this_AMPERSAND_5= RULE_AMPERSAND ( (lv_nsname_6_0= RULE_ID ) ) this_SEMICOLON_7= RULE_SEMICOLON ) )
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==RULE_URI) ) {
                alt14=1;
            }
            else if ( (LA14_0==RULE_AMPERSAND) ) {
                alt14=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }
            switch (alt14) {
                case 1 :
                    // InternalOWLModel.g:511:2: ( ( (lv_nsuri_3_0= RULE_URI ) ) (this_HASHTAG_4= RULE_HASHTAG )? )
                    {
                    // InternalOWLModel.g:511:2: ( ( (lv_nsuri_3_0= RULE_URI ) ) (this_HASHTAG_4= RULE_HASHTAG )? )
                    // InternalOWLModel.g:511:3: ( (lv_nsuri_3_0= RULE_URI ) ) (this_HASHTAG_4= RULE_HASHTAG )?
                    {
                    // InternalOWLModel.g:511:3: ( (lv_nsuri_3_0= RULE_URI ) )
                    // InternalOWLModel.g:512:1: (lv_nsuri_3_0= RULE_URI )
                    {
                    // InternalOWLModel.g:512:1: (lv_nsuri_3_0= RULE_URI )
                    // InternalOWLModel.g:513:3: lv_nsuri_3_0= RULE_URI
                    {
                    lv_nsuri_3_0=(Token)match(input,RULE_URI,FOLLOW_11); 

                    			newLeafNode(lv_nsuri_3_0, grammarAccess.getXmlNsAccess().getNsuriURITerminalRuleCall_3_0_0_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getXmlNsRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"nsuri",
                            		lv_nsuri_3_0, 
                            		"org.smool.sdk.owlparser.OWLModel.URI");
                    	    

                    }


                    }

                    // InternalOWLModel.g:529:2: (this_HASHTAG_4= RULE_HASHTAG )?
                    int alt13=2;
                    int LA13_0 = input.LA(1);

                    if ( (LA13_0==RULE_HASHTAG) ) {
                        alt13=1;
                    }
                    switch (alt13) {
                        case 1 :
                            // InternalOWLModel.g:529:3: this_HASHTAG_4= RULE_HASHTAG
                            {
                            this_HASHTAG_4=(Token)match(input,RULE_HASHTAG,FOLLOW_9); 
                             
                                newLeafNode(this_HASHTAG_4, grammarAccess.getXmlNsAccess().getHASHTAGTerminalRuleCall_3_0_1()); 
                                

                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:534:6: (this_AMPERSAND_5= RULE_AMPERSAND ( (lv_nsname_6_0= RULE_ID ) ) this_SEMICOLON_7= RULE_SEMICOLON )
                    {
                    // InternalOWLModel.g:534:6: (this_AMPERSAND_5= RULE_AMPERSAND ( (lv_nsname_6_0= RULE_ID ) ) this_SEMICOLON_7= RULE_SEMICOLON )
                    // InternalOWLModel.g:534:7: this_AMPERSAND_5= RULE_AMPERSAND ( (lv_nsname_6_0= RULE_ID ) ) this_SEMICOLON_7= RULE_SEMICOLON
                    {
                    this_AMPERSAND_5=(Token)match(input,RULE_AMPERSAND,FOLLOW_8); 
                     
                        newLeafNode(this_AMPERSAND_5, grammarAccess.getXmlNsAccess().getAMPERSANDTerminalRuleCall_3_1_0()); 
                        
                    // InternalOWLModel.g:538:1: ( (lv_nsname_6_0= RULE_ID ) )
                    // InternalOWLModel.g:539:1: (lv_nsname_6_0= RULE_ID )
                    {
                    // InternalOWLModel.g:539:1: (lv_nsname_6_0= RULE_ID )
                    // InternalOWLModel.g:540:3: lv_nsname_6_0= RULE_ID
                    {
                    lv_nsname_6_0=(Token)match(input,RULE_ID,FOLLOW_15); 

                    			newLeafNode(lv_nsname_6_0, grammarAccess.getXmlNsAccess().getNsnameIDTerminalRuleCall_3_1_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getXmlNsRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"nsname",
                            		lv_nsname_6_0, 
                            		"org.smool.sdk.owlparser.OWLModel.ID");
                    	    

                    }


                    }

                    this_SEMICOLON_7=(Token)match(input,RULE_SEMICOLON,FOLLOW_9); 
                     
                        newLeafNode(this_SEMICOLON_7, grammarAccess.getXmlNsAccess().getSEMICOLONTerminalRuleCall_3_1_2()); 
                        

                    }


                    }
                    break;

            }

            this_STR_DELIMITER_8=(Token)match(input,RULE_STR_DELIMITER,FOLLOW_2); 
             
                newLeafNode(this_STR_DELIMITER_8, grammarAccess.getXmlNsAccess().getSTR_DELIMITERTerminalRuleCall_4()); 
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXmlNs"


    // $ANTLR start "entryRuleOwlRef"
    // InternalOWLModel.g:572:1: entryRuleOwlRef returns [EObject current=null] : iv_ruleOwlRef= ruleOwlRef EOF ;
    public final EObject entryRuleOwlRef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOwlRef = null;


        try {
            // InternalOWLModel.g:573:2: (iv_ruleOwlRef= ruleOwlRef EOF )
            // InternalOWLModel.g:574:2: iv_ruleOwlRef= ruleOwlRef EOF
            {
             newCompositeNode(grammarAccess.getOwlRefRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOwlRef=ruleOwlRef();

            state._fsp--;

             current =iv_ruleOwlRef; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOwlRef"


    // $ANTLR start "ruleOwlRef"
    // InternalOWLModel.g:581:1: ruleOwlRef returns [EObject current=null] : (this_STR_DELIMITER_0= RULE_STR_DELIMITER ( ( ( ( ( (lv_ontURI_1_0= RULE_URI ) )? this_HASHTAG_2= RULE_HASHTAG ) | (this_AMPERSAND_3= RULE_AMPERSAND ( (lv_ontName_4_0= RULE_ID ) ) this_SEMICOLON_5= RULE_SEMICOLON ) ) ( (lv_elemID_6_0= RULE_ID ) ) ) | ( (lv_fullURI_7_0= RULE_URI ) ) ) this_STR_DELIMITER_8= RULE_STR_DELIMITER ) ;
    public final EObject ruleOwlRef() throws RecognitionException {
        EObject current = null;

        Token this_STR_DELIMITER_0=null;
        Token lv_ontURI_1_0=null;
        Token this_HASHTAG_2=null;
        Token this_AMPERSAND_3=null;
        Token lv_ontName_4_0=null;
        Token this_SEMICOLON_5=null;
        Token lv_elemID_6_0=null;
        Token lv_fullURI_7_0=null;
        Token this_STR_DELIMITER_8=null;

         enterRule(); 
            
        try {
            // InternalOWLModel.g:584:28: ( (this_STR_DELIMITER_0= RULE_STR_DELIMITER ( ( ( ( ( (lv_ontURI_1_0= RULE_URI ) )? this_HASHTAG_2= RULE_HASHTAG ) | (this_AMPERSAND_3= RULE_AMPERSAND ( (lv_ontName_4_0= RULE_ID ) ) this_SEMICOLON_5= RULE_SEMICOLON ) ) ( (lv_elemID_6_0= RULE_ID ) ) ) | ( (lv_fullURI_7_0= RULE_URI ) ) ) this_STR_DELIMITER_8= RULE_STR_DELIMITER ) )
            // InternalOWLModel.g:585:1: (this_STR_DELIMITER_0= RULE_STR_DELIMITER ( ( ( ( ( (lv_ontURI_1_0= RULE_URI ) )? this_HASHTAG_2= RULE_HASHTAG ) | (this_AMPERSAND_3= RULE_AMPERSAND ( (lv_ontName_4_0= RULE_ID ) ) this_SEMICOLON_5= RULE_SEMICOLON ) ) ( (lv_elemID_6_0= RULE_ID ) ) ) | ( (lv_fullURI_7_0= RULE_URI ) ) ) this_STR_DELIMITER_8= RULE_STR_DELIMITER )
            {
            // InternalOWLModel.g:585:1: (this_STR_DELIMITER_0= RULE_STR_DELIMITER ( ( ( ( ( (lv_ontURI_1_0= RULE_URI ) )? this_HASHTAG_2= RULE_HASHTAG ) | (this_AMPERSAND_3= RULE_AMPERSAND ( (lv_ontName_4_0= RULE_ID ) ) this_SEMICOLON_5= RULE_SEMICOLON ) ) ( (lv_elemID_6_0= RULE_ID ) ) ) | ( (lv_fullURI_7_0= RULE_URI ) ) ) this_STR_DELIMITER_8= RULE_STR_DELIMITER )
            // InternalOWLModel.g:585:2: this_STR_DELIMITER_0= RULE_STR_DELIMITER ( ( ( ( ( (lv_ontURI_1_0= RULE_URI ) )? this_HASHTAG_2= RULE_HASHTAG ) | (this_AMPERSAND_3= RULE_AMPERSAND ( (lv_ontName_4_0= RULE_ID ) ) this_SEMICOLON_5= RULE_SEMICOLON ) ) ( (lv_elemID_6_0= RULE_ID ) ) ) | ( (lv_fullURI_7_0= RULE_URI ) ) ) this_STR_DELIMITER_8= RULE_STR_DELIMITER
            {
            this_STR_DELIMITER_0=(Token)match(input,RULE_STR_DELIMITER,FOLLOW_19); 
             
                newLeafNode(this_STR_DELIMITER_0, grammarAccess.getOwlRefAccess().getSTR_DELIMITERTerminalRuleCall_0()); 
                
            // InternalOWLModel.g:589:1: ( ( ( ( ( (lv_ontURI_1_0= RULE_URI ) )? this_HASHTAG_2= RULE_HASHTAG ) | (this_AMPERSAND_3= RULE_AMPERSAND ( (lv_ontName_4_0= RULE_ID ) ) this_SEMICOLON_5= RULE_SEMICOLON ) ) ( (lv_elemID_6_0= RULE_ID ) ) ) | ( (lv_fullURI_7_0= RULE_URI ) ) )
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==RULE_URI) ) {
                int LA17_1 = input.LA(2);

                if ( (LA17_1==RULE_STR_DELIMITER) ) {
                    alt17=2;
                }
                else if ( (LA17_1==RULE_HASHTAG) ) {
                    alt17=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 17, 1, input);

                    throw nvae;
                }
            }
            else if ( ((LA17_0>=RULE_HASHTAG && LA17_0<=RULE_AMPERSAND)) ) {
                alt17=1;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;
            }
            switch (alt17) {
                case 1 :
                    // InternalOWLModel.g:589:2: ( ( ( ( (lv_ontURI_1_0= RULE_URI ) )? this_HASHTAG_2= RULE_HASHTAG ) | (this_AMPERSAND_3= RULE_AMPERSAND ( (lv_ontName_4_0= RULE_ID ) ) this_SEMICOLON_5= RULE_SEMICOLON ) ) ( (lv_elemID_6_0= RULE_ID ) ) )
                    {
                    // InternalOWLModel.g:589:2: ( ( ( ( (lv_ontURI_1_0= RULE_URI ) )? this_HASHTAG_2= RULE_HASHTAG ) | (this_AMPERSAND_3= RULE_AMPERSAND ( (lv_ontName_4_0= RULE_ID ) ) this_SEMICOLON_5= RULE_SEMICOLON ) ) ( (lv_elemID_6_0= RULE_ID ) ) )
                    // InternalOWLModel.g:589:3: ( ( ( (lv_ontURI_1_0= RULE_URI ) )? this_HASHTAG_2= RULE_HASHTAG ) | (this_AMPERSAND_3= RULE_AMPERSAND ( (lv_ontName_4_0= RULE_ID ) ) this_SEMICOLON_5= RULE_SEMICOLON ) ) ( (lv_elemID_6_0= RULE_ID ) )
                    {
                    // InternalOWLModel.g:589:3: ( ( ( (lv_ontURI_1_0= RULE_URI ) )? this_HASHTAG_2= RULE_HASHTAG ) | (this_AMPERSAND_3= RULE_AMPERSAND ( (lv_ontName_4_0= RULE_ID ) ) this_SEMICOLON_5= RULE_SEMICOLON ) )
                    int alt16=2;
                    int LA16_0 = input.LA(1);

                    if ( ((LA16_0>=RULE_URI && LA16_0<=RULE_HASHTAG)) ) {
                        alt16=1;
                    }
                    else if ( (LA16_0==RULE_AMPERSAND) ) {
                        alt16=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 16, 0, input);

                        throw nvae;
                    }
                    switch (alt16) {
                        case 1 :
                            // InternalOWLModel.g:589:4: ( ( (lv_ontURI_1_0= RULE_URI ) )? this_HASHTAG_2= RULE_HASHTAG )
                            {
                            // InternalOWLModel.g:589:4: ( ( (lv_ontURI_1_0= RULE_URI ) )? this_HASHTAG_2= RULE_HASHTAG )
                            // InternalOWLModel.g:589:5: ( (lv_ontURI_1_0= RULE_URI ) )? this_HASHTAG_2= RULE_HASHTAG
                            {
                            // InternalOWLModel.g:589:5: ( (lv_ontURI_1_0= RULE_URI ) )?
                            int alt15=2;
                            int LA15_0 = input.LA(1);

                            if ( (LA15_0==RULE_URI) ) {
                                alt15=1;
                            }
                            switch (alt15) {
                                case 1 :
                                    // InternalOWLModel.g:590:1: (lv_ontURI_1_0= RULE_URI )
                                    {
                                    // InternalOWLModel.g:590:1: (lv_ontURI_1_0= RULE_URI )
                                    // InternalOWLModel.g:591:3: lv_ontURI_1_0= RULE_URI
                                    {
                                    lv_ontURI_1_0=(Token)match(input,RULE_URI,FOLLOW_20); 

                                    			newLeafNode(lv_ontURI_1_0, grammarAccess.getOwlRefAccess().getOntURIURITerminalRuleCall_1_0_0_0_0_0()); 
                                    		

                                    	        if (current==null) {
                                    	            current = createModelElement(grammarAccess.getOwlRefRule());
                                    	        }
                                           		setWithLastConsumed(
                                           			current, 
                                           			"ontURI",
                                            		lv_ontURI_1_0, 
                                            		"org.smool.sdk.owlparser.OWLModel.URI");
                                    	    

                                    }


                                    }
                                    break;

                            }

                            this_HASHTAG_2=(Token)match(input,RULE_HASHTAG,FOLLOW_8); 
                             
                                newLeafNode(this_HASHTAG_2, grammarAccess.getOwlRefAccess().getHASHTAGTerminalRuleCall_1_0_0_0_1()); 
                                

                            }


                            }
                            break;
                        case 2 :
                            // InternalOWLModel.g:612:6: (this_AMPERSAND_3= RULE_AMPERSAND ( (lv_ontName_4_0= RULE_ID ) ) this_SEMICOLON_5= RULE_SEMICOLON )
                            {
                            // InternalOWLModel.g:612:6: (this_AMPERSAND_3= RULE_AMPERSAND ( (lv_ontName_4_0= RULE_ID ) ) this_SEMICOLON_5= RULE_SEMICOLON )
                            // InternalOWLModel.g:612:7: this_AMPERSAND_3= RULE_AMPERSAND ( (lv_ontName_4_0= RULE_ID ) ) this_SEMICOLON_5= RULE_SEMICOLON
                            {
                            this_AMPERSAND_3=(Token)match(input,RULE_AMPERSAND,FOLLOW_8); 
                             
                                newLeafNode(this_AMPERSAND_3, grammarAccess.getOwlRefAccess().getAMPERSANDTerminalRuleCall_1_0_0_1_0()); 
                                
                            // InternalOWLModel.g:616:1: ( (lv_ontName_4_0= RULE_ID ) )
                            // InternalOWLModel.g:617:1: (lv_ontName_4_0= RULE_ID )
                            {
                            // InternalOWLModel.g:617:1: (lv_ontName_4_0= RULE_ID )
                            // InternalOWLModel.g:618:3: lv_ontName_4_0= RULE_ID
                            {
                            lv_ontName_4_0=(Token)match(input,RULE_ID,FOLLOW_15); 

                            			newLeafNode(lv_ontName_4_0, grammarAccess.getOwlRefAccess().getOntNameIDTerminalRuleCall_1_0_0_1_1_0()); 
                            		

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getOwlRefRule());
                            	        }
                                   		setWithLastConsumed(
                                   			current, 
                                   			"ontName",
                                    		lv_ontName_4_0, 
                                    		"org.smool.sdk.owlparser.OWLModel.ID");
                            	    

                            }


                            }

                            this_SEMICOLON_5=(Token)match(input,RULE_SEMICOLON,FOLLOW_8); 
                             
                                newLeafNode(this_SEMICOLON_5, grammarAccess.getOwlRefAccess().getSEMICOLONTerminalRuleCall_1_0_0_1_2()); 
                                

                            }


                            }
                            break;

                    }

                    // InternalOWLModel.g:638:3: ( (lv_elemID_6_0= RULE_ID ) )
                    // InternalOWLModel.g:639:1: (lv_elemID_6_0= RULE_ID )
                    {
                    // InternalOWLModel.g:639:1: (lv_elemID_6_0= RULE_ID )
                    // InternalOWLModel.g:640:3: lv_elemID_6_0= RULE_ID
                    {
                    lv_elemID_6_0=(Token)match(input,RULE_ID,FOLLOW_9); 

                    			newLeafNode(lv_elemID_6_0, grammarAccess.getOwlRefAccess().getElemIDIDTerminalRuleCall_1_0_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getOwlRefRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"elemID",
                            		lv_elemID_6_0, 
                            		"org.smool.sdk.owlparser.OWLModel.ID");
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:657:6: ( (lv_fullURI_7_0= RULE_URI ) )
                    {
                    // InternalOWLModel.g:657:6: ( (lv_fullURI_7_0= RULE_URI ) )
                    // InternalOWLModel.g:658:1: (lv_fullURI_7_0= RULE_URI )
                    {
                    // InternalOWLModel.g:658:1: (lv_fullURI_7_0= RULE_URI )
                    // InternalOWLModel.g:659:3: lv_fullURI_7_0= RULE_URI
                    {
                    lv_fullURI_7_0=(Token)match(input,RULE_URI,FOLLOW_9); 

                    			newLeafNode(lv_fullURI_7_0, grammarAccess.getOwlRefAccess().getFullURIURITerminalRuleCall_1_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getOwlRefRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"fullURI",
                            		lv_fullURI_7_0, 
                            		"org.smool.sdk.owlparser.OWLModel.URI");
                    	    

                    }


                    }


                    }
                    break;

            }

            this_STR_DELIMITER_8=(Token)match(input,RULE_STR_DELIMITER,FOLLOW_2); 
             
                newLeafNode(this_STR_DELIMITER_8, grammarAccess.getOwlRefAccess().getSTR_DELIMITERTerminalRuleCall_2()); 
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOwlRef"


    // $ANTLR start "entryRuleOwlID"
    // InternalOWLModel.g:687:1: entryRuleOwlID returns [EObject current=null] : iv_ruleOwlID= ruleOwlID EOF ;
    public final EObject entryRuleOwlID() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOwlID = null;


        try {
            // InternalOWLModel.g:688:2: (iv_ruleOwlID= ruleOwlID EOF )
            // InternalOWLModel.g:689:2: iv_ruleOwlID= ruleOwlID EOF
            {
             newCompositeNode(grammarAccess.getOwlIDRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOwlID=ruleOwlID();

            state._fsp--;

             current =iv_ruleOwlID; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOwlID"


    // $ANTLR start "ruleOwlID"
    // InternalOWLModel.g:696:1: ruleOwlID returns [EObject current=null] : ( () this_STR_DELIMITER_1= RULE_STR_DELIMITER ( (lv_elemID_2_0= RULE_ID ) )? this_STR_DELIMITER_3= RULE_STR_DELIMITER ) ;
    public final EObject ruleOwlID() throws RecognitionException {
        EObject current = null;

        Token this_STR_DELIMITER_1=null;
        Token lv_elemID_2_0=null;
        Token this_STR_DELIMITER_3=null;

         enterRule(); 
            
        try {
            // InternalOWLModel.g:699:28: ( ( () this_STR_DELIMITER_1= RULE_STR_DELIMITER ( (lv_elemID_2_0= RULE_ID ) )? this_STR_DELIMITER_3= RULE_STR_DELIMITER ) )
            // InternalOWLModel.g:700:1: ( () this_STR_DELIMITER_1= RULE_STR_DELIMITER ( (lv_elemID_2_0= RULE_ID ) )? this_STR_DELIMITER_3= RULE_STR_DELIMITER )
            {
            // InternalOWLModel.g:700:1: ( () this_STR_DELIMITER_1= RULE_STR_DELIMITER ( (lv_elemID_2_0= RULE_ID ) )? this_STR_DELIMITER_3= RULE_STR_DELIMITER )
            // InternalOWLModel.g:700:2: () this_STR_DELIMITER_1= RULE_STR_DELIMITER ( (lv_elemID_2_0= RULE_ID ) )? this_STR_DELIMITER_3= RULE_STR_DELIMITER
            {
            // InternalOWLModel.g:700:2: ()
            // InternalOWLModel.g:701:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getOwlIDAccess().getOwlIDAction_0(),
                        current);
                

            }

            this_STR_DELIMITER_1=(Token)match(input,RULE_STR_DELIMITER,FOLLOW_21); 
             
                newLeafNode(this_STR_DELIMITER_1, grammarAccess.getOwlIDAccess().getSTR_DELIMITERTerminalRuleCall_1()); 
                
            // InternalOWLModel.g:710:1: ( (lv_elemID_2_0= RULE_ID ) )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==RULE_ID) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalOWLModel.g:711:1: (lv_elemID_2_0= RULE_ID )
                    {
                    // InternalOWLModel.g:711:1: (lv_elemID_2_0= RULE_ID )
                    // InternalOWLModel.g:712:3: lv_elemID_2_0= RULE_ID
                    {
                    lv_elemID_2_0=(Token)match(input,RULE_ID,FOLLOW_9); 

                    			newLeafNode(lv_elemID_2_0, grammarAccess.getOwlIDAccess().getElemIDIDTerminalRuleCall_2_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getOwlIDRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"elemID",
                            		lv_elemID_2_0, 
                            		"org.smool.sdk.owlparser.OWLModel.ID");
                    	    

                    }


                    }
                    break;

            }

            this_STR_DELIMITER_3=(Token)match(input,RULE_STR_DELIMITER,FOLLOW_2); 
             
                newLeafNode(this_STR_DELIMITER_3, grammarAccess.getOwlIDAccess().getSTR_DELIMITERTerminalRuleCall_3()); 
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOwlID"


    // $ANTLR start "entryRuleOntologyID"
    // InternalOWLModel.g:740:1: entryRuleOntologyID returns [EObject current=null] : iv_ruleOntologyID= ruleOntologyID EOF ;
    public final EObject entryRuleOntologyID() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOntologyID = null;


        try {
            // InternalOWLModel.g:741:2: (iv_ruleOntologyID= ruleOntologyID EOF )
            // InternalOWLModel.g:742:2: iv_ruleOntologyID= ruleOntologyID EOF
            {
             newCompositeNode(grammarAccess.getOntologyIDRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOntologyID=ruleOntologyID();

            state._fsp--;

             current =iv_ruleOntologyID; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOntologyID"


    // $ANTLR start "ruleOntologyID"
    // InternalOWLModel.g:749:1: ruleOntologyID returns [EObject current=null] : ( () this_STR_DELIMITER_1= RULE_STR_DELIMITER ( ( (this_AMPERSAND_2= RULE_AMPERSAND ( (lv_superOnt_3_0= RULE_ID ) ) this_SEMICOLON_4= RULE_SEMICOLON )? ( (lv_ontURI_5_0= RULE_ID ) )? ) | ( (lv_ontURI_6_0= RULE_URI ) ) ) this_STR_DELIMITER_7= RULE_STR_DELIMITER ) ;
    public final EObject ruleOntologyID() throws RecognitionException {
        EObject current = null;

        Token this_STR_DELIMITER_1=null;
        Token this_AMPERSAND_2=null;
        Token lv_superOnt_3_0=null;
        Token this_SEMICOLON_4=null;
        Token lv_ontURI_5_0=null;
        Token lv_ontURI_6_0=null;
        Token this_STR_DELIMITER_7=null;

         enterRule(); 
            
        try {
            // InternalOWLModel.g:752:28: ( ( () this_STR_DELIMITER_1= RULE_STR_DELIMITER ( ( (this_AMPERSAND_2= RULE_AMPERSAND ( (lv_superOnt_3_0= RULE_ID ) ) this_SEMICOLON_4= RULE_SEMICOLON )? ( (lv_ontURI_5_0= RULE_ID ) )? ) | ( (lv_ontURI_6_0= RULE_URI ) ) ) this_STR_DELIMITER_7= RULE_STR_DELIMITER ) )
            // InternalOWLModel.g:753:1: ( () this_STR_DELIMITER_1= RULE_STR_DELIMITER ( ( (this_AMPERSAND_2= RULE_AMPERSAND ( (lv_superOnt_3_0= RULE_ID ) ) this_SEMICOLON_4= RULE_SEMICOLON )? ( (lv_ontURI_5_0= RULE_ID ) )? ) | ( (lv_ontURI_6_0= RULE_URI ) ) ) this_STR_DELIMITER_7= RULE_STR_DELIMITER )
            {
            // InternalOWLModel.g:753:1: ( () this_STR_DELIMITER_1= RULE_STR_DELIMITER ( ( (this_AMPERSAND_2= RULE_AMPERSAND ( (lv_superOnt_3_0= RULE_ID ) ) this_SEMICOLON_4= RULE_SEMICOLON )? ( (lv_ontURI_5_0= RULE_ID ) )? ) | ( (lv_ontURI_6_0= RULE_URI ) ) ) this_STR_DELIMITER_7= RULE_STR_DELIMITER )
            // InternalOWLModel.g:753:2: () this_STR_DELIMITER_1= RULE_STR_DELIMITER ( ( (this_AMPERSAND_2= RULE_AMPERSAND ( (lv_superOnt_3_0= RULE_ID ) ) this_SEMICOLON_4= RULE_SEMICOLON )? ( (lv_ontURI_5_0= RULE_ID ) )? ) | ( (lv_ontURI_6_0= RULE_URI ) ) ) this_STR_DELIMITER_7= RULE_STR_DELIMITER
            {
            // InternalOWLModel.g:753:2: ()
            // InternalOWLModel.g:754:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getOntologyIDAccess().getOntologyIDAction_0(),
                        current);
                

            }

            this_STR_DELIMITER_1=(Token)match(input,RULE_STR_DELIMITER,FOLLOW_22); 
             
                newLeafNode(this_STR_DELIMITER_1, grammarAccess.getOntologyIDAccess().getSTR_DELIMITERTerminalRuleCall_1()); 
                
            // InternalOWLModel.g:763:1: ( ( (this_AMPERSAND_2= RULE_AMPERSAND ( (lv_superOnt_3_0= RULE_ID ) ) this_SEMICOLON_4= RULE_SEMICOLON )? ( (lv_ontURI_5_0= RULE_ID ) )? ) | ( (lv_ontURI_6_0= RULE_URI ) ) )
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( ((LA21_0>=RULE_ID && LA21_0<=RULE_STR_DELIMITER)||LA21_0==RULE_AMPERSAND) ) {
                alt21=1;
            }
            else if ( (LA21_0==RULE_URI) ) {
                alt21=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 21, 0, input);

                throw nvae;
            }
            switch (alt21) {
                case 1 :
                    // InternalOWLModel.g:763:2: ( (this_AMPERSAND_2= RULE_AMPERSAND ( (lv_superOnt_3_0= RULE_ID ) ) this_SEMICOLON_4= RULE_SEMICOLON )? ( (lv_ontURI_5_0= RULE_ID ) )? )
                    {
                    // InternalOWLModel.g:763:2: ( (this_AMPERSAND_2= RULE_AMPERSAND ( (lv_superOnt_3_0= RULE_ID ) ) this_SEMICOLON_4= RULE_SEMICOLON )? ( (lv_ontURI_5_0= RULE_ID ) )? )
                    // InternalOWLModel.g:763:3: (this_AMPERSAND_2= RULE_AMPERSAND ( (lv_superOnt_3_0= RULE_ID ) ) this_SEMICOLON_4= RULE_SEMICOLON )? ( (lv_ontURI_5_0= RULE_ID ) )?
                    {
                    // InternalOWLModel.g:763:3: (this_AMPERSAND_2= RULE_AMPERSAND ( (lv_superOnt_3_0= RULE_ID ) ) this_SEMICOLON_4= RULE_SEMICOLON )?
                    int alt19=2;
                    int LA19_0 = input.LA(1);

                    if ( (LA19_0==RULE_AMPERSAND) ) {
                        alt19=1;
                    }
                    switch (alt19) {
                        case 1 :
                            // InternalOWLModel.g:763:4: this_AMPERSAND_2= RULE_AMPERSAND ( (lv_superOnt_3_0= RULE_ID ) ) this_SEMICOLON_4= RULE_SEMICOLON
                            {
                            this_AMPERSAND_2=(Token)match(input,RULE_AMPERSAND,FOLLOW_8); 
                             
                                newLeafNode(this_AMPERSAND_2, grammarAccess.getOntologyIDAccess().getAMPERSANDTerminalRuleCall_2_0_0_0()); 
                                
                            // InternalOWLModel.g:767:1: ( (lv_superOnt_3_0= RULE_ID ) )
                            // InternalOWLModel.g:768:1: (lv_superOnt_3_0= RULE_ID )
                            {
                            // InternalOWLModel.g:768:1: (lv_superOnt_3_0= RULE_ID )
                            // InternalOWLModel.g:769:3: lv_superOnt_3_0= RULE_ID
                            {
                            lv_superOnt_3_0=(Token)match(input,RULE_ID,FOLLOW_15); 

                            			newLeafNode(lv_superOnt_3_0, grammarAccess.getOntologyIDAccess().getSuperOntIDTerminalRuleCall_2_0_0_1_0()); 
                            		

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getOntologyIDRule());
                            	        }
                                   		setWithLastConsumed(
                                   			current, 
                                   			"superOnt",
                                    		lv_superOnt_3_0, 
                                    		"org.smool.sdk.owlparser.OWLModel.ID");
                            	    

                            }


                            }

                            this_SEMICOLON_4=(Token)match(input,RULE_SEMICOLON,FOLLOW_21); 
                             
                                newLeafNode(this_SEMICOLON_4, grammarAccess.getOntologyIDAccess().getSEMICOLONTerminalRuleCall_2_0_0_2()); 
                                

                            }
                            break;

                    }

                    // InternalOWLModel.g:789:3: ( (lv_ontURI_5_0= RULE_ID ) )?
                    int alt20=2;
                    int LA20_0 = input.LA(1);

                    if ( (LA20_0==RULE_ID) ) {
                        alt20=1;
                    }
                    switch (alt20) {
                        case 1 :
                            // InternalOWLModel.g:790:1: (lv_ontURI_5_0= RULE_ID )
                            {
                            // InternalOWLModel.g:790:1: (lv_ontURI_5_0= RULE_ID )
                            // InternalOWLModel.g:791:3: lv_ontURI_5_0= RULE_ID
                            {
                            lv_ontURI_5_0=(Token)match(input,RULE_ID,FOLLOW_9); 

                            			newLeafNode(lv_ontURI_5_0, grammarAccess.getOntologyIDAccess().getOntURIIDTerminalRuleCall_2_0_1_0()); 
                            		

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getOntologyIDRule());
                            	        }
                                   		setWithLastConsumed(
                                   			current, 
                                   			"ontURI",
                                    		lv_ontURI_5_0, 
                                    		"org.smool.sdk.owlparser.OWLModel.ID");
                            	    

                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:808:6: ( (lv_ontURI_6_0= RULE_URI ) )
                    {
                    // InternalOWLModel.g:808:6: ( (lv_ontURI_6_0= RULE_URI ) )
                    // InternalOWLModel.g:809:1: (lv_ontURI_6_0= RULE_URI )
                    {
                    // InternalOWLModel.g:809:1: (lv_ontURI_6_0= RULE_URI )
                    // InternalOWLModel.g:810:3: lv_ontURI_6_0= RULE_URI
                    {
                    lv_ontURI_6_0=(Token)match(input,RULE_URI,FOLLOW_9); 

                    			newLeafNode(lv_ontURI_6_0, grammarAccess.getOntologyIDAccess().getOntURIURITerminalRuleCall_2_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getOntologyIDRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"ontURI",
                            		lv_ontURI_6_0, 
                            		"org.smool.sdk.owlparser.OWLModel.URI");
                    	    

                    }


                    }


                    }
                    break;

            }

            this_STR_DELIMITER_7=(Token)match(input,RULE_STR_DELIMITER,FOLLOW_2); 
             
                newLeafNode(this_STR_DELIMITER_7, grammarAccess.getOntologyIDAccess().getSTR_DELIMITERTerminalRuleCall_3()); 
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOntologyID"


    // $ANTLR start "entryRuleParseType"
    // InternalOWLModel.g:838:1: entryRuleParseType returns [EObject current=null] : iv_ruleParseType= ruleParseType EOF ;
    public final EObject entryRuleParseType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParseType = null;


        try {
            // InternalOWLModel.g:839:2: (iv_ruleParseType= ruleParseType EOF )
            // InternalOWLModel.g:840:2: iv_ruleParseType= ruleParseType EOF
            {
             newCompositeNode(grammarAccess.getParseTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleParseType=ruleParseType();

            state._fsp--;

             current =iv_ruleParseType; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParseType"


    // $ANTLR start "ruleParseType"
    // InternalOWLModel.g:847:1: ruleParseType returns [EObject current=null] : (this_STR_DELIMITER_0= RULE_STR_DELIMITER ( (lv_type_1_0= RULE_ID ) ) this_STR_DELIMITER_2= RULE_STR_DELIMITER ) ;
    public final EObject ruleParseType() throws RecognitionException {
        EObject current = null;

        Token this_STR_DELIMITER_0=null;
        Token lv_type_1_0=null;
        Token this_STR_DELIMITER_2=null;

         enterRule(); 
            
        try {
            // InternalOWLModel.g:850:28: ( (this_STR_DELIMITER_0= RULE_STR_DELIMITER ( (lv_type_1_0= RULE_ID ) ) this_STR_DELIMITER_2= RULE_STR_DELIMITER ) )
            // InternalOWLModel.g:851:1: (this_STR_DELIMITER_0= RULE_STR_DELIMITER ( (lv_type_1_0= RULE_ID ) ) this_STR_DELIMITER_2= RULE_STR_DELIMITER )
            {
            // InternalOWLModel.g:851:1: (this_STR_DELIMITER_0= RULE_STR_DELIMITER ( (lv_type_1_0= RULE_ID ) ) this_STR_DELIMITER_2= RULE_STR_DELIMITER )
            // InternalOWLModel.g:851:2: this_STR_DELIMITER_0= RULE_STR_DELIMITER ( (lv_type_1_0= RULE_ID ) ) this_STR_DELIMITER_2= RULE_STR_DELIMITER
            {
            this_STR_DELIMITER_0=(Token)match(input,RULE_STR_DELIMITER,FOLLOW_8); 
             
                newLeafNode(this_STR_DELIMITER_0, grammarAccess.getParseTypeAccess().getSTR_DELIMITERTerminalRuleCall_0()); 
                
            // InternalOWLModel.g:855:1: ( (lv_type_1_0= RULE_ID ) )
            // InternalOWLModel.g:856:1: (lv_type_1_0= RULE_ID )
            {
            // InternalOWLModel.g:856:1: (lv_type_1_0= RULE_ID )
            // InternalOWLModel.g:857:3: lv_type_1_0= RULE_ID
            {
            lv_type_1_0=(Token)match(input,RULE_ID,FOLLOW_9); 

            			newLeafNode(lv_type_1_0, grammarAccess.getParseTypeAccess().getTypeIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getParseTypeRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"type",
                    		lv_type_1_0, 
                    		"org.smool.sdk.owlparser.OWLModel.ID");
            	    

            }


            }

            this_STR_DELIMITER_2=(Token)match(input,RULE_STR_DELIMITER,FOLLOW_2); 
             
                newLeafNode(this_STR_DELIMITER_2, grammarAccess.getParseTypeAccess().getSTR_DELIMITERTerminalRuleCall_2()); 
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParseType"


    // $ANTLR start "entryRuleOntologyElement"
    // InternalOWLModel.g:885:1: entryRuleOntologyElement returns [EObject current=null] : iv_ruleOntologyElement= ruleOntologyElement EOF ;
    public final EObject entryRuleOntologyElement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOntologyElement = null;


        try {
            // InternalOWLModel.g:886:2: (iv_ruleOntologyElement= ruleOntologyElement EOF )
            // InternalOWLModel.g:887:2: iv_ruleOntologyElement= ruleOntologyElement EOF
            {
             newCompositeNode(grammarAccess.getOntologyElementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOntologyElement=ruleOntologyElement();

            state._fsp--;

             current =iv_ruleOntologyElement; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOntologyElement"


    // $ANTLR start "ruleOntologyElement"
    // InternalOWLModel.g:894:1: ruleOntologyElement returns [EObject current=null] : (this_Ontology_0= ruleOntology | this_Datatype_1= ruleDatatype | this_OwlClass_2= ruleOwlClass | this_Property_3= ruleProperty | this_Description_4= ruleDescription | this_NamedIndividual_5= ruleNamedIndividual ) ;
    public final EObject ruleOntologyElement() throws RecognitionException {
        EObject current = null;

        EObject this_Ontology_0 = null;

        EObject this_Datatype_1 = null;

        EObject this_OwlClass_2 = null;

        EObject this_Property_3 = null;

        EObject this_Description_4 = null;

        EObject this_NamedIndividual_5 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:897:28: ( (this_Ontology_0= ruleOntology | this_Datatype_1= ruleDatatype | this_OwlClass_2= ruleOwlClass | this_Property_3= ruleProperty | this_Description_4= ruleDescription | this_NamedIndividual_5= ruleNamedIndividual ) )
            // InternalOWLModel.g:898:1: (this_Ontology_0= ruleOntology | this_Datatype_1= ruleDatatype | this_OwlClass_2= ruleOwlClass | this_Property_3= ruleProperty | this_Description_4= ruleDescription | this_NamedIndividual_5= ruleNamedIndividual )
            {
            // InternalOWLModel.g:898:1: (this_Ontology_0= ruleOntology | this_Datatype_1= ruleDatatype | this_OwlClass_2= ruleOwlClass | this_Property_3= ruleProperty | this_Description_4= ruleDescription | this_NamedIndividual_5= ruleNamedIndividual )
            int alt22=6;
            switch ( input.LA(1) ) {
            case 31:
                {
                alt22=1;
                }
                break;
            case 130:
                {
                alt22=2;
                }
                break;
            case 39:
                {
                alt22=3;
                }
                break;
            case 94:
            case 96:
            case 98:
            case 100:
            case 102:
            case 104:
            case 106:
                {
                alt22=4;
                }
                break;
            case 122:
                {
                alt22=5;
                }
                break;
            case 27:
            case 58:
                {
                alt22=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 22, 0, input);

                throw nvae;
            }

            switch (alt22) {
                case 1 :
                    // InternalOWLModel.g:899:5: this_Ontology_0= ruleOntology
                    {
                     
                            newCompositeNode(grammarAccess.getOntologyElementAccess().getOntologyParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_2);
                    this_Ontology_0=ruleOntology();

                    state._fsp--;

                     
                            current = this_Ontology_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:909:5: this_Datatype_1= ruleDatatype
                    {
                     
                            newCompositeNode(grammarAccess.getOntologyElementAccess().getDatatypeParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_2);
                    this_Datatype_1=ruleDatatype();

                    state._fsp--;

                     
                            current = this_Datatype_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // InternalOWLModel.g:919:5: this_OwlClass_2= ruleOwlClass
                    {
                     
                            newCompositeNode(grammarAccess.getOntologyElementAccess().getOwlClassParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_2);
                    this_OwlClass_2=ruleOwlClass();

                    state._fsp--;

                     
                            current = this_OwlClass_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // InternalOWLModel.g:929:5: this_Property_3= ruleProperty
                    {
                     
                            newCompositeNode(grammarAccess.getOntologyElementAccess().getPropertyParserRuleCall_3()); 
                        
                    pushFollow(FOLLOW_2);
                    this_Property_3=ruleProperty();

                    state._fsp--;

                     
                            current = this_Property_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 5 :
                    // InternalOWLModel.g:939:5: this_Description_4= ruleDescription
                    {
                     
                            newCompositeNode(grammarAccess.getOntologyElementAccess().getDescriptionParserRuleCall_4()); 
                        
                    pushFollow(FOLLOW_2);
                    this_Description_4=ruleDescription();

                    state._fsp--;

                     
                            current = this_Description_4; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 6 :
                    // InternalOWLModel.g:949:5: this_NamedIndividual_5= ruleNamedIndividual
                    {
                     
                            newCompositeNode(grammarAccess.getOntologyElementAccess().getNamedIndividualParserRuleCall_5()); 
                        
                    pushFollow(FOLLOW_2);
                    this_NamedIndividual_5=ruleNamedIndividual();

                    state._fsp--;

                     
                            current = this_NamedIndividual_5; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOntologyElement"


    // $ANTLR start "entryRuleNamedIndividual"
    // InternalOWLModel.g:965:1: entryRuleNamedIndividual returns [EObject current=null] : iv_ruleNamedIndividual= ruleNamedIndividual EOF ;
    public final EObject entryRuleNamedIndividual() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNamedIndividual = null;


        try {
            // InternalOWLModel.g:966:2: (iv_ruleNamedIndividual= ruleNamedIndividual EOF )
            // InternalOWLModel.g:967:2: iv_ruleNamedIndividual= ruleNamedIndividual EOF
            {
             newCompositeNode(grammarAccess.getNamedIndividualRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNamedIndividual=ruleNamedIndividual();

            state._fsp--;

             current =iv_ruleNamedIndividual; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNamedIndividual"


    // $ANTLR start "ruleNamedIndividual"
    // InternalOWLModel.g:974:1: ruleNamedIndividual returns [EObject current=null] : ( () ( (otherlv_1= '<owl:NamedIndividual' (otherlv_2= 'rdf:about' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) )? (otherlv_5= '/>' | (otherlv_6= '>' ( (lv_types_7_0= ruleRDFType ) )* otherlv_8= '</owl:NamedIndividual' otherlv_9= '>' ) ) ) | ( (lv_desc_10_0= ruleANY_OTHER_TAG ) ) ) ) ;
    public final EObject ruleNamedIndividual() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        EObject lv_ref_4_0 = null;

        EObject lv_types_7_0 = null;

        AntlrDatatypeRuleToken lv_desc_10_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:977:28: ( ( () ( (otherlv_1= '<owl:NamedIndividual' (otherlv_2= 'rdf:about' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) )? (otherlv_5= '/>' | (otherlv_6= '>' ( (lv_types_7_0= ruleRDFType ) )* otherlv_8= '</owl:NamedIndividual' otherlv_9= '>' ) ) ) | ( (lv_desc_10_0= ruleANY_OTHER_TAG ) ) ) ) )
            // InternalOWLModel.g:978:1: ( () ( (otherlv_1= '<owl:NamedIndividual' (otherlv_2= 'rdf:about' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) )? (otherlv_5= '/>' | (otherlv_6= '>' ( (lv_types_7_0= ruleRDFType ) )* otherlv_8= '</owl:NamedIndividual' otherlv_9= '>' ) ) ) | ( (lv_desc_10_0= ruleANY_OTHER_TAG ) ) ) )
            {
            // InternalOWLModel.g:978:1: ( () ( (otherlv_1= '<owl:NamedIndividual' (otherlv_2= 'rdf:about' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) )? (otherlv_5= '/>' | (otherlv_6= '>' ( (lv_types_7_0= ruleRDFType ) )* otherlv_8= '</owl:NamedIndividual' otherlv_9= '>' ) ) ) | ( (lv_desc_10_0= ruleANY_OTHER_TAG ) ) ) )
            // InternalOWLModel.g:978:2: () ( (otherlv_1= '<owl:NamedIndividual' (otherlv_2= 'rdf:about' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) )? (otherlv_5= '/>' | (otherlv_6= '>' ( (lv_types_7_0= ruleRDFType ) )* otherlv_8= '</owl:NamedIndividual' otherlv_9= '>' ) ) ) | ( (lv_desc_10_0= ruleANY_OTHER_TAG ) ) )
            {
            // InternalOWLModel.g:978:2: ()
            // InternalOWLModel.g:979:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getNamedIndividualAccess().getNamedIndividualAction_0(),
                        current);
                

            }

            // InternalOWLModel.g:984:2: ( (otherlv_1= '<owl:NamedIndividual' (otherlv_2= 'rdf:about' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) )? (otherlv_5= '/>' | (otherlv_6= '>' ( (lv_types_7_0= ruleRDFType ) )* otherlv_8= '</owl:NamedIndividual' otherlv_9= '>' ) ) ) | ( (lv_desc_10_0= ruleANY_OTHER_TAG ) ) )
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==27) ) {
                alt26=1;
            }
            else if ( (LA26_0==58) ) {
                alt26=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 26, 0, input);

                throw nvae;
            }
            switch (alt26) {
                case 1 :
                    // InternalOWLModel.g:984:3: (otherlv_1= '<owl:NamedIndividual' (otherlv_2= 'rdf:about' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) )? (otherlv_5= '/>' | (otherlv_6= '>' ( (lv_types_7_0= ruleRDFType ) )* otherlv_8= '</owl:NamedIndividual' otherlv_9= '>' ) ) )
                    {
                    // InternalOWLModel.g:984:3: (otherlv_1= '<owl:NamedIndividual' (otherlv_2= 'rdf:about' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) )? (otherlv_5= '/>' | (otherlv_6= '>' ( (lv_types_7_0= ruleRDFType ) )* otherlv_8= '</owl:NamedIndividual' otherlv_9= '>' ) ) )
                    // InternalOWLModel.g:984:5: otherlv_1= '<owl:NamedIndividual' (otherlv_2= 'rdf:about' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) )? (otherlv_5= '/>' | (otherlv_6= '>' ( (lv_types_7_0= ruleRDFType ) )* otherlv_8= '</owl:NamedIndividual' otherlv_9= '>' ) )
                    {
                    otherlv_1=(Token)match(input,27,FOLLOW_23); 

                        	newLeafNode(otherlv_1, grammarAccess.getNamedIndividualAccess().getOwlNamedIndividualKeyword_1_0_0());
                        
                    // InternalOWLModel.g:988:1: (otherlv_2= 'rdf:about' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) )?
                    int alt23=2;
                    int LA23_0 = input.LA(1);

                    if ( (LA23_0==28) ) {
                        alt23=1;
                    }
                    switch (alt23) {
                        case 1 :
                            // InternalOWLModel.g:988:3: otherlv_2= 'rdf:about' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) )
                            {
                            otherlv_2=(Token)match(input,28,FOLLOW_13); 

                                	newLeafNode(otherlv_2, grammarAccess.getNamedIndividualAccess().getRdfAboutKeyword_1_0_1_0());
                                
                            otherlv_3=(Token)match(input,25,FOLLOW_9); 

                                	newLeafNode(otherlv_3, grammarAccess.getNamedIndividualAccess().getEqualsSignKeyword_1_0_1_1());
                                
                            // InternalOWLModel.g:996:1: ( (lv_ref_4_0= ruleOwlRef ) )
                            // InternalOWLModel.g:997:1: (lv_ref_4_0= ruleOwlRef )
                            {
                            // InternalOWLModel.g:997:1: (lv_ref_4_0= ruleOwlRef )
                            // InternalOWLModel.g:998:3: lv_ref_4_0= ruleOwlRef
                            {
                             
                            	        newCompositeNode(grammarAccess.getNamedIndividualAccess().getRefOwlRefParserRuleCall_1_0_1_2_0()); 
                            	    
                            pushFollow(FOLLOW_24);
                            lv_ref_4_0=ruleOwlRef();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getNamedIndividualRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"ref",
                                    		lv_ref_4_0, 
                                    		"org.smool.sdk.owlparser.OWLModel.OwlRef");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }

                    // InternalOWLModel.g:1014:4: (otherlv_5= '/>' | (otherlv_6= '>' ( (lv_types_7_0= ruleRDFType ) )* otherlv_8= '</owl:NamedIndividual' otherlv_9= '>' ) )
                    int alt25=2;
                    int LA25_0 = input.LA(1);

                    if ( (LA25_0==29) ) {
                        alt25=1;
                    }
                    else if ( (LA25_0==21) ) {
                        alt25=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 25, 0, input);

                        throw nvae;
                    }
                    switch (alt25) {
                        case 1 :
                            // InternalOWLModel.g:1014:6: otherlv_5= '/>'
                            {
                            otherlv_5=(Token)match(input,29,FOLLOW_2); 

                                	newLeafNode(otherlv_5, grammarAccess.getNamedIndividualAccess().getSolidusGreaterThanSignKeyword_1_0_2_0());
                                

                            }
                            break;
                        case 2 :
                            // InternalOWLModel.g:1019:6: (otherlv_6= '>' ( (lv_types_7_0= ruleRDFType ) )* otherlv_8= '</owl:NamedIndividual' otherlv_9= '>' )
                            {
                            // InternalOWLModel.g:1019:6: (otherlv_6= '>' ( (lv_types_7_0= ruleRDFType ) )* otherlv_8= '</owl:NamedIndividual' otherlv_9= '>' )
                            // InternalOWLModel.g:1019:8: otherlv_6= '>' ( (lv_types_7_0= ruleRDFType ) )* otherlv_8= '</owl:NamedIndividual' otherlv_9= '>'
                            {
                            otherlv_6=(Token)match(input,21,FOLLOW_25); 

                                	newLeafNode(otherlv_6, grammarAccess.getNamedIndividualAccess().getGreaterThanSignKeyword_1_0_2_1_0());
                                
                            // InternalOWLModel.g:1023:1: ( (lv_types_7_0= ruleRDFType ) )*
                            loop24:
                            do {
                                int alt24=2;
                                int LA24_0 = input.LA(1);

                                if ( (LA24_0==118) ) {
                                    alt24=1;
                                }


                                switch (alt24) {
                            	case 1 :
                            	    // InternalOWLModel.g:1024:1: (lv_types_7_0= ruleRDFType )
                            	    {
                            	    // InternalOWLModel.g:1024:1: (lv_types_7_0= ruleRDFType )
                            	    // InternalOWLModel.g:1025:3: lv_types_7_0= ruleRDFType
                            	    {
                            	     
                            	    	        newCompositeNode(grammarAccess.getNamedIndividualAccess().getTypesRDFTypeParserRuleCall_1_0_2_1_1_0()); 
                            	    	    
                            	    pushFollow(FOLLOW_25);
                            	    lv_types_7_0=ruleRDFType();

                            	    state._fsp--;


                            	    	        if (current==null) {
                            	    	            current = createModelElementForParent(grammarAccess.getNamedIndividualRule());
                            	    	        }
                            	           		add(
                            	           			current, 
                            	           			"types",
                            	            		lv_types_7_0, 
                            	            		"org.smool.sdk.owlparser.OWLModel.RDFType");
                            	    	        afterParserOrEnumRuleCall();
                            	    	    

                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop24;
                                }
                            } while (true);

                            otherlv_8=(Token)match(input,30,FOLLOW_7); 

                                	newLeafNode(otherlv_8, grammarAccess.getNamedIndividualAccess().getOwlNamedIndividualKeyword_1_0_2_1_2());
                                
                            otherlv_9=(Token)match(input,21,FOLLOW_2); 

                                	newLeafNode(otherlv_9, grammarAccess.getNamedIndividualAccess().getGreaterThanSignKeyword_1_0_2_1_3());
                                

                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:1050:6: ( (lv_desc_10_0= ruleANY_OTHER_TAG ) )
                    {
                    // InternalOWLModel.g:1050:6: ( (lv_desc_10_0= ruleANY_OTHER_TAG ) )
                    // InternalOWLModel.g:1051:1: (lv_desc_10_0= ruleANY_OTHER_TAG )
                    {
                    // InternalOWLModel.g:1051:1: (lv_desc_10_0= ruleANY_OTHER_TAG )
                    // InternalOWLModel.g:1052:3: lv_desc_10_0= ruleANY_OTHER_TAG
                    {
                     
                    	        newCompositeNode(grammarAccess.getNamedIndividualAccess().getDescANY_OTHER_TAGParserRuleCall_1_1_0()); 
                    	    
                    pushFollow(FOLLOW_2);
                    lv_desc_10_0=ruleANY_OTHER_TAG();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getNamedIndividualRule());
                    	        }
                           		set(
                           			current, 
                           			"desc",
                            		lv_desc_10_0, 
                            		"org.smool.sdk.owlparser.OWLModel.ANY_OTHER_TAG");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNamedIndividual"


    // $ANTLR start "entryRuleOntology"
    // InternalOWLModel.g:1076:1: entryRuleOntology returns [EObject current=null] : iv_ruleOntology= ruleOntology EOF ;
    public final EObject entryRuleOntology() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOntology = null;


        try {
            // InternalOWLModel.g:1077:2: (iv_ruleOntology= ruleOntology EOF )
            // InternalOWLModel.g:1078:2: iv_ruleOntology= ruleOntology EOF
            {
             newCompositeNode(grammarAccess.getOntologyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOntology=ruleOntology();

            state._fsp--;

             current =iv_ruleOntology; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOntology"


    // $ANTLR start "ruleOntology"
    // InternalOWLModel.g:1085:1: ruleOntology returns [EObject current=null] : ( () otherlv_1= '<owl:Ontology' ( (otherlv_2= 'rdf:about' otherlv_3= '=' ( (lv_about_4_0= ruleOntologyID ) ) ) | (otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_resource_7_0= ruleOwlRef ) ) ) )? (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_seeAlso_12_0= ruleSeeAlso ) ) | ( (lv_pVers_13_0= rulePriorVersion ) ) | ( (lv_backComp_14_0= ruleBackwardsComp ) ) | ( (lv_importedOntologies_15_0= ruleOwlImport ) ) | ( (lv_others_16_0= ruleANY_OTHER_TAG ) ) )* otherlv_17= '</owl:Ontology' otherlv_18= '>' ) ) ) ;
    public final EObject ruleOntology() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        EObject lv_about_4_0 = null;

        EObject lv_resource_7_0 = null;

        EObject lv_labels_10_0 = null;

        EObject lv_versionInfo_11_0 = null;

        EObject lv_seeAlso_12_0 = null;

        EObject lv_pVers_13_0 = null;

        EObject lv_backComp_14_0 = null;

        EObject lv_importedOntologies_15_0 = null;

        AntlrDatatypeRuleToken lv_others_16_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:1088:28: ( ( () otherlv_1= '<owl:Ontology' ( (otherlv_2= 'rdf:about' otherlv_3= '=' ( (lv_about_4_0= ruleOntologyID ) ) ) | (otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_resource_7_0= ruleOwlRef ) ) ) )? (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_seeAlso_12_0= ruleSeeAlso ) ) | ( (lv_pVers_13_0= rulePriorVersion ) ) | ( (lv_backComp_14_0= ruleBackwardsComp ) ) | ( (lv_importedOntologies_15_0= ruleOwlImport ) ) | ( (lv_others_16_0= ruleANY_OTHER_TAG ) ) )* otherlv_17= '</owl:Ontology' otherlv_18= '>' ) ) ) )
            // InternalOWLModel.g:1089:1: ( () otherlv_1= '<owl:Ontology' ( (otherlv_2= 'rdf:about' otherlv_3= '=' ( (lv_about_4_0= ruleOntologyID ) ) ) | (otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_resource_7_0= ruleOwlRef ) ) ) )? (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_seeAlso_12_0= ruleSeeAlso ) ) | ( (lv_pVers_13_0= rulePriorVersion ) ) | ( (lv_backComp_14_0= ruleBackwardsComp ) ) | ( (lv_importedOntologies_15_0= ruleOwlImport ) ) | ( (lv_others_16_0= ruleANY_OTHER_TAG ) ) )* otherlv_17= '</owl:Ontology' otherlv_18= '>' ) ) )
            {
            // InternalOWLModel.g:1089:1: ( () otherlv_1= '<owl:Ontology' ( (otherlv_2= 'rdf:about' otherlv_3= '=' ( (lv_about_4_0= ruleOntologyID ) ) ) | (otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_resource_7_0= ruleOwlRef ) ) ) )? (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_seeAlso_12_0= ruleSeeAlso ) ) | ( (lv_pVers_13_0= rulePriorVersion ) ) | ( (lv_backComp_14_0= ruleBackwardsComp ) ) | ( (lv_importedOntologies_15_0= ruleOwlImport ) ) | ( (lv_others_16_0= ruleANY_OTHER_TAG ) ) )* otherlv_17= '</owl:Ontology' otherlv_18= '>' ) ) )
            // InternalOWLModel.g:1089:2: () otherlv_1= '<owl:Ontology' ( (otherlv_2= 'rdf:about' otherlv_3= '=' ( (lv_about_4_0= ruleOntologyID ) ) ) | (otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_resource_7_0= ruleOwlRef ) ) ) )? (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_seeAlso_12_0= ruleSeeAlso ) ) | ( (lv_pVers_13_0= rulePriorVersion ) ) | ( (lv_backComp_14_0= ruleBackwardsComp ) ) | ( (lv_importedOntologies_15_0= ruleOwlImport ) ) | ( (lv_others_16_0= ruleANY_OTHER_TAG ) ) )* otherlv_17= '</owl:Ontology' otherlv_18= '>' ) )
            {
            // InternalOWLModel.g:1089:2: ()
            // InternalOWLModel.g:1090:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getOntologyAccess().getOntologyAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,31,FOLLOW_26); 

                	newLeafNode(otherlv_1, grammarAccess.getOntologyAccess().getOwlOntologyKeyword_1());
                
            // InternalOWLModel.g:1099:1: ( (otherlv_2= 'rdf:about' otherlv_3= '=' ( (lv_about_4_0= ruleOntologyID ) ) ) | (otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_resource_7_0= ruleOwlRef ) ) ) )?
            int alt27=3;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==28) ) {
                alt27=1;
            }
            else if ( (LA27_0==32) ) {
                alt27=2;
            }
            switch (alt27) {
                case 1 :
                    // InternalOWLModel.g:1099:2: (otherlv_2= 'rdf:about' otherlv_3= '=' ( (lv_about_4_0= ruleOntologyID ) ) )
                    {
                    // InternalOWLModel.g:1099:2: (otherlv_2= 'rdf:about' otherlv_3= '=' ( (lv_about_4_0= ruleOntologyID ) ) )
                    // InternalOWLModel.g:1099:4: otherlv_2= 'rdf:about' otherlv_3= '=' ( (lv_about_4_0= ruleOntologyID ) )
                    {
                    otherlv_2=(Token)match(input,28,FOLLOW_13); 

                        	newLeafNode(otherlv_2, grammarAccess.getOntologyAccess().getRdfAboutKeyword_2_0_0());
                        
                    otherlv_3=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_3, grammarAccess.getOntologyAccess().getEqualsSignKeyword_2_0_1());
                        
                    // InternalOWLModel.g:1107:1: ( (lv_about_4_0= ruleOntologyID ) )
                    // InternalOWLModel.g:1108:1: (lv_about_4_0= ruleOntologyID )
                    {
                    // InternalOWLModel.g:1108:1: (lv_about_4_0= ruleOntologyID )
                    // InternalOWLModel.g:1109:3: lv_about_4_0= ruleOntologyID
                    {
                     
                    	        newCompositeNode(grammarAccess.getOntologyAccess().getAboutOntologyIDParserRuleCall_2_0_2_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_about_4_0=ruleOntologyID();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getOntologyRule());
                    	        }
                           		set(
                           			current, 
                           			"about",
                            		lv_about_4_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OntologyID");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:1126:6: (otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_resource_7_0= ruleOwlRef ) ) )
                    {
                    // InternalOWLModel.g:1126:6: (otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_resource_7_0= ruleOwlRef ) ) )
                    // InternalOWLModel.g:1126:8: otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_resource_7_0= ruleOwlRef ) )
                    {
                    otherlv_5=(Token)match(input,32,FOLLOW_13); 

                        	newLeafNode(otherlv_5, grammarAccess.getOntologyAccess().getRdfResourceKeyword_2_1_0());
                        
                    otherlv_6=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_6, grammarAccess.getOntologyAccess().getEqualsSignKeyword_2_1_1());
                        
                    // InternalOWLModel.g:1134:1: ( (lv_resource_7_0= ruleOwlRef ) )
                    // InternalOWLModel.g:1135:1: (lv_resource_7_0= ruleOwlRef )
                    {
                    // InternalOWLModel.g:1135:1: (lv_resource_7_0= ruleOwlRef )
                    // InternalOWLModel.g:1136:3: lv_resource_7_0= ruleOwlRef
                    {
                     
                    	        newCompositeNode(grammarAccess.getOntologyAccess().getResourceOwlRefParserRuleCall_2_1_2_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_resource_7_0=ruleOwlRef();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getOntologyRule());
                    	        }
                           		set(
                           			current, 
                           			"resource",
                            		lv_resource_7_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlRef");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;

            }

            // InternalOWLModel.g:1152:5: (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_seeAlso_12_0= ruleSeeAlso ) ) | ( (lv_pVers_13_0= rulePriorVersion ) ) | ( (lv_backComp_14_0= ruleBackwardsComp ) ) | ( (lv_importedOntologies_15_0= ruleOwlImport ) ) | ( (lv_others_16_0= ruleANY_OTHER_TAG ) ) )* otherlv_17= '</owl:Ontology' otherlv_18= '>' ) )
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==29) ) {
                alt29=1;
            }
            else if ( (LA29_0==21) ) {
                alt29=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 29, 0, input);

                throw nvae;
            }
            switch (alt29) {
                case 1 :
                    // InternalOWLModel.g:1152:7: otherlv_8= '/>'
                    {
                    otherlv_8=(Token)match(input,29,FOLLOW_2); 

                        	newLeafNode(otherlv_8, grammarAccess.getOntologyAccess().getSolidusGreaterThanSignKeyword_3_0());
                        

                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:1157:6: (otherlv_9= '>' ( ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_seeAlso_12_0= ruleSeeAlso ) ) | ( (lv_pVers_13_0= rulePriorVersion ) ) | ( (lv_backComp_14_0= ruleBackwardsComp ) ) | ( (lv_importedOntologies_15_0= ruleOwlImport ) ) | ( (lv_others_16_0= ruleANY_OTHER_TAG ) ) )* otherlv_17= '</owl:Ontology' otherlv_18= '>' )
                    {
                    // InternalOWLModel.g:1157:6: (otherlv_9= '>' ( ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_seeAlso_12_0= ruleSeeAlso ) ) | ( (lv_pVers_13_0= rulePriorVersion ) ) | ( (lv_backComp_14_0= ruleBackwardsComp ) ) | ( (lv_importedOntologies_15_0= ruleOwlImport ) ) | ( (lv_others_16_0= ruleANY_OTHER_TAG ) ) )* otherlv_17= '</owl:Ontology' otherlv_18= '>' )
                    // InternalOWLModel.g:1157:8: otherlv_9= '>' ( ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_seeAlso_12_0= ruleSeeAlso ) ) | ( (lv_pVers_13_0= rulePriorVersion ) ) | ( (lv_backComp_14_0= ruleBackwardsComp ) ) | ( (lv_importedOntologies_15_0= ruleOwlImport ) ) | ( (lv_others_16_0= ruleANY_OTHER_TAG ) ) )* otherlv_17= '</owl:Ontology' otherlv_18= '>'
                    {
                    otherlv_9=(Token)match(input,21,FOLLOW_27); 

                        	newLeafNode(otherlv_9, grammarAccess.getOntologyAccess().getGreaterThanSignKeyword_3_1_0());
                        
                    // InternalOWLModel.g:1161:1: ( ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_seeAlso_12_0= ruleSeeAlso ) ) | ( (lv_pVers_13_0= rulePriorVersion ) ) | ( (lv_backComp_14_0= ruleBackwardsComp ) ) | ( (lv_importedOntologies_15_0= ruleOwlImport ) ) | ( (lv_others_16_0= ruleANY_OTHER_TAG ) ) )*
                    loop28:
                    do {
                        int alt28=8;
                        switch ( input.LA(1) ) {
                        case 60:
                            {
                            alt28=1;
                            }
                            break;
                        case 64:
                            {
                            alt28=2;
                            }
                            break;
                        case 34:
                            {
                            alt28=3;
                            }
                            break;
                        case 35:
                            {
                            alt28=4;
                            }
                            break;
                        case 36:
                            {
                            alt28=5;
                            }
                            break;
                        case 37:
                            {
                            alt28=6;
                            }
                            break;
                        case 58:
                            {
                            alt28=7;
                            }
                            break;

                        }

                        switch (alt28) {
                    	case 1 :
                    	    // InternalOWLModel.g:1161:2: ( (lv_labels_10_0= ruleOwlLabel ) )
                    	    {
                    	    // InternalOWLModel.g:1161:2: ( (lv_labels_10_0= ruleOwlLabel ) )
                    	    // InternalOWLModel.g:1162:1: (lv_labels_10_0= ruleOwlLabel )
                    	    {
                    	    // InternalOWLModel.g:1162:1: (lv_labels_10_0= ruleOwlLabel )
                    	    // InternalOWLModel.g:1163:3: lv_labels_10_0= ruleOwlLabel
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getOntologyAccess().getLabelsOwlLabelParserRuleCall_3_1_1_0_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_27);
                    	    lv_labels_10_0=ruleOwlLabel();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getOntologyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"labels",
                    	            		lv_labels_10_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.OwlLabel");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalOWLModel.g:1180:6: ( (lv_versionInfo_11_0= ruleOwlVersion ) )
                    	    {
                    	    // InternalOWLModel.g:1180:6: ( (lv_versionInfo_11_0= ruleOwlVersion ) )
                    	    // InternalOWLModel.g:1181:1: (lv_versionInfo_11_0= ruleOwlVersion )
                    	    {
                    	    // InternalOWLModel.g:1181:1: (lv_versionInfo_11_0= ruleOwlVersion )
                    	    // InternalOWLModel.g:1182:3: lv_versionInfo_11_0= ruleOwlVersion
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getOntologyAccess().getVersionInfoOwlVersionParserRuleCall_3_1_1_1_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_27);
                    	    lv_versionInfo_11_0=ruleOwlVersion();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getOntologyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"versionInfo",
                    	            		lv_versionInfo_11_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.OwlVersion");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 3 :
                    	    // InternalOWLModel.g:1199:6: ( (lv_seeAlso_12_0= ruleSeeAlso ) )
                    	    {
                    	    // InternalOWLModel.g:1199:6: ( (lv_seeAlso_12_0= ruleSeeAlso ) )
                    	    // InternalOWLModel.g:1200:1: (lv_seeAlso_12_0= ruleSeeAlso )
                    	    {
                    	    // InternalOWLModel.g:1200:1: (lv_seeAlso_12_0= ruleSeeAlso )
                    	    // InternalOWLModel.g:1201:3: lv_seeAlso_12_0= ruleSeeAlso
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getOntologyAccess().getSeeAlsoSeeAlsoParserRuleCall_3_1_1_2_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_27);
                    	    lv_seeAlso_12_0=ruleSeeAlso();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getOntologyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"seeAlso",
                    	            		lv_seeAlso_12_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.SeeAlso");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 4 :
                    	    // InternalOWLModel.g:1218:6: ( (lv_pVers_13_0= rulePriorVersion ) )
                    	    {
                    	    // InternalOWLModel.g:1218:6: ( (lv_pVers_13_0= rulePriorVersion ) )
                    	    // InternalOWLModel.g:1219:1: (lv_pVers_13_0= rulePriorVersion )
                    	    {
                    	    // InternalOWLModel.g:1219:1: (lv_pVers_13_0= rulePriorVersion )
                    	    // InternalOWLModel.g:1220:3: lv_pVers_13_0= rulePriorVersion
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getOntologyAccess().getPVersPriorVersionParserRuleCall_3_1_1_3_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_27);
                    	    lv_pVers_13_0=rulePriorVersion();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getOntologyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"pVers",
                    	            		lv_pVers_13_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.PriorVersion");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 5 :
                    	    // InternalOWLModel.g:1237:6: ( (lv_backComp_14_0= ruleBackwardsComp ) )
                    	    {
                    	    // InternalOWLModel.g:1237:6: ( (lv_backComp_14_0= ruleBackwardsComp ) )
                    	    // InternalOWLModel.g:1238:1: (lv_backComp_14_0= ruleBackwardsComp )
                    	    {
                    	    // InternalOWLModel.g:1238:1: (lv_backComp_14_0= ruleBackwardsComp )
                    	    // InternalOWLModel.g:1239:3: lv_backComp_14_0= ruleBackwardsComp
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getOntologyAccess().getBackCompBackwardsCompParserRuleCall_3_1_1_4_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_27);
                    	    lv_backComp_14_0=ruleBackwardsComp();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getOntologyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"backComp",
                    	            		lv_backComp_14_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.BackwardsComp");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 6 :
                    	    // InternalOWLModel.g:1256:6: ( (lv_importedOntologies_15_0= ruleOwlImport ) )
                    	    {
                    	    // InternalOWLModel.g:1256:6: ( (lv_importedOntologies_15_0= ruleOwlImport ) )
                    	    // InternalOWLModel.g:1257:1: (lv_importedOntologies_15_0= ruleOwlImport )
                    	    {
                    	    // InternalOWLModel.g:1257:1: (lv_importedOntologies_15_0= ruleOwlImport )
                    	    // InternalOWLModel.g:1258:3: lv_importedOntologies_15_0= ruleOwlImport
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getOntologyAccess().getImportedOntologiesOwlImportParserRuleCall_3_1_1_5_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_27);
                    	    lv_importedOntologies_15_0=ruleOwlImport();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getOntologyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"importedOntologies",
                    	            		lv_importedOntologies_15_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.OwlImport");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 7 :
                    	    // InternalOWLModel.g:1275:6: ( (lv_others_16_0= ruleANY_OTHER_TAG ) )
                    	    {
                    	    // InternalOWLModel.g:1275:6: ( (lv_others_16_0= ruleANY_OTHER_TAG ) )
                    	    // InternalOWLModel.g:1276:1: (lv_others_16_0= ruleANY_OTHER_TAG )
                    	    {
                    	    // InternalOWLModel.g:1276:1: (lv_others_16_0= ruleANY_OTHER_TAG )
                    	    // InternalOWLModel.g:1277:3: lv_others_16_0= ruleANY_OTHER_TAG
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getOntologyAccess().getOthersANY_OTHER_TAGParserRuleCall_3_1_1_6_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_27);
                    	    lv_others_16_0=ruleANY_OTHER_TAG();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getOntologyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"others",
                    	            		lv_others_16_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.ANY_OTHER_TAG");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop28;
                        }
                    } while (true);

                    otherlv_17=(Token)match(input,33,FOLLOW_7); 

                        	newLeafNode(otherlv_17, grammarAccess.getOntologyAccess().getOwlOntologyKeyword_3_1_2());
                        
                    otherlv_18=(Token)match(input,21,FOLLOW_2); 

                        	newLeafNode(otherlv_18, grammarAccess.getOntologyAccess().getGreaterThanSignKeyword_3_1_3());
                        

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOntology"


    // $ANTLR start "entryRuleSeeAlso"
    // InternalOWLModel.g:1309:1: entryRuleSeeAlso returns [EObject current=null] : iv_ruleSeeAlso= ruleSeeAlso EOF ;
    public final EObject entryRuleSeeAlso() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSeeAlso = null;


        try {
            // InternalOWLModel.g:1310:2: (iv_ruleSeeAlso= ruleSeeAlso EOF )
            // InternalOWLModel.g:1311:2: iv_ruleSeeAlso= ruleSeeAlso EOF
            {
             newCompositeNode(grammarAccess.getSeeAlsoRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSeeAlso=ruleSeeAlso();

            state._fsp--;

             current =iv_ruleSeeAlso; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSeeAlso"


    // $ANTLR start "ruleSeeAlso"
    // InternalOWLModel.g:1318:1: ruleSeeAlso returns [EObject current=null] : (otherlv_0= '<rdfs:seeAlso' otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOntologyID ) ) otherlv_4= '/>' ) ;
    public final EObject ruleSeeAlso() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_ref_3_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:1321:28: ( (otherlv_0= '<rdfs:seeAlso' otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOntologyID ) ) otherlv_4= '/>' ) )
            // InternalOWLModel.g:1322:1: (otherlv_0= '<rdfs:seeAlso' otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOntologyID ) ) otherlv_4= '/>' )
            {
            // InternalOWLModel.g:1322:1: (otherlv_0= '<rdfs:seeAlso' otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOntologyID ) ) otherlv_4= '/>' )
            // InternalOWLModel.g:1322:3: otherlv_0= '<rdfs:seeAlso' otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOntologyID ) ) otherlv_4= '/>'
            {
            otherlv_0=(Token)match(input,34,FOLLOW_28); 

                	newLeafNode(otherlv_0, grammarAccess.getSeeAlsoAccess().getRdfsSeeAlsoKeyword_0());
                
            otherlv_1=(Token)match(input,32,FOLLOW_13); 

                	newLeafNode(otherlv_1, grammarAccess.getSeeAlsoAccess().getRdfResourceKeyword_1());
                
            otherlv_2=(Token)match(input,25,FOLLOW_9); 

                	newLeafNode(otherlv_2, grammarAccess.getSeeAlsoAccess().getEqualsSignKeyword_2());
                
            // InternalOWLModel.g:1334:1: ( (lv_ref_3_0= ruleOntologyID ) )
            // InternalOWLModel.g:1335:1: (lv_ref_3_0= ruleOntologyID )
            {
            // InternalOWLModel.g:1335:1: (lv_ref_3_0= ruleOntologyID )
            // InternalOWLModel.g:1336:3: lv_ref_3_0= ruleOntologyID
            {
             
            	        newCompositeNode(grammarAccess.getSeeAlsoAccess().getRefOntologyIDParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_29);
            lv_ref_3_0=ruleOntologyID();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSeeAlsoRule());
            	        }
                   		set(
                   			current, 
                   			"ref",
                    		lv_ref_3_0, 
                    		"org.smool.sdk.owlparser.OWLModel.OntologyID");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_4=(Token)match(input,29,FOLLOW_2); 

                	newLeafNode(otherlv_4, grammarAccess.getSeeAlsoAccess().getSolidusGreaterThanSignKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSeeAlso"


    // $ANTLR start "entryRulePriorVersion"
    // InternalOWLModel.g:1364:1: entryRulePriorVersion returns [EObject current=null] : iv_rulePriorVersion= rulePriorVersion EOF ;
    public final EObject entryRulePriorVersion() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePriorVersion = null;


        try {
            // InternalOWLModel.g:1365:2: (iv_rulePriorVersion= rulePriorVersion EOF )
            // InternalOWLModel.g:1366:2: iv_rulePriorVersion= rulePriorVersion EOF
            {
             newCompositeNode(grammarAccess.getPriorVersionRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePriorVersion=rulePriorVersion();

            state._fsp--;

             current =iv_rulePriorVersion; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePriorVersion"


    // $ANTLR start "rulePriorVersion"
    // InternalOWLModel.g:1373:1: rulePriorVersion returns [EObject current=null] : (otherlv_0= '<owl:priorVersion' otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOntologyID ) ) otherlv_4= '/>' ) ;
    public final EObject rulePriorVersion() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_ref_3_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:1376:28: ( (otherlv_0= '<owl:priorVersion' otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOntologyID ) ) otherlv_4= '/>' ) )
            // InternalOWLModel.g:1377:1: (otherlv_0= '<owl:priorVersion' otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOntologyID ) ) otherlv_4= '/>' )
            {
            // InternalOWLModel.g:1377:1: (otherlv_0= '<owl:priorVersion' otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOntologyID ) ) otherlv_4= '/>' )
            // InternalOWLModel.g:1377:3: otherlv_0= '<owl:priorVersion' otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOntologyID ) ) otherlv_4= '/>'
            {
            otherlv_0=(Token)match(input,35,FOLLOW_28); 

                	newLeafNode(otherlv_0, grammarAccess.getPriorVersionAccess().getOwlPriorVersionKeyword_0());
                
            otherlv_1=(Token)match(input,32,FOLLOW_13); 

                	newLeafNode(otherlv_1, grammarAccess.getPriorVersionAccess().getRdfResourceKeyword_1());
                
            otherlv_2=(Token)match(input,25,FOLLOW_9); 

                	newLeafNode(otherlv_2, grammarAccess.getPriorVersionAccess().getEqualsSignKeyword_2());
                
            // InternalOWLModel.g:1389:1: ( (lv_ref_3_0= ruleOntologyID ) )
            // InternalOWLModel.g:1390:1: (lv_ref_3_0= ruleOntologyID )
            {
            // InternalOWLModel.g:1390:1: (lv_ref_3_0= ruleOntologyID )
            // InternalOWLModel.g:1391:3: lv_ref_3_0= ruleOntologyID
            {
             
            	        newCompositeNode(grammarAccess.getPriorVersionAccess().getRefOntologyIDParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_29);
            lv_ref_3_0=ruleOntologyID();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getPriorVersionRule());
            	        }
                   		set(
                   			current, 
                   			"ref",
                    		lv_ref_3_0, 
                    		"org.smool.sdk.owlparser.OWLModel.OntologyID");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_4=(Token)match(input,29,FOLLOW_2); 

                	newLeafNode(otherlv_4, grammarAccess.getPriorVersionAccess().getSolidusGreaterThanSignKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePriorVersion"


    // $ANTLR start "entryRuleBackwardsComp"
    // InternalOWLModel.g:1419:1: entryRuleBackwardsComp returns [EObject current=null] : iv_ruleBackwardsComp= ruleBackwardsComp EOF ;
    public final EObject entryRuleBackwardsComp() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBackwardsComp = null;


        try {
            // InternalOWLModel.g:1420:2: (iv_ruleBackwardsComp= ruleBackwardsComp EOF )
            // InternalOWLModel.g:1421:2: iv_ruleBackwardsComp= ruleBackwardsComp EOF
            {
             newCompositeNode(grammarAccess.getBackwardsCompRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBackwardsComp=ruleBackwardsComp();

            state._fsp--;

             current =iv_ruleBackwardsComp; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBackwardsComp"


    // $ANTLR start "ruleBackwardsComp"
    // InternalOWLModel.g:1428:1: ruleBackwardsComp returns [EObject current=null] : (otherlv_0= '<owl:backwardCompatibleWith' otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOntologyID ) ) otherlv_4= '/>' ) ;
    public final EObject ruleBackwardsComp() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_ref_3_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:1431:28: ( (otherlv_0= '<owl:backwardCompatibleWith' otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOntologyID ) ) otherlv_4= '/>' ) )
            // InternalOWLModel.g:1432:1: (otherlv_0= '<owl:backwardCompatibleWith' otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOntologyID ) ) otherlv_4= '/>' )
            {
            // InternalOWLModel.g:1432:1: (otherlv_0= '<owl:backwardCompatibleWith' otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOntologyID ) ) otherlv_4= '/>' )
            // InternalOWLModel.g:1432:3: otherlv_0= '<owl:backwardCompatibleWith' otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOntologyID ) ) otherlv_4= '/>'
            {
            otherlv_0=(Token)match(input,36,FOLLOW_28); 

                	newLeafNode(otherlv_0, grammarAccess.getBackwardsCompAccess().getOwlBackwardCompatibleWithKeyword_0());
                
            otherlv_1=(Token)match(input,32,FOLLOW_13); 

                	newLeafNode(otherlv_1, grammarAccess.getBackwardsCompAccess().getRdfResourceKeyword_1());
                
            otherlv_2=(Token)match(input,25,FOLLOW_9); 

                	newLeafNode(otherlv_2, grammarAccess.getBackwardsCompAccess().getEqualsSignKeyword_2());
                
            // InternalOWLModel.g:1444:1: ( (lv_ref_3_0= ruleOntologyID ) )
            // InternalOWLModel.g:1445:1: (lv_ref_3_0= ruleOntologyID )
            {
            // InternalOWLModel.g:1445:1: (lv_ref_3_0= ruleOntologyID )
            // InternalOWLModel.g:1446:3: lv_ref_3_0= ruleOntologyID
            {
             
            	        newCompositeNode(grammarAccess.getBackwardsCompAccess().getRefOntologyIDParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_29);
            lv_ref_3_0=ruleOntologyID();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBackwardsCompRule());
            	        }
                   		set(
                   			current, 
                   			"ref",
                    		lv_ref_3_0, 
                    		"org.smool.sdk.owlparser.OWLModel.OntologyID");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_4=(Token)match(input,29,FOLLOW_2); 

                	newLeafNode(otherlv_4, grammarAccess.getBackwardsCompAccess().getSolidusGreaterThanSignKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBackwardsComp"


    // $ANTLR start "entryRuleOwlImport"
    // InternalOWLModel.g:1474:1: entryRuleOwlImport returns [EObject current=null] : iv_ruleOwlImport= ruleOwlImport EOF ;
    public final EObject entryRuleOwlImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOwlImport = null;


        try {
            // InternalOWLModel.g:1475:2: (iv_ruleOwlImport= ruleOwlImport EOF )
            // InternalOWLModel.g:1476:2: iv_ruleOwlImport= ruleOwlImport EOF
            {
             newCompositeNode(grammarAccess.getOwlImportRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOwlImport=ruleOwlImport();

            state._fsp--;

             current =iv_ruleOwlImport; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOwlImport"


    // $ANTLR start "ruleOwlImport"
    // InternalOWLModel.g:1483:1: ruleOwlImport returns [EObject current=null] : (otherlv_0= '<owl:imports' ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOntologyID ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:imports' otherlv_7= '>' ) ) ) | (otherlv_8= '>' ( (lv_onts_9_0= ruleOntology ) )+ otherlv_10= '</owl:imports' otherlv_11= '>' ) ) ) ;
    public final EObject ruleOwlImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        EObject lv_ref_3_0 = null;

        EObject lv_onts_9_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:1486:28: ( (otherlv_0= '<owl:imports' ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOntologyID ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:imports' otherlv_7= '>' ) ) ) | (otherlv_8= '>' ( (lv_onts_9_0= ruleOntology ) )+ otherlv_10= '</owl:imports' otherlv_11= '>' ) ) ) )
            // InternalOWLModel.g:1487:1: (otherlv_0= '<owl:imports' ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOntologyID ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:imports' otherlv_7= '>' ) ) ) | (otherlv_8= '>' ( (lv_onts_9_0= ruleOntology ) )+ otherlv_10= '</owl:imports' otherlv_11= '>' ) ) )
            {
            // InternalOWLModel.g:1487:1: (otherlv_0= '<owl:imports' ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOntologyID ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:imports' otherlv_7= '>' ) ) ) | (otherlv_8= '>' ( (lv_onts_9_0= ruleOntology ) )+ otherlv_10= '</owl:imports' otherlv_11= '>' ) ) )
            // InternalOWLModel.g:1487:3: otherlv_0= '<owl:imports' ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOntologyID ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:imports' otherlv_7= '>' ) ) ) | (otherlv_8= '>' ( (lv_onts_9_0= ruleOntology ) )+ otherlv_10= '</owl:imports' otherlv_11= '>' ) )
            {
            otherlv_0=(Token)match(input,37,FOLLOW_30); 

                	newLeafNode(otherlv_0, grammarAccess.getOwlImportAccess().getOwlImportsKeyword_0());
                
            // InternalOWLModel.g:1491:1: ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOntologyID ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:imports' otherlv_7= '>' ) ) ) | (otherlv_8= '>' ( (lv_onts_9_0= ruleOntology ) )+ otherlv_10= '</owl:imports' otherlv_11= '>' ) )
            int alt32=2;
            int LA32_0 = input.LA(1);

            if ( (LA32_0==32) ) {
                alt32=1;
            }
            else if ( (LA32_0==21) ) {
                alt32=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 32, 0, input);

                throw nvae;
            }
            switch (alt32) {
                case 1 :
                    // InternalOWLModel.g:1491:2: (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOntologyID ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:imports' otherlv_7= '>' ) ) )
                    {
                    // InternalOWLModel.g:1491:2: (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOntologyID ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:imports' otherlv_7= '>' ) ) )
                    // InternalOWLModel.g:1491:4: otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOntologyID ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:imports' otherlv_7= '>' ) )
                    {
                    otherlv_1=(Token)match(input,32,FOLLOW_13); 

                        	newLeafNode(otherlv_1, grammarAccess.getOwlImportAccess().getRdfResourceKeyword_1_0_0());
                        
                    otherlv_2=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_2, grammarAccess.getOwlImportAccess().getEqualsSignKeyword_1_0_1());
                        
                    // InternalOWLModel.g:1499:1: ( (lv_ref_3_0= ruleOntologyID ) )
                    // InternalOWLModel.g:1500:1: (lv_ref_3_0= ruleOntologyID )
                    {
                    // InternalOWLModel.g:1500:1: (lv_ref_3_0= ruleOntologyID )
                    // InternalOWLModel.g:1501:3: lv_ref_3_0= ruleOntologyID
                    {
                     
                    	        newCompositeNode(grammarAccess.getOwlImportAccess().getRefOntologyIDParserRuleCall_1_0_2_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_ref_3_0=ruleOntologyID();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getOwlImportRule());
                    	        }
                           		set(
                           			current, 
                           			"ref",
                            		lv_ref_3_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OntologyID");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // InternalOWLModel.g:1517:2: (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:imports' otherlv_7= '>' ) )
                    int alt30=2;
                    int LA30_0 = input.LA(1);

                    if ( (LA30_0==29) ) {
                        alt30=1;
                    }
                    else if ( (LA30_0==21) ) {
                        alt30=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 30, 0, input);

                        throw nvae;
                    }
                    switch (alt30) {
                        case 1 :
                            // InternalOWLModel.g:1517:4: otherlv_4= '/>'
                            {
                            otherlv_4=(Token)match(input,29,FOLLOW_2); 

                                	newLeafNode(otherlv_4, grammarAccess.getOwlImportAccess().getSolidusGreaterThanSignKeyword_1_0_3_0());
                                

                            }
                            break;
                        case 2 :
                            // InternalOWLModel.g:1522:6: (otherlv_5= '>' otherlv_6= '</owl:imports' otherlv_7= '>' )
                            {
                            // InternalOWLModel.g:1522:6: (otherlv_5= '>' otherlv_6= '</owl:imports' otherlv_7= '>' )
                            // InternalOWLModel.g:1522:8: otherlv_5= '>' otherlv_6= '</owl:imports' otherlv_7= '>'
                            {
                            otherlv_5=(Token)match(input,21,FOLLOW_31); 

                                	newLeafNode(otherlv_5, grammarAccess.getOwlImportAccess().getGreaterThanSignKeyword_1_0_3_1_0());
                                
                            otherlv_6=(Token)match(input,38,FOLLOW_7); 

                                	newLeafNode(otherlv_6, grammarAccess.getOwlImportAccess().getOwlImportsKeyword_1_0_3_1_1());
                                
                            otherlv_7=(Token)match(input,21,FOLLOW_2); 

                                	newLeafNode(otherlv_7, grammarAccess.getOwlImportAccess().getGreaterThanSignKeyword_1_0_3_1_2());
                                

                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:1535:6: (otherlv_8= '>' ( (lv_onts_9_0= ruleOntology ) )+ otherlv_10= '</owl:imports' otherlv_11= '>' )
                    {
                    // InternalOWLModel.g:1535:6: (otherlv_8= '>' ( (lv_onts_9_0= ruleOntology ) )+ otherlv_10= '</owl:imports' otherlv_11= '>' )
                    // InternalOWLModel.g:1535:8: otherlv_8= '>' ( (lv_onts_9_0= ruleOntology ) )+ otherlv_10= '</owl:imports' otherlv_11= '>'
                    {
                    otherlv_8=(Token)match(input,21,FOLLOW_32); 

                        	newLeafNode(otherlv_8, grammarAccess.getOwlImportAccess().getGreaterThanSignKeyword_1_1_0());
                        
                    // InternalOWLModel.g:1539:1: ( (lv_onts_9_0= ruleOntology ) )+
                    int cnt31=0;
                    loop31:
                    do {
                        int alt31=2;
                        int LA31_0 = input.LA(1);

                        if ( (LA31_0==31) ) {
                            alt31=1;
                        }


                        switch (alt31) {
                    	case 1 :
                    	    // InternalOWLModel.g:1540:1: (lv_onts_9_0= ruleOntology )
                    	    {
                    	    // InternalOWLModel.g:1540:1: (lv_onts_9_0= ruleOntology )
                    	    // InternalOWLModel.g:1541:3: lv_onts_9_0= ruleOntology
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getOwlImportAccess().getOntsOntologyParserRuleCall_1_1_1_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_33);
                    	    lv_onts_9_0=ruleOntology();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getOwlImportRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"onts",
                    	            		lv_onts_9_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.Ontology");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt31 >= 1 ) break loop31;
                                EarlyExitException eee =
                                    new EarlyExitException(31, input);
                                throw eee;
                        }
                        cnt31++;
                    } while (true);

                    otherlv_10=(Token)match(input,38,FOLLOW_7); 

                        	newLeafNode(otherlv_10, grammarAccess.getOwlImportAccess().getOwlImportsKeyword_1_1_2());
                        
                    otherlv_11=(Token)match(input,21,FOLLOW_2); 

                        	newLeafNode(otherlv_11, grammarAccess.getOwlImportAccess().getGreaterThanSignKeyword_1_1_3());
                        

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOwlImport"


    // $ANTLR start "entryRuleOwlClass"
    // InternalOWLModel.g:1573:1: entryRuleOwlClass returns [EObject current=null] : iv_ruleOwlClass= ruleOwlClass EOF ;
    public final EObject entryRuleOwlClass() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOwlClass = null;


        try {
            // InternalOWLModel.g:1574:2: (iv_ruleOwlClass= ruleOwlClass EOF )
            // InternalOWLModel.g:1575:2: iv_ruleOwlClass= ruleOwlClass EOF
            {
             newCompositeNode(grammarAccess.getOwlClassRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOwlClass=ruleOwlClass();

            state._fsp--;

             current =iv_ruleOwlClass; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOwlClass"


    // $ANTLR start "ruleOwlClass"
    // InternalOWLModel.g:1582:1: ruleOwlClass returns [EObject current=null] : ( () otherlv_1= '<owl:Class' ( (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) ) | ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) ) ) (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_superTypes_12_0= ruleSubTypeOf ) ) | ( (lv_axioms_13_0= ruleClassAxiom ) ) | ( (lv_onts_14_0= ruleOntology ) ) | ( (lv_others_15_0= ruleANY_OTHER_TAG ) ) )* otherlv_16= '</owl:Class' otherlv_17= '>' ) ) ) ;
    public final EObject ruleOwlClass() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token lv_reference_5_0=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        EObject lv_id_4_0 = null;

        EObject lv_ref_7_0 = null;

        EObject lv_labels_10_0 = null;

        EObject lv_versionInfo_11_0 = null;

        EObject lv_superTypes_12_0 = null;

        EObject lv_axioms_13_0 = null;

        EObject lv_onts_14_0 = null;

        AntlrDatatypeRuleToken lv_others_15_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:1585:28: ( ( () otherlv_1= '<owl:Class' ( (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) ) | ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) ) ) (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_superTypes_12_0= ruleSubTypeOf ) ) | ( (lv_axioms_13_0= ruleClassAxiom ) ) | ( (lv_onts_14_0= ruleOntology ) ) | ( (lv_others_15_0= ruleANY_OTHER_TAG ) ) )* otherlv_16= '</owl:Class' otherlv_17= '>' ) ) ) )
            // InternalOWLModel.g:1586:1: ( () otherlv_1= '<owl:Class' ( (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) ) | ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) ) ) (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_superTypes_12_0= ruleSubTypeOf ) ) | ( (lv_axioms_13_0= ruleClassAxiom ) ) | ( (lv_onts_14_0= ruleOntology ) ) | ( (lv_others_15_0= ruleANY_OTHER_TAG ) ) )* otherlv_16= '</owl:Class' otherlv_17= '>' ) ) )
            {
            // InternalOWLModel.g:1586:1: ( () otherlv_1= '<owl:Class' ( (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) ) | ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) ) ) (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_superTypes_12_0= ruleSubTypeOf ) ) | ( (lv_axioms_13_0= ruleClassAxiom ) ) | ( (lv_onts_14_0= ruleOntology ) ) | ( (lv_others_15_0= ruleANY_OTHER_TAG ) ) )* otherlv_16= '</owl:Class' otherlv_17= '>' ) ) )
            // InternalOWLModel.g:1586:2: () otherlv_1= '<owl:Class' ( (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) ) | ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) ) ) (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_superTypes_12_0= ruleSubTypeOf ) ) | ( (lv_axioms_13_0= ruleClassAxiom ) ) | ( (lv_onts_14_0= ruleOntology ) ) | ( (lv_others_15_0= ruleANY_OTHER_TAG ) ) )* otherlv_16= '</owl:Class' otherlv_17= '>' ) )
            {
            // InternalOWLModel.g:1586:2: ()
            // InternalOWLModel.g:1587:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getOwlClassAccess().getOwlClassAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,39,FOLLOW_34); 

                	newLeafNode(otherlv_1, grammarAccess.getOwlClassAccess().getOwlClassKeyword_1());
                
            // InternalOWLModel.g:1596:1: ( (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) ) | ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) ) )
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==40) ) {
                alt33=1;
            }
            else if ( (LA33_0==28) ) {
                alt33=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 33, 0, input);

                throw nvae;
            }
            switch (alt33) {
                case 1 :
                    // InternalOWLModel.g:1596:2: (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) )
                    {
                    // InternalOWLModel.g:1596:2: (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) )
                    // InternalOWLModel.g:1596:4: otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) )
                    {
                    otherlv_2=(Token)match(input,40,FOLLOW_13); 

                        	newLeafNode(otherlv_2, grammarAccess.getOwlClassAccess().getRdfIDKeyword_2_0_0());
                        
                    otherlv_3=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_3, grammarAccess.getOwlClassAccess().getEqualsSignKeyword_2_0_1());
                        
                    // InternalOWLModel.g:1604:1: ( (lv_id_4_0= ruleOwlID ) )
                    // InternalOWLModel.g:1605:1: (lv_id_4_0= ruleOwlID )
                    {
                    // InternalOWLModel.g:1605:1: (lv_id_4_0= ruleOwlID )
                    // InternalOWLModel.g:1606:3: lv_id_4_0= ruleOwlID
                    {
                     
                    	        newCompositeNode(grammarAccess.getOwlClassAccess().getIdOwlIDParserRuleCall_2_0_2_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_id_4_0=ruleOwlID();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getOwlClassRule());
                    	        }
                           		set(
                           			current, 
                           			"id",
                            		lv_id_4_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlID");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:1623:6: ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) )
                    {
                    // InternalOWLModel.g:1623:6: ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) )
                    // InternalOWLModel.g:1623:7: ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) )
                    {
                    // InternalOWLModel.g:1623:7: ( (lv_reference_5_0= 'rdf:about' ) )
                    // InternalOWLModel.g:1624:1: (lv_reference_5_0= 'rdf:about' )
                    {
                    // InternalOWLModel.g:1624:1: (lv_reference_5_0= 'rdf:about' )
                    // InternalOWLModel.g:1625:3: lv_reference_5_0= 'rdf:about'
                    {
                    lv_reference_5_0=(Token)match(input,28,FOLLOW_13); 

                            newLeafNode(lv_reference_5_0, grammarAccess.getOwlClassAccess().getReferenceRdfAboutKeyword_2_1_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getOwlClassRule());
                    	        }
                           		setWithLastConsumed(current, "reference", true, "rdf:about");
                    	    

                    }


                    }

                    otherlv_6=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_6, grammarAccess.getOwlClassAccess().getEqualsSignKeyword_2_1_1());
                        
                    // InternalOWLModel.g:1642:1: ( (lv_ref_7_0= ruleOwlRef ) )
                    // InternalOWLModel.g:1643:1: (lv_ref_7_0= ruleOwlRef )
                    {
                    // InternalOWLModel.g:1643:1: (lv_ref_7_0= ruleOwlRef )
                    // InternalOWLModel.g:1644:3: lv_ref_7_0= ruleOwlRef
                    {
                     
                    	        newCompositeNode(grammarAccess.getOwlClassAccess().getRefOwlRefParserRuleCall_2_1_2_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_ref_7_0=ruleOwlRef();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getOwlClassRule());
                    	        }
                           		set(
                           			current, 
                           			"ref",
                            		lv_ref_7_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlRef");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;

            }

            // InternalOWLModel.g:1660:4: (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_superTypes_12_0= ruleSubTypeOf ) ) | ( (lv_axioms_13_0= ruleClassAxiom ) ) | ( (lv_onts_14_0= ruleOntology ) ) | ( (lv_others_15_0= ruleANY_OTHER_TAG ) ) )* otherlv_16= '</owl:Class' otherlv_17= '>' ) )
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==29) ) {
                alt35=1;
            }
            else if ( (LA35_0==21) ) {
                alt35=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 35, 0, input);

                throw nvae;
            }
            switch (alt35) {
                case 1 :
                    // InternalOWLModel.g:1660:6: otherlv_8= '/>'
                    {
                    otherlv_8=(Token)match(input,29,FOLLOW_2); 

                        	newLeafNode(otherlv_8, grammarAccess.getOwlClassAccess().getSolidusGreaterThanSignKeyword_3_0());
                        

                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:1665:6: (otherlv_9= '>' ( ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_superTypes_12_0= ruleSubTypeOf ) ) | ( (lv_axioms_13_0= ruleClassAxiom ) ) | ( (lv_onts_14_0= ruleOntology ) ) | ( (lv_others_15_0= ruleANY_OTHER_TAG ) ) )* otherlv_16= '</owl:Class' otherlv_17= '>' )
                    {
                    // InternalOWLModel.g:1665:6: (otherlv_9= '>' ( ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_superTypes_12_0= ruleSubTypeOf ) ) | ( (lv_axioms_13_0= ruleClassAxiom ) ) | ( (lv_onts_14_0= ruleOntology ) ) | ( (lv_others_15_0= ruleANY_OTHER_TAG ) ) )* otherlv_16= '</owl:Class' otherlv_17= '>' )
                    // InternalOWLModel.g:1665:8: otherlv_9= '>' ( ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_superTypes_12_0= ruleSubTypeOf ) ) | ( (lv_axioms_13_0= ruleClassAxiom ) ) | ( (lv_onts_14_0= ruleOntology ) ) | ( (lv_others_15_0= ruleANY_OTHER_TAG ) ) )* otherlv_16= '</owl:Class' otherlv_17= '>'
                    {
                    otherlv_9=(Token)match(input,21,FOLLOW_35); 

                        	newLeafNode(otherlv_9, grammarAccess.getOwlClassAccess().getGreaterThanSignKeyword_3_1_0());
                        
                    // InternalOWLModel.g:1669:1: ( ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_superTypes_12_0= ruleSubTypeOf ) ) | ( (lv_axioms_13_0= ruleClassAxiom ) ) | ( (lv_onts_14_0= ruleOntology ) ) | ( (lv_others_15_0= ruleANY_OTHER_TAG ) ) )*
                    loop34:
                    do {
                        int alt34=7;
                        switch ( input.LA(1) ) {
                        case 60:
                            {
                            alt34=1;
                            }
                            break;
                        case 64:
                            {
                            alt34=2;
                            }
                            break;
                        case 66:
                        case 68:
                            {
                            alt34=3;
                            }
                            break;
                        case 42:
                        case 45:
                        case 47:
                        case 49:
                        case 51:
                        case 53:
                        case 55:
                            {
                            alt34=4;
                            }
                            break;
                        case 31:
                            {
                            alt34=5;
                            }
                            break;
                        case 58:
                            {
                            alt34=6;
                            }
                            break;

                        }

                        switch (alt34) {
                    	case 1 :
                    	    // InternalOWLModel.g:1669:2: ( (lv_labels_10_0= ruleOwlLabel ) )
                    	    {
                    	    // InternalOWLModel.g:1669:2: ( (lv_labels_10_0= ruleOwlLabel ) )
                    	    // InternalOWLModel.g:1670:1: (lv_labels_10_0= ruleOwlLabel )
                    	    {
                    	    // InternalOWLModel.g:1670:1: (lv_labels_10_0= ruleOwlLabel )
                    	    // InternalOWLModel.g:1671:3: lv_labels_10_0= ruleOwlLabel
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getOwlClassAccess().getLabelsOwlLabelParserRuleCall_3_1_1_0_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_35);
                    	    lv_labels_10_0=ruleOwlLabel();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getOwlClassRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"labels",
                    	            		lv_labels_10_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.OwlLabel");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalOWLModel.g:1688:6: ( (lv_versionInfo_11_0= ruleOwlVersion ) )
                    	    {
                    	    // InternalOWLModel.g:1688:6: ( (lv_versionInfo_11_0= ruleOwlVersion ) )
                    	    // InternalOWLModel.g:1689:1: (lv_versionInfo_11_0= ruleOwlVersion )
                    	    {
                    	    // InternalOWLModel.g:1689:1: (lv_versionInfo_11_0= ruleOwlVersion )
                    	    // InternalOWLModel.g:1690:3: lv_versionInfo_11_0= ruleOwlVersion
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getOwlClassAccess().getVersionInfoOwlVersionParserRuleCall_3_1_1_1_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_35);
                    	    lv_versionInfo_11_0=ruleOwlVersion();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getOwlClassRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"versionInfo",
                    	            		lv_versionInfo_11_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.OwlVersion");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 3 :
                    	    // InternalOWLModel.g:1707:6: ( (lv_superTypes_12_0= ruleSubTypeOf ) )
                    	    {
                    	    // InternalOWLModel.g:1707:6: ( (lv_superTypes_12_0= ruleSubTypeOf ) )
                    	    // InternalOWLModel.g:1708:1: (lv_superTypes_12_0= ruleSubTypeOf )
                    	    {
                    	    // InternalOWLModel.g:1708:1: (lv_superTypes_12_0= ruleSubTypeOf )
                    	    // InternalOWLModel.g:1709:3: lv_superTypes_12_0= ruleSubTypeOf
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getOwlClassAccess().getSuperTypesSubTypeOfParserRuleCall_3_1_1_2_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_35);
                    	    lv_superTypes_12_0=ruleSubTypeOf();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getOwlClassRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"superTypes",
                    	            		lv_superTypes_12_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.SubTypeOf");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 4 :
                    	    // InternalOWLModel.g:1726:6: ( (lv_axioms_13_0= ruleClassAxiom ) )
                    	    {
                    	    // InternalOWLModel.g:1726:6: ( (lv_axioms_13_0= ruleClassAxiom ) )
                    	    // InternalOWLModel.g:1727:1: (lv_axioms_13_0= ruleClassAxiom )
                    	    {
                    	    // InternalOWLModel.g:1727:1: (lv_axioms_13_0= ruleClassAxiom )
                    	    // InternalOWLModel.g:1728:3: lv_axioms_13_0= ruleClassAxiom
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getOwlClassAccess().getAxiomsClassAxiomParserRuleCall_3_1_1_3_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_35);
                    	    lv_axioms_13_0=ruleClassAxiom();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getOwlClassRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"axioms",
                    	            		lv_axioms_13_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.ClassAxiom");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 5 :
                    	    // InternalOWLModel.g:1745:6: ( (lv_onts_14_0= ruleOntology ) )
                    	    {
                    	    // InternalOWLModel.g:1745:6: ( (lv_onts_14_0= ruleOntology ) )
                    	    // InternalOWLModel.g:1746:1: (lv_onts_14_0= ruleOntology )
                    	    {
                    	    // InternalOWLModel.g:1746:1: (lv_onts_14_0= ruleOntology )
                    	    // InternalOWLModel.g:1747:3: lv_onts_14_0= ruleOntology
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getOwlClassAccess().getOntsOntologyParserRuleCall_3_1_1_4_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_35);
                    	    lv_onts_14_0=ruleOntology();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getOwlClassRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"onts",
                    	            		lv_onts_14_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.Ontology");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 6 :
                    	    // InternalOWLModel.g:1764:6: ( (lv_others_15_0= ruleANY_OTHER_TAG ) )
                    	    {
                    	    // InternalOWLModel.g:1764:6: ( (lv_others_15_0= ruleANY_OTHER_TAG ) )
                    	    // InternalOWLModel.g:1765:1: (lv_others_15_0= ruleANY_OTHER_TAG )
                    	    {
                    	    // InternalOWLModel.g:1765:1: (lv_others_15_0= ruleANY_OTHER_TAG )
                    	    // InternalOWLModel.g:1766:3: lv_others_15_0= ruleANY_OTHER_TAG
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getOwlClassAccess().getOthersANY_OTHER_TAGParserRuleCall_3_1_1_5_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_35);
                    	    lv_others_15_0=ruleANY_OTHER_TAG();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getOwlClassRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"others",
                    	            		lv_others_15_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.ANY_OTHER_TAG");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop34;
                        }
                    } while (true);

                    otherlv_16=(Token)match(input,41,FOLLOW_7); 

                        	newLeafNode(otherlv_16, grammarAccess.getOwlClassAccess().getOwlClassKeyword_3_1_2());
                        
                    otherlv_17=(Token)match(input,21,FOLLOW_2); 

                        	newLeafNode(otherlv_17, grammarAccess.getOwlClassAccess().getGreaterThanSignKeyword_3_1_3());
                        

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOwlClass"


    // $ANTLR start "entryRuleMinimumClass"
    // InternalOWLModel.g:1798:1: entryRuleMinimumClass returns [EObject current=null] : iv_ruleMinimumClass= ruleMinimumClass EOF ;
    public final EObject entryRuleMinimumClass() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMinimumClass = null;


        try {
            // InternalOWLModel.g:1799:2: (iv_ruleMinimumClass= ruleMinimumClass EOF )
            // InternalOWLModel.g:1800:2: iv_ruleMinimumClass= ruleMinimumClass EOF
            {
             newCompositeNode(grammarAccess.getMinimumClassRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleMinimumClass=ruleMinimumClass();

            state._fsp--;

             current =iv_ruleMinimumClass; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMinimumClass"


    // $ANTLR start "ruleMinimumClass"
    // InternalOWLModel.g:1807:1: ruleMinimumClass returns [EObject current=null] : ( () otherlv_1= '<owl:Class' otherlv_2= '>' ( ( (lv_labels_3_0= ruleOwlLabel ) ) | ( (lv_axioms_4_0= ruleClassAxiom ) ) | ( (lv_others_5_0= ruleANY_OTHER_TAG ) ) )* otherlv_6= '</owl:Class' otherlv_7= '>' ) ;
    public final EObject ruleMinimumClass() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        EObject lv_labels_3_0 = null;

        EObject lv_axioms_4_0 = null;

        AntlrDatatypeRuleToken lv_others_5_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:1810:28: ( ( () otherlv_1= '<owl:Class' otherlv_2= '>' ( ( (lv_labels_3_0= ruleOwlLabel ) ) | ( (lv_axioms_4_0= ruleClassAxiom ) ) | ( (lv_others_5_0= ruleANY_OTHER_TAG ) ) )* otherlv_6= '</owl:Class' otherlv_7= '>' ) )
            // InternalOWLModel.g:1811:1: ( () otherlv_1= '<owl:Class' otherlv_2= '>' ( ( (lv_labels_3_0= ruleOwlLabel ) ) | ( (lv_axioms_4_0= ruleClassAxiom ) ) | ( (lv_others_5_0= ruleANY_OTHER_TAG ) ) )* otherlv_6= '</owl:Class' otherlv_7= '>' )
            {
            // InternalOWLModel.g:1811:1: ( () otherlv_1= '<owl:Class' otherlv_2= '>' ( ( (lv_labels_3_0= ruleOwlLabel ) ) | ( (lv_axioms_4_0= ruleClassAxiom ) ) | ( (lv_others_5_0= ruleANY_OTHER_TAG ) ) )* otherlv_6= '</owl:Class' otherlv_7= '>' )
            // InternalOWLModel.g:1811:2: () otherlv_1= '<owl:Class' otherlv_2= '>' ( ( (lv_labels_3_0= ruleOwlLabel ) ) | ( (lv_axioms_4_0= ruleClassAxiom ) ) | ( (lv_others_5_0= ruleANY_OTHER_TAG ) ) )* otherlv_6= '</owl:Class' otherlv_7= '>'
            {
            // InternalOWLModel.g:1811:2: ()
            // InternalOWLModel.g:1812:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getMinimumClassAccess().getMinimumClassAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,39,FOLLOW_7); 

                	newLeafNode(otherlv_1, grammarAccess.getMinimumClassAccess().getOwlClassKeyword_1());
                
            otherlv_2=(Token)match(input,21,FOLLOW_36); 

                	newLeafNode(otherlv_2, grammarAccess.getMinimumClassAccess().getGreaterThanSignKeyword_2());
                
            // InternalOWLModel.g:1825:1: ( ( (lv_labels_3_0= ruleOwlLabel ) ) | ( (lv_axioms_4_0= ruleClassAxiom ) ) | ( (lv_others_5_0= ruleANY_OTHER_TAG ) ) )*
            loop36:
            do {
                int alt36=4;
                switch ( input.LA(1) ) {
                case 60:
                    {
                    alt36=1;
                    }
                    break;
                case 42:
                case 45:
                case 47:
                case 49:
                case 51:
                case 53:
                case 55:
                    {
                    alt36=2;
                    }
                    break;
                case 58:
                    {
                    alt36=3;
                    }
                    break;

                }

                switch (alt36) {
            	case 1 :
            	    // InternalOWLModel.g:1825:2: ( (lv_labels_3_0= ruleOwlLabel ) )
            	    {
            	    // InternalOWLModel.g:1825:2: ( (lv_labels_3_0= ruleOwlLabel ) )
            	    // InternalOWLModel.g:1826:1: (lv_labels_3_0= ruleOwlLabel )
            	    {
            	    // InternalOWLModel.g:1826:1: (lv_labels_3_0= ruleOwlLabel )
            	    // InternalOWLModel.g:1827:3: lv_labels_3_0= ruleOwlLabel
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getMinimumClassAccess().getLabelsOwlLabelParserRuleCall_3_0_0()); 
            	    	    
            	    pushFollow(FOLLOW_36);
            	    lv_labels_3_0=ruleOwlLabel();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getMinimumClassRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"labels",
            	            		lv_labels_3_0, 
            	            		"org.smool.sdk.owlparser.OWLModel.OwlLabel");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalOWLModel.g:1844:6: ( (lv_axioms_4_0= ruleClassAxiom ) )
            	    {
            	    // InternalOWLModel.g:1844:6: ( (lv_axioms_4_0= ruleClassAxiom ) )
            	    // InternalOWLModel.g:1845:1: (lv_axioms_4_0= ruleClassAxiom )
            	    {
            	    // InternalOWLModel.g:1845:1: (lv_axioms_4_0= ruleClassAxiom )
            	    // InternalOWLModel.g:1846:3: lv_axioms_4_0= ruleClassAxiom
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getMinimumClassAccess().getAxiomsClassAxiomParserRuleCall_3_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_36);
            	    lv_axioms_4_0=ruleClassAxiom();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getMinimumClassRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"axioms",
            	            		lv_axioms_4_0, 
            	            		"org.smool.sdk.owlparser.OWLModel.ClassAxiom");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // InternalOWLModel.g:1863:6: ( (lv_others_5_0= ruleANY_OTHER_TAG ) )
            	    {
            	    // InternalOWLModel.g:1863:6: ( (lv_others_5_0= ruleANY_OTHER_TAG ) )
            	    // InternalOWLModel.g:1864:1: (lv_others_5_0= ruleANY_OTHER_TAG )
            	    {
            	    // InternalOWLModel.g:1864:1: (lv_others_5_0= ruleANY_OTHER_TAG )
            	    // InternalOWLModel.g:1865:3: lv_others_5_0= ruleANY_OTHER_TAG
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getMinimumClassAccess().getOthersANY_OTHER_TAGParserRuleCall_3_2_0()); 
            	    	    
            	    pushFollow(FOLLOW_36);
            	    lv_others_5_0=ruleANY_OTHER_TAG();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getMinimumClassRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"others",
            	            		lv_others_5_0, 
            	            		"org.smool.sdk.owlparser.OWLModel.ANY_OTHER_TAG");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop36;
                }
            } while (true);

            otherlv_6=(Token)match(input,41,FOLLOW_7); 

                	newLeafNode(otherlv_6, grammarAccess.getMinimumClassAccess().getOwlClassKeyword_4());
                
            otherlv_7=(Token)match(input,21,FOLLOW_2); 

                	newLeafNode(otherlv_7, grammarAccess.getMinimumClassAccess().getGreaterThanSignKeyword_5());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMinimumClass"


    // $ANTLR start "entryRuleClassAxiom"
    // InternalOWLModel.g:1897:1: entryRuleClassAxiom returns [EObject current=null] : iv_ruleClassAxiom= ruleClassAxiom EOF ;
    public final EObject entryRuleClassAxiom() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClassAxiom = null;


        try {
            // InternalOWLModel.g:1898:2: (iv_ruleClassAxiom= ruleClassAxiom EOF )
            // InternalOWLModel.g:1899:2: iv_ruleClassAxiom= ruleClassAxiom EOF
            {
             newCompositeNode(grammarAccess.getClassAxiomRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleClassAxiom=ruleClassAxiom();

            state._fsp--;

             current =iv_ruleClassAxiom; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClassAxiom"


    // $ANTLR start "ruleClassAxiom"
    // InternalOWLModel.g:1906:1: ruleClassAxiom returns [EObject current=null] : (this_DisjointAxiom_0= ruleDisjointAxiom | this_EquivalentAxiom_1= ruleEquivalentAxiom | this_ComplementAxiom_2= ruleComplementAxiom | this_UnionAxiom_3= ruleUnionAxiom | this_IntersectionAxiom_4= ruleIntersectionAxiom | this_IncompatibleWithAxiom_5= ruleIncompatibleWithAxiom | this_OneOfAxiom_6= ruleOneOfAxiom ) ;
    public final EObject ruleClassAxiom() throws RecognitionException {
        EObject current = null;

        EObject this_DisjointAxiom_0 = null;

        EObject this_EquivalentAxiom_1 = null;

        EObject this_ComplementAxiom_2 = null;

        EObject this_UnionAxiom_3 = null;

        EObject this_IntersectionAxiom_4 = null;

        EObject this_IncompatibleWithAxiom_5 = null;

        EObject this_OneOfAxiom_6 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:1909:28: ( (this_DisjointAxiom_0= ruleDisjointAxiom | this_EquivalentAxiom_1= ruleEquivalentAxiom | this_ComplementAxiom_2= ruleComplementAxiom | this_UnionAxiom_3= ruleUnionAxiom | this_IntersectionAxiom_4= ruleIntersectionAxiom | this_IncompatibleWithAxiom_5= ruleIncompatibleWithAxiom | this_OneOfAxiom_6= ruleOneOfAxiom ) )
            // InternalOWLModel.g:1910:1: (this_DisjointAxiom_0= ruleDisjointAxiom | this_EquivalentAxiom_1= ruleEquivalentAxiom | this_ComplementAxiom_2= ruleComplementAxiom | this_UnionAxiom_3= ruleUnionAxiom | this_IntersectionAxiom_4= ruleIntersectionAxiom | this_IncompatibleWithAxiom_5= ruleIncompatibleWithAxiom | this_OneOfAxiom_6= ruleOneOfAxiom )
            {
            // InternalOWLModel.g:1910:1: (this_DisjointAxiom_0= ruleDisjointAxiom | this_EquivalentAxiom_1= ruleEquivalentAxiom | this_ComplementAxiom_2= ruleComplementAxiom | this_UnionAxiom_3= ruleUnionAxiom | this_IntersectionAxiom_4= ruleIntersectionAxiom | this_IncompatibleWithAxiom_5= ruleIncompatibleWithAxiom | this_OneOfAxiom_6= ruleOneOfAxiom )
            int alt37=7;
            switch ( input.LA(1) ) {
            case 47:
                {
                alt37=1;
                }
                break;
            case 49:
                {
                alt37=2;
                }
                break;
            case 42:
                {
                alt37=3;
                }
                break;
            case 51:
                {
                alt37=4;
                }
                break;
            case 53:
                {
                alt37=5;
                }
                break;
            case 45:
                {
                alt37=6;
                }
                break;
            case 55:
                {
                alt37=7;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 37, 0, input);

                throw nvae;
            }

            switch (alt37) {
                case 1 :
                    // InternalOWLModel.g:1911:5: this_DisjointAxiom_0= ruleDisjointAxiom
                    {
                     
                            newCompositeNode(grammarAccess.getClassAxiomAccess().getDisjointAxiomParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_2);
                    this_DisjointAxiom_0=ruleDisjointAxiom();

                    state._fsp--;

                     
                            current = this_DisjointAxiom_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:1921:5: this_EquivalentAxiom_1= ruleEquivalentAxiom
                    {
                     
                            newCompositeNode(grammarAccess.getClassAxiomAccess().getEquivalentAxiomParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_2);
                    this_EquivalentAxiom_1=ruleEquivalentAxiom();

                    state._fsp--;

                     
                            current = this_EquivalentAxiom_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // InternalOWLModel.g:1931:5: this_ComplementAxiom_2= ruleComplementAxiom
                    {
                     
                            newCompositeNode(grammarAccess.getClassAxiomAccess().getComplementAxiomParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_2);
                    this_ComplementAxiom_2=ruleComplementAxiom();

                    state._fsp--;

                     
                            current = this_ComplementAxiom_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // InternalOWLModel.g:1941:5: this_UnionAxiom_3= ruleUnionAxiom
                    {
                     
                            newCompositeNode(grammarAccess.getClassAxiomAccess().getUnionAxiomParserRuleCall_3()); 
                        
                    pushFollow(FOLLOW_2);
                    this_UnionAxiom_3=ruleUnionAxiom();

                    state._fsp--;

                     
                            current = this_UnionAxiom_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 5 :
                    // InternalOWLModel.g:1951:5: this_IntersectionAxiom_4= ruleIntersectionAxiom
                    {
                     
                            newCompositeNode(grammarAccess.getClassAxiomAccess().getIntersectionAxiomParserRuleCall_4()); 
                        
                    pushFollow(FOLLOW_2);
                    this_IntersectionAxiom_4=ruleIntersectionAxiom();

                    state._fsp--;

                     
                            current = this_IntersectionAxiom_4; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 6 :
                    // InternalOWLModel.g:1961:5: this_IncompatibleWithAxiom_5= ruleIncompatibleWithAxiom
                    {
                     
                            newCompositeNode(grammarAccess.getClassAxiomAccess().getIncompatibleWithAxiomParserRuleCall_5()); 
                        
                    pushFollow(FOLLOW_2);
                    this_IncompatibleWithAxiom_5=ruleIncompatibleWithAxiom();

                    state._fsp--;

                     
                            current = this_IncompatibleWithAxiom_5; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 7 :
                    // InternalOWLModel.g:1971:5: this_OneOfAxiom_6= ruleOneOfAxiom
                    {
                     
                            newCompositeNode(grammarAccess.getClassAxiomAccess().getOneOfAxiomParserRuleCall_6()); 
                        
                    pushFollow(FOLLOW_2);
                    this_OneOfAxiom_6=ruleOneOfAxiom();

                    state._fsp--;

                     
                            current = this_OneOfAxiom_6; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClassAxiom"


    // $ANTLR start "entryRuleComplementAxiom"
    // InternalOWLModel.g:1987:1: entryRuleComplementAxiom returns [EObject current=null] : iv_ruleComplementAxiom= ruleComplementAxiom EOF ;
    public final EObject entryRuleComplementAxiom() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComplementAxiom = null;


        try {
            // InternalOWLModel.g:1988:2: (iv_ruleComplementAxiom= ruleComplementAxiom EOF )
            // InternalOWLModel.g:1989:2: iv_ruleComplementAxiom= ruleComplementAxiom EOF
            {
             newCompositeNode(grammarAccess.getComplementAxiomRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleComplementAxiom=ruleComplementAxiom();

            state._fsp--;

             current =iv_ruleComplementAxiom; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComplementAxiom"


    // $ANTLR start "ruleComplementAxiom"
    // InternalOWLModel.g:1996:1: ruleComplementAxiom returns [EObject current=null] : (otherlv_0= '<owl:complementOf' ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:complementOf' otherlv_7= '>' ) ) ) | ( (otherlv_8= 'rdf:parseType' otherlv_9= '=' ( (lv_parseType_10_0= ruleParseType ) ) )? otherlv_11= '>' ( (lv_classes_12_0= ruleClassModifier ) )+ otherlv_13= '</owl:complementOf' otherlv_14= '>' ) ) ) ;
    public final EObject ruleComplementAxiom() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        EObject lv_ref_3_0 = null;

        EObject lv_parseType_10_0 = null;

        EObject lv_classes_12_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:1999:28: ( (otherlv_0= '<owl:complementOf' ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:complementOf' otherlv_7= '>' ) ) ) | ( (otherlv_8= 'rdf:parseType' otherlv_9= '=' ( (lv_parseType_10_0= ruleParseType ) ) )? otherlv_11= '>' ( (lv_classes_12_0= ruleClassModifier ) )+ otherlv_13= '</owl:complementOf' otherlv_14= '>' ) ) ) )
            // InternalOWLModel.g:2000:1: (otherlv_0= '<owl:complementOf' ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:complementOf' otherlv_7= '>' ) ) ) | ( (otherlv_8= 'rdf:parseType' otherlv_9= '=' ( (lv_parseType_10_0= ruleParseType ) ) )? otherlv_11= '>' ( (lv_classes_12_0= ruleClassModifier ) )+ otherlv_13= '</owl:complementOf' otherlv_14= '>' ) ) )
            {
            // InternalOWLModel.g:2000:1: (otherlv_0= '<owl:complementOf' ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:complementOf' otherlv_7= '>' ) ) ) | ( (otherlv_8= 'rdf:parseType' otherlv_9= '=' ( (lv_parseType_10_0= ruleParseType ) ) )? otherlv_11= '>' ( (lv_classes_12_0= ruleClassModifier ) )+ otherlv_13= '</owl:complementOf' otherlv_14= '>' ) ) )
            // InternalOWLModel.g:2000:3: otherlv_0= '<owl:complementOf' ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:complementOf' otherlv_7= '>' ) ) ) | ( (otherlv_8= 'rdf:parseType' otherlv_9= '=' ( (lv_parseType_10_0= ruleParseType ) ) )? otherlv_11= '>' ( (lv_classes_12_0= ruleClassModifier ) )+ otherlv_13= '</owl:complementOf' otherlv_14= '>' ) )
            {
            otherlv_0=(Token)match(input,42,FOLLOW_37); 

                	newLeafNode(otherlv_0, grammarAccess.getComplementAxiomAccess().getOwlComplementOfKeyword_0());
                
            // InternalOWLModel.g:2004:1: ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:complementOf' otherlv_7= '>' ) ) ) | ( (otherlv_8= 'rdf:parseType' otherlv_9= '=' ( (lv_parseType_10_0= ruleParseType ) ) )? otherlv_11= '>' ( (lv_classes_12_0= ruleClassModifier ) )+ otherlv_13= '</owl:complementOf' otherlv_14= '>' ) )
            int alt41=2;
            int LA41_0 = input.LA(1);

            if ( (LA41_0==32) ) {
                alt41=1;
            }
            else if ( (LA41_0==21||LA41_0==44) ) {
                alt41=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 41, 0, input);

                throw nvae;
            }
            switch (alt41) {
                case 1 :
                    // InternalOWLModel.g:2004:2: (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:complementOf' otherlv_7= '>' ) ) )
                    {
                    // InternalOWLModel.g:2004:2: (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:complementOf' otherlv_7= '>' ) ) )
                    // InternalOWLModel.g:2004:4: otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:complementOf' otherlv_7= '>' ) )
                    {
                    otherlv_1=(Token)match(input,32,FOLLOW_13); 

                        	newLeafNode(otherlv_1, grammarAccess.getComplementAxiomAccess().getRdfResourceKeyword_1_0_0());
                        
                    otherlv_2=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_2, grammarAccess.getComplementAxiomAccess().getEqualsSignKeyword_1_0_1());
                        
                    // InternalOWLModel.g:2012:1: ( (lv_ref_3_0= ruleOwlRef ) )
                    // InternalOWLModel.g:2013:1: (lv_ref_3_0= ruleOwlRef )
                    {
                    // InternalOWLModel.g:2013:1: (lv_ref_3_0= ruleOwlRef )
                    // InternalOWLModel.g:2014:3: lv_ref_3_0= ruleOwlRef
                    {
                     
                    	        newCompositeNode(grammarAccess.getComplementAxiomAccess().getRefOwlRefParserRuleCall_1_0_2_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_ref_3_0=ruleOwlRef();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getComplementAxiomRule());
                    	        }
                           		set(
                           			current, 
                           			"ref",
                            		lv_ref_3_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlRef");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // InternalOWLModel.g:2030:2: (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:complementOf' otherlv_7= '>' ) )
                    int alt38=2;
                    int LA38_0 = input.LA(1);

                    if ( (LA38_0==29) ) {
                        alt38=1;
                    }
                    else if ( (LA38_0==21) ) {
                        alt38=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 38, 0, input);

                        throw nvae;
                    }
                    switch (alt38) {
                        case 1 :
                            // InternalOWLModel.g:2030:4: otherlv_4= '/>'
                            {
                            otherlv_4=(Token)match(input,29,FOLLOW_2); 

                                	newLeafNode(otherlv_4, grammarAccess.getComplementAxiomAccess().getSolidusGreaterThanSignKeyword_1_0_3_0());
                                

                            }
                            break;
                        case 2 :
                            // InternalOWLModel.g:2035:6: (otherlv_5= '>' otherlv_6= '</owl:complementOf' otherlv_7= '>' )
                            {
                            // InternalOWLModel.g:2035:6: (otherlv_5= '>' otherlv_6= '</owl:complementOf' otherlv_7= '>' )
                            // InternalOWLModel.g:2035:8: otherlv_5= '>' otherlv_6= '</owl:complementOf' otherlv_7= '>'
                            {
                            otherlv_5=(Token)match(input,21,FOLLOW_38); 

                                	newLeafNode(otherlv_5, grammarAccess.getComplementAxiomAccess().getGreaterThanSignKeyword_1_0_3_1_0());
                                
                            otherlv_6=(Token)match(input,43,FOLLOW_7); 

                                	newLeafNode(otherlv_6, grammarAccess.getComplementAxiomAccess().getOwlComplementOfKeyword_1_0_3_1_1());
                                
                            otherlv_7=(Token)match(input,21,FOLLOW_2); 

                                	newLeafNode(otherlv_7, grammarAccess.getComplementAxiomAccess().getGreaterThanSignKeyword_1_0_3_1_2());
                                

                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:2048:6: ( (otherlv_8= 'rdf:parseType' otherlv_9= '=' ( (lv_parseType_10_0= ruleParseType ) ) )? otherlv_11= '>' ( (lv_classes_12_0= ruleClassModifier ) )+ otherlv_13= '</owl:complementOf' otherlv_14= '>' )
                    {
                    // InternalOWLModel.g:2048:6: ( (otherlv_8= 'rdf:parseType' otherlv_9= '=' ( (lv_parseType_10_0= ruleParseType ) ) )? otherlv_11= '>' ( (lv_classes_12_0= ruleClassModifier ) )+ otherlv_13= '</owl:complementOf' otherlv_14= '>' )
                    // InternalOWLModel.g:2048:7: (otherlv_8= 'rdf:parseType' otherlv_9= '=' ( (lv_parseType_10_0= ruleParseType ) ) )? otherlv_11= '>' ( (lv_classes_12_0= ruleClassModifier ) )+ otherlv_13= '</owl:complementOf' otherlv_14= '>'
                    {
                    // InternalOWLModel.g:2048:7: (otherlv_8= 'rdf:parseType' otherlv_9= '=' ( (lv_parseType_10_0= ruleParseType ) ) )?
                    int alt39=2;
                    int LA39_0 = input.LA(1);

                    if ( (LA39_0==44) ) {
                        alt39=1;
                    }
                    switch (alt39) {
                        case 1 :
                            // InternalOWLModel.g:2048:9: otherlv_8= 'rdf:parseType' otherlv_9= '=' ( (lv_parseType_10_0= ruleParseType ) )
                            {
                            otherlv_8=(Token)match(input,44,FOLLOW_13); 

                                	newLeafNode(otherlv_8, grammarAccess.getComplementAxiomAccess().getRdfParseTypeKeyword_1_1_0_0());
                                
                            otherlv_9=(Token)match(input,25,FOLLOW_9); 

                                	newLeafNode(otherlv_9, grammarAccess.getComplementAxiomAccess().getEqualsSignKeyword_1_1_0_1());
                                
                            // InternalOWLModel.g:2056:1: ( (lv_parseType_10_0= ruleParseType ) )
                            // InternalOWLModel.g:2057:1: (lv_parseType_10_0= ruleParseType )
                            {
                            // InternalOWLModel.g:2057:1: (lv_parseType_10_0= ruleParseType )
                            // InternalOWLModel.g:2058:3: lv_parseType_10_0= ruleParseType
                            {
                             
                            	        newCompositeNode(grammarAccess.getComplementAxiomAccess().getParseTypeParseTypeParserRuleCall_1_1_0_2_0()); 
                            	    
                            pushFollow(FOLLOW_7);
                            lv_parseType_10_0=ruleParseType();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getComplementAxiomRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"parseType",
                                    		lv_parseType_10_0, 
                                    		"org.smool.sdk.owlparser.OWLModel.ParseType");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }

                    otherlv_11=(Token)match(input,21,FOLLOW_39); 

                        	newLeafNode(otherlv_11, grammarAccess.getComplementAxiomAccess().getGreaterThanSignKeyword_1_1_1());
                        
                    // InternalOWLModel.g:2078:1: ( (lv_classes_12_0= ruleClassModifier ) )+
                    int cnt40=0;
                    loop40:
                    do {
                        int alt40=2;
                        int LA40_0 = input.LA(1);

                        if ( (LA40_0==39||LA40_0==70||LA40_0==122) ) {
                            alt40=1;
                        }


                        switch (alt40) {
                    	case 1 :
                    	    // InternalOWLModel.g:2079:1: (lv_classes_12_0= ruleClassModifier )
                    	    {
                    	    // InternalOWLModel.g:2079:1: (lv_classes_12_0= ruleClassModifier )
                    	    // InternalOWLModel.g:2080:3: lv_classes_12_0= ruleClassModifier
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getComplementAxiomAccess().getClassesClassModifierParserRuleCall_1_1_2_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_40);
                    	    lv_classes_12_0=ruleClassModifier();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getComplementAxiomRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"classes",
                    	            		lv_classes_12_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.ClassModifier");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt40 >= 1 ) break loop40;
                                EarlyExitException eee =
                                    new EarlyExitException(40, input);
                                throw eee;
                        }
                        cnt40++;
                    } while (true);

                    otherlv_13=(Token)match(input,43,FOLLOW_7); 

                        	newLeafNode(otherlv_13, grammarAccess.getComplementAxiomAccess().getOwlComplementOfKeyword_1_1_3());
                        
                    otherlv_14=(Token)match(input,21,FOLLOW_2); 

                        	newLeafNode(otherlv_14, grammarAccess.getComplementAxiomAccess().getGreaterThanSignKeyword_1_1_4());
                        

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComplementAxiom"


    // $ANTLR start "entryRuleIncompatibleWithAxiom"
    // InternalOWLModel.g:2112:1: entryRuleIncompatibleWithAxiom returns [EObject current=null] : iv_ruleIncompatibleWithAxiom= ruleIncompatibleWithAxiom EOF ;
    public final EObject entryRuleIncompatibleWithAxiom() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIncompatibleWithAxiom = null;


        try {
            // InternalOWLModel.g:2113:2: (iv_ruleIncompatibleWithAxiom= ruleIncompatibleWithAxiom EOF )
            // InternalOWLModel.g:2114:2: iv_ruleIncompatibleWithAxiom= ruleIncompatibleWithAxiom EOF
            {
             newCompositeNode(grammarAccess.getIncompatibleWithAxiomRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleIncompatibleWithAxiom=ruleIncompatibleWithAxiom();

            state._fsp--;

             current =iv_ruleIncompatibleWithAxiom; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIncompatibleWithAxiom"


    // $ANTLR start "ruleIncompatibleWithAxiom"
    // InternalOWLModel.g:2121:1: ruleIncompatibleWithAxiom returns [EObject current=null] : ( () otherlv_1= '<owl:incompatibleWith' ( ( (otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) )? (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</owl:incompatibleWith' otherlv_8= '>' ) ) ) | ( (otherlv_9= 'rdf:parseType' otherlv_10= '=' ( (lv_parseType_11_0= ruleParseType ) ) )? otherlv_12= '>' ( (lv_classes_13_0= ruleClassModifier ) )+ otherlv_14= '</owl:incompatibleWith' otherlv_15= '>' ) ) ) ;
    public final EObject ruleIncompatibleWithAxiom() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        EObject lv_ref_4_0 = null;

        EObject lv_parseType_11_0 = null;

        EObject lv_classes_13_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:2124:28: ( ( () otherlv_1= '<owl:incompatibleWith' ( ( (otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) )? (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</owl:incompatibleWith' otherlv_8= '>' ) ) ) | ( (otherlv_9= 'rdf:parseType' otherlv_10= '=' ( (lv_parseType_11_0= ruleParseType ) ) )? otherlv_12= '>' ( (lv_classes_13_0= ruleClassModifier ) )+ otherlv_14= '</owl:incompatibleWith' otherlv_15= '>' ) ) ) )
            // InternalOWLModel.g:2125:1: ( () otherlv_1= '<owl:incompatibleWith' ( ( (otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) )? (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</owl:incompatibleWith' otherlv_8= '>' ) ) ) | ( (otherlv_9= 'rdf:parseType' otherlv_10= '=' ( (lv_parseType_11_0= ruleParseType ) ) )? otherlv_12= '>' ( (lv_classes_13_0= ruleClassModifier ) )+ otherlv_14= '</owl:incompatibleWith' otherlv_15= '>' ) ) )
            {
            // InternalOWLModel.g:2125:1: ( () otherlv_1= '<owl:incompatibleWith' ( ( (otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) )? (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</owl:incompatibleWith' otherlv_8= '>' ) ) ) | ( (otherlv_9= 'rdf:parseType' otherlv_10= '=' ( (lv_parseType_11_0= ruleParseType ) ) )? otherlv_12= '>' ( (lv_classes_13_0= ruleClassModifier ) )+ otherlv_14= '</owl:incompatibleWith' otherlv_15= '>' ) ) )
            // InternalOWLModel.g:2125:2: () otherlv_1= '<owl:incompatibleWith' ( ( (otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) )? (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</owl:incompatibleWith' otherlv_8= '>' ) ) ) | ( (otherlv_9= 'rdf:parseType' otherlv_10= '=' ( (lv_parseType_11_0= ruleParseType ) ) )? otherlv_12= '>' ( (lv_classes_13_0= ruleClassModifier ) )+ otherlv_14= '</owl:incompatibleWith' otherlv_15= '>' ) )
            {
            // InternalOWLModel.g:2125:2: ()
            // InternalOWLModel.g:2126:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getIncompatibleWithAxiomAccess().getIncompatibleWithAxiomAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,45,FOLLOW_41); 

                	newLeafNode(otherlv_1, grammarAccess.getIncompatibleWithAxiomAccess().getOwlIncompatibleWithKeyword_1());
                
            // InternalOWLModel.g:2135:1: ( ( (otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) )? (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</owl:incompatibleWith' otherlv_8= '>' ) ) ) | ( (otherlv_9= 'rdf:parseType' otherlv_10= '=' ( (lv_parseType_11_0= ruleParseType ) ) )? otherlv_12= '>' ( (lv_classes_13_0= ruleClassModifier ) )+ otherlv_14= '</owl:incompatibleWith' otherlv_15= '>' ) )
            int alt46=2;
            switch ( input.LA(1) ) {
            case 29:
            case 32:
                {
                alt46=1;
                }
                break;
            case 21:
                {
                int LA46_2 = input.LA(2);

                if ( (LA46_2==46) ) {
                    alt46=1;
                }
                else if ( (LA46_2==39||LA46_2==70||LA46_2==122) ) {
                    alt46=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 46, 2, input);

                    throw nvae;
                }
                }
                break;
            case 44:
                {
                alt46=2;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 46, 0, input);

                throw nvae;
            }

            switch (alt46) {
                case 1 :
                    // InternalOWLModel.g:2135:2: ( (otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) )? (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</owl:incompatibleWith' otherlv_8= '>' ) ) )
                    {
                    // InternalOWLModel.g:2135:2: ( (otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) )? (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</owl:incompatibleWith' otherlv_8= '>' ) ) )
                    // InternalOWLModel.g:2135:3: (otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) )? (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</owl:incompatibleWith' otherlv_8= '>' ) )
                    {
                    // InternalOWLModel.g:2135:3: (otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) )?
                    int alt42=2;
                    int LA42_0 = input.LA(1);

                    if ( (LA42_0==32) ) {
                        alt42=1;
                    }
                    switch (alt42) {
                        case 1 :
                            // InternalOWLModel.g:2135:5: otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) )
                            {
                            otherlv_2=(Token)match(input,32,FOLLOW_13); 

                                	newLeafNode(otherlv_2, grammarAccess.getIncompatibleWithAxiomAccess().getRdfResourceKeyword_2_0_0_0());
                                
                            otherlv_3=(Token)match(input,25,FOLLOW_9); 

                                	newLeafNode(otherlv_3, grammarAccess.getIncompatibleWithAxiomAccess().getEqualsSignKeyword_2_0_0_1());
                                
                            // InternalOWLModel.g:2143:1: ( (lv_ref_4_0= ruleOwlRef ) )
                            // InternalOWLModel.g:2144:1: (lv_ref_4_0= ruleOwlRef )
                            {
                            // InternalOWLModel.g:2144:1: (lv_ref_4_0= ruleOwlRef )
                            // InternalOWLModel.g:2145:3: lv_ref_4_0= ruleOwlRef
                            {
                             
                            	        newCompositeNode(grammarAccess.getIncompatibleWithAxiomAccess().getRefOwlRefParserRuleCall_2_0_0_2_0()); 
                            	    
                            pushFollow(FOLLOW_24);
                            lv_ref_4_0=ruleOwlRef();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getIncompatibleWithAxiomRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"ref",
                                    		lv_ref_4_0, 
                                    		"org.smool.sdk.owlparser.OWLModel.OwlRef");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }

                    // InternalOWLModel.g:2161:4: (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</owl:incompatibleWith' otherlv_8= '>' ) )
                    int alt43=2;
                    int LA43_0 = input.LA(1);

                    if ( (LA43_0==29) ) {
                        alt43=1;
                    }
                    else if ( (LA43_0==21) ) {
                        alt43=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 43, 0, input);

                        throw nvae;
                    }
                    switch (alt43) {
                        case 1 :
                            // InternalOWLModel.g:2161:6: otherlv_5= '/>'
                            {
                            otherlv_5=(Token)match(input,29,FOLLOW_2); 

                                	newLeafNode(otherlv_5, grammarAccess.getIncompatibleWithAxiomAccess().getSolidusGreaterThanSignKeyword_2_0_1_0());
                                

                            }
                            break;
                        case 2 :
                            // InternalOWLModel.g:2166:6: (otherlv_6= '>' otherlv_7= '</owl:incompatibleWith' otherlv_8= '>' )
                            {
                            // InternalOWLModel.g:2166:6: (otherlv_6= '>' otherlv_7= '</owl:incompatibleWith' otherlv_8= '>' )
                            // InternalOWLModel.g:2166:8: otherlv_6= '>' otherlv_7= '</owl:incompatibleWith' otherlv_8= '>'
                            {
                            otherlv_6=(Token)match(input,21,FOLLOW_42); 

                                	newLeafNode(otherlv_6, grammarAccess.getIncompatibleWithAxiomAccess().getGreaterThanSignKeyword_2_0_1_1_0());
                                
                            otherlv_7=(Token)match(input,46,FOLLOW_7); 

                                	newLeafNode(otherlv_7, grammarAccess.getIncompatibleWithAxiomAccess().getOwlIncompatibleWithKeyword_2_0_1_1_1());
                                
                            otherlv_8=(Token)match(input,21,FOLLOW_2); 

                                	newLeafNode(otherlv_8, grammarAccess.getIncompatibleWithAxiomAccess().getGreaterThanSignKeyword_2_0_1_1_2());
                                

                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:2179:6: ( (otherlv_9= 'rdf:parseType' otherlv_10= '=' ( (lv_parseType_11_0= ruleParseType ) ) )? otherlv_12= '>' ( (lv_classes_13_0= ruleClassModifier ) )+ otherlv_14= '</owl:incompatibleWith' otherlv_15= '>' )
                    {
                    // InternalOWLModel.g:2179:6: ( (otherlv_9= 'rdf:parseType' otherlv_10= '=' ( (lv_parseType_11_0= ruleParseType ) ) )? otherlv_12= '>' ( (lv_classes_13_0= ruleClassModifier ) )+ otherlv_14= '</owl:incompatibleWith' otherlv_15= '>' )
                    // InternalOWLModel.g:2179:7: (otherlv_9= 'rdf:parseType' otherlv_10= '=' ( (lv_parseType_11_0= ruleParseType ) ) )? otherlv_12= '>' ( (lv_classes_13_0= ruleClassModifier ) )+ otherlv_14= '</owl:incompatibleWith' otherlv_15= '>'
                    {
                    // InternalOWLModel.g:2179:7: (otherlv_9= 'rdf:parseType' otherlv_10= '=' ( (lv_parseType_11_0= ruleParseType ) ) )?
                    int alt44=2;
                    int LA44_0 = input.LA(1);

                    if ( (LA44_0==44) ) {
                        alt44=1;
                    }
                    switch (alt44) {
                        case 1 :
                            // InternalOWLModel.g:2179:9: otherlv_9= 'rdf:parseType' otherlv_10= '=' ( (lv_parseType_11_0= ruleParseType ) )
                            {
                            otherlv_9=(Token)match(input,44,FOLLOW_13); 

                                	newLeafNode(otherlv_9, grammarAccess.getIncompatibleWithAxiomAccess().getRdfParseTypeKeyword_2_1_0_0());
                                
                            otherlv_10=(Token)match(input,25,FOLLOW_9); 

                                	newLeafNode(otherlv_10, grammarAccess.getIncompatibleWithAxiomAccess().getEqualsSignKeyword_2_1_0_1());
                                
                            // InternalOWLModel.g:2187:1: ( (lv_parseType_11_0= ruleParseType ) )
                            // InternalOWLModel.g:2188:1: (lv_parseType_11_0= ruleParseType )
                            {
                            // InternalOWLModel.g:2188:1: (lv_parseType_11_0= ruleParseType )
                            // InternalOWLModel.g:2189:3: lv_parseType_11_0= ruleParseType
                            {
                             
                            	        newCompositeNode(grammarAccess.getIncompatibleWithAxiomAccess().getParseTypeParseTypeParserRuleCall_2_1_0_2_0()); 
                            	    
                            pushFollow(FOLLOW_7);
                            lv_parseType_11_0=ruleParseType();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getIncompatibleWithAxiomRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"parseType",
                                    		lv_parseType_11_0, 
                                    		"org.smool.sdk.owlparser.OWLModel.ParseType");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }

                    otherlv_12=(Token)match(input,21,FOLLOW_39); 

                        	newLeafNode(otherlv_12, grammarAccess.getIncompatibleWithAxiomAccess().getGreaterThanSignKeyword_2_1_1());
                        
                    // InternalOWLModel.g:2209:1: ( (lv_classes_13_0= ruleClassModifier ) )+
                    int cnt45=0;
                    loop45:
                    do {
                        int alt45=2;
                        int LA45_0 = input.LA(1);

                        if ( (LA45_0==39||LA45_0==70||LA45_0==122) ) {
                            alt45=1;
                        }


                        switch (alt45) {
                    	case 1 :
                    	    // InternalOWLModel.g:2210:1: (lv_classes_13_0= ruleClassModifier )
                    	    {
                    	    // InternalOWLModel.g:2210:1: (lv_classes_13_0= ruleClassModifier )
                    	    // InternalOWLModel.g:2211:3: lv_classes_13_0= ruleClassModifier
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getIncompatibleWithAxiomAccess().getClassesClassModifierParserRuleCall_2_1_2_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_43);
                    	    lv_classes_13_0=ruleClassModifier();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getIncompatibleWithAxiomRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"classes",
                    	            		lv_classes_13_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.ClassModifier");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt45 >= 1 ) break loop45;
                                EarlyExitException eee =
                                    new EarlyExitException(45, input);
                                throw eee;
                        }
                        cnt45++;
                    } while (true);

                    otherlv_14=(Token)match(input,46,FOLLOW_7); 

                        	newLeafNode(otherlv_14, grammarAccess.getIncompatibleWithAxiomAccess().getOwlIncompatibleWithKeyword_2_1_3());
                        
                    otherlv_15=(Token)match(input,21,FOLLOW_2); 

                        	newLeafNode(otherlv_15, grammarAccess.getIncompatibleWithAxiomAccess().getGreaterThanSignKeyword_2_1_4());
                        

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIncompatibleWithAxiom"


    // $ANTLR start "entryRuleDisjointAxiom"
    // InternalOWLModel.g:2243:1: entryRuleDisjointAxiom returns [EObject current=null] : iv_ruleDisjointAxiom= ruleDisjointAxiom EOF ;
    public final EObject entryRuleDisjointAxiom() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDisjointAxiom = null;


        try {
            // InternalOWLModel.g:2244:2: (iv_ruleDisjointAxiom= ruleDisjointAxiom EOF )
            // InternalOWLModel.g:2245:2: iv_ruleDisjointAxiom= ruleDisjointAxiom EOF
            {
             newCompositeNode(grammarAccess.getDisjointAxiomRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDisjointAxiom=ruleDisjointAxiom();

            state._fsp--;

             current =iv_ruleDisjointAxiom; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDisjointAxiom"


    // $ANTLR start "ruleDisjointAxiom"
    // InternalOWLModel.g:2252:1: ruleDisjointAxiom returns [EObject current=null] : (otherlv_0= '<owl:disjointWith' ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:disjointWith' otherlv_7= '>' ) ) ) | ( (otherlv_8= 'rdf:parseType' otherlv_9= '=' ( (lv_parseType_10_0= ruleParseType ) ) )? otherlv_11= '>' ( (lv_classes_12_0= ruleClassModifier ) )+ otherlv_13= '</owl:disjointWith' otherlv_14= '>' ) ) ) ;
    public final EObject ruleDisjointAxiom() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        EObject lv_ref_3_0 = null;

        EObject lv_parseType_10_0 = null;

        EObject lv_classes_12_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:2255:28: ( (otherlv_0= '<owl:disjointWith' ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:disjointWith' otherlv_7= '>' ) ) ) | ( (otherlv_8= 'rdf:parseType' otherlv_9= '=' ( (lv_parseType_10_0= ruleParseType ) ) )? otherlv_11= '>' ( (lv_classes_12_0= ruleClassModifier ) )+ otherlv_13= '</owl:disjointWith' otherlv_14= '>' ) ) ) )
            // InternalOWLModel.g:2256:1: (otherlv_0= '<owl:disjointWith' ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:disjointWith' otherlv_7= '>' ) ) ) | ( (otherlv_8= 'rdf:parseType' otherlv_9= '=' ( (lv_parseType_10_0= ruleParseType ) ) )? otherlv_11= '>' ( (lv_classes_12_0= ruleClassModifier ) )+ otherlv_13= '</owl:disjointWith' otherlv_14= '>' ) ) )
            {
            // InternalOWLModel.g:2256:1: (otherlv_0= '<owl:disjointWith' ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:disjointWith' otherlv_7= '>' ) ) ) | ( (otherlv_8= 'rdf:parseType' otherlv_9= '=' ( (lv_parseType_10_0= ruleParseType ) ) )? otherlv_11= '>' ( (lv_classes_12_0= ruleClassModifier ) )+ otherlv_13= '</owl:disjointWith' otherlv_14= '>' ) ) )
            // InternalOWLModel.g:2256:3: otherlv_0= '<owl:disjointWith' ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:disjointWith' otherlv_7= '>' ) ) ) | ( (otherlv_8= 'rdf:parseType' otherlv_9= '=' ( (lv_parseType_10_0= ruleParseType ) ) )? otherlv_11= '>' ( (lv_classes_12_0= ruleClassModifier ) )+ otherlv_13= '</owl:disjointWith' otherlv_14= '>' ) )
            {
            otherlv_0=(Token)match(input,47,FOLLOW_37); 

                	newLeafNode(otherlv_0, grammarAccess.getDisjointAxiomAccess().getOwlDisjointWithKeyword_0());
                
            // InternalOWLModel.g:2260:1: ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:disjointWith' otherlv_7= '>' ) ) ) | ( (otherlv_8= 'rdf:parseType' otherlv_9= '=' ( (lv_parseType_10_0= ruleParseType ) ) )? otherlv_11= '>' ( (lv_classes_12_0= ruleClassModifier ) )+ otherlv_13= '</owl:disjointWith' otherlv_14= '>' ) )
            int alt50=2;
            int LA50_0 = input.LA(1);

            if ( (LA50_0==32) ) {
                alt50=1;
            }
            else if ( (LA50_0==21||LA50_0==44) ) {
                alt50=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 50, 0, input);

                throw nvae;
            }
            switch (alt50) {
                case 1 :
                    // InternalOWLModel.g:2260:2: (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:disjointWith' otherlv_7= '>' ) ) )
                    {
                    // InternalOWLModel.g:2260:2: (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:disjointWith' otherlv_7= '>' ) ) )
                    // InternalOWLModel.g:2260:4: otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:disjointWith' otherlv_7= '>' ) )
                    {
                    otherlv_1=(Token)match(input,32,FOLLOW_13); 

                        	newLeafNode(otherlv_1, grammarAccess.getDisjointAxiomAccess().getRdfResourceKeyword_1_0_0());
                        
                    otherlv_2=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_2, grammarAccess.getDisjointAxiomAccess().getEqualsSignKeyword_1_0_1());
                        
                    // InternalOWLModel.g:2268:1: ( (lv_ref_3_0= ruleOwlRef ) )
                    // InternalOWLModel.g:2269:1: (lv_ref_3_0= ruleOwlRef )
                    {
                    // InternalOWLModel.g:2269:1: (lv_ref_3_0= ruleOwlRef )
                    // InternalOWLModel.g:2270:3: lv_ref_3_0= ruleOwlRef
                    {
                     
                    	        newCompositeNode(grammarAccess.getDisjointAxiomAccess().getRefOwlRefParserRuleCall_1_0_2_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_ref_3_0=ruleOwlRef();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getDisjointAxiomRule());
                    	        }
                           		set(
                           			current, 
                           			"ref",
                            		lv_ref_3_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlRef");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // InternalOWLModel.g:2286:2: (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:disjointWith' otherlv_7= '>' ) )
                    int alt47=2;
                    int LA47_0 = input.LA(1);

                    if ( (LA47_0==29) ) {
                        alt47=1;
                    }
                    else if ( (LA47_0==21) ) {
                        alt47=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 47, 0, input);

                        throw nvae;
                    }
                    switch (alt47) {
                        case 1 :
                            // InternalOWLModel.g:2286:4: otherlv_4= '/>'
                            {
                            otherlv_4=(Token)match(input,29,FOLLOW_2); 

                                	newLeafNode(otherlv_4, grammarAccess.getDisjointAxiomAccess().getSolidusGreaterThanSignKeyword_1_0_3_0());
                                

                            }
                            break;
                        case 2 :
                            // InternalOWLModel.g:2291:6: (otherlv_5= '>' otherlv_6= '</owl:disjointWith' otherlv_7= '>' )
                            {
                            // InternalOWLModel.g:2291:6: (otherlv_5= '>' otherlv_6= '</owl:disjointWith' otherlv_7= '>' )
                            // InternalOWLModel.g:2291:8: otherlv_5= '>' otherlv_6= '</owl:disjointWith' otherlv_7= '>'
                            {
                            otherlv_5=(Token)match(input,21,FOLLOW_44); 

                                	newLeafNode(otherlv_5, grammarAccess.getDisjointAxiomAccess().getGreaterThanSignKeyword_1_0_3_1_0());
                                
                            otherlv_6=(Token)match(input,48,FOLLOW_7); 

                                	newLeafNode(otherlv_6, grammarAccess.getDisjointAxiomAccess().getOwlDisjointWithKeyword_1_0_3_1_1());
                                
                            otherlv_7=(Token)match(input,21,FOLLOW_2); 

                                	newLeafNode(otherlv_7, grammarAccess.getDisjointAxiomAccess().getGreaterThanSignKeyword_1_0_3_1_2());
                                

                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:2304:6: ( (otherlv_8= 'rdf:parseType' otherlv_9= '=' ( (lv_parseType_10_0= ruleParseType ) ) )? otherlv_11= '>' ( (lv_classes_12_0= ruleClassModifier ) )+ otherlv_13= '</owl:disjointWith' otherlv_14= '>' )
                    {
                    // InternalOWLModel.g:2304:6: ( (otherlv_8= 'rdf:parseType' otherlv_9= '=' ( (lv_parseType_10_0= ruleParseType ) ) )? otherlv_11= '>' ( (lv_classes_12_0= ruleClassModifier ) )+ otherlv_13= '</owl:disjointWith' otherlv_14= '>' )
                    // InternalOWLModel.g:2304:7: (otherlv_8= 'rdf:parseType' otherlv_9= '=' ( (lv_parseType_10_0= ruleParseType ) ) )? otherlv_11= '>' ( (lv_classes_12_0= ruleClassModifier ) )+ otherlv_13= '</owl:disjointWith' otherlv_14= '>'
                    {
                    // InternalOWLModel.g:2304:7: (otherlv_8= 'rdf:parseType' otherlv_9= '=' ( (lv_parseType_10_0= ruleParseType ) ) )?
                    int alt48=2;
                    int LA48_0 = input.LA(1);

                    if ( (LA48_0==44) ) {
                        alt48=1;
                    }
                    switch (alt48) {
                        case 1 :
                            // InternalOWLModel.g:2304:9: otherlv_8= 'rdf:parseType' otherlv_9= '=' ( (lv_parseType_10_0= ruleParseType ) )
                            {
                            otherlv_8=(Token)match(input,44,FOLLOW_13); 

                                	newLeafNode(otherlv_8, grammarAccess.getDisjointAxiomAccess().getRdfParseTypeKeyword_1_1_0_0());
                                
                            otherlv_9=(Token)match(input,25,FOLLOW_9); 

                                	newLeafNode(otherlv_9, grammarAccess.getDisjointAxiomAccess().getEqualsSignKeyword_1_1_0_1());
                                
                            // InternalOWLModel.g:2312:1: ( (lv_parseType_10_0= ruleParseType ) )
                            // InternalOWLModel.g:2313:1: (lv_parseType_10_0= ruleParseType )
                            {
                            // InternalOWLModel.g:2313:1: (lv_parseType_10_0= ruleParseType )
                            // InternalOWLModel.g:2314:3: lv_parseType_10_0= ruleParseType
                            {
                             
                            	        newCompositeNode(grammarAccess.getDisjointAxiomAccess().getParseTypeParseTypeParserRuleCall_1_1_0_2_0()); 
                            	    
                            pushFollow(FOLLOW_7);
                            lv_parseType_10_0=ruleParseType();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getDisjointAxiomRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"parseType",
                                    		lv_parseType_10_0, 
                                    		"org.smool.sdk.owlparser.OWLModel.ParseType");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }

                    otherlv_11=(Token)match(input,21,FOLLOW_39); 

                        	newLeafNode(otherlv_11, grammarAccess.getDisjointAxiomAccess().getGreaterThanSignKeyword_1_1_1());
                        
                    // InternalOWLModel.g:2334:1: ( (lv_classes_12_0= ruleClassModifier ) )+
                    int cnt49=0;
                    loop49:
                    do {
                        int alt49=2;
                        int LA49_0 = input.LA(1);

                        if ( (LA49_0==39||LA49_0==70||LA49_0==122) ) {
                            alt49=1;
                        }


                        switch (alt49) {
                    	case 1 :
                    	    // InternalOWLModel.g:2335:1: (lv_classes_12_0= ruleClassModifier )
                    	    {
                    	    // InternalOWLModel.g:2335:1: (lv_classes_12_0= ruleClassModifier )
                    	    // InternalOWLModel.g:2336:3: lv_classes_12_0= ruleClassModifier
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getDisjointAxiomAccess().getClassesClassModifierParserRuleCall_1_1_2_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_45);
                    	    lv_classes_12_0=ruleClassModifier();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getDisjointAxiomRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"classes",
                    	            		lv_classes_12_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.ClassModifier");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt49 >= 1 ) break loop49;
                                EarlyExitException eee =
                                    new EarlyExitException(49, input);
                                throw eee;
                        }
                        cnt49++;
                    } while (true);

                    otherlv_13=(Token)match(input,48,FOLLOW_7); 

                        	newLeafNode(otherlv_13, grammarAccess.getDisjointAxiomAccess().getOwlDisjointWithKeyword_1_1_3());
                        
                    otherlv_14=(Token)match(input,21,FOLLOW_2); 

                        	newLeafNode(otherlv_14, grammarAccess.getDisjointAxiomAccess().getGreaterThanSignKeyword_1_1_4());
                        

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDisjointAxiom"


    // $ANTLR start "entryRuleEquivalentAxiom"
    // InternalOWLModel.g:2368:1: entryRuleEquivalentAxiom returns [EObject current=null] : iv_ruleEquivalentAxiom= ruleEquivalentAxiom EOF ;
    public final EObject entryRuleEquivalentAxiom() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEquivalentAxiom = null;


        try {
            // InternalOWLModel.g:2369:2: (iv_ruleEquivalentAxiom= ruleEquivalentAxiom EOF )
            // InternalOWLModel.g:2370:2: iv_ruleEquivalentAxiom= ruleEquivalentAxiom EOF
            {
             newCompositeNode(grammarAccess.getEquivalentAxiomRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEquivalentAxiom=ruleEquivalentAxiom();

            state._fsp--;

             current =iv_ruleEquivalentAxiom; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEquivalentAxiom"


    // $ANTLR start "ruleEquivalentAxiom"
    // InternalOWLModel.g:2377:1: ruleEquivalentAxiom returns [EObject current=null] : (otherlv_0= '<owl:equivalentClass' ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:equivalentClass' otherlv_7= '>' ) ) ) | ( (otherlv_8= 'rdf:parseType' otherlv_9= '=' ( (lv_parseType_10_0= ruleParseType ) ) )? otherlv_11= '>' ( (lv_classes_12_0= ruleClassModifier ) )+ otherlv_13= '</owl:equivalentClass' otherlv_14= '>' ) ) ) ;
    public final EObject ruleEquivalentAxiom() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        EObject lv_ref_3_0 = null;

        EObject lv_parseType_10_0 = null;

        EObject lv_classes_12_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:2380:28: ( (otherlv_0= '<owl:equivalentClass' ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:equivalentClass' otherlv_7= '>' ) ) ) | ( (otherlv_8= 'rdf:parseType' otherlv_9= '=' ( (lv_parseType_10_0= ruleParseType ) ) )? otherlv_11= '>' ( (lv_classes_12_0= ruleClassModifier ) )+ otherlv_13= '</owl:equivalentClass' otherlv_14= '>' ) ) ) )
            // InternalOWLModel.g:2381:1: (otherlv_0= '<owl:equivalentClass' ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:equivalentClass' otherlv_7= '>' ) ) ) | ( (otherlv_8= 'rdf:parseType' otherlv_9= '=' ( (lv_parseType_10_0= ruleParseType ) ) )? otherlv_11= '>' ( (lv_classes_12_0= ruleClassModifier ) )+ otherlv_13= '</owl:equivalentClass' otherlv_14= '>' ) ) )
            {
            // InternalOWLModel.g:2381:1: (otherlv_0= '<owl:equivalentClass' ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:equivalentClass' otherlv_7= '>' ) ) ) | ( (otherlv_8= 'rdf:parseType' otherlv_9= '=' ( (lv_parseType_10_0= ruleParseType ) ) )? otherlv_11= '>' ( (lv_classes_12_0= ruleClassModifier ) )+ otherlv_13= '</owl:equivalentClass' otherlv_14= '>' ) ) )
            // InternalOWLModel.g:2381:3: otherlv_0= '<owl:equivalentClass' ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:equivalentClass' otherlv_7= '>' ) ) ) | ( (otherlv_8= 'rdf:parseType' otherlv_9= '=' ( (lv_parseType_10_0= ruleParseType ) ) )? otherlv_11= '>' ( (lv_classes_12_0= ruleClassModifier ) )+ otherlv_13= '</owl:equivalentClass' otherlv_14= '>' ) )
            {
            otherlv_0=(Token)match(input,49,FOLLOW_37); 

                	newLeafNode(otherlv_0, grammarAccess.getEquivalentAxiomAccess().getOwlEquivalentClassKeyword_0());
                
            // InternalOWLModel.g:2385:1: ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:equivalentClass' otherlv_7= '>' ) ) ) | ( (otherlv_8= 'rdf:parseType' otherlv_9= '=' ( (lv_parseType_10_0= ruleParseType ) ) )? otherlv_11= '>' ( (lv_classes_12_0= ruleClassModifier ) )+ otherlv_13= '</owl:equivalentClass' otherlv_14= '>' ) )
            int alt54=2;
            int LA54_0 = input.LA(1);

            if ( (LA54_0==32) ) {
                alt54=1;
            }
            else if ( (LA54_0==21||LA54_0==44) ) {
                alt54=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 54, 0, input);

                throw nvae;
            }
            switch (alt54) {
                case 1 :
                    // InternalOWLModel.g:2385:2: (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:equivalentClass' otherlv_7= '>' ) ) )
                    {
                    // InternalOWLModel.g:2385:2: (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:equivalentClass' otherlv_7= '>' ) ) )
                    // InternalOWLModel.g:2385:4: otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:equivalentClass' otherlv_7= '>' ) )
                    {
                    otherlv_1=(Token)match(input,32,FOLLOW_13); 

                        	newLeafNode(otherlv_1, grammarAccess.getEquivalentAxiomAccess().getRdfResourceKeyword_1_0_0());
                        
                    otherlv_2=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_2, grammarAccess.getEquivalentAxiomAccess().getEqualsSignKeyword_1_0_1());
                        
                    // InternalOWLModel.g:2393:1: ( (lv_ref_3_0= ruleOwlRef ) )
                    // InternalOWLModel.g:2394:1: (lv_ref_3_0= ruleOwlRef )
                    {
                    // InternalOWLModel.g:2394:1: (lv_ref_3_0= ruleOwlRef )
                    // InternalOWLModel.g:2395:3: lv_ref_3_0= ruleOwlRef
                    {
                     
                    	        newCompositeNode(grammarAccess.getEquivalentAxiomAccess().getRefOwlRefParserRuleCall_1_0_2_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_ref_3_0=ruleOwlRef();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getEquivalentAxiomRule());
                    	        }
                           		set(
                           			current, 
                           			"ref",
                            		lv_ref_3_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlRef");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // InternalOWLModel.g:2411:2: (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:equivalentClass' otherlv_7= '>' ) )
                    int alt51=2;
                    int LA51_0 = input.LA(1);

                    if ( (LA51_0==29) ) {
                        alt51=1;
                    }
                    else if ( (LA51_0==21) ) {
                        alt51=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 51, 0, input);

                        throw nvae;
                    }
                    switch (alt51) {
                        case 1 :
                            // InternalOWLModel.g:2411:4: otherlv_4= '/>'
                            {
                            otherlv_4=(Token)match(input,29,FOLLOW_2); 

                                	newLeafNode(otherlv_4, grammarAccess.getEquivalentAxiomAccess().getSolidusGreaterThanSignKeyword_1_0_3_0());
                                

                            }
                            break;
                        case 2 :
                            // InternalOWLModel.g:2416:6: (otherlv_5= '>' otherlv_6= '</owl:equivalentClass' otherlv_7= '>' )
                            {
                            // InternalOWLModel.g:2416:6: (otherlv_5= '>' otherlv_6= '</owl:equivalentClass' otherlv_7= '>' )
                            // InternalOWLModel.g:2416:8: otherlv_5= '>' otherlv_6= '</owl:equivalentClass' otherlv_7= '>'
                            {
                            otherlv_5=(Token)match(input,21,FOLLOW_46); 

                                	newLeafNode(otherlv_5, grammarAccess.getEquivalentAxiomAccess().getGreaterThanSignKeyword_1_0_3_1_0());
                                
                            otherlv_6=(Token)match(input,50,FOLLOW_7); 

                                	newLeafNode(otherlv_6, grammarAccess.getEquivalentAxiomAccess().getOwlEquivalentClassKeyword_1_0_3_1_1());
                                
                            otherlv_7=(Token)match(input,21,FOLLOW_2); 

                                	newLeafNode(otherlv_7, grammarAccess.getEquivalentAxiomAccess().getGreaterThanSignKeyword_1_0_3_1_2());
                                

                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:2429:6: ( (otherlv_8= 'rdf:parseType' otherlv_9= '=' ( (lv_parseType_10_0= ruleParseType ) ) )? otherlv_11= '>' ( (lv_classes_12_0= ruleClassModifier ) )+ otherlv_13= '</owl:equivalentClass' otherlv_14= '>' )
                    {
                    // InternalOWLModel.g:2429:6: ( (otherlv_8= 'rdf:parseType' otherlv_9= '=' ( (lv_parseType_10_0= ruleParseType ) ) )? otherlv_11= '>' ( (lv_classes_12_0= ruleClassModifier ) )+ otherlv_13= '</owl:equivalentClass' otherlv_14= '>' )
                    // InternalOWLModel.g:2429:7: (otherlv_8= 'rdf:parseType' otherlv_9= '=' ( (lv_parseType_10_0= ruleParseType ) ) )? otherlv_11= '>' ( (lv_classes_12_0= ruleClassModifier ) )+ otherlv_13= '</owl:equivalentClass' otherlv_14= '>'
                    {
                    // InternalOWLModel.g:2429:7: (otherlv_8= 'rdf:parseType' otherlv_9= '=' ( (lv_parseType_10_0= ruleParseType ) ) )?
                    int alt52=2;
                    int LA52_0 = input.LA(1);

                    if ( (LA52_0==44) ) {
                        alt52=1;
                    }
                    switch (alt52) {
                        case 1 :
                            // InternalOWLModel.g:2429:9: otherlv_8= 'rdf:parseType' otherlv_9= '=' ( (lv_parseType_10_0= ruleParseType ) )
                            {
                            otherlv_8=(Token)match(input,44,FOLLOW_13); 

                                	newLeafNode(otherlv_8, grammarAccess.getEquivalentAxiomAccess().getRdfParseTypeKeyword_1_1_0_0());
                                
                            otherlv_9=(Token)match(input,25,FOLLOW_9); 

                                	newLeafNode(otherlv_9, grammarAccess.getEquivalentAxiomAccess().getEqualsSignKeyword_1_1_0_1());
                                
                            // InternalOWLModel.g:2437:1: ( (lv_parseType_10_0= ruleParseType ) )
                            // InternalOWLModel.g:2438:1: (lv_parseType_10_0= ruleParseType )
                            {
                            // InternalOWLModel.g:2438:1: (lv_parseType_10_0= ruleParseType )
                            // InternalOWLModel.g:2439:3: lv_parseType_10_0= ruleParseType
                            {
                             
                            	        newCompositeNode(grammarAccess.getEquivalentAxiomAccess().getParseTypeParseTypeParserRuleCall_1_1_0_2_0()); 
                            	    
                            pushFollow(FOLLOW_7);
                            lv_parseType_10_0=ruleParseType();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getEquivalentAxiomRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"parseType",
                                    		lv_parseType_10_0, 
                                    		"org.smool.sdk.owlparser.OWLModel.ParseType");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }

                    otherlv_11=(Token)match(input,21,FOLLOW_39); 

                        	newLeafNode(otherlv_11, grammarAccess.getEquivalentAxiomAccess().getGreaterThanSignKeyword_1_1_1());
                        
                    // InternalOWLModel.g:2459:1: ( (lv_classes_12_0= ruleClassModifier ) )+
                    int cnt53=0;
                    loop53:
                    do {
                        int alt53=2;
                        int LA53_0 = input.LA(1);

                        if ( (LA53_0==39||LA53_0==70||LA53_0==122) ) {
                            alt53=1;
                        }


                        switch (alt53) {
                    	case 1 :
                    	    // InternalOWLModel.g:2460:1: (lv_classes_12_0= ruleClassModifier )
                    	    {
                    	    // InternalOWLModel.g:2460:1: (lv_classes_12_0= ruleClassModifier )
                    	    // InternalOWLModel.g:2461:3: lv_classes_12_0= ruleClassModifier
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getEquivalentAxiomAccess().getClassesClassModifierParserRuleCall_1_1_2_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_47);
                    	    lv_classes_12_0=ruleClassModifier();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getEquivalentAxiomRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"classes",
                    	            		lv_classes_12_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.ClassModifier");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt53 >= 1 ) break loop53;
                                EarlyExitException eee =
                                    new EarlyExitException(53, input);
                                throw eee;
                        }
                        cnt53++;
                    } while (true);

                    otherlv_13=(Token)match(input,50,FOLLOW_7); 

                        	newLeafNode(otherlv_13, grammarAccess.getEquivalentAxiomAccess().getOwlEquivalentClassKeyword_1_1_3());
                        
                    otherlv_14=(Token)match(input,21,FOLLOW_2); 

                        	newLeafNode(otherlv_14, grammarAccess.getEquivalentAxiomAccess().getGreaterThanSignKeyword_1_1_4());
                        

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEquivalentAxiom"


    // $ANTLR start "entryRuleUnionAxiom"
    // InternalOWLModel.g:2493:1: entryRuleUnionAxiom returns [EObject current=null] : iv_ruleUnionAxiom= ruleUnionAxiom EOF ;
    public final EObject entryRuleUnionAxiom() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUnionAxiom = null;


        try {
            // InternalOWLModel.g:2494:2: (iv_ruleUnionAxiom= ruleUnionAxiom EOF )
            // InternalOWLModel.g:2495:2: iv_ruleUnionAxiom= ruleUnionAxiom EOF
            {
             newCompositeNode(grammarAccess.getUnionAxiomRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleUnionAxiom=ruleUnionAxiom();

            state._fsp--;

             current =iv_ruleUnionAxiom; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUnionAxiom"


    // $ANTLR start "ruleUnionAxiom"
    // InternalOWLModel.g:2502:1: ruleUnionAxiom returns [EObject current=null] : (otherlv_0= '<owl:unionOf' (otherlv_1= 'rdf:parseType' otherlv_2= '=' ( (lv_parseType_3_0= ruleParseType ) ) )? otherlv_4= '>' ( (lv_classes_5_0= ruleClassModifier ) )+ otherlv_6= '</owl:unionOf' otherlv_7= '>' ) ;
    public final EObject ruleUnionAxiom() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        EObject lv_parseType_3_0 = null;

        EObject lv_classes_5_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:2505:28: ( (otherlv_0= '<owl:unionOf' (otherlv_1= 'rdf:parseType' otherlv_2= '=' ( (lv_parseType_3_0= ruleParseType ) ) )? otherlv_4= '>' ( (lv_classes_5_0= ruleClassModifier ) )+ otherlv_6= '</owl:unionOf' otherlv_7= '>' ) )
            // InternalOWLModel.g:2506:1: (otherlv_0= '<owl:unionOf' (otherlv_1= 'rdf:parseType' otherlv_2= '=' ( (lv_parseType_3_0= ruleParseType ) ) )? otherlv_4= '>' ( (lv_classes_5_0= ruleClassModifier ) )+ otherlv_6= '</owl:unionOf' otherlv_7= '>' )
            {
            // InternalOWLModel.g:2506:1: (otherlv_0= '<owl:unionOf' (otherlv_1= 'rdf:parseType' otherlv_2= '=' ( (lv_parseType_3_0= ruleParseType ) ) )? otherlv_4= '>' ( (lv_classes_5_0= ruleClassModifier ) )+ otherlv_6= '</owl:unionOf' otherlv_7= '>' )
            // InternalOWLModel.g:2506:3: otherlv_0= '<owl:unionOf' (otherlv_1= 'rdf:parseType' otherlv_2= '=' ( (lv_parseType_3_0= ruleParseType ) ) )? otherlv_4= '>' ( (lv_classes_5_0= ruleClassModifier ) )+ otherlv_6= '</owl:unionOf' otherlv_7= '>'
            {
            otherlv_0=(Token)match(input,51,FOLLOW_48); 

                	newLeafNode(otherlv_0, grammarAccess.getUnionAxiomAccess().getOwlUnionOfKeyword_0());
                
            // InternalOWLModel.g:2510:1: (otherlv_1= 'rdf:parseType' otherlv_2= '=' ( (lv_parseType_3_0= ruleParseType ) ) )?
            int alt55=2;
            int LA55_0 = input.LA(1);

            if ( (LA55_0==44) ) {
                alt55=1;
            }
            switch (alt55) {
                case 1 :
                    // InternalOWLModel.g:2510:3: otherlv_1= 'rdf:parseType' otherlv_2= '=' ( (lv_parseType_3_0= ruleParseType ) )
                    {
                    otherlv_1=(Token)match(input,44,FOLLOW_13); 

                        	newLeafNode(otherlv_1, grammarAccess.getUnionAxiomAccess().getRdfParseTypeKeyword_1_0());
                        
                    otherlv_2=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_2, grammarAccess.getUnionAxiomAccess().getEqualsSignKeyword_1_1());
                        
                    // InternalOWLModel.g:2518:1: ( (lv_parseType_3_0= ruleParseType ) )
                    // InternalOWLModel.g:2519:1: (lv_parseType_3_0= ruleParseType )
                    {
                    // InternalOWLModel.g:2519:1: (lv_parseType_3_0= ruleParseType )
                    // InternalOWLModel.g:2520:3: lv_parseType_3_0= ruleParseType
                    {
                     
                    	        newCompositeNode(grammarAccess.getUnionAxiomAccess().getParseTypeParseTypeParserRuleCall_1_2_0()); 
                    	    
                    pushFollow(FOLLOW_7);
                    lv_parseType_3_0=ruleParseType();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getUnionAxiomRule());
                    	        }
                           		set(
                           			current, 
                           			"parseType",
                            		lv_parseType_3_0, 
                            		"org.smool.sdk.owlparser.OWLModel.ParseType");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,21,FOLLOW_39); 

                	newLeafNode(otherlv_4, grammarAccess.getUnionAxiomAccess().getGreaterThanSignKeyword_2());
                
            // InternalOWLModel.g:2540:1: ( (lv_classes_5_0= ruleClassModifier ) )+
            int cnt56=0;
            loop56:
            do {
                int alt56=2;
                int LA56_0 = input.LA(1);

                if ( (LA56_0==39||LA56_0==70||LA56_0==122) ) {
                    alt56=1;
                }


                switch (alt56) {
            	case 1 :
            	    // InternalOWLModel.g:2541:1: (lv_classes_5_0= ruleClassModifier )
            	    {
            	    // InternalOWLModel.g:2541:1: (lv_classes_5_0= ruleClassModifier )
            	    // InternalOWLModel.g:2542:3: lv_classes_5_0= ruleClassModifier
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getUnionAxiomAccess().getClassesClassModifierParserRuleCall_3_0()); 
            	    	    
            	    pushFollow(FOLLOW_49);
            	    lv_classes_5_0=ruleClassModifier();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getUnionAxiomRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"classes",
            	            		lv_classes_5_0, 
            	            		"org.smool.sdk.owlparser.OWLModel.ClassModifier");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt56 >= 1 ) break loop56;
                        EarlyExitException eee =
                            new EarlyExitException(56, input);
                        throw eee;
                }
                cnt56++;
            } while (true);

            otherlv_6=(Token)match(input,52,FOLLOW_7); 

                	newLeafNode(otherlv_6, grammarAccess.getUnionAxiomAccess().getOwlUnionOfKeyword_4());
                
            otherlv_7=(Token)match(input,21,FOLLOW_2); 

                	newLeafNode(otherlv_7, grammarAccess.getUnionAxiomAccess().getGreaterThanSignKeyword_5());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnionAxiom"


    // $ANTLR start "entryRuleIntersectionAxiom"
    // InternalOWLModel.g:2574:1: entryRuleIntersectionAxiom returns [EObject current=null] : iv_ruleIntersectionAxiom= ruleIntersectionAxiom EOF ;
    public final EObject entryRuleIntersectionAxiom() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntersectionAxiom = null;


        try {
            // InternalOWLModel.g:2575:2: (iv_ruleIntersectionAxiom= ruleIntersectionAxiom EOF )
            // InternalOWLModel.g:2576:2: iv_ruleIntersectionAxiom= ruleIntersectionAxiom EOF
            {
             newCompositeNode(grammarAccess.getIntersectionAxiomRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleIntersectionAxiom=ruleIntersectionAxiom();

            state._fsp--;

             current =iv_ruleIntersectionAxiom; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntersectionAxiom"


    // $ANTLR start "ruleIntersectionAxiom"
    // InternalOWLModel.g:2583:1: ruleIntersectionAxiom returns [EObject current=null] : (otherlv_0= '<owl:intersectionOf' (otherlv_1= 'rdf:parseType' otherlv_2= '=' ( (lv_parseType_3_0= ruleParseType ) ) )? otherlv_4= '>' ( (lv_classes_5_0= ruleClassModifier ) )+ otherlv_6= '</owl:intersectionOf' otherlv_7= '>' ) ;
    public final EObject ruleIntersectionAxiom() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        EObject lv_parseType_3_0 = null;

        EObject lv_classes_5_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:2586:28: ( (otherlv_0= '<owl:intersectionOf' (otherlv_1= 'rdf:parseType' otherlv_2= '=' ( (lv_parseType_3_0= ruleParseType ) ) )? otherlv_4= '>' ( (lv_classes_5_0= ruleClassModifier ) )+ otherlv_6= '</owl:intersectionOf' otherlv_7= '>' ) )
            // InternalOWLModel.g:2587:1: (otherlv_0= '<owl:intersectionOf' (otherlv_1= 'rdf:parseType' otherlv_2= '=' ( (lv_parseType_3_0= ruleParseType ) ) )? otherlv_4= '>' ( (lv_classes_5_0= ruleClassModifier ) )+ otherlv_6= '</owl:intersectionOf' otherlv_7= '>' )
            {
            // InternalOWLModel.g:2587:1: (otherlv_0= '<owl:intersectionOf' (otherlv_1= 'rdf:parseType' otherlv_2= '=' ( (lv_parseType_3_0= ruleParseType ) ) )? otherlv_4= '>' ( (lv_classes_5_0= ruleClassModifier ) )+ otherlv_6= '</owl:intersectionOf' otherlv_7= '>' )
            // InternalOWLModel.g:2587:3: otherlv_0= '<owl:intersectionOf' (otherlv_1= 'rdf:parseType' otherlv_2= '=' ( (lv_parseType_3_0= ruleParseType ) ) )? otherlv_4= '>' ( (lv_classes_5_0= ruleClassModifier ) )+ otherlv_6= '</owl:intersectionOf' otherlv_7= '>'
            {
            otherlv_0=(Token)match(input,53,FOLLOW_48); 

                	newLeafNode(otherlv_0, grammarAccess.getIntersectionAxiomAccess().getOwlIntersectionOfKeyword_0());
                
            // InternalOWLModel.g:2591:1: (otherlv_1= 'rdf:parseType' otherlv_2= '=' ( (lv_parseType_3_0= ruleParseType ) ) )?
            int alt57=2;
            int LA57_0 = input.LA(1);

            if ( (LA57_0==44) ) {
                alt57=1;
            }
            switch (alt57) {
                case 1 :
                    // InternalOWLModel.g:2591:3: otherlv_1= 'rdf:parseType' otherlv_2= '=' ( (lv_parseType_3_0= ruleParseType ) )
                    {
                    otherlv_1=(Token)match(input,44,FOLLOW_13); 

                        	newLeafNode(otherlv_1, grammarAccess.getIntersectionAxiomAccess().getRdfParseTypeKeyword_1_0());
                        
                    otherlv_2=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_2, grammarAccess.getIntersectionAxiomAccess().getEqualsSignKeyword_1_1());
                        
                    // InternalOWLModel.g:2599:1: ( (lv_parseType_3_0= ruleParseType ) )
                    // InternalOWLModel.g:2600:1: (lv_parseType_3_0= ruleParseType )
                    {
                    // InternalOWLModel.g:2600:1: (lv_parseType_3_0= ruleParseType )
                    // InternalOWLModel.g:2601:3: lv_parseType_3_0= ruleParseType
                    {
                     
                    	        newCompositeNode(grammarAccess.getIntersectionAxiomAccess().getParseTypeParseTypeParserRuleCall_1_2_0()); 
                    	    
                    pushFollow(FOLLOW_7);
                    lv_parseType_3_0=ruleParseType();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getIntersectionAxiomRule());
                    	        }
                           		set(
                           			current, 
                           			"parseType",
                            		lv_parseType_3_0, 
                            		"org.smool.sdk.owlparser.OWLModel.ParseType");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,21,FOLLOW_39); 

                	newLeafNode(otherlv_4, grammarAccess.getIntersectionAxiomAccess().getGreaterThanSignKeyword_2());
                
            // InternalOWLModel.g:2621:1: ( (lv_classes_5_0= ruleClassModifier ) )+
            int cnt58=0;
            loop58:
            do {
                int alt58=2;
                int LA58_0 = input.LA(1);

                if ( (LA58_0==39||LA58_0==70||LA58_0==122) ) {
                    alt58=1;
                }


                switch (alt58) {
            	case 1 :
            	    // InternalOWLModel.g:2622:1: (lv_classes_5_0= ruleClassModifier )
            	    {
            	    // InternalOWLModel.g:2622:1: (lv_classes_5_0= ruleClassModifier )
            	    // InternalOWLModel.g:2623:3: lv_classes_5_0= ruleClassModifier
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getIntersectionAxiomAccess().getClassesClassModifierParserRuleCall_3_0()); 
            	    	    
            	    pushFollow(FOLLOW_50);
            	    lv_classes_5_0=ruleClassModifier();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getIntersectionAxiomRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"classes",
            	            		lv_classes_5_0, 
            	            		"org.smool.sdk.owlparser.OWLModel.ClassModifier");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt58 >= 1 ) break loop58;
                        EarlyExitException eee =
                            new EarlyExitException(58, input);
                        throw eee;
                }
                cnt58++;
            } while (true);

            otherlv_6=(Token)match(input,54,FOLLOW_7); 

                	newLeafNode(otherlv_6, grammarAccess.getIntersectionAxiomAccess().getOwlIntersectionOfKeyword_4());
                
            otherlv_7=(Token)match(input,21,FOLLOW_2); 

                	newLeafNode(otherlv_7, grammarAccess.getIntersectionAxiomAccess().getGreaterThanSignKeyword_5());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntersectionAxiom"


    // $ANTLR start "entryRuleOneOfAxiom"
    // InternalOWLModel.g:2655:1: entryRuleOneOfAxiom returns [EObject current=null] : iv_ruleOneOfAxiom= ruleOneOfAxiom EOF ;
    public final EObject entryRuleOneOfAxiom() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOneOfAxiom = null;


        try {
            // InternalOWLModel.g:2656:2: (iv_ruleOneOfAxiom= ruleOneOfAxiom EOF )
            // InternalOWLModel.g:2657:2: iv_ruleOneOfAxiom= ruleOneOfAxiom EOF
            {
             newCompositeNode(grammarAccess.getOneOfAxiomRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOneOfAxiom=ruleOneOfAxiom();

            state._fsp--;

             current =iv_ruleOneOfAxiom; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOneOfAxiom"


    // $ANTLR start "ruleOneOfAxiom"
    // InternalOWLModel.g:2664:1: ruleOneOfAxiom returns [EObject current=null] : (otherlv_0= '<owl:oneOf' (otherlv_1= 'rdf:parseType' otherlv_2= '=' ( (lv_parseType_3_0= ruleParseType ) ) )? otherlv_4= '>' ( (otherlv_5= '<owl:Thing' otherlv_6= 'rdf:about' otherlv_7= '=' ( ( (lv_values_8_0= ruleOwlID ) ) | ( (lv_values_9_0= ruleOwlRef ) ) ) (otherlv_10= '/>' | (otherlv_11= '>' otherlv_12= '</owl:Thing' otherlv_13= '>' ) ) ) | (otherlv_14= '<' ( ( (lv_tagNames_15_1= RULE_NSID | lv_tagNames_15_2= RULE_ID ) ) ) ( (otherlv_16= 'rdf:ID' otherlv_17= '=' ( (lv_tagids_18_0= ruleOwlID ) ) ) | (otherlv_19= 'rdf:about' otherlv_20= '=' ( (lv_tagids_21_0= ruleOwlRef ) ) ) ) otherlv_22= '/>' ) | ( (lv_first_23_0= ruleOwlFirst ) ) | ( (lv_rest_24_0= ruleRDFRest ) ) )+ otherlv_25= '</owl:oneOf' otherlv_26= '>' ) ;
    public final EObject ruleOneOfAxiom() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token lv_tagNames_15_1=null;
        Token lv_tagNames_15_2=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_20=null;
        Token otherlv_22=null;
        Token otherlv_25=null;
        Token otherlv_26=null;
        EObject lv_parseType_3_0 = null;

        EObject lv_values_8_0 = null;

        EObject lv_values_9_0 = null;

        EObject lv_tagids_18_0 = null;

        EObject lv_tagids_21_0 = null;

        EObject lv_first_23_0 = null;

        EObject lv_rest_24_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:2667:28: ( (otherlv_0= '<owl:oneOf' (otherlv_1= 'rdf:parseType' otherlv_2= '=' ( (lv_parseType_3_0= ruleParseType ) ) )? otherlv_4= '>' ( (otherlv_5= '<owl:Thing' otherlv_6= 'rdf:about' otherlv_7= '=' ( ( (lv_values_8_0= ruleOwlID ) ) | ( (lv_values_9_0= ruleOwlRef ) ) ) (otherlv_10= '/>' | (otherlv_11= '>' otherlv_12= '</owl:Thing' otherlv_13= '>' ) ) ) | (otherlv_14= '<' ( ( (lv_tagNames_15_1= RULE_NSID | lv_tagNames_15_2= RULE_ID ) ) ) ( (otherlv_16= 'rdf:ID' otherlv_17= '=' ( (lv_tagids_18_0= ruleOwlID ) ) ) | (otherlv_19= 'rdf:about' otherlv_20= '=' ( (lv_tagids_21_0= ruleOwlRef ) ) ) ) otherlv_22= '/>' ) | ( (lv_first_23_0= ruleOwlFirst ) ) | ( (lv_rest_24_0= ruleRDFRest ) ) )+ otherlv_25= '</owl:oneOf' otherlv_26= '>' ) )
            // InternalOWLModel.g:2668:1: (otherlv_0= '<owl:oneOf' (otherlv_1= 'rdf:parseType' otherlv_2= '=' ( (lv_parseType_3_0= ruleParseType ) ) )? otherlv_4= '>' ( (otherlv_5= '<owl:Thing' otherlv_6= 'rdf:about' otherlv_7= '=' ( ( (lv_values_8_0= ruleOwlID ) ) | ( (lv_values_9_0= ruleOwlRef ) ) ) (otherlv_10= '/>' | (otherlv_11= '>' otherlv_12= '</owl:Thing' otherlv_13= '>' ) ) ) | (otherlv_14= '<' ( ( (lv_tagNames_15_1= RULE_NSID | lv_tagNames_15_2= RULE_ID ) ) ) ( (otherlv_16= 'rdf:ID' otherlv_17= '=' ( (lv_tagids_18_0= ruleOwlID ) ) ) | (otherlv_19= 'rdf:about' otherlv_20= '=' ( (lv_tagids_21_0= ruleOwlRef ) ) ) ) otherlv_22= '/>' ) | ( (lv_first_23_0= ruleOwlFirst ) ) | ( (lv_rest_24_0= ruleRDFRest ) ) )+ otherlv_25= '</owl:oneOf' otherlv_26= '>' )
            {
            // InternalOWLModel.g:2668:1: (otherlv_0= '<owl:oneOf' (otherlv_1= 'rdf:parseType' otherlv_2= '=' ( (lv_parseType_3_0= ruleParseType ) ) )? otherlv_4= '>' ( (otherlv_5= '<owl:Thing' otherlv_6= 'rdf:about' otherlv_7= '=' ( ( (lv_values_8_0= ruleOwlID ) ) | ( (lv_values_9_0= ruleOwlRef ) ) ) (otherlv_10= '/>' | (otherlv_11= '>' otherlv_12= '</owl:Thing' otherlv_13= '>' ) ) ) | (otherlv_14= '<' ( ( (lv_tagNames_15_1= RULE_NSID | lv_tagNames_15_2= RULE_ID ) ) ) ( (otherlv_16= 'rdf:ID' otherlv_17= '=' ( (lv_tagids_18_0= ruleOwlID ) ) ) | (otherlv_19= 'rdf:about' otherlv_20= '=' ( (lv_tagids_21_0= ruleOwlRef ) ) ) ) otherlv_22= '/>' ) | ( (lv_first_23_0= ruleOwlFirst ) ) | ( (lv_rest_24_0= ruleRDFRest ) ) )+ otherlv_25= '</owl:oneOf' otherlv_26= '>' )
            // InternalOWLModel.g:2668:3: otherlv_0= '<owl:oneOf' (otherlv_1= 'rdf:parseType' otherlv_2= '=' ( (lv_parseType_3_0= ruleParseType ) ) )? otherlv_4= '>' ( (otherlv_5= '<owl:Thing' otherlv_6= 'rdf:about' otherlv_7= '=' ( ( (lv_values_8_0= ruleOwlID ) ) | ( (lv_values_9_0= ruleOwlRef ) ) ) (otherlv_10= '/>' | (otherlv_11= '>' otherlv_12= '</owl:Thing' otherlv_13= '>' ) ) ) | (otherlv_14= '<' ( ( (lv_tagNames_15_1= RULE_NSID | lv_tagNames_15_2= RULE_ID ) ) ) ( (otherlv_16= 'rdf:ID' otherlv_17= '=' ( (lv_tagids_18_0= ruleOwlID ) ) ) | (otherlv_19= 'rdf:about' otherlv_20= '=' ( (lv_tagids_21_0= ruleOwlRef ) ) ) ) otherlv_22= '/>' ) | ( (lv_first_23_0= ruleOwlFirst ) ) | ( (lv_rest_24_0= ruleRDFRest ) ) )+ otherlv_25= '</owl:oneOf' otherlv_26= '>'
            {
            otherlv_0=(Token)match(input,55,FOLLOW_48); 

                	newLeafNode(otherlv_0, grammarAccess.getOneOfAxiomAccess().getOwlOneOfKeyword_0());
                
            // InternalOWLModel.g:2672:1: (otherlv_1= 'rdf:parseType' otherlv_2= '=' ( (lv_parseType_3_0= ruleParseType ) ) )?
            int alt59=2;
            int LA59_0 = input.LA(1);

            if ( (LA59_0==44) ) {
                alt59=1;
            }
            switch (alt59) {
                case 1 :
                    // InternalOWLModel.g:2672:3: otherlv_1= 'rdf:parseType' otherlv_2= '=' ( (lv_parseType_3_0= ruleParseType ) )
                    {
                    otherlv_1=(Token)match(input,44,FOLLOW_13); 

                        	newLeafNode(otherlv_1, grammarAccess.getOneOfAxiomAccess().getRdfParseTypeKeyword_1_0());
                        
                    otherlv_2=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_2, grammarAccess.getOneOfAxiomAccess().getEqualsSignKeyword_1_1());
                        
                    // InternalOWLModel.g:2680:1: ( (lv_parseType_3_0= ruleParseType ) )
                    // InternalOWLModel.g:2681:1: (lv_parseType_3_0= ruleParseType )
                    {
                    // InternalOWLModel.g:2681:1: (lv_parseType_3_0= ruleParseType )
                    // InternalOWLModel.g:2682:3: lv_parseType_3_0= ruleParseType
                    {
                     
                    	        newCompositeNode(grammarAccess.getOneOfAxiomAccess().getParseTypeParseTypeParserRuleCall_1_2_0()); 
                    	    
                    pushFollow(FOLLOW_7);
                    lv_parseType_3_0=ruleParseType();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getOneOfAxiomRule());
                    	        }
                           		set(
                           			current, 
                           			"parseType",
                            		lv_parseType_3_0, 
                            		"org.smool.sdk.owlparser.OWLModel.ParseType");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,21,FOLLOW_51); 

                	newLeafNode(otherlv_4, grammarAccess.getOneOfAxiomAccess().getGreaterThanSignKeyword_2());
                
            // InternalOWLModel.g:2702:1: ( (otherlv_5= '<owl:Thing' otherlv_6= 'rdf:about' otherlv_7= '=' ( ( (lv_values_8_0= ruleOwlID ) ) | ( (lv_values_9_0= ruleOwlRef ) ) ) (otherlv_10= '/>' | (otherlv_11= '>' otherlv_12= '</owl:Thing' otherlv_13= '>' ) ) ) | (otherlv_14= '<' ( ( (lv_tagNames_15_1= RULE_NSID | lv_tagNames_15_2= RULE_ID ) ) ) ( (otherlv_16= 'rdf:ID' otherlv_17= '=' ( (lv_tagids_18_0= ruleOwlID ) ) ) | (otherlv_19= 'rdf:about' otherlv_20= '=' ( (lv_tagids_21_0= ruleOwlRef ) ) ) ) otherlv_22= '/>' ) | ( (lv_first_23_0= ruleOwlFirst ) ) | ( (lv_rest_24_0= ruleRDFRest ) ) )+
            int cnt64=0;
            loop64:
            do {
                int alt64=5;
                switch ( input.LA(1) ) {
                case 56:
                    {
                    alt64=1;
                    }
                    break;
                case 58:
                    {
                    alt64=2;
                    }
                    break;
                case 124:
                    {
                    alt64=3;
                    }
                    break;
                case 126:
                    {
                    alt64=4;
                    }
                    break;

                }

                switch (alt64) {
            	case 1 :
            	    // InternalOWLModel.g:2702:2: (otherlv_5= '<owl:Thing' otherlv_6= 'rdf:about' otherlv_7= '=' ( ( (lv_values_8_0= ruleOwlID ) ) | ( (lv_values_9_0= ruleOwlRef ) ) ) (otherlv_10= '/>' | (otherlv_11= '>' otherlv_12= '</owl:Thing' otherlv_13= '>' ) ) )
            	    {
            	    // InternalOWLModel.g:2702:2: (otherlv_5= '<owl:Thing' otherlv_6= 'rdf:about' otherlv_7= '=' ( ( (lv_values_8_0= ruleOwlID ) ) | ( (lv_values_9_0= ruleOwlRef ) ) ) (otherlv_10= '/>' | (otherlv_11= '>' otherlv_12= '</owl:Thing' otherlv_13= '>' ) ) )
            	    // InternalOWLModel.g:2702:4: otherlv_5= '<owl:Thing' otherlv_6= 'rdf:about' otherlv_7= '=' ( ( (lv_values_8_0= ruleOwlID ) ) | ( (lv_values_9_0= ruleOwlRef ) ) ) (otherlv_10= '/>' | (otherlv_11= '>' otherlv_12= '</owl:Thing' otherlv_13= '>' ) )
            	    {
            	    otherlv_5=(Token)match(input,56,FOLLOW_52); 

            	        	newLeafNode(otherlv_5, grammarAccess.getOneOfAxiomAccess().getOwlThingKeyword_3_0_0());
            	        
            	    otherlv_6=(Token)match(input,28,FOLLOW_13); 

            	        	newLeafNode(otherlv_6, grammarAccess.getOneOfAxiomAccess().getRdfAboutKeyword_3_0_1());
            	        
            	    otherlv_7=(Token)match(input,25,FOLLOW_9); 

            	        	newLeafNode(otherlv_7, grammarAccess.getOneOfAxiomAccess().getEqualsSignKeyword_3_0_2());
            	        
            	    // InternalOWLModel.g:2714:1: ( ( (lv_values_8_0= ruleOwlID ) ) | ( (lv_values_9_0= ruleOwlRef ) ) )
            	    int alt60=2;
            	    int LA60_0 = input.LA(1);

            	    if ( (LA60_0==RULE_STR_DELIMITER) ) {
            	        int LA60_1 = input.LA(2);

            	        if ( ((LA60_1>=RULE_URI && LA60_1<=RULE_AMPERSAND)) ) {
            	            alt60=2;
            	        }
            	        else if ( ((LA60_1>=RULE_ID && LA60_1<=RULE_STR_DELIMITER)) ) {
            	            alt60=1;
            	        }
            	        else {
            	            NoViableAltException nvae =
            	                new NoViableAltException("", 60, 1, input);

            	            throw nvae;
            	        }
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 60, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt60) {
            	        case 1 :
            	            // InternalOWLModel.g:2714:2: ( (lv_values_8_0= ruleOwlID ) )
            	            {
            	            // InternalOWLModel.g:2714:2: ( (lv_values_8_0= ruleOwlID ) )
            	            // InternalOWLModel.g:2715:1: (lv_values_8_0= ruleOwlID )
            	            {
            	            // InternalOWLModel.g:2715:1: (lv_values_8_0= ruleOwlID )
            	            // InternalOWLModel.g:2716:3: lv_values_8_0= ruleOwlID
            	            {
            	             
            	            	        newCompositeNode(grammarAccess.getOneOfAxiomAccess().getValuesOwlIDParserRuleCall_3_0_3_0_0()); 
            	            	    
            	            pushFollow(FOLLOW_24);
            	            lv_values_8_0=ruleOwlID();

            	            state._fsp--;


            	            	        if (current==null) {
            	            	            current = createModelElementForParent(grammarAccess.getOneOfAxiomRule());
            	            	        }
            	                   		add(
            	                   			current, 
            	                   			"values",
            	                    		lv_values_8_0, 
            	                    		"org.smool.sdk.owlparser.OWLModel.OwlID");
            	            	        afterParserOrEnumRuleCall();
            	            	    

            	            }


            	            }


            	            }
            	            break;
            	        case 2 :
            	            // InternalOWLModel.g:2733:6: ( (lv_values_9_0= ruleOwlRef ) )
            	            {
            	            // InternalOWLModel.g:2733:6: ( (lv_values_9_0= ruleOwlRef ) )
            	            // InternalOWLModel.g:2734:1: (lv_values_9_0= ruleOwlRef )
            	            {
            	            // InternalOWLModel.g:2734:1: (lv_values_9_0= ruleOwlRef )
            	            // InternalOWLModel.g:2735:3: lv_values_9_0= ruleOwlRef
            	            {
            	             
            	            	        newCompositeNode(grammarAccess.getOneOfAxiomAccess().getValuesOwlRefParserRuleCall_3_0_3_1_0()); 
            	            	    
            	            pushFollow(FOLLOW_24);
            	            lv_values_9_0=ruleOwlRef();

            	            state._fsp--;


            	            	        if (current==null) {
            	            	            current = createModelElementForParent(grammarAccess.getOneOfAxiomRule());
            	            	        }
            	                   		add(
            	                   			current, 
            	                   			"values",
            	                    		lv_values_9_0, 
            	                    		"org.smool.sdk.owlparser.OWLModel.OwlRef");
            	            	        afterParserOrEnumRuleCall();
            	            	    

            	            }


            	            }


            	            }
            	            break;

            	    }

            	    // InternalOWLModel.g:2751:3: (otherlv_10= '/>' | (otherlv_11= '>' otherlv_12= '</owl:Thing' otherlv_13= '>' ) )
            	    int alt61=2;
            	    int LA61_0 = input.LA(1);

            	    if ( (LA61_0==29) ) {
            	        alt61=1;
            	    }
            	    else if ( (LA61_0==21) ) {
            	        alt61=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 61, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt61) {
            	        case 1 :
            	            // InternalOWLModel.g:2751:5: otherlv_10= '/>'
            	            {
            	            otherlv_10=(Token)match(input,29,FOLLOW_53); 

            	                	newLeafNode(otherlv_10, grammarAccess.getOneOfAxiomAccess().getSolidusGreaterThanSignKeyword_3_0_4_0());
            	                

            	            }
            	            break;
            	        case 2 :
            	            // InternalOWLModel.g:2756:6: (otherlv_11= '>' otherlv_12= '</owl:Thing' otherlv_13= '>' )
            	            {
            	            // InternalOWLModel.g:2756:6: (otherlv_11= '>' otherlv_12= '</owl:Thing' otherlv_13= '>' )
            	            // InternalOWLModel.g:2756:8: otherlv_11= '>' otherlv_12= '</owl:Thing' otherlv_13= '>'
            	            {
            	            otherlv_11=(Token)match(input,21,FOLLOW_54); 

            	                	newLeafNode(otherlv_11, grammarAccess.getOneOfAxiomAccess().getGreaterThanSignKeyword_3_0_4_1_0());
            	                
            	            otherlv_12=(Token)match(input,57,FOLLOW_7); 

            	                	newLeafNode(otherlv_12, grammarAccess.getOneOfAxiomAccess().getOwlThingKeyword_3_0_4_1_1());
            	                
            	            otherlv_13=(Token)match(input,21,FOLLOW_53); 

            	                	newLeafNode(otherlv_13, grammarAccess.getOneOfAxiomAccess().getGreaterThanSignKeyword_3_0_4_1_2());
            	                

            	            }


            	            }
            	            break;

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalOWLModel.g:2769:6: (otherlv_14= '<' ( ( (lv_tagNames_15_1= RULE_NSID | lv_tagNames_15_2= RULE_ID ) ) ) ( (otherlv_16= 'rdf:ID' otherlv_17= '=' ( (lv_tagids_18_0= ruleOwlID ) ) ) | (otherlv_19= 'rdf:about' otherlv_20= '=' ( (lv_tagids_21_0= ruleOwlRef ) ) ) ) otherlv_22= '/>' )
            	    {
            	    // InternalOWLModel.g:2769:6: (otherlv_14= '<' ( ( (lv_tagNames_15_1= RULE_NSID | lv_tagNames_15_2= RULE_ID ) ) ) ( (otherlv_16= 'rdf:ID' otherlv_17= '=' ( (lv_tagids_18_0= ruleOwlID ) ) ) | (otherlv_19= 'rdf:about' otherlv_20= '=' ( (lv_tagids_21_0= ruleOwlRef ) ) ) ) otherlv_22= '/>' )
            	    // InternalOWLModel.g:2769:8: otherlv_14= '<' ( ( (lv_tagNames_15_1= RULE_NSID | lv_tagNames_15_2= RULE_ID ) ) ) ( (otherlv_16= 'rdf:ID' otherlv_17= '=' ( (lv_tagids_18_0= ruleOwlID ) ) ) | (otherlv_19= 'rdf:about' otherlv_20= '=' ( (lv_tagids_21_0= ruleOwlRef ) ) ) ) otherlv_22= '/>'
            	    {
            	    otherlv_14=(Token)match(input,58,FOLLOW_55); 

            	        	newLeafNode(otherlv_14, grammarAccess.getOneOfAxiomAccess().getLessThanSignKeyword_3_1_0());
            	        
            	    // InternalOWLModel.g:2773:1: ( ( (lv_tagNames_15_1= RULE_NSID | lv_tagNames_15_2= RULE_ID ) ) )
            	    // InternalOWLModel.g:2774:1: ( (lv_tagNames_15_1= RULE_NSID | lv_tagNames_15_2= RULE_ID ) )
            	    {
            	    // InternalOWLModel.g:2774:1: ( (lv_tagNames_15_1= RULE_NSID | lv_tagNames_15_2= RULE_ID ) )
            	    // InternalOWLModel.g:2775:1: (lv_tagNames_15_1= RULE_NSID | lv_tagNames_15_2= RULE_ID )
            	    {
            	    // InternalOWLModel.g:2775:1: (lv_tagNames_15_1= RULE_NSID | lv_tagNames_15_2= RULE_ID )
            	    int alt62=2;
            	    int LA62_0 = input.LA(1);

            	    if ( (LA62_0==RULE_NSID) ) {
            	        alt62=1;
            	    }
            	    else if ( (LA62_0==RULE_ID) ) {
            	        alt62=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 62, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt62) {
            	        case 1 :
            	            // InternalOWLModel.g:2776:3: lv_tagNames_15_1= RULE_NSID
            	            {
            	            lv_tagNames_15_1=(Token)match(input,RULE_NSID,FOLLOW_34); 

            	            			newLeafNode(lv_tagNames_15_1, grammarAccess.getOneOfAxiomAccess().getTagNamesNSIDTerminalRuleCall_3_1_1_0_0()); 
            	            		

            	            	        if (current==null) {
            	            	            current = createModelElement(grammarAccess.getOneOfAxiomRule());
            	            	        }
            	                   		addWithLastConsumed(
            	                   			current, 
            	                   			"tagNames",
            	                    		lv_tagNames_15_1, 
            	                    		"org.smool.sdk.owlparser.OWLModel.NSID");
            	            	    

            	            }
            	            break;
            	        case 2 :
            	            // InternalOWLModel.g:2791:8: lv_tagNames_15_2= RULE_ID
            	            {
            	            lv_tagNames_15_2=(Token)match(input,RULE_ID,FOLLOW_34); 

            	            			newLeafNode(lv_tagNames_15_2, grammarAccess.getOneOfAxiomAccess().getTagNamesIDTerminalRuleCall_3_1_1_0_1()); 
            	            		

            	            	        if (current==null) {
            	            	            current = createModelElement(grammarAccess.getOneOfAxiomRule());
            	            	        }
            	                   		addWithLastConsumed(
            	                   			current, 
            	                   			"tagNames",
            	                    		lv_tagNames_15_2, 
            	                    		"org.smool.sdk.owlparser.OWLModel.ID");
            	            	    

            	            }
            	            break;

            	    }


            	    }


            	    }

            	    // InternalOWLModel.g:2809:2: ( (otherlv_16= 'rdf:ID' otherlv_17= '=' ( (lv_tagids_18_0= ruleOwlID ) ) ) | (otherlv_19= 'rdf:about' otherlv_20= '=' ( (lv_tagids_21_0= ruleOwlRef ) ) ) )
            	    int alt63=2;
            	    int LA63_0 = input.LA(1);

            	    if ( (LA63_0==40) ) {
            	        alt63=1;
            	    }
            	    else if ( (LA63_0==28) ) {
            	        alt63=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 63, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt63) {
            	        case 1 :
            	            // InternalOWLModel.g:2809:3: (otherlv_16= 'rdf:ID' otherlv_17= '=' ( (lv_tagids_18_0= ruleOwlID ) ) )
            	            {
            	            // InternalOWLModel.g:2809:3: (otherlv_16= 'rdf:ID' otherlv_17= '=' ( (lv_tagids_18_0= ruleOwlID ) ) )
            	            // InternalOWLModel.g:2809:5: otherlv_16= 'rdf:ID' otherlv_17= '=' ( (lv_tagids_18_0= ruleOwlID ) )
            	            {
            	            otherlv_16=(Token)match(input,40,FOLLOW_13); 

            	                	newLeafNode(otherlv_16, grammarAccess.getOneOfAxiomAccess().getRdfIDKeyword_3_1_2_0_0());
            	                
            	            otherlv_17=(Token)match(input,25,FOLLOW_9); 

            	                	newLeafNode(otherlv_17, grammarAccess.getOneOfAxiomAccess().getEqualsSignKeyword_3_1_2_0_1());
            	                
            	            // InternalOWLModel.g:2817:1: ( (lv_tagids_18_0= ruleOwlID ) )
            	            // InternalOWLModel.g:2818:1: (lv_tagids_18_0= ruleOwlID )
            	            {
            	            // InternalOWLModel.g:2818:1: (lv_tagids_18_0= ruleOwlID )
            	            // InternalOWLModel.g:2819:3: lv_tagids_18_0= ruleOwlID
            	            {
            	             
            	            	        newCompositeNode(grammarAccess.getOneOfAxiomAccess().getTagidsOwlIDParserRuleCall_3_1_2_0_2_0()); 
            	            	    
            	            pushFollow(FOLLOW_29);
            	            lv_tagids_18_0=ruleOwlID();

            	            state._fsp--;


            	            	        if (current==null) {
            	            	            current = createModelElementForParent(grammarAccess.getOneOfAxiomRule());
            	            	        }
            	                   		add(
            	                   			current, 
            	                   			"tagids",
            	                    		lv_tagids_18_0, 
            	                    		"org.smool.sdk.owlparser.OWLModel.OwlID");
            	            	        afterParserOrEnumRuleCall();
            	            	    

            	            }


            	            }


            	            }


            	            }
            	            break;
            	        case 2 :
            	            // InternalOWLModel.g:2836:6: (otherlv_19= 'rdf:about' otherlv_20= '=' ( (lv_tagids_21_0= ruleOwlRef ) ) )
            	            {
            	            // InternalOWLModel.g:2836:6: (otherlv_19= 'rdf:about' otherlv_20= '=' ( (lv_tagids_21_0= ruleOwlRef ) ) )
            	            // InternalOWLModel.g:2836:8: otherlv_19= 'rdf:about' otherlv_20= '=' ( (lv_tagids_21_0= ruleOwlRef ) )
            	            {
            	            otherlv_19=(Token)match(input,28,FOLLOW_13); 

            	                	newLeafNode(otherlv_19, grammarAccess.getOneOfAxiomAccess().getRdfAboutKeyword_3_1_2_1_0());
            	                
            	            otherlv_20=(Token)match(input,25,FOLLOW_9); 

            	                	newLeafNode(otherlv_20, grammarAccess.getOneOfAxiomAccess().getEqualsSignKeyword_3_1_2_1_1());
            	                
            	            // InternalOWLModel.g:2844:1: ( (lv_tagids_21_0= ruleOwlRef ) )
            	            // InternalOWLModel.g:2845:1: (lv_tagids_21_0= ruleOwlRef )
            	            {
            	            // InternalOWLModel.g:2845:1: (lv_tagids_21_0= ruleOwlRef )
            	            // InternalOWLModel.g:2846:3: lv_tagids_21_0= ruleOwlRef
            	            {
            	             
            	            	        newCompositeNode(grammarAccess.getOneOfAxiomAccess().getTagidsOwlRefParserRuleCall_3_1_2_1_2_0()); 
            	            	    
            	            pushFollow(FOLLOW_29);
            	            lv_tagids_21_0=ruleOwlRef();

            	            state._fsp--;


            	            	        if (current==null) {
            	            	            current = createModelElementForParent(grammarAccess.getOneOfAxiomRule());
            	            	        }
            	                   		add(
            	                   			current, 
            	                   			"tagids",
            	                    		lv_tagids_21_0, 
            	                    		"org.smool.sdk.owlparser.OWLModel.OwlRef");
            	            	        afterParserOrEnumRuleCall();
            	            	    

            	            }


            	            }


            	            }


            	            }
            	            break;

            	    }

            	    otherlv_22=(Token)match(input,29,FOLLOW_53); 

            	        	newLeafNode(otherlv_22, grammarAccess.getOneOfAxiomAccess().getSolidusGreaterThanSignKeyword_3_1_3());
            	        

            	    }


            	    }
            	    break;
            	case 3 :
            	    // InternalOWLModel.g:2867:6: ( (lv_first_23_0= ruleOwlFirst ) )
            	    {
            	    // InternalOWLModel.g:2867:6: ( (lv_first_23_0= ruleOwlFirst ) )
            	    // InternalOWLModel.g:2868:1: (lv_first_23_0= ruleOwlFirst )
            	    {
            	    // InternalOWLModel.g:2868:1: (lv_first_23_0= ruleOwlFirst )
            	    // InternalOWLModel.g:2869:3: lv_first_23_0= ruleOwlFirst
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getOneOfAxiomAccess().getFirstOwlFirstParserRuleCall_3_2_0()); 
            	    	    
            	    pushFollow(FOLLOW_53);
            	    lv_first_23_0=ruleOwlFirst();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getOneOfAxiomRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"first",
            	            		lv_first_23_0, 
            	            		"org.smool.sdk.owlparser.OWLModel.OwlFirst");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;
            	case 4 :
            	    // InternalOWLModel.g:2886:6: ( (lv_rest_24_0= ruleRDFRest ) )
            	    {
            	    // InternalOWLModel.g:2886:6: ( (lv_rest_24_0= ruleRDFRest ) )
            	    // InternalOWLModel.g:2887:1: (lv_rest_24_0= ruleRDFRest )
            	    {
            	    // InternalOWLModel.g:2887:1: (lv_rest_24_0= ruleRDFRest )
            	    // InternalOWLModel.g:2888:3: lv_rest_24_0= ruleRDFRest
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getOneOfAxiomAccess().getRestRDFRestParserRuleCall_3_3_0()); 
            	    	    
            	    pushFollow(FOLLOW_53);
            	    lv_rest_24_0=ruleRDFRest();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getOneOfAxiomRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"rest",
            	            		lv_rest_24_0, 
            	            		"org.smool.sdk.owlparser.OWLModel.RDFRest");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt64 >= 1 ) break loop64;
                        EarlyExitException eee =
                            new EarlyExitException(64, input);
                        throw eee;
                }
                cnt64++;
            } while (true);

            otherlv_25=(Token)match(input,59,FOLLOW_7); 

                	newLeafNode(otherlv_25, grammarAccess.getOneOfAxiomAccess().getOwlOneOfKeyword_4());
                
            otherlv_26=(Token)match(input,21,FOLLOW_2); 

                	newLeafNode(otherlv_26, grammarAccess.getOneOfAxiomAccess().getGreaterThanSignKeyword_5());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOneOfAxiom"


    // $ANTLR start "entryRuleOwlLabel"
    // InternalOWLModel.g:2920:1: entryRuleOwlLabel returns [EObject current=null] : iv_ruleOwlLabel= ruleOwlLabel EOF ;
    public final EObject entryRuleOwlLabel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOwlLabel = null;


        try {
            // InternalOWLModel.g:2921:2: (iv_ruleOwlLabel= ruleOwlLabel EOF )
            // InternalOWLModel.g:2922:2: iv_ruleOwlLabel= ruleOwlLabel EOF
            {
             newCompositeNode(grammarAccess.getOwlLabelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOwlLabel=ruleOwlLabel();

            state._fsp--;

             current =iv_ruleOwlLabel; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOwlLabel"


    // $ANTLR start "ruleOwlLabel"
    // InternalOWLModel.g:2929:1: ruleOwlLabel returns [EObject current=null] : (otherlv_0= '<rdfs:label' (otherlv_1= 'xml:lang' otherlv_2= '=' this_STR_DELIMITER_3= RULE_STR_DELIMITER ( (lv_lang_4_0= rulePARAM_ANY_VALUE ) ) this_STR_DELIMITER_5= RULE_STR_DELIMITER )? (otherlv_6= 'rdf:datatype' otherlv_7= '=' ( (lv_datatype_8_0= ruleOwlRef ) ) )? otherlv_9= '>' ( (lv_value_10_0= ruleTAG_ANY_VALUE ) ) otherlv_11= '</rdfs:label' otherlv_12= '>' ) ;
    public final EObject ruleOwlLabel() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token this_STR_DELIMITER_3=null;
        Token this_STR_DELIMITER_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        AntlrDatatypeRuleToken lv_lang_4_0 = null;

        EObject lv_datatype_8_0 = null;

        AntlrDatatypeRuleToken lv_value_10_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:2932:28: ( (otherlv_0= '<rdfs:label' (otherlv_1= 'xml:lang' otherlv_2= '=' this_STR_DELIMITER_3= RULE_STR_DELIMITER ( (lv_lang_4_0= rulePARAM_ANY_VALUE ) ) this_STR_DELIMITER_5= RULE_STR_DELIMITER )? (otherlv_6= 'rdf:datatype' otherlv_7= '=' ( (lv_datatype_8_0= ruleOwlRef ) ) )? otherlv_9= '>' ( (lv_value_10_0= ruleTAG_ANY_VALUE ) ) otherlv_11= '</rdfs:label' otherlv_12= '>' ) )
            // InternalOWLModel.g:2933:1: (otherlv_0= '<rdfs:label' (otherlv_1= 'xml:lang' otherlv_2= '=' this_STR_DELIMITER_3= RULE_STR_DELIMITER ( (lv_lang_4_0= rulePARAM_ANY_VALUE ) ) this_STR_DELIMITER_5= RULE_STR_DELIMITER )? (otherlv_6= 'rdf:datatype' otherlv_7= '=' ( (lv_datatype_8_0= ruleOwlRef ) ) )? otherlv_9= '>' ( (lv_value_10_0= ruleTAG_ANY_VALUE ) ) otherlv_11= '</rdfs:label' otherlv_12= '>' )
            {
            // InternalOWLModel.g:2933:1: (otherlv_0= '<rdfs:label' (otherlv_1= 'xml:lang' otherlv_2= '=' this_STR_DELIMITER_3= RULE_STR_DELIMITER ( (lv_lang_4_0= rulePARAM_ANY_VALUE ) ) this_STR_DELIMITER_5= RULE_STR_DELIMITER )? (otherlv_6= 'rdf:datatype' otherlv_7= '=' ( (lv_datatype_8_0= ruleOwlRef ) ) )? otherlv_9= '>' ( (lv_value_10_0= ruleTAG_ANY_VALUE ) ) otherlv_11= '</rdfs:label' otherlv_12= '>' )
            // InternalOWLModel.g:2933:3: otherlv_0= '<rdfs:label' (otherlv_1= 'xml:lang' otherlv_2= '=' this_STR_DELIMITER_3= RULE_STR_DELIMITER ( (lv_lang_4_0= rulePARAM_ANY_VALUE ) ) this_STR_DELIMITER_5= RULE_STR_DELIMITER )? (otherlv_6= 'rdf:datatype' otherlv_7= '=' ( (lv_datatype_8_0= ruleOwlRef ) ) )? otherlv_9= '>' ( (lv_value_10_0= ruleTAG_ANY_VALUE ) ) otherlv_11= '</rdfs:label' otherlv_12= '>'
            {
            otherlv_0=(Token)match(input,60,FOLLOW_56); 

                	newLeafNode(otherlv_0, grammarAccess.getOwlLabelAccess().getRdfsLabelKeyword_0());
                
            // InternalOWLModel.g:2937:1: (otherlv_1= 'xml:lang' otherlv_2= '=' this_STR_DELIMITER_3= RULE_STR_DELIMITER ( (lv_lang_4_0= rulePARAM_ANY_VALUE ) ) this_STR_DELIMITER_5= RULE_STR_DELIMITER )?
            int alt65=2;
            int LA65_0 = input.LA(1);

            if ( (LA65_0==61) ) {
                alt65=1;
            }
            switch (alt65) {
                case 1 :
                    // InternalOWLModel.g:2937:3: otherlv_1= 'xml:lang' otherlv_2= '=' this_STR_DELIMITER_3= RULE_STR_DELIMITER ( (lv_lang_4_0= rulePARAM_ANY_VALUE ) ) this_STR_DELIMITER_5= RULE_STR_DELIMITER
                    {
                    otherlv_1=(Token)match(input,61,FOLLOW_13); 

                        	newLeafNode(otherlv_1, grammarAccess.getOwlLabelAccess().getXmlLangKeyword_1_0());
                        
                    otherlv_2=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_2, grammarAccess.getOwlLabelAccess().getEqualsSignKeyword_1_1());
                        
                    this_STR_DELIMITER_3=(Token)match(input,RULE_STR_DELIMITER,FOLLOW_57); 
                     
                        newLeafNode(this_STR_DELIMITER_3, grammarAccess.getOwlLabelAccess().getSTR_DELIMITERTerminalRuleCall_1_2()); 
                        
                    // InternalOWLModel.g:2949:1: ( (lv_lang_4_0= rulePARAM_ANY_VALUE ) )
                    // InternalOWLModel.g:2950:1: (lv_lang_4_0= rulePARAM_ANY_VALUE )
                    {
                    // InternalOWLModel.g:2950:1: (lv_lang_4_0= rulePARAM_ANY_VALUE )
                    // InternalOWLModel.g:2951:3: lv_lang_4_0= rulePARAM_ANY_VALUE
                    {
                     
                    	        newCompositeNode(grammarAccess.getOwlLabelAccess().getLangPARAM_ANY_VALUEParserRuleCall_1_3_0()); 
                    	    
                    pushFollow(FOLLOW_9);
                    lv_lang_4_0=rulePARAM_ANY_VALUE();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getOwlLabelRule());
                    	        }
                           		set(
                           			current, 
                           			"lang",
                            		lv_lang_4_0, 
                            		"org.smool.sdk.owlparser.OWLModel.PARAM_ANY_VALUE");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    this_STR_DELIMITER_5=(Token)match(input,RULE_STR_DELIMITER,FOLLOW_58); 
                     
                        newLeafNode(this_STR_DELIMITER_5, grammarAccess.getOwlLabelAccess().getSTR_DELIMITERTerminalRuleCall_1_4()); 
                        

                    }
                    break;

            }

            // InternalOWLModel.g:2971:3: (otherlv_6= 'rdf:datatype' otherlv_7= '=' ( (lv_datatype_8_0= ruleOwlRef ) ) )?
            int alt66=2;
            int LA66_0 = input.LA(1);

            if ( (LA66_0==62) ) {
                alt66=1;
            }
            switch (alt66) {
                case 1 :
                    // InternalOWLModel.g:2971:5: otherlv_6= 'rdf:datatype' otherlv_7= '=' ( (lv_datatype_8_0= ruleOwlRef ) )
                    {
                    otherlv_6=(Token)match(input,62,FOLLOW_13); 

                        	newLeafNode(otherlv_6, grammarAccess.getOwlLabelAccess().getRdfDatatypeKeyword_2_0());
                        
                    otherlv_7=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_7, grammarAccess.getOwlLabelAccess().getEqualsSignKeyword_2_1());
                        
                    // InternalOWLModel.g:2979:1: ( (lv_datatype_8_0= ruleOwlRef ) )
                    // InternalOWLModel.g:2980:1: (lv_datatype_8_0= ruleOwlRef )
                    {
                    // InternalOWLModel.g:2980:1: (lv_datatype_8_0= ruleOwlRef )
                    // InternalOWLModel.g:2981:3: lv_datatype_8_0= ruleOwlRef
                    {
                     
                    	        newCompositeNode(grammarAccess.getOwlLabelAccess().getDatatypeOwlRefParserRuleCall_2_2_0()); 
                    	    
                    pushFollow(FOLLOW_7);
                    lv_datatype_8_0=ruleOwlRef();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getOwlLabelRule());
                    	        }
                           		set(
                           			current, 
                           			"datatype",
                            		lv_datatype_8_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlRef");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            otherlv_9=(Token)match(input,21,FOLLOW_59); 

                	newLeafNode(otherlv_9, grammarAccess.getOwlLabelAccess().getGreaterThanSignKeyword_3());
                
            // InternalOWLModel.g:3001:1: ( (lv_value_10_0= ruleTAG_ANY_VALUE ) )
            // InternalOWLModel.g:3002:1: (lv_value_10_0= ruleTAG_ANY_VALUE )
            {
            // InternalOWLModel.g:3002:1: (lv_value_10_0= ruleTAG_ANY_VALUE )
            // InternalOWLModel.g:3003:3: lv_value_10_0= ruleTAG_ANY_VALUE
            {
             
            	        newCompositeNode(grammarAccess.getOwlLabelAccess().getValueTAG_ANY_VALUEParserRuleCall_4_0()); 
            	    
            pushFollow(FOLLOW_60);
            lv_value_10_0=ruleTAG_ANY_VALUE();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getOwlLabelRule());
            	        }
                   		set(
                   			current, 
                   			"value",
                    		lv_value_10_0, 
                    		"org.smool.sdk.owlparser.OWLModel.TAG_ANY_VALUE");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_11=(Token)match(input,63,FOLLOW_7); 

                	newLeafNode(otherlv_11, grammarAccess.getOwlLabelAccess().getRdfsLabelKeyword_5());
                
            otherlv_12=(Token)match(input,21,FOLLOW_2); 

                	newLeafNode(otherlv_12, grammarAccess.getOwlLabelAccess().getGreaterThanSignKeyword_6());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOwlLabel"


    // $ANTLR start "entryRuleOwlVersion"
    // InternalOWLModel.g:3035:1: entryRuleOwlVersion returns [EObject current=null] : iv_ruleOwlVersion= ruleOwlVersion EOF ;
    public final EObject entryRuleOwlVersion() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOwlVersion = null;


        try {
            // InternalOWLModel.g:3036:2: (iv_ruleOwlVersion= ruleOwlVersion EOF )
            // InternalOWLModel.g:3037:2: iv_ruleOwlVersion= ruleOwlVersion EOF
            {
             newCompositeNode(grammarAccess.getOwlVersionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOwlVersion=ruleOwlVersion();

            state._fsp--;

             current =iv_ruleOwlVersion; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOwlVersion"


    // $ANTLR start "ruleOwlVersion"
    // InternalOWLModel.g:3044:1: ruleOwlVersion returns [EObject current=null] : (otherlv_0= '<owl:versionInfo' (otherlv_1= 'xml:lang' otherlv_2= '=' this_STR_DELIMITER_3= RULE_STR_DELIMITER ( (lv_lang_4_0= rulePARAM_ANY_VALUE ) ) this_STR_DELIMITER_5= RULE_STR_DELIMITER )? (otherlv_6= 'rdf:datatype' otherlv_7= '=' ( (lv_datatype_8_0= ruleOwlRef ) ) )? otherlv_9= '>' ( (lv_value_10_0= ruleTAG_ANY_VALUE ) ) otherlv_11= '</owl:versionInfo' otherlv_12= '>' ) ;
    public final EObject ruleOwlVersion() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token this_STR_DELIMITER_3=null;
        Token this_STR_DELIMITER_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        AntlrDatatypeRuleToken lv_lang_4_0 = null;

        EObject lv_datatype_8_0 = null;

        AntlrDatatypeRuleToken lv_value_10_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:3047:28: ( (otherlv_0= '<owl:versionInfo' (otherlv_1= 'xml:lang' otherlv_2= '=' this_STR_DELIMITER_3= RULE_STR_DELIMITER ( (lv_lang_4_0= rulePARAM_ANY_VALUE ) ) this_STR_DELIMITER_5= RULE_STR_DELIMITER )? (otherlv_6= 'rdf:datatype' otherlv_7= '=' ( (lv_datatype_8_0= ruleOwlRef ) ) )? otherlv_9= '>' ( (lv_value_10_0= ruleTAG_ANY_VALUE ) ) otherlv_11= '</owl:versionInfo' otherlv_12= '>' ) )
            // InternalOWLModel.g:3048:1: (otherlv_0= '<owl:versionInfo' (otherlv_1= 'xml:lang' otherlv_2= '=' this_STR_DELIMITER_3= RULE_STR_DELIMITER ( (lv_lang_4_0= rulePARAM_ANY_VALUE ) ) this_STR_DELIMITER_5= RULE_STR_DELIMITER )? (otherlv_6= 'rdf:datatype' otherlv_7= '=' ( (lv_datatype_8_0= ruleOwlRef ) ) )? otherlv_9= '>' ( (lv_value_10_0= ruleTAG_ANY_VALUE ) ) otherlv_11= '</owl:versionInfo' otherlv_12= '>' )
            {
            // InternalOWLModel.g:3048:1: (otherlv_0= '<owl:versionInfo' (otherlv_1= 'xml:lang' otherlv_2= '=' this_STR_DELIMITER_3= RULE_STR_DELIMITER ( (lv_lang_4_0= rulePARAM_ANY_VALUE ) ) this_STR_DELIMITER_5= RULE_STR_DELIMITER )? (otherlv_6= 'rdf:datatype' otherlv_7= '=' ( (lv_datatype_8_0= ruleOwlRef ) ) )? otherlv_9= '>' ( (lv_value_10_0= ruleTAG_ANY_VALUE ) ) otherlv_11= '</owl:versionInfo' otherlv_12= '>' )
            // InternalOWLModel.g:3048:3: otherlv_0= '<owl:versionInfo' (otherlv_1= 'xml:lang' otherlv_2= '=' this_STR_DELIMITER_3= RULE_STR_DELIMITER ( (lv_lang_4_0= rulePARAM_ANY_VALUE ) ) this_STR_DELIMITER_5= RULE_STR_DELIMITER )? (otherlv_6= 'rdf:datatype' otherlv_7= '=' ( (lv_datatype_8_0= ruleOwlRef ) ) )? otherlv_9= '>' ( (lv_value_10_0= ruleTAG_ANY_VALUE ) ) otherlv_11= '</owl:versionInfo' otherlv_12= '>'
            {
            otherlv_0=(Token)match(input,64,FOLLOW_56); 

                	newLeafNode(otherlv_0, grammarAccess.getOwlVersionAccess().getOwlVersionInfoKeyword_0());
                
            // InternalOWLModel.g:3052:1: (otherlv_1= 'xml:lang' otherlv_2= '=' this_STR_DELIMITER_3= RULE_STR_DELIMITER ( (lv_lang_4_0= rulePARAM_ANY_VALUE ) ) this_STR_DELIMITER_5= RULE_STR_DELIMITER )?
            int alt67=2;
            int LA67_0 = input.LA(1);

            if ( (LA67_0==61) ) {
                alt67=1;
            }
            switch (alt67) {
                case 1 :
                    // InternalOWLModel.g:3052:3: otherlv_1= 'xml:lang' otherlv_2= '=' this_STR_DELIMITER_3= RULE_STR_DELIMITER ( (lv_lang_4_0= rulePARAM_ANY_VALUE ) ) this_STR_DELIMITER_5= RULE_STR_DELIMITER
                    {
                    otherlv_1=(Token)match(input,61,FOLLOW_13); 

                        	newLeafNode(otherlv_1, grammarAccess.getOwlVersionAccess().getXmlLangKeyword_1_0());
                        
                    otherlv_2=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_2, grammarAccess.getOwlVersionAccess().getEqualsSignKeyword_1_1());
                        
                    this_STR_DELIMITER_3=(Token)match(input,RULE_STR_DELIMITER,FOLLOW_57); 
                     
                        newLeafNode(this_STR_DELIMITER_3, grammarAccess.getOwlVersionAccess().getSTR_DELIMITERTerminalRuleCall_1_2()); 
                        
                    // InternalOWLModel.g:3064:1: ( (lv_lang_4_0= rulePARAM_ANY_VALUE ) )
                    // InternalOWLModel.g:3065:1: (lv_lang_4_0= rulePARAM_ANY_VALUE )
                    {
                    // InternalOWLModel.g:3065:1: (lv_lang_4_0= rulePARAM_ANY_VALUE )
                    // InternalOWLModel.g:3066:3: lv_lang_4_0= rulePARAM_ANY_VALUE
                    {
                     
                    	        newCompositeNode(grammarAccess.getOwlVersionAccess().getLangPARAM_ANY_VALUEParserRuleCall_1_3_0()); 
                    	    
                    pushFollow(FOLLOW_9);
                    lv_lang_4_0=rulePARAM_ANY_VALUE();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getOwlVersionRule());
                    	        }
                           		set(
                           			current, 
                           			"lang",
                            		lv_lang_4_0, 
                            		"org.smool.sdk.owlparser.OWLModel.PARAM_ANY_VALUE");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    this_STR_DELIMITER_5=(Token)match(input,RULE_STR_DELIMITER,FOLLOW_58); 
                     
                        newLeafNode(this_STR_DELIMITER_5, grammarAccess.getOwlVersionAccess().getSTR_DELIMITERTerminalRuleCall_1_4()); 
                        

                    }
                    break;

            }

            // InternalOWLModel.g:3086:3: (otherlv_6= 'rdf:datatype' otherlv_7= '=' ( (lv_datatype_8_0= ruleOwlRef ) ) )?
            int alt68=2;
            int LA68_0 = input.LA(1);

            if ( (LA68_0==62) ) {
                alt68=1;
            }
            switch (alt68) {
                case 1 :
                    // InternalOWLModel.g:3086:5: otherlv_6= 'rdf:datatype' otherlv_7= '=' ( (lv_datatype_8_0= ruleOwlRef ) )
                    {
                    otherlv_6=(Token)match(input,62,FOLLOW_13); 

                        	newLeafNode(otherlv_6, grammarAccess.getOwlVersionAccess().getRdfDatatypeKeyword_2_0());
                        
                    otherlv_7=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_7, grammarAccess.getOwlVersionAccess().getEqualsSignKeyword_2_1());
                        
                    // InternalOWLModel.g:3094:1: ( (lv_datatype_8_0= ruleOwlRef ) )
                    // InternalOWLModel.g:3095:1: (lv_datatype_8_0= ruleOwlRef )
                    {
                    // InternalOWLModel.g:3095:1: (lv_datatype_8_0= ruleOwlRef )
                    // InternalOWLModel.g:3096:3: lv_datatype_8_0= ruleOwlRef
                    {
                     
                    	        newCompositeNode(grammarAccess.getOwlVersionAccess().getDatatypeOwlRefParserRuleCall_2_2_0()); 
                    	    
                    pushFollow(FOLLOW_7);
                    lv_datatype_8_0=ruleOwlRef();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getOwlVersionRule());
                    	        }
                           		set(
                           			current, 
                           			"datatype",
                            		lv_datatype_8_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlRef");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            otherlv_9=(Token)match(input,21,FOLLOW_59); 

                	newLeafNode(otherlv_9, grammarAccess.getOwlVersionAccess().getGreaterThanSignKeyword_3());
                
            // InternalOWLModel.g:3116:1: ( (lv_value_10_0= ruleTAG_ANY_VALUE ) )
            // InternalOWLModel.g:3117:1: (lv_value_10_0= ruleTAG_ANY_VALUE )
            {
            // InternalOWLModel.g:3117:1: (lv_value_10_0= ruleTAG_ANY_VALUE )
            // InternalOWLModel.g:3118:3: lv_value_10_0= ruleTAG_ANY_VALUE
            {
             
            	        newCompositeNode(grammarAccess.getOwlVersionAccess().getValueTAG_ANY_VALUEParserRuleCall_4_0()); 
            	    
            pushFollow(FOLLOW_61);
            lv_value_10_0=ruleTAG_ANY_VALUE();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getOwlVersionRule());
            	        }
                   		set(
                   			current, 
                   			"value",
                    		lv_value_10_0, 
                    		"org.smool.sdk.owlparser.OWLModel.TAG_ANY_VALUE");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_11=(Token)match(input,65,FOLLOW_7); 

                	newLeafNode(otherlv_11, grammarAccess.getOwlVersionAccess().getOwlVersionInfoKeyword_5());
                
            otherlv_12=(Token)match(input,21,FOLLOW_2); 

                	newLeafNode(otherlv_12, grammarAccess.getOwlVersionAccess().getGreaterThanSignKeyword_6());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOwlVersion"


    // $ANTLR start "entryRuleSubTypeOf"
    // InternalOWLModel.g:3150:1: entryRuleSubTypeOf returns [EObject current=null] : iv_ruleSubTypeOf= ruleSubTypeOf EOF ;
    public final EObject entryRuleSubTypeOf() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSubTypeOf = null;


        try {
            // InternalOWLModel.g:3151:2: (iv_ruleSubTypeOf= ruleSubTypeOf EOF )
            // InternalOWLModel.g:3152:2: iv_ruleSubTypeOf= ruleSubTypeOf EOF
            {
             newCompositeNode(grammarAccess.getSubTypeOfRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSubTypeOf=ruleSubTypeOf();

            state._fsp--;

             current =iv_ruleSubTypeOf; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSubTypeOf"


    // $ANTLR start "ruleSubTypeOf"
    // InternalOWLModel.g:3159:1: ruleSubTypeOf returns [EObject current=null] : ( (otherlv_0= '<rdfs:subClassOf' ( ( ( (lv_resourceBased_1_0= 'rdf:resource' ) ) otherlv_2= '=' ( (lv_classRef_3_0= ruleOwlRef ) ) otherlv_4= '/>' ) | (otherlv_5= '>' ( (lv_modifiers_6_0= ruleClassModifier ) )+ otherlv_7= '</rdfs:subClassOf' otherlv_8= '>' ) ) ) | (otherlv_9= '<owl:subClassOf' ( ( ( (lv_resourceBased_10_0= 'rdf:resource' ) ) otherlv_11= '=' ( (lv_classRef_12_0= ruleOwlRef ) ) otherlv_13= '/>' ) | (otherlv_14= '>' ( (lv_modifiers_15_0= ruleClassModifier ) )+ otherlv_16= '</owl:subClassOf' otherlv_17= '>' ) ) ) ) ;
    public final EObject ruleSubTypeOf() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_resourceBased_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token lv_resourceBased_10_0=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        EObject lv_classRef_3_0 = null;

        EObject lv_modifiers_6_0 = null;

        EObject lv_classRef_12_0 = null;

        EObject lv_modifiers_15_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:3162:28: ( ( (otherlv_0= '<rdfs:subClassOf' ( ( ( (lv_resourceBased_1_0= 'rdf:resource' ) ) otherlv_2= '=' ( (lv_classRef_3_0= ruleOwlRef ) ) otherlv_4= '/>' ) | (otherlv_5= '>' ( (lv_modifiers_6_0= ruleClassModifier ) )+ otherlv_7= '</rdfs:subClassOf' otherlv_8= '>' ) ) ) | (otherlv_9= '<owl:subClassOf' ( ( ( (lv_resourceBased_10_0= 'rdf:resource' ) ) otherlv_11= '=' ( (lv_classRef_12_0= ruleOwlRef ) ) otherlv_13= '/>' ) | (otherlv_14= '>' ( (lv_modifiers_15_0= ruleClassModifier ) )+ otherlv_16= '</owl:subClassOf' otherlv_17= '>' ) ) ) ) )
            // InternalOWLModel.g:3163:1: ( (otherlv_0= '<rdfs:subClassOf' ( ( ( (lv_resourceBased_1_0= 'rdf:resource' ) ) otherlv_2= '=' ( (lv_classRef_3_0= ruleOwlRef ) ) otherlv_4= '/>' ) | (otherlv_5= '>' ( (lv_modifiers_6_0= ruleClassModifier ) )+ otherlv_7= '</rdfs:subClassOf' otherlv_8= '>' ) ) ) | (otherlv_9= '<owl:subClassOf' ( ( ( (lv_resourceBased_10_0= 'rdf:resource' ) ) otherlv_11= '=' ( (lv_classRef_12_0= ruleOwlRef ) ) otherlv_13= '/>' ) | (otherlv_14= '>' ( (lv_modifiers_15_0= ruleClassModifier ) )+ otherlv_16= '</owl:subClassOf' otherlv_17= '>' ) ) ) )
            {
            // InternalOWLModel.g:3163:1: ( (otherlv_0= '<rdfs:subClassOf' ( ( ( (lv_resourceBased_1_0= 'rdf:resource' ) ) otherlv_2= '=' ( (lv_classRef_3_0= ruleOwlRef ) ) otherlv_4= '/>' ) | (otherlv_5= '>' ( (lv_modifiers_6_0= ruleClassModifier ) )+ otherlv_7= '</rdfs:subClassOf' otherlv_8= '>' ) ) ) | (otherlv_9= '<owl:subClassOf' ( ( ( (lv_resourceBased_10_0= 'rdf:resource' ) ) otherlv_11= '=' ( (lv_classRef_12_0= ruleOwlRef ) ) otherlv_13= '/>' ) | (otherlv_14= '>' ( (lv_modifiers_15_0= ruleClassModifier ) )+ otherlv_16= '</owl:subClassOf' otherlv_17= '>' ) ) ) )
            int alt73=2;
            int LA73_0 = input.LA(1);

            if ( (LA73_0==66) ) {
                alt73=1;
            }
            else if ( (LA73_0==68) ) {
                alt73=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 73, 0, input);

                throw nvae;
            }
            switch (alt73) {
                case 1 :
                    // InternalOWLModel.g:3163:2: (otherlv_0= '<rdfs:subClassOf' ( ( ( (lv_resourceBased_1_0= 'rdf:resource' ) ) otherlv_2= '=' ( (lv_classRef_3_0= ruleOwlRef ) ) otherlv_4= '/>' ) | (otherlv_5= '>' ( (lv_modifiers_6_0= ruleClassModifier ) )+ otherlv_7= '</rdfs:subClassOf' otherlv_8= '>' ) ) )
                    {
                    // InternalOWLModel.g:3163:2: (otherlv_0= '<rdfs:subClassOf' ( ( ( (lv_resourceBased_1_0= 'rdf:resource' ) ) otherlv_2= '=' ( (lv_classRef_3_0= ruleOwlRef ) ) otherlv_4= '/>' ) | (otherlv_5= '>' ( (lv_modifiers_6_0= ruleClassModifier ) )+ otherlv_7= '</rdfs:subClassOf' otherlv_8= '>' ) ) )
                    // InternalOWLModel.g:3163:4: otherlv_0= '<rdfs:subClassOf' ( ( ( (lv_resourceBased_1_0= 'rdf:resource' ) ) otherlv_2= '=' ( (lv_classRef_3_0= ruleOwlRef ) ) otherlv_4= '/>' ) | (otherlv_5= '>' ( (lv_modifiers_6_0= ruleClassModifier ) )+ otherlv_7= '</rdfs:subClassOf' otherlv_8= '>' ) )
                    {
                    otherlv_0=(Token)match(input,66,FOLLOW_30); 

                        	newLeafNode(otherlv_0, grammarAccess.getSubTypeOfAccess().getRdfsSubClassOfKeyword_0_0());
                        
                    // InternalOWLModel.g:3167:1: ( ( ( (lv_resourceBased_1_0= 'rdf:resource' ) ) otherlv_2= '=' ( (lv_classRef_3_0= ruleOwlRef ) ) otherlv_4= '/>' ) | (otherlv_5= '>' ( (lv_modifiers_6_0= ruleClassModifier ) )+ otherlv_7= '</rdfs:subClassOf' otherlv_8= '>' ) )
                    int alt70=2;
                    int LA70_0 = input.LA(1);

                    if ( (LA70_0==32) ) {
                        alt70=1;
                    }
                    else if ( (LA70_0==21) ) {
                        alt70=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 70, 0, input);

                        throw nvae;
                    }
                    switch (alt70) {
                        case 1 :
                            // InternalOWLModel.g:3167:2: ( ( (lv_resourceBased_1_0= 'rdf:resource' ) ) otherlv_2= '=' ( (lv_classRef_3_0= ruleOwlRef ) ) otherlv_4= '/>' )
                            {
                            // InternalOWLModel.g:3167:2: ( ( (lv_resourceBased_1_0= 'rdf:resource' ) ) otherlv_2= '=' ( (lv_classRef_3_0= ruleOwlRef ) ) otherlv_4= '/>' )
                            // InternalOWLModel.g:3167:3: ( (lv_resourceBased_1_0= 'rdf:resource' ) ) otherlv_2= '=' ( (lv_classRef_3_0= ruleOwlRef ) ) otherlv_4= '/>'
                            {
                            // InternalOWLModel.g:3167:3: ( (lv_resourceBased_1_0= 'rdf:resource' ) )
                            // InternalOWLModel.g:3168:1: (lv_resourceBased_1_0= 'rdf:resource' )
                            {
                            // InternalOWLModel.g:3168:1: (lv_resourceBased_1_0= 'rdf:resource' )
                            // InternalOWLModel.g:3169:3: lv_resourceBased_1_0= 'rdf:resource'
                            {
                            lv_resourceBased_1_0=(Token)match(input,32,FOLLOW_13); 

                                    newLeafNode(lv_resourceBased_1_0, grammarAccess.getSubTypeOfAccess().getResourceBasedRdfResourceKeyword_0_1_0_0_0());
                                

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getSubTypeOfRule());
                            	        }
                                   		setWithLastConsumed(current, "resourceBased", true, "rdf:resource");
                            	    

                            }


                            }

                            otherlv_2=(Token)match(input,25,FOLLOW_9); 

                                	newLeafNode(otherlv_2, grammarAccess.getSubTypeOfAccess().getEqualsSignKeyword_0_1_0_1());
                                
                            // InternalOWLModel.g:3186:1: ( (lv_classRef_3_0= ruleOwlRef ) )
                            // InternalOWLModel.g:3187:1: (lv_classRef_3_0= ruleOwlRef )
                            {
                            // InternalOWLModel.g:3187:1: (lv_classRef_3_0= ruleOwlRef )
                            // InternalOWLModel.g:3188:3: lv_classRef_3_0= ruleOwlRef
                            {
                             
                            	        newCompositeNode(grammarAccess.getSubTypeOfAccess().getClassRefOwlRefParserRuleCall_0_1_0_2_0()); 
                            	    
                            pushFollow(FOLLOW_29);
                            lv_classRef_3_0=ruleOwlRef();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getSubTypeOfRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"classRef",
                                    		lv_classRef_3_0, 
                                    		"org.smool.sdk.owlparser.OWLModel.OwlRef");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }

                            otherlv_4=(Token)match(input,29,FOLLOW_2); 

                                	newLeafNode(otherlv_4, grammarAccess.getSubTypeOfAccess().getSolidusGreaterThanSignKeyword_0_1_0_3());
                                

                            }


                            }
                            break;
                        case 2 :
                            // InternalOWLModel.g:3209:6: (otherlv_5= '>' ( (lv_modifiers_6_0= ruleClassModifier ) )+ otherlv_7= '</rdfs:subClassOf' otherlv_8= '>' )
                            {
                            // InternalOWLModel.g:3209:6: (otherlv_5= '>' ( (lv_modifiers_6_0= ruleClassModifier ) )+ otherlv_7= '</rdfs:subClassOf' otherlv_8= '>' )
                            // InternalOWLModel.g:3209:8: otherlv_5= '>' ( (lv_modifiers_6_0= ruleClassModifier ) )+ otherlv_7= '</rdfs:subClassOf' otherlv_8= '>'
                            {
                            otherlv_5=(Token)match(input,21,FOLLOW_39); 

                                	newLeafNode(otherlv_5, grammarAccess.getSubTypeOfAccess().getGreaterThanSignKeyword_0_1_1_0());
                                
                            // InternalOWLModel.g:3213:1: ( (lv_modifiers_6_0= ruleClassModifier ) )+
                            int cnt69=0;
                            loop69:
                            do {
                                int alt69=2;
                                int LA69_0 = input.LA(1);

                                if ( (LA69_0==39||LA69_0==70||LA69_0==122) ) {
                                    alt69=1;
                                }


                                switch (alt69) {
                            	case 1 :
                            	    // InternalOWLModel.g:3214:1: (lv_modifiers_6_0= ruleClassModifier )
                            	    {
                            	    // InternalOWLModel.g:3214:1: (lv_modifiers_6_0= ruleClassModifier )
                            	    // InternalOWLModel.g:3215:3: lv_modifiers_6_0= ruleClassModifier
                            	    {
                            	     
                            	    	        newCompositeNode(grammarAccess.getSubTypeOfAccess().getModifiersClassModifierParserRuleCall_0_1_1_1_0()); 
                            	    	    
                            	    pushFollow(FOLLOW_62);
                            	    lv_modifiers_6_0=ruleClassModifier();

                            	    state._fsp--;


                            	    	        if (current==null) {
                            	    	            current = createModelElementForParent(grammarAccess.getSubTypeOfRule());
                            	    	        }
                            	           		add(
                            	           			current, 
                            	           			"modifiers",
                            	            		lv_modifiers_6_0, 
                            	            		"org.smool.sdk.owlparser.OWLModel.ClassModifier");
                            	    	        afterParserOrEnumRuleCall();
                            	    	    

                            	    }


                            	    }
                            	    break;

                            	default :
                            	    if ( cnt69 >= 1 ) break loop69;
                                        EarlyExitException eee =
                                            new EarlyExitException(69, input);
                                        throw eee;
                                }
                                cnt69++;
                            } while (true);

                            otherlv_7=(Token)match(input,67,FOLLOW_7); 

                                	newLeafNode(otherlv_7, grammarAccess.getSubTypeOfAccess().getRdfsSubClassOfKeyword_0_1_1_2());
                                
                            otherlv_8=(Token)match(input,21,FOLLOW_2); 

                                	newLeafNode(otherlv_8, grammarAccess.getSubTypeOfAccess().getGreaterThanSignKeyword_0_1_1_3());
                                

                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:3240:6: (otherlv_9= '<owl:subClassOf' ( ( ( (lv_resourceBased_10_0= 'rdf:resource' ) ) otherlv_11= '=' ( (lv_classRef_12_0= ruleOwlRef ) ) otherlv_13= '/>' ) | (otherlv_14= '>' ( (lv_modifiers_15_0= ruleClassModifier ) )+ otherlv_16= '</owl:subClassOf' otherlv_17= '>' ) ) )
                    {
                    // InternalOWLModel.g:3240:6: (otherlv_9= '<owl:subClassOf' ( ( ( (lv_resourceBased_10_0= 'rdf:resource' ) ) otherlv_11= '=' ( (lv_classRef_12_0= ruleOwlRef ) ) otherlv_13= '/>' ) | (otherlv_14= '>' ( (lv_modifiers_15_0= ruleClassModifier ) )+ otherlv_16= '</owl:subClassOf' otherlv_17= '>' ) ) )
                    // InternalOWLModel.g:3240:8: otherlv_9= '<owl:subClassOf' ( ( ( (lv_resourceBased_10_0= 'rdf:resource' ) ) otherlv_11= '=' ( (lv_classRef_12_0= ruleOwlRef ) ) otherlv_13= '/>' ) | (otherlv_14= '>' ( (lv_modifiers_15_0= ruleClassModifier ) )+ otherlv_16= '</owl:subClassOf' otherlv_17= '>' ) )
                    {
                    otherlv_9=(Token)match(input,68,FOLLOW_30); 

                        	newLeafNode(otherlv_9, grammarAccess.getSubTypeOfAccess().getOwlSubClassOfKeyword_1_0());
                        
                    // InternalOWLModel.g:3244:1: ( ( ( (lv_resourceBased_10_0= 'rdf:resource' ) ) otherlv_11= '=' ( (lv_classRef_12_0= ruleOwlRef ) ) otherlv_13= '/>' ) | (otherlv_14= '>' ( (lv_modifiers_15_0= ruleClassModifier ) )+ otherlv_16= '</owl:subClassOf' otherlv_17= '>' ) )
                    int alt72=2;
                    int LA72_0 = input.LA(1);

                    if ( (LA72_0==32) ) {
                        alt72=1;
                    }
                    else if ( (LA72_0==21) ) {
                        alt72=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 72, 0, input);

                        throw nvae;
                    }
                    switch (alt72) {
                        case 1 :
                            // InternalOWLModel.g:3244:2: ( ( (lv_resourceBased_10_0= 'rdf:resource' ) ) otherlv_11= '=' ( (lv_classRef_12_0= ruleOwlRef ) ) otherlv_13= '/>' )
                            {
                            // InternalOWLModel.g:3244:2: ( ( (lv_resourceBased_10_0= 'rdf:resource' ) ) otherlv_11= '=' ( (lv_classRef_12_0= ruleOwlRef ) ) otherlv_13= '/>' )
                            // InternalOWLModel.g:3244:3: ( (lv_resourceBased_10_0= 'rdf:resource' ) ) otherlv_11= '=' ( (lv_classRef_12_0= ruleOwlRef ) ) otherlv_13= '/>'
                            {
                            // InternalOWLModel.g:3244:3: ( (lv_resourceBased_10_0= 'rdf:resource' ) )
                            // InternalOWLModel.g:3245:1: (lv_resourceBased_10_0= 'rdf:resource' )
                            {
                            // InternalOWLModel.g:3245:1: (lv_resourceBased_10_0= 'rdf:resource' )
                            // InternalOWLModel.g:3246:3: lv_resourceBased_10_0= 'rdf:resource'
                            {
                            lv_resourceBased_10_0=(Token)match(input,32,FOLLOW_13); 

                                    newLeafNode(lv_resourceBased_10_0, grammarAccess.getSubTypeOfAccess().getResourceBasedRdfResourceKeyword_1_1_0_0_0());
                                

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getSubTypeOfRule());
                            	        }
                                   		setWithLastConsumed(current, "resourceBased", true, "rdf:resource");
                            	    

                            }


                            }

                            otherlv_11=(Token)match(input,25,FOLLOW_9); 

                                	newLeafNode(otherlv_11, grammarAccess.getSubTypeOfAccess().getEqualsSignKeyword_1_1_0_1());
                                
                            // InternalOWLModel.g:3263:1: ( (lv_classRef_12_0= ruleOwlRef ) )
                            // InternalOWLModel.g:3264:1: (lv_classRef_12_0= ruleOwlRef )
                            {
                            // InternalOWLModel.g:3264:1: (lv_classRef_12_0= ruleOwlRef )
                            // InternalOWLModel.g:3265:3: lv_classRef_12_0= ruleOwlRef
                            {
                             
                            	        newCompositeNode(grammarAccess.getSubTypeOfAccess().getClassRefOwlRefParserRuleCall_1_1_0_2_0()); 
                            	    
                            pushFollow(FOLLOW_29);
                            lv_classRef_12_0=ruleOwlRef();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getSubTypeOfRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"classRef",
                                    		lv_classRef_12_0, 
                                    		"org.smool.sdk.owlparser.OWLModel.OwlRef");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }

                            otherlv_13=(Token)match(input,29,FOLLOW_2); 

                                	newLeafNode(otherlv_13, grammarAccess.getSubTypeOfAccess().getSolidusGreaterThanSignKeyword_1_1_0_3());
                                

                            }


                            }
                            break;
                        case 2 :
                            // InternalOWLModel.g:3286:6: (otherlv_14= '>' ( (lv_modifiers_15_0= ruleClassModifier ) )+ otherlv_16= '</owl:subClassOf' otherlv_17= '>' )
                            {
                            // InternalOWLModel.g:3286:6: (otherlv_14= '>' ( (lv_modifiers_15_0= ruleClassModifier ) )+ otherlv_16= '</owl:subClassOf' otherlv_17= '>' )
                            // InternalOWLModel.g:3286:8: otherlv_14= '>' ( (lv_modifiers_15_0= ruleClassModifier ) )+ otherlv_16= '</owl:subClassOf' otherlv_17= '>'
                            {
                            otherlv_14=(Token)match(input,21,FOLLOW_39); 

                                	newLeafNode(otherlv_14, grammarAccess.getSubTypeOfAccess().getGreaterThanSignKeyword_1_1_1_0());
                                
                            // InternalOWLModel.g:3290:1: ( (lv_modifiers_15_0= ruleClassModifier ) )+
                            int cnt71=0;
                            loop71:
                            do {
                                int alt71=2;
                                int LA71_0 = input.LA(1);

                                if ( (LA71_0==39||LA71_0==70||LA71_0==122) ) {
                                    alt71=1;
                                }


                                switch (alt71) {
                            	case 1 :
                            	    // InternalOWLModel.g:3291:1: (lv_modifiers_15_0= ruleClassModifier )
                            	    {
                            	    // InternalOWLModel.g:3291:1: (lv_modifiers_15_0= ruleClassModifier )
                            	    // InternalOWLModel.g:3292:3: lv_modifiers_15_0= ruleClassModifier
                            	    {
                            	     
                            	    	        newCompositeNode(grammarAccess.getSubTypeOfAccess().getModifiersClassModifierParserRuleCall_1_1_1_1_0()); 
                            	    	    
                            	    pushFollow(FOLLOW_63);
                            	    lv_modifiers_15_0=ruleClassModifier();

                            	    state._fsp--;


                            	    	        if (current==null) {
                            	    	            current = createModelElementForParent(grammarAccess.getSubTypeOfRule());
                            	    	        }
                            	           		add(
                            	           			current, 
                            	           			"modifiers",
                            	            		lv_modifiers_15_0, 
                            	            		"org.smool.sdk.owlparser.OWLModel.ClassModifier");
                            	    	        afterParserOrEnumRuleCall();
                            	    	    

                            	    }


                            	    }
                            	    break;

                            	default :
                            	    if ( cnt71 >= 1 ) break loop71;
                                        EarlyExitException eee =
                                            new EarlyExitException(71, input);
                                        throw eee;
                                }
                                cnt71++;
                            } while (true);

                            otherlv_16=(Token)match(input,69,FOLLOW_7); 

                                	newLeafNode(otherlv_16, grammarAccess.getSubTypeOfAccess().getOwlSubClassOfKeyword_1_1_1_2());
                                
                            otherlv_17=(Token)match(input,21,FOLLOW_2); 

                                	newLeafNode(otherlv_17, grammarAccess.getSubTypeOfAccess().getGreaterThanSignKeyword_1_1_1_3());
                                

                            }


                            }
                            break;

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSubTypeOf"


    // $ANTLR start "entryRuleClassModifier"
    // InternalOWLModel.g:3324:1: entryRuleClassModifier returns [EObject current=null] : iv_ruleClassModifier= ruleClassModifier EOF ;
    public final EObject entryRuleClassModifier() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClassModifier = null;


        try {
            // InternalOWLModel.g:3325:2: (iv_ruleClassModifier= ruleClassModifier EOF )
            // InternalOWLModel.g:3326:2: iv_ruleClassModifier= ruleClassModifier EOF
            {
             newCompositeNode(grammarAccess.getClassModifierRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleClassModifier=ruleClassModifier();

            state._fsp--;

             current =iv_ruleClassModifier; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClassModifier"


    // $ANTLR start "ruleClassModifier"
    // InternalOWLModel.g:3333:1: ruleClassModifier returns [EObject current=null] : (this_Description_0= ruleDescription | this_OwlRestriction_1= ruleOwlRestriction | this_OwlClass_2= ruleOwlClass | this_MinimumClass_3= ruleMinimumClass ) ;
    public final EObject ruleClassModifier() throws RecognitionException {
        EObject current = null;

        EObject this_Description_0 = null;

        EObject this_OwlRestriction_1 = null;

        EObject this_OwlClass_2 = null;

        EObject this_MinimumClass_3 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:3336:28: ( (this_Description_0= ruleDescription | this_OwlRestriction_1= ruleOwlRestriction | this_OwlClass_2= ruleOwlClass | this_MinimumClass_3= ruleMinimumClass ) )
            // InternalOWLModel.g:3337:1: (this_Description_0= ruleDescription | this_OwlRestriction_1= ruleOwlRestriction | this_OwlClass_2= ruleOwlClass | this_MinimumClass_3= ruleMinimumClass )
            {
            // InternalOWLModel.g:3337:1: (this_Description_0= ruleDescription | this_OwlRestriction_1= ruleOwlRestriction | this_OwlClass_2= ruleOwlClass | this_MinimumClass_3= ruleMinimumClass )
            int alt74=4;
            switch ( input.LA(1) ) {
            case 122:
                {
                alt74=1;
                }
                break;
            case 70:
                {
                alt74=2;
                }
                break;
            case 39:
                {
                int LA74_3 = input.LA(2);

                if ( (LA74_3==28||LA74_3==40) ) {
                    alt74=3;
                }
                else if ( (LA74_3==21) ) {
                    alt74=4;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 74, 3, input);

                    throw nvae;
                }
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 74, 0, input);

                throw nvae;
            }

            switch (alt74) {
                case 1 :
                    // InternalOWLModel.g:3338:5: this_Description_0= ruleDescription
                    {
                     
                            newCompositeNode(grammarAccess.getClassModifierAccess().getDescriptionParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_2);
                    this_Description_0=ruleDescription();

                    state._fsp--;

                     
                            current = this_Description_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:3348:5: this_OwlRestriction_1= ruleOwlRestriction
                    {
                     
                            newCompositeNode(grammarAccess.getClassModifierAccess().getOwlRestrictionParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_2);
                    this_OwlRestriction_1=ruleOwlRestriction();

                    state._fsp--;

                     
                            current = this_OwlRestriction_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // InternalOWLModel.g:3358:5: this_OwlClass_2= ruleOwlClass
                    {
                     
                            newCompositeNode(grammarAccess.getClassModifierAccess().getOwlClassParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_2);
                    this_OwlClass_2=ruleOwlClass();

                    state._fsp--;

                     
                            current = this_OwlClass_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // InternalOWLModel.g:3368:5: this_MinimumClass_3= ruleMinimumClass
                    {
                     
                            newCompositeNode(grammarAccess.getClassModifierAccess().getMinimumClassParserRuleCall_3()); 
                        
                    pushFollow(FOLLOW_2);
                    this_MinimumClass_3=ruleMinimumClass();

                    state._fsp--;

                     
                            current = this_MinimumClass_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClassModifier"


    // $ANTLR start "entryRuleOwlRestriction"
    // InternalOWLModel.g:3384:1: entryRuleOwlRestriction returns [EObject current=null] : iv_ruleOwlRestriction= ruleOwlRestriction EOF ;
    public final EObject entryRuleOwlRestriction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOwlRestriction = null;


        try {
            // InternalOWLModel.g:3385:2: (iv_ruleOwlRestriction= ruleOwlRestriction EOF )
            // InternalOWLModel.g:3386:2: iv_ruleOwlRestriction= ruleOwlRestriction EOF
            {
             newCompositeNode(grammarAccess.getOwlRestrictionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOwlRestriction=ruleOwlRestriction();

            state._fsp--;

             current =iv_ruleOwlRestriction; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOwlRestriction"


    // $ANTLR start "ruleOwlRestriction"
    // InternalOWLModel.g:3393:1: ruleOwlRestriction returns [EObject current=null] : (otherlv_0= '<owl:Restriction' otherlv_1= '>' ( (lv_restrictionBodies_2_0= ruleRestrictionBody ) )* otherlv_3= '<owl:onProperty' ( (otherlv_4= 'rdf:resource' otherlv_5= '=' ( (lv_prop_6_0= ruleOwlRef ) ) (otherlv_7= '/>' | (otherlv_8= '>' otherlv_9= '</owl:onProperty' otherlv_10= '>' ) ) ) | (otherlv_11= '>' ( (lv_property_12_0= ruleProperty ) ) otherlv_13= '</owl:onProperty' otherlv_14= '>' ) ) (otherlv_15= '<owl:onClass' ( (otherlv_16= 'rdf:resource' otherlv_17= '=' ( (lv_class_18_0= ruleOwlRef ) ) (otherlv_19= '/>' | (otherlv_20= '>' otherlv_21= '</owl:onClass' otherlv_22= '>' ) ) ) | (otherlv_23= '>' ( (lv_classes_24_0= ruleClassModifier ) ) otherlv_25= '</owl:onClass' otherlv_26= '>' ) ) )? ( (lv_restrictionBodies_27_0= ruleRestrictionBody ) )* otherlv_28= '</owl:Restriction' otherlv_29= '>' ) ;
    public final EObject ruleOwlRestriction() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_20=null;
        Token otherlv_21=null;
        Token otherlv_22=null;
        Token otherlv_23=null;
        Token otherlv_25=null;
        Token otherlv_26=null;
        Token otherlv_28=null;
        Token otherlv_29=null;
        EObject lv_restrictionBodies_2_0 = null;

        EObject lv_prop_6_0 = null;

        EObject lv_property_12_0 = null;

        EObject lv_class_18_0 = null;

        EObject lv_classes_24_0 = null;

        EObject lv_restrictionBodies_27_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:3396:28: ( (otherlv_0= '<owl:Restriction' otherlv_1= '>' ( (lv_restrictionBodies_2_0= ruleRestrictionBody ) )* otherlv_3= '<owl:onProperty' ( (otherlv_4= 'rdf:resource' otherlv_5= '=' ( (lv_prop_6_0= ruleOwlRef ) ) (otherlv_7= '/>' | (otherlv_8= '>' otherlv_9= '</owl:onProperty' otherlv_10= '>' ) ) ) | (otherlv_11= '>' ( (lv_property_12_0= ruleProperty ) ) otherlv_13= '</owl:onProperty' otherlv_14= '>' ) ) (otherlv_15= '<owl:onClass' ( (otherlv_16= 'rdf:resource' otherlv_17= '=' ( (lv_class_18_0= ruleOwlRef ) ) (otherlv_19= '/>' | (otherlv_20= '>' otherlv_21= '</owl:onClass' otherlv_22= '>' ) ) ) | (otherlv_23= '>' ( (lv_classes_24_0= ruleClassModifier ) ) otherlv_25= '</owl:onClass' otherlv_26= '>' ) ) )? ( (lv_restrictionBodies_27_0= ruleRestrictionBody ) )* otherlv_28= '</owl:Restriction' otherlv_29= '>' ) )
            // InternalOWLModel.g:3397:1: (otherlv_0= '<owl:Restriction' otherlv_1= '>' ( (lv_restrictionBodies_2_0= ruleRestrictionBody ) )* otherlv_3= '<owl:onProperty' ( (otherlv_4= 'rdf:resource' otherlv_5= '=' ( (lv_prop_6_0= ruleOwlRef ) ) (otherlv_7= '/>' | (otherlv_8= '>' otherlv_9= '</owl:onProperty' otherlv_10= '>' ) ) ) | (otherlv_11= '>' ( (lv_property_12_0= ruleProperty ) ) otherlv_13= '</owl:onProperty' otherlv_14= '>' ) ) (otherlv_15= '<owl:onClass' ( (otherlv_16= 'rdf:resource' otherlv_17= '=' ( (lv_class_18_0= ruleOwlRef ) ) (otherlv_19= '/>' | (otherlv_20= '>' otherlv_21= '</owl:onClass' otherlv_22= '>' ) ) ) | (otherlv_23= '>' ( (lv_classes_24_0= ruleClassModifier ) ) otherlv_25= '</owl:onClass' otherlv_26= '>' ) ) )? ( (lv_restrictionBodies_27_0= ruleRestrictionBody ) )* otherlv_28= '</owl:Restriction' otherlv_29= '>' )
            {
            // InternalOWLModel.g:3397:1: (otherlv_0= '<owl:Restriction' otherlv_1= '>' ( (lv_restrictionBodies_2_0= ruleRestrictionBody ) )* otherlv_3= '<owl:onProperty' ( (otherlv_4= 'rdf:resource' otherlv_5= '=' ( (lv_prop_6_0= ruleOwlRef ) ) (otherlv_7= '/>' | (otherlv_8= '>' otherlv_9= '</owl:onProperty' otherlv_10= '>' ) ) ) | (otherlv_11= '>' ( (lv_property_12_0= ruleProperty ) ) otherlv_13= '</owl:onProperty' otherlv_14= '>' ) ) (otherlv_15= '<owl:onClass' ( (otherlv_16= 'rdf:resource' otherlv_17= '=' ( (lv_class_18_0= ruleOwlRef ) ) (otherlv_19= '/>' | (otherlv_20= '>' otherlv_21= '</owl:onClass' otherlv_22= '>' ) ) ) | (otherlv_23= '>' ( (lv_classes_24_0= ruleClassModifier ) ) otherlv_25= '</owl:onClass' otherlv_26= '>' ) ) )? ( (lv_restrictionBodies_27_0= ruleRestrictionBody ) )* otherlv_28= '</owl:Restriction' otherlv_29= '>' )
            // InternalOWLModel.g:3397:3: otherlv_0= '<owl:Restriction' otherlv_1= '>' ( (lv_restrictionBodies_2_0= ruleRestrictionBody ) )* otherlv_3= '<owl:onProperty' ( (otherlv_4= 'rdf:resource' otherlv_5= '=' ( (lv_prop_6_0= ruleOwlRef ) ) (otherlv_7= '/>' | (otherlv_8= '>' otherlv_9= '</owl:onProperty' otherlv_10= '>' ) ) ) | (otherlv_11= '>' ( (lv_property_12_0= ruleProperty ) ) otherlv_13= '</owl:onProperty' otherlv_14= '>' ) ) (otherlv_15= '<owl:onClass' ( (otherlv_16= 'rdf:resource' otherlv_17= '=' ( (lv_class_18_0= ruleOwlRef ) ) (otherlv_19= '/>' | (otherlv_20= '>' otherlv_21= '</owl:onClass' otherlv_22= '>' ) ) ) | (otherlv_23= '>' ( (lv_classes_24_0= ruleClassModifier ) ) otherlv_25= '</owl:onClass' otherlv_26= '>' ) ) )? ( (lv_restrictionBodies_27_0= ruleRestrictionBody ) )* otherlv_28= '</owl:Restriction' otherlv_29= '>'
            {
            otherlv_0=(Token)match(input,70,FOLLOW_7); 

                	newLeafNode(otherlv_0, grammarAccess.getOwlRestrictionAccess().getOwlRestrictionKeyword_0());
                
            otherlv_1=(Token)match(input,21,FOLLOW_64); 

                	newLeafNode(otherlv_1, grammarAccess.getOwlRestrictionAccess().getGreaterThanSignKeyword_1());
                
            // InternalOWLModel.g:3405:1: ( (lv_restrictionBodies_2_0= ruleRestrictionBody ) )*
            loop75:
            do {
                int alt75=2;
                int LA75_0 = input.LA(1);

                if ( (LA75_0==76||LA75_0==78||LA75_0==80||LA75_0==82||LA75_0==84||LA75_0==86||LA75_0==88||LA75_0==90||LA75_0==92) ) {
                    alt75=1;
                }


                switch (alt75) {
            	case 1 :
            	    // InternalOWLModel.g:3406:1: (lv_restrictionBodies_2_0= ruleRestrictionBody )
            	    {
            	    // InternalOWLModel.g:3406:1: (lv_restrictionBodies_2_0= ruleRestrictionBody )
            	    // InternalOWLModel.g:3407:3: lv_restrictionBodies_2_0= ruleRestrictionBody
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getOwlRestrictionAccess().getRestrictionBodiesRestrictionBodyParserRuleCall_2_0()); 
            	    	    
            	    pushFollow(FOLLOW_64);
            	    lv_restrictionBodies_2_0=ruleRestrictionBody();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getOwlRestrictionRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"restrictionBodies",
            	            		lv_restrictionBodies_2_0, 
            	            		"org.smool.sdk.owlparser.OWLModel.RestrictionBody");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop75;
                }
            } while (true);

            otherlv_3=(Token)match(input,71,FOLLOW_30); 

                	newLeafNode(otherlv_3, grammarAccess.getOwlRestrictionAccess().getOwlOnPropertyKeyword_3());
                
            // InternalOWLModel.g:3427:1: ( (otherlv_4= 'rdf:resource' otherlv_5= '=' ( (lv_prop_6_0= ruleOwlRef ) ) (otherlv_7= '/>' | (otherlv_8= '>' otherlv_9= '</owl:onProperty' otherlv_10= '>' ) ) ) | (otherlv_11= '>' ( (lv_property_12_0= ruleProperty ) ) otherlv_13= '</owl:onProperty' otherlv_14= '>' ) )
            int alt77=2;
            int LA77_0 = input.LA(1);

            if ( (LA77_0==32) ) {
                alt77=1;
            }
            else if ( (LA77_0==21) ) {
                alt77=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 77, 0, input);

                throw nvae;
            }
            switch (alt77) {
                case 1 :
                    // InternalOWLModel.g:3427:2: (otherlv_4= 'rdf:resource' otherlv_5= '=' ( (lv_prop_6_0= ruleOwlRef ) ) (otherlv_7= '/>' | (otherlv_8= '>' otherlv_9= '</owl:onProperty' otherlv_10= '>' ) ) )
                    {
                    // InternalOWLModel.g:3427:2: (otherlv_4= 'rdf:resource' otherlv_5= '=' ( (lv_prop_6_0= ruleOwlRef ) ) (otherlv_7= '/>' | (otherlv_8= '>' otherlv_9= '</owl:onProperty' otherlv_10= '>' ) ) )
                    // InternalOWLModel.g:3427:4: otherlv_4= 'rdf:resource' otherlv_5= '=' ( (lv_prop_6_0= ruleOwlRef ) ) (otherlv_7= '/>' | (otherlv_8= '>' otherlv_9= '</owl:onProperty' otherlv_10= '>' ) )
                    {
                    otherlv_4=(Token)match(input,32,FOLLOW_13); 

                        	newLeafNode(otherlv_4, grammarAccess.getOwlRestrictionAccess().getRdfResourceKeyword_4_0_0());
                        
                    otherlv_5=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_5, grammarAccess.getOwlRestrictionAccess().getEqualsSignKeyword_4_0_1());
                        
                    // InternalOWLModel.g:3435:1: ( (lv_prop_6_0= ruleOwlRef ) )
                    // InternalOWLModel.g:3436:1: (lv_prop_6_0= ruleOwlRef )
                    {
                    // InternalOWLModel.g:3436:1: (lv_prop_6_0= ruleOwlRef )
                    // InternalOWLModel.g:3437:3: lv_prop_6_0= ruleOwlRef
                    {
                     
                    	        newCompositeNode(grammarAccess.getOwlRestrictionAccess().getPropOwlRefParserRuleCall_4_0_2_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_prop_6_0=ruleOwlRef();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getOwlRestrictionRule());
                    	        }
                           		set(
                           			current, 
                           			"prop",
                            		lv_prop_6_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlRef");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // InternalOWLModel.g:3453:2: (otherlv_7= '/>' | (otherlv_8= '>' otherlv_9= '</owl:onProperty' otherlv_10= '>' ) )
                    int alt76=2;
                    int LA76_0 = input.LA(1);

                    if ( (LA76_0==29) ) {
                        alt76=1;
                    }
                    else if ( (LA76_0==21) ) {
                        alt76=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 76, 0, input);

                        throw nvae;
                    }
                    switch (alt76) {
                        case 1 :
                            // InternalOWLModel.g:3453:4: otherlv_7= '/>'
                            {
                            otherlv_7=(Token)match(input,29,FOLLOW_65); 

                                	newLeafNode(otherlv_7, grammarAccess.getOwlRestrictionAccess().getSolidusGreaterThanSignKeyword_4_0_3_0());
                                

                            }
                            break;
                        case 2 :
                            // InternalOWLModel.g:3458:6: (otherlv_8= '>' otherlv_9= '</owl:onProperty' otherlv_10= '>' )
                            {
                            // InternalOWLModel.g:3458:6: (otherlv_8= '>' otherlv_9= '</owl:onProperty' otherlv_10= '>' )
                            // InternalOWLModel.g:3458:8: otherlv_8= '>' otherlv_9= '</owl:onProperty' otherlv_10= '>'
                            {
                            otherlv_8=(Token)match(input,21,FOLLOW_66); 

                                	newLeafNode(otherlv_8, grammarAccess.getOwlRestrictionAccess().getGreaterThanSignKeyword_4_0_3_1_0());
                                
                            otherlv_9=(Token)match(input,72,FOLLOW_7); 

                                	newLeafNode(otherlv_9, grammarAccess.getOwlRestrictionAccess().getOwlOnPropertyKeyword_4_0_3_1_1());
                                
                            otherlv_10=(Token)match(input,21,FOLLOW_65); 

                                	newLeafNode(otherlv_10, grammarAccess.getOwlRestrictionAccess().getGreaterThanSignKeyword_4_0_3_1_2());
                                

                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:3471:6: (otherlv_11= '>' ( (lv_property_12_0= ruleProperty ) ) otherlv_13= '</owl:onProperty' otherlv_14= '>' )
                    {
                    // InternalOWLModel.g:3471:6: (otherlv_11= '>' ( (lv_property_12_0= ruleProperty ) ) otherlv_13= '</owl:onProperty' otherlv_14= '>' )
                    // InternalOWLModel.g:3471:8: otherlv_11= '>' ( (lv_property_12_0= ruleProperty ) ) otherlv_13= '</owl:onProperty' otherlv_14= '>'
                    {
                    otherlv_11=(Token)match(input,21,FOLLOW_67); 

                        	newLeafNode(otherlv_11, grammarAccess.getOwlRestrictionAccess().getGreaterThanSignKeyword_4_1_0());
                        
                    // InternalOWLModel.g:3475:1: ( (lv_property_12_0= ruleProperty ) )
                    // InternalOWLModel.g:3476:1: (lv_property_12_0= ruleProperty )
                    {
                    // InternalOWLModel.g:3476:1: (lv_property_12_0= ruleProperty )
                    // InternalOWLModel.g:3477:3: lv_property_12_0= ruleProperty
                    {
                     
                    	        newCompositeNode(grammarAccess.getOwlRestrictionAccess().getPropertyPropertyParserRuleCall_4_1_1_0()); 
                    	    
                    pushFollow(FOLLOW_66);
                    lv_property_12_0=ruleProperty();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getOwlRestrictionRule());
                    	        }
                           		set(
                           			current, 
                           			"property",
                            		lv_property_12_0, 
                            		"org.smool.sdk.owlparser.OWLModel.Property");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_13=(Token)match(input,72,FOLLOW_7); 

                        	newLeafNode(otherlv_13, grammarAccess.getOwlRestrictionAccess().getOwlOnPropertyKeyword_4_1_2());
                        
                    otherlv_14=(Token)match(input,21,FOLLOW_65); 

                        	newLeafNode(otherlv_14, grammarAccess.getOwlRestrictionAccess().getGreaterThanSignKeyword_4_1_3());
                        

                    }


                    }
                    break;

            }

            // InternalOWLModel.g:3501:3: (otherlv_15= '<owl:onClass' ( (otherlv_16= 'rdf:resource' otherlv_17= '=' ( (lv_class_18_0= ruleOwlRef ) ) (otherlv_19= '/>' | (otherlv_20= '>' otherlv_21= '</owl:onClass' otherlv_22= '>' ) ) ) | (otherlv_23= '>' ( (lv_classes_24_0= ruleClassModifier ) ) otherlv_25= '</owl:onClass' otherlv_26= '>' ) ) )?
            int alt80=2;
            int LA80_0 = input.LA(1);

            if ( (LA80_0==73) ) {
                alt80=1;
            }
            switch (alt80) {
                case 1 :
                    // InternalOWLModel.g:3501:5: otherlv_15= '<owl:onClass' ( (otherlv_16= 'rdf:resource' otherlv_17= '=' ( (lv_class_18_0= ruleOwlRef ) ) (otherlv_19= '/>' | (otherlv_20= '>' otherlv_21= '</owl:onClass' otherlv_22= '>' ) ) ) | (otherlv_23= '>' ( (lv_classes_24_0= ruleClassModifier ) ) otherlv_25= '</owl:onClass' otherlv_26= '>' ) )
                    {
                    otherlv_15=(Token)match(input,73,FOLLOW_30); 

                        	newLeafNode(otherlv_15, grammarAccess.getOwlRestrictionAccess().getOwlOnClassKeyword_5_0());
                        
                    // InternalOWLModel.g:3505:1: ( (otherlv_16= 'rdf:resource' otherlv_17= '=' ( (lv_class_18_0= ruleOwlRef ) ) (otherlv_19= '/>' | (otherlv_20= '>' otherlv_21= '</owl:onClass' otherlv_22= '>' ) ) ) | (otherlv_23= '>' ( (lv_classes_24_0= ruleClassModifier ) ) otherlv_25= '</owl:onClass' otherlv_26= '>' ) )
                    int alt79=2;
                    int LA79_0 = input.LA(1);

                    if ( (LA79_0==32) ) {
                        alt79=1;
                    }
                    else if ( (LA79_0==21) ) {
                        alt79=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 79, 0, input);

                        throw nvae;
                    }
                    switch (alt79) {
                        case 1 :
                            // InternalOWLModel.g:3505:2: (otherlv_16= 'rdf:resource' otherlv_17= '=' ( (lv_class_18_0= ruleOwlRef ) ) (otherlv_19= '/>' | (otherlv_20= '>' otherlv_21= '</owl:onClass' otherlv_22= '>' ) ) )
                            {
                            // InternalOWLModel.g:3505:2: (otherlv_16= 'rdf:resource' otherlv_17= '=' ( (lv_class_18_0= ruleOwlRef ) ) (otherlv_19= '/>' | (otherlv_20= '>' otherlv_21= '</owl:onClass' otherlv_22= '>' ) ) )
                            // InternalOWLModel.g:3505:4: otherlv_16= 'rdf:resource' otherlv_17= '=' ( (lv_class_18_0= ruleOwlRef ) ) (otherlv_19= '/>' | (otherlv_20= '>' otherlv_21= '</owl:onClass' otherlv_22= '>' ) )
                            {
                            otherlv_16=(Token)match(input,32,FOLLOW_13); 

                                	newLeafNode(otherlv_16, grammarAccess.getOwlRestrictionAccess().getRdfResourceKeyword_5_1_0_0());
                                
                            otherlv_17=(Token)match(input,25,FOLLOW_9); 

                                	newLeafNode(otherlv_17, grammarAccess.getOwlRestrictionAccess().getEqualsSignKeyword_5_1_0_1());
                                
                            // InternalOWLModel.g:3513:1: ( (lv_class_18_0= ruleOwlRef ) )
                            // InternalOWLModel.g:3514:1: (lv_class_18_0= ruleOwlRef )
                            {
                            // InternalOWLModel.g:3514:1: (lv_class_18_0= ruleOwlRef )
                            // InternalOWLModel.g:3515:3: lv_class_18_0= ruleOwlRef
                            {
                             
                            	        newCompositeNode(grammarAccess.getOwlRestrictionAccess().getClassOwlRefParserRuleCall_5_1_0_2_0()); 
                            	    
                            pushFollow(FOLLOW_24);
                            lv_class_18_0=ruleOwlRef();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getOwlRestrictionRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"class",
                                    		lv_class_18_0, 
                                    		"org.smool.sdk.owlparser.OWLModel.OwlRef");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }

                            // InternalOWLModel.g:3531:2: (otherlv_19= '/>' | (otherlv_20= '>' otherlv_21= '</owl:onClass' otherlv_22= '>' ) )
                            int alt78=2;
                            int LA78_0 = input.LA(1);

                            if ( (LA78_0==29) ) {
                                alt78=1;
                            }
                            else if ( (LA78_0==21) ) {
                                alt78=2;
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 78, 0, input);

                                throw nvae;
                            }
                            switch (alt78) {
                                case 1 :
                                    // InternalOWLModel.g:3531:4: otherlv_19= '/>'
                                    {
                                    otherlv_19=(Token)match(input,29,FOLLOW_68); 

                                        	newLeafNode(otherlv_19, grammarAccess.getOwlRestrictionAccess().getSolidusGreaterThanSignKeyword_5_1_0_3_0());
                                        

                                    }
                                    break;
                                case 2 :
                                    // InternalOWLModel.g:3536:6: (otherlv_20= '>' otherlv_21= '</owl:onClass' otherlv_22= '>' )
                                    {
                                    // InternalOWLModel.g:3536:6: (otherlv_20= '>' otherlv_21= '</owl:onClass' otherlv_22= '>' )
                                    // InternalOWLModel.g:3536:8: otherlv_20= '>' otherlv_21= '</owl:onClass' otherlv_22= '>'
                                    {
                                    otherlv_20=(Token)match(input,21,FOLLOW_69); 

                                        	newLeafNode(otherlv_20, grammarAccess.getOwlRestrictionAccess().getGreaterThanSignKeyword_5_1_0_3_1_0());
                                        
                                    otherlv_21=(Token)match(input,74,FOLLOW_7); 

                                        	newLeafNode(otherlv_21, grammarAccess.getOwlRestrictionAccess().getOwlOnClassKeyword_5_1_0_3_1_1());
                                        
                                    otherlv_22=(Token)match(input,21,FOLLOW_68); 

                                        	newLeafNode(otherlv_22, grammarAccess.getOwlRestrictionAccess().getGreaterThanSignKeyword_5_1_0_3_1_2());
                                        

                                    }


                                    }
                                    break;

                            }


                            }


                            }
                            break;
                        case 2 :
                            // InternalOWLModel.g:3549:6: (otherlv_23= '>' ( (lv_classes_24_0= ruleClassModifier ) ) otherlv_25= '</owl:onClass' otherlv_26= '>' )
                            {
                            // InternalOWLModel.g:3549:6: (otherlv_23= '>' ( (lv_classes_24_0= ruleClassModifier ) ) otherlv_25= '</owl:onClass' otherlv_26= '>' )
                            // InternalOWLModel.g:3549:8: otherlv_23= '>' ( (lv_classes_24_0= ruleClassModifier ) ) otherlv_25= '</owl:onClass' otherlv_26= '>'
                            {
                            otherlv_23=(Token)match(input,21,FOLLOW_39); 

                                	newLeafNode(otherlv_23, grammarAccess.getOwlRestrictionAccess().getGreaterThanSignKeyword_5_1_1_0());
                                
                            // InternalOWLModel.g:3553:1: ( (lv_classes_24_0= ruleClassModifier ) )
                            // InternalOWLModel.g:3554:1: (lv_classes_24_0= ruleClassModifier )
                            {
                            // InternalOWLModel.g:3554:1: (lv_classes_24_0= ruleClassModifier )
                            // InternalOWLModel.g:3555:3: lv_classes_24_0= ruleClassModifier
                            {
                             
                            	        newCompositeNode(grammarAccess.getOwlRestrictionAccess().getClassesClassModifierParserRuleCall_5_1_1_1_0()); 
                            	    
                            pushFollow(FOLLOW_69);
                            lv_classes_24_0=ruleClassModifier();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getOwlRestrictionRule());
                            	        }
                                   		add(
                                   			current, 
                                   			"classes",
                                    		lv_classes_24_0, 
                                    		"org.smool.sdk.owlparser.OWLModel.ClassModifier");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }

                            otherlv_25=(Token)match(input,74,FOLLOW_7); 

                                	newLeafNode(otherlv_25, grammarAccess.getOwlRestrictionAccess().getOwlOnClassKeyword_5_1_1_2());
                                
                            otherlv_26=(Token)match(input,21,FOLLOW_68); 

                                	newLeafNode(otherlv_26, grammarAccess.getOwlRestrictionAccess().getGreaterThanSignKeyword_5_1_1_3());
                                

                            }


                            }
                            break;

                    }


                    }
                    break;

            }

            // InternalOWLModel.g:3579:5: ( (lv_restrictionBodies_27_0= ruleRestrictionBody ) )*
            loop81:
            do {
                int alt81=2;
                int LA81_0 = input.LA(1);

                if ( (LA81_0==76||LA81_0==78||LA81_0==80||LA81_0==82||LA81_0==84||LA81_0==86||LA81_0==88||LA81_0==90||LA81_0==92) ) {
                    alt81=1;
                }


                switch (alt81) {
            	case 1 :
            	    // InternalOWLModel.g:3580:1: (lv_restrictionBodies_27_0= ruleRestrictionBody )
            	    {
            	    // InternalOWLModel.g:3580:1: (lv_restrictionBodies_27_0= ruleRestrictionBody )
            	    // InternalOWLModel.g:3581:3: lv_restrictionBodies_27_0= ruleRestrictionBody
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getOwlRestrictionAccess().getRestrictionBodiesRestrictionBodyParserRuleCall_6_0()); 
            	    	    
            	    pushFollow(FOLLOW_68);
            	    lv_restrictionBodies_27_0=ruleRestrictionBody();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getOwlRestrictionRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"restrictionBodies",
            	            		lv_restrictionBodies_27_0, 
            	            		"org.smool.sdk.owlparser.OWLModel.RestrictionBody");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop81;
                }
            } while (true);

            otherlv_28=(Token)match(input,75,FOLLOW_7); 

                	newLeafNode(otherlv_28, grammarAccess.getOwlRestrictionAccess().getOwlRestrictionKeyword_7());
                
            otherlv_29=(Token)match(input,21,FOLLOW_2); 

                	newLeafNode(otherlv_29, grammarAccess.getOwlRestrictionAccess().getGreaterThanSignKeyword_8());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOwlRestriction"


    // $ANTLR start "entryRuleRestrictionBody"
    // InternalOWLModel.g:3613:1: entryRuleRestrictionBody returns [EObject current=null] : iv_ruleRestrictionBody= ruleRestrictionBody EOF ;
    public final EObject entryRuleRestrictionBody() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRestrictionBody = null;


        try {
            // InternalOWLModel.g:3614:2: (iv_ruleRestrictionBody= ruleRestrictionBody EOF )
            // InternalOWLModel.g:3615:2: iv_ruleRestrictionBody= ruleRestrictionBody EOF
            {
             newCompositeNode(grammarAccess.getRestrictionBodyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRestrictionBody=ruleRestrictionBody();

            state._fsp--;

             current =iv_ruleRestrictionBody; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRestrictionBody"


    // $ANTLR start "ruleRestrictionBody"
    // InternalOWLModel.g:3622:1: ruleRestrictionBody returns [EObject current=null] : (this_MinCardinality_0= ruleMinCardinality | this_MaxCardinality_1= ruleMaxCardinality | this_Cardinality_2= ruleCardinality | this_QualifiedCardinality_3= ruleQualifiedCardinality | this_MinQualifiedCardinality_4= ruleMinQualifiedCardinality | this_MaxQualifiedCardinality_5= ruleMaxQualifiedCardinality | this_SomeValues_6= ruleSomeValues | this_AllValues_7= ruleAllValues | this_HasValue_8= ruleHasValue ) ;
    public final EObject ruleRestrictionBody() throws RecognitionException {
        EObject current = null;

        EObject this_MinCardinality_0 = null;

        EObject this_MaxCardinality_1 = null;

        EObject this_Cardinality_2 = null;

        EObject this_QualifiedCardinality_3 = null;

        EObject this_MinQualifiedCardinality_4 = null;

        EObject this_MaxQualifiedCardinality_5 = null;

        EObject this_SomeValues_6 = null;

        EObject this_AllValues_7 = null;

        EObject this_HasValue_8 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:3625:28: ( (this_MinCardinality_0= ruleMinCardinality | this_MaxCardinality_1= ruleMaxCardinality | this_Cardinality_2= ruleCardinality | this_QualifiedCardinality_3= ruleQualifiedCardinality | this_MinQualifiedCardinality_4= ruleMinQualifiedCardinality | this_MaxQualifiedCardinality_5= ruleMaxQualifiedCardinality | this_SomeValues_6= ruleSomeValues | this_AllValues_7= ruleAllValues | this_HasValue_8= ruleHasValue ) )
            // InternalOWLModel.g:3626:1: (this_MinCardinality_0= ruleMinCardinality | this_MaxCardinality_1= ruleMaxCardinality | this_Cardinality_2= ruleCardinality | this_QualifiedCardinality_3= ruleQualifiedCardinality | this_MinQualifiedCardinality_4= ruleMinQualifiedCardinality | this_MaxQualifiedCardinality_5= ruleMaxQualifiedCardinality | this_SomeValues_6= ruleSomeValues | this_AllValues_7= ruleAllValues | this_HasValue_8= ruleHasValue )
            {
            // InternalOWLModel.g:3626:1: (this_MinCardinality_0= ruleMinCardinality | this_MaxCardinality_1= ruleMaxCardinality | this_Cardinality_2= ruleCardinality | this_QualifiedCardinality_3= ruleQualifiedCardinality | this_MinQualifiedCardinality_4= ruleMinQualifiedCardinality | this_MaxQualifiedCardinality_5= ruleMaxQualifiedCardinality | this_SomeValues_6= ruleSomeValues | this_AllValues_7= ruleAllValues | this_HasValue_8= ruleHasValue )
            int alt82=9;
            switch ( input.LA(1) ) {
            case 76:
                {
                alt82=1;
                }
                break;
            case 78:
                {
                alt82=2;
                }
                break;
            case 80:
                {
                alt82=3;
                }
                break;
            case 82:
                {
                alt82=4;
                }
                break;
            case 84:
                {
                alt82=5;
                }
                break;
            case 86:
                {
                alt82=6;
                }
                break;
            case 88:
                {
                alt82=7;
                }
                break;
            case 90:
                {
                alt82=8;
                }
                break;
            case 92:
                {
                alt82=9;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 82, 0, input);

                throw nvae;
            }

            switch (alt82) {
                case 1 :
                    // InternalOWLModel.g:3627:5: this_MinCardinality_0= ruleMinCardinality
                    {
                     
                            newCompositeNode(grammarAccess.getRestrictionBodyAccess().getMinCardinalityParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_2);
                    this_MinCardinality_0=ruleMinCardinality();

                    state._fsp--;

                     
                            current = this_MinCardinality_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:3637:5: this_MaxCardinality_1= ruleMaxCardinality
                    {
                     
                            newCompositeNode(grammarAccess.getRestrictionBodyAccess().getMaxCardinalityParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_2);
                    this_MaxCardinality_1=ruleMaxCardinality();

                    state._fsp--;

                     
                            current = this_MaxCardinality_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // InternalOWLModel.g:3647:5: this_Cardinality_2= ruleCardinality
                    {
                     
                            newCompositeNode(grammarAccess.getRestrictionBodyAccess().getCardinalityParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_2);
                    this_Cardinality_2=ruleCardinality();

                    state._fsp--;

                     
                            current = this_Cardinality_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // InternalOWLModel.g:3657:5: this_QualifiedCardinality_3= ruleQualifiedCardinality
                    {
                     
                            newCompositeNode(grammarAccess.getRestrictionBodyAccess().getQualifiedCardinalityParserRuleCall_3()); 
                        
                    pushFollow(FOLLOW_2);
                    this_QualifiedCardinality_3=ruleQualifiedCardinality();

                    state._fsp--;

                     
                            current = this_QualifiedCardinality_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 5 :
                    // InternalOWLModel.g:3667:5: this_MinQualifiedCardinality_4= ruleMinQualifiedCardinality
                    {
                     
                            newCompositeNode(grammarAccess.getRestrictionBodyAccess().getMinQualifiedCardinalityParserRuleCall_4()); 
                        
                    pushFollow(FOLLOW_2);
                    this_MinQualifiedCardinality_4=ruleMinQualifiedCardinality();

                    state._fsp--;

                     
                            current = this_MinQualifiedCardinality_4; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 6 :
                    // InternalOWLModel.g:3677:5: this_MaxQualifiedCardinality_5= ruleMaxQualifiedCardinality
                    {
                     
                            newCompositeNode(grammarAccess.getRestrictionBodyAccess().getMaxQualifiedCardinalityParserRuleCall_5()); 
                        
                    pushFollow(FOLLOW_2);
                    this_MaxQualifiedCardinality_5=ruleMaxQualifiedCardinality();

                    state._fsp--;

                     
                            current = this_MaxQualifiedCardinality_5; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 7 :
                    // InternalOWLModel.g:3687:5: this_SomeValues_6= ruleSomeValues
                    {
                     
                            newCompositeNode(grammarAccess.getRestrictionBodyAccess().getSomeValuesParserRuleCall_6()); 
                        
                    pushFollow(FOLLOW_2);
                    this_SomeValues_6=ruleSomeValues();

                    state._fsp--;

                     
                            current = this_SomeValues_6; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 8 :
                    // InternalOWLModel.g:3697:5: this_AllValues_7= ruleAllValues
                    {
                     
                            newCompositeNode(grammarAccess.getRestrictionBodyAccess().getAllValuesParserRuleCall_7()); 
                        
                    pushFollow(FOLLOW_2);
                    this_AllValues_7=ruleAllValues();

                    state._fsp--;

                     
                            current = this_AllValues_7; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 9 :
                    // InternalOWLModel.g:3707:5: this_HasValue_8= ruleHasValue
                    {
                     
                            newCompositeNode(grammarAccess.getRestrictionBodyAccess().getHasValueParserRuleCall_8()); 
                        
                    pushFollow(FOLLOW_2);
                    this_HasValue_8=ruleHasValue();

                    state._fsp--;

                     
                            current = this_HasValue_8; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRestrictionBody"


    // $ANTLR start "entryRuleMinCardinality"
    // InternalOWLModel.g:3723:1: entryRuleMinCardinality returns [EObject current=null] : iv_ruleMinCardinality= ruleMinCardinality EOF ;
    public final EObject entryRuleMinCardinality() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMinCardinality = null;


        try {
            // InternalOWLModel.g:3724:2: (iv_ruleMinCardinality= ruleMinCardinality EOF )
            // InternalOWLModel.g:3725:2: iv_ruleMinCardinality= ruleMinCardinality EOF
            {
             newCompositeNode(grammarAccess.getMinCardinalityRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleMinCardinality=ruleMinCardinality();

            state._fsp--;

             current =iv_ruleMinCardinality; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMinCardinality"


    // $ANTLR start "ruleMinCardinality"
    // InternalOWLModel.g:3732:1: ruleMinCardinality returns [EObject current=null] : (otherlv_0= '<owl:minCardinality' otherlv_1= 'rdf:datatype' otherlv_2= '=' ( (lv_datatype_3_0= ruleOwlRef ) ) otherlv_4= '>' ( (lv_value_5_0= RULE_INT ) ) otherlv_6= '</owl:minCardinality' otherlv_7= '>' ) ;
    public final EObject ruleMinCardinality() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token lv_value_5_0=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        EObject lv_datatype_3_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:3735:28: ( (otherlv_0= '<owl:minCardinality' otherlv_1= 'rdf:datatype' otherlv_2= '=' ( (lv_datatype_3_0= ruleOwlRef ) ) otherlv_4= '>' ( (lv_value_5_0= RULE_INT ) ) otherlv_6= '</owl:minCardinality' otherlv_7= '>' ) )
            // InternalOWLModel.g:3736:1: (otherlv_0= '<owl:minCardinality' otherlv_1= 'rdf:datatype' otherlv_2= '=' ( (lv_datatype_3_0= ruleOwlRef ) ) otherlv_4= '>' ( (lv_value_5_0= RULE_INT ) ) otherlv_6= '</owl:minCardinality' otherlv_7= '>' )
            {
            // InternalOWLModel.g:3736:1: (otherlv_0= '<owl:minCardinality' otherlv_1= 'rdf:datatype' otherlv_2= '=' ( (lv_datatype_3_0= ruleOwlRef ) ) otherlv_4= '>' ( (lv_value_5_0= RULE_INT ) ) otherlv_6= '</owl:minCardinality' otherlv_7= '>' )
            // InternalOWLModel.g:3736:3: otherlv_0= '<owl:minCardinality' otherlv_1= 'rdf:datatype' otherlv_2= '=' ( (lv_datatype_3_0= ruleOwlRef ) ) otherlv_4= '>' ( (lv_value_5_0= RULE_INT ) ) otherlv_6= '</owl:minCardinality' otherlv_7= '>'
            {
            otherlv_0=(Token)match(input,76,FOLLOW_70); 

                	newLeafNode(otherlv_0, grammarAccess.getMinCardinalityAccess().getOwlMinCardinalityKeyword_0());
                
            otherlv_1=(Token)match(input,62,FOLLOW_13); 

                	newLeafNode(otherlv_1, grammarAccess.getMinCardinalityAccess().getRdfDatatypeKeyword_1());
                
            otherlv_2=(Token)match(input,25,FOLLOW_9); 

                	newLeafNode(otherlv_2, grammarAccess.getMinCardinalityAccess().getEqualsSignKeyword_2());
                
            // InternalOWLModel.g:3748:1: ( (lv_datatype_3_0= ruleOwlRef ) )
            // InternalOWLModel.g:3749:1: (lv_datatype_3_0= ruleOwlRef )
            {
            // InternalOWLModel.g:3749:1: (lv_datatype_3_0= ruleOwlRef )
            // InternalOWLModel.g:3750:3: lv_datatype_3_0= ruleOwlRef
            {
             
            	        newCompositeNode(grammarAccess.getMinCardinalityAccess().getDatatypeOwlRefParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_7);
            lv_datatype_3_0=ruleOwlRef();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getMinCardinalityRule());
            	        }
                   		set(
                   			current, 
                   			"datatype",
                    		lv_datatype_3_0, 
                    		"org.smool.sdk.owlparser.OWLModel.OwlRef");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_4=(Token)match(input,21,FOLLOW_71); 

                	newLeafNode(otherlv_4, grammarAccess.getMinCardinalityAccess().getGreaterThanSignKeyword_4());
                
            // InternalOWLModel.g:3770:1: ( (lv_value_5_0= RULE_INT ) )
            // InternalOWLModel.g:3771:1: (lv_value_5_0= RULE_INT )
            {
            // InternalOWLModel.g:3771:1: (lv_value_5_0= RULE_INT )
            // InternalOWLModel.g:3772:3: lv_value_5_0= RULE_INT
            {
            lv_value_5_0=(Token)match(input,RULE_INT,FOLLOW_72); 

            			newLeafNode(lv_value_5_0, grammarAccess.getMinCardinalityAccess().getValueINTTerminalRuleCall_5_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getMinCardinalityRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"value",
                    		lv_value_5_0, 
                    		"org.smool.sdk.owlparser.OWLModel.INT");
            	    

            }


            }

            otherlv_6=(Token)match(input,77,FOLLOW_7); 

                	newLeafNode(otherlv_6, grammarAccess.getMinCardinalityAccess().getOwlMinCardinalityKeyword_6());
                
            otherlv_7=(Token)match(input,21,FOLLOW_2); 

                	newLeafNode(otherlv_7, grammarAccess.getMinCardinalityAccess().getGreaterThanSignKeyword_7());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMinCardinality"


    // $ANTLR start "entryRuleMaxCardinality"
    // InternalOWLModel.g:3804:1: entryRuleMaxCardinality returns [EObject current=null] : iv_ruleMaxCardinality= ruleMaxCardinality EOF ;
    public final EObject entryRuleMaxCardinality() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMaxCardinality = null;


        try {
            // InternalOWLModel.g:3805:2: (iv_ruleMaxCardinality= ruleMaxCardinality EOF )
            // InternalOWLModel.g:3806:2: iv_ruleMaxCardinality= ruleMaxCardinality EOF
            {
             newCompositeNode(grammarAccess.getMaxCardinalityRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleMaxCardinality=ruleMaxCardinality();

            state._fsp--;

             current =iv_ruleMaxCardinality; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMaxCardinality"


    // $ANTLR start "ruleMaxCardinality"
    // InternalOWLModel.g:3813:1: ruleMaxCardinality returns [EObject current=null] : (otherlv_0= '<owl:maxCardinality' otherlv_1= 'rdf:datatype' otherlv_2= '=' ( (lv_datatype_3_0= ruleOwlRef ) ) otherlv_4= '>' ( (lv_value_5_0= RULE_INT ) ) otherlv_6= '</owl:maxCardinality' otherlv_7= '>' ) ;
    public final EObject ruleMaxCardinality() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token lv_value_5_0=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        EObject lv_datatype_3_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:3816:28: ( (otherlv_0= '<owl:maxCardinality' otherlv_1= 'rdf:datatype' otherlv_2= '=' ( (lv_datatype_3_0= ruleOwlRef ) ) otherlv_4= '>' ( (lv_value_5_0= RULE_INT ) ) otherlv_6= '</owl:maxCardinality' otherlv_7= '>' ) )
            // InternalOWLModel.g:3817:1: (otherlv_0= '<owl:maxCardinality' otherlv_1= 'rdf:datatype' otherlv_2= '=' ( (lv_datatype_3_0= ruleOwlRef ) ) otherlv_4= '>' ( (lv_value_5_0= RULE_INT ) ) otherlv_6= '</owl:maxCardinality' otherlv_7= '>' )
            {
            // InternalOWLModel.g:3817:1: (otherlv_0= '<owl:maxCardinality' otherlv_1= 'rdf:datatype' otherlv_2= '=' ( (lv_datatype_3_0= ruleOwlRef ) ) otherlv_4= '>' ( (lv_value_5_0= RULE_INT ) ) otherlv_6= '</owl:maxCardinality' otherlv_7= '>' )
            // InternalOWLModel.g:3817:3: otherlv_0= '<owl:maxCardinality' otherlv_1= 'rdf:datatype' otherlv_2= '=' ( (lv_datatype_3_0= ruleOwlRef ) ) otherlv_4= '>' ( (lv_value_5_0= RULE_INT ) ) otherlv_6= '</owl:maxCardinality' otherlv_7= '>'
            {
            otherlv_0=(Token)match(input,78,FOLLOW_70); 

                	newLeafNode(otherlv_0, grammarAccess.getMaxCardinalityAccess().getOwlMaxCardinalityKeyword_0());
                
            otherlv_1=(Token)match(input,62,FOLLOW_13); 

                	newLeafNode(otherlv_1, grammarAccess.getMaxCardinalityAccess().getRdfDatatypeKeyword_1());
                
            otherlv_2=(Token)match(input,25,FOLLOW_9); 

                	newLeafNode(otherlv_2, grammarAccess.getMaxCardinalityAccess().getEqualsSignKeyword_2());
                
            // InternalOWLModel.g:3829:1: ( (lv_datatype_3_0= ruleOwlRef ) )
            // InternalOWLModel.g:3830:1: (lv_datatype_3_0= ruleOwlRef )
            {
            // InternalOWLModel.g:3830:1: (lv_datatype_3_0= ruleOwlRef )
            // InternalOWLModel.g:3831:3: lv_datatype_3_0= ruleOwlRef
            {
             
            	        newCompositeNode(grammarAccess.getMaxCardinalityAccess().getDatatypeOwlRefParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_7);
            lv_datatype_3_0=ruleOwlRef();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getMaxCardinalityRule());
            	        }
                   		set(
                   			current, 
                   			"datatype",
                    		lv_datatype_3_0, 
                    		"org.smool.sdk.owlparser.OWLModel.OwlRef");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_4=(Token)match(input,21,FOLLOW_71); 

                	newLeafNode(otherlv_4, grammarAccess.getMaxCardinalityAccess().getGreaterThanSignKeyword_4());
                
            // InternalOWLModel.g:3851:1: ( (lv_value_5_0= RULE_INT ) )
            // InternalOWLModel.g:3852:1: (lv_value_5_0= RULE_INT )
            {
            // InternalOWLModel.g:3852:1: (lv_value_5_0= RULE_INT )
            // InternalOWLModel.g:3853:3: lv_value_5_0= RULE_INT
            {
            lv_value_5_0=(Token)match(input,RULE_INT,FOLLOW_73); 

            			newLeafNode(lv_value_5_0, grammarAccess.getMaxCardinalityAccess().getValueINTTerminalRuleCall_5_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getMaxCardinalityRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"value",
                    		lv_value_5_0, 
                    		"org.smool.sdk.owlparser.OWLModel.INT");
            	    

            }


            }

            otherlv_6=(Token)match(input,79,FOLLOW_7); 

                	newLeafNode(otherlv_6, grammarAccess.getMaxCardinalityAccess().getOwlMaxCardinalityKeyword_6());
                
            otherlv_7=(Token)match(input,21,FOLLOW_2); 

                	newLeafNode(otherlv_7, grammarAccess.getMaxCardinalityAccess().getGreaterThanSignKeyword_7());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMaxCardinality"


    // $ANTLR start "entryRuleCardinality"
    // InternalOWLModel.g:3885:1: entryRuleCardinality returns [EObject current=null] : iv_ruleCardinality= ruleCardinality EOF ;
    public final EObject entryRuleCardinality() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCardinality = null;


        try {
            // InternalOWLModel.g:3886:2: (iv_ruleCardinality= ruleCardinality EOF )
            // InternalOWLModel.g:3887:2: iv_ruleCardinality= ruleCardinality EOF
            {
             newCompositeNode(grammarAccess.getCardinalityRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCardinality=ruleCardinality();

            state._fsp--;

             current =iv_ruleCardinality; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCardinality"


    // $ANTLR start "ruleCardinality"
    // InternalOWLModel.g:3894:1: ruleCardinality returns [EObject current=null] : (otherlv_0= '<owl:cardinality' otherlv_1= 'rdf:datatype' otherlv_2= '=' ( (lv_datatype_3_0= ruleOwlRef ) ) otherlv_4= '>' ( (lv_value_5_0= RULE_INT ) ) otherlv_6= '</owl:cardinality' otherlv_7= '>' ) ;
    public final EObject ruleCardinality() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token lv_value_5_0=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        EObject lv_datatype_3_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:3897:28: ( (otherlv_0= '<owl:cardinality' otherlv_1= 'rdf:datatype' otherlv_2= '=' ( (lv_datatype_3_0= ruleOwlRef ) ) otherlv_4= '>' ( (lv_value_5_0= RULE_INT ) ) otherlv_6= '</owl:cardinality' otherlv_7= '>' ) )
            // InternalOWLModel.g:3898:1: (otherlv_0= '<owl:cardinality' otherlv_1= 'rdf:datatype' otherlv_2= '=' ( (lv_datatype_3_0= ruleOwlRef ) ) otherlv_4= '>' ( (lv_value_5_0= RULE_INT ) ) otherlv_6= '</owl:cardinality' otherlv_7= '>' )
            {
            // InternalOWLModel.g:3898:1: (otherlv_0= '<owl:cardinality' otherlv_1= 'rdf:datatype' otherlv_2= '=' ( (lv_datatype_3_0= ruleOwlRef ) ) otherlv_4= '>' ( (lv_value_5_0= RULE_INT ) ) otherlv_6= '</owl:cardinality' otherlv_7= '>' )
            // InternalOWLModel.g:3898:3: otherlv_0= '<owl:cardinality' otherlv_1= 'rdf:datatype' otherlv_2= '=' ( (lv_datatype_3_0= ruleOwlRef ) ) otherlv_4= '>' ( (lv_value_5_0= RULE_INT ) ) otherlv_6= '</owl:cardinality' otherlv_7= '>'
            {
            otherlv_0=(Token)match(input,80,FOLLOW_70); 

                	newLeafNode(otherlv_0, grammarAccess.getCardinalityAccess().getOwlCardinalityKeyword_0());
                
            otherlv_1=(Token)match(input,62,FOLLOW_13); 

                	newLeafNode(otherlv_1, grammarAccess.getCardinalityAccess().getRdfDatatypeKeyword_1());
                
            otherlv_2=(Token)match(input,25,FOLLOW_9); 

                	newLeafNode(otherlv_2, grammarAccess.getCardinalityAccess().getEqualsSignKeyword_2());
                
            // InternalOWLModel.g:3910:1: ( (lv_datatype_3_0= ruleOwlRef ) )
            // InternalOWLModel.g:3911:1: (lv_datatype_3_0= ruleOwlRef )
            {
            // InternalOWLModel.g:3911:1: (lv_datatype_3_0= ruleOwlRef )
            // InternalOWLModel.g:3912:3: lv_datatype_3_0= ruleOwlRef
            {
             
            	        newCompositeNode(grammarAccess.getCardinalityAccess().getDatatypeOwlRefParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_7);
            lv_datatype_3_0=ruleOwlRef();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getCardinalityRule());
            	        }
                   		set(
                   			current, 
                   			"datatype",
                    		lv_datatype_3_0, 
                    		"org.smool.sdk.owlparser.OWLModel.OwlRef");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_4=(Token)match(input,21,FOLLOW_71); 

                	newLeafNode(otherlv_4, grammarAccess.getCardinalityAccess().getGreaterThanSignKeyword_4());
                
            // InternalOWLModel.g:3932:1: ( (lv_value_5_0= RULE_INT ) )
            // InternalOWLModel.g:3933:1: (lv_value_5_0= RULE_INT )
            {
            // InternalOWLModel.g:3933:1: (lv_value_5_0= RULE_INT )
            // InternalOWLModel.g:3934:3: lv_value_5_0= RULE_INT
            {
            lv_value_5_0=(Token)match(input,RULE_INT,FOLLOW_74); 

            			newLeafNode(lv_value_5_0, grammarAccess.getCardinalityAccess().getValueINTTerminalRuleCall_5_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getCardinalityRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"value",
                    		lv_value_5_0, 
                    		"org.smool.sdk.owlparser.OWLModel.INT");
            	    

            }


            }

            otherlv_6=(Token)match(input,81,FOLLOW_7); 

                	newLeafNode(otherlv_6, grammarAccess.getCardinalityAccess().getOwlCardinalityKeyword_6());
                
            otherlv_7=(Token)match(input,21,FOLLOW_2); 

                	newLeafNode(otherlv_7, grammarAccess.getCardinalityAccess().getGreaterThanSignKeyword_7());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCardinality"


    // $ANTLR start "entryRuleQualifiedCardinality"
    // InternalOWLModel.g:3966:1: entryRuleQualifiedCardinality returns [EObject current=null] : iv_ruleQualifiedCardinality= ruleQualifiedCardinality EOF ;
    public final EObject entryRuleQualifiedCardinality() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQualifiedCardinality = null;


        try {
            // InternalOWLModel.g:3967:2: (iv_ruleQualifiedCardinality= ruleQualifiedCardinality EOF )
            // InternalOWLModel.g:3968:2: iv_ruleQualifiedCardinality= ruleQualifiedCardinality EOF
            {
             newCompositeNode(grammarAccess.getQualifiedCardinalityRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedCardinality=ruleQualifiedCardinality();

            state._fsp--;

             current =iv_ruleQualifiedCardinality; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedCardinality"


    // $ANTLR start "ruleQualifiedCardinality"
    // InternalOWLModel.g:3975:1: ruleQualifiedCardinality returns [EObject current=null] : (otherlv_0= '<owl:qualifiedCardinality' otherlv_1= 'rdf:datatype' otherlv_2= '=' ( (lv_datatype_3_0= ruleOwlRef ) ) otherlv_4= '>' ( (lv_value_5_0= RULE_INT ) ) otherlv_6= '</owl:qualifiedCardinality' otherlv_7= '>' ) ;
    public final EObject ruleQualifiedCardinality() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token lv_value_5_0=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        EObject lv_datatype_3_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:3978:28: ( (otherlv_0= '<owl:qualifiedCardinality' otherlv_1= 'rdf:datatype' otherlv_2= '=' ( (lv_datatype_3_0= ruleOwlRef ) ) otherlv_4= '>' ( (lv_value_5_0= RULE_INT ) ) otherlv_6= '</owl:qualifiedCardinality' otherlv_7= '>' ) )
            // InternalOWLModel.g:3979:1: (otherlv_0= '<owl:qualifiedCardinality' otherlv_1= 'rdf:datatype' otherlv_2= '=' ( (lv_datatype_3_0= ruleOwlRef ) ) otherlv_4= '>' ( (lv_value_5_0= RULE_INT ) ) otherlv_6= '</owl:qualifiedCardinality' otherlv_7= '>' )
            {
            // InternalOWLModel.g:3979:1: (otherlv_0= '<owl:qualifiedCardinality' otherlv_1= 'rdf:datatype' otherlv_2= '=' ( (lv_datatype_3_0= ruleOwlRef ) ) otherlv_4= '>' ( (lv_value_5_0= RULE_INT ) ) otherlv_6= '</owl:qualifiedCardinality' otherlv_7= '>' )
            // InternalOWLModel.g:3979:3: otherlv_0= '<owl:qualifiedCardinality' otherlv_1= 'rdf:datatype' otherlv_2= '=' ( (lv_datatype_3_0= ruleOwlRef ) ) otherlv_4= '>' ( (lv_value_5_0= RULE_INT ) ) otherlv_6= '</owl:qualifiedCardinality' otherlv_7= '>'
            {
            otherlv_0=(Token)match(input,82,FOLLOW_70); 

                	newLeafNode(otherlv_0, grammarAccess.getQualifiedCardinalityAccess().getOwlQualifiedCardinalityKeyword_0());
                
            otherlv_1=(Token)match(input,62,FOLLOW_13); 

                	newLeafNode(otherlv_1, grammarAccess.getQualifiedCardinalityAccess().getRdfDatatypeKeyword_1());
                
            otherlv_2=(Token)match(input,25,FOLLOW_9); 

                	newLeafNode(otherlv_2, grammarAccess.getQualifiedCardinalityAccess().getEqualsSignKeyword_2());
                
            // InternalOWLModel.g:3991:1: ( (lv_datatype_3_0= ruleOwlRef ) )
            // InternalOWLModel.g:3992:1: (lv_datatype_3_0= ruleOwlRef )
            {
            // InternalOWLModel.g:3992:1: (lv_datatype_3_0= ruleOwlRef )
            // InternalOWLModel.g:3993:3: lv_datatype_3_0= ruleOwlRef
            {
             
            	        newCompositeNode(grammarAccess.getQualifiedCardinalityAccess().getDatatypeOwlRefParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_7);
            lv_datatype_3_0=ruleOwlRef();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getQualifiedCardinalityRule());
            	        }
                   		set(
                   			current, 
                   			"datatype",
                    		lv_datatype_3_0, 
                    		"org.smool.sdk.owlparser.OWLModel.OwlRef");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_4=(Token)match(input,21,FOLLOW_71); 

                	newLeafNode(otherlv_4, grammarAccess.getQualifiedCardinalityAccess().getGreaterThanSignKeyword_4());
                
            // InternalOWLModel.g:4013:1: ( (lv_value_5_0= RULE_INT ) )
            // InternalOWLModel.g:4014:1: (lv_value_5_0= RULE_INT )
            {
            // InternalOWLModel.g:4014:1: (lv_value_5_0= RULE_INT )
            // InternalOWLModel.g:4015:3: lv_value_5_0= RULE_INT
            {
            lv_value_5_0=(Token)match(input,RULE_INT,FOLLOW_75); 

            			newLeafNode(lv_value_5_0, grammarAccess.getQualifiedCardinalityAccess().getValueINTTerminalRuleCall_5_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getQualifiedCardinalityRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"value",
                    		lv_value_5_0, 
                    		"org.smool.sdk.owlparser.OWLModel.INT");
            	    

            }


            }

            otherlv_6=(Token)match(input,83,FOLLOW_7); 

                	newLeafNode(otherlv_6, grammarAccess.getQualifiedCardinalityAccess().getOwlQualifiedCardinalityKeyword_6());
                
            otherlv_7=(Token)match(input,21,FOLLOW_2); 

                	newLeafNode(otherlv_7, grammarAccess.getQualifiedCardinalityAccess().getGreaterThanSignKeyword_7());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedCardinality"


    // $ANTLR start "entryRuleMinQualifiedCardinality"
    // InternalOWLModel.g:4047:1: entryRuleMinQualifiedCardinality returns [EObject current=null] : iv_ruleMinQualifiedCardinality= ruleMinQualifiedCardinality EOF ;
    public final EObject entryRuleMinQualifiedCardinality() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMinQualifiedCardinality = null;


        try {
            // InternalOWLModel.g:4048:2: (iv_ruleMinQualifiedCardinality= ruleMinQualifiedCardinality EOF )
            // InternalOWLModel.g:4049:2: iv_ruleMinQualifiedCardinality= ruleMinQualifiedCardinality EOF
            {
             newCompositeNode(grammarAccess.getMinQualifiedCardinalityRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleMinQualifiedCardinality=ruleMinQualifiedCardinality();

            state._fsp--;

             current =iv_ruleMinQualifiedCardinality; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMinQualifiedCardinality"


    // $ANTLR start "ruleMinQualifiedCardinality"
    // InternalOWLModel.g:4056:1: ruleMinQualifiedCardinality returns [EObject current=null] : (otherlv_0= '<owl:minQualifiedCardinality' otherlv_1= 'rdf:datatype' otherlv_2= '=' ( (lv_datatype_3_0= ruleOwlRef ) ) otherlv_4= '>' ( (lv_value_5_0= RULE_INT ) ) otherlv_6= '</owl:minQualifiedCardinality' otherlv_7= '>' ) ;
    public final EObject ruleMinQualifiedCardinality() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token lv_value_5_0=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        EObject lv_datatype_3_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:4059:28: ( (otherlv_0= '<owl:minQualifiedCardinality' otherlv_1= 'rdf:datatype' otherlv_2= '=' ( (lv_datatype_3_0= ruleOwlRef ) ) otherlv_4= '>' ( (lv_value_5_0= RULE_INT ) ) otherlv_6= '</owl:minQualifiedCardinality' otherlv_7= '>' ) )
            // InternalOWLModel.g:4060:1: (otherlv_0= '<owl:minQualifiedCardinality' otherlv_1= 'rdf:datatype' otherlv_2= '=' ( (lv_datatype_3_0= ruleOwlRef ) ) otherlv_4= '>' ( (lv_value_5_0= RULE_INT ) ) otherlv_6= '</owl:minQualifiedCardinality' otherlv_7= '>' )
            {
            // InternalOWLModel.g:4060:1: (otherlv_0= '<owl:minQualifiedCardinality' otherlv_1= 'rdf:datatype' otherlv_2= '=' ( (lv_datatype_3_0= ruleOwlRef ) ) otherlv_4= '>' ( (lv_value_5_0= RULE_INT ) ) otherlv_6= '</owl:minQualifiedCardinality' otherlv_7= '>' )
            // InternalOWLModel.g:4060:3: otherlv_0= '<owl:minQualifiedCardinality' otherlv_1= 'rdf:datatype' otherlv_2= '=' ( (lv_datatype_3_0= ruleOwlRef ) ) otherlv_4= '>' ( (lv_value_5_0= RULE_INT ) ) otherlv_6= '</owl:minQualifiedCardinality' otherlv_7= '>'
            {
            otherlv_0=(Token)match(input,84,FOLLOW_70); 

                	newLeafNode(otherlv_0, grammarAccess.getMinQualifiedCardinalityAccess().getOwlMinQualifiedCardinalityKeyword_0());
                
            otherlv_1=(Token)match(input,62,FOLLOW_13); 

                	newLeafNode(otherlv_1, grammarAccess.getMinQualifiedCardinalityAccess().getRdfDatatypeKeyword_1());
                
            otherlv_2=(Token)match(input,25,FOLLOW_9); 

                	newLeafNode(otherlv_2, grammarAccess.getMinQualifiedCardinalityAccess().getEqualsSignKeyword_2());
                
            // InternalOWLModel.g:4072:1: ( (lv_datatype_3_0= ruleOwlRef ) )
            // InternalOWLModel.g:4073:1: (lv_datatype_3_0= ruleOwlRef )
            {
            // InternalOWLModel.g:4073:1: (lv_datatype_3_0= ruleOwlRef )
            // InternalOWLModel.g:4074:3: lv_datatype_3_0= ruleOwlRef
            {
             
            	        newCompositeNode(grammarAccess.getMinQualifiedCardinalityAccess().getDatatypeOwlRefParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_7);
            lv_datatype_3_0=ruleOwlRef();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getMinQualifiedCardinalityRule());
            	        }
                   		set(
                   			current, 
                   			"datatype",
                    		lv_datatype_3_0, 
                    		"org.smool.sdk.owlparser.OWLModel.OwlRef");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_4=(Token)match(input,21,FOLLOW_71); 

                	newLeafNode(otherlv_4, grammarAccess.getMinQualifiedCardinalityAccess().getGreaterThanSignKeyword_4());
                
            // InternalOWLModel.g:4094:1: ( (lv_value_5_0= RULE_INT ) )
            // InternalOWLModel.g:4095:1: (lv_value_5_0= RULE_INT )
            {
            // InternalOWLModel.g:4095:1: (lv_value_5_0= RULE_INT )
            // InternalOWLModel.g:4096:3: lv_value_5_0= RULE_INT
            {
            lv_value_5_0=(Token)match(input,RULE_INT,FOLLOW_76); 

            			newLeafNode(lv_value_5_0, grammarAccess.getMinQualifiedCardinalityAccess().getValueINTTerminalRuleCall_5_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getMinQualifiedCardinalityRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"value",
                    		lv_value_5_0, 
                    		"org.smool.sdk.owlparser.OWLModel.INT");
            	    

            }


            }

            otherlv_6=(Token)match(input,85,FOLLOW_7); 

                	newLeafNode(otherlv_6, grammarAccess.getMinQualifiedCardinalityAccess().getOwlMinQualifiedCardinalityKeyword_6());
                
            otherlv_7=(Token)match(input,21,FOLLOW_2); 

                	newLeafNode(otherlv_7, grammarAccess.getMinQualifiedCardinalityAccess().getGreaterThanSignKeyword_7());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMinQualifiedCardinality"


    // $ANTLR start "entryRuleMaxQualifiedCardinality"
    // InternalOWLModel.g:4128:1: entryRuleMaxQualifiedCardinality returns [EObject current=null] : iv_ruleMaxQualifiedCardinality= ruleMaxQualifiedCardinality EOF ;
    public final EObject entryRuleMaxQualifiedCardinality() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMaxQualifiedCardinality = null;


        try {
            // InternalOWLModel.g:4129:2: (iv_ruleMaxQualifiedCardinality= ruleMaxQualifiedCardinality EOF )
            // InternalOWLModel.g:4130:2: iv_ruleMaxQualifiedCardinality= ruleMaxQualifiedCardinality EOF
            {
             newCompositeNode(grammarAccess.getMaxQualifiedCardinalityRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleMaxQualifiedCardinality=ruleMaxQualifiedCardinality();

            state._fsp--;

             current =iv_ruleMaxQualifiedCardinality; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMaxQualifiedCardinality"


    // $ANTLR start "ruleMaxQualifiedCardinality"
    // InternalOWLModel.g:4137:1: ruleMaxQualifiedCardinality returns [EObject current=null] : (otherlv_0= '<owl:maxQualifiedCardinality' otherlv_1= 'rdf:datatype' otherlv_2= '=' ( (lv_datatype_3_0= ruleOwlRef ) ) otherlv_4= '>' ( (lv_value_5_0= RULE_INT ) ) otherlv_6= '</owl:maxQualifiedCardinality' otherlv_7= '>' ) ;
    public final EObject ruleMaxQualifiedCardinality() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token lv_value_5_0=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        EObject lv_datatype_3_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:4140:28: ( (otherlv_0= '<owl:maxQualifiedCardinality' otherlv_1= 'rdf:datatype' otherlv_2= '=' ( (lv_datatype_3_0= ruleOwlRef ) ) otherlv_4= '>' ( (lv_value_5_0= RULE_INT ) ) otherlv_6= '</owl:maxQualifiedCardinality' otherlv_7= '>' ) )
            // InternalOWLModel.g:4141:1: (otherlv_0= '<owl:maxQualifiedCardinality' otherlv_1= 'rdf:datatype' otherlv_2= '=' ( (lv_datatype_3_0= ruleOwlRef ) ) otherlv_4= '>' ( (lv_value_5_0= RULE_INT ) ) otherlv_6= '</owl:maxQualifiedCardinality' otherlv_7= '>' )
            {
            // InternalOWLModel.g:4141:1: (otherlv_0= '<owl:maxQualifiedCardinality' otherlv_1= 'rdf:datatype' otherlv_2= '=' ( (lv_datatype_3_0= ruleOwlRef ) ) otherlv_4= '>' ( (lv_value_5_0= RULE_INT ) ) otherlv_6= '</owl:maxQualifiedCardinality' otherlv_7= '>' )
            // InternalOWLModel.g:4141:3: otherlv_0= '<owl:maxQualifiedCardinality' otherlv_1= 'rdf:datatype' otherlv_2= '=' ( (lv_datatype_3_0= ruleOwlRef ) ) otherlv_4= '>' ( (lv_value_5_0= RULE_INT ) ) otherlv_6= '</owl:maxQualifiedCardinality' otherlv_7= '>'
            {
            otherlv_0=(Token)match(input,86,FOLLOW_70); 

                	newLeafNode(otherlv_0, grammarAccess.getMaxQualifiedCardinalityAccess().getOwlMaxQualifiedCardinalityKeyword_0());
                
            otherlv_1=(Token)match(input,62,FOLLOW_13); 

                	newLeafNode(otherlv_1, grammarAccess.getMaxQualifiedCardinalityAccess().getRdfDatatypeKeyword_1());
                
            otherlv_2=(Token)match(input,25,FOLLOW_9); 

                	newLeafNode(otherlv_2, grammarAccess.getMaxQualifiedCardinalityAccess().getEqualsSignKeyword_2());
                
            // InternalOWLModel.g:4153:1: ( (lv_datatype_3_0= ruleOwlRef ) )
            // InternalOWLModel.g:4154:1: (lv_datatype_3_0= ruleOwlRef )
            {
            // InternalOWLModel.g:4154:1: (lv_datatype_3_0= ruleOwlRef )
            // InternalOWLModel.g:4155:3: lv_datatype_3_0= ruleOwlRef
            {
             
            	        newCompositeNode(grammarAccess.getMaxQualifiedCardinalityAccess().getDatatypeOwlRefParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_7);
            lv_datatype_3_0=ruleOwlRef();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getMaxQualifiedCardinalityRule());
            	        }
                   		set(
                   			current, 
                   			"datatype",
                    		lv_datatype_3_0, 
                    		"org.smool.sdk.owlparser.OWLModel.OwlRef");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_4=(Token)match(input,21,FOLLOW_71); 

                	newLeafNode(otherlv_4, grammarAccess.getMaxQualifiedCardinalityAccess().getGreaterThanSignKeyword_4());
                
            // InternalOWLModel.g:4175:1: ( (lv_value_5_0= RULE_INT ) )
            // InternalOWLModel.g:4176:1: (lv_value_5_0= RULE_INT )
            {
            // InternalOWLModel.g:4176:1: (lv_value_5_0= RULE_INT )
            // InternalOWLModel.g:4177:3: lv_value_5_0= RULE_INT
            {
            lv_value_5_0=(Token)match(input,RULE_INT,FOLLOW_77); 

            			newLeafNode(lv_value_5_0, grammarAccess.getMaxQualifiedCardinalityAccess().getValueINTTerminalRuleCall_5_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getMaxQualifiedCardinalityRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"value",
                    		lv_value_5_0, 
                    		"org.smool.sdk.owlparser.OWLModel.INT");
            	    

            }


            }

            otherlv_6=(Token)match(input,87,FOLLOW_7); 

                	newLeafNode(otherlv_6, grammarAccess.getMaxQualifiedCardinalityAccess().getOwlMaxQualifiedCardinalityKeyword_6());
                
            otherlv_7=(Token)match(input,21,FOLLOW_2); 

                	newLeafNode(otherlv_7, grammarAccess.getMaxQualifiedCardinalityAccess().getGreaterThanSignKeyword_7());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMaxQualifiedCardinality"


    // $ANTLR start "entryRuleSomeValues"
    // InternalOWLModel.g:4209:1: entryRuleSomeValues returns [EObject current=null] : iv_ruleSomeValues= ruleSomeValues EOF ;
    public final EObject entryRuleSomeValues() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSomeValues = null;


        try {
            // InternalOWLModel.g:4210:2: (iv_ruleSomeValues= ruleSomeValues EOF )
            // InternalOWLModel.g:4211:2: iv_ruleSomeValues= ruleSomeValues EOF
            {
             newCompositeNode(grammarAccess.getSomeValuesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSomeValues=ruleSomeValues();

            state._fsp--;

             current =iv_ruleSomeValues; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSomeValues"


    // $ANTLR start "ruleSomeValues"
    // InternalOWLModel.g:4218:1: ruleSomeValues returns [EObject current=null] : ( () otherlv_1= '<owl:someValuesFrom' (otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) )? (otherlv_5= '/>' | (otherlv_6= '>' ( (lv_modifiers_7_0= ruleClassModifier ) )* otherlv_8= '</owl:someValuesFrom' otherlv_9= '>' ) ) ) ;
    public final EObject ruleSomeValues() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        EObject lv_ref_4_0 = null;

        EObject lv_modifiers_7_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:4221:28: ( ( () otherlv_1= '<owl:someValuesFrom' (otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) )? (otherlv_5= '/>' | (otherlv_6= '>' ( (lv_modifiers_7_0= ruleClassModifier ) )* otherlv_8= '</owl:someValuesFrom' otherlv_9= '>' ) ) ) )
            // InternalOWLModel.g:4222:1: ( () otherlv_1= '<owl:someValuesFrom' (otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) )? (otherlv_5= '/>' | (otherlv_6= '>' ( (lv_modifiers_7_0= ruleClassModifier ) )* otherlv_8= '</owl:someValuesFrom' otherlv_9= '>' ) ) )
            {
            // InternalOWLModel.g:4222:1: ( () otherlv_1= '<owl:someValuesFrom' (otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) )? (otherlv_5= '/>' | (otherlv_6= '>' ( (lv_modifiers_7_0= ruleClassModifier ) )* otherlv_8= '</owl:someValuesFrom' otherlv_9= '>' ) ) )
            // InternalOWLModel.g:4222:2: () otherlv_1= '<owl:someValuesFrom' (otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) )? (otherlv_5= '/>' | (otherlv_6= '>' ( (lv_modifiers_7_0= ruleClassModifier ) )* otherlv_8= '</owl:someValuesFrom' otherlv_9= '>' ) )
            {
            // InternalOWLModel.g:4222:2: ()
            // InternalOWLModel.g:4223:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getSomeValuesAccess().getSomeValuesAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,88,FOLLOW_78); 

                	newLeafNode(otherlv_1, grammarAccess.getSomeValuesAccess().getOwlSomeValuesFromKeyword_1());
                
            // InternalOWLModel.g:4232:1: (otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) )?
            int alt83=2;
            int LA83_0 = input.LA(1);

            if ( (LA83_0==32) ) {
                alt83=1;
            }
            switch (alt83) {
                case 1 :
                    // InternalOWLModel.g:4232:3: otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) )
                    {
                    otherlv_2=(Token)match(input,32,FOLLOW_13); 

                        	newLeafNode(otherlv_2, grammarAccess.getSomeValuesAccess().getRdfResourceKeyword_2_0());
                        
                    otherlv_3=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_3, grammarAccess.getSomeValuesAccess().getEqualsSignKeyword_2_1());
                        
                    // InternalOWLModel.g:4240:1: ( (lv_ref_4_0= ruleOwlRef ) )
                    // InternalOWLModel.g:4241:1: (lv_ref_4_0= ruleOwlRef )
                    {
                    // InternalOWLModel.g:4241:1: (lv_ref_4_0= ruleOwlRef )
                    // InternalOWLModel.g:4242:3: lv_ref_4_0= ruleOwlRef
                    {
                     
                    	        newCompositeNode(grammarAccess.getSomeValuesAccess().getRefOwlRefParserRuleCall_2_2_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_ref_4_0=ruleOwlRef();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getSomeValuesRule());
                    	        }
                           		set(
                           			current, 
                           			"ref",
                            		lv_ref_4_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlRef");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            // InternalOWLModel.g:4258:4: (otherlv_5= '/>' | (otherlv_6= '>' ( (lv_modifiers_7_0= ruleClassModifier ) )* otherlv_8= '</owl:someValuesFrom' otherlv_9= '>' ) )
            int alt85=2;
            int LA85_0 = input.LA(1);

            if ( (LA85_0==29) ) {
                alt85=1;
            }
            else if ( (LA85_0==21) ) {
                alt85=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 85, 0, input);

                throw nvae;
            }
            switch (alt85) {
                case 1 :
                    // InternalOWLModel.g:4258:6: otherlv_5= '/>'
                    {
                    otherlv_5=(Token)match(input,29,FOLLOW_2); 

                        	newLeafNode(otherlv_5, grammarAccess.getSomeValuesAccess().getSolidusGreaterThanSignKeyword_3_0());
                        

                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:4263:6: (otherlv_6= '>' ( (lv_modifiers_7_0= ruleClassModifier ) )* otherlv_8= '</owl:someValuesFrom' otherlv_9= '>' )
                    {
                    // InternalOWLModel.g:4263:6: (otherlv_6= '>' ( (lv_modifiers_7_0= ruleClassModifier ) )* otherlv_8= '</owl:someValuesFrom' otherlv_9= '>' )
                    // InternalOWLModel.g:4263:8: otherlv_6= '>' ( (lv_modifiers_7_0= ruleClassModifier ) )* otherlv_8= '</owl:someValuesFrom' otherlv_9= '>'
                    {
                    otherlv_6=(Token)match(input,21,FOLLOW_79); 

                        	newLeafNode(otherlv_6, grammarAccess.getSomeValuesAccess().getGreaterThanSignKeyword_3_1_0());
                        
                    // InternalOWLModel.g:4267:1: ( (lv_modifiers_7_0= ruleClassModifier ) )*
                    loop84:
                    do {
                        int alt84=2;
                        int LA84_0 = input.LA(1);

                        if ( (LA84_0==39||LA84_0==70||LA84_0==122) ) {
                            alt84=1;
                        }


                        switch (alt84) {
                    	case 1 :
                    	    // InternalOWLModel.g:4268:1: (lv_modifiers_7_0= ruleClassModifier )
                    	    {
                    	    // InternalOWLModel.g:4268:1: (lv_modifiers_7_0= ruleClassModifier )
                    	    // InternalOWLModel.g:4269:3: lv_modifiers_7_0= ruleClassModifier
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getSomeValuesAccess().getModifiersClassModifierParserRuleCall_3_1_1_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_79);
                    	    lv_modifiers_7_0=ruleClassModifier();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getSomeValuesRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"modifiers",
                    	            		lv_modifiers_7_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.ClassModifier");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop84;
                        }
                    } while (true);

                    otherlv_8=(Token)match(input,89,FOLLOW_7); 

                        	newLeafNode(otherlv_8, grammarAccess.getSomeValuesAccess().getOwlSomeValuesFromKeyword_3_1_2());
                        
                    otherlv_9=(Token)match(input,21,FOLLOW_2); 

                        	newLeafNode(otherlv_9, grammarAccess.getSomeValuesAccess().getGreaterThanSignKeyword_3_1_3());
                        

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSomeValues"


    // $ANTLR start "entryRuleAllValues"
    // InternalOWLModel.g:4301:1: entryRuleAllValues returns [EObject current=null] : iv_ruleAllValues= ruleAllValues EOF ;
    public final EObject entryRuleAllValues() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAllValues = null;


        try {
            // InternalOWLModel.g:4302:2: (iv_ruleAllValues= ruleAllValues EOF )
            // InternalOWLModel.g:4303:2: iv_ruleAllValues= ruleAllValues EOF
            {
             newCompositeNode(grammarAccess.getAllValuesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAllValues=ruleAllValues();

            state._fsp--;

             current =iv_ruleAllValues; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAllValues"


    // $ANTLR start "ruleAllValues"
    // InternalOWLModel.g:4310:1: ruleAllValues returns [EObject current=null] : ( () otherlv_1= '<owl:allValuesFrom' (otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) )? (otherlv_5= '/>' | (otherlv_6= '>' ( (lv_modifiers_7_0= ruleClassModifier ) )* otherlv_8= '</owl:allValuesFrom' otherlv_9= '>' ) ) ) ;
    public final EObject ruleAllValues() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        EObject lv_ref_4_0 = null;

        EObject lv_modifiers_7_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:4313:28: ( ( () otherlv_1= '<owl:allValuesFrom' (otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) )? (otherlv_5= '/>' | (otherlv_6= '>' ( (lv_modifiers_7_0= ruleClassModifier ) )* otherlv_8= '</owl:allValuesFrom' otherlv_9= '>' ) ) ) )
            // InternalOWLModel.g:4314:1: ( () otherlv_1= '<owl:allValuesFrom' (otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) )? (otherlv_5= '/>' | (otherlv_6= '>' ( (lv_modifiers_7_0= ruleClassModifier ) )* otherlv_8= '</owl:allValuesFrom' otherlv_9= '>' ) ) )
            {
            // InternalOWLModel.g:4314:1: ( () otherlv_1= '<owl:allValuesFrom' (otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) )? (otherlv_5= '/>' | (otherlv_6= '>' ( (lv_modifiers_7_0= ruleClassModifier ) )* otherlv_8= '</owl:allValuesFrom' otherlv_9= '>' ) ) )
            // InternalOWLModel.g:4314:2: () otherlv_1= '<owl:allValuesFrom' (otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) )? (otherlv_5= '/>' | (otherlv_6= '>' ( (lv_modifiers_7_0= ruleClassModifier ) )* otherlv_8= '</owl:allValuesFrom' otherlv_9= '>' ) )
            {
            // InternalOWLModel.g:4314:2: ()
            // InternalOWLModel.g:4315:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getAllValuesAccess().getAllValuesAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,90,FOLLOW_78); 

                	newLeafNode(otherlv_1, grammarAccess.getAllValuesAccess().getOwlAllValuesFromKeyword_1());
                
            // InternalOWLModel.g:4324:1: (otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) )?
            int alt86=2;
            int LA86_0 = input.LA(1);

            if ( (LA86_0==32) ) {
                alt86=1;
            }
            switch (alt86) {
                case 1 :
                    // InternalOWLModel.g:4324:3: otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) )
                    {
                    otherlv_2=(Token)match(input,32,FOLLOW_13); 

                        	newLeafNode(otherlv_2, grammarAccess.getAllValuesAccess().getRdfResourceKeyword_2_0());
                        
                    otherlv_3=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_3, grammarAccess.getAllValuesAccess().getEqualsSignKeyword_2_1());
                        
                    // InternalOWLModel.g:4332:1: ( (lv_ref_4_0= ruleOwlRef ) )
                    // InternalOWLModel.g:4333:1: (lv_ref_4_0= ruleOwlRef )
                    {
                    // InternalOWLModel.g:4333:1: (lv_ref_4_0= ruleOwlRef )
                    // InternalOWLModel.g:4334:3: lv_ref_4_0= ruleOwlRef
                    {
                     
                    	        newCompositeNode(grammarAccess.getAllValuesAccess().getRefOwlRefParserRuleCall_2_2_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_ref_4_0=ruleOwlRef();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getAllValuesRule());
                    	        }
                           		set(
                           			current, 
                           			"ref",
                            		lv_ref_4_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlRef");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            // InternalOWLModel.g:4350:4: (otherlv_5= '/>' | (otherlv_6= '>' ( (lv_modifiers_7_0= ruleClassModifier ) )* otherlv_8= '</owl:allValuesFrom' otherlv_9= '>' ) )
            int alt88=2;
            int LA88_0 = input.LA(1);

            if ( (LA88_0==29) ) {
                alt88=1;
            }
            else if ( (LA88_0==21) ) {
                alt88=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 88, 0, input);

                throw nvae;
            }
            switch (alt88) {
                case 1 :
                    // InternalOWLModel.g:4350:6: otherlv_5= '/>'
                    {
                    otherlv_5=(Token)match(input,29,FOLLOW_2); 

                        	newLeafNode(otherlv_5, grammarAccess.getAllValuesAccess().getSolidusGreaterThanSignKeyword_3_0());
                        

                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:4355:6: (otherlv_6= '>' ( (lv_modifiers_7_0= ruleClassModifier ) )* otherlv_8= '</owl:allValuesFrom' otherlv_9= '>' )
                    {
                    // InternalOWLModel.g:4355:6: (otherlv_6= '>' ( (lv_modifiers_7_0= ruleClassModifier ) )* otherlv_8= '</owl:allValuesFrom' otherlv_9= '>' )
                    // InternalOWLModel.g:4355:8: otherlv_6= '>' ( (lv_modifiers_7_0= ruleClassModifier ) )* otherlv_8= '</owl:allValuesFrom' otherlv_9= '>'
                    {
                    otherlv_6=(Token)match(input,21,FOLLOW_80); 

                        	newLeafNode(otherlv_6, grammarAccess.getAllValuesAccess().getGreaterThanSignKeyword_3_1_0());
                        
                    // InternalOWLModel.g:4359:1: ( (lv_modifiers_7_0= ruleClassModifier ) )*
                    loop87:
                    do {
                        int alt87=2;
                        int LA87_0 = input.LA(1);

                        if ( (LA87_0==39||LA87_0==70||LA87_0==122) ) {
                            alt87=1;
                        }


                        switch (alt87) {
                    	case 1 :
                    	    // InternalOWLModel.g:4360:1: (lv_modifiers_7_0= ruleClassModifier )
                    	    {
                    	    // InternalOWLModel.g:4360:1: (lv_modifiers_7_0= ruleClassModifier )
                    	    // InternalOWLModel.g:4361:3: lv_modifiers_7_0= ruleClassModifier
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getAllValuesAccess().getModifiersClassModifierParserRuleCall_3_1_1_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_80);
                    	    lv_modifiers_7_0=ruleClassModifier();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getAllValuesRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"modifiers",
                    	            		lv_modifiers_7_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.ClassModifier");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop87;
                        }
                    } while (true);

                    otherlv_8=(Token)match(input,91,FOLLOW_7); 

                        	newLeafNode(otherlv_8, grammarAccess.getAllValuesAccess().getOwlAllValuesFromKeyword_3_1_2());
                        
                    otherlv_9=(Token)match(input,21,FOLLOW_2); 

                        	newLeafNode(otherlv_9, grammarAccess.getAllValuesAccess().getGreaterThanSignKeyword_3_1_3());
                        

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAllValues"


    // $ANTLR start "entryRuleHasValue"
    // InternalOWLModel.g:4393:1: entryRuleHasValue returns [EObject current=null] : iv_ruleHasValue= ruleHasValue EOF ;
    public final EObject entryRuleHasValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleHasValue = null;


        try {
            // InternalOWLModel.g:4394:2: (iv_ruleHasValue= ruleHasValue EOF )
            // InternalOWLModel.g:4395:2: iv_ruleHasValue= ruleHasValue EOF
            {
             newCompositeNode(grammarAccess.getHasValueRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleHasValue=ruleHasValue();

            state._fsp--;

             current =iv_ruleHasValue; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleHasValue"


    // $ANTLR start "ruleHasValue"
    // InternalOWLModel.g:4402:1: ruleHasValue returns [EObject current=null] : (otherlv_0= '<owl:hasValue' ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:hasValue' otherlv_7= '>' ) ) ) | (otherlv_8= 'rdf:datatype' otherlv_9= '=' ( (lv_type_10_0= ruleOwlRef ) ) otherlv_11= '>' ( (lv_value_12_0= ruleTAG_ANY_VALUE ) ) otherlv_13= '</owl:hasValue' otherlv_14= '>' ) ) ) ;
    public final EObject ruleHasValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        EObject lv_ref_3_0 = null;

        EObject lv_type_10_0 = null;

        AntlrDatatypeRuleToken lv_value_12_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:4405:28: ( (otherlv_0= '<owl:hasValue' ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:hasValue' otherlv_7= '>' ) ) ) | (otherlv_8= 'rdf:datatype' otherlv_9= '=' ( (lv_type_10_0= ruleOwlRef ) ) otherlv_11= '>' ( (lv_value_12_0= ruleTAG_ANY_VALUE ) ) otherlv_13= '</owl:hasValue' otherlv_14= '>' ) ) ) )
            // InternalOWLModel.g:4406:1: (otherlv_0= '<owl:hasValue' ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:hasValue' otherlv_7= '>' ) ) ) | (otherlv_8= 'rdf:datatype' otherlv_9= '=' ( (lv_type_10_0= ruleOwlRef ) ) otherlv_11= '>' ( (lv_value_12_0= ruleTAG_ANY_VALUE ) ) otherlv_13= '</owl:hasValue' otherlv_14= '>' ) ) )
            {
            // InternalOWLModel.g:4406:1: (otherlv_0= '<owl:hasValue' ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:hasValue' otherlv_7= '>' ) ) ) | (otherlv_8= 'rdf:datatype' otherlv_9= '=' ( (lv_type_10_0= ruleOwlRef ) ) otherlv_11= '>' ( (lv_value_12_0= ruleTAG_ANY_VALUE ) ) otherlv_13= '</owl:hasValue' otherlv_14= '>' ) ) )
            // InternalOWLModel.g:4406:3: otherlv_0= '<owl:hasValue' ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:hasValue' otherlv_7= '>' ) ) ) | (otherlv_8= 'rdf:datatype' otherlv_9= '=' ( (lv_type_10_0= ruleOwlRef ) ) otherlv_11= '>' ( (lv_value_12_0= ruleTAG_ANY_VALUE ) ) otherlv_13= '</owl:hasValue' otherlv_14= '>' ) )
            {
            otherlv_0=(Token)match(input,92,FOLLOW_81); 

                	newLeafNode(otherlv_0, grammarAccess.getHasValueAccess().getOwlHasValueKeyword_0());
                
            // InternalOWLModel.g:4410:1: ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:hasValue' otherlv_7= '>' ) ) ) | (otherlv_8= 'rdf:datatype' otherlv_9= '=' ( (lv_type_10_0= ruleOwlRef ) ) otherlv_11= '>' ( (lv_value_12_0= ruleTAG_ANY_VALUE ) ) otherlv_13= '</owl:hasValue' otherlv_14= '>' ) )
            int alt90=2;
            int LA90_0 = input.LA(1);

            if ( (LA90_0==32) ) {
                alt90=1;
            }
            else if ( (LA90_0==62) ) {
                alt90=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 90, 0, input);

                throw nvae;
            }
            switch (alt90) {
                case 1 :
                    // InternalOWLModel.g:4410:2: (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:hasValue' otherlv_7= '>' ) ) )
                    {
                    // InternalOWLModel.g:4410:2: (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:hasValue' otherlv_7= '>' ) ) )
                    // InternalOWLModel.g:4410:4: otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_ref_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:hasValue' otherlv_7= '>' ) )
                    {
                    otherlv_1=(Token)match(input,32,FOLLOW_13); 

                        	newLeafNode(otherlv_1, grammarAccess.getHasValueAccess().getRdfResourceKeyword_1_0_0());
                        
                    otherlv_2=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_2, grammarAccess.getHasValueAccess().getEqualsSignKeyword_1_0_1());
                        
                    // InternalOWLModel.g:4418:1: ( (lv_ref_3_0= ruleOwlRef ) )
                    // InternalOWLModel.g:4419:1: (lv_ref_3_0= ruleOwlRef )
                    {
                    // InternalOWLModel.g:4419:1: (lv_ref_3_0= ruleOwlRef )
                    // InternalOWLModel.g:4420:3: lv_ref_3_0= ruleOwlRef
                    {
                     
                    	        newCompositeNode(grammarAccess.getHasValueAccess().getRefOwlRefParserRuleCall_1_0_2_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_ref_3_0=ruleOwlRef();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getHasValueRule());
                    	        }
                           		set(
                           			current, 
                           			"ref",
                            		lv_ref_3_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlRef");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // InternalOWLModel.g:4436:2: (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</owl:hasValue' otherlv_7= '>' ) )
                    int alt89=2;
                    int LA89_0 = input.LA(1);

                    if ( (LA89_0==29) ) {
                        alt89=1;
                    }
                    else if ( (LA89_0==21) ) {
                        alt89=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 89, 0, input);

                        throw nvae;
                    }
                    switch (alt89) {
                        case 1 :
                            // InternalOWLModel.g:4436:4: otherlv_4= '/>'
                            {
                            otherlv_4=(Token)match(input,29,FOLLOW_2); 

                                	newLeafNode(otherlv_4, grammarAccess.getHasValueAccess().getSolidusGreaterThanSignKeyword_1_0_3_0());
                                

                            }
                            break;
                        case 2 :
                            // InternalOWLModel.g:4441:6: (otherlv_5= '>' otherlv_6= '</owl:hasValue' otherlv_7= '>' )
                            {
                            // InternalOWLModel.g:4441:6: (otherlv_5= '>' otherlv_6= '</owl:hasValue' otherlv_7= '>' )
                            // InternalOWLModel.g:4441:8: otherlv_5= '>' otherlv_6= '</owl:hasValue' otherlv_7= '>'
                            {
                            otherlv_5=(Token)match(input,21,FOLLOW_82); 

                                	newLeafNode(otherlv_5, grammarAccess.getHasValueAccess().getGreaterThanSignKeyword_1_0_3_1_0());
                                
                            otherlv_6=(Token)match(input,93,FOLLOW_7); 

                                	newLeafNode(otherlv_6, grammarAccess.getHasValueAccess().getOwlHasValueKeyword_1_0_3_1_1());
                                
                            otherlv_7=(Token)match(input,21,FOLLOW_2); 

                                	newLeafNode(otherlv_7, grammarAccess.getHasValueAccess().getGreaterThanSignKeyword_1_0_3_1_2());
                                

                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:4454:6: (otherlv_8= 'rdf:datatype' otherlv_9= '=' ( (lv_type_10_0= ruleOwlRef ) ) otherlv_11= '>' ( (lv_value_12_0= ruleTAG_ANY_VALUE ) ) otherlv_13= '</owl:hasValue' otherlv_14= '>' )
                    {
                    // InternalOWLModel.g:4454:6: (otherlv_8= 'rdf:datatype' otherlv_9= '=' ( (lv_type_10_0= ruleOwlRef ) ) otherlv_11= '>' ( (lv_value_12_0= ruleTAG_ANY_VALUE ) ) otherlv_13= '</owl:hasValue' otherlv_14= '>' )
                    // InternalOWLModel.g:4454:8: otherlv_8= 'rdf:datatype' otherlv_9= '=' ( (lv_type_10_0= ruleOwlRef ) ) otherlv_11= '>' ( (lv_value_12_0= ruleTAG_ANY_VALUE ) ) otherlv_13= '</owl:hasValue' otherlv_14= '>'
                    {
                    otherlv_8=(Token)match(input,62,FOLLOW_13); 

                        	newLeafNode(otherlv_8, grammarAccess.getHasValueAccess().getRdfDatatypeKeyword_1_1_0());
                        
                    otherlv_9=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_9, grammarAccess.getHasValueAccess().getEqualsSignKeyword_1_1_1());
                        
                    // InternalOWLModel.g:4462:1: ( (lv_type_10_0= ruleOwlRef ) )
                    // InternalOWLModel.g:4463:1: (lv_type_10_0= ruleOwlRef )
                    {
                    // InternalOWLModel.g:4463:1: (lv_type_10_0= ruleOwlRef )
                    // InternalOWLModel.g:4464:3: lv_type_10_0= ruleOwlRef
                    {
                     
                    	        newCompositeNode(grammarAccess.getHasValueAccess().getTypeOwlRefParserRuleCall_1_1_2_0()); 
                    	    
                    pushFollow(FOLLOW_7);
                    lv_type_10_0=ruleOwlRef();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getHasValueRule());
                    	        }
                           		set(
                           			current, 
                           			"type",
                            		lv_type_10_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlRef");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_11=(Token)match(input,21,FOLLOW_59); 

                        	newLeafNode(otherlv_11, grammarAccess.getHasValueAccess().getGreaterThanSignKeyword_1_1_3());
                        
                    // InternalOWLModel.g:4484:1: ( (lv_value_12_0= ruleTAG_ANY_VALUE ) )
                    // InternalOWLModel.g:4485:1: (lv_value_12_0= ruleTAG_ANY_VALUE )
                    {
                    // InternalOWLModel.g:4485:1: (lv_value_12_0= ruleTAG_ANY_VALUE )
                    // InternalOWLModel.g:4486:3: lv_value_12_0= ruleTAG_ANY_VALUE
                    {
                     
                    	        newCompositeNode(grammarAccess.getHasValueAccess().getValueTAG_ANY_VALUEParserRuleCall_1_1_4_0()); 
                    	    
                    pushFollow(FOLLOW_82);
                    lv_value_12_0=ruleTAG_ANY_VALUE();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getHasValueRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_12_0, 
                            		"org.smool.sdk.owlparser.OWLModel.TAG_ANY_VALUE");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_13=(Token)match(input,93,FOLLOW_7); 

                        	newLeafNode(otherlv_13, grammarAccess.getHasValueAccess().getOwlHasValueKeyword_1_1_5());
                        
                    otherlv_14=(Token)match(input,21,FOLLOW_2); 

                        	newLeafNode(otherlv_14, grammarAccess.getHasValueAccess().getGreaterThanSignKeyword_1_1_6());
                        

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleHasValue"


    // $ANTLR start "entryRuleProperty"
    // InternalOWLModel.g:4518:1: entryRuleProperty returns [EObject current=null] : iv_ruleProperty= ruleProperty EOF ;
    public final EObject entryRuleProperty() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProperty = null;


        try {
            // InternalOWLModel.g:4519:2: (iv_ruleProperty= ruleProperty EOF )
            // InternalOWLModel.g:4520:2: iv_ruleProperty= ruleProperty EOF
            {
             newCompositeNode(grammarAccess.getPropertyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleProperty=ruleProperty();

            state._fsp--;

             current =iv_ruleProperty; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProperty"


    // $ANTLR start "ruleProperty"
    // InternalOWLModel.g:4527:1: ruleProperty returns [EObject current=null] : (this_AnnotationProperty_0= ruleAnnotationProperty | this_ObjectProperty_1= ruleObjectProperty | this_FunctionalProperty_2= ruleFunctionalProperty | this_InverseFunctionalProperty_3= ruleInverseFunctionalProperty | this_SymmetricProperty_4= ruleSymmetricProperty | this_TransitiveProperty_5= ruleTransitiveProperty | this_DatatypeProperty_6= ruleDatatypeProperty ) ;
    public final EObject ruleProperty() throws RecognitionException {
        EObject current = null;

        EObject this_AnnotationProperty_0 = null;

        EObject this_ObjectProperty_1 = null;

        EObject this_FunctionalProperty_2 = null;

        EObject this_InverseFunctionalProperty_3 = null;

        EObject this_SymmetricProperty_4 = null;

        EObject this_TransitiveProperty_5 = null;

        EObject this_DatatypeProperty_6 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:4530:28: ( (this_AnnotationProperty_0= ruleAnnotationProperty | this_ObjectProperty_1= ruleObjectProperty | this_FunctionalProperty_2= ruleFunctionalProperty | this_InverseFunctionalProperty_3= ruleInverseFunctionalProperty | this_SymmetricProperty_4= ruleSymmetricProperty | this_TransitiveProperty_5= ruleTransitiveProperty | this_DatatypeProperty_6= ruleDatatypeProperty ) )
            // InternalOWLModel.g:4531:1: (this_AnnotationProperty_0= ruleAnnotationProperty | this_ObjectProperty_1= ruleObjectProperty | this_FunctionalProperty_2= ruleFunctionalProperty | this_InverseFunctionalProperty_3= ruleInverseFunctionalProperty | this_SymmetricProperty_4= ruleSymmetricProperty | this_TransitiveProperty_5= ruleTransitiveProperty | this_DatatypeProperty_6= ruleDatatypeProperty )
            {
            // InternalOWLModel.g:4531:1: (this_AnnotationProperty_0= ruleAnnotationProperty | this_ObjectProperty_1= ruleObjectProperty | this_FunctionalProperty_2= ruleFunctionalProperty | this_InverseFunctionalProperty_3= ruleInverseFunctionalProperty | this_SymmetricProperty_4= ruleSymmetricProperty | this_TransitiveProperty_5= ruleTransitiveProperty | this_DatatypeProperty_6= ruleDatatypeProperty )
            int alt91=7;
            switch ( input.LA(1) ) {
            case 94:
                {
                alt91=1;
                }
                break;
            case 96:
                {
                alt91=2;
                }
                break;
            case 100:
                {
                alt91=3;
                }
                break;
            case 102:
                {
                alt91=4;
                }
                break;
            case 104:
                {
                alt91=5;
                }
                break;
            case 106:
                {
                alt91=6;
                }
                break;
            case 98:
                {
                alt91=7;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 91, 0, input);

                throw nvae;
            }

            switch (alt91) {
                case 1 :
                    // InternalOWLModel.g:4532:5: this_AnnotationProperty_0= ruleAnnotationProperty
                    {
                     
                            newCompositeNode(grammarAccess.getPropertyAccess().getAnnotationPropertyParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_2);
                    this_AnnotationProperty_0=ruleAnnotationProperty();

                    state._fsp--;

                     
                            current = this_AnnotationProperty_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:4542:5: this_ObjectProperty_1= ruleObjectProperty
                    {
                     
                            newCompositeNode(grammarAccess.getPropertyAccess().getObjectPropertyParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_2);
                    this_ObjectProperty_1=ruleObjectProperty();

                    state._fsp--;

                     
                            current = this_ObjectProperty_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // InternalOWLModel.g:4552:5: this_FunctionalProperty_2= ruleFunctionalProperty
                    {
                     
                            newCompositeNode(grammarAccess.getPropertyAccess().getFunctionalPropertyParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_2);
                    this_FunctionalProperty_2=ruleFunctionalProperty();

                    state._fsp--;

                     
                            current = this_FunctionalProperty_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // InternalOWLModel.g:4562:5: this_InverseFunctionalProperty_3= ruleInverseFunctionalProperty
                    {
                     
                            newCompositeNode(grammarAccess.getPropertyAccess().getInverseFunctionalPropertyParserRuleCall_3()); 
                        
                    pushFollow(FOLLOW_2);
                    this_InverseFunctionalProperty_3=ruleInverseFunctionalProperty();

                    state._fsp--;

                     
                            current = this_InverseFunctionalProperty_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 5 :
                    // InternalOWLModel.g:4572:5: this_SymmetricProperty_4= ruleSymmetricProperty
                    {
                     
                            newCompositeNode(grammarAccess.getPropertyAccess().getSymmetricPropertyParserRuleCall_4()); 
                        
                    pushFollow(FOLLOW_2);
                    this_SymmetricProperty_4=ruleSymmetricProperty();

                    state._fsp--;

                     
                            current = this_SymmetricProperty_4; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 6 :
                    // InternalOWLModel.g:4582:5: this_TransitiveProperty_5= ruleTransitiveProperty
                    {
                     
                            newCompositeNode(grammarAccess.getPropertyAccess().getTransitivePropertyParserRuleCall_5()); 
                        
                    pushFollow(FOLLOW_2);
                    this_TransitiveProperty_5=ruleTransitiveProperty();

                    state._fsp--;

                     
                            current = this_TransitiveProperty_5; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 7 :
                    // InternalOWLModel.g:4592:5: this_DatatypeProperty_6= ruleDatatypeProperty
                    {
                     
                            newCompositeNode(grammarAccess.getPropertyAccess().getDatatypePropertyParserRuleCall_6()); 
                        
                    pushFollow(FOLLOW_2);
                    this_DatatypeProperty_6=ruleDatatypeProperty();

                    state._fsp--;

                     
                            current = this_DatatypeProperty_6; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProperty"


    // $ANTLR start "entryRuleAnnotationProperty"
    // InternalOWLModel.g:4608:1: entryRuleAnnotationProperty returns [EObject current=null] : iv_ruleAnnotationProperty= ruleAnnotationProperty EOF ;
    public final EObject entryRuleAnnotationProperty() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAnnotationProperty = null;


        try {
            // InternalOWLModel.g:4609:2: (iv_ruleAnnotationProperty= ruleAnnotationProperty EOF )
            // InternalOWLModel.g:4610:2: iv_ruleAnnotationProperty= ruleAnnotationProperty EOF
            {
             newCompositeNode(grammarAccess.getAnnotationPropertyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAnnotationProperty=ruleAnnotationProperty();

            state._fsp--;

             current =iv_ruleAnnotationProperty; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnnotationProperty"


    // $ANTLR start "ruleAnnotationProperty"
    // InternalOWLModel.g:4617:1: ruleAnnotationProperty returns [EObject current=null] : (otherlv_0= '<owl:AnnotationProperty' ( (otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) ) ) | ( ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) ) ) ) (otherlv_7= '/>' | (otherlv_8= '>' ( ( (lv_others_9_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_ranges_12_0= ruleRDFSRange ) ) | ( (lv_types_13_0= ruleRDFType ) ) | ( (lv_domains_14_0= ruleRDFSDomain ) ) | ( (lv_modifiers_15_0= rulePropertyModifier ) ) )* otherlv_16= '</owl:AnnotationProperty' otherlv_17= '>' ) ) ) ;
    public final EObject ruleAnnotationProperty() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_reference_4_0=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        EObject lv_id_3_0 = null;

        EObject lv_ref_6_0 = null;

        AntlrDatatypeRuleToken lv_others_9_0 = null;

        EObject lv_labels_10_0 = null;

        EObject lv_versionInfo_11_0 = null;

        EObject lv_ranges_12_0 = null;

        EObject lv_types_13_0 = null;

        EObject lv_domains_14_0 = null;

        EObject lv_modifiers_15_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:4620:28: ( (otherlv_0= '<owl:AnnotationProperty' ( (otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) ) ) | ( ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) ) ) ) (otherlv_7= '/>' | (otherlv_8= '>' ( ( (lv_others_9_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_ranges_12_0= ruleRDFSRange ) ) | ( (lv_types_13_0= ruleRDFType ) ) | ( (lv_domains_14_0= ruleRDFSDomain ) ) | ( (lv_modifiers_15_0= rulePropertyModifier ) ) )* otherlv_16= '</owl:AnnotationProperty' otherlv_17= '>' ) ) ) )
            // InternalOWLModel.g:4621:1: (otherlv_0= '<owl:AnnotationProperty' ( (otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) ) ) | ( ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) ) ) ) (otherlv_7= '/>' | (otherlv_8= '>' ( ( (lv_others_9_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_ranges_12_0= ruleRDFSRange ) ) | ( (lv_types_13_0= ruleRDFType ) ) | ( (lv_domains_14_0= ruleRDFSDomain ) ) | ( (lv_modifiers_15_0= rulePropertyModifier ) ) )* otherlv_16= '</owl:AnnotationProperty' otherlv_17= '>' ) ) )
            {
            // InternalOWLModel.g:4621:1: (otherlv_0= '<owl:AnnotationProperty' ( (otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) ) ) | ( ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) ) ) ) (otherlv_7= '/>' | (otherlv_8= '>' ( ( (lv_others_9_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_ranges_12_0= ruleRDFSRange ) ) | ( (lv_types_13_0= ruleRDFType ) ) | ( (lv_domains_14_0= ruleRDFSDomain ) ) | ( (lv_modifiers_15_0= rulePropertyModifier ) ) )* otherlv_16= '</owl:AnnotationProperty' otherlv_17= '>' ) ) )
            // InternalOWLModel.g:4621:3: otherlv_0= '<owl:AnnotationProperty' ( (otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) ) ) | ( ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) ) ) ) (otherlv_7= '/>' | (otherlv_8= '>' ( ( (lv_others_9_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_ranges_12_0= ruleRDFSRange ) ) | ( (lv_types_13_0= ruleRDFType ) ) | ( (lv_domains_14_0= ruleRDFSDomain ) ) | ( (lv_modifiers_15_0= rulePropertyModifier ) ) )* otherlv_16= '</owl:AnnotationProperty' otherlv_17= '>' ) )
            {
            otherlv_0=(Token)match(input,94,FOLLOW_34); 

                	newLeafNode(otherlv_0, grammarAccess.getAnnotationPropertyAccess().getOwlAnnotationPropertyKeyword_0());
                
            // InternalOWLModel.g:4625:1: ( (otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) ) ) | ( ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) ) ) )
            int alt92=2;
            int LA92_0 = input.LA(1);

            if ( (LA92_0==40) ) {
                alt92=1;
            }
            else if ( (LA92_0==28) ) {
                alt92=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 92, 0, input);

                throw nvae;
            }
            switch (alt92) {
                case 1 :
                    // InternalOWLModel.g:4625:2: (otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) ) )
                    {
                    // InternalOWLModel.g:4625:2: (otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) ) )
                    // InternalOWLModel.g:4625:4: otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) )
                    {
                    otherlv_1=(Token)match(input,40,FOLLOW_13); 

                        	newLeafNode(otherlv_1, grammarAccess.getAnnotationPropertyAccess().getRdfIDKeyword_1_0_0());
                        
                    otherlv_2=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_2, grammarAccess.getAnnotationPropertyAccess().getEqualsSignKeyword_1_0_1());
                        
                    // InternalOWLModel.g:4633:1: ( (lv_id_3_0= ruleOwlID ) )
                    // InternalOWLModel.g:4634:1: (lv_id_3_0= ruleOwlID )
                    {
                    // InternalOWLModel.g:4634:1: (lv_id_3_0= ruleOwlID )
                    // InternalOWLModel.g:4635:3: lv_id_3_0= ruleOwlID
                    {
                     
                    	        newCompositeNode(grammarAccess.getAnnotationPropertyAccess().getIdOwlIDParserRuleCall_1_0_2_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_id_3_0=ruleOwlID();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getAnnotationPropertyRule());
                    	        }
                           		set(
                           			current, 
                           			"id",
                            		lv_id_3_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlID");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:4652:6: ( ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) ) )
                    {
                    // InternalOWLModel.g:4652:6: ( ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) ) )
                    // InternalOWLModel.g:4652:7: ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) )
                    {
                    // InternalOWLModel.g:4652:7: ( (lv_reference_4_0= 'rdf:about' ) )
                    // InternalOWLModel.g:4653:1: (lv_reference_4_0= 'rdf:about' )
                    {
                    // InternalOWLModel.g:4653:1: (lv_reference_4_0= 'rdf:about' )
                    // InternalOWLModel.g:4654:3: lv_reference_4_0= 'rdf:about'
                    {
                    lv_reference_4_0=(Token)match(input,28,FOLLOW_13); 

                            newLeafNode(lv_reference_4_0, grammarAccess.getAnnotationPropertyAccess().getReferenceRdfAboutKeyword_1_1_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getAnnotationPropertyRule());
                    	        }
                           		setWithLastConsumed(current, "reference", true, "rdf:about");
                    	    

                    }


                    }

                    otherlv_5=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_5, grammarAccess.getAnnotationPropertyAccess().getEqualsSignKeyword_1_1_1());
                        
                    // InternalOWLModel.g:4671:1: ( (lv_ref_6_0= ruleOwlRef ) )
                    // InternalOWLModel.g:4672:1: (lv_ref_6_0= ruleOwlRef )
                    {
                    // InternalOWLModel.g:4672:1: (lv_ref_6_0= ruleOwlRef )
                    // InternalOWLModel.g:4673:3: lv_ref_6_0= ruleOwlRef
                    {
                     
                    	        newCompositeNode(grammarAccess.getAnnotationPropertyAccess().getRefOwlRefParserRuleCall_1_1_2_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_ref_6_0=ruleOwlRef();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getAnnotationPropertyRule());
                    	        }
                           		set(
                           			current, 
                           			"ref",
                            		lv_ref_6_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlRef");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;

            }

            // InternalOWLModel.g:4689:4: (otherlv_7= '/>' | (otherlv_8= '>' ( ( (lv_others_9_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_ranges_12_0= ruleRDFSRange ) ) | ( (lv_types_13_0= ruleRDFType ) ) | ( (lv_domains_14_0= ruleRDFSDomain ) ) | ( (lv_modifiers_15_0= rulePropertyModifier ) ) )* otherlv_16= '</owl:AnnotationProperty' otherlv_17= '>' ) )
            int alt94=2;
            int LA94_0 = input.LA(1);

            if ( (LA94_0==29) ) {
                alt94=1;
            }
            else if ( (LA94_0==21) ) {
                alt94=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 94, 0, input);

                throw nvae;
            }
            switch (alt94) {
                case 1 :
                    // InternalOWLModel.g:4689:6: otherlv_7= '/>'
                    {
                    otherlv_7=(Token)match(input,29,FOLLOW_2); 

                        	newLeafNode(otherlv_7, grammarAccess.getAnnotationPropertyAccess().getSolidusGreaterThanSignKeyword_2_0());
                        

                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:4694:6: (otherlv_8= '>' ( ( (lv_others_9_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_ranges_12_0= ruleRDFSRange ) ) | ( (lv_types_13_0= ruleRDFType ) ) | ( (lv_domains_14_0= ruleRDFSDomain ) ) | ( (lv_modifiers_15_0= rulePropertyModifier ) ) )* otherlv_16= '</owl:AnnotationProperty' otherlv_17= '>' )
                    {
                    // InternalOWLModel.g:4694:6: (otherlv_8= '>' ( ( (lv_others_9_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_ranges_12_0= ruleRDFSRange ) ) | ( (lv_types_13_0= ruleRDFType ) ) | ( (lv_domains_14_0= ruleRDFSDomain ) ) | ( (lv_modifiers_15_0= rulePropertyModifier ) ) )* otherlv_16= '</owl:AnnotationProperty' otherlv_17= '>' )
                    // InternalOWLModel.g:4694:8: otherlv_8= '>' ( ( (lv_others_9_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_ranges_12_0= ruleRDFSRange ) ) | ( (lv_types_13_0= ruleRDFType ) ) | ( (lv_domains_14_0= ruleRDFSDomain ) ) | ( (lv_modifiers_15_0= rulePropertyModifier ) ) )* otherlv_16= '</owl:AnnotationProperty' otherlv_17= '>'
                    {
                    otherlv_8=(Token)match(input,21,FOLLOW_83); 

                        	newLeafNode(otherlv_8, grammarAccess.getAnnotationPropertyAccess().getGreaterThanSignKeyword_2_1_0());
                        
                    // InternalOWLModel.g:4698:1: ( ( (lv_others_9_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_ranges_12_0= ruleRDFSRange ) ) | ( (lv_types_13_0= ruleRDFType ) ) | ( (lv_domains_14_0= ruleRDFSDomain ) ) | ( (lv_modifiers_15_0= rulePropertyModifier ) ) )*
                    loop93:
                    do {
                        int alt93=8;
                        switch ( input.LA(1) ) {
                        case 58:
                            {
                            alt93=1;
                            }
                            break;
                        case 60:
                            {
                            alt93=2;
                            }
                            break;
                        case 64:
                            {
                            alt93=3;
                            }
                            break;
                        case 114:
                            {
                            alt93=4;
                            }
                            break;
                        case 118:
                            {
                            alt93=5;
                            }
                            break;
                        case 112:
                            {
                            alt93=6;
                            }
                            break;
                        case 108:
                        case 110:
                        case 120:
                            {
                            alt93=7;
                            }
                            break;

                        }

                        switch (alt93) {
                    	case 1 :
                    	    // InternalOWLModel.g:4698:2: ( (lv_others_9_0= ruleANY_OTHER_TAG ) )
                    	    {
                    	    // InternalOWLModel.g:4698:2: ( (lv_others_9_0= ruleANY_OTHER_TAG ) )
                    	    // InternalOWLModel.g:4699:1: (lv_others_9_0= ruleANY_OTHER_TAG )
                    	    {
                    	    // InternalOWLModel.g:4699:1: (lv_others_9_0= ruleANY_OTHER_TAG )
                    	    // InternalOWLModel.g:4700:3: lv_others_9_0= ruleANY_OTHER_TAG
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getAnnotationPropertyAccess().getOthersANY_OTHER_TAGParserRuleCall_2_1_1_0_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_83);
                    	    lv_others_9_0=ruleANY_OTHER_TAG();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getAnnotationPropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"others",
                    	            		lv_others_9_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.ANY_OTHER_TAG");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalOWLModel.g:4717:6: ( (lv_labels_10_0= ruleOwlLabel ) )
                    	    {
                    	    // InternalOWLModel.g:4717:6: ( (lv_labels_10_0= ruleOwlLabel ) )
                    	    // InternalOWLModel.g:4718:1: (lv_labels_10_0= ruleOwlLabel )
                    	    {
                    	    // InternalOWLModel.g:4718:1: (lv_labels_10_0= ruleOwlLabel )
                    	    // InternalOWLModel.g:4719:3: lv_labels_10_0= ruleOwlLabel
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getAnnotationPropertyAccess().getLabelsOwlLabelParserRuleCall_2_1_1_1_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_83);
                    	    lv_labels_10_0=ruleOwlLabel();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getAnnotationPropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"labels",
                    	            		lv_labels_10_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.OwlLabel");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 3 :
                    	    // InternalOWLModel.g:4736:6: ( (lv_versionInfo_11_0= ruleOwlVersion ) )
                    	    {
                    	    // InternalOWLModel.g:4736:6: ( (lv_versionInfo_11_0= ruleOwlVersion ) )
                    	    // InternalOWLModel.g:4737:1: (lv_versionInfo_11_0= ruleOwlVersion )
                    	    {
                    	    // InternalOWLModel.g:4737:1: (lv_versionInfo_11_0= ruleOwlVersion )
                    	    // InternalOWLModel.g:4738:3: lv_versionInfo_11_0= ruleOwlVersion
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getAnnotationPropertyAccess().getVersionInfoOwlVersionParserRuleCall_2_1_1_2_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_83);
                    	    lv_versionInfo_11_0=ruleOwlVersion();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getAnnotationPropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"versionInfo",
                    	            		lv_versionInfo_11_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.OwlVersion");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 4 :
                    	    // InternalOWLModel.g:4755:6: ( (lv_ranges_12_0= ruleRDFSRange ) )
                    	    {
                    	    // InternalOWLModel.g:4755:6: ( (lv_ranges_12_0= ruleRDFSRange ) )
                    	    // InternalOWLModel.g:4756:1: (lv_ranges_12_0= ruleRDFSRange )
                    	    {
                    	    // InternalOWLModel.g:4756:1: (lv_ranges_12_0= ruleRDFSRange )
                    	    // InternalOWLModel.g:4757:3: lv_ranges_12_0= ruleRDFSRange
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getAnnotationPropertyAccess().getRangesRDFSRangeParserRuleCall_2_1_1_3_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_83);
                    	    lv_ranges_12_0=ruleRDFSRange();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getAnnotationPropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"ranges",
                    	            		lv_ranges_12_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.RDFSRange");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 5 :
                    	    // InternalOWLModel.g:4774:6: ( (lv_types_13_0= ruleRDFType ) )
                    	    {
                    	    // InternalOWLModel.g:4774:6: ( (lv_types_13_0= ruleRDFType ) )
                    	    // InternalOWLModel.g:4775:1: (lv_types_13_0= ruleRDFType )
                    	    {
                    	    // InternalOWLModel.g:4775:1: (lv_types_13_0= ruleRDFType )
                    	    // InternalOWLModel.g:4776:3: lv_types_13_0= ruleRDFType
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getAnnotationPropertyAccess().getTypesRDFTypeParserRuleCall_2_1_1_4_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_83);
                    	    lv_types_13_0=ruleRDFType();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getAnnotationPropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"types",
                    	            		lv_types_13_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.RDFType");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 6 :
                    	    // InternalOWLModel.g:4793:6: ( (lv_domains_14_0= ruleRDFSDomain ) )
                    	    {
                    	    // InternalOWLModel.g:4793:6: ( (lv_domains_14_0= ruleRDFSDomain ) )
                    	    // InternalOWLModel.g:4794:1: (lv_domains_14_0= ruleRDFSDomain )
                    	    {
                    	    // InternalOWLModel.g:4794:1: (lv_domains_14_0= ruleRDFSDomain )
                    	    // InternalOWLModel.g:4795:3: lv_domains_14_0= ruleRDFSDomain
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getAnnotationPropertyAccess().getDomainsRDFSDomainParserRuleCall_2_1_1_5_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_83);
                    	    lv_domains_14_0=ruleRDFSDomain();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getAnnotationPropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"domains",
                    	            		lv_domains_14_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.RDFSDomain");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 7 :
                    	    // InternalOWLModel.g:4812:6: ( (lv_modifiers_15_0= rulePropertyModifier ) )
                    	    {
                    	    // InternalOWLModel.g:4812:6: ( (lv_modifiers_15_0= rulePropertyModifier ) )
                    	    // InternalOWLModel.g:4813:1: (lv_modifiers_15_0= rulePropertyModifier )
                    	    {
                    	    // InternalOWLModel.g:4813:1: (lv_modifiers_15_0= rulePropertyModifier )
                    	    // InternalOWLModel.g:4814:3: lv_modifiers_15_0= rulePropertyModifier
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getAnnotationPropertyAccess().getModifiersPropertyModifierParserRuleCall_2_1_1_6_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_83);
                    	    lv_modifiers_15_0=rulePropertyModifier();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getAnnotationPropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"modifiers",
                    	            		lv_modifiers_15_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.PropertyModifier");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop93;
                        }
                    } while (true);

                    otherlv_16=(Token)match(input,95,FOLLOW_7); 

                        	newLeafNode(otherlv_16, grammarAccess.getAnnotationPropertyAccess().getOwlAnnotationPropertyKeyword_2_1_2());
                        
                    otherlv_17=(Token)match(input,21,FOLLOW_2); 

                        	newLeafNode(otherlv_17, grammarAccess.getAnnotationPropertyAccess().getGreaterThanSignKeyword_2_1_3());
                        

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnnotationProperty"


    // $ANTLR start "entryRuleObjectProperty"
    // InternalOWLModel.g:4846:1: entryRuleObjectProperty returns [EObject current=null] : iv_ruleObjectProperty= ruleObjectProperty EOF ;
    public final EObject entryRuleObjectProperty() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleObjectProperty = null;


        try {
            // InternalOWLModel.g:4847:2: (iv_ruleObjectProperty= ruleObjectProperty EOF )
            // InternalOWLModel.g:4848:2: iv_ruleObjectProperty= ruleObjectProperty EOF
            {
             newCompositeNode(grammarAccess.getObjectPropertyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleObjectProperty=ruleObjectProperty();

            state._fsp--;

             current =iv_ruleObjectProperty; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleObjectProperty"


    // $ANTLR start "ruleObjectProperty"
    // InternalOWLModel.g:4855:1: ruleObjectProperty returns [EObject current=null] : (otherlv_0= '<owl:ObjectProperty' ( (otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) ) ) | ( ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) ) ) ) (otherlv_7= '/>' | (otherlv_8= '>' ( ( (lv_others_9_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_ranges_12_0= ruleRDFSRange ) ) | ( (lv_types_13_0= ruleRDFType ) ) | ( (lv_domains_14_0= ruleRDFSDomain ) ) | ( (lv_modifiers_15_0= rulePropertyModifier ) ) )* otherlv_16= '</owl:ObjectProperty' otherlv_17= '>' ) ) ) ;
    public final EObject ruleObjectProperty() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_reference_4_0=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        EObject lv_id_3_0 = null;

        EObject lv_ref_6_0 = null;

        AntlrDatatypeRuleToken lv_others_9_0 = null;

        EObject lv_labels_10_0 = null;

        EObject lv_versionInfo_11_0 = null;

        EObject lv_ranges_12_0 = null;

        EObject lv_types_13_0 = null;

        EObject lv_domains_14_0 = null;

        EObject lv_modifiers_15_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:4858:28: ( (otherlv_0= '<owl:ObjectProperty' ( (otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) ) ) | ( ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) ) ) ) (otherlv_7= '/>' | (otherlv_8= '>' ( ( (lv_others_9_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_ranges_12_0= ruleRDFSRange ) ) | ( (lv_types_13_0= ruleRDFType ) ) | ( (lv_domains_14_0= ruleRDFSDomain ) ) | ( (lv_modifiers_15_0= rulePropertyModifier ) ) )* otherlv_16= '</owl:ObjectProperty' otherlv_17= '>' ) ) ) )
            // InternalOWLModel.g:4859:1: (otherlv_0= '<owl:ObjectProperty' ( (otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) ) ) | ( ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) ) ) ) (otherlv_7= '/>' | (otherlv_8= '>' ( ( (lv_others_9_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_ranges_12_0= ruleRDFSRange ) ) | ( (lv_types_13_0= ruleRDFType ) ) | ( (lv_domains_14_0= ruleRDFSDomain ) ) | ( (lv_modifiers_15_0= rulePropertyModifier ) ) )* otherlv_16= '</owl:ObjectProperty' otherlv_17= '>' ) ) )
            {
            // InternalOWLModel.g:4859:1: (otherlv_0= '<owl:ObjectProperty' ( (otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) ) ) | ( ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) ) ) ) (otherlv_7= '/>' | (otherlv_8= '>' ( ( (lv_others_9_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_ranges_12_0= ruleRDFSRange ) ) | ( (lv_types_13_0= ruleRDFType ) ) | ( (lv_domains_14_0= ruleRDFSDomain ) ) | ( (lv_modifiers_15_0= rulePropertyModifier ) ) )* otherlv_16= '</owl:ObjectProperty' otherlv_17= '>' ) ) )
            // InternalOWLModel.g:4859:3: otherlv_0= '<owl:ObjectProperty' ( (otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) ) ) | ( ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) ) ) ) (otherlv_7= '/>' | (otherlv_8= '>' ( ( (lv_others_9_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_ranges_12_0= ruleRDFSRange ) ) | ( (lv_types_13_0= ruleRDFType ) ) | ( (lv_domains_14_0= ruleRDFSDomain ) ) | ( (lv_modifiers_15_0= rulePropertyModifier ) ) )* otherlv_16= '</owl:ObjectProperty' otherlv_17= '>' ) )
            {
            otherlv_0=(Token)match(input,96,FOLLOW_34); 

                	newLeafNode(otherlv_0, grammarAccess.getObjectPropertyAccess().getOwlObjectPropertyKeyword_0());
                
            // InternalOWLModel.g:4863:1: ( (otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) ) ) | ( ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) ) ) )
            int alt95=2;
            int LA95_0 = input.LA(1);

            if ( (LA95_0==40) ) {
                alt95=1;
            }
            else if ( (LA95_0==28) ) {
                alt95=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 95, 0, input);

                throw nvae;
            }
            switch (alt95) {
                case 1 :
                    // InternalOWLModel.g:4863:2: (otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) ) )
                    {
                    // InternalOWLModel.g:4863:2: (otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) ) )
                    // InternalOWLModel.g:4863:4: otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) )
                    {
                    otherlv_1=(Token)match(input,40,FOLLOW_13); 

                        	newLeafNode(otherlv_1, grammarAccess.getObjectPropertyAccess().getRdfIDKeyword_1_0_0());
                        
                    otherlv_2=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_2, grammarAccess.getObjectPropertyAccess().getEqualsSignKeyword_1_0_1());
                        
                    // InternalOWLModel.g:4871:1: ( (lv_id_3_0= ruleOwlID ) )
                    // InternalOWLModel.g:4872:1: (lv_id_3_0= ruleOwlID )
                    {
                    // InternalOWLModel.g:4872:1: (lv_id_3_0= ruleOwlID )
                    // InternalOWLModel.g:4873:3: lv_id_3_0= ruleOwlID
                    {
                     
                    	        newCompositeNode(grammarAccess.getObjectPropertyAccess().getIdOwlIDParserRuleCall_1_0_2_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_id_3_0=ruleOwlID();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getObjectPropertyRule());
                    	        }
                           		set(
                           			current, 
                           			"id",
                            		lv_id_3_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlID");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:4890:6: ( ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) ) )
                    {
                    // InternalOWLModel.g:4890:6: ( ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) ) )
                    // InternalOWLModel.g:4890:7: ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) )
                    {
                    // InternalOWLModel.g:4890:7: ( (lv_reference_4_0= 'rdf:about' ) )
                    // InternalOWLModel.g:4891:1: (lv_reference_4_0= 'rdf:about' )
                    {
                    // InternalOWLModel.g:4891:1: (lv_reference_4_0= 'rdf:about' )
                    // InternalOWLModel.g:4892:3: lv_reference_4_0= 'rdf:about'
                    {
                    lv_reference_4_0=(Token)match(input,28,FOLLOW_13); 

                            newLeafNode(lv_reference_4_0, grammarAccess.getObjectPropertyAccess().getReferenceRdfAboutKeyword_1_1_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getObjectPropertyRule());
                    	        }
                           		setWithLastConsumed(current, "reference", true, "rdf:about");
                    	    

                    }


                    }

                    otherlv_5=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_5, grammarAccess.getObjectPropertyAccess().getEqualsSignKeyword_1_1_1());
                        
                    // InternalOWLModel.g:4909:1: ( (lv_ref_6_0= ruleOwlRef ) )
                    // InternalOWLModel.g:4910:1: (lv_ref_6_0= ruleOwlRef )
                    {
                    // InternalOWLModel.g:4910:1: (lv_ref_6_0= ruleOwlRef )
                    // InternalOWLModel.g:4911:3: lv_ref_6_0= ruleOwlRef
                    {
                     
                    	        newCompositeNode(grammarAccess.getObjectPropertyAccess().getRefOwlRefParserRuleCall_1_1_2_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_ref_6_0=ruleOwlRef();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getObjectPropertyRule());
                    	        }
                           		set(
                           			current, 
                           			"ref",
                            		lv_ref_6_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlRef");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;

            }

            // InternalOWLModel.g:4927:4: (otherlv_7= '/>' | (otherlv_8= '>' ( ( (lv_others_9_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_ranges_12_0= ruleRDFSRange ) ) | ( (lv_types_13_0= ruleRDFType ) ) | ( (lv_domains_14_0= ruleRDFSDomain ) ) | ( (lv_modifiers_15_0= rulePropertyModifier ) ) )* otherlv_16= '</owl:ObjectProperty' otherlv_17= '>' ) )
            int alt97=2;
            int LA97_0 = input.LA(1);

            if ( (LA97_0==29) ) {
                alt97=1;
            }
            else if ( (LA97_0==21) ) {
                alt97=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 97, 0, input);

                throw nvae;
            }
            switch (alt97) {
                case 1 :
                    // InternalOWLModel.g:4927:6: otherlv_7= '/>'
                    {
                    otherlv_7=(Token)match(input,29,FOLLOW_2); 

                        	newLeafNode(otherlv_7, grammarAccess.getObjectPropertyAccess().getSolidusGreaterThanSignKeyword_2_0());
                        

                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:4932:6: (otherlv_8= '>' ( ( (lv_others_9_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_ranges_12_0= ruleRDFSRange ) ) | ( (lv_types_13_0= ruleRDFType ) ) | ( (lv_domains_14_0= ruleRDFSDomain ) ) | ( (lv_modifiers_15_0= rulePropertyModifier ) ) )* otherlv_16= '</owl:ObjectProperty' otherlv_17= '>' )
                    {
                    // InternalOWLModel.g:4932:6: (otherlv_8= '>' ( ( (lv_others_9_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_ranges_12_0= ruleRDFSRange ) ) | ( (lv_types_13_0= ruleRDFType ) ) | ( (lv_domains_14_0= ruleRDFSDomain ) ) | ( (lv_modifiers_15_0= rulePropertyModifier ) ) )* otherlv_16= '</owl:ObjectProperty' otherlv_17= '>' )
                    // InternalOWLModel.g:4932:8: otherlv_8= '>' ( ( (lv_others_9_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_ranges_12_0= ruleRDFSRange ) ) | ( (lv_types_13_0= ruleRDFType ) ) | ( (lv_domains_14_0= ruleRDFSDomain ) ) | ( (lv_modifiers_15_0= rulePropertyModifier ) ) )* otherlv_16= '</owl:ObjectProperty' otherlv_17= '>'
                    {
                    otherlv_8=(Token)match(input,21,FOLLOW_84); 

                        	newLeafNode(otherlv_8, grammarAccess.getObjectPropertyAccess().getGreaterThanSignKeyword_2_1_0());
                        
                    // InternalOWLModel.g:4936:1: ( ( (lv_others_9_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_ranges_12_0= ruleRDFSRange ) ) | ( (lv_types_13_0= ruleRDFType ) ) | ( (lv_domains_14_0= ruleRDFSDomain ) ) | ( (lv_modifiers_15_0= rulePropertyModifier ) ) )*
                    loop96:
                    do {
                        int alt96=8;
                        switch ( input.LA(1) ) {
                        case 58:
                            {
                            alt96=1;
                            }
                            break;
                        case 60:
                            {
                            alt96=2;
                            }
                            break;
                        case 64:
                            {
                            alt96=3;
                            }
                            break;
                        case 114:
                            {
                            alt96=4;
                            }
                            break;
                        case 118:
                            {
                            alt96=5;
                            }
                            break;
                        case 112:
                            {
                            alt96=6;
                            }
                            break;
                        case 108:
                        case 110:
                        case 120:
                            {
                            alt96=7;
                            }
                            break;

                        }

                        switch (alt96) {
                    	case 1 :
                    	    // InternalOWLModel.g:4936:2: ( (lv_others_9_0= ruleANY_OTHER_TAG ) )
                    	    {
                    	    // InternalOWLModel.g:4936:2: ( (lv_others_9_0= ruleANY_OTHER_TAG ) )
                    	    // InternalOWLModel.g:4937:1: (lv_others_9_0= ruleANY_OTHER_TAG )
                    	    {
                    	    // InternalOWLModel.g:4937:1: (lv_others_9_0= ruleANY_OTHER_TAG )
                    	    // InternalOWLModel.g:4938:3: lv_others_9_0= ruleANY_OTHER_TAG
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getObjectPropertyAccess().getOthersANY_OTHER_TAGParserRuleCall_2_1_1_0_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_84);
                    	    lv_others_9_0=ruleANY_OTHER_TAG();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getObjectPropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"others",
                    	            		lv_others_9_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.ANY_OTHER_TAG");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalOWLModel.g:4955:6: ( (lv_labels_10_0= ruleOwlLabel ) )
                    	    {
                    	    // InternalOWLModel.g:4955:6: ( (lv_labels_10_0= ruleOwlLabel ) )
                    	    // InternalOWLModel.g:4956:1: (lv_labels_10_0= ruleOwlLabel )
                    	    {
                    	    // InternalOWLModel.g:4956:1: (lv_labels_10_0= ruleOwlLabel )
                    	    // InternalOWLModel.g:4957:3: lv_labels_10_0= ruleOwlLabel
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getObjectPropertyAccess().getLabelsOwlLabelParserRuleCall_2_1_1_1_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_84);
                    	    lv_labels_10_0=ruleOwlLabel();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getObjectPropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"labels",
                    	            		lv_labels_10_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.OwlLabel");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 3 :
                    	    // InternalOWLModel.g:4974:6: ( (lv_versionInfo_11_0= ruleOwlVersion ) )
                    	    {
                    	    // InternalOWLModel.g:4974:6: ( (lv_versionInfo_11_0= ruleOwlVersion ) )
                    	    // InternalOWLModel.g:4975:1: (lv_versionInfo_11_0= ruleOwlVersion )
                    	    {
                    	    // InternalOWLModel.g:4975:1: (lv_versionInfo_11_0= ruleOwlVersion )
                    	    // InternalOWLModel.g:4976:3: lv_versionInfo_11_0= ruleOwlVersion
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getObjectPropertyAccess().getVersionInfoOwlVersionParserRuleCall_2_1_1_2_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_84);
                    	    lv_versionInfo_11_0=ruleOwlVersion();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getObjectPropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"versionInfo",
                    	            		lv_versionInfo_11_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.OwlVersion");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 4 :
                    	    // InternalOWLModel.g:4993:6: ( (lv_ranges_12_0= ruleRDFSRange ) )
                    	    {
                    	    // InternalOWLModel.g:4993:6: ( (lv_ranges_12_0= ruleRDFSRange ) )
                    	    // InternalOWLModel.g:4994:1: (lv_ranges_12_0= ruleRDFSRange )
                    	    {
                    	    // InternalOWLModel.g:4994:1: (lv_ranges_12_0= ruleRDFSRange )
                    	    // InternalOWLModel.g:4995:3: lv_ranges_12_0= ruleRDFSRange
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getObjectPropertyAccess().getRangesRDFSRangeParserRuleCall_2_1_1_3_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_84);
                    	    lv_ranges_12_0=ruleRDFSRange();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getObjectPropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"ranges",
                    	            		lv_ranges_12_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.RDFSRange");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 5 :
                    	    // InternalOWLModel.g:5012:6: ( (lv_types_13_0= ruleRDFType ) )
                    	    {
                    	    // InternalOWLModel.g:5012:6: ( (lv_types_13_0= ruleRDFType ) )
                    	    // InternalOWLModel.g:5013:1: (lv_types_13_0= ruleRDFType )
                    	    {
                    	    // InternalOWLModel.g:5013:1: (lv_types_13_0= ruleRDFType )
                    	    // InternalOWLModel.g:5014:3: lv_types_13_0= ruleRDFType
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getObjectPropertyAccess().getTypesRDFTypeParserRuleCall_2_1_1_4_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_84);
                    	    lv_types_13_0=ruleRDFType();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getObjectPropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"types",
                    	            		lv_types_13_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.RDFType");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 6 :
                    	    // InternalOWLModel.g:5031:6: ( (lv_domains_14_0= ruleRDFSDomain ) )
                    	    {
                    	    // InternalOWLModel.g:5031:6: ( (lv_domains_14_0= ruleRDFSDomain ) )
                    	    // InternalOWLModel.g:5032:1: (lv_domains_14_0= ruleRDFSDomain )
                    	    {
                    	    // InternalOWLModel.g:5032:1: (lv_domains_14_0= ruleRDFSDomain )
                    	    // InternalOWLModel.g:5033:3: lv_domains_14_0= ruleRDFSDomain
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getObjectPropertyAccess().getDomainsRDFSDomainParserRuleCall_2_1_1_5_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_84);
                    	    lv_domains_14_0=ruleRDFSDomain();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getObjectPropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"domains",
                    	            		lv_domains_14_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.RDFSDomain");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 7 :
                    	    // InternalOWLModel.g:5050:6: ( (lv_modifiers_15_0= rulePropertyModifier ) )
                    	    {
                    	    // InternalOWLModel.g:5050:6: ( (lv_modifiers_15_0= rulePropertyModifier ) )
                    	    // InternalOWLModel.g:5051:1: (lv_modifiers_15_0= rulePropertyModifier )
                    	    {
                    	    // InternalOWLModel.g:5051:1: (lv_modifiers_15_0= rulePropertyModifier )
                    	    // InternalOWLModel.g:5052:3: lv_modifiers_15_0= rulePropertyModifier
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getObjectPropertyAccess().getModifiersPropertyModifierParserRuleCall_2_1_1_6_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_84);
                    	    lv_modifiers_15_0=rulePropertyModifier();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getObjectPropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"modifiers",
                    	            		lv_modifiers_15_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.PropertyModifier");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop96;
                        }
                    } while (true);

                    otherlv_16=(Token)match(input,97,FOLLOW_7); 

                        	newLeafNode(otherlv_16, grammarAccess.getObjectPropertyAccess().getOwlObjectPropertyKeyword_2_1_2());
                        
                    otherlv_17=(Token)match(input,21,FOLLOW_2); 

                        	newLeafNode(otherlv_17, grammarAccess.getObjectPropertyAccess().getGreaterThanSignKeyword_2_1_3());
                        

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleObjectProperty"


    // $ANTLR start "entryRuleDatatypeProperty"
    // InternalOWLModel.g:5084:1: entryRuleDatatypeProperty returns [EObject current=null] : iv_ruleDatatypeProperty= ruleDatatypeProperty EOF ;
    public final EObject entryRuleDatatypeProperty() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDatatypeProperty = null;


        try {
            // InternalOWLModel.g:5085:2: (iv_ruleDatatypeProperty= ruleDatatypeProperty EOF )
            // InternalOWLModel.g:5086:2: iv_ruleDatatypeProperty= ruleDatatypeProperty EOF
            {
             newCompositeNode(grammarAccess.getDatatypePropertyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDatatypeProperty=ruleDatatypeProperty();

            state._fsp--;

             current =iv_ruleDatatypeProperty; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDatatypeProperty"


    // $ANTLR start "ruleDatatypeProperty"
    // InternalOWLModel.g:5093:1: ruleDatatypeProperty returns [EObject current=null] : (otherlv_0= '<owl:DatatypeProperty' ( (otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) ) ) | ( ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) ) ) ) (otherlv_7= '/>' | (otherlv_8= '>' ( ( (lv_others_9_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_ranges_12_0= ruleRDFSRange ) ) | ( (lv_types_13_0= ruleRDFType ) ) | ( (lv_domains_14_0= ruleRDFSDomain ) ) | ( (lv_modifiers_15_0= rulePropertyModifier ) ) )* otherlv_16= '</owl:DatatypeProperty' otherlv_17= '>' ) ) ) ;
    public final EObject ruleDatatypeProperty() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_reference_4_0=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        EObject lv_id_3_0 = null;

        EObject lv_ref_6_0 = null;

        AntlrDatatypeRuleToken lv_others_9_0 = null;

        EObject lv_labels_10_0 = null;

        EObject lv_versionInfo_11_0 = null;

        EObject lv_ranges_12_0 = null;

        EObject lv_types_13_0 = null;

        EObject lv_domains_14_0 = null;

        EObject lv_modifiers_15_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:5096:28: ( (otherlv_0= '<owl:DatatypeProperty' ( (otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) ) ) | ( ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) ) ) ) (otherlv_7= '/>' | (otherlv_8= '>' ( ( (lv_others_9_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_ranges_12_0= ruleRDFSRange ) ) | ( (lv_types_13_0= ruleRDFType ) ) | ( (lv_domains_14_0= ruleRDFSDomain ) ) | ( (lv_modifiers_15_0= rulePropertyModifier ) ) )* otherlv_16= '</owl:DatatypeProperty' otherlv_17= '>' ) ) ) )
            // InternalOWLModel.g:5097:1: (otherlv_0= '<owl:DatatypeProperty' ( (otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) ) ) | ( ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) ) ) ) (otherlv_7= '/>' | (otherlv_8= '>' ( ( (lv_others_9_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_ranges_12_0= ruleRDFSRange ) ) | ( (lv_types_13_0= ruleRDFType ) ) | ( (lv_domains_14_0= ruleRDFSDomain ) ) | ( (lv_modifiers_15_0= rulePropertyModifier ) ) )* otherlv_16= '</owl:DatatypeProperty' otherlv_17= '>' ) ) )
            {
            // InternalOWLModel.g:5097:1: (otherlv_0= '<owl:DatatypeProperty' ( (otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) ) ) | ( ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) ) ) ) (otherlv_7= '/>' | (otherlv_8= '>' ( ( (lv_others_9_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_ranges_12_0= ruleRDFSRange ) ) | ( (lv_types_13_0= ruleRDFType ) ) | ( (lv_domains_14_0= ruleRDFSDomain ) ) | ( (lv_modifiers_15_0= rulePropertyModifier ) ) )* otherlv_16= '</owl:DatatypeProperty' otherlv_17= '>' ) ) )
            // InternalOWLModel.g:5097:3: otherlv_0= '<owl:DatatypeProperty' ( (otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) ) ) | ( ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) ) ) ) (otherlv_7= '/>' | (otherlv_8= '>' ( ( (lv_others_9_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_ranges_12_0= ruleRDFSRange ) ) | ( (lv_types_13_0= ruleRDFType ) ) | ( (lv_domains_14_0= ruleRDFSDomain ) ) | ( (lv_modifiers_15_0= rulePropertyModifier ) ) )* otherlv_16= '</owl:DatatypeProperty' otherlv_17= '>' ) )
            {
            otherlv_0=(Token)match(input,98,FOLLOW_34); 

                	newLeafNode(otherlv_0, grammarAccess.getDatatypePropertyAccess().getOwlDatatypePropertyKeyword_0());
                
            // InternalOWLModel.g:5101:1: ( (otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) ) ) | ( ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) ) ) )
            int alt98=2;
            int LA98_0 = input.LA(1);

            if ( (LA98_0==40) ) {
                alt98=1;
            }
            else if ( (LA98_0==28) ) {
                alt98=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 98, 0, input);

                throw nvae;
            }
            switch (alt98) {
                case 1 :
                    // InternalOWLModel.g:5101:2: (otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) ) )
                    {
                    // InternalOWLModel.g:5101:2: (otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) ) )
                    // InternalOWLModel.g:5101:4: otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) )
                    {
                    otherlv_1=(Token)match(input,40,FOLLOW_13); 

                        	newLeafNode(otherlv_1, grammarAccess.getDatatypePropertyAccess().getRdfIDKeyword_1_0_0());
                        
                    otherlv_2=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_2, grammarAccess.getDatatypePropertyAccess().getEqualsSignKeyword_1_0_1());
                        
                    // InternalOWLModel.g:5109:1: ( (lv_id_3_0= ruleOwlID ) )
                    // InternalOWLModel.g:5110:1: (lv_id_3_0= ruleOwlID )
                    {
                    // InternalOWLModel.g:5110:1: (lv_id_3_0= ruleOwlID )
                    // InternalOWLModel.g:5111:3: lv_id_3_0= ruleOwlID
                    {
                     
                    	        newCompositeNode(grammarAccess.getDatatypePropertyAccess().getIdOwlIDParserRuleCall_1_0_2_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_id_3_0=ruleOwlID();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getDatatypePropertyRule());
                    	        }
                           		set(
                           			current, 
                           			"id",
                            		lv_id_3_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlID");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:5128:6: ( ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) ) )
                    {
                    // InternalOWLModel.g:5128:6: ( ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) ) )
                    // InternalOWLModel.g:5128:7: ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) )
                    {
                    // InternalOWLModel.g:5128:7: ( (lv_reference_4_0= 'rdf:about' ) )
                    // InternalOWLModel.g:5129:1: (lv_reference_4_0= 'rdf:about' )
                    {
                    // InternalOWLModel.g:5129:1: (lv_reference_4_0= 'rdf:about' )
                    // InternalOWLModel.g:5130:3: lv_reference_4_0= 'rdf:about'
                    {
                    lv_reference_4_0=(Token)match(input,28,FOLLOW_13); 

                            newLeafNode(lv_reference_4_0, grammarAccess.getDatatypePropertyAccess().getReferenceRdfAboutKeyword_1_1_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getDatatypePropertyRule());
                    	        }
                           		setWithLastConsumed(current, "reference", true, "rdf:about");
                    	    

                    }


                    }

                    otherlv_5=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_5, grammarAccess.getDatatypePropertyAccess().getEqualsSignKeyword_1_1_1());
                        
                    // InternalOWLModel.g:5147:1: ( (lv_ref_6_0= ruleOwlRef ) )
                    // InternalOWLModel.g:5148:1: (lv_ref_6_0= ruleOwlRef )
                    {
                    // InternalOWLModel.g:5148:1: (lv_ref_6_0= ruleOwlRef )
                    // InternalOWLModel.g:5149:3: lv_ref_6_0= ruleOwlRef
                    {
                     
                    	        newCompositeNode(grammarAccess.getDatatypePropertyAccess().getRefOwlRefParserRuleCall_1_1_2_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_ref_6_0=ruleOwlRef();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getDatatypePropertyRule());
                    	        }
                           		set(
                           			current, 
                           			"ref",
                            		lv_ref_6_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlRef");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;

            }

            // InternalOWLModel.g:5165:4: (otherlv_7= '/>' | (otherlv_8= '>' ( ( (lv_others_9_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_ranges_12_0= ruleRDFSRange ) ) | ( (lv_types_13_0= ruleRDFType ) ) | ( (lv_domains_14_0= ruleRDFSDomain ) ) | ( (lv_modifiers_15_0= rulePropertyModifier ) ) )* otherlv_16= '</owl:DatatypeProperty' otherlv_17= '>' ) )
            int alt100=2;
            int LA100_0 = input.LA(1);

            if ( (LA100_0==29) ) {
                alt100=1;
            }
            else if ( (LA100_0==21) ) {
                alt100=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 100, 0, input);

                throw nvae;
            }
            switch (alt100) {
                case 1 :
                    // InternalOWLModel.g:5165:6: otherlv_7= '/>'
                    {
                    otherlv_7=(Token)match(input,29,FOLLOW_2); 

                        	newLeafNode(otherlv_7, grammarAccess.getDatatypePropertyAccess().getSolidusGreaterThanSignKeyword_2_0());
                        

                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:5170:6: (otherlv_8= '>' ( ( (lv_others_9_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_ranges_12_0= ruleRDFSRange ) ) | ( (lv_types_13_0= ruleRDFType ) ) | ( (lv_domains_14_0= ruleRDFSDomain ) ) | ( (lv_modifiers_15_0= rulePropertyModifier ) ) )* otherlv_16= '</owl:DatatypeProperty' otherlv_17= '>' )
                    {
                    // InternalOWLModel.g:5170:6: (otherlv_8= '>' ( ( (lv_others_9_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_ranges_12_0= ruleRDFSRange ) ) | ( (lv_types_13_0= ruleRDFType ) ) | ( (lv_domains_14_0= ruleRDFSDomain ) ) | ( (lv_modifiers_15_0= rulePropertyModifier ) ) )* otherlv_16= '</owl:DatatypeProperty' otherlv_17= '>' )
                    // InternalOWLModel.g:5170:8: otherlv_8= '>' ( ( (lv_others_9_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_ranges_12_0= ruleRDFSRange ) ) | ( (lv_types_13_0= ruleRDFType ) ) | ( (lv_domains_14_0= ruleRDFSDomain ) ) | ( (lv_modifiers_15_0= rulePropertyModifier ) ) )* otherlv_16= '</owl:DatatypeProperty' otherlv_17= '>'
                    {
                    otherlv_8=(Token)match(input,21,FOLLOW_85); 

                        	newLeafNode(otherlv_8, grammarAccess.getDatatypePropertyAccess().getGreaterThanSignKeyword_2_1_0());
                        
                    // InternalOWLModel.g:5174:1: ( ( (lv_others_9_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_10_0= ruleOwlLabel ) ) | ( (lv_versionInfo_11_0= ruleOwlVersion ) ) | ( (lv_ranges_12_0= ruleRDFSRange ) ) | ( (lv_types_13_0= ruleRDFType ) ) | ( (lv_domains_14_0= ruleRDFSDomain ) ) | ( (lv_modifiers_15_0= rulePropertyModifier ) ) )*
                    loop99:
                    do {
                        int alt99=8;
                        switch ( input.LA(1) ) {
                        case 58:
                            {
                            alt99=1;
                            }
                            break;
                        case 60:
                            {
                            alt99=2;
                            }
                            break;
                        case 64:
                            {
                            alt99=3;
                            }
                            break;
                        case 114:
                            {
                            alt99=4;
                            }
                            break;
                        case 118:
                            {
                            alt99=5;
                            }
                            break;
                        case 112:
                            {
                            alt99=6;
                            }
                            break;
                        case 108:
                        case 110:
                        case 120:
                            {
                            alt99=7;
                            }
                            break;

                        }

                        switch (alt99) {
                    	case 1 :
                    	    // InternalOWLModel.g:5174:2: ( (lv_others_9_0= ruleANY_OTHER_TAG ) )
                    	    {
                    	    // InternalOWLModel.g:5174:2: ( (lv_others_9_0= ruleANY_OTHER_TAG ) )
                    	    // InternalOWLModel.g:5175:1: (lv_others_9_0= ruleANY_OTHER_TAG )
                    	    {
                    	    // InternalOWLModel.g:5175:1: (lv_others_9_0= ruleANY_OTHER_TAG )
                    	    // InternalOWLModel.g:5176:3: lv_others_9_0= ruleANY_OTHER_TAG
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getDatatypePropertyAccess().getOthersANY_OTHER_TAGParserRuleCall_2_1_1_0_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_85);
                    	    lv_others_9_0=ruleANY_OTHER_TAG();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getDatatypePropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"others",
                    	            		lv_others_9_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.ANY_OTHER_TAG");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalOWLModel.g:5193:6: ( (lv_labels_10_0= ruleOwlLabel ) )
                    	    {
                    	    // InternalOWLModel.g:5193:6: ( (lv_labels_10_0= ruleOwlLabel ) )
                    	    // InternalOWLModel.g:5194:1: (lv_labels_10_0= ruleOwlLabel )
                    	    {
                    	    // InternalOWLModel.g:5194:1: (lv_labels_10_0= ruleOwlLabel )
                    	    // InternalOWLModel.g:5195:3: lv_labels_10_0= ruleOwlLabel
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getDatatypePropertyAccess().getLabelsOwlLabelParserRuleCall_2_1_1_1_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_85);
                    	    lv_labels_10_0=ruleOwlLabel();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getDatatypePropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"labels",
                    	            		lv_labels_10_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.OwlLabel");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 3 :
                    	    // InternalOWLModel.g:5212:6: ( (lv_versionInfo_11_0= ruleOwlVersion ) )
                    	    {
                    	    // InternalOWLModel.g:5212:6: ( (lv_versionInfo_11_0= ruleOwlVersion ) )
                    	    // InternalOWLModel.g:5213:1: (lv_versionInfo_11_0= ruleOwlVersion )
                    	    {
                    	    // InternalOWLModel.g:5213:1: (lv_versionInfo_11_0= ruleOwlVersion )
                    	    // InternalOWLModel.g:5214:3: lv_versionInfo_11_0= ruleOwlVersion
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getDatatypePropertyAccess().getVersionInfoOwlVersionParserRuleCall_2_1_1_2_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_85);
                    	    lv_versionInfo_11_0=ruleOwlVersion();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getDatatypePropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"versionInfo",
                    	            		lv_versionInfo_11_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.OwlVersion");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 4 :
                    	    // InternalOWLModel.g:5231:6: ( (lv_ranges_12_0= ruleRDFSRange ) )
                    	    {
                    	    // InternalOWLModel.g:5231:6: ( (lv_ranges_12_0= ruleRDFSRange ) )
                    	    // InternalOWLModel.g:5232:1: (lv_ranges_12_0= ruleRDFSRange )
                    	    {
                    	    // InternalOWLModel.g:5232:1: (lv_ranges_12_0= ruleRDFSRange )
                    	    // InternalOWLModel.g:5233:3: lv_ranges_12_0= ruleRDFSRange
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getDatatypePropertyAccess().getRangesRDFSRangeParserRuleCall_2_1_1_3_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_85);
                    	    lv_ranges_12_0=ruleRDFSRange();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getDatatypePropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"ranges",
                    	            		lv_ranges_12_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.RDFSRange");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 5 :
                    	    // InternalOWLModel.g:5250:6: ( (lv_types_13_0= ruleRDFType ) )
                    	    {
                    	    // InternalOWLModel.g:5250:6: ( (lv_types_13_0= ruleRDFType ) )
                    	    // InternalOWLModel.g:5251:1: (lv_types_13_0= ruleRDFType )
                    	    {
                    	    // InternalOWLModel.g:5251:1: (lv_types_13_0= ruleRDFType )
                    	    // InternalOWLModel.g:5252:3: lv_types_13_0= ruleRDFType
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getDatatypePropertyAccess().getTypesRDFTypeParserRuleCall_2_1_1_4_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_85);
                    	    lv_types_13_0=ruleRDFType();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getDatatypePropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"types",
                    	            		lv_types_13_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.RDFType");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 6 :
                    	    // InternalOWLModel.g:5269:6: ( (lv_domains_14_0= ruleRDFSDomain ) )
                    	    {
                    	    // InternalOWLModel.g:5269:6: ( (lv_domains_14_0= ruleRDFSDomain ) )
                    	    // InternalOWLModel.g:5270:1: (lv_domains_14_0= ruleRDFSDomain )
                    	    {
                    	    // InternalOWLModel.g:5270:1: (lv_domains_14_0= ruleRDFSDomain )
                    	    // InternalOWLModel.g:5271:3: lv_domains_14_0= ruleRDFSDomain
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getDatatypePropertyAccess().getDomainsRDFSDomainParserRuleCall_2_1_1_5_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_85);
                    	    lv_domains_14_0=ruleRDFSDomain();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getDatatypePropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"domains",
                    	            		lv_domains_14_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.RDFSDomain");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 7 :
                    	    // InternalOWLModel.g:5288:6: ( (lv_modifiers_15_0= rulePropertyModifier ) )
                    	    {
                    	    // InternalOWLModel.g:5288:6: ( (lv_modifiers_15_0= rulePropertyModifier ) )
                    	    // InternalOWLModel.g:5289:1: (lv_modifiers_15_0= rulePropertyModifier )
                    	    {
                    	    // InternalOWLModel.g:5289:1: (lv_modifiers_15_0= rulePropertyModifier )
                    	    // InternalOWLModel.g:5290:3: lv_modifiers_15_0= rulePropertyModifier
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getDatatypePropertyAccess().getModifiersPropertyModifierParserRuleCall_2_1_1_6_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_85);
                    	    lv_modifiers_15_0=rulePropertyModifier();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getDatatypePropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"modifiers",
                    	            		lv_modifiers_15_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.PropertyModifier");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop99;
                        }
                    } while (true);

                    otherlv_16=(Token)match(input,99,FOLLOW_7); 

                        	newLeafNode(otherlv_16, grammarAccess.getDatatypePropertyAccess().getOwlDatatypePropertyKeyword_2_1_2());
                        
                    otherlv_17=(Token)match(input,21,FOLLOW_2); 

                        	newLeafNode(otherlv_17, grammarAccess.getDatatypePropertyAccess().getGreaterThanSignKeyword_2_1_3());
                        

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDatatypeProperty"


    // $ANTLR start "entryRuleFunctionalProperty"
    // InternalOWLModel.g:5322:1: entryRuleFunctionalProperty returns [EObject current=null] : iv_ruleFunctionalProperty= ruleFunctionalProperty EOF ;
    public final EObject entryRuleFunctionalProperty() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFunctionalProperty = null;


        try {
            // InternalOWLModel.g:5323:2: (iv_ruleFunctionalProperty= ruleFunctionalProperty EOF )
            // InternalOWLModel.g:5324:2: iv_ruleFunctionalProperty= ruleFunctionalProperty EOF
            {
             newCompositeNode(grammarAccess.getFunctionalPropertyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFunctionalProperty=ruleFunctionalProperty();

            state._fsp--;

             current =iv_ruleFunctionalProperty; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFunctionalProperty"


    // $ANTLR start "ruleFunctionalProperty"
    // InternalOWLModel.g:5331:1: ruleFunctionalProperty returns [EObject current=null] : ( () otherlv_1= '<owl:FunctionalProperty' ( (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) ) | ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) ) ) (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:FunctionalProperty' otherlv_18= '>' ) ) ) ;
    public final EObject ruleFunctionalProperty() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token lv_reference_5_0=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        EObject lv_id_4_0 = null;

        EObject lv_ref_7_0 = null;

        AntlrDatatypeRuleToken lv_others_10_0 = null;

        EObject lv_labels_11_0 = null;

        EObject lv_versionInfo_12_0 = null;

        EObject lv_ranges_13_0 = null;

        EObject lv_types_14_0 = null;

        EObject lv_domains_15_0 = null;

        EObject lv_modifiers_16_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:5334:28: ( ( () otherlv_1= '<owl:FunctionalProperty' ( (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) ) | ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) ) ) (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:FunctionalProperty' otherlv_18= '>' ) ) ) )
            // InternalOWLModel.g:5335:1: ( () otherlv_1= '<owl:FunctionalProperty' ( (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) ) | ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) ) ) (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:FunctionalProperty' otherlv_18= '>' ) ) )
            {
            // InternalOWLModel.g:5335:1: ( () otherlv_1= '<owl:FunctionalProperty' ( (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) ) | ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) ) ) (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:FunctionalProperty' otherlv_18= '>' ) ) )
            // InternalOWLModel.g:5335:2: () otherlv_1= '<owl:FunctionalProperty' ( (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) ) | ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) ) ) (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:FunctionalProperty' otherlv_18= '>' ) )
            {
            // InternalOWLModel.g:5335:2: ()
            // InternalOWLModel.g:5336:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getFunctionalPropertyAccess().getFunctionalPropertyAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,100,FOLLOW_34); 

                	newLeafNode(otherlv_1, grammarAccess.getFunctionalPropertyAccess().getOwlFunctionalPropertyKeyword_1());
                
            // InternalOWLModel.g:5345:1: ( (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) ) | ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) ) )
            int alt101=2;
            int LA101_0 = input.LA(1);

            if ( (LA101_0==40) ) {
                alt101=1;
            }
            else if ( (LA101_0==28) ) {
                alt101=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 101, 0, input);

                throw nvae;
            }
            switch (alt101) {
                case 1 :
                    // InternalOWLModel.g:5345:2: (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) )
                    {
                    // InternalOWLModel.g:5345:2: (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) )
                    // InternalOWLModel.g:5345:4: otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) )
                    {
                    otherlv_2=(Token)match(input,40,FOLLOW_13); 

                        	newLeafNode(otherlv_2, grammarAccess.getFunctionalPropertyAccess().getRdfIDKeyword_2_0_0());
                        
                    otherlv_3=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_3, grammarAccess.getFunctionalPropertyAccess().getEqualsSignKeyword_2_0_1());
                        
                    // InternalOWLModel.g:5353:1: ( (lv_id_4_0= ruleOwlID ) )
                    // InternalOWLModel.g:5354:1: (lv_id_4_0= ruleOwlID )
                    {
                    // InternalOWLModel.g:5354:1: (lv_id_4_0= ruleOwlID )
                    // InternalOWLModel.g:5355:3: lv_id_4_0= ruleOwlID
                    {
                     
                    	        newCompositeNode(grammarAccess.getFunctionalPropertyAccess().getIdOwlIDParserRuleCall_2_0_2_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_id_4_0=ruleOwlID();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getFunctionalPropertyRule());
                    	        }
                           		set(
                           			current, 
                           			"id",
                            		lv_id_4_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlID");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:5372:6: ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) )
                    {
                    // InternalOWLModel.g:5372:6: ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) )
                    // InternalOWLModel.g:5372:7: ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) )
                    {
                    // InternalOWLModel.g:5372:7: ( (lv_reference_5_0= 'rdf:about' ) )
                    // InternalOWLModel.g:5373:1: (lv_reference_5_0= 'rdf:about' )
                    {
                    // InternalOWLModel.g:5373:1: (lv_reference_5_0= 'rdf:about' )
                    // InternalOWLModel.g:5374:3: lv_reference_5_0= 'rdf:about'
                    {
                    lv_reference_5_0=(Token)match(input,28,FOLLOW_13); 

                            newLeafNode(lv_reference_5_0, grammarAccess.getFunctionalPropertyAccess().getReferenceRdfAboutKeyword_2_1_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getFunctionalPropertyRule());
                    	        }
                           		setWithLastConsumed(current, "reference", true, "rdf:about");
                    	    

                    }


                    }

                    otherlv_6=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_6, grammarAccess.getFunctionalPropertyAccess().getEqualsSignKeyword_2_1_1());
                        
                    // InternalOWLModel.g:5391:1: ( (lv_ref_7_0= ruleOwlRef ) )
                    // InternalOWLModel.g:5392:1: (lv_ref_7_0= ruleOwlRef )
                    {
                    // InternalOWLModel.g:5392:1: (lv_ref_7_0= ruleOwlRef )
                    // InternalOWLModel.g:5393:3: lv_ref_7_0= ruleOwlRef
                    {
                     
                    	        newCompositeNode(grammarAccess.getFunctionalPropertyAccess().getRefOwlRefParserRuleCall_2_1_2_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_ref_7_0=ruleOwlRef();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getFunctionalPropertyRule());
                    	        }
                           		set(
                           			current, 
                           			"ref",
                            		lv_ref_7_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlRef");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;

            }

            // InternalOWLModel.g:5409:4: (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:FunctionalProperty' otherlv_18= '>' ) )
            int alt103=2;
            int LA103_0 = input.LA(1);

            if ( (LA103_0==29) ) {
                alt103=1;
            }
            else if ( (LA103_0==21) ) {
                alt103=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 103, 0, input);

                throw nvae;
            }
            switch (alt103) {
                case 1 :
                    // InternalOWLModel.g:5409:6: otherlv_8= '/>'
                    {
                    otherlv_8=(Token)match(input,29,FOLLOW_2); 

                        	newLeafNode(otherlv_8, grammarAccess.getFunctionalPropertyAccess().getSolidusGreaterThanSignKeyword_3_0());
                        

                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:5414:6: (otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:FunctionalProperty' otherlv_18= '>' )
                    {
                    // InternalOWLModel.g:5414:6: (otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:FunctionalProperty' otherlv_18= '>' )
                    // InternalOWLModel.g:5414:8: otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:FunctionalProperty' otherlv_18= '>'
                    {
                    otherlv_9=(Token)match(input,21,FOLLOW_86); 

                        	newLeafNode(otherlv_9, grammarAccess.getFunctionalPropertyAccess().getGreaterThanSignKeyword_3_1_0());
                        
                    // InternalOWLModel.g:5418:1: ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )*
                    loop102:
                    do {
                        int alt102=8;
                        switch ( input.LA(1) ) {
                        case 58:
                            {
                            alt102=1;
                            }
                            break;
                        case 60:
                            {
                            alt102=2;
                            }
                            break;
                        case 64:
                            {
                            alt102=3;
                            }
                            break;
                        case 114:
                            {
                            alt102=4;
                            }
                            break;
                        case 118:
                            {
                            alt102=5;
                            }
                            break;
                        case 112:
                            {
                            alt102=6;
                            }
                            break;
                        case 108:
                        case 110:
                        case 120:
                            {
                            alt102=7;
                            }
                            break;

                        }

                        switch (alt102) {
                    	case 1 :
                    	    // InternalOWLModel.g:5418:2: ( (lv_others_10_0= ruleANY_OTHER_TAG ) )
                    	    {
                    	    // InternalOWLModel.g:5418:2: ( (lv_others_10_0= ruleANY_OTHER_TAG ) )
                    	    // InternalOWLModel.g:5419:1: (lv_others_10_0= ruleANY_OTHER_TAG )
                    	    {
                    	    // InternalOWLModel.g:5419:1: (lv_others_10_0= ruleANY_OTHER_TAG )
                    	    // InternalOWLModel.g:5420:3: lv_others_10_0= ruleANY_OTHER_TAG
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getFunctionalPropertyAccess().getOthersANY_OTHER_TAGParserRuleCall_3_1_1_0_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_86);
                    	    lv_others_10_0=ruleANY_OTHER_TAG();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getFunctionalPropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"others",
                    	            		lv_others_10_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.ANY_OTHER_TAG");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalOWLModel.g:5437:6: ( (lv_labels_11_0= ruleOwlLabel ) )
                    	    {
                    	    // InternalOWLModel.g:5437:6: ( (lv_labels_11_0= ruleOwlLabel ) )
                    	    // InternalOWLModel.g:5438:1: (lv_labels_11_0= ruleOwlLabel )
                    	    {
                    	    // InternalOWLModel.g:5438:1: (lv_labels_11_0= ruleOwlLabel )
                    	    // InternalOWLModel.g:5439:3: lv_labels_11_0= ruleOwlLabel
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getFunctionalPropertyAccess().getLabelsOwlLabelParserRuleCall_3_1_1_1_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_86);
                    	    lv_labels_11_0=ruleOwlLabel();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getFunctionalPropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"labels",
                    	            		lv_labels_11_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.OwlLabel");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 3 :
                    	    // InternalOWLModel.g:5456:6: ( (lv_versionInfo_12_0= ruleOwlVersion ) )
                    	    {
                    	    // InternalOWLModel.g:5456:6: ( (lv_versionInfo_12_0= ruleOwlVersion ) )
                    	    // InternalOWLModel.g:5457:1: (lv_versionInfo_12_0= ruleOwlVersion )
                    	    {
                    	    // InternalOWLModel.g:5457:1: (lv_versionInfo_12_0= ruleOwlVersion )
                    	    // InternalOWLModel.g:5458:3: lv_versionInfo_12_0= ruleOwlVersion
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getFunctionalPropertyAccess().getVersionInfoOwlVersionParserRuleCall_3_1_1_2_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_86);
                    	    lv_versionInfo_12_0=ruleOwlVersion();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getFunctionalPropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"versionInfo",
                    	            		lv_versionInfo_12_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.OwlVersion");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 4 :
                    	    // InternalOWLModel.g:5475:6: ( (lv_ranges_13_0= ruleRDFSRange ) )
                    	    {
                    	    // InternalOWLModel.g:5475:6: ( (lv_ranges_13_0= ruleRDFSRange ) )
                    	    // InternalOWLModel.g:5476:1: (lv_ranges_13_0= ruleRDFSRange )
                    	    {
                    	    // InternalOWLModel.g:5476:1: (lv_ranges_13_0= ruleRDFSRange )
                    	    // InternalOWLModel.g:5477:3: lv_ranges_13_0= ruleRDFSRange
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getFunctionalPropertyAccess().getRangesRDFSRangeParserRuleCall_3_1_1_3_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_86);
                    	    lv_ranges_13_0=ruleRDFSRange();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getFunctionalPropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"ranges",
                    	            		lv_ranges_13_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.RDFSRange");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 5 :
                    	    // InternalOWLModel.g:5494:6: ( (lv_types_14_0= ruleRDFType ) )
                    	    {
                    	    // InternalOWLModel.g:5494:6: ( (lv_types_14_0= ruleRDFType ) )
                    	    // InternalOWLModel.g:5495:1: (lv_types_14_0= ruleRDFType )
                    	    {
                    	    // InternalOWLModel.g:5495:1: (lv_types_14_0= ruleRDFType )
                    	    // InternalOWLModel.g:5496:3: lv_types_14_0= ruleRDFType
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getFunctionalPropertyAccess().getTypesRDFTypeParserRuleCall_3_1_1_4_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_86);
                    	    lv_types_14_0=ruleRDFType();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getFunctionalPropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"types",
                    	            		lv_types_14_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.RDFType");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 6 :
                    	    // InternalOWLModel.g:5513:6: ( (lv_domains_15_0= ruleRDFSDomain ) )
                    	    {
                    	    // InternalOWLModel.g:5513:6: ( (lv_domains_15_0= ruleRDFSDomain ) )
                    	    // InternalOWLModel.g:5514:1: (lv_domains_15_0= ruleRDFSDomain )
                    	    {
                    	    // InternalOWLModel.g:5514:1: (lv_domains_15_0= ruleRDFSDomain )
                    	    // InternalOWLModel.g:5515:3: lv_domains_15_0= ruleRDFSDomain
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getFunctionalPropertyAccess().getDomainsRDFSDomainParserRuleCall_3_1_1_5_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_86);
                    	    lv_domains_15_0=ruleRDFSDomain();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getFunctionalPropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"domains",
                    	            		lv_domains_15_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.RDFSDomain");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 7 :
                    	    // InternalOWLModel.g:5532:6: ( (lv_modifiers_16_0= rulePropertyModifier ) )
                    	    {
                    	    // InternalOWLModel.g:5532:6: ( (lv_modifiers_16_0= rulePropertyModifier ) )
                    	    // InternalOWLModel.g:5533:1: (lv_modifiers_16_0= rulePropertyModifier )
                    	    {
                    	    // InternalOWLModel.g:5533:1: (lv_modifiers_16_0= rulePropertyModifier )
                    	    // InternalOWLModel.g:5534:3: lv_modifiers_16_0= rulePropertyModifier
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getFunctionalPropertyAccess().getModifiersPropertyModifierParserRuleCall_3_1_1_6_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_86);
                    	    lv_modifiers_16_0=rulePropertyModifier();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getFunctionalPropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"modifiers",
                    	            		lv_modifiers_16_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.PropertyModifier");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop102;
                        }
                    } while (true);

                    otherlv_17=(Token)match(input,101,FOLLOW_7); 

                        	newLeafNode(otherlv_17, grammarAccess.getFunctionalPropertyAccess().getOwlFunctionalPropertyKeyword_3_1_2());
                        
                    otherlv_18=(Token)match(input,21,FOLLOW_2); 

                        	newLeafNode(otherlv_18, grammarAccess.getFunctionalPropertyAccess().getGreaterThanSignKeyword_3_1_3());
                        

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFunctionalProperty"


    // $ANTLR start "entryRuleInverseFunctionalProperty"
    // InternalOWLModel.g:5566:1: entryRuleInverseFunctionalProperty returns [EObject current=null] : iv_ruleInverseFunctionalProperty= ruleInverseFunctionalProperty EOF ;
    public final EObject entryRuleInverseFunctionalProperty() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInverseFunctionalProperty = null;


        try {
            // InternalOWLModel.g:5567:2: (iv_ruleInverseFunctionalProperty= ruleInverseFunctionalProperty EOF )
            // InternalOWLModel.g:5568:2: iv_ruleInverseFunctionalProperty= ruleInverseFunctionalProperty EOF
            {
             newCompositeNode(grammarAccess.getInverseFunctionalPropertyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleInverseFunctionalProperty=ruleInverseFunctionalProperty();

            state._fsp--;

             current =iv_ruleInverseFunctionalProperty; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInverseFunctionalProperty"


    // $ANTLR start "ruleInverseFunctionalProperty"
    // InternalOWLModel.g:5575:1: ruleInverseFunctionalProperty returns [EObject current=null] : ( () otherlv_1= '<owl:InverseFunctionalProperty' ( (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) ) | ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) ) ) (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:InverseFunctionalProperty' otherlv_18= '>' ) ) ) ;
    public final EObject ruleInverseFunctionalProperty() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token lv_reference_5_0=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        EObject lv_id_4_0 = null;

        EObject lv_ref_7_0 = null;

        AntlrDatatypeRuleToken lv_others_10_0 = null;

        EObject lv_labels_11_0 = null;

        EObject lv_versionInfo_12_0 = null;

        EObject lv_ranges_13_0 = null;

        EObject lv_types_14_0 = null;

        EObject lv_domains_15_0 = null;

        EObject lv_modifiers_16_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:5578:28: ( ( () otherlv_1= '<owl:InverseFunctionalProperty' ( (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) ) | ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) ) ) (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:InverseFunctionalProperty' otherlv_18= '>' ) ) ) )
            // InternalOWLModel.g:5579:1: ( () otherlv_1= '<owl:InverseFunctionalProperty' ( (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) ) | ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) ) ) (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:InverseFunctionalProperty' otherlv_18= '>' ) ) )
            {
            // InternalOWLModel.g:5579:1: ( () otherlv_1= '<owl:InverseFunctionalProperty' ( (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) ) | ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) ) ) (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:InverseFunctionalProperty' otherlv_18= '>' ) ) )
            // InternalOWLModel.g:5579:2: () otherlv_1= '<owl:InverseFunctionalProperty' ( (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) ) | ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) ) ) (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:InverseFunctionalProperty' otherlv_18= '>' ) )
            {
            // InternalOWLModel.g:5579:2: ()
            // InternalOWLModel.g:5580:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getInverseFunctionalPropertyAccess().getInverseFunctionalPropertyAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,102,FOLLOW_34); 

                	newLeafNode(otherlv_1, grammarAccess.getInverseFunctionalPropertyAccess().getOwlInverseFunctionalPropertyKeyword_1());
                
            // InternalOWLModel.g:5589:1: ( (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) ) | ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) ) )
            int alt104=2;
            int LA104_0 = input.LA(1);

            if ( (LA104_0==40) ) {
                alt104=1;
            }
            else if ( (LA104_0==28) ) {
                alt104=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 104, 0, input);

                throw nvae;
            }
            switch (alt104) {
                case 1 :
                    // InternalOWLModel.g:5589:2: (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) )
                    {
                    // InternalOWLModel.g:5589:2: (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) )
                    // InternalOWLModel.g:5589:4: otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) )
                    {
                    otherlv_2=(Token)match(input,40,FOLLOW_13); 

                        	newLeafNode(otherlv_2, grammarAccess.getInverseFunctionalPropertyAccess().getRdfIDKeyword_2_0_0());
                        
                    otherlv_3=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_3, grammarAccess.getInverseFunctionalPropertyAccess().getEqualsSignKeyword_2_0_1());
                        
                    // InternalOWLModel.g:5597:1: ( (lv_id_4_0= ruleOwlID ) )
                    // InternalOWLModel.g:5598:1: (lv_id_4_0= ruleOwlID )
                    {
                    // InternalOWLModel.g:5598:1: (lv_id_4_0= ruleOwlID )
                    // InternalOWLModel.g:5599:3: lv_id_4_0= ruleOwlID
                    {
                     
                    	        newCompositeNode(grammarAccess.getInverseFunctionalPropertyAccess().getIdOwlIDParserRuleCall_2_0_2_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_id_4_0=ruleOwlID();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getInverseFunctionalPropertyRule());
                    	        }
                           		set(
                           			current, 
                           			"id",
                            		lv_id_4_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlID");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:5616:6: ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) )
                    {
                    // InternalOWLModel.g:5616:6: ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) )
                    // InternalOWLModel.g:5616:7: ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) )
                    {
                    // InternalOWLModel.g:5616:7: ( (lv_reference_5_0= 'rdf:about' ) )
                    // InternalOWLModel.g:5617:1: (lv_reference_5_0= 'rdf:about' )
                    {
                    // InternalOWLModel.g:5617:1: (lv_reference_5_0= 'rdf:about' )
                    // InternalOWLModel.g:5618:3: lv_reference_5_0= 'rdf:about'
                    {
                    lv_reference_5_0=(Token)match(input,28,FOLLOW_13); 

                            newLeafNode(lv_reference_5_0, grammarAccess.getInverseFunctionalPropertyAccess().getReferenceRdfAboutKeyword_2_1_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getInverseFunctionalPropertyRule());
                    	        }
                           		setWithLastConsumed(current, "reference", true, "rdf:about");
                    	    

                    }


                    }

                    otherlv_6=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_6, grammarAccess.getInverseFunctionalPropertyAccess().getEqualsSignKeyword_2_1_1());
                        
                    // InternalOWLModel.g:5635:1: ( (lv_ref_7_0= ruleOwlRef ) )
                    // InternalOWLModel.g:5636:1: (lv_ref_7_0= ruleOwlRef )
                    {
                    // InternalOWLModel.g:5636:1: (lv_ref_7_0= ruleOwlRef )
                    // InternalOWLModel.g:5637:3: lv_ref_7_0= ruleOwlRef
                    {
                     
                    	        newCompositeNode(grammarAccess.getInverseFunctionalPropertyAccess().getRefOwlRefParserRuleCall_2_1_2_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_ref_7_0=ruleOwlRef();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getInverseFunctionalPropertyRule());
                    	        }
                           		set(
                           			current, 
                           			"ref",
                            		lv_ref_7_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlRef");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;

            }

            // InternalOWLModel.g:5653:4: (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:InverseFunctionalProperty' otherlv_18= '>' ) )
            int alt106=2;
            int LA106_0 = input.LA(1);

            if ( (LA106_0==29) ) {
                alt106=1;
            }
            else if ( (LA106_0==21) ) {
                alt106=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 106, 0, input);

                throw nvae;
            }
            switch (alt106) {
                case 1 :
                    // InternalOWLModel.g:5653:6: otherlv_8= '/>'
                    {
                    otherlv_8=(Token)match(input,29,FOLLOW_2); 

                        	newLeafNode(otherlv_8, grammarAccess.getInverseFunctionalPropertyAccess().getSolidusGreaterThanSignKeyword_3_0());
                        

                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:5658:6: (otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:InverseFunctionalProperty' otherlv_18= '>' )
                    {
                    // InternalOWLModel.g:5658:6: (otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:InverseFunctionalProperty' otherlv_18= '>' )
                    // InternalOWLModel.g:5658:8: otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:InverseFunctionalProperty' otherlv_18= '>'
                    {
                    otherlv_9=(Token)match(input,21,FOLLOW_87); 

                        	newLeafNode(otherlv_9, grammarAccess.getInverseFunctionalPropertyAccess().getGreaterThanSignKeyword_3_1_0());
                        
                    // InternalOWLModel.g:5662:1: ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )*
                    loop105:
                    do {
                        int alt105=8;
                        switch ( input.LA(1) ) {
                        case 58:
                            {
                            alt105=1;
                            }
                            break;
                        case 60:
                            {
                            alt105=2;
                            }
                            break;
                        case 64:
                            {
                            alt105=3;
                            }
                            break;
                        case 114:
                            {
                            alt105=4;
                            }
                            break;
                        case 118:
                            {
                            alt105=5;
                            }
                            break;
                        case 112:
                            {
                            alt105=6;
                            }
                            break;
                        case 108:
                        case 110:
                        case 120:
                            {
                            alt105=7;
                            }
                            break;

                        }

                        switch (alt105) {
                    	case 1 :
                    	    // InternalOWLModel.g:5662:2: ( (lv_others_10_0= ruleANY_OTHER_TAG ) )
                    	    {
                    	    // InternalOWLModel.g:5662:2: ( (lv_others_10_0= ruleANY_OTHER_TAG ) )
                    	    // InternalOWLModel.g:5663:1: (lv_others_10_0= ruleANY_OTHER_TAG )
                    	    {
                    	    // InternalOWLModel.g:5663:1: (lv_others_10_0= ruleANY_OTHER_TAG )
                    	    // InternalOWLModel.g:5664:3: lv_others_10_0= ruleANY_OTHER_TAG
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getInverseFunctionalPropertyAccess().getOthersANY_OTHER_TAGParserRuleCall_3_1_1_0_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_87);
                    	    lv_others_10_0=ruleANY_OTHER_TAG();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getInverseFunctionalPropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"others",
                    	            		lv_others_10_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.ANY_OTHER_TAG");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalOWLModel.g:5681:6: ( (lv_labels_11_0= ruleOwlLabel ) )
                    	    {
                    	    // InternalOWLModel.g:5681:6: ( (lv_labels_11_0= ruleOwlLabel ) )
                    	    // InternalOWLModel.g:5682:1: (lv_labels_11_0= ruleOwlLabel )
                    	    {
                    	    // InternalOWLModel.g:5682:1: (lv_labels_11_0= ruleOwlLabel )
                    	    // InternalOWLModel.g:5683:3: lv_labels_11_0= ruleOwlLabel
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getInverseFunctionalPropertyAccess().getLabelsOwlLabelParserRuleCall_3_1_1_1_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_87);
                    	    lv_labels_11_0=ruleOwlLabel();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getInverseFunctionalPropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"labels",
                    	            		lv_labels_11_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.OwlLabel");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 3 :
                    	    // InternalOWLModel.g:5700:6: ( (lv_versionInfo_12_0= ruleOwlVersion ) )
                    	    {
                    	    // InternalOWLModel.g:5700:6: ( (lv_versionInfo_12_0= ruleOwlVersion ) )
                    	    // InternalOWLModel.g:5701:1: (lv_versionInfo_12_0= ruleOwlVersion )
                    	    {
                    	    // InternalOWLModel.g:5701:1: (lv_versionInfo_12_0= ruleOwlVersion )
                    	    // InternalOWLModel.g:5702:3: lv_versionInfo_12_0= ruleOwlVersion
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getInverseFunctionalPropertyAccess().getVersionInfoOwlVersionParserRuleCall_3_1_1_2_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_87);
                    	    lv_versionInfo_12_0=ruleOwlVersion();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getInverseFunctionalPropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"versionInfo",
                    	            		lv_versionInfo_12_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.OwlVersion");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 4 :
                    	    // InternalOWLModel.g:5719:6: ( (lv_ranges_13_0= ruleRDFSRange ) )
                    	    {
                    	    // InternalOWLModel.g:5719:6: ( (lv_ranges_13_0= ruleRDFSRange ) )
                    	    // InternalOWLModel.g:5720:1: (lv_ranges_13_0= ruleRDFSRange )
                    	    {
                    	    // InternalOWLModel.g:5720:1: (lv_ranges_13_0= ruleRDFSRange )
                    	    // InternalOWLModel.g:5721:3: lv_ranges_13_0= ruleRDFSRange
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getInverseFunctionalPropertyAccess().getRangesRDFSRangeParserRuleCall_3_1_1_3_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_87);
                    	    lv_ranges_13_0=ruleRDFSRange();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getInverseFunctionalPropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"ranges",
                    	            		lv_ranges_13_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.RDFSRange");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 5 :
                    	    // InternalOWLModel.g:5738:6: ( (lv_types_14_0= ruleRDFType ) )
                    	    {
                    	    // InternalOWLModel.g:5738:6: ( (lv_types_14_0= ruleRDFType ) )
                    	    // InternalOWLModel.g:5739:1: (lv_types_14_0= ruleRDFType )
                    	    {
                    	    // InternalOWLModel.g:5739:1: (lv_types_14_0= ruleRDFType )
                    	    // InternalOWLModel.g:5740:3: lv_types_14_0= ruleRDFType
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getInverseFunctionalPropertyAccess().getTypesRDFTypeParserRuleCall_3_1_1_4_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_87);
                    	    lv_types_14_0=ruleRDFType();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getInverseFunctionalPropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"types",
                    	            		lv_types_14_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.RDFType");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 6 :
                    	    // InternalOWLModel.g:5757:6: ( (lv_domains_15_0= ruleRDFSDomain ) )
                    	    {
                    	    // InternalOWLModel.g:5757:6: ( (lv_domains_15_0= ruleRDFSDomain ) )
                    	    // InternalOWLModel.g:5758:1: (lv_domains_15_0= ruleRDFSDomain )
                    	    {
                    	    // InternalOWLModel.g:5758:1: (lv_domains_15_0= ruleRDFSDomain )
                    	    // InternalOWLModel.g:5759:3: lv_domains_15_0= ruleRDFSDomain
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getInverseFunctionalPropertyAccess().getDomainsRDFSDomainParserRuleCall_3_1_1_5_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_87);
                    	    lv_domains_15_0=ruleRDFSDomain();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getInverseFunctionalPropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"domains",
                    	            		lv_domains_15_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.RDFSDomain");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 7 :
                    	    // InternalOWLModel.g:5776:6: ( (lv_modifiers_16_0= rulePropertyModifier ) )
                    	    {
                    	    // InternalOWLModel.g:5776:6: ( (lv_modifiers_16_0= rulePropertyModifier ) )
                    	    // InternalOWLModel.g:5777:1: (lv_modifiers_16_0= rulePropertyModifier )
                    	    {
                    	    // InternalOWLModel.g:5777:1: (lv_modifiers_16_0= rulePropertyModifier )
                    	    // InternalOWLModel.g:5778:3: lv_modifiers_16_0= rulePropertyModifier
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getInverseFunctionalPropertyAccess().getModifiersPropertyModifierParserRuleCall_3_1_1_6_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_87);
                    	    lv_modifiers_16_0=rulePropertyModifier();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getInverseFunctionalPropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"modifiers",
                    	            		lv_modifiers_16_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.PropertyModifier");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop105;
                        }
                    } while (true);

                    otherlv_17=(Token)match(input,103,FOLLOW_7); 

                        	newLeafNode(otherlv_17, grammarAccess.getInverseFunctionalPropertyAccess().getOwlInverseFunctionalPropertyKeyword_3_1_2());
                        
                    otherlv_18=(Token)match(input,21,FOLLOW_2); 

                        	newLeafNode(otherlv_18, grammarAccess.getInverseFunctionalPropertyAccess().getGreaterThanSignKeyword_3_1_3());
                        

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInverseFunctionalProperty"


    // $ANTLR start "entryRuleSymmetricProperty"
    // InternalOWLModel.g:5810:1: entryRuleSymmetricProperty returns [EObject current=null] : iv_ruleSymmetricProperty= ruleSymmetricProperty EOF ;
    public final EObject entryRuleSymmetricProperty() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSymmetricProperty = null;


        try {
            // InternalOWLModel.g:5811:2: (iv_ruleSymmetricProperty= ruleSymmetricProperty EOF )
            // InternalOWLModel.g:5812:2: iv_ruleSymmetricProperty= ruleSymmetricProperty EOF
            {
             newCompositeNode(grammarAccess.getSymmetricPropertyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSymmetricProperty=ruleSymmetricProperty();

            state._fsp--;

             current =iv_ruleSymmetricProperty; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSymmetricProperty"


    // $ANTLR start "ruleSymmetricProperty"
    // InternalOWLModel.g:5819:1: ruleSymmetricProperty returns [EObject current=null] : ( () otherlv_1= '<owl:SymmetricProperty' ( (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) ) | ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) ) ) (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:SymmetricProperty' otherlv_18= '>' ) ) ) ;
    public final EObject ruleSymmetricProperty() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token lv_reference_5_0=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        EObject lv_id_4_0 = null;

        EObject lv_ref_7_0 = null;

        AntlrDatatypeRuleToken lv_others_10_0 = null;

        EObject lv_labels_11_0 = null;

        EObject lv_versionInfo_12_0 = null;

        EObject lv_ranges_13_0 = null;

        EObject lv_types_14_0 = null;

        EObject lv_domains_15_0 = null;

        EObject lv_modifiers_16_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:5822:28: ( ( () otherlv_1= '<owl:SymmetricProperty' ( (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) ) | ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) ) ) (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:SymmetricProperty' otherlv_18= '>' ) ) ) )
            // InternalOWLModel.g:5823:1: ( () otherlv_1= '<owl:SymmetricProperty' ( (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) ) | ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) ) ) (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:SymmetricProperty' otherlv_18= '>' ) ) )
            {
            // InternalOWLModel.g:5823:1: ( () otherlv_1= '<owl:SymmetricProperty' ( (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) ) | ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) ) ) (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:SymmetricProperty' otherlv_18= '>' ) ) )
            // InternalOWLModel.g:5823:2: () otherlv_1= '<owl:SymmetricProperty' ( (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) ) | ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) ) ) (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:SymmetricProperty' otherlv_18= '>' ) )
            {
            // InternalOWLModel.g:5823:2: ()
            // InternalOWLModel.g:5824:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getSymmetricPropertyAccess().getSymmetricPropertyAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,104,FOLLOW_34); 

                	newLeafNode(otherlv_1, grammarAccess.getSymmetricPropertyAccess().getOwlSymmetricPropertyKeyword_1());
                
            // InternalOWLModel.g:5833:1: ( (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) ) | ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) ) )
            int alt107=2;
            int LA107_0 = input.LA(1);

            if ( (LA107_0==40) ) {
                alt107=1;
            }
            else if ( (LA107_0==28) ) {
                alt107=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 107, 0, input);

                throw nvae;
            }
            switch (alt107) {
                case 1 :
                    // InternalOWLModel.g:5833:2: (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) )
                    {
                    // InternalOWLModel.g:5833:2: (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) )
                    // InternalOWLModel.g:5833:4: otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) )
                    {
                    otherlv_2=(Token)match(input,40,FOLLOW_13); 

                        	newLeafNode(otherlv_2, grammarAccess.getSymmetricPropertyAccess().getRdfIDKeyword_2_0_0());
                        
                    otherlv_3=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_3, grammarAccess.getSymmetricPropertyAccess().getEqualsSignKeyword_2_0_1());
                        
                    // InternalOWLModel.g:5841:1: ( (lv_id_4_0= ruleOwlID ) )
                    // InternalOWLModel.g:5842:1: (lv_id_4_0= ruleOwlID )
                    {
                    // InternalOWLModel.g:5842:1: (lv_id_4_0= ruleOwlID )
                    // InternalOWLModel.g:5843:3: lv_id_4_0= ruleOwlID
                    {
                     
                    	        newCompositeNode(grammarAccess.getSymmetricPropertyAccess().getIdOwlIDParserRuleCall_2_0_2_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_id_4_0=ruleOwlID();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getSymmetricPropertyRule());
                    	        }
                           		set(
                           			current, 
                           			"id",
                            		lv_id_4_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlID");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:5860:6: ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) )
                    {
                    // InternalOWLModel.g:5860:6: ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) )
                    // InternalOWLModel.g:5860:7: ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) )
                    {
                    // InternalOWLModel.g:5860:7: ( (lv_reference_5_0= 'rdf:about' ) )
                    // InternalOWLModel.g:5861:1: (lv_reference_5_0= 'rdf:about' )
                    {
                    // InternalOWLModel.g:5861:1: (lv_reference_5_0= 'rdf:about' )
                    // InternalOWLModel.g:5862:3: lv_reference_5_0= 'rdf:about'
                    {
                    lv_reference_5_0=(Token)match(input,28,FOLLOW_13); 

                            newLeafNode(lv_reference_5_0, grammarAccess.getSymmetricPropertyAccess().getReferenceRdfAboutKeyword_2_1_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getSymmetricPropertyRule());
                    	        }
                           		setWithLastConsumed(current, "reference", true, "rdf:about");
                    	    

                    }


                    }

                    otherlv_6=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_6, grammarAccess.getSymmetricPropertyAccess().getEqualsSignKeyword_2_1_1());
                        
                    // InternalOWLModel.g:5879:1: ( (lv_ref_7_0= ruleOwlRef ) )
                    // InternalOWLModel.g:5880:1: (lv_ref_7_0= ruleOwlRef )
                    {
                    // InternalOWLModel.g:5880:1: (lv_ref_7_0= ruleOwlRef )
                    // InternalOWLModel.g:5881:3: lv_ref_7_0= ruleOwlRef
                    {
                     
                    	        newCompositeNode(grammarAccess.getSymmetricPropertyAccess().getRefOwlRefParserRuleCall_2_1_2_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_ref_7_0=ruleOwlRef();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getSymmetricPropertyRule());
                    	        }
                           		set(
                           			current, 
                           			"ref",
                            		lv_ref_7_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlRef");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;

            }

            // InternalOWLModel.g:5897:4: (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:SymmetricProperty' otherlv_18= '>' ) )
            int alt109=2;
            int LA109_0 = input.LA(1);

            if ( (LA109_0==29) ) {
                alt109=1;
            }
            else if ( (LA109_0==21) ) {
                alt109=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 109, 0, input);

                throw nvae;
            }
            switch (alt109) {
                case 1 :
                    // InternalOWLModel.g:5897:6: otherlv_8= '/>'
                    {
                    otherlv_8=(Token)match(input,29,FOLLOW_2); 

                        	newLeafNode(otherlv_8, grammarAccess.getSymmetricPropertyAccess().getSolidusGreaterThanSignKeyword_3_0());
                        

                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:5902:6: (otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:SymmetricProperty' otherlv_18= '>' )
                    {
                    // InternalOWLModel.g:5902:6: (otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:SymmetricProperty' otherlv_18= '>' )
                    // InternalOWLModel.g:5902:8: otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:SymmetricProperty' otherlv_18= '>'
                    {
                    otherlv_9=(Token)match(input,21,FOLLOW_88); 

                        	newLeafNode(otherlv_9, grammarAccess.getSymmetricPropertyAccess().getGreaterThanSignKeyword_3_1_0());
                        
                    // InternalOWLModel.g:5906:1: ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )*
                    loop108:
                    do {
                        int alt108=8;
                        switch ( input.LA(1) ) {
                        case 58:
                            {
                            alt108=1;
                            }
                            break;
                        case 60:
                            {
                            alt108=2;
                            }
                            break;
                        case 64:
                            {
                            alt108=3;
                            }
                            break;
                        case 114:
                            {
                            alt108=4;
                            }
                            break;
                        case 118:
                            {
                            alt108=5;
                            }
                            break;
                        case 112:
                            {
                            alt108=6;
                            }
                            break;
                        case 108:
                        case 110:
                        case 120:
                            {
                            alt108=7;
                            }
                            break;

                        }

                        switch (alt108) {
                    	case 1 :
                    	    // InternalOWLModel.g:5906:2: ( (lv_others_10_0= ruleANY_OTHER_TAG ) )
                    	    {
                    	    // InternalOWLModel.g:5906:2: ( (lv_others_10_0= ruleANY_OTHER_TAG ) )
                    	    // InternalOWLModel.g:5907:1: (lv_others_10_0= ruleANY_OTHER_TAG )
                    	    {
                    	    // InternalOWLModel.g:5907:1: (lv_others_10_0= ruleANY_OTHER_TAG )
                    	    // InternalOWLModel.g:5908:3: lv_others_10_0= ruleANY_OTHER_TAG
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getSymmetricPropertyAccess().getOthersANY_OTHER_TAGParserRuleCall_3_1_1_0_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_88);
                    	    lv_others_10_0=ruleANY_OTHER_TAG();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getSymmetricPropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"others",
                    	            		lv_others_10_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.ANY_OTHER_TAG");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalOWLModel.g:5925:6: ( (lv_labels_11_0= ruleOwlLabel ) )
                    	    {
                    	    // InternalOWLModel.g:5925:6: ( (lv_labels_11_0= ruleOwlLabel ) )
                    	    // InternalOWLModel.g:5926:1: (lv_labels_11_0= ruleOwlLabel )
                    	    {
                    	    // InternalOWLModel.g:5926:1: (lv_labels_11_0= ruleOwlLabel )
                    	    // InternalOWLModel.g:5927:3: lv_labels_11_0= ruleOwlLabel
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getSymmetricPropertyAccess().getLabelsOwlLabelParserRuleCall_3_1_1_1_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_88);
                    	    lv_labels_11_0=ruleOwlLabel();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getSymmetricPropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"labels",
                    	            		lv_labels_11_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.OwlLabel");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 3 :
                    	    // InternalOWLModel.g:5944:6: ( (lv_versionInfo_12_0= ruleOwlVersion ) )
                    	    {
                    	    // InternalOWLModel.g:5944:6: ( (lv_versionInfo_12_0= ruleOwlVersion ) )
                    	    // InternalOWLModel.g:5945:1: (lv_versionInfo_12_0= ruleOwlVersion )
                    	    {
                    	    // InternalOWLModel.g:5945:1: (lv_versionInfo_12_0= ruleOwlVersion )
                    	    // InternalOWLModel.g:5946:3: lv_versionInfo_12_0= ruleOwlVersion
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getSymmetricPropertyAccess().getVersionInfoOwlVersionParserRuleCall_3_1_1_2_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_88);
                    	    lv_versionInfo_12_0=ruleOwlVersion();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getSymmetricPropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"versionInfo",
                    	            		lv_versionInfo_12_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.OwlVersion");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 4 :
                    	    // InternalOWLModel.g:5963:6: ( (lv_ranges_13_0= ruleRDFSRange ) )
                    	    {
                    	    // InternalOWLModel.g:5963:6: ( (lv_ranges_13_0= ruleRDFSRange ) )
                    	    // InternalOWLModel.g:5964:1: (lv_ranges_13_0= ruleRDFSRange )
                    	    {
                    	    // InternalOWLModel.g:5964:1: (lv_ranges_13_0= ruleRDFSRange )
                    	    // InternalOWLModel.g:5965:3: lv_ranges_13_0= ruleRDFSRange
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getSymmetricPropertyAccess().getRangesRDFSRangeParserRuleCall_3_1_1_3_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_88);
                    	    lv_ranges_13_0=ruleRDFSRange();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getSymmetricPropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"ranges",
                    	            		lv_ranges_13_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.RDFSRange");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 5 :
                    	    // InternalOWLModel.g:5982:6: ( (lv_types_14_0= ruleRDFType ) )
                    	    {
                    	    // InternalOWLModel.g:5982:6: ( (lv_types_14_0= ruleRDFType ) )
                    	    // InternalOWLModel.g:5983:1: (lv_types_14_0= ruleRDFType )
                    	    {
                    	    // InternalOWLModel.g:5983:1: (lv_types_14_0= ruleRDFType )
                    	    // InternalOWLModel.g:5984:3: lv_types_14_0= ruleRDFType
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getSymmetricPropertyAccess().getTypesRDFTypeParserRuleCall_3_1_1_4_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_88);
                    	    lv_types_14_0=ruleRDFType();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getSymmetricPropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"types",
                    	            		lv_types_14_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.RDFType");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 6 :
                    	    // InternalOWLModel.g:6001:6: ( (lv_domains_15_0= ruleRDFSDomain ) )
                    	    {
                    	    // InternalOWLModel.g:6001:6: ( (lv_domains_15_0= ruleRDFSDomain ) )
                    	    // InternalOWLModel.g:6002:1: (lv_domains_15_0= ruleRDFSDomain )
                    	    {
                    	    // InternalOWLModel.g:6002:1: (lv_domains_15_0= ruleRDFSDomain )
                    	    // InternalOWLModel.g:6003:3: lv_domains_15_0= ruleRDFSDomain
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getSymmetricPropertyAccess().getDomainsRDFSDomainParserRuleCall_3_1_1_5_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_88);
                    	    lv_domains_15_0=ruleRDFSDomain();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getSymmetricPropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"domains",
                    	            		lv_domains_15_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.RDFSDomain");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 7 :
                    	    // InternalOWLModel.g:6020:6: ( (lv_modifiers_16_0= rulePropertyModifier ) )
                    	    {
                    	    // InternalOWLModel.g:6020:6: ( (lv_modifiers_16_0= rulePropertyModifier ) )
                    	    // InternalOWLModel.g:6021:1: (lv_modifiers_16_0= rulePropertyModifier )
                    	    {
                    	    // InternalOWLModel.g:6021:1: (lv_modifiers_16_0= rulePropertyModifier )
                    	    // InternalOWLModel.g:6022:3: lv_modifiers_16_0= rulePropertyModifier
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getSymmetricPropertyAccess().getModifiersPropertyModifierParserRuleCall_3_1_1_6_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_88);
                    	    lv_modifiers_16_0=rulePropertyModifier();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getSymmetricPropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"modifiers",
                    	            		lv_modifiers_16_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.PropertyModifier");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop108;
                        }
                    } while (true);

                    otherlv_17=(Token)match(input,105,FOLLOW_7); 

                        	newLeafNode(otherlv_17, grammarAccess.getSymmetricPropertyAccess().getOwlSymmetricPropertyKeyword_3_1_2());
                        
                    otherlv_18=(Token)match(input,21,FOLLOW_2); 

                        	newLeafNode(otherlv_18, grammarAccess.getSymmetricPropertyAccess().getGreaterThanSignKeyword_3_1_3());
                        

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSymmetricProperty"


    // $ANTLR start "entryRuleTransitiveProperty"
    // InternalOWLModel.g:6054:1: entryRuleTransitiveProperty returns [EObject current=null] : iv_ruleTransitiveProperty= ruleTransitiveProperty EOF ;
    public final EObject entryRuleTransitiveProperty() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTransitiveProperty = null;


        try {
            // InternalOWLModel.g:6055:2: (iv_ruleTransitiveProperty= ruleTransitiveProperty EOF )
            // InternalOWLModel.g:6056:2: iv_ruleTransitiveProperty= ruleTransitiveProperty EOF
            {
             newCompositeNode(grammarAccess.getTransitivePropertyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTransitiveProperty=ruleTransitiveProperty();

            state._fsp--;

             current =iv_ruleTransitiveProperty; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTransitiveProperty"


    // $ANTLR start "ruleTransitiveProperty"
    // InternalOWLModel.g:6063:1: ruleTransitiveProperty returns [EObject current=null] : ( () otherlv_1= '<owl:TransitiveProperty' ( (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) ) | ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) ) ) (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:TransitiveProperty' otherlv_18= '>' ) ) ) ;
    public final EObject ruleTransitiveProperty() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token lv_reference_5_0=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        EObject lv_id_4_0 = null;

        EObject lv_ref_7_0 = null;

        AntlrDatatypeRuleToken lv_others_10_0 = null;

        EObject lv_labels_11_0 = null;

        EObject lv_versionInfo_12_0 = null;

        EObject lv_ranges_13_0 = null;

        EObject lv_types_14_0 = null;

        EObject lv_domains_15_0 = null;

        EObject lv_modifiers_16_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:6066:28: ( ( () otherlv_1= '<owl:TransitiveProperty' ( (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) ) | ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) ) ) (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:TransitiveProperty' otherlv_18= '>' ) ) ) )
            // InternalOWLModel.g:6067:1: ( () otherlv_1= '<owl:TransitiveProperty' ( (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) ) | ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) ) ) (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:TransitiveProperty' otherlv_18= '>' ) ) )
            {
            // InternalOWLModel.g:6067:1: ( () otherlv_1= '<owl:TransitiveProperty' ( (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) ) | ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) ) ) (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:TransitiveProperty' otherlv_18= '>' ) ) )
            // InternalOWLModel.g:6067:2: () otherlv_1= '<owl:TransitiveProperty' ( (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) ) | ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) ) ) (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:TransitiveProperty' otherlv_18= '>' ) )
            {
            // InternalOWLModel.g:6067:2: ()
            // InternalOWLModel.g:6068:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getTransitivePropertyAccess().getTransitivePropertyAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,106,FOLLOW_34); 

                	newLeafNode(otherlv_1, grammarAccess.getTransitivePropertyAccess().getOwlTransitivePropertyKeyword_1());
                
            // InternalOWLModel.g:6077:1: ( (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) ) | ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) ) )
            int alt110=2;
            int LA110_0 = input.LA(1);

            if ( (LA110_0==40) ) {
                alt110=1;
            }
            else if ( (LA110_0==28) ) {
                alt110=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 110, 0, input);

                throw nvae;
            }
            switch (alt110) {
                case 1 :
                    // InternalOWLModel.g:6077:2: (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) )
                    {
                    // InternalOWLModel.g:6077:2: (otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) ) )
                    // InternalOWLModel.g:6077:4: otherlv_2= 'rdf:ID' otherlv_3= '=' ( (lv_id_4_0= ruleOwlID ) )
                    {
                    otherlv_2=(Token)match(input,40,FOLLOW_13); 

                        	newLeafNode(otherlv_2, grammarAccess.getTransitivePropertyAccess().getRdfIDKeyword_2_0_0());
                        
                    otherlv_3=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_3, grammarAccess.getTransitivePropertyAccess().getEqualsSignKeyword_2_0_1());
                        
                    // InternalOWLModel.g:6085:1: ( (lv_id_4_0= ruleOwlID ) )
                    // InternalOWLModel.g:6086:1: (lv_id_4_0= ruleOwlID )
                    {
                    // InternalOWLModel.g:6086:1: (lv_id_4_0= ruleOwlID )
                    // InternalOWLModel.g:6087:3: lv_id_4_0= ruleOwlID
                    {
                     
                    	        newCompositeNode(grammarAccess.getTransitivePropertyAccess().getIdOwlIDParserRuleCall_2_0_2_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_id_4_0=ruleOwlID();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getTransitivePropertyRule());
                    	        }
                           		set(
                           			current, 
                           			"id",
                            		lv_id_4_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlID");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:6104:6: ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) )
                    {
                    // InternalOWLModel.g:6104:6: ( ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) )
                    // InternalOWLModel.g:6104:7: ( (lv_reference_5_0= 'rdf:about' ) ) otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) )
                    {
                    // InternalOWLModel.g:6104:7: ( (lv_reference_5_0= 'rdf:about' ) )
                    // InternalOWLModel.g:6105:1: (lv_reference_5_0= 'rdf:about' )
                    {
                    // InternalOWLModel.g:6105:1: (lv_reference_5_0= 'rdf:about' )
                    // InternalOWLModel.g:6106:3: lv_reference_5_0= 'rdf:about'
                    {
                    lv_reference_5_0=(Token)match(input,28,FOLLOW_13); 

                            newLeafNode(lv_reference_5_0, grammarAccess.getTransitivePropertyAccess().getReferenceRdfAboutKeyword_2_1_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getTransitivePropertyRule());
                    	        }
                           		setWithLastConsumed(current, "reference", true, "rdf:about");
                    	    

                    }


                    }

                    otherlv_6=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_6, grammarAccess.getTransitivePropertyAccess().getEqualsSignKeyword_2_1_1());
                        
                    // InternalOWLModel.g:6123:1: ( (lv_ref_7_0= ruleOwlRef ) )
                    // InternalOWLModel.g:6124:1: (lv_ref_7_0= ruleOwlRef )
                    {
                    // InternalOWLModel.g:6124:1: (lv_ref_7_0= ruleOwlRef )
                    // InternalOWLModel.g:6125:3: lv_ref_7_0= ruleOwlRef
                    {
                     
                    	        newCompositeNode(grammarAccess.getTransitivePropertyAccess().getRefOwlRefParserRuleCall_2_1_2_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_ref_7_0=ruleOwlRef();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getTransitivePropertyRule());
                    	        }
                           		set(
                           			current, 
                           			"ref",
                            		lv_ref_7_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlRef");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;

            }

            // InternalOWLModel.g:6141:4: (otherlv_8= '/>' | (otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:TransitiveProperty' otherlv_18= '>' ) )
            int alt112=2;
            int LA112_0 = input.LA(1);

            if ( (LA112_0==29) ) {
                alt112=1;
            }
            else if ( (LA112_0==21) ) {
                alt112=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 112, 0, input);

                throw nvae;
            }
            switch (alt112) {
                case 1 :
                    // InternalOWLModel.g:6141:6: otherlv_8= '/>'
                    {
                    otherlv_8=(Token)match(input,29,FOLLOW_2); 

                        	newLeafNode(otherlv_8, grammarAccess.getTransitivePropertyAccess().getSolidusGreaterThanSignKeyword_3_0());
                        

                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:6146:6: (otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:TransitiveProperty' otherlv_18= '>' )
                    {
                    // InternalOWLModel.g:6146:6: (otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:TransitiveProperty' otherlv_18= '>' )
                    // InternalOWLModel.g:6146:8: otherlv_9= '>' ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )* otherlv_17= '</owl:TransitiveProperty' otherlv_18= '>'
                    {
                    otherlv_9=(Token)match(input,21,FOLLOW_89); 

                        	newLeafNode(otherlv_9, grammarAccess.getTransitivePropertyAccess().getGreaterThanSignKeyword_3_1_0());
                        
                    // InternalOWLModel.g:6150:1: ( ( (lv_others_10_0= ruleANY_OTHER_TAG ) ) | ( (lv_labels_11_0= ruleOwlLabel ) ) | ( (lv_versionInfo_12_0= ruleOwlVersion ) ) | ( (lv_ranges_13_0= ruleRDFSRange ) ) | ( (lv_types_14_0= ruleRDFType ) ) | ( (lv_domains_15_0= ruleRDFSDomain ) ) | ( (lv_modifiers_16_0= rulePropertyModifier ) ) )*
                    loop111:
                    do {
                        int alt111=8;
                        switch ( input.LA(1) ) {
                        case 58:
                            {
                            alt111=1;
                            }
                            break;
                        case 60:
                            {
                            alt111=2;
                            }
                            break;
                        case 64:
                            {
                            alt111=3;
                            }
                            break;
                        case 114:
                            {
                            alt111=4;
                            }
                            break;
                        case 118:
                            {
                            alt111=5;
                            }
                            break;
                        case 112:
                            {
                            alt111=6;
                            }
                            break;
                        case 108:
                        case 110:
                        case 120:
                            {
                            alt111=7;
                            }
                            break;

                        }

                        switch (alt111) {
                    	case 1 :
                    	    // InternalOWLModel.g:6150:2: ( (lv_others_10_0= ruleANY_OTHER_TAG ) )
                    	    {
                    	    // InternalOWLModel.g:6150:2: ( (lv_others_10_0= ruleANY_OTHER_TAG ) )
                    	    // InternalOWLModel.g:6151:1: (lv_others_10_0= ruleANY_OTHER_TAG )
                    	    {
                    	    // InternalOWLModel.g:6151:1: (lv_others_10_0= ruleANY_OTHER_TAG )
                    	    // InternalOWLModel.g:6152:3: lv_others_10_0= ruleANY_OTHER_TAG
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getTransitivePropertyAccess().getOthersANY_OTHER_TAGParserRuleCall_3_1_1_0_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_89);
                    	    lv_others_10_0=ruleANY_OTHER_TAG();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getTransitivePropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"others",
                    	            		lv_others_10_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.ANY_OTHER_TAG");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalOWLModel.g:6169:6: ( (lv_labels_11_0= ruleOwlLabel ) )
                    	    {
                    	    // InternalOWLModel.g:6169:6: ( (lv_labels_11_0= ruleOwlLabel ) )
                    	    // InternalOWLModel.g:6170:1: (lv_labels_11_0= ruleOwlLabel )
                    	    {
                    	    // InternalOWLModel.g:6170:1: (lv_labels_11_0= ruleOwlLabel )
                    	    // InternalOWLModel.g:6171:3: lv_labels_11_0= ruleOwlLabel
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getTransitivePropertyAccess().getLabelsOwlLabelParserRuleCall_3_1_1_1_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_89);
                    	    lv_labels_11_0=ruleOwlLabel();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getTransitivePropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"labels",
                    	            		lv_labels_11_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.OwlLabel");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 3 :
                    	    // InternalOWLModel.g:6188:6: ( (lv_versionInfo_12_0= ruleOwlVersion ) )
                    	    {
                    	    // InternalOWLModel.g:6188:6: ( (lv_versionInfo_12_0= ruleOwlVersion ) )
                    	    // InternalOWLModel.g:6189:1: (lv_versionInfo_12_0= ruleOwlVersion )
                    	    {
                    	    // InternalOWLModel.g:6189:1: (lv_versionInfo_12_0= ruleOwlVersion )
                    	    // InternalOWLModel.g:6190:3: lv_versionInfo_12_0= ruleOwlVersion
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getTransitivePropertyAccess().getVersionInfoOwlVersionParserRuleCall_3_1_1_2_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_89);
                    	    lv_versionInfo_12_0=ruleOwlVersion();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getTransitivePropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"versionInfo",
                    	            		lv_versionInfo_12_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.OwlVersion");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 4 :
                    	    // InternalOWLModel.g:6207:6: ( (lv_ranges_13_0= ruleRDFSRange ) )
                    	    {
                    	    // InternalOWLModel.g:6207:6: ( (lv_ranges_13_0= ruleRDFSRange ) )
                    	    // InternalOWLModel.g:6208:1: (lv_ranges_13_0= ruleRDFSRange )
                    	    {
                    	    // InternalOWLModel.g:6208:1: (lv_ranges_13_0= ruleRDFSRange )
                    	    // InternalOWLModel.g:6209:3: lv_ranges_13_0= ruleRDFSRange
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getTransitivePropertyAccess().getRangesRDFSRangeParserRuleCall_3_1_1_3_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_89);
                    	    lv_ranges_13_0=ruleRDFSRange();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getTransitivePropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"ranges",
                    	            		lv_ranges_13_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.RDFSRange");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 5 :
                    	    // InternalOWLModel.g:6226:6: ( (lv_types_14_0= ruleRDFType ) )
                    	    {
                    	    // InternalOWLModel.g:6226:6: ( (lv_types_14_0= ruleRDFType ) )
                    	    // InternalOWLModel.g:6227:1: (lv_types_14_0= ruleRDFType )
                    	    {
                    	    // InternalOWLModel.g:6227:1: (lv_types_14_0= ruleRDFType )
                    	    // InternalOWLModel.g:6228:3: lv_types_14_0= ruleRDFType
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getTransitivePropertyAccess().getTypesRDFTypeParserRuleCall_3_1_1_4_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_89);
                    	    lv_types_14_0=ruleRDFType();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getTransitivePropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"types",
                    	            		lv_types_14_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.RDFType");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 6 :
                    	    // InternalOWLModel.g:6245:6: ( (lv_domains_15_0= ruleRDFSDomain ) )
                    	    {
                    	    // InternalOWLModel.g:6245:6: ( (lv_domains_15_0= ruleRDFSDomain ) )
                    	    // InternalOWLModel.g:6246:1: (lv_domains_15_0= ruleRDFSDomain )
                    	    {
                    	    // InternalOWLModel.g:6246:1: (lv_domains_15_0= ruleRDFSDomain )
                    	    // InternalOWLModel.g:6247:3: lv_domains_15_0= ruleRDFSDomain
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getTransitivePropertyAccess().getDomainsRDFSDomainParserRuleCall_3_1_1_5_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_89);
                    	    lv_domains_15_0=ruleRDFSDomain();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getTransitivePropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"domains",
                    	            		lv_domains_15_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.RDFSDomain");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 7 :
                    	    // InternalOWLModel.g:6264:6: ( (lv_modifiers_16_0= rulePropertyModifier ) )
                    	    {
                    	    // InternalOWLModel.g:6264:6: ( (lv_modifiers_16_0= rulePropertyModifier ) )
                    	    // InternalOWLModel.g:6265:1: (lv_modifiers_16_0= rulePropertyModifier )
                    	    {
                    	    // InternalOWLModel.g:6265:1: (lv_modifiers_16_0= rulePropertyModifier )
                    	    // InternalOWLModel.g:6266:3: lv_modifiers_16_0= rulePropertyModifier
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getTransitivePropertyAccess().getModifiersPropertyModifierParserRuleCall_3_1_1_6_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_89);
                    	    lv_modifiers_16_0=rulePropertyModifier();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getTransitivePropertyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"modifiers",
                    	            		lv_modifiers_16_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.PropertyModifier");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop111;
                        }
                    } while (true);

                    otherlv_17=(Token)match(input,107,FOLLOW_7); 

                        	newLeafNode(otherlv_17, grammarAccess.getTransitivePropertyAccess().getOwlTransitivePropertyKeyword_3_1_2());
                        
                    otherlv_18=(Token)match(input,21,FOLLOW_2); 

                        	newLeafNode(otherlv_18, grammarAccess.getTransitivePropertyAccess().getGreaterThanSignKeyword_3_1_3());
                        

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTransitiveProperty"


    // $ANTLR start "entryRulePropertyModifier"
    // InternalOWLModel.g:6298:1: entryRulePropertyModifier returns [EObject current=null] : iv_rulePropertyModifier= rulePropertyModifier EOF ;
    public final EObject entryRulePropertyModifier() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePropertyModifier = null;


        try {
            // InternalOWLModel.g:6299:2: (iv_rulePropertyModifier= rulePropertyModifier EOF )
            // InternalOWLModel.g:6300:2: iv_rulePropertyModifier= rulePropertyModifier EOF
            {
             newCompositeNode(grammarAccess.getPropertyModifierRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePropertyModifier=rulePropertyModifier();

            state._fsp--;

             current =iv_rulePropertyModifier; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePropertyModifier"


    // $ANTLR start "rulePropertyModifier"
    // InternalOWLModel.g:6307:1: rulePropertyModifier returns [EObject current=null] : (this_InverseType_0= ruleInverseType | this_EquivalentType_1= ruleEquivalentType | this_SubPropertyOf_2= ruleSubPropertyOf ) ;
    public final EObject rulePropertyModifier() throws RecognitionException {
        EObject current = null;

        EObject this_InverseType_0 = null;

        EObject this_EquivalentType_1 = null;

        EObject this_SubPropertyOf_2 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:6310:28: ( (this_InverseType_0= ruleInverseType | this_EquivalentType_1= ruleEquivalentType | this_SubPropertyOf_2= ruleSubPropertyOf ) )
            // InternalOWLModel.g:6311:1: (this_InverseType_0= ruleInverseType | this_EquivalentType_1= ruleEquivalentType | this_SubPropertyOf_2= ruleSubPropertyOf )
            {
            // InternalOWLModel.g:6311:1: (this_InverseType_0= ruleInverseType | this_EquivalentType_1= ruleEquivalentType | this_SubPropertyOf_2= ruleSubPropertyOf )
            int alt113=3;
            switch ( input.LA(1) ) {
            case 108:
                {
                alt113=1;
                }
                break;
            case 110:
                {
                alt113=2;
                }
                break;
            case 120:
                {
                alt113=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 113, 0, input);

                throw nvae;
            }

            switch (alt113) {
                case 1 :
                    // InternalOWLModel.g:6312:5: this_InverseType_0= ruleInverseType
                    {
                     
                            newCompositeNode(grammarAccess.getPropertyModifierAccess().getInverseTypeParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_2);
                    this_InverseType_0=ruleInverseType();

                    state._fsp--;

                     
                            current = this_InverseType_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:6322:5: this_EquivalentType_1= ruleEquivalentType
                    {
                     
                            newCompositeNode(grammarAccess.getPropertyModifierAccess().getEquivalentTypeParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_2);
                    this_EquivalentType_1=ruleEquivalentType();

                    state._fsp--;

                     
                            current = this_EquivalentType_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // InternalOWLModel.g:6332:5: this_SubPropertyOf_2= ruleSubPropertyOf
                    {
                     
                            newCompositeNode(grammarAccess.getPropertyModifierAccess().getSubPropertyOfParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_2);
                    this_SubPropertyOf_2=ruleSubPropertyOf();

                    state._fsp--;

                     
                            current = this_SubPropertyOf_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePropertyModifier"


    // $ANTLR start "entryRuleInverseType"
    // InternalOWLModel.g:6348:1: entryRuleInverseType returns [EObject current=null] : iv_ruleInverseType= ruleInverseType EOF ;
    public final EObject entryRuleInverseType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInverseType = null;


        try {
            // InternalOWLModel.g:6349:2: (iv_ruleInverseType= ruleInverseType EOF )
            // InternalOWLModel.g:6350:2: iv_ruleInverseType= ruleInverseType EOF
            {
             newCompositeNode(grammarAccess.getInverseTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleInverseType=ruleInverseType();

            state._fsp--;

             current =iv_ruleInverseType; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInverseType"


    // $ANTLR start "ruleInverseType"
    // InternalOWLModel.g:6357:1: ruleInverseType returns [EObject current=null] : (otherlv_0= '<owl:inverseOf' ( (otherlv_1= '>' ( (lv_props_2_0= ruleProperty ) )+ otherlv_3= '</owl:inverseOf' otherlv_4= '>' ) | (otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) (otherlv_8= '/>' | (otherlv_9= '>' otherlv_10= '</owl:inverseOf' otherlv_11= '>' ) ) ) ) ) ;
    public final EObject ruleInverseType() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        EObject lv_props_2_0 = null;

        EObject lv_ref_7_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:6360:28: ( (otherlv_0= '<owl:inverseOf' ( (otherlv_1= '>' ( (lv_props_2_0= ruleProperty ) )+ otherlv_3= '</owl:inverseOf' otherlv_4= '>' ) | (otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) (otherlv_8= '/>' | (otherlv_9= '>' otherlv_10= '</owl:inverseOf' otherlv_11= '>' ) ) ) ) ) )
            // InternalOWLModel.g:6361:1: (otherlv_0= '<owl:inverseOf' ( (otherlv_1= '>' ( (lv_props_2_0= ruleProperty ) )+ otherlv_3= '</owl:inverseOf' otherlv_4= '>' ) | (otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) (otherlv_8= '/>' | (otherlv_9= '>' otherlv_10= '</owl:inverseOf' otherlv_11= '>' ) ) ) ) )
            {
            // InternalOWLModel.g:6361:1: (otherlv_0= '<owl:inverseOf' ( (otherlv_1= '>' ( (lv_props_2_0= ruleProperty ) )+ otherlv_3= '</owl:inverseOf' otherlv_4= '>' ) | (otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) (otherlv_8= '/>' | (otherlv_9= '>' otherlv_10= '</owl:inverseOf' otherlv_11= '>' ) ) ) ) )
            // InternalOWLModel.g:6361:3: otherlv_0= '<owl:inverseOf' ( (otherlv_1= '>' ( (lv_props_2_0= ruleProperty ) )+ otherlv_3= '</owl:inverseOf' otherlv_4= '>' ) | (otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) (otherlv_8= '/>' | (otherlv_9= '>' otherlv_10= '</owl:inverseOf' otherlv_11= '>' ) ) ) )
            {
            otherlv_0=(Token)match(input,108,FOLLOW_30); 

                	newLeafNode(otherlv_0, grammarAccess.getInverseTypeAccess().getOwlInverseOfKeyword_0());
                
            // InternalOWLModel.g:6365:1: ( (otherlv_1= '>' ( (lv_props_2_0= ruleProperty ) )+ otherlv_3= '</owl:inverseOf' otherlv_4= '>' ) | (otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) (otherlv_8= '/>' | (otherlv_9= '>' otherlv_10= '</owl:inverseOf' otherlv_11= '>' ) ) ) )
            int alt116=2;
            int LA116_0 = input.LA(1);

            if ( (LA116_0==21) ) {
                alt116=1;
            }
            else if ( (LA116_0==32) ) {
                alt116=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 116, 0, input);

                throw nvae;
            }
            switch (alt116) {
                case 1 :
                    // InternalOWLModel.g:6365:2: (otherlv_1= '>' ( (lv_props_2_0= ruleProperty ) )+ otherlv_3= '</owl:inverseOf' otherlv_4= '>' )
                    {
                    // InternalOWLModel.g:6365:2: (otherlv_1= '>' ( (lv_props_2_0= ruleProperty ) )+ otherlv_3= '</owl:inverseOf' otherlv_4= '>' )
                    // InternalOWLModel.g:6365:4: otherlv_1= '>' ( (lv_props_2_0= ruleProperty ) )+ otherlv_3= '</owl:inverseOf' otherlv_4= '>'
                    {
                    otherlv_1=(Token)match(input,21,FOLLOW_67); 

                        	newLeafNode(otherlv_1, grammarAccess.getInverseTypeAccess().getGreaterThanSignKeyword_1_0_0());
                        
                    // InternalOWLModel.g:6369:1: ( (lv_props_2_0= ruleProperty ) )+
                    int cnt114=0;
                    loop114:
                    do {
                        int alt114=2;
                        int LA114_0 = input.LA(1);

                        if ( (LA114_0==94||LA114_0==96||LA114_0==98||LA114_0==100||LA114_0==102||LA114_0==104||LA114_0==106) ) {
                            alt114=1;
                        }


                        switch (alt114) {
                    	case 1 :
                    	    // InternalOWLModel.g:6370:1: (lv_props_2_0= ruleProperty )
                    	    {
                    	    // InternalOWLModel.g:6370:1: (lv_props_2_0= ruleProperty )
                    	    // InternalOWLModel.g:6371:3: lv_props_2_0= ruleProperty
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getInverseTypeAccess().getPropsPropertyParserRuleCall_1_0_1_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_90);
                    	    lv_props_2_0=ruleProperty();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getInverseTypeRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"props",
                    	            		lv_props_2_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.Property");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt114 >= 1 ) break loop114;
                                EarlyExitException eee =
                                    new EarlyExitException(114, input);
                                throw eee;
                        }
                        cnt114++;
                    } while (true);

                    otherlv_3=(Token)match(input,109,FOLLOW_7); 

                        	newLeafNode(otherlv_3, grammarAccess.getInverseTypeAccess().getOwlInverseOfKeyword_1_0_2());
                        
                    otherlv_4=(Token)match(input,21,FOLLOW_2); 

                        	newLeafNode(otherlv_4, grammarAccess.getInverseTypeAccess().getGreaterThanSignKeyword_1_0_3());
                        

                    }


                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:6396:6: (otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) (otherlv_8= '/>' | (otherlv_9= '>' otherlv_10= '</owl:inverseOf' otherlv_11= '>' ) ) )
                    {
                    // InternalOWLModel.g:6396:6: (otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) (otherlv_8= '/>' | (otherlv_9= '>' otherlv_10= '</owl:inverseOf' otherlv_11= '>' ) ) )
                    // InternalOWLModel.g:6396:8: otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) (otherlv_8= '/>' | (otherlv_9= '>' otherlv_10= '</owl:inverseOf' otherlv_11= '>' ) )
                    {
                    otherlv_5=(Token)match(input,32,FOLLOW_13); 

                        	newLeafNode(otherlv_5, grammarAccess.getInverseTypeAccess().getRdfResourceKeyword_1_1_0());
                        
                    otherlv_6=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_6, grammarAccess.getInverseTypeAccess().getEqualsSignKeyword_1_1_1());
                        
                    // InternalOWLModel.g:6404:1: ( (lv_ref_7_0= ruleOwlRef ) )
                    // InternalOWLModel.g:6405:1: (lv_ref_7_0= ruleOwlRef )
                    {
                    // InternalOWLModel.g:6405:1: (lv_ref_7_0= ruleOwlRef )
                    // InternalOWLModel.g:6406:3: lv_ref_7_0= ruleOwlRef
                    {
                     
                    	        newCompositeNode(grammarAccess.getInverseTypeAccess().getRefOwlRefParserRuleCall_1_1_2_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_ref_7_0=ruleOwlRef();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getInverseTypeRule());
                    	        }
                           		set(
                           			current, 
                           			"ref",
                            		lv_ref_7_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlRef");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // InternalOWLModel.g:6422:2: (otherlv_8= '/>' | (otherlv_9= '>' otherlv_10= '</owl:inverseOf' otherlv_11= '>' ) )
                    int alt115=2;
                    int LA115_0 = input.LA(1);

                    if ( (LA115_0==29) ) {
                        alt115=1;
                    }
                    else if ( (LA115_0==21) ) {
                        alt115=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 115, 0, input);

                        throw nvae;
                    }
                    switch (alt115) {
                        case 1 :
                            // InternalOWLModel.g:6422:4: otherlv_8= '/>'
                            {
                            otherlv_8=(Token)match(input,29,FOLLOW_2); 

                                	newLeafNode(otherlv_8, grammarAccess.getInverseTypeAccess().getSolidusGreaterThanSignKeyword_1_1_3_0());
                                

                            }
                            break;
                        case 2 :
                            // InternalOWLModel.g:6427:6: (otherlv_9= '>' otherlv_10= '</owl:inverseOf' otherlv_11= '>' )
                            {
                            // InternalOWLModel.g:6427:6: (otherlv_9= '>' otherlv_10= '</owl:inverseOf' otherlv_11= '>' )
                            // InternalOWLModel.g:6427:8: otherlv_9= '>' otherlv_10= '</owl:inverseOf' otherlv_11= '>'
                            {
                            otherlv_9=(Token)match(input,21,FOLLOW_91); 

                                	newLeafNode(otherlv_9, grammarAccess.getInverseTypeAccess().getGreaterThanSignKeyword_1_1_3_1_0());
                                
                            otherlv_10=(Token)match(input,109,FOLLOW_7); 

                                	newLeafNode(otherlv_10, grammarAccess.getInverseTypeAccess().getOwlInverseOfKeyword_1_1_3_1_1());
                                
                            otherlv_11=(Token)match(input,21,FOLLOW_2); 

                                	newLeafNode(otherlv_11, grammarAccess.getInverseTypeAccess().getGreaterThanSignKeyword_1_1_3_1_2());
                                

                            }


                            }
                            break;

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInverseType"


    // $ANTLR start "entryRuleEquivalentType"
    // InternalOWLModel.g:6447:1: entryRuleEquivalentType returns [EObject current=null] : iv_ruleEquivalentType= ruleEquivalentType EOF ;
    public final EObject entryRuleEquivalentType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEquivalentType = null;


        try {
            // InternalOWLModel.g:6448:2: (iv_ruleEquivalentType= ruleEquivalentType EOF )
            // InternalOWLModel.g:6449:2: iv_ruleEquivalentType= ruleEquivalentType EOF
            {
             newCompositeNode(grammarAccess.getEquivalentTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEquivalentType=ruleEquivalentType();

            state._fsp--;

             current =iv_ruleEquivalentType; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEquivalentType"


    // $ANTLR start "ruleEquivalentType"
    // InternalOWLModel.g:6456:1: ruleEquivalentType returns [EObject current=null] : (otherlv_0= '<owl:equivalentProperty' ( (otherlv_1= '>' ( (lv_props_2_0= ruleProperty ) )+ otherlv_3= '</owl:equivalentProperty' otherlv_4= '>' ) | (otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) (otherlv_8= '/>' | (otherlv_9= '>' otherlv_10= '</owl:equivalentProperty' otherlv_11= '>' ) ) ) ) ) ;
    public final EObject ruleEquivalentType() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        EObject lv_props_2_0 = null;

        EObject lv_ref_7_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:6459:28: ( (otherlv_0= '<owl:equivalentProperty' ( (otherlv_1= '>' ( (lv_props_2_0= ruleProperty ) )+ otherlv_3= '</owl:equivalentProperty' otherlv_4= '>' ) | (otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) (otherlv_8= '/>' | (otherlv_9= '>' otherlv_10= '</owl:equivalentProperty' otherlv_11= '>' ) ) ) ) ) )
            // InternalOWLModel.g:6460:1: (otherlv_0= '<owl:equivalentProperty' ( (otherlv_1= '>' ( (lv_props_2_0= ruleProperty ) )+ otherlv_3= '</owl:equivalentProperty' otherlv_4= '>' ) | (otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) (otherlv_8= '/>' | (otherlv_9= '>' otherlv_10= '</owl:equivalentProperty' otherlv_11= '>' ) ) ) ) )
            {
            // InternalOWLModel.g:6460:1: (otherlv_0= '<owl:equivalentProperty' ( (otherlv_1= '>' ( (lv_props_2_0= ruleProperty ) )+ otherlv_3= '</owl:equivalentProperty' otherlv_4= '>' ) | (otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) (otherlv_8= '/>' | (otherlv_9= '>' otherlv_10= '</owl:equivalentProperty' otherlv_11= '>' ) ) ) ) )
            // InternalOWLModel.g:6460:3: otherlv_0= '<owl:equivalentProperty' ( (otherlv_1= '>' ( (lv_props_2_0= ruleProperty ) )+ otherlv_3= '</owl:equivalentProperty' otherlv_4= '>' ) | (otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) (otherlv_8= '/>' | (otherlv_9= '>' otherlv_10= '</owl:equivalentProperty' otherlv_11= '>' ) ) ) )
            {
            otherlv_0=(Token)match(input,110,FOLLOW_30); 

                	newLeafNode(otherlv_0, grammarAccess.getEquivalentTypeAccess().getOwlEquivalentPropertyKeyword_0());
                
            // InternalOWLModel.g:6464:1: ( (otherlv_1= '>' ( (lv_props_2_0= ruleProperty ) )+ otherlv_3= '</owl:equivalentProperty' otherlv_4= '>' ) | (otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) (otherlv_8= '/>' | (otherlv_9= '>' otherlv_10= '</owl:equivalentProperty' otherlv_11= '>' ) ) ) )
            int alt119=2;
            int LA119_0 = input.LA(1);

            if ( (LA119_0==21) ) {
                alt119=1;
            }
            else if ( (LA119_0==32) ) {
                alt119=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 119, 0, input);

                throw nvae;
            }
            switch (alt119) {
                case 1 :
                    // InternalOWLModel.g:6464:2: (otherlv_1= '>' ( (lv_props_2_0= ruleProperty ) )+ otherlv_3= '</owl:equivalentProperty' otherlv_4= '>' )
                    {
                    // InternalOWLModel.g:6464:2: (otherlv_1= '>' ( (lv_props_2_0= ruleProperty ) )+ otherlv_3= '</owl:equivalentProperty' otherlv_4= '>' )
                    // InternalOWLModel.g:6464:4: otherlv_1= '>' ( (lv_props_2_0= ruleProperty ) )+ otherlv_3= '</owl:equivalentProperty' otherlv_4= '>'
                    {
                    otherlv_1=(Token)match(input,21,FOLLOW_67); 

                        	newLeafNode(otherlv_1, grammarAccess.getEquivalentTypeAccess().getGreaterThanSignKeyword_1_0_0());
                        
                    // InternalOWLModel.g:6468:1: ( (lv_props_2_0= ruleProperty ) )+
                    int cnt117=0;
                    loop117:
                    do {
                        int alt117=2;
                        int LA117_0 = input.LA(1);

                        if ( (LA117_0==94||LA117_0==96||LA117_0==98||LA117_0==100||LA117_0==102||LA117_0==104||LA117_0==106) ) {
                            alt117=1;
                        }


                        switch (alt117) {
                    	case 1 :
                    	    // InternalOWLModel.g:6469:1: (lv_props_2_0= ruleProperty )
                    	    {
                    	    // InternalOWLModel.g:6469:1: (lv_props_2_0= ruleProperty )
                    	    // InternalOWLModel.g:6470:3: lv_props_2_0= ruleProperty
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getEquivalentTypeAccess().getPropsPropertyParserRuleCall_1_0_1_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_92);
                    	    lv_props_2_0=ruleProperty();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getEquivalentTypeRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"props",
                    	            		lv_props_2_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.Property");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt117 >= 1 ) break loop117;
                                EarlyExitException eee =
                                    new EarlyExitException(117, input);
                                throw eee;
                        }
                        cnt117++;
                    } while (true);

                    otherlv_3=(Token)match(input,111,FOLLOW_7); 

                        	newLeafNode(otherlv_3, grammarAccess.getEquivalentTypeAccess().getOwlEquivalentPropertyKeyword_1_0_2());
                        
                    otherlv_4=(Token)match(input,21,FOLLOW_2); 

                        	newLeafNode(otherlv_4, grammarAccess.getEquivalentTypeAccess().getGreaterThanSignKeyword_1_0_3());
                        

                    }


                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:6495:6: (otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) (otherlv_8= '/>' | (otherlv_9= '>' otherlv_10= '</owl:equivalentProperty' otherlv_11= '>' ) ) )
                    {
                    // InternalOWLModel.g:6495:6: (otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) (otherlv_8= '/>' | (otherlv_9= '>' otherlv_10= '</owl:equivalentProperty' otherlv_11= '>' ) ) )
                    // InternalOWLModel.g:6495:8: otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_ref_7_0= ruleOwlRef ) ) (otherlv_8= '/>' | (otherlv_9= '>' otherlv_10= '</owl:equivalentProperty' otherlv_11= '>' ) )
                    {
                    otherlv_5=(Token)match(input,32,FOLLOW_13); 

                        	newLeafNode(otherlv_5, grammarAccess.getEquivalentTypeAccess().getRdfResourceKeyword_1_1_0());
                        
                    otherlv_6=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_6, grammarAccess.getEquivalentTypeAccess().getEqualsSignKeyword_1_1_1());
                        
                    // InternalOWLModel.g:6503:1: ( (lv_ref_7_0= ruleOwlRef ) )
                    // InternalOWLModel.g:6504:1: (lv_ref_7_0= ruleOwlRef )
                    {
                    // InternalOWLModel.g:6504:1: (lv_ref_7_0= ruleOwlRef )
                    // InternalOWLModel.g:6505:3: lv_ref_7_0= ruleOwlRef
                    {
                     
                    	        newCompositeNode(grammarAccess.getEquivalentTypeAccess().getRefOwlRefParserRuleCall_1_1_2_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_ref_7_0=ruleOwlRef();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getEquivalentTypeRule());
                    	        }
                           		set(
                           			current, 
                           			"ref",
                            		lv_ref_7_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlRef");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // InternalOWLModel.g:6521:2: (otherlv_8= '/>' | (otherlv_9= '>' otherlv_10= '</owl:equivalentProperty' otherlv_11= '>' ) )
                    int alt118=2;
                    int LA118_0 = input.LA(1);

                    if ( (LA118_0==29) ) {
                        alt118=1;
                    }
                    else if ( (LA118_0==21) ) {
                        alt118=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 118, 0, input);

                        throw nvae;
                    }
                    switch (alt118) {
                        case 1 :
                            // InternalOWLModel.g:6521:4: otherlv_8= '/>'
                            {
                            otherlv_8=(Token)match(input,29,FOLLOW_2); 

                                	newLeafNode(otherlv_8, grammarAccess.getEquivalentTypeAccess().getSolidusGreaterThanSignKeyword_1_1_3_0());
                                

                            }
                            break;
                        case 2 :
                            // InternalOWLModel.g:6526:6: (otherlv_9= '>' otherlv_10= '</owl:equivalentProperty' otherlv_11= '>' )
                            {
                            // InternalOWLModel.g:6526:6: (otherlv_9= '>' otherlv_10= '</owl:equivalentProperty' otherlv_11= '>' )
                            // InternalOWLModel.g:6526:8: otherlv_9= '>' otherlv_10= '</owl:equivalentProperty' otherlv_11= '>'
                            {
                            otherlv_9=(Token)match(input,21,FOLLOW_93); 

                                	newLeafNode(otherlv_9, grammarAccess.getEquivalentTypeAccess().getGreaterThanSignKeyword_1_1_3_1_0());
                                
                            otherlv_10=(Token)match(input,111,FOLLOW_7); 

                                	newLeafNode(otherlv_10, grammarAccess.getEquivalentTypeAccess().getOwlEquivalentPropertyKeyword_1_1_3_1_1());
                                
                            otherlv_11=(Token)match(input,21,FOLLOW_2); 

                                	newLeafNode(otherlv_11, grammarAccess.getEquivalentTypeAccess().getGreaterThanSignKeyword_1_1_3_1_2());
                                

                            }


                            }
                            break;

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEquivalentType"


    // $ANTLR start "entryRuleRDFSDomain"
    // InternalOWLModel.g:6546:1: entryRuleRDFSDomain returns [EObject current=null] : iv_ruleRDFSDomain= ruleRDFSDomain EOF ;
    public final EObject entryRuleRDFSDomain() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRDFSDomain = null;


        try {
            // InternalOWLModel.g:6547:2: (iv_ruleRDFSDomain= ruleRDFSDomain EOF )
            // InternalOWLModel.g:6548:2: iv_ruleRDFSDomain= ruleRDFSDomain EOF
            {
             newCompositeNode(grammarAccess.getRDFSDomainRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRDFSDomain=ruleRDFSDomain();

            state._fsp--;

             current =iv_ruleRDFSDomain; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRDFSDomain"


    // $ANTLR start "ruleRDFSDomain"
    // InternalOWLModel.g:6555:1: ruleRDFSDomain returns [EObject current=null] : (otherlv_0= '<rdfs:domain' ( ( () otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</rdfs:domain' otherlv_8= '>' ) ) ) | ( () (otherlv_10= 'rdf:parseType' otherlv_11= '=' ( (lv_parseType_12_0= ruleParseType ) ) )? otherlv_13= '>' ( (lv_modifiers_14_0= ruleClassModifier ) )+ otherlv_15= '</rdfs:domain' otherlv_16= '>' ) ) ) ;
    public final EObject ruleRDFSDomain() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        EObject lv_ref_4_0 = null;

        EObject lv_parseType_12_0 = null;

        EObject lv_modifiers_14_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:6558:28: ( (otherlv_0= '<rdfs:domain' ( ( () otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</rdfs:domain' otherlv_8= '>' ) ) ) | ( () (otherlv_10= 'rdf:parseType' otherlv_11= '=' ( (lv_parseType_12_0= ruleParseType ) ) )? otherlv_13= '>' ( (lv_modifiers_14_0= ruleClassModifier ) )+ otherlv_15= '</rdfs:domain' otherlv_16= '>' ) ) ) )
            // InternalOWLModel.g:6559:1: (otherlv_0= '<rdfs:domain' ( ( () otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</rdfs:domain' otherlv_8= '>' ) ) ) | ( () (otherlv_10= 'rdf:parseType' otherlv_11= '=' ( (lv_parseType_12_0= ruleParseType ) ) )? otherlv_13= '>' ( (lv_modifiers_14_0= ruleClassModifier ) )+ otherlv_15= '</rdfs:domain' otherlv_16= '>' ) ) )
            {
            // InternalOWLModel.g:6559:1: (otherlv_0= '<rdfs:domain' ( ( () otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</rdfs:domain' otherlv_8= '>' ) ) ) | ( () (otherlv_10= 'rdf:parseType' otherlv_11= '=' ( (lv_parseType_12_0= ruleParseType ) ) )? otherlv_13= '>' ( (lv_modifiers_14_0= ruleClassModifier ) )+ otherlv_15= '</rdfs:domain' otherlv_16= '>' ) ) )
            // InternalOWLModel.g:6559:3: otherlv_0= '<rdfs:domain' ( ( () otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</rdfs:domain' otherlv_8= '>' ) ) ) | ( () (otherlv_10= 'rdf:parseType' otherlv_11= '=' ( (lv_parseType_12_0= ruleParseType ) ) )? otherlv_13= '>' ( (lv_modifiers_14_0= ruleClassModifier ) )+ otherlv_15= '</rdfs:domain' otherlv_16= '>' ) )
            {
            otherlv_0=(Token)match(input,112,FOLLOW_37); 

                	newLeafNode(otherlv_0, grammarAccess.getRDFSDomainAccess().getRdfsDomainKeyword_0());
                
            // InternalOWLModel.g:6563:1: ( ( () otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</rdfs:domain' otherlv_8= '>' ) ) ) | ( () (otherlv_10= 'rdf:parseType' otherlv_11= '=' ( (lv_parseType_12_0= ruleParseType ) ) )? otherlv_13= '>' ( (lv_modifiers_14_0= ruleClassModifier ) )+ otherlv_15= '</rdfs:domain' otherlv_16= '>' ) )
            int alt123=2;
            int LA123_0 = input.LA(1);

            if ( (LA123_0==32) ) {
                alt123=1;
            }
            else if ( (LA123_0==21||LA123_0==44) ) {
                alt123=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 123, 0, input);

                throw nvae;
            }
            switch (alt123) {
                case 1 :
                    // InternalOWLModel.g:6563:2: ( () otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</rdfs:domain' otherlv_8= '>' ) ) )
                    {
                    // InternalOWLModel.g:6563:2: ( () otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</rdfs:domain' otherlv_8= '>' ) ) )
                    // InternalOWLModel.g:6563:3: () otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</rdfs:domain' otherlv_8= '>' ) )
                    {
                    // InternalOWLModel.g:6563:3: ()
                    // InternalOWLModel.g:6564:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getRDFSDomainAccess().getSimpleRDFSDomainAction_1_0_0(),
                                current);
                        

                    }

                    otherlv_2=(Token)match(input,32,FOLLOW_13); 

                        	newLeafNode(otherlv_2, grammarAccess.getRDFSDomainAccess().getRdfResourceKeyword_1_0_1());
                        
                    otherlv_3=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_3, grammarAccess.getRDFSDomainAccess().getEqualsSignKeyword_1_0_2());
                        
                    // InternalOWLModel.g:6577:1: ( (lv_ref_4_0= ruleOwlRef ) )
                    // InternalOWLModel.g:6578:1: (lv_ref_4_0= ruleOwlRef )
                    {
                    // InternalOWLModel.g:6578:1: (lv_ref_4_0= ruleOwlRef )
                    // InternalOWLModel.g:6579:3: lv_ref_4_0= ruleOwlRef
                    {
                     
                    	        newCompositeNode(grammarAccess.getRDFSDomainAccess().getRefOwlRefParserRuleCall_1_0_3_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_ref_4_0=ruleOwlRef();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getRDFSDomainRule());
                    	        }
                           		set(
                           			current, 
                           			"ref",
                            		lv_ref_4_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlRef");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // InternalOWLModel.g:6595:2: (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</rdfs:domain' otherlv_8= '>' ) )
                    int alt120=2;
                    int LA120_0 = input.LA(1);

                    if ( (LA120_0==29) ) {
                        alt120=1;
                    }
                    else if ( (LA120_0==21) ) {
                        alt120=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 120, 0, input);

                        throw nvae;
                    }
                    switch (alt120) {
                        case 1 :
                            // InternalOWLModel.g:6595:4: otherlv_5= '/>'
                            {
                            otherlv_5=(Token)match(input,29,FOLLOW_2); 

                                	newLeafNode(otherlv_5, grammarAccess.getRDFSDomainAccess().getSolidusGreaterThanSignKeyword_1_0_4_0());
                                

                            }
                            break;
                        case 2 :
                            // InternalOWLModel.g:6600:6: (otherlv_6= '>' otherlv_7= '</rdfs:domain' otherlv_8= '>' )
                            {
                            // InternalOWLModel.g:6600:6: (otherlv_6= '>' otherlv_7= '</rdfs:domain' otherlv_8= '>' )
                            // InternalOWLModel.g:6600:8: otherlv_6= '>' otherlv_7= '</rdfs:domain' otherlv_8= '>'
                            {
                            otherlv_6=(Token)match(input,21,FOLLOW_94); 

                                	newLeafNode(otherlv_6, grammarAccess.getRDFSDomainAccess().getGreaterThanSignKeyword_1_0_4_1_0());
                                
                            otherlv_7=(Token)match(input,113,FOLLOW_7); 

                                	newLeafNode(otherlv_7, grammarAccess.getRDFSDomainAccess().getRdfsDomainKeyword_1_0_4_1_1());
                                
                            otherlv_8=(Token)match(input,21,FOLLOW_2); 

                                	newLeafNode(otherlv_8, grammarAccess.getRDFSDomainAccess().getGreaterThanSignKeyword_1_0_4_1_2());
                                

                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:6613:6: ( () (otherlv_10= 'rdf:parseType' otherlv_11= '=' ( (lv_parseType_12_0= ruleParseType ) ) )? otherlv_13= '>' ( (lv_modifiers_14_0= ruleClassModifier ) )+ otherlv_15= '</rdfs:domain' otherlv_16= '>' )
                    {
                    // InternalOWLModel.g:6613:6: ( () (otherlv_10= 'rdf:parseType' otherlv_11= '=' ( (lv_parseType_12_0= ruleParseType ) ) )? otherlv_13= '>' ( (lv_modifiers_14_0= ruleClassModifier ) )+ otherlv_15= '</rdfs:domain' otherlv_16= '>' )
                    // InternalOWLModel.g:6613:7: () (otherlv_10= 'rdf:parseType' otherlv_11= '=' ( (lv_parseType_12_0= ruleParseType ) ) )? otherlv_13= '>' ( (lv_modifiers_14_0= ruleClassModifier ) )+ otherlv_15= '</rdfs:domain' otherlv_16= '>'
                    {
                    // InternalOWLModel.g:6613:7: ()
                    // InternalOWLModel.g:6614:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getRDFSDomainAccess().getParsedRDFSDomainAction_1_1_0(),
                                current);
                        

                    }

                    // InternalOWLModel.g:6619:2: (otherlv_10= 'rdf:parseType' otherlv_11= '=' ( (lv_parseType_12_0= ruleParseType ) ) )?
                    int alt121=2;
                    int LA121_0 = input.LA(1);

                    if ( (LA121_0==44) ) {
                        alt121=1;
                    }
                    switch (alt121) {
                        case 1 :
                            // InternalOWLModel.g:6619:4: otherlv_10= 'rdf:parseType' otherlv_11= '=' ( (lv_parseType_12_0= ruleParseType ) )
                            {
                            otherlv_10=(Token)match(input,44,FOLLOW_13); 

                                	newLeafNode(otherlv_10, grammarAccess.getRDFSDomainAccess().getRdfParseTypeKeyword_1_1_1_0());
                                
                            otherlv_11=(Token)match(input,25,FOLLOW_9); 

                                	newLeafNode(otherlv_11, grammarAccess.getRDFSDomainAccess().getEqualsSignKeyword_1_1_1_1());
                                
                            // InternalOWLModel.g:6627:1: ( (lv_parseType_12_0= ruleParseType ) )
                            // InternalOWLModel.g:6628:1: (lv_parseType_12_0= ruleParseType )
                            {
                            // InternalOWLModel.g:6628:1: (lv_parseType_12_0= ruleParseType )
                            // InternalOWLModel.g:6629:3: lv_parseType_12_0= ruleParseType
                            {
                             
                            	        newCompositeNode(grammarAccess.getRDFSDomainAccess().getParseTypeParseTypeParserRuleCall_1_1_1_2_0()); 
                            	    
                            pushFollow(FOLLOW_7);
                            lv_parseType_12_0=ruleParseType();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getRDFSDomainRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"parseType",
                                    		lv_parseType_12_0, 
                                    		"org.smool.sdk.owlparser.OWLModel.ParseType");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }

                    otherlv_13=(Token)match(input,21,FOLLOW_39); 

                        	newLeafNode(otherlv_13, grammarAccess.getRDFSDomainAccess().getGreaterThanSignKeyword_1_1_2());
                        
                    // InternalOWLModel.g:6649:1: ( (lv_modifiers_14_0= ruleClassModifier ) )+
                    int cnt122=0;
                    loop122:
                    do {
                        int alt122=2;
                        int LA122_0 = input.LA(1);

                        if ( (LA122_0==39||LA122_0==70||LA122_0==122) ) {
                            alt122=1;
                        }


                        switch (alt122) {
                    	case 1 :
                    	    // InternalOWLModel.g:6650:1: (lv_modifiers_14_0= ruleClassModifier )
                    	    {
                    	    // InternalOWLModel.g:6650:1: (lv_modifiers_14_0= ruleClassModifier )
                    	    // InternalOWLModel.g:6651:3: lv_modifiers_14_0= ruleClassModifier
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getRDFSDomainAccess().getModifiersClassModifierParserRuleCall_1_1_3_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_95);
                    	    lv_modifiers_14_0=ruleClassModifier();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getRDFSDomainRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"modifiers",
                    	            		lv_modifiers_14_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.ClassModifier");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt122 >= 1 ) break loop122;
                                EarlyExitException eee =
                                    new EarlyExitException(122, input);
                                throw eee;
                        }
                        cnt122++;
                    } while (true);

                    otherlv_15=(Token)match(input,113,FOLLOW_7); 

                        	newLeafNode(otherlv_15, grammarAccess.getRDFSDomainAccess().getRdfsDomainKeyword_1_1_4());
                        
                    otherlv_16=(Token)match(input,21,FOLLOW_2); 

                        	newLeafNode(otherlv_16, grammarAccess.getRDFSDomainAccess().getGreaterThanSignKeyword_1_1_5());
                        

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRDFSDomain"


    // $ANTLR start "entryRuleRDFSRange"
    // InternalOWLModel.g:6683:1: entryRuleRDFSRange returns [EObject current=null] : iv_ruleRDFSRange= ruleRDFSRange EOF ;
    public final EObject entryRuleRDFSRange() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRDFSRange = null;


        try {
            // InternalOWLModel.g:6684:2: (iv_ruleRDFSRange= ruleRDFSRange EOF )
            // InternalOWLModel.g:6685:2: iv_ruleRDFSRange= ruleRDFSRange EOF
            {
             newCompositeNode(grammarAccess.getRDFSRangeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRDFSRange=ruleRDFSRange();

            state._fsp--;

             current =iv_ruleRDFSRange; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRDFSRange"


    // $ANTLR start "ruleRDFSRange"
    // InternalOWLModel.g:6692:1: ruleRDFSRange returns [EObject current=null] : (otherlv_0= '<rdfs:range' ( ( () otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_range_4_0= ruleOwlRef ) ) (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</rdfs:range' otherlv_8= '>' ) ) ) | ( () (otherlv_10= 'rdf:parseType' otherlv_11= '=' ( (lv_parseType_12_0= ruleParseType ) ) )? otherlv_13= '>' ( (lv_modifiers_14_0= ruleClassModifier ) )+ otherlv_15= '</rdfs:range' otherlv_16= '>' ) | ( () otherlv_18= '>' ( (lv_owlRange_19_0= ruleOwlDataRange ) ) otherlv_20= '</rdfs:range' otherlv_21= '>' ) ) ) ;
    public final EObject ruleRDFSRange() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_18=null;
        Token otherlv_20=null;
        Token otherlv_21=null;
        EObject lv_range_4_0 = null;

        EObject lv_parseType_12_0 = null;

        EObject lv_modifiers_14_0 = null;

        EObject lv_owlRange_19_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:6695:28: ( (otherlv_0= '<rdfs:range' ( ( () otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_range_4_0= ruleOwlRef ) ) (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</rdfs:range' otherlv_8= '>' ) ) ) | ( () (otherlv_10= 'rdf:parseType' otherlv_11= '=' ( (lv_parseType_12_0= ruleParseType ) ) )? otherlv_13= '>' ( (lv_modifiers_14_0= ruleClassModifier ) )+ otherlv_15= '</rdfs:range' otherlv_16= '>' ) | ( () otherlv_18= '>' ( (lv_owlRange_19_0= ruleOwlDataRange ) ) otherlv_20= '</rdfs:range' otherlv_21= '>' ) ) ) )
            // InternalOWLModel.g:6696:1: (otherlv_0= '<rdfs:range' ( ( () otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_range_4_0= ruleOwlRef ) ) (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</rdfs:range' otherlv_8= '>' ) ) ) | ( () (otherlv_10= 'rdf:parseType' otherlv_11= '=' ( (lv_parseType_12_0= ruleParseType ) ) )? otherlv_13= '>' ( (lv_modifiers_14_0= ruleClassModifier ) )+ otherlv_15= '</rdfs:range' otherlv_16= '>' ) | ( () otherlv_18= '>' ( (lv_owlRange_19_0= ruleOwlDataRange ) ) otherlv_20= '</rdfs:range' otherlv_21= '>' ) ) )
            {
            // InternalOWLModel.g:6696:1: (otherlv_0= '<rdfs:range' ( ( () otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_range_4_0= ruleOwlRef ) ) (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</rdfs:range' otherlv_8= '>' ) ) ) | ( () (otherlv_10= 'rdf:parseType' otherlv_11= '=' ( (lv_parseType_12_0= ruleParseType ) ) )? otherlv_13= '>' ( (lv_modifiers_14_0= ruleClassModifier ) )+ otherlv_15= '</rdfs:range' otherlv_16= '>' ) | ( () otherlv_18= '>' ( (lv_owlRange_19_0= ruleOwlDataRange ) ) otherlv_20= '</rdfs:range' otherlv_21= '>' ) ) )
            // InternalOWLModel.g:6696:3: otherlv_0= '<rdfs:range' ( ( () otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_range_4_0= ruleOwlRef ) ) (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</rdfs:range' otherlv_8= '>' ) ) ) | ( () (otherlv_10= 'rdf:parseType' otherlv_11= '=' ( (lv_parseType_12_0= ruleParseType ) ) )? otherlv_13= '>' ( (lv_modifiers_14_0= ruleClassModifier ) )+ otherlv_15= '</rdfs:range' otherlv_16= '>' ) | ( () otherlv_18= '>' ( (lv_owlRange_19_0= ruleOwlDataRange ) ) otherlv_20= '</rdfs:range' otherlv_21= '>' ) )
            {
            otherlv_0=(Token)match(input,114,FOLLOW_37); 

                	newLeafNode(otherlv_0, grammarAccess.getRDFSRangeAccess().getRdfsRangeKeyword_0());
                
            // InternalOWLModel.g:6700:1: ( ( () otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_range_4_0= ruleOwlRef ) ) (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</rdfs:range' otherlv_8= '>' ) ) ) | ( () (otherlv_10= 'rdf:parseType' otherlv_11= '=' ( (lv_parseType_12_0= ruleParseType ) ) )? otherlv_13= '>' ( (lv_modifiers_14_0= ruleClassModifier ) )+ otherlv_15= '</rdfs:range' otherlv_16= '>' ) | ( () otherlv_18= '>' ( (lv_owlRange_19_0= ruleOwlDataRange ) ) otherlv_20= '</rdfs:range' otherlv_21= '>' ) )
            int alt127=3;
            switch ( input.LA(1) ) {
            case 32:
                {
                alt127=1;
                }
                break;
            case 44:
                {
                alt127=2;
                }
                break;
            case 21:
                {
                int LA127_3 = input.LA(2);

                if ( (LA127_3==39||LA127_3==70||LA127_3==122) ) {
                    alt127=2;
                }
                else if ( (LA127_3==116) ) {
                    alt127=3;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 127, 3, input);

                    throw nvae;
                }
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 127, 0, input);

                throw nvae;
            }

            switch (alt127) {
                case 1 :
                    // InternalOWLModel.g:6700:2: ( () otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_range_4_0= ruleOwlRef ) ) (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</rdfs:range' otherlv_8= '>' ) ) )
                    {
                    // InternalOWLModel.g:6700:2: ( () otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_range_4_0= ruleOwlRef ) ) (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</rdfs:range' otherlv_8= '>' ) ) )
                    // InternalOWLModel.g:6700:3: () otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_range_4_0= ruleOwlRef ) ) (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</rdfs:range' otherlv_8= '>' ) )
                    {
                    // InternalOWLModel.g:6700:3: ()
                    // InternalOWLModel.g:6701:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getRDFSRangeAccess().getSimpleRDFSRangeAction_1_0_0(),
                                current);
                        

                    }

                    otherlv_2=(Token)match(input,32,FOLLOW_13); 

                        	newLeafNode(otherlv_2, grammarAccess.getRDFSRangeAccess().getRdfResourceKeyword_1_0_1());
                        
                    otherlv_3=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_3, grammarAccess.getRDFSRangeAccess().getEqualsSignKeyword_1_0_2());
                        
                    // InternalOWLModel.g:6714:1: ( (lv_range_4_0= ruleOwlRef ) )
                    // InternalOWLModel.g:6715:1: (lv_range_4_0= ruleOwlRef )
                    {
                    // InternalOWLModel.g:6715:1: (lv_range_4_0= ruleOwlRef )
                    // InternalOWLModel.g:6716:3: lv_range_4_0= ruleOwlRef
                    {
                     
                    	        newCompositeNode(grammarAccess.getRDFSRangeAccess().getRangeOwlRefParserRuleCall_1_0_3_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_range_4_0=ruleOwlRef();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getRDFSRangeRule());
                    	        }
                           		set(
                           			current, 
                           			"range",
                            		lv_range_4_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlRef");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // InternalOWLModel.g:6732:2: (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</rdfs:range' otherlv_8= '>' ) )
                    int alt124=2;
                    int LA124_0 = input.LA(1);

                    if ( (LA124_0==29) ) {
                        alt124=1;
                    }
                    else if ( (LA124_0==21) ) {
                        alt124=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 124, 0, input);

                        throw nvae;
                    }
                    switch (alt124) {
                        case 1 :
                            // InternalOWLModel.g:6732:4: otherlv_5= '/>'
                            {
                            otherlv_5=(Token)match(input,29,FOLLOW_2); 

                                	newLeafNode(otherlv_5, grammarAccess.getRDFSRangeAccess().getSolidusGreaterThanSignKeyword_1_0_4_0());
                                

                            }
                            break;
                        case 2 :
                            // InternalOWLModel.g:6737:6: (otherlv_6= '>' otherlv_7= '</rdfs:range' otherlv_8= '>' )
                            {
                            // InternalOWLModel.g:6737:6: (otherlv_6= '>' otherlv_7= '</rdfs:range' otherlv_8= '>' )
                            // InternalOWLModel.g:6737:8: otherlv_6= '>' otherlv_7= '</rdfs:range' otherlv_8= '>'
                            {
                            otherlv_6=(Token)match(input,21,FOLLOW_96); 

                                	newLeafNode(otherlv_6, grammarAccess.getRDFSRangeAccess().getGreaterThanSignKeyword_1_0_4_1_0());
                                
                            otherlv_7=(Token)match(input,115,FOLLOW_7); 

                                	newLeafNode(otherlv_7, grammarAccess.getRDFSRangeAccess().getRdfsRangeKeyword_1_0_4_1_1());
                                
                            otherlv_8=(Token)match(input,21,FOLLOW_2); 

                                	newLeafNode(otherlv_8, grammarAccess.getRDFSRangeAccess().getGreaterThanSignKeyword_1_0_4_1_2());
                                

                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:6750:6: ( () (otherlv_10= 'rdf:parseType' otherlv_11= '=' ( (lv_parseType_12_0= ruleParseType ) ) )? otherlv_13= '>' ( (lv_modifiers_14_0= ruleClassModifier ) )+ otherlv_15= '</rdfs:range' otherlv_16= '>' )
                    {
                    // InternalOWLModel.g:6750:6: ( () (otherlv_10= 'rdf:parseType' otherlv_11= '=' ( (lv_parseType_12_0= ruleParseType ) ) )? otherlv_13= '>' ( (lv_modifiers_14_0= ruleClassModifier ) )+ otherlv_15= '</rdfs:range' otherlv_16= '>' )
                    // InternalOWLModel.g:6750:7: () (otherlv_10= 'rdf:parseType' otherlv_11= '=' ( (lv_parseType_12_0= ruleParseType ) ) )? otherlv_13= '>' ( (lv_modifiers_14_0= ruleClassModifier ) )+ otherlv_15= '</rdfs:range' otherlv_16= '>'
                    {
                    // InternalOWLModel.g:6750:7: ()
                    // InternalOWLModel.g:6751:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getRDFSRangeAccess().getParsedPropertyRangeAction_1_1_0(),
                                current);
                        

                    }

                    // InternalOWLModel.g:6756:2: (otherlv_10= 'rdf:parseType' otherlv_11= '=' ( (lv_parseType_12_0= ruleParseType ) ) )?
                    int alt125=2;
                    int LA125_0 = input.LA(1);

                    if ( (LA125_0==44) ) {
                        alt125=1;
                    }
                    switch (alt125) {
                        case 1 :
                            // InternalOWLModel.g:6756:4: otherlv_10= 'rdf:parseType' otherlv_11= '=' ( (lv_parseType_12_0= ruleParseType ) )
                            {
                            otherlv_10=(Token)match(input,44,FOLLOW_13); 

                                	newLeafNode(otherlv_10, grammarAccess.getRDFSRangeAccess().getRdfParseTypeKeyword_1_1_1_0());
                                
                            otherlv_11=(Token)match(input,25,FOLLOW_9); 

                                	newLeafNode(otherlv_11, grammarAccess.getRDFSRangeAccess().getEqualsSignKeyword_1_1_1_1());
                                
                            // InternalOWLModel.g:6764:1: ( (lv_parseType_12_0= ruleParseType ) )
                            // InternalOWLModel.g:6765:1: (lv_parseType_12_0= ruleParseType )
                            {
                            // InternalOWLModel.g:6765:1: (lv_parseType_12_0= ruleParseType )
                            // InternalOWLModel.g:6766:3: lv_parseType_12_0= ruleParseType
                            {
                             
                            	        newCompositeNode(grammarAccess.getRDFSRangeAccess().getParseTypeParseTypeParserRuleCall_1_1_1_2_0()); 
                            	    
                            pushFollow(FOLLOW_7);
                            lv_parseType_12_0=ruleParseType();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getRDFSRangeRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"parseType",
                                    		lv_parseType_12_0, 
                                    		"org.smool.sdk.owlparser.OWLModel.ParseType");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }

                    otherlv_13=(Token)match(input,21,FOLLOW_39); 

                        	newLeafNode(otherlv_13, grammarAccess.getRDFSRangeAccess().getGreaterThanSignKeyword_1_1_2());
                        
                    // InternalOWLModel.g:6786:1: ( (lv_modifiers_14_0= ruleClassModifier ) )+
                    int cnt126=0;
                    loop126:
                    do {
                        int alt126=2;
                        int LA126_0 = input.LA(1);

                        if ( (LA126_0==39||LA126_0==70||LA126_0==122) ) {
                            alt126=1;
                        }


                        switch (alt126) {
                    	case 1 :
                    	    // InternalOWLModel.g:6787:1: (lv_modifiers_14_0= ruleClassModifier )
                    	    {
                    	    // InternalOWLModel.g:6787:1: (lv_modifiers_14_0= ruleClassModifier )
                    	    // InternalOWLModel.g:6788:3: lv_modifiers_14_0= ruleClassModifier
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getRDFSRangeAccess().getModifiersClassModifierParserRuleCall_1_1_3_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_97);
                    	    lv_modifiers_14_0=ruleClassModifier();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getRDFSRangeRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"modifiers",
                    	            		lv_modifiers_14_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.ClassModifier");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt126 >= 1 ) break loop126;
                                EarlyExitException eee =
                                    new EarlyExitException(126, input);
                                throw eee;
                        }
                        cnt126++;
                    } while (true);

                    otherlv_15=(Token)match(input,115,FOLLOW_7); 

                        	newLeafNode(otherlv_15, grammarAccess.getRDFSRangeAccess().getRdfsRangeKeyword_1_1_4());
                        
                    otherlv_16=(Token)match(input,21,FOLLOW_2); 

                        	newLeafNode(otherlv_16, grammarAccess.getRDFSRangeAccess().getGreaterThanSignKeyword_1_1_5());
                        

                    }


                    }
                    break;
                case 3 :
                    // InternalOWLModel.g:6813:6: ( () otherlv_18= '>' ( (lv_owlRange_19_0= ruleOwlDataRange ) ) otherlv_20= '</rdfs:range' otherlv_21= '>' )
                    {
                    // InternalOWLModel.g:6813:6: ( () otherlv_18= '>' ( (lv_owlRange_19_0= ruleOwlDataRange ) ) otherlv_20= '</rdfs:range' otherlv_21= '>' )
                    // InternalOWLModel.g:6813:7: () otherlv_18= '>' ( (lv_owlRange_19_0= ruleOwlDataRange ) ) otherlv_20= '</rdfs:range' otherlv_21= '>'
                    {
                    // InternalOWLModel.g:6813:7: ()
                    // InternalOWLModel.g:6814:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getRDFSRangeAccess().getOwlDataRangeAction_1_2_0(),
                                current);
                        

                    }

                    otherlv_18=(Token)match(input,21,FOLLOW_98); 

                        	newLeafNode(otherlv_18, grammarAccess.getRDFSRangeAccess().getGreaterThanSignKeyword_1_2_1());
                        
                    // InternalOWLModel.g:6823:1: ( (lv_owlRange_19_0= ruleOwlDataRange ) )
                    // InternalOWLModel.g:6824:1: (lv_owlRange_19_0= ruleOwlDataRange )
                    {
                    // InternalOWLModel.g:6824:1: (lv_owlRange_19_0= ruleOwlDataRange )
                    // InternalOWLModel.g:6825:3: lv_owlRange_19_0= ruleOwlDataRange
                    {
                     
                    	        newCompositeNode(grammarAccess.getRDFSRangeAccess().getOwlRangeOwlDataRangeParserRuleCall_1_2_2_0()); 
                    	    
                    pushFollow(FOLLOW_96);
                    lv_owlRange_19_0=ruleOwlDataRange();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getRDFSRangeRule());
                    	        }
                           		set(
                           			current, 
                           			"owlRange",
                            		lv_owlRange_19_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlDataRange");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_20=(Token)match(input,115,FOLLOW_7); 

                        	newLeafNode(otherlv_20, grammarAccess.getRDFSRangeAccess().getRdfsRangeKeyword_1_2_3());
                        
                    otherlv_21=(Token)match(input,21,FOLLOW_2); 

                        	newLeafNode(otherlv_21, grammarAccess.getRDFSRangeAccess().getGreaterThanSignKeyword_1_2_4());
                        

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRDFSRange"


    // $ANTLR start "entryRuleOwlDataRange"
    // InternalOWLModel.g:6857:1: entryRuleOwlDataRange returns [EObject current=null] : iv_ruleOwlDataRange= ruleOwlDataRange EOF ;
    public final EObject entryRuleOwlDataRange() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOwlDataRange = null;


        try {
            // InternalOWLModel.g:6858:2: (iv_ruleOwlDataRange= ruleOwlDataRange EOF )
            // InternalOWLModel.g:6859:2: iv_ruleOwlDataRange= ruleOwlDataRange EOF
            {
             newCompositeNode(grammarAccess.getOwlDataRangeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOwlDataRange=ruleOwlDataRange();

            state._fsp--;

             current =iv_ruleOwlDataRange; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOwlDataRange"


    // $ANTLR start "ruleOwlDataRange"
    // InternalOWLModel.g:6866:1: ruleOwlDataRange returns [EObject current=null] : (otherlv_0= '<owl:DataRange' otherlv_1= '>' ( ( (lv_union_2_0= ruleUnionAxiom ) ) | ( (lv_oneOf_3_0= ruleOneOfAxiom ) ) | ( (lv_intersection_4_0= ruleIntersectionAxiom ) ) ) otherlv_5= '</owl:DataRange' otherlv_6= '>' ) ;
    public final EObject ruleOwlDataRange() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        EObject lv_union_2_0 = null;

        EObject lv_oneOf_3_0 = null;

        EObject lv_intersection_4_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:6869:28: ( (otherlv_0= '<owl:DataRange' otherlv_1= '>' ( ( (lv_union_2_0= ruleUnionAxiom ) ) | ( (lv_oneOf_3_0= ruleOneOfAxiom ) ) | ( (lv_intersection_4_0= ruleIntersectionAxiom ) ) ) otherlv_5= '</owl:DataRange' otherlv_6= '>' ) )
            // InternalOWLModel.g:6870:1: (otherlv_0= '<owl:DataRange' otherlv_1= '>' ( ( (lv_union_2_0= ruleUnionAxiom ) ) | ( (lv_oneOf_3_0= ruleOneOfAxiom ) ) | ( (lv_intersection_4_0= ruleIntersectionAxiom ) ) ) otherlv_5= '</owl:DataRange' otherlv_6= '>' )
            {
            // InternalOWLModel.g:6870:1: (otherlv_0= '<owl:DataRange' otherlv_1= '>' ( ( (lv_union_2_0= ruleUnionAxiom ) ) | ( (lv_oneOf_3_0= ruleOneOfAxiom ) ) | ( (lv_intersection_4_0= ruleIntersectionAxiom ) ) ) otherlv_5= '</owl:DataRange' otherlv_6= '>' )
            // InternalOWLModel.g:6870:3: otherlv_0= '<owl:DataRange' otherlv_1= '>' ( ( (lv_union_2_0= ruleUnionAxiom ) ) | ( (lv_oneOf_3_0= ruleOneOfAxiom ) ) | ( (lv_intersection_4_0= ruleIntersectionAxiom ) ) ) otherlv_5= '</owl:DataRange' otherlv_6= '>'
            {
            otherlv_0=(Token)match(input,116,FOLLOW_7); 

                	newLeafNode(otherlv_0, grammarAccess.getOwlDataRangeAccess().getOwlDataRangeKeyword_0());
                
            otherlv_1=(Token)match(input,21,FOLLOW_99); 

                	newLeafNode(otherlv_1, grammarAccess.getOwlDataRangeAccess().getGreaterThanSignKeyword_1());
                
            // InternalOWLModel.g:6878:1: ( ( (lv_union_2_0= ruleUnionAxiom ) ) | ( (lv_oneOf_3_0= ruleOneOfAxiom ) ) | ( (lv_intersection_4_0= ruleIntersectionAxiom ) ) )
            int alt128=3;
            switch ( input.LA(1) ) {
            case 51:
                {
                alt128=1;
                }
                break;
            case 55:
                {
                alt128=2;
                }
                break;
            case 53:
                {
                alt128=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 128, 0, input);

                throw nvae;
            }

            switch (alt128) {
                case 1 :
                    // InternalOWLModel.g:6878:2: ( (lv_union_2_0= ruleUnionAxiom ) )
                    {
                    // InternalOWLModel.g:6878:2: ( (lv_union_2_0= ruleUnionAxiom ) )
                    // InternalOWLModel.g:6879:1: (lv_union_2_0= ruleUnionAxiom )
                    {
                    // InternalOWLModel.g:6879:1: (lv_union_2_0= ruleUnionAxiom )
                    // InternalOWLModel.g:6880:3: lv_union_2_0= ruleUnionAxiom
                    {
                     
                    	        newCompositeNode(grammarAccess.getOwlDataRangeAccess().getUnionUnionAxiomParserRuleCall_2_0_0()); 
                    	    
                    pushFollow(FOLLOW_100);
                    lv_union_2_0=ruleUnionAxiom();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getOwlDataRangeRule());
                    	        }
                           		set(
                           			current, 
                           			"union",
                            		lv_union_2_0, 
                            		"org.smool.sdk.owlparser.OWLModel.UnionAxiom");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:6897:6: ( (lv_oneOf_3_0= ruleOneOfAxiom ) )
                    {
                    // InternalOWLModel.g:6897:6: ( (lv_oneOf_3_0= ruleOneOfAxiom ) )
                    // InternalOWLModel.g:6898:1: (lv_oneOf_3_0= ruleOneOfAxiom )
                    {
                    // InternalOWLModel.g:6898:1: (lv_oneOf_3_0= ruleOneOfAxiom )
                    // InternalOWLModel.g:6899:3: lv_oneOf_3_0= ruleOneOfAxiom
                    {
                     
                    	        newCompositeNode(grammarAccess.getOwlDataRangeAccess().getOneOfOneOfAxiomParserRuleCall_2_1_0()); 
                    	    
                    pushFollow(FOLLOW_100);
                    lv_oneOf_3_0=ruleOneOfAxiom();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getOwlDataRangeRule());
                    	        }
                           		set(
                           			current, 
                           			"oneOf",
                            		lv_oneOf_3_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OneOfAxiom");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalOWLModel.g:6916:6: ( (lv_intersection_4_0= ruleIntersectionAxiom ) )
                    {
                    // InternalOWLModel.g:6916:6: ( (lv_intersection_4_0= ruleIntersectionAxiom ) )
                    // InternalOWLModel.g:6917:1: (lv_intersection_4_0= ruleIntersectionAxiom )
                    {
                    // InternalOWLModel.g:6917:1: (lv_intersection_4_0= ruleIntersectionAxiom )
                    // InternalOWLModel.g:6918:3: lv_intersection_4_0= ruleIntersectionAxiom
                    {
                     
                    	        newCompositeNode(grammarAccess.getOwlDataRangeAccess().getIntersectionIntersectionAxiomParserRuleCall_2_2_0()); 
                    	    
                    pushFollow(FOLLOW_100);
                    lv_intersection_4_0=ruleIntersectionAxiom();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getOwlDataRangeRule());
                    	        }
                           		set(
                           			current, 
                           			"intersection",
                            		lv_intersection_4_0, 
                            		"org.smool.sdk.owlparser.OWLModel.IntersectionAxiom");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            otherlv_5=(Token)match(input,117,FOLLOW_7); 

                	newLeafNode(otherlv_5, grammarAccess.getOwlDataRangeAccess().getOwlDataRangeKeyword_3());
                
            otherlv_6=(Token)match(input,21,FOLLOW_2); 

                	newLeafNode(otherlv_6, grammarAccess.getOwlDataRangeAccess().getGreaterThanSignKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOwlDataRange"


    // $ANTLR start "entryRuleRDFType"
    // InternalOWLModel.g:6950:1: entryRuleRDFType returns [EObject current=null] : iv_ruleRDFType= ruleRDFType EOF ;
    public final EObject entryRuleRDFType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRDFType = null;


        try {
            // InternalOWLModel.g:6951:2: (iv_ruleRDFType= ruleRDFType EOF )
            // InternalOWLModel.g:6952:2: iv_ruleRDFType= ruleRDFType EOF
            {
             newCompositeNode(grammarAccess.getRDFTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRDFType=ruleRDFType();

            state._fsp--;

             current =iv_ruleRDFType; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRDFType"


    // $ANTLR start "ruleRDFType"
    // InternalOWLModel.g:6959:1: ruleRDFType returns [EObject current=null] : (otherlv_0= '<rdf:type' otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_type_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</rdf:type' otherlv_7= '>' ) ) ) ;
    public final EObject ruleRDFType() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        EObject lv_type_3_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:6962:28: ( (otherlv_0= '<rdf:type' otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_type_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</rdf:type' otherlv_7= '>' ) ) ) )
            // InternalOWLModel.g:6963:1: (otherlv_0= '<rdf:type' otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_type_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</rdf:type' otherlv_7= '>' ) ) )
            {
            // InternalOWLModel.g:6963:1: (otherlv_0= '<rdf:type' otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_type_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</rdf:type' otherlv_7= '>' ) ) )
            // InternalOWLModel.g:6963:3: otherlv_0= '<rdf:type' otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_type_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</rdf:type' otherlv_7= '>' ) )
            {
            otherlv_0=(Token)match(input,118,FOLLOW_28); 

                	newLeafNode(otherlv_0, grammarAccess.getRDFTypeAccess().getRdfTypeKeyword_0());
                
            otherlv_1=(Token)match(input,32,FOLLOW_13); 

                	newLeafNode(otherlv_1, grammarAccess.getRDFTypeAccess().getRdfResourceKeyword_1());
                
            otherlv_2=(Token)match(input,25,FOLLOW_9); 

                	newLeafNode(otherlv_2, grammarAccess.getRDFTypeAccess().getEqualsSignKeyword_2());
                
            // InternalOWLModel.g:6975:1: ( (lv_type_3_0= ruleOwlRef ) )
            // InternalOWLModel.g:6976:1: (lv_type_3_0= ruleOwlRef )
            {
            // InternalOWLModel.g:6976:1: (lv_type_3_0= ruleOwlRef )
            // InternalOWLModel.g:6977:3: lv_type_3_0= ruleOwlRef
            {
             
            	        newCompositeNode(grammarAccess.getRDFTypeAccess().getTypeOwlRefParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_24);
            lv_type_3_0=ruleOwlRef();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getRDFTypeRule());
            	        }
                   		set(
                   			current, 
                   			"type",
                    		lv_type_3_0, 
                    		"org.smool.sdk.owlparser.OWLModel.OwlRef");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalOWLModel.g:6993:2: (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</rdf:type' otherlv_7= '>' ) )
            int alt129=2;
            int LA129_0 = input.LA(1);

            if ( (LA129_0==29) ) {
                alt129=1;
            }
            else if ( (LA129_0==21) ) {
                alt129=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 129, 0, input);

                throw nvae;
            }
            switch (alt129) {
                case 1 :
                    // InternalOWLModel.g:6993:4: otherlv_4= '/>'
                    {
                    otherlv_4=(Token)match(input,29,FOLLOW_2); 

                        	newLeafNode(otherlv_4, grammarAccess.getRDFTypeAccess().getSolidusGreaterThanSignKeyword_4_0());
                        

                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:6998:6: (otherlv_5= '>' otherlv_6= '</rdf:type' otherlv_7= '>' )
                    {
                    // InternalOWLModel.g:6998:6: (otherlv_5= '>' otherlv_6= '</rdf:type' otherlv_7= '>' )
                    // InternalOWLModel.g:6998:8: otherlv_5= '>' otherlv_6= '</rdf:type' otherlv_7= '>'
                    {
                    otherlv_5=(Token)match(input,21,FOLLOW_101); 

                        	newLeafNode(otherlv_5, grammarAccess.getRDFTypeAccess().getGreaterThanSignKeyword_4_1_0());
                        
                    otherlv_6=(Token)match(input,119,FOLLOW_7); 

                        	newLeafNode(otherlv_6, grammarAccess.getRDFTypeAccess().getRdfTypeKeyword_4_1_1());
                        
                    otherlv_7=(Token)match(input,21,FOLLOW_2); 

                        	newLeafNode(otherlv_7, grammarAccess.getRDFTypeAccess().getGreaterThanSignKeyword_4_1_2());
                        

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRDFType"


    // $ANTLR start "entryRuleSubPropertyOf"
    // InternalOWLModel.g:7018:1: entryRuleSubPropertyOf returns [EObject current=null] : iv_ruleSubPropertyOf= ruleSubPropertyOf EOF ;
    public final EObject entryRuleSubPropertyOf() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSubPropertyOf = null;


        try {
            // InternalOWLModel.g:7019:2: (iv_ruleSubPropertyOf= ruleSubPropertyOf EOF )
            // InternalOWLModel.g:7020:2: iv_ruleSubPropertyOf= ruleSubPropertyOf EOF
            {
             newCompositeNode(grammarAccess.getSubPropertyOfRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSubPropertyOf=ruleSubPropertyOf();

            state._fsp--;

             current =iv_ruleSubPropertyOf; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSubPropertyOf"


    // $ANTLR start "ruleSubPropertyOf"
    // InternalOWLModel.g:7027:1: ruleSubPropertyOf returns [EObject current=null] : (otherlv_0= '<rdfs:subPropertyOf' ( (otherlv_1= '>' ( (lv_props_2_0= ruleProperty ) )+ otherlv_3= '</rdfs:subPropertyOf' otherlv_4= '>' ) | (otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_type_7_0= ruleOwlRef ) ) (otherlv_8= '/>' | (otherlv_9= '>' otherlv_10= '</rdfs:subPropertyOf' otherlv_11= '>' ) ) ) ) ) ;
    public final EObject ruleSubPropertyOf() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        EObject lv_props_2_0 = null;

        EObject lv_type_7_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:7030:28: ( (otherlv_0= '<rdfs:subPropertyOf' ( (otherlv_1= '>' ( (lv_props_2_0= ruleProperty ) )+ otherlv_3= '</rdfs:subPropertyOf' otherlv_4= '>' ) | (otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_type_7_0= ruleOwlRef ) ) (otherlv_8= '/>' | (otherlv_9= '>' otherlv_10= '</rdfs:subPropertyOf' otherlv_11= '>' ) ) ) ) ) )
            // InternalOWLModel.g:7031:1: (otherlv_0= '<rdfs:subPropertyOf' ( (otherlv_1= '>' ( (lv_props_2_0= ruleProperty ) )+ otherlv_3= '</rdfs:subPropertyOf' otherlv_4= '>' ) | (otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_type_7_0= ruleOwlRef ) ) (otherlv_8= '/>' | (otherlv_9= '>' otherlv_10= '</rdfs:subPropertyOf' otherlv_11= '>' ) ) ) ) )
            {
            // InternalOWLModel.g:7031:1: (otherlv_0= '<rdfs:subPropertyOf' ( (otherlv_1= '>' ( (lv_props_2_0= ruleProperty ) )+ otherlv_3= '</rdfs:subPropertyOf' otherlv_4= '>' ) | (otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_type_7_0= ruleOwlRef ) ) (otherlv_8= '/>' | (otherlv_9= '>' otherlv_10= '</rdfs:subPropertyOf' otherlv_11= '>' ) ) ) ) )
            // InternalOWLModel.g:7031:3: otherlv_0= '<rdfs:subPropertyOf' ( (otherlv_1= '>' ( (lv_props_2_0= ruleProperty ) )+ otherlv_3= '</rdfs:subPropertyOf' otherlv_4= '>' ) | (otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_type_7_0= ruleOwlRef ) ) (otherlv_8= '/>' | (otherlv_9= '>' otherlv_10= '</rdfs:subPropertyOf' otherlv_11= '>' ) ) ) )
            {
            otherlv_0=(Token)match(input,120,FOLLOW_30); 

                	newLeafNode(otherlv_0, grammarAccess.getSubPropertyOfAccess().getRdfsSubPropertyOfKeyword_0());
                
            // InternalOWLModel.g:7035:1: ( (otherlv_1= '>' ( (lv_props_2_0= ruleProperty ) )+ otherlv_3= '</rdfs:subPropertyOf' otherlv_4= '>' ) | (otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_type_7_0= ruleOwlRef ) ) (otherlv_8= '/>' | (otherlv_9= '>' otherlv_10= '</rdfs:subPropertyOf' otherlv_11= '>' ) ) ) )
            int alt132=2;
            int LA132_0 = input.LA(1);

            if ( (LA132_0==21) ) {
                alt132=1;
            }
            else if ( (LA132_0==32) ) {
                alt132=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 132, 0, input);

                throw nvae;
            }
            switch (alt132) {
                case 1 :
                    // InternalOWLModel.g:7035:2: (otherlv_1= '>' ( (lv_props_2_0= ruleProperty ) )+ otherlv_3= '</rdfs:subPropertyOf' otherlv_4= '>' )
                    {
                    // InternalOWLModel.g:7035:2: (otherlv_1= '>' ( (lv_props_2_0= ruleProperty ) )+ otherlv_3= '</rdfs:subPropertyOf' otherlv_4= '>' )
                    // InternalOWLModel.g:7035:4: otherlv_1= '>' ( (lv_props_2_0= ruleProperty ) )+ otherlv_3= '</rdfs:subPropertyOf' otherlv_4= '>'
                    {
                    otherlv_1=(Token)match(input,21,FOLLOW_67); 

                        	newLeafNode(otherlv_1, grammarAccess.getSubPropertyOfAccess().getGreaterThanSignKeyword_1_0_0());
                        
                    // InternalOWLModel.g:7039:1: ( (lv_props_2_0= ruleProperty ) )+
                    int cnt130=0;
                    loop130:
                    do {
                        int alt130=2;
                        int LA130_0 = input.LA(1);

                        if ( (LA130_0==94||LA130_0==96||LA130_0==98||LA130_0==100||LA130_0==102||LA130_0==104||LA130_0==106) ) {
                            alt130=1;
                        }


                        switch (alt130) {
                    	case 1 :
                    	    // InternalOWLModel.g:7040:1: (lv_props_2_0= ruleProperty )
                    	    {
                    	    // InternalOWLModel.g:7040:1: (lv_props_2_0= ruleProperty )
                    	    // InternalOWLModel.g:7041:3: lv_props_2_0= ruleProperty
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getSubPropertyOfAccess().getPropsPropertyParserRuleCall_1_0_1_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_102);
                    	    lv_props_2_0=ruleProperty();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getSubPropertyOfRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"props",
                    	            		lv_props_2_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.Property");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt130 >= 1 ) break loop130;
                                EarlyExitException eee =
                                    new EarlyExitException(130, input);
                                throw eee;
                        }
                        cnt130++;
                    } while (true);

                    otherlv_3=(Token)match(input,121,FOLLOW_7); 

                        	newLeafNode(otherlv_3, grammarAccess.getSubPropertyOfAccess().getRdfsSubPropertyOfKeyword_1_0_2());
                        
                    otherlv_4=(Token)match(input,21,FOLLOW_2); 

                        	newLeafNode(otherlv_4, grammarAccess.getSubPropertyOfAccess().getGreaterThanSignKeyword_1_0_3());
                        

                    }


                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:7066:6: (otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_type_7_0= ruleOwlRef ) ) (otherlv_8= '/>' | (otherlv_9= '>' otherlv_10= '</rdfs:subPropertyOf' otherlv_11= '>' ) ) )
                    {
                    // InternalOWLModel.g:7066:6: (otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_type_7_0= ruleOwlRef ) ) (otherlv_8= '/>' | (otherlv_9= '>' otherlv_10= '</rdfs:subPropertyOf' otherlv_11= '>' ) ) )
                    // InternalOWLModel.g:7066:8: otherlv_5= 'rdf:resource' otherlv_6= '=' ( (lv_type_7_0= ruleOwlRef ) ) (otherlv_8= '/>' | (otherlv_9= '>' otherlv_10= '</rdfs:subPropertyOf' otherlv_11= '>' ) )
                    {
                    otherlv_5=(Token)match(input,32,FOLLOW_13); 

                        	newLeafNode(otherlv_5, grammarAccess.getSubPropertyOfAccess().getRdfResourceKeyword_1_1_0());
                        
                    otherlv_6=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_6, grammarAccess.getSubPropertyOfAccess().getEqualsSignKeyword_1_1_1());
                        
                    // InternalOWLModel.g:7074:1: ( (lv_type_7_0= ruleOwlRef ) )
                    // InternalOWLModel.g:7075:1: (lv_type_7_0= ruleOwlRef )
                    {
                    // InternalOWLModel.g:7075:1: (lv_type_7_0= ruleOwlRef )
                    // InternalOWLModel.g:7076:3: lv_type_7_0= ruleOwlRef
                    {
                     
                    	        newCompositeNode(grammarAccess.getSubPropertyOfAccess().getTypeOwlRefParserRuleCall_1_1_2_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_type_7_0=ruleOwlRef();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getSubPropertyOfRule());
                    	        }
                           		set(
                           			current, 
                           			"type",
                            		lv_type_7_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlRef");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // InternalOWLModel.g:7092:2: (otherlv_8= '/>' | (otherlv_9= '>' otherlv_10= '</rdfs:subPropertyOf' otherlv_11= '>' ) )
                    int alt131=2;
                    int LA131_0 = input.LA(1);

                    if ( (LA131_0==29) ) {
                        alt131=1;
                    }
                    else if ( (LA131_0==21) ) {
                        alt131=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 131, 0, input);

                        throw nvae;
                    }
                    switch (alt131) {
                        case 1 :
                            // InternalOWLModel.g:7092:4: otherlv_8= '/>'
                            {
                            otherlv_8=(Token)match(input,29,FOLLOW_2); 

                                	newLeafNode(otherlv_8, grammarAccess.getSubPropertyOfAccess().getSolidusGreaterThanSignKeyword_1_1_3_0());
                                

                            }
                            break;
                        case 2 :
                            // InternalOWLModel.g:7097:6: (otherlv_9= '>' otherlv_10= '</rdfs:subPropertyOf' otherlv_11= '>' )
                            {
                            // InternalOWLModel.g:7097:6: (otherlv_9= '>' otherlv_10= '</rdfs:subPropertyOf' otherlv_11= '>' )
                            // InternalOWLModel.g:7097:8: otherlv_9= '>' otherlv_10= '</rdfs:subPropertyOf' otherlv_11= '>'
                            {
                            otherlv_9=(Token)match(input,21,FOLLOW_103); 

                                	newLeafNode(otherlv_9, grammarAccess.getSubPropertyOfAccess().getGreaterThanSignKeyword_1_1_3_1_0());
                                
                            otherlv_10=(Token)match(input,121,FOLLOW_7); 

                                	newLeafNode(otherlv_10, grammarAccess.getSubPropertyOfAccess().getRdfsSubPropertyOfKeyword_1_1_3_1_1());
                                
                            otherlv_11=(Token)match(input,21,FOLLOW_2); 

                                	newLeafNode(otherlv_11, grammarAccess.getSubPropertyOfAccess().getGreaterThanSignKeyword_1_1_3_1_2());
                                

                            }


                            }
                            break;

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSubPropertyOf"


    // $ANTLR start "entryRuleDescription"
    // InternalOWLModel.g:7117:1: entryRuleDescription returns [EObject current=null] : iv_ruleDescription= ruleDescription EOF ;
    public final EObject entryRuleDescription() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDescription = null;


        try {
            // InternalOWLModel.g:7118:2: (iv_ruleDescription= ruleDescription EOF )
            // InternalOWLModel.g:7119:2: iv_ruleDescription= ruleDescription EOF
            {
             newCompositeNode(grammarAccess.getDescriptionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDescription=ruleDescription();

            state._fsp--;

             current =iv_ruleDescription; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDescription"


    // $ANTLR start "ruleDescription"
    // InternalOWLModel.g:7126:1: ruleDescription returns [EObject current=null] : ( () otherlv_1= '<rdf:Description' (otherlv_2= 'rdf:about' otherlv_3= '=' ( (lv_about_4_0= ruleOwlRef ) ) )? (otherlv_5= '/>' | (otherlv_6= '>' ( ( (lv_first_7_0= ruleOwlFirst ) ) | ( (lv_type_8_0= ruleOwlType ) ) | ( (lv_domain_9_0= ruleRDFSDomain ) ) | ( (lv_range_10_0= ruleRDFSRange ) ) | ( (lv_propModifiers_11_0= rulePropertyModifier ) ) | ( (lv_labels_12_0= ruleOwlLabel ) ) | ( (lv_axioms_13_0= ruleClassAxiom ) ) )* otherlv_14= '</rdf:Description' otherlv_15= '>' ) ) ) ;
    public final EObject ruleDescription() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        EObject lv_about_4_0 = null;

        EObject lv_first_7_0 = null;

        EObject lv_type_8_0 = null;

        EObject lv_domain_9_0 = null;

        EObject lv_range_10_0 = null;

        EObject lv_propModifiers_11_0 = null;

        EObject lv_labels_12_0 = null;

        EObject lv_axioms_13_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:7129:28: ( ( () otherlv_1= '<rdf:Description' (otherlv_2= 'rdf:about' otherlv_3= '=' ( (lv_about_4_0= ruleOwlRef ) ) )? (otherlv_5= '/>' | (otherlv_6= '>' ( ( (lv_first_7_0= ruleOwlFirst ) ) | ( (lv_type_8_0= ruleOwlType ) ) | ( (lv_domain_9_0= ruleRDFSDomain ) ) | ( (lv_range_10_0= ruleRDFSRange ) ) | ( (lv_propModifiers_11_0= rulePropertyModifier ) ) | ( (lv_labels_12_0= ruleOwlLabel ) ) | ( (lv_axioms_13_0= ruleClassAxiom ) ) )* otherlv_14= '</rdf:Description' otherlv_15= '>' ) ) ) )
            // InternalOWLModel.g:7130:1: ( () otherlv_1= '<rdf:Description' (otherlv_2= 'rdf:about' otherlv_3= '=' ( (lv_about_4_0= ruleOwlRef ) ) )? (otherlv_5= '/>' | (otherlv_6= '>' ( ( (lv_first_7_0= ruleOwlFirst ) ) | ( (lv_type_8_0= ruleOwlType ) ) | ( (lv_domain_9_0= ruleRDFSDomain ) ) | ( (lv_range_10_0= ruleRDFSRange ) ) | ( (lv_propModifiers_11_0= rulePropertyModifier ) ) | ( (lv_labels_12_0= ruleOwlLabel ) ) | ( (lv_axioms_13_0= ruleClassAxiom ) ) )* otherlv_14= '</rdf:Description' otherlv_15= '>' ) ) )
            {
            // InternalOWLModel.g:7130:1: ( () otherlv_1= '<rdf:Description' (otherlv_2= 'rdf:about' otherlv_3= '=' ( (lv_about_4_0= ruleOwlRef ) ) )? (otherlv_5= '/>' | (otherlv_6= '>' ( ( (lv_first_7_0= ruleOwlFirst ) ) | ( (lv_type_8_0= ruleOwlType ) ) | ( (lv_domain_9_0= ruleRDFSDomain ) ) | ( (lv_range_10_0= ruleRDFSRange ) ) | ( (lv_propModifiers_11_0= rulePropertyModifier ) ) | ( (lv_labels_12_0= ruleOwlLabel ) ) | ( (lv_axioms_13_0= ruleClassAxiom ) ) )* otherlv_14= '</rdf:Description' otherlv_15= '>' ) ) )
            // InternalOWLModel.g:7130:2: () otherlv_1= '<rdf:Description' (otherlv_2= 'rdf:about' otherlv_3= '=' ( (lv_about_4_0= ruleOwlRef ) ) )? (otherlv_5= '/>' | (otherlv_6= '>' ( ( (lv_first_7_0= ruleOwlFirst ) ) | ( (lv_type_8_0= ruleOwlType ) ) | ( (lv_domain_9_0= ruleRDFSDomain ) ) | ( (lv_range_10_0= ruleRDFSRange ) ) | ( (lv_propModifiers_11_0= rulePropertyModifier ) ) | ( (lv_labels_12_0= ruleOwlLabel ) ) | ( (lv_axioms_13_0= ruleClassAxiom ) ) )* otherlv_14= '</rdf:Description' otherlv_15= '>' ) )
            {
            // InternalOWLModel.g:7130:2: ()
            // InternalOWLModel.g:7131:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getDescriptionAccess().getDescriptionAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,122,FOLLOW_23); 

                	newLeafNode(otherlv_1, grammarAccess.getDescriptionAccess().getRdfDescriptionKeyword_1());
                
            // InternalOWLModel.g:7140:1: (otherlv_2= 'rdf:about' otherlv_3= '=' ( (lv_about_4_0= ruleOwlRef ) ) )?
            int alt133=2;
            int LA133_0 = input.LA(1);

            if ( (LA133_0==28) ) {
                alt133=1;
            }
            switch (alt133) {
                case 1 :
                    // InternalOWLModel.g:7140:3: otherlv_2= 'rdf:about' otherlv_3= '=' ( (lv_about_4_0= ruleOwlRef ) )
                    {
                    otherlv_2=(Token)match(input,28,FOLLOW_13); 

                        	newLeafNode(otherlv_2, grammarAccess.getDescriptionAccess().getRdfAboutKeyword_2_0());
                        
                    otherlv_3=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_3, grammarAccess.getDescriptionAccess().getEqualsSignKeyword_2_1());
                        
                    // InternalOWLModel.g:7148:1: ( (lv_about_4_0= ruleOwlRef ) )
                    // InternalOWLModel.g:7149:1: (lv_about_4_0= ruleOwlRef )
                    {
                    // InternalOWLModel.g:7149:1: (lv_about_4_0= ruleOwlRef )
                    // InternalOWLModel.g:7150:3: lv_about_4_0= ruleOwlRef
                    {
                     
                    	        newCompositeNode(grammarAccess.getDescriptionAccess().getAboutOwlRefParserRuleCall_2_2_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_about_4_0=ruleOwlRef();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getDescriptionRule());
                    	        }
                           		set(
                           			current, 
                           			"about",
                            		lv_about_4_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlRef");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            // InternalOWLModel.g:7166:4: (otherlv_5= '/>' | (otherlv_6= '>' ( ( (lv_first_7_0= ruleOwlFirst ) ) | ( (lv_type_8_0= ruleOwlType ) ) | ( (lv_domain_9_0= ruleRDFSDomain ) ) | ( (lv_range_10_0= ruleRDFSRange ) ) | ( (lv_propModifiers_11_0= rulePropertyModifier ) ) | ( (lv_labels_12_0= ruleOwlLabel ) ) | ( (lv_axioms_13_0= ruleClassAxiom ) ) )* otherlv_14= '</rdf:Description' otherlv_15= '>' ) )
            int alt135=2;
            int LA135_0 = input.LA(1);

            if ( (LA135_0==29) ) {
                alt135=1;
            }
            else if ( (LA135_0==21) ) {
                alt135=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 135, 0, input);

                throw nvae;
            }
            switch (alt135) {
                case 1 :
                    // InternalOWLModel.g:7166:6: otherlv_5= '/>'
                    {
                    otherlv_5=(Token)match(input,29,FOLLOW_2); 

                        	newLeafNode(otherlv_5, grammarAccess.getDescriptionAccess().getSolidusGreaterThanSignKeyword_3_0());
                        

                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:7171:6: (otherlv_6= '>' ( ( (lv_first_7_0= ruleOwlFirst ) ) | ( (lv_type_8_0= ruleOwlType ) ) | ( (lv_domain_9_0= ruleRDFSDomain ) ) | ( (lv_range_10_0= ruleRDFSRange ) ) | ( (lv_propModifiers_11_0= rulePropertyModifier ) ) | ( (lv_labels_12_0= ruleOwlLabel ) ) | ( (lv_axioms_13_0= ruleClassAxiom ) ) )* otherlv_14= '</rdf:Description' otherlv_15= '>' )
                    {
                    // InternalOWLModel.g:7171:6: (otherlv_6= '>' ( ( (lv_first_7_0= ruleOwlFirst ) ) | ( (lv_type_8_0= ruleOwlType ) ) | ( (lv_domain_9_0= ruleRDFSDomain ) ) | ( (lv_range_10_0= ruleRDFSRange ) ) | ( (lv_propModifiers_11_0= rulePropertyModifier ) ) | ( (lv_labels_12_0= ruleOwlLabel ) ) | ( (lv_axioms_13_0= ruleClassAxiom ) ) )* otherlv_14= '</rdf:Description' otherlv_15= '>' )
                    // InternalOWLModel.g:7171:8: otherlv_6= '>' ( ( (lv_first_7_0= ruleOwlFirst ) ) | ( (lv_type_8_0= ruleOwlType ) ) | ( (lv_domain_9_0= ruleRDFSDomain ) ) | ( (lv_range_10_0= ruleRDFSRange ) ) | ( (lv_propModifiers_11_0= rulePropertyModifier ) ) | ( (lv_labels_12_0= ruleOwlLabel ) ) | ( (lv_axioms_13_0= ruleClassAxiom ) ) )* otherlv_14= '</rdf:Description' otherlv_15= '>'
                    {
                    otherlv_6=(Token)match(input,21,FOLLOW_104); 

                        	newLeafNode(otherlv_6, grammarAccess.getDescriptionAccess().getGreaterThanSignKeyword_3_1_0());
                        
                    // InternalOWLModel.g:7175:1: ( ( (lv_first_7_0= ruleOwlFirst ) ) | ( (lv_type_8_0= ruleOwlType ) ) | ( (lv_domain_9_0= ruleRDFSDomain ) ) | ( (lv_range_10_0= ruleRDFSRange ) ) | ( (lv_propModifiers_11_0= rulePropertyModifier ) ) | ( (lv_labels_12_0= ruleOwlLabel ) ) | ( (lv_axioms_13_0= ruleClassAxiom ) ) )*
                    loop134:
                    do {
                        int alt134=8;
                        switch ( input.LA(1) ) {
                        case 124:
                            {
                            alt134=1;
                            }
                            break;
                        case 118:
                            {
                            alt134=2;
                            }
                            break;
                        case 112:
                            {
                            alt134=3;
                            }
                            break;
                        case 114:
                            {
                            alt134=4;
                            }
                            break;
                        case 108:
                        case 110:
                        case 120:
                            {
                            alt134=5;
                            }
                            break;
                        case 60:
                            {
                            alt134=6;
                            }
                            break;
                        case 42:
                        case 45:
                        case 47:
                        case 49:
                        case 51:
                        case 53:
                        case 55:
                            {
                            alt134=7;
                            }
                            break;

                        }

                        switch (alt134) {
                    	case 1 :
                    	    // InternalOWLModel.g:7175:2: ( (lv_first_7_0= ruleOwlFirst ) )
                    	    {
                    	    // InternalOWLModel.g:7175:2: ( (lv_first_7_0= ruleOwlFirst ) )
                    	    // InternalOWLModel.g:7176:1: (lv_first_7_0= ruleOwlFirst )
                    	    {
                    	    // InternalOWLModel.g:7176:1: (lv_first_7_0= ruleOwlFirst )
                    	    // InternalOWLModel.g:7177:3: lv_first_7_0= ruleOwlFirst
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getDescriptionAccess().getFirstOwlFirstParserRuleCall_3_1_1_0_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_104);
                    	    lv_first_7_0=ruleOwlFirst();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getDescriptionRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"first",
                    	            		lv_first_7_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.OwlFirst");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalOWLModel.g:7194:6: ( (lv_type_8_0= ruleOwlType ) )
                    	    {
                    	    // InternalOWLModel.g:7194:6: ( (lv_type_8_0= ruleOwlType ) )
                    	    // InternalOWLModel.g:7195:1: (lv_type_8_0= ruleOwlType )
                    	    {
                    	    // InternalOWLModel.g:7195:1: (lv_type_8_0= ruleOwlType )
                    	    // InternalOWLModel.g:7196:3: lv_type_8_0= ruleOwlType
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getDescriptionAccess().getTypeOwlTypeParserRuleCall_3_1_1_1_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_104);
                    	    lv_type_8_0=ruleOwlType();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getDescriptionRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"type",
                    	            		lv_type_8_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.OwlType");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 3 :
                    	    // InternalOWLModel.g:7213:6: ( (lv_domain_9_0= ruleRDFSDomain ) )
                    	    {
                    	    // InternalOWLModel.g:7213:6: ( (lv_domain_9_0= ruleRDFSDomain ) )
                    	    // InternalOWLModel.g:7214:1: (lv_domain_9_0= ruleRDFSDomain )
                    	    {
                    	    // InternalOWLModel.g:7214:1: (lv_domain_9_0= ruleRDFSDomain )
                    	    // InternalOWLModel.g:7215:3: lv_domain_9_0= ruleRDFSDomain
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getDescriptionAccess().getDomainRDFSDomainParserRuleCall_3_1_1_2_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_104);
                    	    lv_domain_9_0=ruleRDFSDomain();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getDescriptionRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"domain",
                    	            		lv_domain_9_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.RDFSDomain");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 4 :
                    	    // InternalOWLModel.g:7232:6: ( (lv_range_10_0= ruleRDFSRange ) )
                    	    {
                    	    // InternalOWLModel.g:7232:6: ( (lv_range_10_0= ruleRDFSRange ) )
                    	    // InternalOWLModel.g:7233:1: (lv_range_10_0= ruleRDFSRange )
                    	    {
                    	    // InternalOWLModel.g:7233:1: (lv_range_10_0= ruleRDFSRange )
                    	    // InternalOWLModel.g:7234:3: lv_range_10_0= ruleRDFSRange
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getDescriptionAccess().getRangeRDFSRangeParserRuleCall_3_1_1_3_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_104);
                    	    lv_range_10_0=ruleRDFSRange();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getDescriptionRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"range",
                    	            		lv_range_10_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.RDFSRange");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 5 :
                    	    // InternalOWLModel.g:7251:6: ( (lv_propModifiers_11_0= rulePropertyModifier ) )
                    	    {
                    	    // InternalOWLModel.g:7251:6: ( (lv_propModifiers_11_0= rulePropertyModifier ) )
                    	    // InternalOWLModel.g:7252:1: (lv_propModifiers_11_0= rulePropertyModifier )
                    	    {
                    	    // InternalOWLModel.g:7252:1: (lv_propModifiers_11_0= rulePropertyModifier )
                    	    // InternalOWLModel.g:7253:3: lv_propModifiers_11_0= rulePropertyModifier
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getDescriptionAccess().getPropModifiersPropertyModifierParserRuleCall_3_1_1_4_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_104);
                    	    lv_propModifiers_11_0=rulePropertyModifier();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getDescriptionRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"propModifiers",
                    	            		lv_propModifiers_11_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.PropertyModifier");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 6 :
                    	    // InternalOWLModel.g:7270:6: ( (lv_labels_12_0= ruleOwlLabel ) )
                    	    {
                    	    // InternalOWLModel.g:7270:6: ( (lv_labels_12_0= ruleOwlLabel ) )
                    	    // InternalOWLModel.g:7271:1: (lv_labels_12_0= ruleOwlLabel )
                    	    {
                    	    // InternalOWLModel.g:7271:1: (lv_labels_12_0= ruleOwlLabel )
                    	    // InternalOWLModel.g:7272:3: lv_labels_12_0= ruleOwlLabel
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getDescriptionAccess().getLabelsOwlLabelParserRuleCall_3_1_1_5_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_104);
                    	    lv_labels_12_0=ruleOwlLabel();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getDescriptionRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"labels",
                    	            		lv_labels_12_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.OwlLabel");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 7 :
                    	    // InternalOWLModel.g:7289:6: ( (lv_axioms_13_0= ruleClassAxiom ) )
                    	    {
                    	    // InternalOWLModel.g:7289:6: ( (lv_axioms_13_0= ruleClassAxiom ) )
                    	    // InternalOWLModel.g:7290:1: (lv_axioms_13_0= ruleClassAxiom )
                    	    {
                    	    // InternalOWLModel.g:7290:1: (lv_axioms_13_0= ruleClassAxiom )
                    	    // InternalOWLModel.g:7291:3: lv_axioms_13_0= ruleClassAxiom
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getDescriptionAccess().getAxiomsClassAxiomParserRuleCall_3_1_1_6_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_104);
                    	    lv_axioms_13_0=ruleClassAxiom();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getDescriptionRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"axioms",
                    	            		lv_axioms_13_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.ClassAxiom");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop134;
                        }
                    } while (true);

                    otherlv_14=(Token)match(input,123,FOLLOW_7); 

                        	newLeafNode(otherlv_14, grammarAccess.getDescriptionAccess().getRdfDescriptionKeyword_3_1_2());
                        
                    otherlv_15=(Token)match(input,21,FOLLOW_2); 

                        	newLeafNode(otherlv_15, grammarAccess.getDescriptionAccess().getGreaterThanSignKeyword_3_1_3());
                        

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDescription"


    // $ANTLR start "entryRuleOwlFirst"
    // InternalOWLModel.g:7323:1: entryRuleOwlFirst returns [EObject current=null] : iv_ruleOwlFirst= ruleOwlFirst EOF ;
    public final EObject entryRuleOwlFirst() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOwlFirst = null;


        try {
            // InternalOWLModel.g:7324:2: (iv_ruleOwlFirst= ruleOwlFirst EOF )
            // InternalOWLModel.g:7325:2: iv_ruleOwlFirst= ruleOwlFirst EOF
            {
             newCompositeNode(grammarAccess.getOwlFirstRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOwlFirst=ruleOwlFirst();

            state._fsp--;

             current =iv_ruleOwlFirst; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOwlFirst"


    // $ANTLR start "ruleOwlFirst"
    // InternalOWLModel.g:7332:1: ruleOwlFirst returns [EObject current=null] : (otherlv_0= '<rdf:first' ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_refType_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</rdf:first' otherlv_7= '>' ) ) ) | (otherlv_8= 'rdf:datatype' otherlv_9= '=' ( (lv_dataType_10_0= ruleOwlRef ) ) otherlv_11= '>' ( (lv_dataTypeValue_12_0= ruleTAG_ANY_VALUE ) ) otherlv_13= '</rdf:first' otherlv_14= '>' ) ) ) ;
    public final EObject ruleOwlFirst() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        EObject lv_refType_3_0 = null;

        EObject lv_dataType_10_0 = null;

        AntlrDatatypeRuleToken lv_dataTypeValue_12_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:7335:28: ( (otherlv_0= '<rdf:first' ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_refType_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</rdf:first' otherlv_7= '>' ) ) ) | (otherlv_8= 'rdf:datatype' otherlv_9= '=' ( (lv_dataType_10_0= ruleOwlRef ) ) otherlv_11= '>' ( (lv_dataTypeValue_12_0= ruleTAG_ANY_VALUE ) ) otherlv_13= '</rdf:first' otherlv_14= '>' ) ) ) )
            // InternalOWLModel.g:7336:1: (otherlv_0= '<rdf:first' ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_refType_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</rdf:first' otherlv_7= '>' ) ) ) | (otherlv_8= 'rdf:datatype' otherlv_9= '=' ( (lv_dataType_10_0= ruleOwlRef ) ) otherlv_11= '>' ( (lv_dataTypeValue_12_0= ruleTAG_ANY_VALUE ) ) otherlv_13= '</rdf:first' otherlv_14= '>' ) ) )
            {
            // InternalOWLModel.g:7336:1: (otherlv_0= '<rdf:first' ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_refType_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</rdf:first' otherlv_7= '>' ) ) ) | (otherlv_8= 'rdf:datatype' otherlv_9= '=' ( (lv_dataType_10_0= ruleOwlRef ) ) otherlv_11= '>' ( (lv_dataTypeValue_12_0= ruleTAG_ANY_VALUE ) ) otherlv_13= '</rdf:first' otherlv_14= '>' ) ) )
            // InternalOWLModel.g:7336:3: otherlv_0= '<rdf:first' ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_refType_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</rdf:first' otherlv_7= '>' ) ) ) | (otherlv_8= 'rdf:datatype' otherlv_9= '=' ( (lv_dataType_10_0= ruleOwlRef ) ) otherlv_11= '>' ( (lv_dataTypeValue_12_0= ruleTAG_ANY_VALUE ) ) otherlv_13= '</rdf:first' otherlv_14= '>' ) )
            {
            otherlv_0=(Token)match(input,124,FOLLOW_81); 

                	newLeafNode(otherlv_0, grammarAccess.getOwlFirstAccess().getRdfFirstKeyword_0());
                
            // InternalOWLModel.g:7340:1: ( (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_refType_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</rdf:first' otherlv_7= '>' ) ) ) | (otherlv_8= 'rdf:datatype' otherlv_9= '=' ( (lv_dataType_10_0= ruleOwlRef ) ) otherlv_11= '>' ( (lv_dataTypeValue_12_0= ruleTAG_ANY_VALUE ) ) otherlv_13= '</rdf:first' otherlv_14= '>' ) )
            int alt137=2;
            int LA137_0 = input.LA(1);

            if ( (LA137_0==32) ) {
                alt137=1;
            }
            else if ( (LA137_0==62) ) {
                alt137=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 137, 0, input);

                throw nvae;
            }
            switch (alt137) {
                case 1 :
                    // InternalOWLModel.g:7340:2: (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_refType_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</rdf:first' otherlv_7= '>' ) ) )
                    {
                    // InternalOWLModel.g:7340:2: (otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_refType_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</rdf:first' otherlv_7= '>' ) ) )
                    // InternalOWLModel.g:7340:4: otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_refType_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</rdf:first' otherlv_7= '>' ) )
                    {
                    otherlv_1=(Token)match(input,32,FOLLOW_13); 

                        	newLeafNode(otherlv_1, grammarAccess.getOwlFirstAccess().getRdfResourceKeyword_1_0_0());
                        
                    otherlv_2=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_2, grammarAccess.getOwlFirstAccess().getEqualsSignKeyword_1_0_1());
                        
                    // InternalOWLModel.g:7348:1: ( (lv_refType_3_0= ruleOwlRef ) )
                    // InternalOWLModel.g:7349:1: (lv_refType_3_0= ruleOwlRef )
                    {
                    // InternalOWLModel.g:7349:1: (lv_refType_3_0= ruleOwlRef )
                    // InternalOWLModel.g:7350:3: lv_refType_3_0= ruleOwlRef
                    {
                     
                    	        newCompositeNode(grammarAccess.getOwlFirstAccess().getRefTypeOwlRefParserRuleCall_1_0_2_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_refType_3_0=ruleOwlRef();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getOwlFirstRule());
                    	        }
                           		set(
                           			current, 
                           			"refType",
                            		lv_refType_3_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlRef");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // InternalOWLModel.g:7366:2: (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</rdf:first' otherlv_7= '>' ) )
                    int alt136=2;
                    int LA136_0 = input.LA(1);

                    if ( (LA136_0==29) ) {
                        alt136=1;
                    }
                    else if ( (LA136_0==21) ) {
                        alt136=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 136, 0, input);

                        throw nvae;
                    }
                    switch (alt136) {
                        case 1 :
                            // InternalOWLModel.g:7366:4: otherlv_4= '/>'
                            {
                            otherlv_4=(Token)match(input,29,FOLLOW_2); 

                                	newLeafNode(otherlv_4, grammarAccess.getOwlFirstAccess().getSolidusGreaterThanSignKeyword_1_0_3_0());
                                

                            }
                            break;
                        case 2 :
                            // InternalOWLModel.g:7371:6: (otherlv_5= '>' otherlv_6= '</rdf:first' otherlv_7= '>' )
                            {
                            // InternalOWLModel.g:7371:6: (otherlv_5= '>' otherlv_6= '</rdf:first' otherlv_7= '>' )
                            // InternalOWLModel.g:7371:8: otherlv_5= '>' otherlv_6= '</rdf:first' otherlv_7= '>'
                            {
                            otherlv_5=(Token)match(input,21,FOLLOW_105); 

                                	newLeafNode(otherlv_5, grammarAccess.getOwlFirstAccess().getGreaterThanSignKeyword_1_0_3_1_0());
                                
                            otherlv_6=(Token)match(input,125,FOLLOW_7); 

                                	newLeafNode(otherlv_6, grammarAccess.getOwlFirstAccess().getRdfFirstKeyword_1_0_3_1_1());
                                
                            otherlv_7=(Token)match(input,21,FOLLOW_2); 

                                	newLeafNode(otherlv_7, grammarAccess.getOwlFirstAccess().getGreaterThanSignKeyword_1_0_3_1_2());
                                

                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:7384:6: (otherlv_8= 'rdf:datatype' otherlv_9= '=' ( (lv_dataType_10_0= ruleOwlRef ) ) otherlv_11= '>' ( (lv_dataTypeValue_12_0= ruleTAG_ANY_VALUE ) ) otherlv_13= '</rdf:first' otherlv_14= '>' )
                    {
                    // InternalOWLModel.g:7384:6: (otherlv_8= 'rdf:datatype' otherlv_9= '=' ( (lv_dataType_10_0= ruleOwlRef ) ) otherlv_11= '>' ( (lv_dataTypeValue_12_0= ruleTAG_ANY_VALUE ) ) otherlv_13= '</rdf:first' otherlv_14= '>' )
                    // InternalOWLModel.g:7384:8: otherlv_8= 'rdf:datatype' otherlv_9= '=' ( (lv_dataType_10_0= ruleOwlRef ) ) otherlv_11= '>' ( (lv_dataTypeValue_12_0= ruleTAG_ANY_VALUE ) ) otherlv_13= '</rdf:first' otherlv_14= '>'
                    {
                    otherlv_8=(Token)match(input,62,FOLLOW_13); 

                        	newLeafNode(otherlv_8, grammarAccess.getOwlFirstAccess().getRdfDatatypeKeyword_1_1_0());
                        
                    otherlv_9=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_9, grammarAccess.getOwlFirstAccess().getEqualsSignKeyword_1_1_1());
                        
                    // InternalOWLModel.g:7392:1: ( (lv_dataType_10_0= ruleOwlRef ) )
                    // InternalOWLModel.g:7393:1: (lv_dataType_10_0= ruleOwlRef )
                    {
                    // InternalOWLModel.g:7393:1: (lv_dataType_10_0= ruleOwlRef )
                    // InternalOWLModel.g:7394:3: lv_dataType_10_0= ruleOwlRef
                    {
                     
                    	        newCompositeNode(grammarAccess.getOwlFirstAccess().getDataTypeOwlRefParserRuleCall_1_1_2_0()); 
                    	    
                    pushFollow(FOLLOW_7);
                    lv_dataType_10_0=ruleOwlRef();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getOwlFirstRule());
                    	        }
                           		set(
                           			current, 
                           			"dataType",
                            		lv_dataType_10_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlRef");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_11=(Token)match(input,21,FOLLOW_59); 

                        	newLeafNode(otherlv_11, grammarAccess.getOwlFirstAccess().getGreaterThanSignKeyword_1_1_3());
                        
                    // InternalOWLModel.g:7414:1: ( (lv_dataTypeValue_12_0= ruleTAG_ANY_VALUE ) )
                    // InternalOWLModel.g:7415:1: (lv_dataTypeValue_12_0= ruleTAG_ANY_VALUE )
                    {
                    // InternalOWLModel.g:7415:1: (lv_dataTypeValue_12_0= ruleTAG_ANY_VALUE )
                    // InternalOWLModel.g:7416:3: lv_dataTypeValue_12_0= ruleTAG_ANY_VALUE
                    {
                     
                    	        newCompositeNode(grammarAccess.getOwlFirstAccess().getDataTypeValueTAG_ANY_VALUEParserRuleCall_1_1_4_0()); 
                    	    
                    pushFollow(FOLLOW_105);
                    lv_dataTypeValue_12_0=ruleTAG_ANY_VALUE();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getOwlFirstRule());
                    	        }
                           		set(
                           			current, 
                           			"dataTypeValue",
                            		lv_dataTypeValue_12_0, 
                            		"org.smool.sdk.owlparser.OWLModel.TAG_ANY_VALUE");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_13=(Token)match(input,125,FOLLOW_7); 

                        	newLeafNode(otherlv_13, grammarAccess.getOwlFirstAccess().getRdfFirstKeyword_1_1_5());
                        
                    otherlv_14=(Token)match(input,21,FOLLOW_2); 

                        	newLeafNode(otherlv_14, grammarAccess.getOwlFirstAccess().getGreaterThanSignKeyword_1_1_6());
                        

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOwlFirst"


    // $ANTLR start "entryRuleRDFRest"
    // InternalOWLModel.g:7448:1: entryRuleRDFRest returns [EObject current=null] : iv_ruleRDFRest= ruleRDFRest EOF ;
    public final EObject entryRuleRDFRest() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRDFRest = null;


        try {
            // InternalOWLModel.g:7449:2: (iv_ruleRDFRest= ruleRDFRest EOF )
            // InternalOWLModel.g:7450:2: iv_ruleRDFRest= ruleRDFRest EOF
            {
             newCompositeNode(grammarAccess.getRDFRestRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRDFRest=ruleRDFRest();

            state._fsp--;

             current =iv_ruleRDFRest; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRDFRest"


    // $ANTLR start "ruleRDFRest"
    // InternalOWLModel.g:7457:1: ruleRDFRest returns [EObject current=null] : ( () otherlv_1= '<rdf:rest' ( (otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</rdf:rest' otherlv_8= '>' ) ) ) | ( (otherlv_9= 'rdf:parseType' otherlv_10= '=' ( (lv_parseType_11_0= ruleParseType ) ) )? otherlv_12= '>' ( ( (lv_first_13_0= ruleOwlFirst ) ) | ( (lv_rest_14_0= ruleRDFRest ) ) )* otherlv_15= '</rdf:rest' otherlv_16= '>' ) ) ) ;
    public final EObject ruleRDFRest() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        EObject lv_ref_4_0 = null;

        EObject lv_parseType_11_0 = null;

        EObject lv_first_13_0 = null;

        EObject lv_rest_14_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:7460:28: ( ( () otherlv_1= '<rdf:rest' ( (otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</rdf:rest' otherlv_8= '>' ) ) ) | ( (otherlv_9= 'rdf:parseType' otherlv_10= '=' ( (lv_parseType_11_0= ruleParseType ) ) )? otherlv_12= '>' ( ( (lv_first_13_0= ruleOwlFirst ) ) | ( (lv_rest_14_0= ruleRDFRest ) ) )* otherlv_15= '</rdf:rest' otherlv_16= '>' ) ) ) )
            // InternalOWLModel.g:7461:1: ( () otherlv_1= '<rdf:rest' ( (otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</rdf:rest' otherlv_8= '>' ) ) ) | ( (otherlv_9= 'rdf:parseType' otherlv_10= '=' ( (lv_parseType_11_0= ruleParseType ) ) )? otherlv_12= '>' ( ( (lv_first_13_0= ruleOwlFirst ) ) | ( (lv_rest_14_0= ruleRDFRest ) ) )* otherlv_15= '</rdf:rest' otherlv_16= '>' ) ) )
            {
            // InternalOWLModel.g:7461:1: ( () otherlv_1= '<rdf:rest' ( (otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</rdf:rest' otherlv_8= '>' ) ) ) | ( (otherlv_9= 'rdf:parseType' otherlv_10= '=' ( (lv_parseType_11_0= ruleParseType ) ) )? otherlv_12= '>' ( ( (lv_first_13_0= ruleOwlFirst ) ) | ( (lv_rest_14_0= ruleRDFRest ) ) )* otherlv_15= '</rdf:rest' otherlv_16= '>' ) ) )
            // InternalOWLModel.g:7461:2: () otherlv_1= '<rdf:rest' ( (otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</rdf:rest' otherlv_8= '>' ) ) ) | ( (otherlv_9= 'rdf:parseType' otherlv_10= '=' ( (lv_parseType_11_0= ruleParseType ) ) )? otherlv_12= '>' ( ( (lv_first_13_0= ruleOwlFirst ) ) | ( (lv_rest_14_0= ruleRDFRest ) ) )* otherlv_15= '</rdf:rest' otherlv_16= '>' ) )
            {
            // InternalOWLModel.g:7461:2: ()
            // InternalOWLModel.g:7462:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getRDFRestAccess().getRDFRestAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,126,FOLLOW_37); 

                	newLeafNode(otherlv_1, grammarAccess.getRDFRestAccess().getRdfRestKeyword_1());
                
            // InternalOWLModel.g:7471:1: ( (otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</rdf:rest' otherlv_8= '>' ) ) ) | ( (otherlv_9= 'rdf:parseType' otherlv_10= '=' ( (lv_parseType_11_0= ruleParseType ) ) )? otherlv_12= '>' ( ( (lv_first_13_0= ruleOwlFirst ) ) | ( (lv_rest_14_0= ruleRDFRest ) ) )* otherlv_15= '</rdf:rest' otherlv_16= '>' ) )
            int alt141=2;
            int LA141_0 = input.LA(1);

            if ( (LA141_0==32) ) {
                alt141=1;
            }
            else if ( (LA141_0==21||LA141_0==44) ) {
                alt141=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 141, 0, input);

                throw nvae;
            }
            switch (alt141) {
                case 1 :
                    // InternalOWLModel.g:7471:2: (otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</rdf:rest' otherlv_8= '>' ) ) )
                    {
                    // InternalOWLModel.g:7471:2: (otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</rdf:rest' otherlv_8= '>' ) ) )
                    // InternalOWLModel.g:7471:4: otherlv_2= 'rdf:resource' otherlv_3= '=' ( (lv_ref_4_0= ruleOwlRef ) ) (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</rdf:rest' otherlv_8= '>' ) )
                    {
                    otherlv_2=(Token)match(input,32,FOLLOW_13); 

                        	newLeafNode(otherlv_2, grammarAccess.getRDFRestAccess().getRdfResourceKeyword_2_0_0());
                        
                    otherlv_3=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_3, grammarAccess.getRDFRestAccess().getEqualsSignKeyword_2_0_1());
                        
                    // InternalOWLModel.g:7479:1: ( (lv_ref_4_0= ruleOwlRef ) )
                    // InternalOWLModel.g:7480:1: (lv_ref_4_0= ruleOwlRef )
                    {
                    // InternalOWLModel.g:7480:1: (lv_ref_4_0= ruleOwlRef )
                    // InternalOWLModel.g:7481:3: lv_ref_4_0= ruleOwlRef
                    {
                     
                    	        newCompositeNode(grammarAccess.getRDFRestAccess().getRefOwlRefParserRuleCall_2_0_2_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_ref_4_0=ruleOwlRef();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getRDFRestRule());
                    	        }
                           		set(
                           			current, 
                           			"ref",
                            		lv_ref_4_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlRef");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // InternalOWLModel.g:7497:2: (otherlv_5= '/>' | (otherlv_6= '>' otherlv_7= '</rdf:rest' otherlv_8= '>' ) )
                    int alt138=2;
                    int LA138_0 = input.LA(1);

                    if ( (LA138_0==29) ) {
                        alt138=1;
                    }
                    else if ( (LA138_0==21) ) {
                        alt138=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 138, 0, input);

                        throw nvae;
                    }
                    switch (alt138) {
                        case 1 :
                            // InternalOWLModel.g:7497:4: otherlv_5= '/>'
                            {
                            otherlv_5=(Token)match(input,29,FOLLOW_2); 

                                	newLeafNode(otherlv_5, grammarAccess.getRDFRestAccess().getSolidusGreaterThanSignKeyword_2_0_3_0());
                                

                            }
                            break;
                        case 2 :
                            // InternalOWLModel.g:7502:6: (otherlv_6= '>' otherlv_7= '</rdf:rest' otherlv_8= '>' )
                            {
                            // InternalOWLModel.g:7502:6: (otherlv_6= '>' otherlv_7= '</rdf:rest' otherlv_8= '>' )
                            // InternalOWLModel.g:7502:8: otherlv_6= '>' otherlv_7= '</rdf:rest' otherlv_8= '>'
                            {
                            otherlv_6=(Token)match(input,21,FOLLOW_106); 

                                	newLeafNode(otherlv_6, grammarAccess.getRDFRestAccess().getGreaterThanSignKeyword_2_0_3_1_0());
                                
                            otherlv_7=(Token)match(input,127,FOLLOW_7); 

                                	newLeafNode(otherlv_7, grammarAccess.getRDFRestAccess().getRdfRestKeyword_2_0_3_1_1());
                                
                            otherlv_8=(Token)match(input,21,FOLLOW_2); 

                                	newLeafNode(otherlv_8, grammarAccess.getRDFRestAccess().getGreaterThanSignKeyword_2_0_3_1_2());
                                

                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:7515:6: ( (otherlv_9= 'rdf:parseType' otherlv_10= '=' ( (lv_parseType_11_0= ruleParseType ) ) )? otherlv_12= '>' ( ( (lv_first_13_0= ruleOwlFirst ) ) | ( (lv_rest_14_0= ruleRDFRest ) ) )* otherlv_15= '</rdf:rest' otherlv_16= '>' )
                    {
                    // InternalOWLModel.g:7515:6: ( (otherlv_9= 'rdf:parseType' otherlv_10= '=' ( (lv_parseType_11_0= ruleParseType ) ) )? otherlv_12= '>' ( ( (lv_first_13_0= ruleOwlFirst ) ) | ( (lv_rest_14_0= ruleRDFRest ) ) )* otherlv_15= '</rdf:rest' otherlv_16= '>' )
                    // InternalOWLModel.g:7515:7: (otherlv_9= 'rdf:parseType' otherlv_10= '=' ( (lv_parseType_11_0= ruleParseType ) ) )? otherlv_12= '>' ( ( (lv_first_13_0= ruleOwlFirst ) ) | ( (lv_rest_14_0= ruleRDFRest ) ) )* otherlv_15= '</rdf:rest' otherlv_16= '>'
                    {
                    // InternalOWLModel.g:7515:7: (otherlv_9= 'rdf:parseType' otherlv_10= '=' ( (lv_parseType_11_0= ruleParseType ) ) )?
                    int alt139=2;
                    int LA139_0 = input.LA(1);

                    if ( (LA139_0==44) ) {
                        alt139=1;
                    }
                    switch (alt139) {
                        case 1 :
                            // InternalOWLModel.g:7515:9: otherlv_9= 'rdf:parseType' otherlv_10= '=' ( (lv_parseType_11_0= ruleParseType ) )
                            {
                            otherlv_9=(Token)match(input,44,FOLLOW_13); 

                                	newLeafNode(otherlv_9, grammarAccess.getRDFRestAccess().getRdfParseTypeKeyword_2_1_0_0());
                                
                            otherlv_10=(Token)match(input,25,FOLLOW_9); 

                                	newLeafNode(otherlv_10, grammarAccess.getRDFRestAccess().getEqualsSignKeyword_2_1_0_1());
                                
                            // InternalOWLModel.g:7523:1: ( (lv_parseType_11_0= ruleParseType ) )
                            // InternalOWLModel.g:7524:1: (lv_parseType_11_0= ruleParseType )
                            {
                            // InternalOWLModel.g:7524:1: (lv_parseType_11_0= ruleParseType )
                            // InternalOWLModel.g:7525:3: lv_parseType_11_0= ruleParseType
                            {
                             
                            	        newCompositeNode(grammarAccess.getRDFRestAccess().getParseTypeParseTypeParserRuleCall_2_1_0_2_0()); 
                            	    
                            pushFollow(FOLLOW_7);
                            lv_parseType_11_0=ruleParseType();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getRDFRestRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"parseType",
                                    		lv_parseType_11_0, 
                                    		"org.smool.sdk.owlparser.OWLModel.ParseType");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }

                    otherlv_12=(Token)match(input,21,FOLLOW_107); 

                        	newLeafNode(otherlv_12, grammarAccess.getRDFRestAccess().getGreaterThanSignKeyword_2_1_1());
                        
                    // InternalOWLModel.g:7545:1: ( ( (lv_first_13_0= ruleOwlFirst ) ) | ( (lv_rest_14_0= ruleRDFRest ) ) )*
                    loop140:
                    do {
                        int alt140=3;
                        int LA140_0 = input.LA(1);

                        if ( (LA140_0==124) ) {
                            alt140=1;
                        }
                        else if ( (LA140_0==126) ) {
                            alt140=2;
                        }


                        switch (alt140) {
                    	case 1 :
                    	    // InternalOWLModel.g:7545:2: ( (lv_first_13_0= ruleOwlFirst ) )
                    	    {
                    	    // InternalOWLModel.g:7545:2: ( (lv_first_13_0= ruleOwlFirst ) )
                    	    // InternalOWLModel.g:7546:1: (lv_first_13_0= ruleOwlFirst )
                    	    {
                    	    // InternalOWLModel.g:7546:1: (lv_first_13_0= ruleOwlFirst )
                    	    // InternalOWLModel.g:7547:3: lv_first_13_0= ruleOwlFirst
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getRDFRestAccess().getFirstOwlFirstParserRuleCall_2_1_2_0_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_107);
                    	    lv_first_13_0=ruleOwlFirst();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getRDFRestRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"first",
                    	            		lv_first_13_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.OwlFirst");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalOWLModel.g:7564:6: ( (lv_rest_14_0= ruleRDFRest ) )
                    	    {
                    	    // InternalOWLModel.g:7564:6: ( (lv_rest_14_0= ruleRDFRest ) )
                    	    // InternalOWLModel.g:7565:1: (lv_rest_14_0= ruleRDFRest )
                    	    {
                    	    // InternalOWLModel.g:7565:1: (lv_rest_14_0= ruleRDFRest )
                    	    // InternalOWLModel.g:7566:3: lv_rest_14_0= ruleRDFRest
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getRDFRestAccess().getRestRDFRestParserRuleCall_2_1_2_1_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_107);
                    	    lv_rest_14_0=ruleRDFRest();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getRDFRestRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"rest",
                    	            		lv_rest_14_0, 
                    	            		"org.smool.sdk.owlparser.OWLModel.RDFRest");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop140;
                        }
                    } while (true);

                    otherlv_15=(Token)match(input,127,FOLLOW_7); 

                        	newLeafNode(otherlv_15, grammarAccess.getRDFRestAccess().getRdfRestKeyword_2_1_3());
                        
                    otherlv_16=(Token)match(input,21,FOLLOW_2); 

                        	newLeafNode(otherlv_16, grammarAccess.getRDFRestAccess().getGreaterThanSignKeyword_2_1_4());
                        

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRDFRest"


    // $ANTLR start "entryRuleOwlType"
    // InternalOWLModel.g:7598:1: entryRuleOwlType returns [EObject current=null] : iv_ruleOwlType= ruleOwlType EOF ;
    public final EObject entryRuleOwlType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOwlType = null;


        try {
            // InternalOWLModel.g:7599:2: (iv_ruleOwlType= ruleOwlType EOF )
            // InternalOWLModel.g:7600:2: iv_ruleOwlType= ruleOwlType EOF
            {
             newCompositeNode(grammarAccess.getOwlTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOwlType=ruleOwlType();

            state._fsp--;

             current =iv_ruleOwlType; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOwlType"


    // $ANTLR start "ruleOwlType"
    // InternalOWLModel.g:7607:1: ruleOwlType returns [EObject current=null] : (otherlv_0= '<rdf:type' otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_refType_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</rdf:type' otherlv_7= '>' ) ) ( (lv_members_8_0= ruleOwlMembers ) )? ) ;
    public final EObject ruleOwlType() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        EObject lv_refType_3_0 = null;

        EObject lv_members_8_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:7610:28: ( (otherlv_0= '<rdf:type' otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_refType_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</rdf:type' otherlv_7= '>' ) ) ( (lv_members_8_0= ruleOwlMembers ) )? ) )
            // InternalOWLModel.g:7611:1: (otherlv_0= '<rdf:type' otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_refType_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</rdf:type' otherlv_7= '>' ) ) ( (lv_members_8_0= ruleOwlMembers ) )? )
            {
            // InternalOWLModel.g:7611:1: (otherlv_0= '<rdf:type' otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_refType_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</rdf:type' otherlv_7= '>' ) ) ( (lv_members_8_0= ruleOwlMembers ) )? )
            // InternalOWLModel.g:7611:3: otherlv_0= '<rdf:type' otherlv_1= 'rdf:resource' otherlv_2= '=' ( (lv_refType_3_0= ruleOwlRef ) ) (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</rdf:type' otherlv_7= '>' ) ) ( (lv_members_8_0= ruleOwlMembers ) )?
            {
            otherlv_0=(Token)match(input,118,FOLLOW_28); 

                	newLeafNode(otherlv_0, grammarAccess.getOwlTypeAccess().getRdfTypeKeyword_0());
                
            otherlv_1=(Token)match(input,32,FOLLOW_13); 

                	newLeafNode(otherlv_1, grammarAccess.getOwlTypeAccess().getRdfResourceKeyword_1());
                
            otherlv_2=(Token)match(input,25,FOLLOW_9); 

                	newLeafNode(otherlv_2, grammarAccess.getOwlTypeAccess().getEqualsSignKeyword_2());
                
            // InternalOWLModel.g:7623:1: ( (lv_refType_3_0= ruleOwlRef ) )
            // InternalOWLModel.g:7624:1: (lv_refType_3_0= ruleOwlRef )
            {
            // InternalOWLModel.g:7624:1: (lv_refType_3_0= ruleOwlRef )
            // InternalOWLModel.g:7625:3: lv_refType_3_0= ruleOwlRef
            {
             
            	        newCompositeNode(grammarAccess.getOwlTypeAccess().getRefTypeOwlRefParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_24);
            lv_refType_3_0=ruleOwlRef();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getOwlTypeRule());
            	        }
                   		set(
                   			current, 
                   			"refType",
                    		lv_refType_3_0, 
                    		"org.smool.sdk.owlparser.OWLModel.OwlRef");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalOWLModel.g:7641:2: (otherlv_4= '/>' | (otherlv_5= '>' otherlv_6= '</rdf:type' otherlv_7= '>' ) )
            int alt142=2;
            int LA142_0 = input.LA(1);

            if ( (LA142_0==29) ) {
                alt142=1;
            }
            else if ( (LA142_0==21) ) {
                alt142=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 142, 0, input);

                throw nvae;
            }
            switch (alt142) {
                case 1 :
                    // InternalOWLModel.g:7641:4: otherlv_4= '/>'
                    {
                    otherlv_4=(Token)match(input,29,FOLLOW_108); 

                        	newLeafNode(otherlv_4, grammarAccess.getOwlTypeAccess().getSolidusGreaterThanSignKeyword_4_0());
                        

                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:7646:6: (otherlv_5= '>' otherlv_6= '</rdf:type' otherlv_7= '>' )
                    {
                    // InternalOWLModel.g:7646:6: (otherlv_5= '>' otherlv_6= '</rdf:type' otherlv_7= '>' )
                    // InternalOWLModel.g:7646:8: otherlv_5= '>' otherlv_6= '</rdf:type' otherlv_7= '>'
                    {
                    otherlv_5=(Token)match(input,21,FOLLOW_101); 

                        	newLeafNode(otherlv_5, grammarAccess.getOwlTypeAccess().getGreaterThanSignKeyword_4_1_0());
                        
                    otherlv_6=(Token)match(input,119,FOLLOW_7); 

                        	newLeafNode(otherlv_6, grammarAccess.getOwlTypeAccess().getRdfTypeKeyword_4_1_1());
                        
                    otherlv_7=(Token)match(input,21,FOLLOW_108); 

                        	newLeafNode(otherlv_7, grammarAccess.getOwlTypeAccess().getGreaterThanSignKeyword_4_1_2());
                        

                    }


                    }
                    break;

            }

            // InternalOWLModel.g:7658:3: ( (lv_members_8_0= ruleOwlMembers ) )?
            int alt143=2;
            int LA143_0 = input.LA(1);

            if ( (LA143_0==128) ) {
                alt143=1;
            }
            switch (alt143) {
                case 1 :
                    // InternalOWLModel.g:7659:1: (lv_members_8_0= ruleOwlMembers )
                    {
                    // InternalOWLModel.g:7659:1: (lv_members_8_0= ruleOwlMembers )
                    // InternalOWLModel.g:7660:3: lv_members_8_0= ruleOwlMembers
                    {
                     
                    	        newCompositeNode(grammarAccess.getOwlTypeAccess().getMembersOwlMembersParserRuleCall_5_0()); 
                    	    
                    pushFollow(FOLLOW_2);
                    lv_members_8_0=ruleOwlMembers();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getOwlTypeRule());
                    	        }
                           		set(
                           			current, 
                           			"members",
                            		lv_members_8_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlMembers");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOwlType"


    // $ANTLR start "entryRuleOwlMembers"
    // InternalOWLModel.g:7684:1: entryRuleOwlMembers returns [EObject current=null] : iv_ruleOwlMembers= ruleOwlMembers EOF ;
    public final EObject entryRuleOwlMembers() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOwlMembers = null;


        try {
            // InternalOWLModel.g:7685:2: (iv_ruleOwlMembers= ruleOwlMembers EOF )
            // InternalOWLModel.g:7686:2: iv_ruleOwlMembers= ruleOwlMembers EOF
            {
             newCompositeNode(grammarAccess.getOwlMembersRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOwlMembers=ruleOwlMembers();

            state._fsp--;

             current =iv_ruleOwlMembers; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOwlMembers"


    // $ANTLR start "ruleOwlMembers"
    // InternalOWLModel.g:7693:1: ruleOwlMembers returns [EObject current=null] : (otherlv_0= '<owl:members' otherlv_1= 'rdf:parseType' otherlv_2= '=' ( (lv_parseType_3_0= ruleParseType ) ) otherlv_4= '>' ( (lv_descriptions_5_0= ruleDescription ) )+ otherlv_6= '</owl:members' otherlv_7= '>' ) ;
    public final EObject ruleOwlMembers() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        EObject lv_parseType_3_0 = null;

        EObject lv_descriptions_5_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:7696:28: ( (otherlv_0= '<owl:members' otherlv_1= 'rdf:parseType' otherlv_2= '=' ( (lv_parseType_3_0= ruleParseType ) ) otherlv_4= '>' ( (lv_descriptions_5_0= ruleDescription ) )+ otherlv_6= '</owl:members' otherlv_7= '>' ) )
            // InternalOWLModel.g:7697:1: (otherlv_0= '<owl:members' otherlv_1= 'rdf:parseType' otherlv_2= '=' ( (lv_parseType_3_0= ruleParseType ) ) otherlv_4= '>' ( (lv_descriptions_5_0= ruleDescription ) )+ otherlv_6= '</owl:members' otherlv_7= '>' )
            {
            // InternalOWLModel.g:7697:1: (otherlv_0= '<owl:members' otherlv_1= 'rdf:parseType' otherlv_2= '=' ( (lv_parseType_3_0= ruleParseType ) ) otherlv_4= '>' ( (lv_descriptions_5_0= ruleDescription ) )+ otherlv_6= '</owl:members' otherlv_7= '>' )
            // InternalOWLModel.g:7697:3: otherlv_0= '<owl:members' otherlv_1= 'rdf:parseType' otherlv_2= '=' ( (lv_parseType_3_0= ruleParseType ) ) otherlv_4= '>' ( (lv_descriptions_5_0= ruleDescription ) )+ otherlv_6= '</owl:members' otherlv_7= '>'
            {
            otherlv_0=(Token)match(input,128,FOLLOW_109); 

                	newLeafNode(otherlv_0, grammarAccess.getOwlMembersAccess().getOwlMembersKeyword_0());
                
            otherlv_1=(Token)match(input,44,FOLLOW_13); 

                	newLeafNode(otherlv_1, grammarAccess.getOwlMembersAccess().getRdfParseTypeKeyword_1());
                
            otherlv_2=(Token)match(input,25,FOLLOW_9); 

                	newLeafNode(otherlv_2, grammarAccess.getOwlMembersAccess().getEqualsSignKeyword_2());
                
            // InternalOWLModel.g:7709:1: ( (lv_parseType_3_0= ruleParseType ) )
            // InternalOWLModel.g:7710:1: (lv_parseType_3_0= ruleParseType )
            {
            // InternalOWLModel.g:7710:1: (lv_parseType_3_0= ruleParseType )
            // InternalOWLModel.g:7711:3: lv_parseType_3_0= ruleParseType
            {
             
            	        newCompositeNode(grammarAccess.getOwlMembersAccess().getParseTypeParseTypeParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_7);
            lv_parseType_3_0=ruleParseType();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getOwlMembersRule());
            	        }
                   		set(
                   			current, 
                   			"parseType",
                    		lv_parseType_3_0, 
                    		"org.smool.sdk.owlparser.OWLModel.ParseType");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_4=(Token)match(input,21,FOLLOW_110); 

                	newLeafNode(otherlv_4, grammarAccess.getOwlMembersAccess().getGreaterThanSignKeyword_4());
                
            // InternalOWLModel.g:7731:1: ( (lv_descriptions_5_0= ruleDescription ) )+
            int cnt144=0;
            loop144:
            do {
                int alt144=2;
                int LA144_0 = input.LA(1);

                if ( (LA144_0==122) ) {
                    alt144=1;
                }


                switch (alt144) {
            	case 1 :
            	    // InternalOWLModel.g:7732:1: (lv_descriptions_5_0= ruleDescription )
            	    {
            	    // InternalOWLModel.g:7732:1: (lv_descriptions_5_0= ruleDescription )
            	    // InternalOWLModel.g:7733:3: lv_descriptions_5_0= ruleDescription
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getOwlMembersAccess().getDescriptionsDescriptionParserRuleCall_5_0()); 
            	    	    
            	    pushFollow(FOLLOW_111);
            	    lv_descriptions_5_0=ruleDescription();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getOwlMembersRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"descriptions",
            	            		lv_descriptions_5_0, 
            	            		"org.smool.sdk.owlparser.OWLModel.Description");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt144 >= 1 ) break loop144;
                        EarlyExitException eee =
                            new EarlyExitException(144, input);
                        throw eee;
                }
                cnt144++;
            } while (true);

            otherlv_6=(Token)match(input,129,FOLLOW_7); 

                	newLeafNode(otherlv_6, grammarAccess.getOwlMembersAccess().getOwlMembersKeyword_6());
                
            otherlv_7=(Token)match(input,21,FOLLOW_2); 

                	newLeafNode(otherlv_7, grammarAccess.getOwlMembersAccess().getGreaterThanSignKeyword_7());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOwlMembers"


    // $ANTLR start "entryRuleDatatype"
    // InternalOWLModel.g:7765:1: entryRuleDatatype returns [EObject current=null] : iv_ruleDatatype= ruleDatatype EOF ;
    public final EObject entryRuleDatatype() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDatatype = null;


        try {
            // InternalOWLModel.g:7766:2: (iv_ruleDatatype= ruleDatatype EOF )
            // InternalOWLModel.g:7767:2: iv_ruleDatatype= ruleDatatype EOF
            {
             newCompositeNode(grammarAccess.getDatatypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDatatype=ruleDatatype();

            state._fsp--;

             current =iv_ruleDatatype; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDatatype"


    // $ANTLR start "ruleDatatype"
    // InternalOWLModel.g:7774:1: ruleDatatype returns [EObject current=null] : (otherlv_0= '<rdfs:Datatype' ( (otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) ) ) | ( ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) ) ) ) (otherlv_7= '/>' | (otherlv_8= '>' otherlv_9= '</rdfs:Datatype' otherlv_10= '>' ) ) ) ;
    public final EObject ruleDatatype() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_reference_4_0=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        EObject lv_id_3_0 = null;

        EObject lv_ref_6_0 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:7777:28: ( (otherlv_0= '<rdfs:Datatype' ( (otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) ) ) | ( ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) ) ) ) (otherlv_7= '/>' | (otherlv_8= '>' otherlv_9= '</rdfs:Datatype' otherlv_10= '>' ) ) ) )
            // InternalOWLModel.g:7778:1: (otherlv_0= '<rdfs:Datatype' ( (otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) ) ) | ( ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) ) ) ) (otherlv_7= '/>' | (otherlv_8= '>' otherlv_9= '</rdfs:Datatype' otherlv_10= '>' ) ) )
            {
            // InternalOWLModel.g:7778:1: (otherlv_0= '<rdfs:Datatype' ( (otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) ) ) | ( ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) ) ) ) (otherlv_7= '/>' | (otherlv_8= '>' otherlv_9= '</rdfs:Datatype' otherlv_10= '>' ) ) )
            // InternalOWLModel.g:7778:3: otherlv_0= '<rdfs:Datatype' ( (otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) ) ) | ( ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) ) ) ) (otherlv_7= '/>' | (otherlv_8= '>' otherlv_9= '</rdfs:Datatype' otherlv_10= '>' ) )
            {
            otherlv_0=(Token)match(input,130,FOLLOW_34); 

                	newLeafNode(otherlv_0, grammarAccess.getDatatypeAccess().getRdfsDatatypeKeyword_0());
                
            // InternalOWLModel.g:7782:1: ( (otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) ) ) | ( ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) ) ) )
            int alt145=2;
            int LA145_0 = input.LA(1);

            if ( (LA145_0==40) ) {
                alt145=1;
            }
            else if ( (LA145_0==28) ) {
                alt145=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 145, 0, input);

                throw nvae;
            }
            switch (alt145) {
                case 1 :
                    // InternalOWLModel.g:7782:2: (otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) ) )
                    {
                    // InternalOWLModel.g:7782:2: (otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) ) )
                    // InternalOWLModel.g:7782:4: otherlv_1= 'rdf:ID' otherlv_2= '=' ( (lv_id_3_0= ruleOwlID ) )
                    {
                    otherlv_1=(Token)match(input,40,FOLLOW_13); 

                        	newLeafNode(otherlv_1, grammarAccess.getDatatypeAccess().getRdfIDKeyword_1_0_0());
                        
                    otherlv_2=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_2, grammarAccess.getDatatypeAccess().getEqualsSignKeyword_1_0_1());
                        
                    // InternalOWLModel.g:7790:1: ( (lv_id_3_0= ruleOwlID ) )
                    // InternalOWLModel.g:7791:1: (lv_id_3_0= ruleOwlID )
                    {
                    // InternalOWLModel.g:7791:1: (lv_id_3_0= ruleOwlID )
                    // InternalOWLModel.g:7792:3: lv_id_3_0= ruleOwlID
                    {
                     
                    	        newCompositeNode(grammarAccess.getDatatypeAccess().getIdOwlIDParserRuleCall_1_0_2_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_id_3_0=ruleOwlID();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getDatatypeRule());
                    	        }
                           		set(
                           			current, 
                           			"id",
                            		lv_id_3_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlID");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:7809:6: ( ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) ) )
                    {
                    // InternalOWLModel.g:7809:6: ( ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) ) )
                    // InternalOWLModel.g:7809:7: ( (lv_reference_4_0= 'rdf:about' ) ) otherlv_5= '=' ( (lv_ref_6_0= ruleOwlRef ) )
                    {
                    // InternalOWLModel.g:7809:7: ( (lv_reference_4_0= 'rdf:about' ) )
                    // InternalOWLModel.g:7810:1: (lv_reference_4_0= 'rdf:about' )
                    {
                    // InternalOWLModel.g:7810:1: (lv_reference_4_0= 'rdf:about' )
                    // InternalOWLModel.g:7811:3: lv_reference_4_0= 'rdf:about'
                    {
                    lv_reference_4_0=(Token)match(input,28,FOLLOW_13); 

                            newLeafNode(lv_reference_4_0, grammarAccess.getDatatypeAccess().getReferenceRdfAboutKeyword_1_1_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getDatatypeRule());
                    	        }
                           		setWithLastConsumed(current, "reference", true, "rdf:about");
                    	    

                    }


                    }

                    otherlv_5=(Token)match(input,25,FOLLOW_9); 

                        	newLeafNode(otherlv_5, grammarAccess.getDatatypeAccess().getEqualsSignKeyword_1_1_1());
                        
                    // InternalOWLModel.g:7828:1: ( (lv_ref_6_0= ruleOwlRef ) )
                    // InternalOWLModel.g:7829:1: (lv_ref_6_0= ruleOwlRef )
                    {
                    // InternalOWLModel.g:7829:1: (lv_ref_6_0= ruleOwlRef )
                    // InternalOWLModel.g:7830:3: lv_ref_6_0= ruleOwlRef
                    {
                     
                    	        newCompositeNode(grammarAccess.getDatatypeAccess().getRefOwlRefParserRuleCall_1_1_2_0()); 
                    	    
                    pushFollow(FOLLOW_24);
                    lv_ref_6_0=ruleOwlRef();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getDatatypeRule());
                    	        }
                           		set(
                           			current, 
                           			"ref",
                            		lv_ref_6_0, 
                            		"org.smool.sdk.owlparser.OWLModel.OwlRef");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;

            }

            // InternalOWLModel.g:7846:4: (otherlv_7= '/>' | (otherlv_8= '>' otherlv_9= '</rdfs:Datatype' otherlv_10= '>' ) )
            int alt146=2;
            int LA146_0 = input.LA(1);

            if ( (LA146_0==29) ) {
                alt146=1;
            }
            else if ( (LA146_0==21) ) {
                alt146=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 146, 0, input);

                throw nvae;
            }
            switch (alt146) {
                case 1 :
                    // InternalOWLModel.g:7846:6: otherlv_7= '/>'
                    {
                    otherlv_7=(Token)match(input,29,FOLLOW_2); 

                        	newLeafNode(otherlv_7, grammarAccess.getDatatypeAccess().getSolidusGreaterThanSignKeyword_2_0());
                        

                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:7851:6: (otherlv_8= '>' otherlv_9= '</rdfs:Datatype' otherlv_10= '>' )
                    {
                    // InternalOWLModel.g:7851:6: (otherlv_8= '>' otherlv_9= '</rdfs:Datatype' otherlv_10= '>' )
                    // InternalOWLModel.g:7851:8: otherlv_8= '>' otherlv_9= '</rdfs:Datatype' otherlv_10= '>'
                    {
                    otherlv_8=(Token)match(input,21,FOLLOW_112); 

                        	newLeafNode(otherlv_8, grammarAccess.getDatatypeAccess().getGreaterThanSignKeyword_2_1_0());
                        
                    otherlv_9=(Token)match(input,131,FOLLOW_7); 

                        	newLeafNode(otherlv_9, grammarAccess.getDatatypeAccess().getRdfsDatatypeKeyword_2_1_1());
                        
                    otherlv_10=(Token)match(input,21,FOLLOW_2); 

                        	newLeafNode(otherlv_10, grammarAccess.getDatatypeAccess().getGreaterThanSignKeyword_2_1_2());
                        

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDatatype"


    // $ANTLR start "entryRuleANY_OTHER_TAG"
    // InternalOWLModel.g:7871:1: entryRuleANY_OTHER_TAG returns [String current=null] : iv_ruleANY_OTHER_TAG= ruleANY_OTHER_TAG EOF ;
    public final String entryRuleANY_OTHER_TAG() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleANY_OTHER_TAG = null;


        try {
            // InternalOWLModel.g:7872:2: (iv_ruleANY_OTHER_TAG= ruleANY_OTHER_TAG EOF )
            // InternalOWLModel.g:7873:2: iv_ruleANY_OTHER_TAG= ruleANY_OTHER_TAG EOF
            {
             newCompositeNode(grammarAccess.getANY_OTHER_TAGRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleANY_OTHER_TAG=ruleANY_OTHER_TAG();

            state._fsp--;

             current =iv_ruleANY_OTHER_TAG.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleANY_OTHER_TAG"


    // $ANTLR start "ruleANY_OTHER_TAG"
    // InternalOWLModel.g:7880:1: ruleANY_OTHER_TAG returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '<' (kw= 'rdfs:' | kw= 'owl:' | kw= 'rdf:' )? (this_NSID_4= RULE_NSID | this_ID_5= RULE_ID ) (this_ANY_OTHER_PARAM_6= ruleANY_OTHER_PARAM )* (kw= '/>' | (kw= '>' ( (this_ANY_OTHER_TAG_9= ruleANY_OTHER_TAG )* | this_TAG_ANY_VALUE_10= ruleTAG_ANY_VALUE ) kw= '</' (this_NSID_12= RULE_NSID | this_ID_13= RULE_ID ) kw= '>' ) ) ) ;
    public final AntlrDatatypeRuleToken ruleANY_OTHER_TAG() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_NSID_4=null;
        Token this_ID_5=null;
        Token this_NSID_12=null;
        Token this_ID_13=null;
        AntlrDatatypeRuleToken this_ANY_OTHER_PARAM_6 = null;

        AntlrDatatypeRuleToken this_ANY_OTHER_TAG_9 = null;

        AntlrDatatypeRuleToken this_TAG_ANY_VALUE_10 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:7883:28: ( (kw= '<' (kw= 'rdfs:' | kw= 'owl:' | kw= 'rdf:' )? (this_NSID_4= RULE_NSID | this_ID_5= RULE_ID ) (this_ANY_OTHER_PARAM_6= ruleANY_OTHER_PARAM )* (kw= '/>' | (kw= '>' ( (this_ANY_OTHER_TAG_9= ruleANY_OTHER_TAG )* | this_TAG_ANY_VALUE_10= ruleTAG_ANY_VALUE ) kw= '</' (this_NSID_12= RULE_NSID | this_ID_13= RULE_ID ) kw= '>' ) ) ) )
            // InternalOWLModel.g:7884:1: (kw= '<' (kw= 'rdfs:' | kw= 'owl:' | kw= 'rdf:' )? (this_NSID_4= RULE_NSID | this_ID_5= RULE_ID ) (this_ANY_OTHER_PARAM_6= ruleANY_OTHER_PARAM )* (kw= '/>' | (kw= '>' ( (this_ANY_OTHER_TAG_9= ruleANY_OTHER_TAG )* | this_TAG_ANY_VALUE_10= ruleTAG_ANY_VALUE ) kw= '</' (this_NSID_12= RULE_NSID | this_ID_13= RULE_ID ) kw= '>' ) ) )
            {
            // InternalOWLModel.g:7884:1: (kw= '<' (kw= 'rdfs:' | kw= 'owl:' | kw= 'rdf:' )? (this_NSID_4= RULE_NSID | this_ID_5= RULE_ID ) (this_ANY_OTHER_PARAM_6= ruleANY_OTHER_PARAM )* (kw= '/>' | (kw= '>' ( (this_ANY_OTHER_TAG_9= ruleANY_OTHER_TAG )* | this_TAG_ANY_VALUE_10= ruleTAG_ANY_VALUE ) kw= '</' (this_NSID_12= RULE_NSID | this_ID_13= RULE_ID ) kw= '>' ) ) )
            // InternalOWLModel.g:7885:2: kw= '<' (kw= 'rdfs:' | kw= 'owl:' | kw= 'rdf:' )? (this_NSID_4= RULE_NSID | this_ID_5= RULE_ID ) (this_ANY_OTHER_PARAM_6= ruleANY_OTHER_PARAM )* (kw= '/>' | (kw= '>' ( (this_ANY_OTHER_TAG_9= ruleANY_OTHER_TAG )* | this_TAG_ANY_VALUE_10= ruleTAG_ANY_VALUE ) kw= '</' (this_NSID_12= RULE_NSID | this_ID_13= RULE_ID ) kw= '>' ) )
            {
            kw=(Token)match(input,58,FOLLOW_113); 

                    current.merge(kw);
                    newLeafNode(kw, grammarAccess.getANY_OTHER_TAGAccess().getLessThanSignKeyword_0()); 
                
            // InternalOWLModel.g:7890:1: (kw= 'rdfs:' | kw= 'owl:' | kw= 'rdf:' )?
            int alt147=4;
            switch ( input.LA(1) ) {
                case 132:
                    {
                    alt147=1;
                    }
                    break;
                case 133:
                    {
                    alt147=2;
                    }
                    break;
                case 134:
                    {
                    alt147=3;
                    }
                    break;
            }

            switch (alt147) {
                case 1 :
                    // InternalOWLModel.g:7891:2: kw= 'rdfs:'
                    {
                    kw=(Token)match(input,132,FOLLOW_55); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getANY_OTHER_TAGAccess().getRdfsKeyword_1_0()); 
                        

                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:7898:2: kw= 'owl:'
                    {
                    kw=(Token)match(input,133,FOLLOW_55); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getANY_OTHER_TAGAccess().getOwlKeyword_1_1()); 
                        

                    }
                    break;
                case 3 :
                    // InternalOWLModel.g:7905:2: kw= 'rdf:'
                    {
                    kw=(Token)match(input,134,FOLLOW_55); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getANY_OTHER_TAGAccess().getRdfKeyword_1_2()); 
                        

                    }
                    break;

            }

            // InternalOWLModel.g:7910:3: (this_NSID_4= RULE_NSID | this_ID_5= RULE_ID )
            int alt148=2;
            int LA148_0 = input.LA(1);

            if ( (LA148_0==RULE_NSID) ) {
                alt148=1;
            }
            else if ( (LA148_0==RULE_ID) ) {
                alt148=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 148, 0, input);

                throw nvae;
            }
            switch (alt148) {
                case 1 :
                    // InternalOWLModel.g:7910:8: this_NSID_4= RULE_NSID
                    {
                    this_NSID_4=(Token)match(input,RULE_NSID,FOLLOW_114); 

                    		current.merge(this_NSID_4);
                        
                     
                        newLeafNode(this_NSID_4, grammarAccess.getANY_OTHER_TAGAccess().getNSIDTerminalRuleCall_2_0()); 
                        

                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:7918:10: this_ID_5= RULE_ID
                    {
                    this_ID_5=(Token)match(input,RULE_ID,FOLLOW_114); 

                    		current.merge(this_ID_5);
                        
                     
                        newLeafNode(this_ID_5, grammarAccess.getANY_OTHER_TAGAccess().getIDTerminalRuleCall_2_1()); 
                        

                    }
                    break;

            }

            // InternalOWLModel.g:7925:2: (this_ANY_OTHER_PARAM_6= ruleANY_OTHER_PARAM )*
            loop149:
            do {
                int alt149=2;
                int LA149_0 = input.LA(1);

                if ( ((LA149_0>=RULE_NSID && LA149_0<=RULE_ID)||LA149_0==28||LA149_0==32||LA149_0==40||LA149_0==62) ) {
                    alt149=1;
                }


                switch (alt149) {
            	case 1 :
            	    // InternalOWLModel.g:7926:5: this_ANY_OTHER_PARAM_6= ruleANY_OTHER_PARAM
            	    {
            	     
            	            newCompositeNode(grammarAccess.getANY_OTHER_TAGAccess().getANY_OTHER_PARAMParserRuleCall_3()); 
            	        
            	    pushFollow(FOLLOW_114);
            	    this_ANY_OTHER_PARAM_6=ruleANY_OTHER_PARAM();

            	    state._fsp--;


            	    		current.merge(this_ANY_OTHER_PARAM_6);
            	        
            	     
            	            afterParserOrEnumRuleCall();
            	        

            	    }
            	    break;

            	default :
            	    break loop149;
                }
            } while (true);

            // InternalOWLModel.g:7936:3: (kw= '/>' | (kw= '>' ( (this_ANY_OTHER_TAG_9= ruleANY_OTHER_TAG )* | this_TAG_ANY_VALUE_10= ruleTAG_ANY_VALUE ) kw= '</' (this_NSID_12= RULE_NSID | this_ID_13= RULE_ID ) kw= '>' ) )
            int alt153=2;
            int LA153_0 = input.LA(1);

            if ( (LA153_0==29) ) {
                alt153=1;
            }
            else if ( (LA153_0==21) ) {
                alt153=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 153, 0, input);

                throw nvae;
            }
            switch (alt153) {
                case 1 :
                    // InternalOWLModel.g:7937:2: kw= '/>'
                    {
                    kw=(Token)match(input,29,FOLLOW_2); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getANY_OTHER_TAGAccess().getSolidusGreaterThanSignKeyword_4_0()); 
                        

                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:7943:6: (kw= '>' ( (this_ANY_OTHER_TAG_9= ruleANY_OTHER_TAG )* | this_TAG_ANY_VALUE_10= ruleTAG_ANY_VALUE ) kw= '</' (this_NSID_12= RULE_NSID | this_ID_13= RULE_ID ) kw= '>' )
                    {
                    // InternalOWLModel.g:7943:6: (kw= '>' ( (this_ANY_OTHER_TAG_9= ruleANY_OTHER_TAG )* | this_TAG_ANY_VALUE_10= ruleTAG_ANY_VALUE ) kw= '</' (this_NSID_12= RULE_NSID | this_ID_13= RULE_ID ) kw= '>' )
                    // InternalOWLModel.g:7944:2: kw= '>' ( (this_ANY_OTHER_TAG_9= ruleANY_OTHER_TAG )* | this_TAG_ANY_VALUE_10= ruleTAG_ANY_VALUE ) kw= '</' (this_NSID_12= RULE_NSID | this_ID_13= RULE_ID ) kw= '>'
                    {
                    kw=(Token)match(input,21,FOLLOW_115); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getANY_OTHER_TAGAccess().getGreaterThanSignKeyword_4_1_0()); 
                        
                    // InternalOWLModel.g:7949:1: ( (this_ANY_OTHER_TAG_9= ruleANY_OTHER_TAG )* | this_TAG_ANY_VALUE_10= ruleTAG_ANY_VALUE )
                    int alt151=2;
                    int LA151_0 = input.LA(1);

                    if ( (LA151_0==58||LA151_0==135) ) {
                        alt151=1;
                    }
                    else if ( ((LA151_0>=RULE_NSID && LA151_0<=RULE_SEMICOLON)||(LA151_0>=RULE_INT && LA151_0<=RULE_ANY_OTHER)||(LA151_0>=19 && LA151_0<=21)||LA151_0==25) ) {
                        alt151=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 151, 0, input);

                        throw nvae;
                    }
                    switch (alt151) {
                        case 1 :
                            // InternalOWLModel.g:7949:2: (this_ANY_OTHER_TAG_9= ruleANY_OTHER_TAG )*
                            {
                            // InternalOWLModel.g:7949:2: (this_ANY_OTHER_TAG_9= ruleANY_OTHER_TAG )*
                            loop150:
                            do {
                                int alt150=2;
                                int LA150_0 = input.LA(1);

                                if ( (LA150_0==58) ) {
                                    alt150=1;
                                }


                                switch (alt150) {
                            	case 1 :
                            	    // InternalOWLModel.g:7950:5: this_ANY_OTHER_TAG_9= ruleANY_OTHER_TAG
                            	    {
                            	     
                            	            newCompositeNode(grammarAccess.getANY_OTHER_TAGAccess().getANY_OTHER_TAGParserRuleCall_4_1_1_0()); 
                            	        
                            	    pushFollow(FOLLOW_116);
                            	    this_ANY_OTHER_TAG_9=ruleANY_OTHER_TAG();

                            	    state._fsp--;


                            	    		current.merge(this_ANY_OTHER_TAG_9);
                            	        
                            	     
                            	            afterParserOrEnumRuleCall();
                            	        

                            	    }
                            	    break;

                            	default :
                            	    break loop150;
                                }
                            } while (true);


                            }
                            break;
                        case 2 :
                            // InternalOWLModel.g:7962:5: this_TAG_ANY_VALUE_10= ruleTAG_ANY_VALUE
                            {
                             
                                    newCompositeNode(grammarAccess.getANY_OTHER_TAGAccess().getTAG_ANY_VALUEParserRuleCall_4_1_1_1()); 
                                
                            pushFollow(FOLLOW_117);
                            this_TAG_ANY_VALUE_10=ruleTAG_ANY_VALUE();

                            state._fsp--;


                            		current.merge(this_TAG_ANY_VALUE_10);
                                
                             
                                    afterParserOrEnumRuleCall();
                                

                            }
                            break;

                    }

                    kw=(Token)match(input,135,FOLLOW_55); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getANY_OTHER_TAGAccess().getLessThanSignSolidusKeyword_4_1_2()); 
                        
                    // InternalOWLModel.g:7978:1: (this_NSID_12= RULE_NSID | this_ID_13= RULE_ID )
                    int alt152=2;
                    int LA152_0 = input.LA(1);

                    if ( (LA152_0==RULE_NSID) ) {
                        alt152=1;
                    }
                    else if ( (LA152_0==RULE_ID) ) {
                        alt152=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 152, 0, input);

                        throw nvae;
                    }
                    switch (alt152) {
                        case 1 :
                            // InternalOWLModel.g:7978:6: this_NSID_12= RULE_NSID
                            {
                            this_NSID_12=(Token)match(input,RULE_NSID,FOLLOW_7); 

                            		current.merge(this_NSID_12);
                                
                             
                                newLeafNode(this_NSID_12, grammarAccess.getANY_OTHER_TAGAccess().getNSIDTerminalRuleCall_4_1_3_0()); 
                                

                            }
                            break;
                        case 2 :
                            // InternalOWLModel.g:7986:10: this_ID_13= RULE_ID
                            {
                            this_ID_13=(Token)match(input,RULE_ID,FOLLOW_7); 

                            		current.merge(this_ID_13);
                                
                             
                                newLeafNode(this_ID_13, grammarAccess.getANY_OTHER_TAGAccess().getIDTerminalRuleCall_4_1_3_1()); 
                                

                            }
                            break;

                    }

                    kw=(Token)match(input,21,FOLLOW_2); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getANY_OTHER_TAGAccess().getGreaterThanSignKeyword_4_1_4()); 
                        

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleANY_OTHER_TAG"


    // $ANTLR start "entryRuleANY_OTHER_PARAM"
    // InternalOWLModel.g:8007:1: entryRuleANY_OTHER_PARAM returns [String current=null] : iv_ruleANY_OTHER_PARAM= ruleANY_OTHER_PARAM EOF ;
    public final String entryRuleANY_OTHER_PARAM() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleANY_OTHER_PARAM = null;


        try {
            // InternalOWLModel.g:8008:2: (iv_ruleANY_OTHER_PARAM= ruleANY_OTHER_PARAM EOF )
            // InternalOWLModel.g:8009:2: iv_ruleANY_OTHER_PARAM= ruleANY_OTHER_PARAM EOF
            {
             newCompositeNode(grammarAccess.getANY_OTHER_PARAMRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleANY_OTHER_PARAM=ruleANY_OTHER_PARAM();

            state._fsp--;

             current =iv_ruleANY_OTHER_PARAM.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleANY_OTHER_PARAM"


    // $ANTLR start "ruleANY_OTHER_PARAM"
    // InternalOWLModel.g:8016:1: ruleANY_OTHER_PARAM returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (kw= 'rdf:resource' | kw= 'rdf:ID' | kw= 'rdf:datatype' | kw= 'rdf:about' | this_NSID_4= RULE_NSID | this_ID_5= RULE_ID ) kw= '=' this_STR_DELIMITER_7= RULE_STR_DELIMITER this_PARAM_ANY_VALUE_8= rulePARAM_ANY_VALUE this_STR_DELIMITER_9= RULE_STR_DELIMITER ) ;
    public final AntlrDatatypeRuleToken ruleANY_OTHER_PARAM() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_NSID_4=null;
        Token this_ID_5=null;
        Token this_STR_DELIMITER_7=null;
        Token this_STR_DELIMITER_9=null;
        AntlrDatatypeRuleToken this_PARAM_ANY_VALUE_8 = null;


         enterRule(); 
            
        try {
            // InternalOWLModel.g:8019:28: ( ( (kw= 'rdf:resource' | kw= 'rdf:ID' | kw= 'rdf:datatype' | kw= 'rdf:about' | this_NSID_4= RULE_NSID | this_ID_5= RULE_ID ) kw= '=' this_STR_DELIMITER_7= RULE_STR_DELIMITER this_PARAM_ANY_VALUE_8= rulePARAM_ANY_VALUE this_STR_DELIMITER_9= RULE_STR_DELIMITER ) )
            // InternalOWLModel.g:8020:1: ( (kw= 'rdf:resource' | kw= 'rdf:ID' | kw= 'rdf:datatype' | kw= 'rdf:about' | this_NSID_4= RULE_NSID | this_ID_5= RULE_ID ) kw= '=' this_STR_DELIMITER_7= RULE_STR_DELIMITER this_PARAM_ANY_VALUE_8= rulePARAM_ANY_VALUE this_STR_DELIMITER_9= RULE_STR_DELIMITER )
            {
            // InternalOWLModel.g:8020:1: ( (kw= 'rdf:resource' | kw= 'rdf:ID' | kw= 'rdf:datatype' | kw= 'rdf:about' | this_NSID_4= RULE_NSID | this_ID_5= RULE_ID ) kw= '=' this_STR_DELIMITER_7= RULE_STR_DELIMITER this_PARAM_ANY_VALUE_8= rulePARAM_ANY_VALUE this_STR_DELIMITER_9= RULE_STR_DELIMITER )
            // InternalOWLModel.g:8020:2: (kw= 'rdf:resource' | kw= 'rdf:ID' | kw= 'rdf:datatype' | kw= 'rdf:about' | this_NSID_4= RULE_NSID | this_ID_5= RULE_ID ) kw= '=' this_STR_DELIMITER_7= RULE_STR_DELIMITER this_PARAM_ANY_VALUE_8= rulePARAM_ANY_VALUE this_STR_DELIMITER_9= RULE_STR_DELIMITER
            {
            // InternalOWLModel.g:8020:2: (kw= 'rdf:resource' | kw= 'rdf:ID' | kw= 'rdf:datatype' | kw= 'rdf:about' | this_NSID_4= RULE_NSID | this_ID_5= RULE_ID )
            int alt154=6;
            switch ( input.LA(1) ) {
            case 32:
                {
                alt154=1;
                }
                break;
            case 40:
                {
                alt154=2;
                }
                break;
            case 62:
                {
                alt154=3;
                }
                break;
            case 28:
                {
                alt154=4;
                }
                break;
            case RULE_NSID:
                {
                alt154=5;
                }
                break;
            case RULE_ID:
                {
                alt154=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 154, 0, input);

                throw nvae;
            }

            switch (alt154) {
                case 1 :
                    // InternalOWLModel.g:8021:2: kw= 'rdf:resource'
                    {
                    kw=(Token)match(input,32,FOLLOW_13); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getANY_OTHER_PARAMAccess().getRdfResourceKeyword_0_0()); 
                        

                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:8028:2: kw= 'rdf:ID'
                    {
                    kw=(Token)match(input,40,FOLLOW_13); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getANY_OTHER_PARAMAccess().getRdfIDKeyword_0_1()); 
                        

                    }
                    break;
                case 3 :
                    // InternalOWLModel.g:8035:2: kw= 'rdf:datatype'
                    {
                    kw=(Token)match(input,62,FOLLOW_13); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getANY_OTHER_PARAMAccess().getRdfDatatypeKeyword_0_2()); 
                        

                    }
                    break;
                case 4 :
                    // InternalOWLModel.g:8042:2: kw= 'rdf:about'
                    {
                    kw=(Token)match(input,28,FOLLOW_13); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getANY_OTHER_PARAMAccess().getRdfAboutKeyword_0_3()); 
                        

                    }
                    break;
                case 5 :
                    // InternalOWLModel.g:8048:10: this_NSID_4= RULE_NSID
                    {
                    this_NSID_4=(Token)match(input,RULE_NSID,FOLLOW_13); 

                    		current.merge(this_NSID_4);
                        
                     
                        newLeafNode(this_NSID_4, grammarAccess.getANY_OTHER_PARAMAccess().getNSIDTerminalRuleCall_0_4()); 
                        

                    }
                    break;
                case 6 :
                    // InternalOWLModel.g:8056:10: this_ID_5= RULE_ID
                    {
                    this_ID_5=(Token)match(input,RULE_ID,FOLLOW_13); 

                    		current.merge(this_ID_5);
                        
                     
                        newLeafNode(this_ID_5, grammarAccess.getANY_OTHER_PARAMAccess().getIDTerminalRuleCall_0_5()); 
                        

                    }
                    break;

            }

            kw=(Token)match(input,25,FOLLOW_9); 

                    current.merge(kw);
                    newLeafNode(kw, grammarAccess.getANY_OTHER_PARAMAccess().getEqualsSignKeyword_1()); 
                
            this_STR_DELIMITER_7=(Token)match(input,RULE_STR_DELIMITER,FOLLOW_57); 

            		current.merge(this_STR_DELIMITER_7);
                
             
                newLeafNode(this_STR_DELIMITER_7, grammarAccess.getANY_OTHER_PARAMAccess().getSTR_DELIMITERTerminalRuleCall_2()); 
                
             
                    newCompositeNode(grammarAccess.getANY_OTHER_PARAMAccess().getPARAM_ANY_VALUEParserRuleCall_3()); 
                
            pushFollow(FOLLOW_9);
            this_PARAM_ANY_VALUE_8=rulePARAM_ANY_VALUE();

            state._fsp--;


            		current.merge(this_PARAM_ANY_VALUE_8);
                
             
                    afterParserOrEnumRuleCall();
                
            this_STR_DELIMITER_9=(Token)match(input,RULE_STR_DELIMITER,FOLLOW_2); 

            		current.merge(this_STR_DELIMITER_9);
                
             
                newLeafNode(this_STR_DELIMITER_9, grammarAccess.getANY_OTHER_PARAMAccess().getSTR_DELIMITERTerminalRuleCall_4()); 
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleANY_OTHER_PARAM"


    // $ANTLR start "entryRuleTAG_ANY_VALUE"
    // InternalOWLModel.g:8102:1: entryRuleTAG_ANY_VALUE returns [String current=null] : iv_ruleTAG_ANY_VALUE= ruleTAG_ANY_VALUE EOF ;
    public final String entryRuleTAG_ANY_VALUE() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleTAG_ANY_VALUE = null;


        try {
            // InternalOWLModel.g:8103:2: (iv_ruleTAG_ANY_VALUE= ruleTAG_ANY_VALUE EOF )
            // InternalOWLModel.g:8104:2: iv_ruleTAG_ANY_VALUE= ruleTAG_ANY_VALUE EOF
            {
             newCompositeNode(grammarAccess.getTAG_ANY_VALUERule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTAG_ANY_VALUE=ruleTAG_ANY_VALUE();

            state._fsp--;

             current =iv_ruleTAG_ANY_VALUE.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTAG_ANY_VALUE"


    // $ANTLR start "ruleTAG_ANY_VALUE"
    // InternalOWLModel.g:8111:1: ruleTAG_ANY_VALUE returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (this_URI_0= RULE_URI | this_NSID_1= RULE_NSID | this_ID_2= RULE_ID | this_INT_3= RULE_INT | this_AMPERSAND_4= RULE_AMPERSAND | this_STR_DELIMITER_5= RULE_STR_DELIMITER | this_HASHTAG_6= RULE_HASHTAG | this_SEMICOLON_7= RULE_SEMICOLON | kw= '[' | kw= ']' | kw= '=' | this_ANY_OTHER_11= RULE_ANY_OTHER | kw= '>' ) (this_AMPERSAND_13= RULE_AMPERSAND | this_HASHTAG_14= RULE_HASHTAG | this_SEMICOLON_15= RULE_SEMICOLON | kw= '[' | kw= ']' | this_ANY_OTHER_18= RULE_ANY_OTHER | kw= '>' | this_STR_DELIMITER_20= RULE_STR_DELIMITER | this_URI_21= RULE_URI | this_NSID_22= RULE_NSID | this_ID_23= RULE_ID | this_INT_24= RULE_INT )* ) ;
    public final AntlrDatatypeRuleToken ruleTAG_ANY_VALUE() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_URI_0=null;
        Token this_NSID_1=null;
        Token this_ID_2=null;
        Token this_INT_3=null;
        Token this_AMPERSAND_4=null;
        Token this_STR_DELIMITER_5=null;
        Token this_HASHTAG_6=null;
        Token this_SEMICOLON_7=null;
        Token kw=null;
        Token this_ANY_OTHER_11=null;
        Token this_AMPERSAND_13=null;
        Token this_HASHTAG_14=null;
        Token this_SEMICOLON_15=null;
        Token this_ANY_OTHER_18=null;
        Token this_STR_DELIMITER_20=null;
        Token this_URI_21=null;
        Token this_NSID_22=null;
        Token this_ID_23=null;
        Token this_INT_24=null;

         enterRule(); 
            
        try {
            // InternalOWLModel.g:8114:28: ( ( (this_URI_0= RULE_URI | this_NSID_1= RULE_NSID | this_ID_2= RULE_ID | this_INT_3= RULE_INT | this_AMPERSAND_4= RULE_AMPERSAND | this_STR_DELIMITER_5= RULE_STR_DELIMITER | this_HASHTAG_6= RULE_HASHTAG | this_SEMICOLON_7= RULE_SEMICOLON | kw= '[' | kw= ']' | kw= '=' | this_ANY_OTHER_11= RULE_ANY_OTHER | kw= '>' ) (this_AMPERSAND_13= RULE_AMPERSAND | this_HASHTAG_14= RULE_HASHTAG | this_SEMICOLON_15= RULE_SEMICOLON | kw= '[' | kw= ']' | this_ANY_OTHER_18= RULE_ANY_OTHER | kw= '>' | this_STR_DELIMITER_20= RULE_STR_DELIMITER | this_URI_21= RULE_URI | this_NSID_22= RULE_NSID | this_ID_23= RULE_ID | this_INT_24= RULE_INT )* ) )
            // InternalOWLModel.g:8115:1: ( (this_URI_0= RULE_URI | this_NSID_1= RULE_NSID | this_ID_2= RULE_ID | this_INT_3= RULE_INT | this_AMPERSAND_4= RULE_AMPERSAND | this_STR_DELIMITER_5= RULE_STR_DELIMITER | this_HASHTAG_6= RULE_HASHTAG | this_SEMICOLON_7= RULE_SEMICOLON | kw= '[' | kw= ']' | kw= '=' | this_ANY_OTHER_11= RULE_ANY_OTHER | kw= '>' ) (this_AMPERSAND_13= RULE_AMPERSAND | this_HASHTAG_14= RULE_HASHTAG | this_SEMICOLON_15= RULE_SEMICOLON | kw= '[' | kw= ']' | this_ANY_OTHER_18= RULE_ANY_OTHER | kw= '>' | this_STR_DELIMITER_20= RULE_STR_DELIMITER | this_URI_21= RULE_URI | this_NSID_22= RULE_NSID | this_ID_23= RULE_ID | this_INT_24= RULE_INT )* )
            {
            // InternalOWLModel.g:8115:1: ( (this_URI_0= RULE_URI | this_NSID_1= RULE_NSID | this_ID_2= RULE_ID | this_INT_3= RULE_INT | this_AMPERSAND_4= RULE_AMPERSAND | this_STR_DELIMITER_5= RULE_STR_DELIMITER | this_HASHTAG_6= RULE_HASHTAG | this_SEMICOLON_7= RULE_SEMICOLON | kw= '[' | kw= ']' | kw= '=' | this_ANY_OTHER_11= RULE_ANY_OTHER | kw= '>' ) (this_AMPERSAND_13= RULE_AMPERSAND | this_HASHTAG_14= RULE_HASHTAG | this_SEMICOLON_15= RULE_SEMICOLON | kw= '[' | kw= ']' | this_ANY_OTHER_18= RULE_ANY_OTHER | kw= '>' | this_STR_DELIMITER_20= RULE_STR_DELIMITER | this_URI_21= RULE_URI | this_NSID_22= RULE_NSID | this_ID_23= RULE_ID | this_INT_24= RULE_INT )* )
            // InternalOWLModel.g:8115:2: (this_URI_0= RULE_URI | this_NSID_1= RULE_NSID | this_ID_2= RULE_ID | this_INT_3= RULE_INT | this_AMPERSAND_4= RULE_AMPERSAND | this_STR_DELIMITER_5= RULE_STR_DELIMITER | this_HASHTAG_6= RULE_HASHTAG | this_SEMICOLON_7= RULE_SEMICOLON | kw= '[' | kw= ']' | kw= '=' | this_ANY_OTHER_11= RULE_ANY_OTHER | kw= '>' ) (this_AMPERSAND_13= RULE_AMPERSAND | this_HASHTAG_14= RULE_HASHTAG | this_SEMICOLON_15= RULE_SEMICOLON | kw= '[' | kw= ']' | this_ANY_OTHER_18= RULE_ANY_OTHER | kw= '>' | this_STR_DELIMITER_20= RULE_STR_DELIMITER | this_URI_21= RULE_URI | this_NSID_22= RULE_NSID | this_ID_23= RULE_ID | this_INT_24= RULE_INT )*
            {
            // InternalOWLModel.g:8115:2: (this_URI_0= RULE_URI | this_NSID_1= RULE_NSID | this_ID_2= RULE_ID | this_INT_3= RULE_INT | this_AMPERSAND_4= RULE_AMPERSAND | this_STR_DELIMITER_5= RULE_STR_DELIMITER | this_HASHTAG_6= RULE_HASHTAG | this_SEMICOLON_7= RULE_SEMICOLON | kw= '[' | kw= ']' | kw= '=' | this_ANY_OTHER_11= RULE_ANY_OTHER | kw= '>' )
            int alt155=13;
            switch ( input.LA(1) ) {
            case RULE_URI:
                {
                alt155=1;
                }
                break;
            case RULE_NSID:
                {
                alt155=2;
                }
                break;
            case RULE_ID:
                {
                alt155=3;
                }
                break;
            case RULE_INT:
                {
                alt155=4;
                }
                break;
            case RULE_AMPERSAND:
                {
                alt155=5;
                }
                break;
            case RULE_STR_DELIMITER:
                {
                alt155=6;
                }
                break;
            case RULE_HASHTAG:
                {
                alt155=7;
                }
                break;
            case RULE_SEMICOLON:
                {
                alt155=8;
                }
                break;
            case 19:
                {
                alt155=9;
                }
                break;
            case 20:
                {
                alt155=10;
                }
                break;
            case 25:
                {
                alt155=11;
                }
                break;
            case RULE_ANY_OTHER:
                {
                alt155=12;
                }
                break;
            case 21:
                {
                alt155=13;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 155, 0, input);

                throw nvae;
            }

            switch (alt155) {
                case 1 :
                    // InternalOWLModel.g:8115:7: this_URI_0= RULE_URI
                    {
                    this_URI_0=(Token)match(input,RULE_URI,FOLLOW_118); 

                    		current.merge(this_URI_0);
                        
                     
                        newLeafNode(this_URI_0, grammarAccess.getTAG_ANY_VALUEAccess().getURITerminalRuleCall_0_0()); 
                        

                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:8123:10: this_NSID_1= RULE_NSID
                    {
                    this_NSID_1=(Token)match(input,RULE_NSID,FOLLOW_118); 

                    		current.merge(this_NSID_1);
                        
                     
                        newLeafNode(this_NSID_1, grammarAccess.getTAG_ANY_VALUEAccess().getNSIDTerminalRuleCall_0_1()); 
                        

                    }
                    break;
                case 3 :
                    // InternalOWLModel.g:8131:10: this_ID_2= RULE_ID
                    {
                    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_118); 

                    		current.merge(this_ID_2);
                        
                     
                        newLeafNode(this_ID_2, grammarAccess.getTAG_ANY_VALUEAccess().getIDTerminalRuleCall_0_2()); 
                        

                    }
                    break;
                case 4 :
                    // InternalOWLModel.g:8139:10: this_INT_3= RULE_INT
                    {
                    this_INT_3=(Token)match(input,RULE_INT,FOLLOW_118); 

                    		current.merge(this_INT_3);
                        
                     
                        newLeafNode(this_INT_3, grammarAccess.getTAG_ANY_VALUEAccess().getINTTerminalRuleCall_0_3()); 
                        

                    }
                    break;
                case 5 :
                    // InternalOWLModel.g:8147:10: this_AMPERSAND_4= RULE_AMPERSAND
                    {
                    this_AMPERSAND_4=(Token)match(input,RULE_AMPERSAND,FOLLOW_118); 

                    		current.merge(this_AMPERSAND_4);
                        
                     
                        newLeafNode(this_AMPERSAND_4, grammarAccess.getTAG_ANY_VALUEAccess().getAMPERSANDTerminalRuleCall_0_4()); 
                        

                    }
                    break;
                case 6 :
                    // InternalOWLModel.g:8155:10: this_STR_DELIMITER_5= RULE_STR_DELIMITER
                    {
                    this_STR_DELIMITER_5=(Token)match(input,RULE_STR_DELIMITER,FOLLOW_118); 

                    		current.merge(this_STR_DELIMITER_5);
                        
                     
                        newLeafNode(this_STR_DELIMITER_5, grammarAccess.getTAG_ANY_VALUEAccess().getSTR_DELIMITERTerminalRuleCall_0_5()); 
                        

                    }
                    break;
                case 7 :
                    // InternalOWLModel.g:8163:10: this_HASHTAG_6= RULE_HASHTAG
                    {
                    this_HASHTAG_6=(Token)match(input,RULE_HASHTAG,FOLLOW_118); 

                    		current.merge(this_HASHTAG_6);
                        
                     
                        newLeafNode(this_HASHTAG_6, grammarAccess.getTAG_ANY_VALUEAccess().getHASHTAGTerminalRuleCall_0_6()); 
                        

                    }
                    break;
                case 8 :
                    // InternalOWLModel.g:8171:10: this_SEMICOLON_7= RULE_SEMICOLON
                    {
                    this_SEMICOLON_7=(Token)match(input,RULE_SEMICOLON,FOLLOW_118); 

                    		current.merge(this_SEMICOLON_7);
                        
                     
                        newLeafNode(this_SEMICOLON_7, grammarAccess.getTAG_ANY_VALUEAccess().getSEMICOLONTerminalRuleCall_0_7()); 
                        

                    }
                    break;
                case 9 :
                    // InternalOWLModel.g:8180:2: kw= '['
                    {
                    kw=(Token)match(input,19,FOLLOW_118); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getTAG_ANY_VALUEAccess().getLeftSquareBracketKeyword_0_8()); 
                        

                    }
                    break;
                case 10 :
                    // InternalOWLModel.g:8187:2: kw= ']'
                    {
                    kw=(Token)match(input,20,FOLLOW_118); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getTAG_ANY_VALUEAccess().getRightSquareBracketKeyword_0_9()); 
                        

                    }
                    break;
                case 11 :
                    // InternalOWLModel.g:8194:2: kw= '='
                    {
                    kw=(Token)match(input,25,FOLLOW_118); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getTAG_ANY_VALUEAccess().getEqualsSignKeyword_0_10()); 
                        

                    }
                    break;
                case 12 :
                    // InternalOWLModel.g:8200:10: this_ANY_OTHER_11= RULE_ANY_OTHER
                    {
                    this_ANY_OTHER_11=(Token)match(input,RULE_ANY_OTHER,FOLLOW_118); 

                    		current.merge(this_ANY_OTHER_11);
                        
                     
                        newLeafNode(this_ANY_OTHER_11, grammarAccess.getTAG_ANY_VALUEAccess().getANY_OTHERTerminalRuleCall_0_11()); 
                        

                    }
                    break;
                case 13 :
                    // InternalOWLModel.g:8209:2: kw= '>'
                    {
                    kw=(Token)match(input,21,FOLLOW_118); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getTAG_ANY_VALUEAccess().getGreaterThanSignKeyword_0_12()); 
                        

                    }
                    break;

            }

            // InternalOWLModel.g:8214:2: (this_AMPERSAND_13= RULE_AMPERSAND | this_HASHTAG_14= RULE_HASHTAG | this_SEMICOLON_15= RULE_SEMICOLON | kw= '[' | kw= ']' | this_ANY_OTHER_18= RULE_ANY_OTHER | kw= '>' | this_STR_DELIMITER_20= RULE_STR_DELIMITER | this_URI_21= RULE_URI | this_NSID_22= RULE_NSID | this_ID_23= RULE_ID | this_INT_24= RULE_INT )*
            loop156:
            do {
                int alt156=13;
                switch ( input.LA(1) ) {
                case RULE_AMPERSAND:
                    {
                    alt156=1;
                    }
                    break;
                case RULE_HASHTAG:
                    {
                    alt156=2;
                    }
                    break;
                case RULE_SEMICOLON:
                    {
                    alt156=3;
                    }
                    break;
                case 19:
                    {
                    alt156=4;
                    }
                    break;
                case 20:
                    {
                    alt156=5;
                    }
                    break;
                case RULE_ANY_OTHER:
                    {
                    alt156=6;
                    }
                    break;
                case 21:
                    {
                    alt156=7;
                    }
                    break;
                case RULE_STR_DELIMITER:
                    {
                    alt156=8;
                    }
                    break;
                case RULE_URI:
                    {
                    alt156=9;
                    }
                    break;
                case RULE_NSID:
                    {
                    alt156=10;
                    }
                    break;
                case RULE_ID:
                    {
                    alt156=11;
                    }
                    break;
                case RULE_INT:
                    {
                    alt156=12;
                    }
                    break;

                }

                switch (alt156) {
            	case 1 :
            	    // InternalOWLModel.g:8214:7: this_AMPERSAND_13= RULE_AMPERSAND
            	    {
            	    this_AMPERSAND_13=(Token)match(input,RULE_AMPERSAND,FOLLOW_118); 

            	    		current.merge(this_AMPERSAND_13);
            	        
            	     
            	        newLeafNode(this_AMPERSAND_13, grammarAccess.getTAG_ANY_VALUEAccess().getAMPERSANDTerminalRuleCall_1_0()); 
            	        

            	    }
            	    break;
            	case 2 :
            	    // InternalOWLModel.g:8222:10: this_HASHTAG_14= RULE_HASHTAG
            	    {
            	    this_HASHTAG_14=(Token)match(input,RULE_HASHTAG,FOLLOW_118); 

            	    		current.merge(this_HASHTAG_14);
            	        
            	     
            	        newLeafNode(this_HASHTAG_14, grammarAccess.getTAG_ANY_VALUEAccess().getHASHTAGTerminalRuleCall_1_1()); 
            	        

            	    }
            	    break;
            	case 3 :
            	    // InternalOWLModel.g:8230:10: this_SEMICOLON_15= RULE_SEMICOLON
            	    {
            	    this_SEMICOLON_15=(Token)match(input,RULE_SEMICOLON,FOLLOW_118); 

            	    		current.merge(this_SEMICOLON_15);
            	        
            	     
            	        newLeafNode(this_SEMICOLON_15, grammarAccess.getTAG_ANY_VALUEAccess().getSEMICOLONTerminalRuleCall_1_2()); 
            	        

            	    }
            	    break;
            	case 4 :
            	    // InternalOWLModel.g:8239:2: kw= '['
            	    {
            	    kw=(Token)match(input,19,FOLLOW_118); 

            	            current.merge(kw);
            	            newLeafNode(kw, grammarAccess.getTAG_ANY_VALUEAccess().getLeftSquareBracketKeyword_1_3()); 
            	        

            	    }
            	    break;
            	case 5 :
            	    // InternalOWLModel.g:8246:2: kw= ']'
            	    {
            	    kw=(Token)match(input,20,FOLLOW_118); 

            	            current.merge(kw);
            	            newLeafNode(kw, grammarAccess.getTAG_ANY_VALUEAccess().getRightSquareBracketKeyword_1_4()); 
            	        

            	    }
            	    break;
            	case 6 :
            	    // InternalOWLModel.g:8252:10: this_ANY_OTHER_18= RULE_ANY_OTHER
            	    {
            	    this_ANY_OTHER_18=(Token)match(input,RULE_ANY_OTHER,FOLLOW_118); 

            	    		current.merge(this_ANY_OTHER_18);
            	        
            	     
            	        newLeafNode(this_ANY_OTHER_18, grammarAccess.getTAG_ANY_VALUEAccess().getANY_OTHERTerminalRuleCall_1_5()); 
            	        

            	    }
            	    break;
            	case 7 :
            	    // InternalOWLModel.g:8261:2: kw= '>'
            	    {
            	    kw=(Token)match(input,21,FOLLOW_118); 

            	            current.merge(kw);
            	            newLeafNode(kw, grammarAccess.getTAG_ANY_VALUEAccess().getGreaterThanSignKeyword_1_6()); 
            	        

            	    }
            	    break;
            	case 8 :
            	    // InternalOWLModel.g:8267:10: this_STR_DELIMITER_20= RULE_STR_DELIMITER
            	    {
            	    this_STR_DELIMITER_20=(Token)match(input,RULE_STR_DELIMITER,FOLLOW_118); 

            	    		current.merge(this_STR_DELIMITER_20);
            	        
            	     
            	        newLeafNode(this_STR_DELIMITER_20, grammarAccess.getTAG_ANY_VALUEAccess().getSTR_DELIMITERTerminalRuleCall_1_7()); 
            	        

            	    }
            	    break;
            	case 9 :
            	    // InternalOWLModel.g:8275:10: this_URI_21= RULE_URI
            	    {
            	    this_URI_21=(Token)match(input,RULE_URI,FOLLOW_118); 

            	    		current.merge(this_URI_21);
            	        
            	     
            	        newLeafNode(this_URI_21, grammarAccess.getTAG_ANY_VALUEAccess().getURITerminalRuleCall_1_8()); 
            	        

            	    }
            	    break;
            	case 10 :
            	    // InternalOWLModel.g:8283:10: this_NSID_22= RULE_NSID
            	    {
            	    this_NSID_22=(Token)match(input,RULE_NSID,FOLLOW_118); 

            	    		current.merge(this_NSID_22);
            	        
            	     
            	        newLeafNode(this_NSID_22, grammarAccess.getTAG_ANY_VALUEAccess().getNSIDTerminalRuleCall_1_9()); 
            	        

            	    }
            	    break;
            	case 11 :
            	    // InternalOWLModel.g:8291:10: this_ID_23= RULE_ID
            	    {
            	    this_ID_23=(Token)match(input,RULE_ID,FOLLOW_118); 

            	    		current.merge(this_ID_23);
            	        
            	     
            	        newLeafNode(this_ID_23, grammarAccess.getTAG_ANY_VALUEAccess().getIDTerminalRuleCall_1_10()); 
            	        

            	    }
            	    break;
            	case 12 :
            	    // InternalOWLModel.g:8299:10: this_INT_24= RULE_INT
            	    {
            	    this_INT_24=(Token)match(input,RULE_INT,FOLLOW_118); 

            	    		current.merge(this_INT_24);
            	        
            	     
            	        newLeafNode(this_INT_24, grammarAccess.getTAG_ANY_VALUEAccess().getINTTerminalRuleCall_1_11()); 
            	        

            	    }
            	    break;

            	default :
            	    break loop156;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTAG_ANY_VALUE"


    // $ANTLR start "entryRulePARAM_ANY_VALUE"
    // InternalOWLModel.g:8314:1: entryRulePARAM_ANY_VALUE returns [String current=null] : iv_rulePARAM_ANY_VALUE= rulePARAM_ANY_VALUE EOF ;
    public final String entryRulePARAM_ANY_VALUE() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_rulePARAM_ANY_VALUE = null;


        try {
            // InternalOWLModel.g:8315:2: (iv_rulePARAM_ANY_VALUE= rulePARAM_ANY_VALUE EOF )
            // InternalOWLModel.g:8316:2: iv_rulePARAM_ANY_VALUE= rulePARAM_ANY_VALUE EOF
            {
             newCompositeNode(grammarAccess.getPARAM_ANY_VALUERule()); 
            pushFollow(FOLLOW_1);
            iv_rulePARAM_ANY_VALUE=rulePARAM_ANY_VALUE();

            state._fsp--;

             current =iv_rulePARAM_ANY_VALUE.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePARAM_ANY_VALUE"


    // $ANTLR start "rulePARAM_ANY_VALUE"
    // InternalOWLModel.g:8323:1: rulePARAM_ANY_VALUE returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (this_URI_0= RULE_URI | this_NSID_1= RULE_NSID | this_ID_2= RULE_ID | this_INT_3= RULE_INT | this_AMPERSAND_4= RULE_AMPERSAND | this_HASHTAG_5= RULE_HASHTAG | this_SEMICOLON_6= RULE_SEMICOLON | kw= '[' | kw= ']' | kw= '=' | this_ANY_OTHER_10= RULE_ANY_OTHER ) (this_AMPERSAND_11= RULE_AMPERSAND | this_HASHTAG_12= RULE_HASHTAG | this_SEMICOLON_13= RULE_SEMICOLON | kw= '[' | kw= ']' | kw= '=' | this_ANY_OTHER_17= RULE_ANY_OTHER | this_URI_18= RULE_URI | this_NSID_19= RULE_NSID | this_ID_20= RULE_ID | this_INT_21= RULE_INT )* ) ;
    public final AntlrDatatypeRuleToken rulePARAM_ANY_VALUE() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_URI_0=null;
        Token this_NSID_1=null;
        Token this_ID_2=null;
        Token this_INT_3=null;
        Token this_AMPERSAND_4=null;
        Token this_HASHTAG_5=null;
        Token this_SEMICOLON_6=null;
        Token kw=null;
        Token this_ANY_OTHER_10=null;
        Token this_AMPERSAND_11=null;
        Token this_HASHTAG_12=null;
        Token this_SEMICOLON_13=null;
        Token this_ANY_OTHER_17=null;
        Token this_URI_18=null;
        Token this_NSID_19=null;
        Token this_ID_20=null;
        Token this_INT_21=null;

         enterRule(); 
            
        try {
            // InternalOWLModel.g:8326:28: ( ( (this_URI_0= RULE_URI | this_NSID_1= RULE_NSID | this_ID_2= RULE_ID | this_INT_3= RULE_INT | this_AMPERSAND_4= RULE_AMPERSAND | this_HASHTAG_5= RULE_HASHTAG | this_SEMICOLON_6= RULE_SEMICOLON | kw= '[' | kw= ']' | kw= '=' | this_ANY_OTHER_10= RULE_ANY_OTHER ) (this_AMPERSAND_11= RULE_AMPERSAND | this_HASHTAG_12= RULE_HASHTAG | this_SEMICOLON_13= RULE_SEMICOLON | kw= '[' | kw= ']' | kw= '=' | this_ANY_OTHER_17= RULE_ANY_OTHER | this_URI_18= RULE_URI | this_NSID_19= RULE_NSID | this_ID_20= RULE_ID | this_INT_21= RULE_INT )* ) )
            // InternalOWLModel.g:8327:1: ( (this_URI_0= RULE_URI | this_NSID_1= RULE_NSID | this_ID_2= RULE_ID | this_INT_3= RULE_INT | this_AMPERSAND_4= RULE_AMPERSAND | this_HASHTAG_5= RULE_HASHTAG | this_SEMICOLON_6= RULE_SEMICOLON | kw= '[' | kw= ']' | kw= '=' | this_ANY_OTHER_10= RULE_ANY_OTHER ) (this_AMPERSAND_11= RULE_AMPERSAND | this_HASHTAG_12= RULE_HASHTAG | this_SEMICOLON_13= RULE_SEMICOLON | kw= '[' | kw= ']' | kw= '=' | this_ANY_OTHER_17= RULE_ANY_OTHER | this_URI_18= RULE_URI | this_NSID_19= RULE_NSID | this_ID_20= RULE_ID | this_INT_21= RULE_INT )* )
            {
            // InternalOWLModel.g:8327:1: ( (this_URI_0= RULE_URI | this_NSID_1= RULE_NSID | this_ID_2= RULE_ID | this_INT_3= RULE_INT | this_AMPERSAND_4= RULE_AMPERSAND | this_HASHTAG_5= RULE_HASHTAG | this_SEMICOLON_6= RULE_SEMICOLON | kw= '[' | kw= ']' | kw= '=' | this_ANY_OTHER_10= RULE_ANY_OTHER ) (this_AMPERSAND_11= RULE_AMPERSAND | this_HASHTAG_12= RULE_HASHTAG | this_SEMICOLON_13= RULE_SEMICOLON | kw= '[' | kw= ']' | kw= '=' | this_ANY_OTHER_17= RULE_ANY_OTHER | this_URI_18= RULE_URI | this_NSID_19= RULE_NSID | this_ID_20= RULE_ID | this_INT_21= RULE_INT )* )
            // InternalOWLModel.g:8327:2: (this_URI_0= RULE_URI | this_NSID_1= RULE_NSID | this_ID_2= RULE_ID | this_INT_3= RULE_INT | this_AMPERSAND_4= RULE_AMPERSAND | this_HASHTAG_5= RULE_HASHTAG | this_SEMICOLON_6= RULE_SEMICOLON | kw= '[' | kw= ']' | kw= '=' | this_ANY_OTHER_10= RULE_ANY_OTHER ) (this_AMPERSAND_11= RULE_AMPERSAND | this_HASHTAG_12= RULE_HASHTAG | this_SEMICOLON_13= RULE_SEMICOLON | kw= '[' | kw= ']' | kw= '=' | this_ANY_OTHER_17= RULE_ANY_OTHER | this_URI_18= RULE_URI | this_NSID_19= RULE_NSID | this_ID_20= RULE_ID | this_INT_21= RULE_INT )*
            {
            // InternalOWLModel.g:8327:2: (this_URI_0= RULE_URI | this_NSID_1= RULE_NSID | this_ID_2= RULE_ID | this_INT_3= RULE_INT | this_AMPERSAND_4= RULE_AMPERSAND | this_HASHTAG_5= RULE_HASHTAG | this_SEMICOLON_6= RULE_SEMICOLON | kw= '[' | kw= ']' | kw= '=' | this_ANY_OTHER_10= RULE_ANY_OTHER )
            int alt157=11;
            switch ( input.LA(1) ) {
            case RULE_URI:
                {
                alt157=1;
                }
                break;
            case RULE_NSID:
                {
                alt157=2;
                }
                break;
            case RULE_ID:
                {
                alt157=3;
                }
                break;
            case RULE_INT:
                {
                alt157=4;
                }
                break;
            case RULE_AMPERSAND:
                {
                alt157=5;
                }
                break;
            case RULE_HASHTAG:
                {
                alt157=6;
                }
                break;
            case RULE_SEMICOLON:
                {
                alt157=7;
                }
                break;
            case 19:
                {
                alt157=8;
                }
                break;
            case 20:
                {
                alt157=9;
                }
                break;
            case 25:
                {
                alt157=10;
                }
                break;
            case RULE_ANY_OTHER:
                {
                alt157=11;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 157, 0, input);

                throw nvae;
            }

            switch (alt157) {
                case 1 :
                    // InternalOWLModel.g:8327:7: this_URI_0= RULE_URI
                    {
                    this_URI_0=(Token)match(input,RULE_URI,FOLLOW_119); 

                    		current.merge(this_URI_0);
                        
                     
                        newLeafNode(this_URI_0, grammarAccess.getPARAM_ANY_VALUEAccess().getURITerminalRuleCall_0_0()); 
                        

                    }
                    break;
                case 2 :
                    // InternalOWLModel.g:8335:10: this_NSID_1= RULE_NSID
                    {
                    this_NSID_1=(Token)match(input,RULE_NSID,FOLLOW_119); 

                    		current.merge(this_NSID_1);
                        
                     
                        newLeafNode(this_NSID_1, grammarAccess.getPARAM_ANY_VALUEAccess().getNSIDTerminalRuleCall_0_1()); 
                        

                    }
                    break;
                case 3 :
                    // InternalOWLModel.g:8343:10: this_ID_2= RULE_ID
                    {
                    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_119); 

                    		current.merge(this_ID_2);
                        
                     
                        newLeafNode(this_ID_2, grammarAccess.getPARAM_ANY_VALUEAccess().getIDTerminalRuleCall_0_2()); 
                        

                    }
                    break;
                case 4 :
                    // InternalOWLModel.g:8351:10: this_INT_3= RULE_INT
                    {
                    this_INT_3=(Token)match(input,RULE_INT,FOLLOW_119); 

                    		current.merge(this_INT_3);
                        
                     
                        newLeafNode(this_INT_3, grammarAccess.getPARAM_ANY_VALUEAccess().getINTTerminalRuleCall_0_3()); 
                        

                    }
                    break;
                case 5 :
                    // InternalOWLModel.g:8359:10: this_AMPERSAND_4= RULE_AMPERSAND
                    {
                    this_AMPERSAND_4=(Token)match(input,RULE_AMPERSAND,FOLLOW_119); 

                    		current.merge(this_AMPERSAND_4);
                        
                     
                        newLeafNode(this_AMPERSAND_4, grammarAccess.getPARAM_ANY_VALUEAccess().getAMPERSANDTerminalRuleCall_0_4()); 
                        

                    }
                    break;
                case 6 :
                    // InternalOWLModel.g:8367:10: this_HASHTAG_5= RULE_HASHTAG
                    {
                    this_HASHTAG_5=(Token)match(input,RULE_HASHTAG,FOLLOW_119); 

                    		current.merge(this_HASHTAG_5);
                        
                     
                        newLeafNode(this_HASHTAG_5, grammarAccess.getPARAM_ANY_VALUEAccess().getHASHTAGTerminalRuleCall_0_5()); 
                        

                    }
                    break;
                case 7 :
                    // InternalOWLModel.g:8375:10: this_SEMICOLON_6= RULE_SEMICOLON
                    {
                    this_SEMICOLON_6=(Token)match(input,RULE_SEMICOLON,FOLLOW_119); 

                    		current.merge(this_SEMICOLON_6);
                        
                     
                        newLeafNode(this_SEMICOLON_6, grammarAccess.getPARAM_ANY_VALUEAccess().getSEMICOLONTerminalRuleCall_0_6()); 
                        

                    }
                    break;
                case 8 :
                    // InternalOWLModel.g:8384:2: kw= '['
                    {
                    kw=(Token)match(input,19,FOLLOW_119); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getPARAM_ANY_VALUEAccess().getLeftSquareBracketKeyword_0_7()); 
                        

                    }
                    break;
                case 9 :
                    // InternalOWLModel.g:8391:2: kw= ']'
                    {
                    kw=(Token)match(input,20,FOLLOW_119); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getPARAM_ANY_VALUEAccess().getRightSquareBracketKeyword_0_8()); 
                        

                    }
                    break;
                case 10 :
                    // InternalOWLModel.g:8398:2: kw= '='
                    {
                    kw=(Token)match(input,25,FOLLOW_119); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getPARAM_ANY_VALUEAccess().getEqualsSignKeyword_0_9()); 
                        

                    }
                    break;
                case 11 :
                    // InternalOWLModel.g:8404:10: this_ANY_OTHER_10= RULE_ANY_OTHER
                    {
                    this_ANY_OTHER_10=(Token)match(input,RULE_ANY_OTHER,FOLLOW_119); 

                    		current.merge(this_ANY_OTHER_10);
                        
                     
                        newLeafNode(this_ANY_OTHER_10, grammarAccess.getPARAM_ANY_VALUEAccess().getANY_OTHERTerminalRuleCall_0_10()); 
                        

                    }
                    break;

            }

            // InternalOWLModel.g:8411:2: (this_AMPERSAND_11= RULE_AMPERSAND | this_HASHTAG_12= RULE_HASHTAG | this_SEMICOLON_13= RULE_SEMICOLON | kw= '[' | kw= ']' | kw= '=' | this_ANY_OTHER_17= RULE_ANY_OTHER | this_URI_18= RULE_URI | this_NSID_19= RULE_NSID | this_ID_20= RULE_ID | this_INT_21= RULE_INT )*
            loop158:
            do {
                int alt158=12;
                switch ( input.LA(1) ) {
                case RULE_AMPERSAND:
                    {
                    alt158=1;
                    }
                    break;
                case RULE_HASHTAG:
                    {
                    alt158=2;
                    }
                    break;
                case RULE_SEMICOLON:
                    {
                    alt158=3;
                    }
                    break;
                case 19:
                    {
                    alt158=4;
                    }
                    break;
                case 20:
                    {
                    alt158=5;
                    }
                    break;
                case 25:
                    {
                    alt158=6;
                    }
                    break;
                case RULE_ANY_OTHER:
                    {
                    alt158=7;
                    }
                    break;
                case RULE_URI:
                    {
                    alt158=8;
                    }
                    break;
                case RULE_NSID:
                    {
                    alt158=9;
                    }
                    break;
                case RULE_ID:
                    {
                    alt158=10;
                    }
                    break;
                case RULE_INT:
                    {
                    alt158=11;
                    }
                    break;

                }

                switch (alt158) {
            	case 1 :
            	    // InternalOWLModel.g:8411:7: this_AMPERSAND_11= RULE_AMPERSAND
            	    {
            	    this_AMPERSAND_11=(Token)match(input,RULE_AMPERSAND,FOLLOW_119); 

            	    		current.merge(this_AMPERSAND_11);
            	        
            	     
            	        newLeafNode(this_AMPERSAND_11, grammarAccess.getPARAM_ANY_VALUEAccess().getAMPERSANDTerminalRuleCall_1_0()); 
            	        

            	    }
            	    break;
            	case 2 :
            	    // InternalOWLModel.g:8419:10: this_HASHTAG_12= RULE_HASHTAG
            	    {
            	    this_HASHTAG_12=(Token)match(input,RULE_HASHTAG,FOLLOW_119); 

            	    		current.merge(this_HASHTAG_12);
            	        
            	     
            	        newLeafNode(this_HASHTAG_12, grammarAccess.getPARAM_ANY_VALUEAccess().getHASHTAGTerminalRuleCall_1_1()); 
            	        

            	    }
            	    break;
            	case 3 :
            	    // InternalOWLModel.g:8427:10: this_SEMICOLON_13= RULE_SEMICOLON
            	    {
            	    this_SEMICOLON_13=(Token)match(input,RULE_SEMICOLON,FOLLOW_119); 

            	    		current.merge(this_SEMICOLON_13);
            	        
            	     
            	        newLeafNode(this_SEMICOLON_13, grammarAccess.getPARAM_ANY_VALUEAccess().getSEMICOLONTerminalRuleCall_1_2()); 
            	        

            	    }
            	    break;
            	case 4 :
            	    // InternalOWLModel.g:8436:2: kw= '['
            	    {
            	    kw=(Token)match(input,19,FOLLOW_119); 

            	            current.merge(kw);
            	            newLeafNode(kw, grammarAccess.getPARAM_ANY_VALUEAccess().getLeftSquareBracketKeyword_1_3()); 
            	        

            	    }
            	    break;
            	case 5 :
            	    // InternalOWLModel.g:8443:2: kw= ']'
            	    {
            	    kw=(Token)match(input,20,FOLLOW_119); 

            	            current.merge(kw);
            	            newLeafNode(kw, grammarAccess.getPARAM_ANY_VALUEAccess().getRightSquareBracketKeyword_1_4()); 
            	        

            	    }
            	    break;
            	case 6 :
            	    // InternalOWLModel.g:8450:2: kw= '='
            	    {
            	    kw=(Token)match(input,25,FOLLOW_119); 

            	            current.merge(kw);
            	            newLeafNode(kw, grammarAccess.getPARAM_ANY_VALUEAccess().getEqualsSignKeyword_1_5()); 
            	        

            	    }
            	    break;
            	case 7 :
            	    // InternalOWLModel.g:8456:10: this_ANY_OTHER_17= RULE_ANY_OTHER
            	    {
            	    this_ANY_OTHER_17=(Token)match(input,RULE_ANY_OTHER,FOLLOW_119); 

            	    		current.merge(this_ANY_OTHER_17);
            	        
            	     
            	        newLeafNode(this_ANY_OTHER_17, grammarAccess.getPARAM_ANY_VALUEAccess().getANY_OTHERTerminalRuleCall_1_6()); 
            	        

            	    }
            	    break;
            	case 8 :
            	    // InternalOWLModel.g:8464:10: this_URI_18= RULE_URI
            	    {
            	    this_URI_18=(Token)match(input,RULE_URI,FOLLOW_119); 

            	    		current.merge(this_URI_18);
            	        
            	     
            	        newLeafNode(this_URI_18, grammarAccess.getPARAM_ANY_VALUEAccess().getURITerminalRuleCall_1_7()); 
            	        

            	    }
            	    break;
            	case 9 :
            	    // InternalOWLModel.g:8472:10: this_NSID_19= RULE_NSID
            	    {
            	    this_NSID_19=(Token)match(input,RULE_NSID,FOLLOW_119); 

            	    		current.merge(this_NSID_19);
            	        
            	     
            	        newLeafNode(this_NSID_19, grammarAccess.getPARAM_ANY_VALUEAccess().getNSIDTerminalRuleCall_1_8()); 
            	        

            	    }
            	    break;
            	case 10 :
            	    // InternalOWLModel.g:8480:10: this_ID_20= RULE_ID
            	    {
            	    this_ID_20=(Token)match(input,RULE_ID,FOLLOW_119); 

            	    		current.merge(this_ID_20);
            	        
            	     
            	        newLeafNode(this_ID_20, grammarAccess.getPARAM_ANY_VALUEAccess().getIDTerminalRuleCall_1_9()); 
            	        

            	    }
            	    break;
            	case 11 :
            	    // InternalOWLModel.g:8488:10: this_INT_21= RULE_INT
            	    {
            	    this_INT_21=(Token)match(input,RULE_INT,FOLLOW_119); 

            	    		current.merge(this_INT_21);
            	        
            	     
            	        newLeafNode(this_INT_21, grammarAccess.getPARAM_ANY_VALUEAccess().getINTTerminalRuleCall_1_10()); 
            	        

            	    }
            	    break;

            	default :
            	    break loop158;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePARAM_ANY_VALUE"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000840000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000200060L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000500000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000000280L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000001201000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000000500L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000201000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0400008088000000L,0x0400055540000000L,0x0000000000000004L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x040000808C000000L,0x0400055540000000L,0x0000000000000004L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000000700L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x00000000000000C0L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x00000000000005C0L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000030200000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000020200000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000040000000L,0x0040000000000000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000130200000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x140000BE88000000L,0x0400055540000001L,0x0000000000000004L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000100200000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000004080000000L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000010010000000L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x14AAA68088000000L,0x0400055540000015L,0x0000000000000004L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x14AAA68088000000L,0x0400055540000000L,0x0000000000000004L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000100100200000L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0000008000000000L,0x0400000000000040L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0000088000000000L,0x0400000000000040L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0000100120200000L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0000408000000000L,0x0400000000000040L});
    public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x0001008000000000L,0x0400000000000040L});
    public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x0004000000000000L});
    public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x0004008000000000L,0x0400000000000040L});
    public static final BitSet FOLLOW_48 = new BitSet(new long[]{0x0000100000200000L});
    public static final BitSet FOLLOW_49 = new BitSet(new long[]{0x0010008000000000L,0x0400000000000040L});
    public static final BitSet FOLLOW_50 = new BitSet(new long[]{0x0040008000000000L,0x0400000000000040L});
    public static final BitSet FOLLOW_51 = new BitSet(new long[]{0x0500000000000000L,0x5000000000000000L});
    public static final BitSet FOLLOW_52 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_53 = new BitSet(new long[]{0x0D00000000000000L,0x5000000000000000L});
    public static final BitSet FOLLOW_54 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_55 = new BitSet(new long[]{0x0000000000000060L});
    public static final BitSet FOLLOW_56 = new BitSet(new long[]{0x6000000000200000L});
    public static final BitSet FOLLOW_57 = new BitSet(new long[]{0x0000000002186F60L});
    public static final BitSet FOLLOW_58 = new BitSet(new long[]{0x4000000000200000L});
    public static final BitSet FOLLOW_59 = new BitSet(new long[]{0x0000000002386FE0L});
    public static final BitSet FOLLOW_60 = new BitSet(new long[]{0x8000000000000000L});
    public static final BitSet FOLLOW_61 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000002L});
    public static final BitSet FOLLOW_62 = new BitSet(new long[]{0x0000008000000000L,0x0400000000000048L});
    public static final BitSet FOLLOW_63 = new BitSet(new long[]{0x0000008000000000L,0x0400000000000060L});
    public static final BitSet FOLLOW_64 = new BitSet(new long[]{0x0000000000000000L,0x0000000015555080L});
    public static final BitSet FOLLOW_65 = new BitSet(new long[]{0x0000000000000000L,0x0000000015555A00L});
    public static final BitSet FOLLOW_66 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000100L});
    public static final BitSet FOLLOW_67 = new BitSet(new long[]{0x0000000000000000L,0x0000055540000000L});
    public static final BitSet FOLLOW_68 = new BitSet(new long[]{0x0000000000000000L,0x0000000015555800L});
    public static final BitSet FOLLOW_69 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000400L});
    public static final BitSet FOLLOW_70 = new BitSet(new long[]{0x4000000000000000L});
    public static final BitSet FOLLOW_71 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_72 = new BitSet(new long[]{0x0000000000000000L,0x0000000000002000L});
    public static final BitSet FOLLOW_73 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L});
    public static final BitSet FOLLOW_74 = new BitSet(new long[]{0x0000000000000000L,0x0000000000020000L});
    public static final BitSet FOLLOW_75 = new BitSet(new long[]{0x0000000000000000L,0x0000000000080000L});
    public static final BitSet FOLLOW_76 = new BitSet(new long[]{0x0000000000000000L,0x0000000000200000L});
    public static final BitSet FOLLOW_77 = new BitSet(new long[]{0x0000000000000000L,0x0000000000800000L});
    public static final BitSet FOLLOW_78 = new BitSet(new long[]{0x0000000120200000L});
    public static final BitSet FOLLOW_79 = new BitSet(new long[]{0x0000008000000000L,0x0400000002000040L});
    public static final BitSet FOLLOW_80 = new BitSet(new long[]{0x0000008000000000L,0x0400000008000040L});
    public static final BitSet FOLLOW_81 = new BitSet(new long[]{0x4000000100000000L});
    public static final BitSet FOLLOW_82 = new BitSet(new long[]{0x0000000000000000L,0x0000000020000000L});
    public static final BitSet FOLLOW_83 = new BitSet(new long[]{0x1400008088000000L,0x05455555C0000001L,0x0000000000000004L});
    public static final BitSet FOLLOW_84 = new BitSet(new long[]{0x1400008088000000L,0x0545555740000001L,0x0000000000000004L});
    public static final BitSet FOLLOW_85 = new BitSet(new long[]{0x1400008088000000L,0x0545555D40000001L,0x0000000000000004L});
    public static final BitSet FOLLOW_86 = new BitSet(new long[]{0x1400008088000000L,0x0545557540000001L,0x0000000000000004L});
    public static final BitSet FOLLOW_87 = new BitSet(new long[]{0x1400008088000000L,0x054555D540000001L,0x0000000000000004L});
    public static final BitSet FOLLOW_88 = new BitSet(new long[]{0x1400008088000000L,0x0545575540000001L,0x0000000000000004L});
    public static final BitSet FOLLOW_89 = new BitSet(new long[]{0x1400008088000000L,0x05455D5540000001L,0x0000000000000004L});
    public static final BitSet FOLLOW_90 = new BitSet(new long[]{0x0000000000000000L,0x0000255540000000L});
    public static final BitSet FOLLOW_91 = new BitSet(new long[]{0x0000000000000000L,0x0000200000000000L});
    public static final BitSet FOLLOW_92 = new BitSet(new long[]{0x0000000000000000L,0x0000855540000000L});
    public static final BitSet FOLLOW_93 = new BitSet(new long[]{0x0000000000000000L,0x0000800000000000L});
    public static final BitSet FOLLOW_94 = new BitSet(new long[]{0x0000000000000000L,0x0002000000000000L});
    public static final BitSet FOLLOW_95 = new BitSet(new long[]{0x0000008000000000L,0x0402000000000040L});
    public static final BitSet FOLLOW_96 = new BitSet(new long[]{0x0000000000000000L,0x0008000000000000L});
    public static final BitSet FOLLOW_97 = new BitSet(new long[]{0x0000008000000000L,0x0408000000000040L});
    public static final BitSet FOLLOW_98 = new BitSet(new long[]{0x0000000000000000L,0x0010000000000000L});
    public static final BitSet FOLLOW_99 = new BitSet(new long[]{0x00AAA40000000000L});
    public static final BitSet FOLLOW_100 = new BitSet(new long[]{0x0000000000000000L,0x0020000000000000L});
    public static final BitSet FOLLOW_101 = new BitSet(new long[]{0x0000000000000000L,0x0080000000000000L});
    public static final BitSet FOLLOW_102 = new BitSet(new long[]{0x0000000000000000L,0x0200055540000000L});
    public static final BitSet FOLLOW_103 = new BitSet(new long[]{0x0000000000000000L,0x0200000000000000L});
    public static final BitSet FOLLOW_104 = new BitSet(new long[]{0x14AAA48088000000L,0x1D45555540000001L,0x0000000000000004L});
    public static final BitSet FOLLOW_105 = new BitSet(new long[]{0x0000000000000000L,0x2000000000000000L});
    public static final BitSet FOLLOW_106 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L});
    public static final BitSet FOLLOW_107 = new BitSet(new long[]{0x0500000000000000L,0xD000000000000000L});
    public static final BitSet FOLLOW_108 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_109 = new BitSet(new long[]{0x0000100000000000L});
    public static final BitSet FOLLOW_110 = new BitSet(new long[]{0x0000000000000000L,0x0400000000000000L});
    public static final BitSet FOLLOW_111 = new BitSet(new long[]{0x0000000000000000L,0x0400000000000000L,0x0000000000000002L});
    public static final BitSet FOLLOW_112 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000008L});
    public static final BitSet FOLLOW_113 = new BitSet(new long[]{0x0000000000000060L,0x0000000000000000L,0x0000000000000070L});
    public static final BitSet FOLLOW_114 = new BitSet(new long[]{0x4000010130200060L});
    public static final BitSet FOLLOW_115 = new BitSet(new long[]{0x040000808A386FE0L,0x0400055540000000L,0x0000000000000084L});
    public static final BitSet FOLLOW_116 = new BitSet(new long[]{0x0400008088000000L,0x0400055540000000L,0x0000000000000084L});
    public static final BitSet FOLLOW_117 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000080L});
    public static final BitSet FOLLOW_118 = new BitSet(new long[]{0x0000000000386FE2L});
    public static final BitSet FOLLOW_119 = new BitSet(new long[]{0x0000000002186F62L});

}