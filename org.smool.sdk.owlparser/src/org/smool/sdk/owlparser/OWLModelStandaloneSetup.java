/*
* generated by Xtext
*/
package org.smool.sdk.owlparser;

/**
 * Initialization support for running Xtext languages 
 * without equinox extension registry
 */
public class OWLModelStandaloneSetup extends OWLModelStandaloneSetupGenerated{

	public static void doSetup() {
		new OWLModelStandaloneSetup().createInjectorAndDoEMFRegistration();
	}
}

