/*
 * generated by Xtext
 */
package org.smool.sdk.owlparser;

/**
 * Use this class to register components to be used at runtime / without the Equinox extension registry.
 */
public class OWLModelRuntimeModule extends org.smool.sdk.owlparser.AbstractOWLModelRuntimeModule {

}
