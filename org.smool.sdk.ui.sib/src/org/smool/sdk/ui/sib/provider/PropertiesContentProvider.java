/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Cristina L�pez (Fundacion European Software Institute) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.sib.provider;

import java.util.List;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.smool.sdk.ui.sib.data.PropertyData;


/**
 * This class is the Content Provider implementation required by the viewer of the 
 * Properties view.
 * 
 * @author Cristina L�pez , cristina.lopez@esi.es, ESI
 *
 */

public class PropertiesContentProvider implements IStructuredContentProvider{

	@SuppressWarnings("unchecked")
	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider
	 */
	public Object[] getElements(Object inputElement) {
		if (inputElement instanceof List<?>){
			if (((List<?>) inputElement).get(0) instanceof PropertyData){
				List<PropertyData> props= (List<PropertyData>) inputElement;
				return props.toArray();
			}
			else{
				return null;
			}
		}
		else{
			return null;
		}
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider
	 */
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider
	 */
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		// TODO Auto-generated method stub
		
	}

}
