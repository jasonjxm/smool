/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Cristina L�pez (Fundacion European Software Institute) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.sib.provider;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.smool.sdk.gateway.service.IGateway;
import org.smool.sdk.ui.sib.Activator;
import org.smool.sdk.ui.sib.data.Element;


/**
 * This class is the Label Provider implementation required by the viewer of the 
 * Servers view. 
 * 
 * @author Cristina L�pez, cristina.lopez@esi.es, ESI
 *
 */
public class SIBLabelProvider extends LabelProvider implements ITableLabelProvider{

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITableLabelProvider
	 */
	public Image getColumnImage(Object element, int columnIndex) {
		Image _return = null;
		
		if (columnIndex==0){
			if (element instanceof Element){
				_return=Activator.getImageDescriptor("icons/general/server_perspective.gif").createImage();
			}
			else if (element instanceof IGateway){
				if (((IGateway) element).getType().equals("TCP/IP")){
					_return=Activator.getImageDescriptor("icons/gateway/network.gif").createImage();
				}
				else if (((IGateway) element).getType().equals("Bluetooth")){
					_return=Activator.getImageDescriptor("icons/gateway/bluetooth.png").createImage();
				}
			}
		}
	
		return _return;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITableLabelProvider
	 */
	public String getColumnText(Object element, int columnIndex) {		
		String _return="";
		Object object= element;
		
		switch(columnIndex)
		{
			case 0:
				if (element instanceof Element){
					_return= ((Element)object).getSIB().getName();
				}
				else if (element instanceof IGateway){
					_return= ((IGateway)object).getName();
				}
				break;
			case 1:
				if (element instanceof Element){
					String status="";
					if (((Element)object).getSIB().isRunning()){
						status="Started";
					} else {
						status="Stopped";
					}
					_return= status;
				}
				else if (element instanceof IGateway){
					String status="";
					if (((IGateway)object).isRunning()){
						status="Started";
					} else {
						status="Stopped";
					}
					_return= status;
				}
				break;
			default:
				_return="";
				break;
		}
		return _return;
	}

}
