/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Cristina L�pez (Fundacion European Software Institute) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.sib.provider;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.smool.sdk.gateway.service.IGateway;
import org.smool.sdk.ui.sib.data.Element;


/**
 * This class is the Content Provider implementation required by the viewer of the 
 * Servers view.
 * 
 * @author Cristina L�pez, cristina.lopez@esi.es, ESI
 *
 */
public class SIBContentProvider extends ArrayContentProvider implements ITreeContentProvider{

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider
	 */
	public Object[] getChildren(Object parentElement) {
		Element sib =(Element) parentElement;
		return sib.getGateways().toArray();
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider
	 */
	public Object getParent(Object element) {
		Object _return;
		if (element instanceof IGateway){
			_return= ((IGateway) element).getSIB();
		}
		else{
			_return=null;
		}
		return _return;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider
	 */
	public boolean hasChildren(Object element) {
		Boolean hasChildren=false;
		if (element instanceof Element){
			hasChildren= (((Element)element).getGateways().size()>0);
		}
		else if (element instanceof IGateway){
			hasChildren=false;
		}

		return hasChildren;
	}

}
