/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Cristina L�pez (Fundacion European Software Institute) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.sib.view;

import java.util.List;
import java.util.Properties;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.smool.sdk.gateway.exception.GatewayException;
import org.smool.sdk.gateway.service.IGateway;
import org.smool.sdk.gateway.service.IGatewayFactory;
import org.smool.sdk.sib.exception.SIBException;
import org.smool.sdk.sib.service.ISIBFactory;
import org.smool.sdk.ui.sib.Activator;
import org.smool.sdk.ui.sib.data.Element;
import org.smool.sdk.ui.sib.data.PropertyData;
import org.smool.sdk.ui.sib.provider.SIBContentProvider;
import org.smool.sdk.ui.sib.provider.SIBLabelProvider;
import org.smool.sdk.ui.sib.wizard.NewGatewayWizard;
import org.smool.sdk.ui.sib.wizard.NewServerWizard;


/**
 * This class implements the Server View, used to create, start, stop and delete instances
 * of SIB servers. It also allow to create, start, stop and delete different gateway
 * types within the servers.
 *
 * The view contains action buttons to control the operations that can be performed
 * over the servers and gateways.
 * 
 * @author Cristina L�pez, cristina.lopez@esi.es, ESI
 *
 */
public class SIBServer extends ViewPart {
	/**
	 * Viewer manager
	 */
	private static TreeViewer viewer;

	/**
	 * Start element action
	 */
	private static Action start;
	/**
	 * Stop element action
	 */
	private static Action stop;
	/**
	 * Restarts element action
	 */
	private static Action restart;	
	/**
	 * Delete element action
	 */
	private static Action delete;
	/**
	 * NAO Info element action
	 */
	private static Action info;
	/**
	 * Create new server action
	 */
	private Action newServer;
	/**
	 * Create new gateway action
	 */
	private static Action newGateway;
	
	/**
	 * SIB service manager
	 */
	private ISIBFactory serverMgt;
	
	/**
	 * Gateway service manager
	 */
	private IGatewayFactory gwMgt;	

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.ViewPart
	 */
	public void createPartControl(Composite parent) {
		initializeViewer(parent);
		initializeActions();
		createActionBar();
		createContextMenu();
		activateListeners();
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.ViewPart
	 */
	public void setFocus() {
		viewer.getControl().setFocus();
		
	}
	
	/**
	 * Method that sets the structure of the view
	 * @param parent
	 */
	private void initializeViewer(Composite parent){
		viewer= new TreeViewer(parent, SWT.BORDER
				| SWT.FULL_SELECTION | SWT.H_SCROLL | SWT.V_SCROLL);
		viewer.getTree().setHeaderVisible(true);
		viewer.getTree().setLinesVisible(true);
		
		TreeViewerColumn instance = new TreeViewerColumn(viewer, SWT.LEFT);
		instance.getColumn().setText("Instance");
		instance.getColumn().setWidth(180);
		TreeViewerColumn status = new TreeViewerColumn(viewer, SWT.LEFT);
		status.getColumn().setText("Status");
		status.getColumn().setWidth(150);
		
		viewer.setContentProvider(new SIBContentProvider());
		viewer.setLabelProvider(new SIBLabelProvider());
		viewer.setInput(Activator.getDefault().getSibList());
		viewer.expandAll();

	}
	
	/**
	 * Method that initialize the action elements and sets their behavior
	 */
	private void initializeActions() {
		restart = new Action() {
			public void run() {
				Object selection=((StructuredSelection)viewer.getSelection()).getFirstElement();				
				restartElement(selection);
			}
		};
		restart.setText("Restart");
		restart.setToolTipText("Restart");
		restart.setImageDescriptor(Activator.getImageDescriptor("icons/eserver/restart_on.gif"));
		restart.setEnabled(false);		
		
		start = new Action() {
			public void run() {
				Object selection=((StructuredSelection)viewer.getSelection()).getFirstElement();
				startElement(selection);
			}
		};
		start.setText("Start");
		start.setToolTipText("Start");
		start.setImageDescriptor(Activator.getImageDescriptor("icons/eserver/launch_run.gif"));
		start.setEnabled(false);
		
		stop = new Action() {
			public void run() {
				Object selection=((StructuredSelection)viewer.getSelection()).getFirstElement();
				stopElement(selection);
			}
		};
		stop.setText("Stop");
		stop.setToolTipText("Stop");
		stop.setImageDescriptor(Activator.getImageDescriptor("icons/eserver/launch_stop.gif"));
		stop.setEnabled(false);
		
		newServer= new Action(){
			public void run() {
				createNewServerWizard();
			}
		};
		newServer.setText("Create Server");
		newServer.setToolTipText("Create Server");
		newServer.setImageDescriptor(Activator.getImageDescriptor("icons/eserver/wiz_new_server.gif"));
		
		newGateway= new Action(){
			public void run() {
				createNewGatewayWizard();
			}
		};
		newGateway.setText("Create Gateway");
		newGateway.setToolTipText("Create Gateway");
		newGateway.setImageDescriptor(Activator.getImageDescriptor("icons/gateway/internal_browser.gif"));
		newGateway.setEnabled(false);
		
		delete= new Action(){
			public void run() {
				Object selection=((StructuredSelection)viewer.getSelection()).getFirstElement();
				deleteElement(selection);
			}
		};
		delete.setText("Delete");
		delete.setToolTipText("Delete");
		delete.setImageDescriptor(Activator.getImageDescriptor(ISharedImages.IMG_TOOL_DELETE));
		delete.setEnabled(false);
		//NAO
		info= new Action(){
			public void run() {
				Object selection=((StructuredSelection)viewer.getSelection()).getFirstElement();
				infoElement(selection);
			}
		};
		info.setText("Service Address");
		info.setToolTipText("Service Address");
		info.setImageDescriptor(Activator.getImageDescriptor(ISharedImages.IMG_TOOL_DELETE));
		info.setEnabled(true);
		// fin NAO
	}
	
	/**
	 * Method that creates the Action bar using some of the actions previously defined
	 */
	private void createActionBar(){
		IActionBars bar= getViewSite().getActionBars();
		IToolBarManager tool= bar.getToolBarManager();
		
		tool.add(start);
		tool.add(stop);
		tool.add(restart);
		tool.add(newServer);
		tool.add(newGateway);
	}
	
	/**
	 * Method that creates the Context menu using some of the action previosly defined
	 */
	private void createContextMenu() {
		MenuManager menuMgr = new MenuManager("Right Button Menu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				manager.add(start);
				manager.add(stop);
				manager.add(newServer);
				manager.add(newGateway);
				manager.add(delete);
				// NAO
//				manager.add(info);
				//fin NAO
			}
		});
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}
	
	/**
	 * Method that implements the behavior of the viewer when the element selected changes
	 */
	private void activateListeners(){
		viewer.addSelectionChangedListener(new ISelectionChangedListener(){

			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				if(!viewer.getSelection().isEmpty())
				{
					Object selection=((StructuredSelection)viewer.getSelection()).getFirstElement();
					if (selection instanceof Element){
						newGateway.setEnabled(true);						
						Element serverSelected= (Element) ((StructuredSelection)viewer.getSelection()).getFirstElement();
						if(serverSelected.getSIB().isRunning()){
							start.setEnabled(false);
							stop.setEnabled(true);
						}
						else if (serverSelected.getSIB().isStopped()){
							start.setEnabled(true);
							stop.setEnabled(false);
						}
						
						PropertiesView.updateContent(PropertyData.convertSibProp(serverSelected));
					}
					else if (selection instanceof IGateway){
						newGateway.setEnabled(false);
						
						IGateway gatewaySelected= (IGateway) ((StructuredSelection)viewer.getSelection()).getFirstElement();
						if (gatewaySelected.getSIB().isRunning()) {
							if (gatewaySelected.isRunning()){
								start.setEnabled(false);
								stop.setEnabled(true);
							}
							else if (gatewaySelected.isStopped()){
								start.setEnabled(true);
								stop.setEnabled(false);
							}							
						}
						else if (gatewaySelected.getSIB().isStopped()){
							start.setEnabled(false);
							stop.setEnabled(false);
						}
						PropertiesView.updateContent(PropertyData.convertGatewayProp(gatewaySelected));
					}
				}
			}			
		});
	}
	

//********************
//    MESSAGES 
//********************
	
	/**
	 * Error message dialog
	 */
	private void showErrorMessage(String message) {
		MessageDialog.openError(
			viewer.getControl().getShell(),
			"SIB View Error",
			message);
	}

//********************
//     WIZARDS 
//********************
	
	/**
	 * Method that creates the dialog to add a new server
	 */
	private void createNewServerWizard(){
		
		try{	
			serverMgt= Activator.getDefault().getSIBMgt(); 
			if(serverMgt!=null){
				NewServerWizard wizard= new NewServerWizard(serverMgt);
				// Shell shell= new Shell();
				Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
				WizardDialog dialog = new WizardDialog(shell, wizard);
				dialog.create();
		        dialog.open();
		        
		        String name= wizard.getName();
		        if (wizard.isCancelled()!=true){
		        	serverMgt.create(name, null);
		        }

			}
			else{
				showErrorMessage("We are sorry to say that the Semantic Information Broker manager service is not available");
			}
		}
		catch(SIBException e){ 
			showErrorMessage("Sorry, there has been an error creating the server: \n \n"+e);
		}
        
    }
	
	/**
	 * Method that creates the dialog to add a new gateway within a server
	 */
	private void createNewGatewayWizard(){

		try{
			
			if (!Activator.getDefault().getGwMgtMap().isEmpty()){
				NewGatewayWizard wizard= new NewGatewayWizard(Activator.getDefault().getGwMgtMap());
				Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
				// Shell shell= new Shell();
				WizardDialog dialog = new WizardDialog(shell, wizard);
		        dialog.create();
		        dialog.open();
		        
				
				Element serverSelected= (Element) ((StructuredSelection)viewer.getSelection()).getFirstElement();	
	
				if (wizard.isCancelled()!=true){
			        String type= wizard.getType();
					gwMgt= Activator.getDefault().getGwMgtMap().get(type);
					gwMgt.create( wizard.getName(),serverSelected.getSIB(), wizard.getConfigurableProperties());
				}
				
			}
			else{
				showErrorMessage("We are sorry to say that the Gateway manager service is not available");
			}
		}

		catch(GatewayException e){
			showErrorMessage("Sorry, there has been an error creating the gateway: \n \n"+e);
		}
	}
	
//********************
//    OPERATIONS OVER SIBS AND GATEWAYS
//********************
	
	/**
	 * Restarts the element
	 */
	private void restartElement(Object selection) {
		if(!viewer.getSelection().isEmpty()){
			//Object selection=((StructuredSelection)viewer.getSelection()).getFirstElement();
			if (selection instanceof Element){
				try {
					System.out.println("Restarting selected SIB...");
					
					Element serverSelected= (Element) ((StructuredSelection)viewer.getSelection()).getFirstElement();
					if (serverSelected.getSIB().isRunning()) {
						serverSelected.getSIB().stop();
						//Stop children
						List<IGateway> gwList= serverSelected.getGateways();
						for (int i=0; i<gwList.size();i++){
							stopElement(gwList.get(i));
						}
					}
					
					serverSelected.getSIB().start();
					List<IGateway> gwList= serverSelected.getGateways();
					for (int i=0; i<gwList.size();i++){
						startElement(gwList.get(i));				
					}
				}
				catch(SIBException e) {
					showErrorMessage("Sorry, there has been an error restarting the server: \n \n"+e);
				}
			} else if (selection instanceof IGateway) {
				try{
					System.out.println("Gateway restarting...");
					// Note that if only gw is selected, only gw is restarted, not the sib
					IGateway gwSelected= (IGateway)selection;						
					gwSelected.stop();
					gwSelected.start();
				}
				catch(GatewayException e){ 
					showErrorMessage("Sorry, there has been an error stopping the gateway: \n \n"+e);
				}				
			}
		} else {
			showErrorMessage("Please, select an element");
		}
	}	
	
	/**
	 * Method called when the start button/action is pressed
	 */
	private void startElement(Object selection) {
		if(!viewer.getSelection().isEmpty()){
			//Object selection=((StructuredSelection)viewer.getSelection()).getFirstElement();
			if (selection instanceof Element){
				try{
					System.out.println("Server starting...");
					
					Element serverSelected = (Element) selection;

					serverSelected.getSIB().start();					

					//Start the associated gateways
					List<IGateway> gwList= serverSelected.getGateways();
					for (int i=0; i<gwList.size();i++){
						startElement(gwList.get(i));
					}
					
				} catch(SIBException e) {
					showErrorMessage("Sorry, there has been an error starting the server: \n \n"+e);
				}
			} else if (selection instanceof IGateway){
				try{
					System.out.println("Gateway starting ...");
					
					IGateway gwSelected= (IGateway) selection;	
					gwSelected.start();
				}
				catch(GatewayException e){ 
					showErrorMessage("Sorry, there has been an error starting the gateway: \n \n"+e);
				}
			}
		}
		else {
			showErrorMessage("Please, select an element");
		}
	}
	
	/**
	 * Method called when the stop button/action is pressed
	 * @param selection Object
	 */
	private void stopElement( Object selection)
	{
		if(!viewer.getSelection().isEmpty()){
			if (selection instanceof Element){
				try{
					System.out.println("Server stopping...");
					Element serverSelected= (Element) selection;
					
					//Stop children
					List<IGateway> gwList= serverSelected.getGateways();
					for (int i=0; i<gwList.size();i++){
						stopElement(gwList.get(i));
					}
					
					serverSelected.getSIB().stop();
				}
				catch(SIBException e){
					showErrorMessage("Sorry, there has been an error stopping the server: \n \n"+e);
				}
			}
			else if (selection instanceof IGateway){
				try{
					System.out.println("Gateway stopping...");
					
					IGateway gwSelected= (IGateway)selection;						
					gwSelected.stop();					
				}
				catch(GatewayException e){ 
					showErrorMessage("Sorry, there has been an error stopping the gateway: \n \n"+e);
				}
			}
		}
		else {
			showErrorMessage("Please, select an element");
		}
	}
	
	/**
	 * Method called when the delete button/action is pressed
	 * @param selection
	 */
	private void deleteElement(Object selection){
		if(!viewer.getSelection().isEmpty()){
			if (selection instanceof Element){
				try{
					Element serverSelected= (Element) selection;
					
					//Delete children
					List<IGateway> gwList=serverSelected.getGateways();
					while(gwList.size()>0){
						deleteElement(gwList.get(0));
					}
					
					if(serverSelected.getSIB().isRunning()){
						stopElement(serverSelected);
					}
					serverMgt=Activator.getDefault().getSIBMgt();
					serverMgt.destroy(serverSelected.getSIB().getId());
					
				}
				catch(SIBException e){ 
					showErrorMessage("Sorry, there has been an error deleting the server: \n \n"+e);
				}				
			}
			else if (selection instanceof IGateway){
				try{
					System.out.println("Gateway deleted");
					IGateway gwSelected= (IGateway) selection;
					
					if(gwSelected.isRunning()){
						stopElement(gwSelected);
					}				
					gwMgt= Activator.getDefault().getGwMgtMap().get(gwSelected.getType());
					gwMgt.destroy(gwSelected.getId());

				}
				catch(GatewayException e){ 
					showErrorMessage("Sorry, there has been an error deleting the gateway: \n \n"+e);
				}
				
			}
			updateList();
		}
		else{
			showErrorMessage("Please, select the element to delete");
		}
	}
	//NAO
	/**
	 * Method called when the Service Address action is pressed
	 * @param selection
	 */
	private void infoElement(Object selection){
		if(!viewer.getSelection().isEmpty()){
			if (selection instanceof Element){
				System.out.print("\n"+selection.toString());	
			}
			else if (selection instanceof IGateway){
				System.out.println("Gateway Service Address");
				IGateway gwSelected= (IGateway) selection;
				gwMgt= Activator.getDefault().getGwMgtMap().get(gwSelected.getType());
				Properties p=gwSelected.getProperties();
				String service_address = p.getProperty("SERVICE_ADDRESS");
				System.out.println(service_address);
			}
			updateList();
		}
		else{
			showErrorMessage("Please, select an element");
		}
		//finNAO
	}
	
//********************************
//  VISUALIZATION RELATED METHODS 
//********************************	

	/**
	 * Method that updates the viewer when a new element is created
	 * This method is called when the SIB or Gateway status listener send any notification
	 */
	public static void createUpdateList(){
		try {
			Runnable r = new Runnable() {
				public void run() {
					delete.setEnabled(true);
					viewer.refresh();
					viewer.expandAll();	
				};
			};
			if (!viewer.getTree().isDisposed()) {
				viewer.getTree().getDisplay().syncExec(r);
			}
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Method that updates the viewer when required
	 * This method is called when the SIB or Gateway status listener sends any notification
	 */
	public static void updateList(){
		try {
			Runnable r = new Runnable() {
				public void run() {
					viewer.refresh();
					viewer.expandAll();		
					
					if (Activator.getDefault().getSibList().size()<=0){
						start.setEnabled(false);
						stop.setEnabled(false);
						restart.setEnabled(false);
						delete.setEnabled(false);
						newGateway.setEnabled(false);
						PropertiesView.clearContent();
					}
				};
			};
			if (!viewer.getTree().isDisposed()) {
				viewer.getTree().getDisplay().syncExec(r);
			}
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Method that changes the status of an element and the corresponding start/stop actions.
	 * This method is called when the SIB or Gateway status listener sends any notification
	 */
	public static void enableStartedElement(){
		try {
			Runnable r = new Runnable() {
				public void run() {
					start.setEnabled(false);
					stop.setEnabled(true);
					restart.setEnabled(true);
					viewer.refresh();
					viewer.expandAll();	
				};
			};
			if (!viewer.getTree().isDisposed()) {
				viewer.getTree().getDisplay().syncExec(r);
			}
		} catch(Exception e){
			e.printStackTrace();
		}		
	}
	
	/**
	 * Method that changes the status of an element and the corresponding start/stop actions.
	 * This method is called when the SIB or Gateway status listener sends any notification
	 */
	public static void enableStoppedElement(){
		try {
			Runnable r = new Runnable() {
				public void run() {
					stop.setEnabled(false);
					start.setEnabled(true);	
					restart.setEnabled(true);
					viewer.refresh();
					viewer.expandAll();	
				};
			};
			if (!viewer.getTree().isDisposed()) {
				viewer.getTree().getDisplay().syncExec(r);
			}
		} catch(Exception e){
			e.printStackTrace();
		}			
	}
	
}
