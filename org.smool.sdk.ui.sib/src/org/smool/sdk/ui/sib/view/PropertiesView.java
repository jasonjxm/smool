/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Cristina L�pez (Fundacion European Software Institute) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.sib.view;

import java.util.List;

import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.part.ViewPart;
import org.smool.sdk.ui.sib.data.PropertyData;
import org.smool.sdk.ui.sib.provider.PropertiesContentProvider;
import org.smool.sdk.ui.sib.provider.PropertiesLabelProvider;


/**
 * This class implements the Properties View, used to visualize the properties of the
 * servers and gateways created using the Servers view. 
 *
 * The information displayed on the view is refreshed with the information of the element
 * that is selected in the Servers view. If the selected element changes, the information 
 * displayed by the view changes consequently.
 * 
 * @author Cristina L�pez, cristina.lopez@esi.es, ESI
 *
 */
public class PropertiesView extends ViewPart {

	/**
	 * View manager
	 */
	private static TableViewer propertiesViewer;

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.ViewPart
	 */
	public void createPartControl(Composite parent) {
		propertiesViewer = new TableViewer(parent, SWT.BORDER
				| SWT.FULL_SELECTION | SWT.V_SCROLL | SWT.H_SCROLL | SWT.MULTI);
		
		Table table= propertiesViewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		
		TableColumn property= new TableColumn(table, SWT.LEFT);
		property.setText("Property");
		property.setWidth(170);
		
		TableColumn value= new TableColumn(table, SWT.LEFT);
		value.setText("Value");
		value.setWidth(170);	
		
		propertiesViewer.setContentProvider(new PropertiesContentProvider());
		propertiesViewer.setLabelProvider(new PropertiesLabelProvider());

	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.ViewPart
	 */
	public void setFocus() {
		propertiesViewer.getControl().setFocus();
	}
	
	/**
	 * Static method used externally by the Servers view to update the information 
	 * displayed in the Properties View.
	 * 
	 * @param inputData
	 */
	public static void updateContent(final List<PropertyData> inputData) {
		try {
			if(propertiesViewer!=null && inputData.size()>0){
				Runnable r = new Runnable() {
					public void run() {
						propertiesViewer.setInput(inputData);
						propertiesViewer.refresh();
					};
				};
				propertiesViewer.getTable().getDisplay().syncExec(r);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public static void clearContent(){
		try {
			propertiesViewer.setInput(null);
			propertiesViewer.refresh();
		} catch (Exception ex) {
			// nothing to do
		}
	}

}
