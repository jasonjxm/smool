/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Cristina L�pez (Fundacion European Software Institute) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.sib;


import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import org.smool.sdk.gateway.service.IGatewayFactory;

/**
 * This class is a service tracker customizer that detects if any Gateway manager service
 * has been registered or unregistered in the OSGI Framework.
 * 
 * @author Cristina L�pez, cristina.lopez@esi.es, ESI
 *
 */
@SuppressWarnings("rawtypes")
public class GatewayManagerServiceTrackerCustomizer implements ServiceTrackerCustomizer {

	/**
	 * Bundle Context
	 */
	private BundleContext bc;
	
	/**
	 * Constructor with one parameter 
	 * @param bc BundleContext
	 */
	public GatewayManagerServiceTrackerCustomizer(BundleContext bc){
		this.bc=bc;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	/*
	 * (non-Javadoc)
	 * @see org.osgi.util.tracker.ServiceTrackerCustomizer
	 */
	public Object addingService(ServiceReference reference) {
		Object obj = bc.getService(reference);
		if (obj instanceof IGatewayFactory){
			String type= (String) reference.getProperty("Gateway");			
			IGatewayFactory gwMgt=(IGatewayFactory) bc.getService(reference);
			Activator.getDefault().getGwMgtMap().put(type, gwMgt);
		}
		return obj;
	}

	@SuppressWarnings("unchecked")
	@Override
	/*
	 * (non-Javadoc)
	 * @see org.osgi.util.tracker.ServiceTrackerCustomizer
	 */
	public void modifiedService(ServiceReference reference, Object service) {
		Object obj = bc.getService(reference);
		if (obj instanceof IGatewayFactory){
			String type= (String) reference.getProperty("Gateway");

			IGatewayFactory gwMgt=(IGatewayFactory) bc.getService(reference);
			Activator.getDefault().getGwMgtMap().put(type, gwMgt);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	/*
	 * (non-Javadoc)
	 * @see org.osgi.util.tracker.ServiceTrackerCustomizer
	 */
	public void removedService(ServiceReference reference, Object service) {
		Object obj = bc.getService(reference);
		if (obj instanceof IGatewayFactory){
			String type= (String) reference.getProperty("Gateway");
			Activator.getDefault().getGwMgtMap().remove(type);

		}
	}
	
}
