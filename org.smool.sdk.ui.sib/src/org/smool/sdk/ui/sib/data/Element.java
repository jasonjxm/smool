/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Cristina L�pez (Fundacion European Software Institute) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.sib.data;

import java.util.ArrayList;
import java.util.List;

import org.smool.sdk.gateway.service.IGateway;
import org.smool.sdk.gateway.service.IGatewayStateListener;
import org.smool.sdk.sib.service.ISIB;
import org.smool.sdk.sib.service.ISIBStateListener;
import org.smool.sdk.ui.sib.view.SIBServer;


/**
 * This is a data class used by the Semantic Information Broker View. 
 * The internal viewer structure of the view uses this data class internally
 * to manage the elements on display.
 * 
 * The class implements two listeners: ISIBStateListener and IGatewayStateListener. The listeners are used notify the change of status
 * of the SIB elements and the Gateways instances within it.
 * 
 * 
 * @author Cristina L�pez, cristina.lopez@esi.es, ESI
 *
 */
public class Element implements ISIBStateListener, IGatewayStateListener{
	
	/**
	 * SIB server instance
	 */
	private ISIB sib;
	
	/**
	 * List of gateways of the SIB
	 */
	private List<IGateway> gateways= new ArrayList<IGateway>();
	
	/**
	 * List of extensions
	 */
	//TODO
	
	/**
	 * Default Constructor
	 */
	public Element (){		
	}
	
	/**
	 * Constructor
	 * @param sib
	 */
	public Element (ISIB sib){
		this.sib=sib;
	}
	
	/**
	 * Retrieves the SIB elements
	 * @return
	 */
	public ISIB getSIB() {
		return sib;
	}
	
	/**
	 * Sets the sib element
	 * @param sib
	 */
	public void setSIB(ISIB sib) {
		this.sib = sib;
	}
	
	/**
	 * Retrieves the list of Gateways of the Sib element
	 * @return
	 */
	public List<IGateway> getGateways() {
		return gateways;
	}
	
	/**
	 * Sets the list of Gateways of the Sib element
	 * @param gateways
	 */
	public void setGateways(List<IGateway> gateways) {
		this.gateways = gateways;
	}

	/**
	 * Implementation of the ISIBStateListener
	 */
	@Override
	/*
	 * (non-Javadoc)
	 * @see org.smool.sdk.sib.service.ISIBStateListener#SIBRunning(org.smool.sdk.sib.service.ISIB)
	 */
	public void SIBRunning(ISIB sib) {
		SIBServer.enableStartedElement();
		
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.smool.sdk.sib.service.ISIBStateListener#SIBStopped(org.smool.sdk.sib.service.ISIB)
	 */
	public void SIBStopped(ISIB sib) {
		SIBServer.enableStoppedElement();
		
	}

	/**
	 * Implementation of the IGatewayStateListener
	 */
	@Override
	/*
	 * (non-Javadoc)
	 * @see org.smool.sdk.gateway.service.IGatewayStateListener#gatewayRunning(org.smool.sdk.gateway.service.IGateway)
	 */
	public void gatewayRunning(IGateway gw) {
		SIBServer.enableStartedElement();
		
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.smool.sdk.gateway.service.IGatewayStateListener#gatewayStopped(org.smool.sdk.gateway.service.IGateway)
	 */
	public void gatewayStopped(IGateway gw) {
		SIBServer.enableStoppedElement();
		
	}	
	
}
