/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Cristina L�pez (Fundacion European Software Institute) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.sib.data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.smool.sdk.gateway.service.IGateway;


/**
 * This is a data class used by the Properties View to organized the properties information
 * related to Sib servers and gateways.
 * 
 * @author Cristina Lopez, cristina.lopez@esi.es , ESI
 *
 */
public class PropertyData {

	/**
	 * Property name
	 */
	private String name;
	/**
	 * Property value
	 */
	private String value;
	
	/**
	 * Default Constructor
	 */
	public PropertyData(){
		
	}
	
	/**
	 * Constructor with two parameters
	 * @param name
	 * @param value
	 */
	public PropertyData(String name, String value) {
		super();
		this.name = name;
		this.value = value;
	}
	
	/**
	 * Gets the name
	 * @return String name
	 */
	public String getName() {
		return name;
	}
	/**
	 * Sets the name
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the value
	 * @return String value
	 */
	public String getValue() {
		return value;
	}
	/**
	 * Sets the value
	 * @param value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * This static method converts the SibProp data class in a list of properties that
	 * can be used as input by the Properties View
	 * 
	 * @param server
	 * @return
	 */
	public static List<PropertyData> convertSibProp(Element server){
		List<PropertyData> _return= new ArrayList<PropertyData>();		
		PropertyData name=new PropertyData("Name", server.getSIB().getName());
		PropertyData id=new PropertyData("Id", Integer.toString(server.getSIB().getId()));
		String status="";
		if (server.getSIB().isRunning()) {
			status="Started";
		} else {
			status="Stopped";
		}
		
		PropertyData statusProp=new PropertyData("Status", status);
		_return.add(name);
		_return.add(id);		
		_return.add(statusProp);
		return _return;
	}
	
	/**
	 * This static method converts the GatewayProp data class in a list of properties that
	 * can be used as input by the Properties View
	 * 
	 * @param server
	 * @return
	 */	
	public static List<PropertyData> convertGatewayProp(IGateway gateway){
		List<PropertyData> _return= new ArrayList<PropertyData>();
		
		PropertyData name=new PropertyData("Name", gateway.getName());
		PropertyData type=new PropertyData("Type", gateway.getType());
		String gwStatus="";
		if (gateway.isRunning()){
			gwStatus="Started";
		} else if (gateway.isStopped()){
			gwStatus="Stopped";
		} else {
			gwStatus="Unknown";
		}
		PropertyData status=new PropertyData("Status", gwStatus);
		PropertyData server=new PropertyData("Sib Id", Integer.toString(gateway.getSIB().getId()));
		
		_return.add(name);
		_return.add(type);
		_return.add(status);
		_return.add(server);
		
        Iterator<Object> properties=gateway.getProperties().keySet().iterator();
        while(properties.hasNext()){
        	Object current= properties.next();
        	String label=current.toString().substring(0, 1)+current.toString().substring(1, current.toString().length()).toLowerCase();
        	PropertyData prop= new PropertyData(label, gateway.getProperties().get(current).toString());
        	_return.add(prop);
        }
        
		return _return;
	}
	
}

