/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.sib;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.util.tracker.ServiceTracker;
import org.smool.sdk.gateway.service.IGateway;
import org.smool.sdk.gateway.service.IGatewayFactory;
import org.smool.sdk.sib.service.ISIB;
import org.smool.sdk.sib.service.ISIBFactory;
import org.smool.sdk.ui.sib.data.Element;


/**
 * This Activator class prepares the plug-in to work both with the manager of the SIB
 * and the manager of the Gateway.
 * 
 * @author Cristina L�pez, cristina.lopez@esi.es, ESI 
 */
public class Activator extends AbstractUIPlugin {
	
	/**
	 * Plug-in Id
	 */
	public static final String PLUGIN_ID = "org.smool.sdk.ui.sib";
	/**
	 * Shared Activator Instance
	 */
	private static Activator plugin;	
	/**
	 * Bundle Context
	 */
	private static BundleContext bc;	
	/**
	 * ISIB service tracker customizer
	 */
	private ServerManagerServiceTrackerCustomizer mgtstc;	
	/**
	 * Gateway service tracker customizer
	 */
	private GatewayManagerServiceTrackerCustomizer gwmgtstc;
	
	/**
	 * Sib service tracker customizer
	 */
	private SIBServiceTrackerCustomizer sibstc;
	
	/**
	 * Gateway service tracker customizer
	 */
	private GatewayServiceTrackerCustomizer gwstc;
	
	/**
	 * List of SIB instances
	 */
	private ArrayList<Element> sibList= new ArrayList<Element>();
	
	/**
	 * List containing the types of gateway available
	 */
	private HashMap<String, IGatewayFactory> gwMgtMap;
	
	
	private static String GATEWAY_PREFIX = "org.smool.sdk.gateway";	
	
	/**
	 * Default Constructor
	 */
	public Activator() {

	}
	
	/**
	 * Gets the shared instance
	 * @return Activator
	 */
	public static Activator getDefault() {
		return plugin;
	}
	
	/**
	 * Gets the bundle context
	 * @return BundleContext
	 */
	public static BundleContext getBc() {
		return bc;
	}

	/**
	 * Gets the ISIB Manager
	 * @return ISIBManager
	 */
	public ISIBFactory getSIBMgt(){
		return mgtstc.getSibManager();
	}
	
	public HashMap<String, IGatewayFactory> getGwMgtMap() {
		return gwMgtMap;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void start(BundleContext context) throws Exception {
		super.start(context);
		bc=context;
		plugin = this;
		
		try {
			Bundle[] bundles = bc.getBundles();
			for (Bundle bundle : bundles) {
				if (bundle.getSymbolicName().startsWith(GATEWAY_PREFIX)) {
//					System.out.println("Getting info for " + bundle.getSymbolicName() + "...");
//					switch (bundle.getState()) {
//					case Bundle.ACTIVE:
//						System.out.println("Bundle is ACTIVE");
//						break;
//					case Bundle.INSTALLED:
//						System.out.println("Bundle is INSTALLED");
//						break;
//					case Bundle.RESOLVED:
//						System.out.println("Bundle is RESOLVED");
//						break;
//					case Bundle.UNINSTALLED:
//						System.out.println("Bundle is UNINSTALLED");
//						break;
//					case Bundle.STARTING:
//						System.out.println("Bundle is STARTING");
//						break;
//					case Bundle.STOPPING:
//						System.out.println("Bundle is STOPPING");
//						break;
//					default:
//						System.out.println("Bundle has not the cycle");
//					}
					
					if (bundle.getState() == Bundle.RESOLVED) {
						bundle.start();
					}
				}
			}
		} catch (BundleException e) {
			System.err.println("Bundle exception");
			e.printStackTrace();
		}		

		//gwTypes= new ArrayList<String>();
		gwMgtMap= new HashMap<String,IGatewayFactory>();
		
		//Obtains the SIB manager service
		mgtstc= new ServerManagerServiceTrackerCustomizer(bc);
		ServiceTracker serverTracker = new ServiceTracker(bc, ISIBFactory.class.getName(), mgtstc);
		serverTracker.open();
		
		//Obtains the Generic Gateway manager service
		gwmgtstc= new GatewayManagerServiceTrackerCustomizer(bc);
		ServiceTracker tcpipGwTracker = new ServiceTracker(bc, IGatewayFactory.class.getName(), gwmgtstc);
		tcpipGwTracker.open();	
		
		//Obtains the SIB instances
		sibstc= new SIBServiceTrackerCustomizer(bc);
		ServiceTracker sibTracker= new ServiceTracker(bc, ISIB.class.getName(), sibstc);
		sibTracker.open();
		
		//Obtains the Gateway instances
		gwstc= new GatewayServiceTrackerCustomizer(bc);
		ServiceTracker gwTracker= new ServiceTracker(bc, IGateway.class.getName(),gwstc);
		gwTracker.open();
		
		
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
		this.mgtstc=null;
		this.gwmgtstc=null;
		this.sibstc=null;
		this.gwstc=null;
		
		gwMgtMap.clear();
		this.gwmgtstc=null;
	}
	
	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path the path
	 * @return ImageDescriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}

	/**
	 * Retrieves the list of SIB instances
	 * @return
	 */
	public ArrayList<Element> getSibList() {
		return sibList;
	}
			
}
