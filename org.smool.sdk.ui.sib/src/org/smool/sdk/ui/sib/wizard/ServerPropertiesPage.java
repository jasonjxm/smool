/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Cristina L�pez (Fundacion European Software Institute) - initial API, implementation and documentation
 *******************************************************************************/

package org.smool.sdk.ui.sib.wizard;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.smool.sdk.sib.service.ISIBFactory;

/**
 * Wizard page called by the New Server Wizard that collects the properties
 * necessary to create a new Server object
 * 
 * @author Cristina L�pez, cristina.lopez@esi.es, ESI
 * 
 */
public class ServerPropertiesPage extends WizardPage {

	/**
	 * Name of the new server
	 */
	public Text nameServer;

	/**
	 * Wizard page Constructor
	 * 
	 * @param pageName  String
	 * @param serverMgt ISIBManager
	 * @param root      List<SibProp>
	 */
	protected ServerPropertiesPage(String pageName, ISIBFactory serverMgt) {
		super(pageName);
		this.setTitle("SIB properties");
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.WizardPage
	 */
	public void createControl(Composite parent) {

		Composite composite = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		composite.setLayout(layout);
		setControl(composite);
		this.setPageComplete(false);
		new Label(composite, SWT.NONE).setText("SIB Name: ");
		nameServer = new Text(composite, SWT.SINGLE | SWT.BORDER);
		nameServer.setText("sib1");
		this.setErrorMessage("The name of the server must be set");
		nameServer.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		nameServer.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				validateContent();
			}
		});

		validateContent();

	}

	/**
	 * Method used to check is the value of a Text element is null
	 * 
	 * @param text
	 * @return boolean
	 */
	private boolean isNull(Text text) {
		Boolean _return = false;

		if (text.getText().length() == 0) {
			_return = true;
		}
		if (text.getText() == null) {
			_return = true;
		}
		if (text.getText().trim().length() == 0) {
			_return = true;
		}

		return _return;
	}

	/**
	 * Method used to validate if the page have collected correct values. If not, an
	 * error message is shown.
	 */
	private void validateContent() {
		if (isNull(nameServer)) {
			setErrorMessage("The name of the server must be set");
			setPageComplete(false);
		} else if (!isNull(nameServer)) {
			setErrorMessage(null);
			setPageComplete(true);
		}
	}

}
