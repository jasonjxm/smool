/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Cristina L�pez (Fundacion European Software Institute) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.sib.wizard;


import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.smool.sdk.sib.service.ISIBFactory;


/**
 * Wizard used to create a new Server element. 
 * This wizard is created when the New Server button/action is pressed in the servers view
 * 
 * @author Cristina L�pez, cristin.lopez@esi.es, ESI
 *
 */
public class NewServerWizard extends Wizard {

	/**
	 * Page that show the properties that have to be collected in order to 
	 * create a new Server object.
	 */
	private ServerPropertiesPage page;

	/**
	 * Server service manager used to perform some validation on the properties collected
	 */
	private ISIBFactory serverMgt;
	
	/**
	 * Name of the server created by the wizard
	 */
	private String name;
	
	/**
	 * Property that serves to indicate if the Cancel button has been pressed
	 */
	private boolean cancelled=false;

	
	/**
	 * Wizard Constructor
	 * @param serverMgt ISIBManager
	 * @param root List<SibProp>
	 */
	public NewServerWizard(ISIBFactory serverMgt){
		this.serverMgt = serverMgt;
		this.setWindowTitle("New Semantic Information Broker Wizard");
	}
	
	/** 
	 * Retrieves the name of the server established in the wizard
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Retrieves the property that indicate if the Cancel button has been pressed
	 * @return
	 */
	public boolean isCancelled() {
		return cancelled;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.wizard.Wizard
	 */
    public void addPages() {
    	page = new ServerPropertiesPage("SIB Properties Page", serverMgt/*, root*/);
        addPage(page);
    }

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.wizard.Wizard
	 */
	public boolean performFinish() {
		name = page.nameServer.getText();
		try {
			PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView("org.smool.sdk.ui.sibviewer.SibViewer");
		} catch (PartInitException e) {
			e.printStackTrace();
		}		
		return true;
	}
	
	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.wizard.Wizard
	 */
	public boolean performCancel() {
		cancelled=true;
		return super.performCancel();
	}
}
