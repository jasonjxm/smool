/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Cristina L�pez (Fundacion European Software Institute) - initial API, implementation and documentation
 *******************************************************************************/

package org.smool.sdk.ui.sib.wizard;

import java.util.HashMap;
import java.util.Set;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.smool.sdk.gateway.service.IGatewayFactory;

/**
 * Wizard page called by the New Gateway Wizard that collects the properties
 * necessary to create a new Gateway object
 * 
 * @author Cristina L�pez, cristina.lopez@esi.es, ESI
 *
 */
public class GatewayTypesPage extends WizardPage {

	/**
	 * Name of the new gateway
	 */
	public Text nameGateway;
	/**
	 * Type of the new gateway
	 */
	public Combo typeGateway;
	/**
	 * Dynamic properties that change depending on the type of gateway
	 */
	private HashMap<String, IGatewayFactory> gwMgtMap;

	/**
	 * Wizard page Constructor
	 * 
	 * @param pageName               String
	 * @param configurableProperties Properties
	 */
	protected GatewayTypesPage(String pageName, HashMap<String, IGatewayFactory> gwMgtMap) {
		super(pageName);
		this.setTitle("Gateway type");
		this.gwMgtMap = gwMgtMap;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.WizardPage
	 */
	public void createControl(Composite parent) {
		final Composite composite = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		composite.setLayout(layout);
		setControl(composite);
		new Label(composite, SWT.NONE).setText("Gateway Name: ");
		nameGateway = new Text(composite, SWT.SINGLE | SWT.BORDER);
		nameGateway.setText("gw");
		nameGateway.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		this.setErrorMessage("The name of the gateway must be set");
		nameGateway.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				validateContent();
			}
		});

		new Label(composite, SWT.NONE).setText("Type: ");
		Object[] obj = this.gwMgtMap.keySet().toArray();
		String[] types = new String[obj.length];
		for (int i = 0; i < obj.length; i++) {
			types[i] = obj[i].toString();
		}

		typeGateway = new Combo(composite, SWT.READ_ONLY);
		typeGateway.setItems(types);
		// set tcp as default option (if available)
		int defaultIndex = 0;
		int r = 0;
		for (String type : types) {
			if (type.toLowerCase().contains("tcp")) {
				defaultIndex = r;
				break;
			}
			r++;
		}
		// end
		typeGateway.select(defaultIndex);
		typeGateway.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		typeGateway.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				validateContent();
			}
		});

		setPageComplete(false);

		validateContent();

	}

	/**
	 * Method used to check is the value of a Text element is null
	 * 
	 * @param text
	 * @return boolean
	 */
	private boolean isNull(String text) {
		Boolean _return = false;

		if (text == null) {
			_return = true;
		} else if (text.length() == 0) {
			_return = true;
		} else if (text.trim().length() == 0) {
			_return = true;
		}

		return _return;
	}

	/**
	 * Method used to validate if the page have collected correct values. If not, an
	 * error message is shown.
	 */
	private void validateContent() {
		if (isNull(nameGateway.getText())) {
			setErrorMessage("The name of the gateway must be set");
			setPageComplete(false);
		} else if (isInvalidType(typeGateway.getText())) {
			setErrorMessage("The type of the gateway must be set. Please, select a value from the list");
			setPageComplete(false);
		} else if (!isNull(nameGateway.getText()) && !isInvalidType(typeGateway.getText())) {
			if (!isPageComplete()) {
				setPageComplete(true);
			}
			setErrorMessage(null);
		}
	}

	/**
	 * Method that check if the gateway type selected is valid
	 * 
	 * @param text
	 * @return boolean
	 */
	private boolean isInvalidType(String text) {
		Boolean _return = false;
		Set<String> types = this.gwMgtMap.keySet();

		if (isNull(text)) {
			_return = true;
		} else if (!types.contains(text)) {
			_return = true;
		}

		return _return;
	}

	/**
	 * Retrieves the next page of the wizard
	 */
	public IWizardPage getNextPage() {
		GatewayPropertiesPage page = (GatewayPropertiesPage) this.getWizard().getPage("Gateway Properties Page");
		page.create();
		return page;
	}

}
