/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Cristina L�pez (Fundacion European Software Institute) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.sib.wizard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Text;
import org.smool.sdk.gateway.service.IGatewayFactory;


/**
 * Wizard page called by the New Gateway Wizard that collects the properties
 * necessary to create a new Gateway object
 * 
 * @author Cristina López, cristina.lopez@esi.es, ESI
 * 
 */
public class GatewayPropertiesPage extends WizardPage{

	private static final String ADDRESSES = "ADDRESSES";
	
	private static final String DEFAULT_IPADDRESS = "DEFAULT_IPADDRESS";
	
	//this is just to keep backward compatibility with releases prior 2.0.4
	private static final String HOST = "HOST"; //TODO Angel: remove HOST, and code related to HOST (TcpIpConnector and etc)
	
	private static final String SELECT_OPTION = "Please select a valid IP...";
	
	/**
	 * Dynamic properties that change depending on the type of gateway
	 */
	private Properties configurableProperties;
	
	/**
	 * Map containing the Gateway Factory classes corresponding to the different types of Gateway
	 */
	private HashMap<String, IGatewayFactory> gwMgtMap;
	
	/**
	 * Array of radio buttons used for the network adapter names
	 */
	private Button[] radios;
	
	/**
	 * Array of combos used for the network addresses of each network adapter
	 */
	private Combo[] combos;
	
	/**
	 * Used to store the selected ip index
	 */
	private int selectedIndex;
	
	/**
	 * Element used to display the different configurable properties
	 */
	private Text text;
	
	/**
	 * Network adapter addresses combo
	 */
	public Combo naCombo;
	
	/**
	 * Composite structure of the wizard
	 */
	private Composite composite;
	
	/**
	 * Composite structure of the wizard
	 */
	private Composite parent;
	
	/**
	 * Constructor
	 * @param pageName
	 * @param gwMgtMap
	 */
	protected GatewayPropertiesPage(String pageName, HashMap<String, IGatewayFactory> gwMgtMap) {
		super(pageName);
		this.setTitle("Gateway properties");
		this.gwMgtMap=gwMgtMap;
		selectedIndex = 0;
		
	}
	
	/**
	 * Method that retrieves the configurable dynamic properties
	 * @return
	 */
	public Properties getConfigurableProperties() {
		return configurableProperties;
	}

	@Override
	public void createControl(Composite parent) {
		this.parent=parent;
		this.composite = new Composite(parent, SWT.NONE);
        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        this.composite.setLayout(layout);
        setControl(this.composite);
        
        //Get the center of the display
        Display d = GatewayPropertiesPage.getDisplay();
        Monitor primary = d.getPrimaryMonitor();
        Rectangle bounds = primary.getBounds();
        Rectangle rect = this.getShell().getBounds();
        
        int x = bounds.x + (bounds.width - rect.width) / 2;
        int y = bounds.y + (bounds.height - rect.height) / 2;
        //Place the wizard page in the middle
        this.getShell().setSize(550, 550);
        this.getShell().setLocation(x, y);
        
       
	}

	//@SuppressWarnings({ "unchecked", "rawtypes" })
	@SuppressWarnings({ "unchecked" })
	void create() 
	{
		//Disable finish on creation
		setPageComplete(false);
		
		final Properties copyOfGwProperties = new Properties();

		if (composite!=null)
		{
			this.composite.dispose();
			this.composite = new Composite(parent, SWT.NONE);
	        GridLayout layout = new GridLayout();
	        layout.numColumns = 2;
	       
	        this.composite.setLayout(layout);
	        setControl(this.composite);
		}
        
		GatewayTypesPage previousPage=(GatewayTypesPage)this.getPreviousPage();
		final String selectedType=previousPage.typeGateway.getText();
		
		this.configurableProperties=this.gwMgtMap.get(selectedType).getConfigurableProperties();
		//get a copy of properties, so we can avoid ConcurrentModificationException, happening if we iterate over a hashmap and insert items within the loop
		copyOfGwProperties.putAll(this.configurableProperties);
		Iterator<Object> properties=copyOfGwProperties.keySet().iterator();
		
        while(properties.hasNext())
        {
	       	Object p = properties.next();
	       	String label = p.toString().substring(0, 1).toUpperCase()+p.toString().substring(1, p.toString().length()).toLowerCase();
	       	
	       	   
	       	//check that property is not NETWORK_ADAPTER and dont show TCPIP properties which are useless from the user point of view
	       	if (!label.toUpperCase().equals("NETWORK_ADAPTER") && 
	       		!label.toUpperCase().equals("DEFAULT_CONTROLLER_WORKERS") && 
	       		!label.toUpperCase().equals("CHANNEL_WRITE_SLEEP") &&
	       		!label.toUpperCase().equals("ADDRESSES") &&
	       		!label.toUpperCase().equals("EVENT_WRITER_WORKERS") &&
	       		!label.toUpperCase().equals("CHANNEL_READER_SLEEP") &&
	       		!label.toUpperCase().equals("WRITE_QUEUE_SIZE"))
	       	{
	       		
	       		new Label(this.composite,SWT.NONE).setText(label);
	       		text= new Text(this.composite,SWT.SINGLE | SWT.BORDER);
		       	text.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		       	text.setToolTipText(label);
		       	text.setText(this.gwMgtMap.get(selectedType).getConfigurableProperties().get(p).toString());
		       	text.addModifyListener(new ModifyListener() 
		       	{
		       		public void modifyText(ModifyEvent e) 
		       		{
	      				Text current=(Text)e.getSource();
	      				String text=current.getText();
		       			if (isNull(text))
		       			{
		       				setErrorMessage("Please, make sure that all the properties have been set");
		      				setPageComplete(false);	
		      			}
		      			else
		      			{
		      				configurableProperties.put(current.getToolTipText().toUpperCase(), text);
		      				String message = validateContent();
		      				if (message.equalsIgnoreCase("Content valid")){
		      					setPageComplete(true);
			      				setErrorMessage(null);
		      				}
		      				else{
		      					setPageComplete(false);
			      				setErrorMessage(message);
		      				}
		      					
		      				
		      				
		      			}
		      		}


		         }); 
	       	}
	       	//Otherwise a special set of controls have to be built for the NETWORK_ADAPTER property
	       	else
	       	{
	       		if (label.toUpperCase().equals("NETWORK_ADAPTER")){
		       		
	       			new Label(this.composite,SWT.NONE).setText("Select IP Adress[es]:");
		       		new Label(this.composite,SWT.NONE).setText("");
		       		
		       		//HashMap with pairs <adapterName, listOfAddresses>
		       		HashMap<String, ArrayList<String>> mappings;
		       		//ArrayList for the addresses
		       		ArrayList<String> addresses=new ArrayList<>();
		    		
		       		String adapterName;
		       		
		       		//get the mappings for the NETWORK_ADAPTERR property	
		       		//mappings = (HashMap<String, ArrayList<String>>) this.gwMgtMap.get(selectedType).getConfigurableProperties().get(p);
		       		mappings = (HashMap<String, ArrayList<String>>) configurableProperties.get(p);
		       		
		       		radios = new Button[mappings.size()];
		       		combos = new Combo[mappings.size()];
		       		
		       		
		       		
		       		//iterate the mappings and create a label with the adapter name
		       		//and a combo box with the associated list of addresses
		       	    
		       		Iterator<Map.Entry<String, ArrayList<String>>> it = mappings.entrySet().iterator();
		       		
		       		
		       		//iterator counter
		       		int i = 0;
		       		
		       		while (it.hasNext()) 
		       		{
		       			
		       			Map.Entry<String, ArrayList<String>> e = (Map.Entry<String, ArrayList<String>>)it.next();
		       			adapterName = e.getKey().toString();
		       			addresses = (ArrayList<String>) e.getValue();
		       		
		       				
		       			//Now add the  combos with the ip addresses
		       			String[] types= new String[addresses.size()+1];
		       			types[0]= GatewayPropertiesPage.SELECT_OPTION;
		       			
			       		for(int k=0; k<addresses.size();k++)
				        {
			       			types[k+1]=adapterName + " - " + addresses.get(k).toString();
				        }
				        combos[i] = new Combo(composite,SWT.READ_ONLY); 
				        combos[i].setItems(types);
				        combos[i].select(0);
				        combos[i].setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
				        combos[i].addModifyListener(new ModifyListener() 
				        {
					   			public void modifyText(ModifyEvent e) 
					   			{
					   				boolean validIPs = false;
					   				boolean validDefault = false;
					   				
					   				//determine the selected combo
									int selectedCombo = 0;
									String defaultAddress = new String();
									//Array list to store the selected ip addresses
						    		ArrayList<String> selectedAddresses = new ArrayList<String>();
						    		
									//iterate the radio buttons array to see what option was selected
									for (int i=0; i<combos.length; i++)
									{							
										if (e.widget.equals(combos[i]))
										{
											selectedCombo = i;
											
											//Need to check if the user has selected the default text again
											if (combos[i].getText().equals(GatewayPropertiesPage.SELECT_OPTION))
											{
												//disable the check button associated
												radios[i].setSelection(false);
												radios[i].setEnabled(false);
												selectedIndex = -1;
											}
											else
											{
												//set the correspondent check button to enabled + selected
												radios[i].setEnabled(false);
												radios[i].setSelection(true);
												selectedIndex = selectedCombo;
											}
											
										}

										//otherwise disable the rest of check buttons if they don't have an IP assigned
										else
										{
												radios[i].setSelection(false);
												radios[i].setEnabled(false);
												selectedIndex = -1;
										}
										
										
										//get the default address
										if (!validDefault && selectedIndex==selectedCombo)
										{
											//check that the default IP is actually an IP
											if (!combos[selectedCombo].getText().equals(GatewayPropertiesPage.SELECT_OPTION))
											{
												//Find the hyphen char in the string "-"
												defaultAddress = combos[selectedCombo].getText();
												defaultAddress = defaultAddress.substring(defaultAddress.indexOf("-") + 3);
												validDefault = true;
											}										
										}
										
										//check if more than a IP was selected and put in in selectedAddresses
										if (!combos[i].getText().equals(GatewayPropertiesPage.SELECT_OPTION))
										{
											//Find the hyphen char in the string "-"
											String t = combos[i].getText();
											t = t.substring(t.indexOf("-") + 3);
											selectedAddresses.add(t);
											validIPs = true;
										}

									}

									
									//check if at least an IP was selected
									if (validIPs && validDefault)
									{
										//insert the arraylist in the properties with key ADDRESSES
										configurableProperties.put(GatewayPropertiesPage.ADDRESSES, selectedAddresses);
										//insert the defaultAddress in the propertis with key DEFAULT_IPADDRESS
										configurableProperties.put(GatewayPropertiesPage.DEFAULT_IPADDRESS, defaultAddress);
										//Enable finish on creation
										setPageComplete(true);
										
									}
									else
										//Disable finish on creation
										setPageComplete(false);
								
					   			}
					    }); 
	       			

		       			//create a radio button to select the adapter
		       			radios[i] = new Button(this.composite, SWT.CHECK);
		       			radios[i].setText("Default");
		       			radios[i].setToolTipText("Make this IP the default one. KP's will connect to it if discovering method is used.");
		       		    radios[i].setBounds(10, 5, 75, 30);
		       		    radios[i].addSelectionListener(new SelectionListener() 
		       		    {

							@Override
							public void widgetDefaultSelected(SelectionEvent e) 
							{}

							@Override
							public void widgetSelected(SelectionEvent e) 
							{}
		       		    	
		       		    });
		       		    //increase counter
		       		    i++;
		       		       			
		       		}
		       		
		       		
		       		
		       		//select the first network adapter by default
		    		for (int l=0; l<radios.length; l++)
		    		{
		    			if (l==0)
		    			{
		    				radios[l].setSelection(true);
		    				radios[l].setEnabled(false);
		    		    	combos[l].select(1);
		    		    }
		    		    else
		    		    	radios[l].setEnabled(false);
		    		}
		    		
		    		//insert default selection of ADDRESSES and DEFAULTIPADDRESS in the copy of properties
		    		String defaultAddress = combos[0].getText();
		    		defaultAddress = defaultAddress.substring(defaultAddress.indexOf("-") + 3);
		    		//insert the arraylist in the properties with key ADDRESSES
		    		List<String> addrs=new ArrayList<>();
		    		for(ArrayList<String> arr : mappings.values()) addrs.add(arr.get(0).substring(1));
		    		configurableProperties.put(GatewayPropertiesPage.ADDRESSES, addrs);
		    		//insert the defaultAddress in the properties with key DEFAULT_IPADDRESS
		    		configurableProperties.put(GatewayPropertiesPage.DEFAULT_IPADDRESS, defaultAddress);
		    		//insert the defaultAddress in the properties with key HOST just to keep backward compatibility to old releases
		    		configurableProperties.put(GatewayPropertiesPage.HOST, defaultAddress);
	       		}

	       		
	       	}
	       	
        }
        
        //now that the loop is done, we can overwrite contents of configurableProperties with the contents of copyOfGwProperties
        //this.configurableProperties.putAll(copyOfGwProperties);

        composite.pack();
		//Enable the finish button
		setPageComplete(true);


	    
	}

	
	/**
	 * Method used to check is the value of a Text element is null
	 * @param text
	 * @return boolean
	 */
	private boolean isNull (String text){
		Boolean _return=false;
		
		if (text == null) {
			_return=true;
		} else if (text.length()==0){
			_return=true;
		} else if (text.trim().length()==0){
			_return=true;
		}
		
		return _return;
	}
	
	/**
	 * Method used to check if the given content in config properties attribute is valid
	 * @return boolean		True if content is valid, false otherwise
	 */
	private String validateContent() {
		
		String message = new String();
		GatewayTypesPage previousPage=(GatewayTypesPage)this.getPreviousPage();
		final String selectedType=previousPage.typeGateway.getText();
		if (selectedType.equalsIgnoreCase("tcp/ip")){
			//check that port is a number
			try{
				
				int port = Integer.parseInt((String)this.configurableProperties.get("PORT"));
				if (port > 65535){
					message = "Error, Port can not be greater than 65535";
					return message;
				}	
				else
					message = "Content valid";
				//check that net buffer size is a valid integer
				try{
					int bsize = Integer.parseInt((String)this.configurableProperties.get("NET_BUFFER_SIZE"));
					//check that size is no bigger than 4096, for instance
					if (bsize>4096){
						message = "Error, Net Buffer Size can not be greater than 4096 bytes";
						return message;
					}
					else
						message = "Content valid";
				}
				catch(NumberFormatException e){
					message = "Error, Net Buffer Size must be a valid number";
					return message;
				}
				
			}
			catch(NumberFormatException e){
				message = "Error, port must be valid integer number.";
				return message;
			}
		}
		else{
			if (selectedType.equalsIgnoreCase("bluetooth")){
				//Check that the uuid is in valid format
				String uuid = (String)this.configurableProperties.get("UUID");
				if (isUUID(uuid)){
					message = "Content valid";
				}
				else{
					message = "Error, UUID must have valid format, 32 hexadecimal characters with no hyphens.";
					return message;
				}
				
				try{
					//check that response_retry_number is a digit
					int retries = Integer.parseInt((String)this.configurableProperties.get("RESPONSE_RETRY_NUMBERS"));
					//check that size is no bigger than 4096, for instance
					if (retries>10){
						message = "Error, more than 10 retries is not permitted.";
						return message;
					}
					else
						message = "Content valid";
				}
				catch(NumberFormatException e){
					message = "Error, Reponse Retry Number must be a valid numeric character.";
					return message;
				}
				
				
			}
		}
		return message;
	}
	
	/**
	 * Checks that a given UUID is valid. Valid means 32 hexadecimal characters
	 * @return boolean		True if content is valid, false otherwise
	 */
    public boolean isUUID(String id){

    	if (!id.isEmpty()){

    		if (id.matches("[0-9a-fA-F]{32}"))
    			return true;
        }

        return false;

    }
	
	
		
	//Method that returns current or default display
	public static Display getDisplay() 
	{
		Display display = Display.getCurrent();
		//may be null if outside the UI thread
		if (display == null)
			display = Display.getDefault();
		return display;		
	}
}
