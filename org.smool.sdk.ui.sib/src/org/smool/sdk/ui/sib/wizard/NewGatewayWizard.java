/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Cristina L�pez (Fundacion European Software Institute) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.sib.wizard;

import java.util.HashMap;
import java.util.Properties;

import org.eclipse.jface.wizard.Wizard;
import org.smool.sdk.gateway.service.IGatewayFactory;


/**
 * Wizard used to create a new Gateway element. 
 * This wizard is created when the New Gateway button/action is pressed in the servers view
 * 
 * @author Cristina López, cristina.lopez@esi.es, ESI
 *
 */
public class NewGatewayWizard extends Wizard {

	/**
	 * Page that show the properties that have to be collected in order to 
	 * create a new Gateway object.
	 */
	private GatewayTypesPage page1;
	private GatewayPropertiesPage page2;
	
	/**
	 * Name of the gateway created by the wizard
	 */
	String name;
	
	/**
	 * Type of the gateway created by the wizard
	 */
	String type;
	
	/**
	 * Additional dynamic properties
	 */
	private Properties configurableProperties;
	
	/**
	 * Dynamic properties that change depending on the type of gateway
	 */
	
	private HashMap<String, IGatewayFactory> gwMgtMap;
	
	/**
	 * Property that serves to indicate if the Cancel button has been pressed
	 */
	private boolean cancelled=false;
	
	/**
	 * Wizard Constructor
	 * @param configurableProperties
	 */
	public NewGatewayWizard(HashMap<String, IGatewayFactory> gwMgtMap){
		 this.setWindowTitle("New Gateway Wizard");
		 this.gwMgtMap=gwMgtMap;
		 
	}
	
	/**
	 * Retrieves the name of the gateway created by the wizard
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * Retrieves the type of the gateway created by the wizard
	 * @return
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * Retrieves the values established for the dynamic configurable properties
	 * @return
	 */
	public Properties getConfigurableProperties() {
		return configurableProperties;
	}
	
	/**
	 * Retrieves the property that indicate if the Cancel button has been pressed
	 * @return
	 */
	public boolean isCancelled() {
		return cancelled;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.wizard.Wizard
	 */
    public void addPages() {
    	page1 = new GatewayTypesPage("Gateway Type Page", this.gwMgtMap);
    	addPage(page1);
    	page2= new GatewayPropertiesPage("Gateway Properties Page", this.gwMgtMap);      
        addPage(page2);
        
    }

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.wizard.Wizard
	 */
	public boolean performFinish() {
		
		name= page1.nameGateway.getText();
		type=page1.typeGateway.getText();
		configurableProperties= page2.getConfigurableProperties();		
		return true;
	}

	@Override
	public boolean performCancel() {
		cancelled=true;
		return super.performCancel();
	}
	
	

}
