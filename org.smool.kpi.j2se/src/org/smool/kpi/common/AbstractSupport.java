/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.common;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractSupport<T> {
	
	protected List<T> listeners = null;
	
	public AbstractSupport() {
		super();
		listeners = new ArrayList<T>( );
	}
  
    public void addListener(T l) {
        if(l == null) {
            throw new IllegalArgumentException("The listener can not be null");
        }
        synchronized(listeners) {
        	listeners.add(l);
        }
        
    }

    public void removeListener(T l) {
    	synchronized(listeners) {
    		listeners.remove(l);
    	}
    }
}
