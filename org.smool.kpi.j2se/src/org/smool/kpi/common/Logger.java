/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Alejandro Villamarin (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/

package org.smool.kpi.common;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.exception.ExceptionUtils;


public final class Logger {

	private static final int INFO = 1;
	private static final int WARN = 2;
	private static final int ERROR = 3;
	private static final int DEBUG = 4;
	
	private static SimpleDateFormat dateTime;
	
	private static boolean isDatetimeActivated;
	private static boolean isTypeMessageActivated;
	private static boolean isClassNameActivated;
	
	private static int debugLevel;
	
	//set debugging to true by default
	private static boolean isDebugging=true;
	private static FileWriter log;
	private static boolean dumpToFile;
	private static final KPProperties kpProperties = KPProperties.getInstance();
	
	static {
		dateTime = new SimpleDateFormat("dd.MM.yy HH:mm:ss.SSS");
		isDatetimeActivated = kpProperties.getBoolean( "DEBUG_OPT_DATETIME" );
		isTypeMessageActivated = kpProperties.getBoolean( "DEBUG_OPT_TYPEMSG" );
		isClassNameActivated = kpProperties.getBoolean( "DEBUG_OPT_CLASSNAME" );
		//set debug Level to 4 by default
		setDebugLevel( kpProperties.getInteger( "DEBUG_LEVEL", 4 ) );
		setDebugging( kpProperties.getBoolean("DEBUG") );
		setDumpToFile( kpProperties.getBoolean( "DEBUG_TO_FILE" ) );
	}

	/**
	 * Check DateAndTime flag value
	 * @param None
	 * @return boolean
	 * 
	 */
	public static boolean isDatetimeActivated() {
		return isDatetimeActivated;
	}

	/**
	 * Set DateAndTime flag value
	 * @param datetimeActivated		Value for the DateAndTime flag
	 * @return None
	 * 
	 */
	public static void setDatetimeActivated(boolean datetimeActivated) {
		Logger.isDatetimeActivated = datetimeActivated;
	}

	/**
	 * Check isTypeMessageActivated flag value
	 * @param None
	 * @return boolean
	 * 
	 */
	public static boolean isTypeMessageActivated() {
		return isTypeMessageActivated;
	}

	/**
	 * Set isTypeMessageActivated flag value
	 * @param typeMessageActivated 		Value for the isTypeMessageActivated flag
	 * @return None
	 * 
	 */
	public static void setTypeMessageActivated(boolean typeMessageActivated) {
		Logger.isTypeMessageActivated = typeMessageActivated;
	}

	/**
	 * Check isClassNameActivated flag value
	 * @param None
	 * @return boolean
	 * 
	 */
	public static boolean isClassNameActivated() {
		return isClassNameActivated;
	}

	/**
	 * Set isClassNameActivated flag value
	 * @param classNameActivated 		Value for the isClassNameActivated flag
	 * @return None
	 * 
	 */
	public static void setClassNameActivated(boolean classNameActivated) {
		Logger.isClassNameActivated = classNameActivated;
	}

	/**
	 * Check debug level 
	 * @param None
	 * @return int		Possible values are: 1 - INFO, 2 - WARN, 3 - ERROR, 4 - DEBUG
	 * 
	 */
	public static int getDebugLevel() {
		return debugLevel;
	}

	/**
	 * Set debugLevel value
	 * @param debugLevel 		Value for debugLevel attribute. Possible values are: 1 - INFO, 2 - WARN, 3 - ERROR, 4 - DEBUG
	 * @return None
	 * 
	 */
	public static void setDebugLevel(int debugLevel) {
		Logger.debugLevel = debugLevel;
	}
	
	/**
	 * Set dumpToFile value, used to log also into a file
	 * @param b 		Value for dumpToFile attribute. 
	 * @return None
	 * 
	 */
	public static void setDumpToFile(boolean b) {
		Logger.dumpToFile = b;
	}
	
	/**
	 * Default constructor
	 * @return None
	 * 
	 */
	private Logger() {
	}
	
	/**
	 * Prints message if debugLevel is set to DEBUG
	 * @param msg 		The logging message to be printed 
	 * @return None
	 * 
	 */
	public static void debug(String msg) {
		Logger.debug(null, msg);	
	}

	/**
	 * Prints message if debugLevel is set to DEBUG
	 * @param className		The class Name that logs the message
	 * @param msg 			The logging message to be printed 
	 * @return None
	 * 
	 */
	public static void debug(String className, String msg) {
		//check if debugging flag is true
		if(isDebugging()) {
			//check that debugLevel is at least DEBUG
			if(debugLevel >= DEBUG) {
				Logger.write(className, msg, Logger.DEBUG);
			}
//			else
//				System.out.println("Current Debug level does not allow DEBUG mode. Please change debug level.");
		}
	}
	
	/**
	 * Prints message if debugLevel is set to INFO
	 * @param msg 		The logging message to be printed 
	 * @return None
	 * 
	 */
	public static void info(String msg) {
		Logger.info(null, msg);	
	}

	/**
	 * Prints message if debugLevel is set to INFO
	 * @param className		The class Name that logs the message
	 * @param msg 		The logging message to be printed 
	 * @return None
	 * 
	 */
	public static void info(String className, String msg) {
		//check that debugLevel is at least INFO
		if(debugLevel >= INFO) {
			Logger.write(className, msg, Logger.INFO);
		}
//		else
//			System.out.println("Current Debug level does not allow INFO mode. Please change debug level.");
	}

	/**
	 * Prints message if debugLevel is set to ERROR
	 * @param msg 		The logging message to be printed 
	 * @return None
	 * 
	 */
	public static void error(String msg) {
		Logger.error(null, msg);
	}

	/**
	 * Prints message if debugLevel is set to ERROR, appending the error to the stack trace
	 * @param msg 		The logging message to be printed
	 * @param e			Exception thrown with the error 
	 * @return None
	 * 
	 */
	public static void error(String msg, Throwable e) {
		Logger.error(null, msg+'\n'+ExceptionUtils.getStackTrace( e ));
	}

	/**
	 * Prints message if debugLevel is set to ERROR
	 * @param className		The class Name that logs the message
	 * @param msg 		The logging message to be printed 
	 * @return None
	 * 
	 */
	public static void error(String className, String msg) {
		
		//check that debugLevel is at least ERROR
		if(debugLevel >= ERROR) {
			Logger.write(className, msg, Logger.ERROR);
		}
//		else
//			System.out.println("Current Debug level does not allow ERROR mode. Please change debug level.");
		
	}

	/**
	 * Prints message if debugLevel is set to WARN
	 * @param msg 		The logging message to be printed 
	 * @return None
	 * 
	 */
	public static void warn(String msg) {
		Logger.warn(null, msg);	
	}

	/**
	 * Prints message if debugLevel is set to WARN
	 * @param className		The class Name that logs the message
	 * @param msg 		The logging message to be printed 
	 * @return None
	 * 
	 */
	public static void warn(String className, String msg) {
		//check that debugLevel is at least WARN
		if(debugLevel >= WARN) {
			Logger.write(className, msg, Logger.WARN);
		}
//		else
//			System.out.println("Current Debug level does not allow WARN mode. Please change debug level.");
	}
	
	/**
	 * Returns a reference to the log attribute used to write to file
	 * @return FileWriter		The log attribute initialized
	 * 
	 */
	private static FileWriter getLogger() {
		if (log == null) {
			File f = new File("kp_"+dateTime.format(new Date(System.currentTimeMillis())).replace(':', '.')+".log");
			try {
				log = new FileWriter(f);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
        }

        return log;
	}

	/**
	 * Writes the message both in console and into file, depending on both the debugLevel and other flags
	 * @param className		The className that prints the message
	 * @param msg			The message to be printed
	 * @param type			Indicates the logging message, DEBUG, ERROR, WARN or INFO
	 * @return None
	 * 
	 */
	private static void write(String className, String msg, int type) {
		//object used to build the final message
		StringBuilder sb = new StringBuilder();

		//check if date and time has to be included
		if(isDatetimeActivated) {
			sb.append("[");
			sb.append(dateTime.format(new Date(System.currentTimeMillis())));
			sb.append("]");
		}
		//check if the object causing the message hast to be included
		if(isClassNameActivated && (className != null)) {
			sb.append("[");
			sb.append(className); 
			sb.append("]");
		}
		
		sb.append(" ");
		//Check the type to see header to include in the message
		switch(type) {
			case INFO:
				sb.append("[INFO] ");
				break;
			case WARN:
				sb.append("[WARN] ");
				break;
			case DEBUG:
				sb.append("[DEBUG] ");
				break;
			case ERROR:
				sb.append("[ERROR] ");
				break;
			
		}
		//Add the message
		sb.append(msg);
		//Check if we have to write to file
		if(Logger.dumpToFile) {
			final FileWriter log = getLogger();
			try {
				log.write(sb.toString());
				log.write("\n");
				log.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		//otherwise print to console
		else {
			System.out.println(sb.toString());
		}
		
	}

	/**
	 * Check the value of the isDebugging flag
	 * @return boolean	
	 * 
	 */
    public static final boolean isDebugging() {
    	return isDebugging;
    }

	/**
	 * Set the value of the isDebugging flag
	 * @param isDebugging		Value for the isDebugging flag
	 * @return None	
	 * 
	 */	
    public static void setDebugging( boolean isDebugging ) {
    	Logger.isDebugging = isDebugging;
    }
	
}
