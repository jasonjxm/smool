/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/

package org.smool.kpi.common;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import org.smool.kpi.model.exception.KPIModelException;


public class KPProperties {

	public final static String DEFAULT_TCPIP_GW_PORT = "DEFAULT_TCPIP_GW_PORT";
	public final static String SIB_NAME = "SIB_NAME";
	public final static String DEBUG = "DEBUG";
	private static final String DUMPTOFILE = "DUMPTOFILE";
	public final static String PROPERTIES_PATH = "/config/config.properties";

	/** The path of the ontology representing this KP */
	private static final String KP_ONTOLOGY = "/ontology/ontology.owl";
	
	private static KPProperties _instance;
	
	private Properties properties;
	
	private KPProperties() {	
		loadConfiguration();
	}	
	
	public static KPProperties getInstance() {
		
		if(_instance == null) {
			synchronized(KPProperties.class) {
				if(_instance == null) {
					_instance = new KPProperties(); 
				}
			}
		}
		return _instance;
	}
	
	private void loadConfiguration() {
		try {
			this.properties = new Properties();
	
			InputStream is = this.getClass().getResourceAsStream(KPProperties.PROPERTIES_PATH);
			if(is == null) {
				return;
			}
			this.properties.load(is);
		} catch (IOException e) {
		}
	}	
	
	public String getProperty(String name) {
		return properties.getProperty(name);
	}
	
	public String getProperty(String name, String defaultValue) {
		return properties.getProperty(name,defaultValue);
	}

	public boolean getBoolean(String name) {
		// Code liberated from java.lang.Boolean.getBoolean
		boolean result = false;
		try {
			result = "true".equalsIgnoreCase(getProperty(name));
		} catch (IllegalArgumentException e) {
		} catch (NullPointerException e) {
		}
        return result;
    }

	public Integer getInteger(String nm, Integer val) {
		// Code liberated from java.lang.Integer.getInteger
		String v = null;
		try {
			v = getProperty(nm);
		} catch (IllegalArgumentException e) {
		} catch (NullPointerException e) {
		}
		if (v != null) {
			try {
				return Integer.decode(v);
			} catch (NumberFormatException e) {
			}
		}
		return val;
	}

	public boolean isDebugging() {
		String debug = this.properties.getProperty(KPProperties.DEBUG, "false");
		if(debug != null) {
			try {
				return Boolean.parseBoolean(debug);
			} catch(Exception e) {
				return false;
			}
		} else {
			return false;
		}
	}
	
	/**
	 * Gets the ontology model that represents the current KP model
	 * @return a String containing the KP model
	 * @throws KPIModelException 
	 */
	public String getModelOntology() throws Exception {
		StringBuilder initialModel = new StringBuilder();
		
		try {
			InputStream is = this.getClass().getResourceAsStream(KPProperties.KP_ONTOLOGY);

			//File file = new File(url.getFile());
			// Open the file that is the first command line parameter
		    //FileInputStream fstream = new FileInputStream(file);
		    // Get the object of DataInputStream
		    DataInputStream in = new DataInputStream(is);
		    BufferedReader br = new BufferedReader(new InputStreamReader(in));
		    String strLine;
		    //Read File Line By Line
		    while ((strLine = br.readLine()) != null)   {
				// Print the content on the console
				initialModel.append(strLine);
		    }
		    //Close the input stream
		    in.close();
		    
		    return initialModel.toString();
		} catch (FileNotFoundException fnfex) {
			fnfex.printStackTrace();
			throw new Exception("Cannot find ontology model file in the KP project", fnfex);
		} catch (IOException ioex) {
			ioex.printStackTrace();
			throw new Exception("Cannot read from the ontology file " + KPProperties.KP_ONTOLOGY, ioex);
		}
	}

	public boolean isFileDumping() {
		String dumpToFile = this.properties.getProperty(KPProperties.DUMPTOFILE, "false");
		if(dumpToFile != null) {
			try {
				return Boolean.parseBoolean(dumpToFile);
			} catch(Exception e) {
				return false;
			}
		} else {
			return false;
		}
	}
}
