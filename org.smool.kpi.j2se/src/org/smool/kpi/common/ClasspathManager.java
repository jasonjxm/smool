package org.smool.kpi.common;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.StringTokenizer;
import java.util.Vector;

/*
 * Wrapper class for allowing different class loading strategies for different use cases
 * To support 'default' class loading as well as configurable class loading from e.g. jruby.
 * 
 * Usage from jruby:
 * 
 * # import the Java jar file
 * require '...'
 * 
 * # Obtain jruby class_loader
 *  class_loader = JRuby.runtime.jruby_class_loader
 *
 * # Get the ClasspathManager
 * import Java::com.philips.kpi.common.ClasspathManager
 * cpm = ClasspathManager.getInstance()
 * cpm.setURLClassLoader(class_loader)
 * 
 */ 
public class ClasspathManager {
	
	private static final String PATH_SEPARATOR = System.getProperty("path.separator");

	private ClassLoader cl;
	private Vector<URL> urls;
	private static ClasspathManager _instance;
	
	public static ClasspathManager getInstance() {
		if(_instance == null) {
			synchronized(ClasspathManager.class) {
				if(_instance == null) {
					_instance = new ClasspathManager();
				}
			}
		}
		return _instance;
	}
	
	private ClasspathManager(){
		cl = null;
		
		// By default the elements in the class path are added to this manager
		String classpath = System.getProperty("java.class.path");
		urls = new Vector<URL>();
		StringTokenizer st = new StringTokenizer(classpath, PATH_SEPARATOR);
		while (st.hasMoreTokens()) {
			try {
				String token = st.nextToken();				
				File f = new File(token);
				urls.add(f.toURI().toURL());
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/*
	 * This purposely only accepts URLClassLoader
	 */
	public void setURLClassLoader(URLClassLoader cl) {
		this.cl = cl;
		URL[] oldURLs = cl.getURLs();
		// Forget previous URLs
		urls.clear();
		for (URL u : oldURLs){
			urls.add(u);
		}
	}

	public ClassLoader getClassLoader() {
		if (cl == null){
			// revert to the default class loader
			cl = this.getClass().getClassLoader();
		}
		return cl;
	}
	
	public URL[] getURLs(){
		URL[] result = new URL[urls.size()];
		urls.toArray(result);
		return result;
	}
	
	public void appendUrl(URL u){
		
		// add to the end
		urls.add(u);
		
		// instantiate new URLClassLoader
		cl = new URLClassLoader(getURLs());
	}
	

}
