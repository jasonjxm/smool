/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.model.direct;

import org.smool.kpi.model.exception.KPIModelException;
import org.smool.kpi.ssap.SIB;


/**
 * This class represents the Model that allows the 
 * sending of information in RDF-M3 format.
 * 
 * @author Raul Otaolea, Raul.Otaolea@tecnalia.com, Tecnalia-Software
 * @author Fran Ruiz, Fran.Ruiz@tecnalia.com, Tecnalia-Software
 */
public class N3Model extends AbstractDirectModel {

	public N3Model(SIB sib) {
		super();
		this.setSIB(sib);	
	}

	/**
	 * Inserts data into the SIB
	 * @param data data in Turtle/N3 format
	 * @return a String with the obtained blank nodes
	 * @throws KPIModelException if some error occurs in creation of the output message
	 */
	public String insert(String data) throws KPIModelException {
		return null;
//		try {
//			//
//			RDFM3 m3 = transformFromN3ToRDFM3(data);
//			
//		    return super.insert(m3.toString(), TypeAttribute.RDFM3);
//		} catch (KPIModelException kpiex) {
//			Logger.debug(this, "Cannot insert in the SIB: " + kpiex.getMessage());
//			throw new KPIModelException("Could not insert into SIB: " + kpiex.getMessage(), kpiex, kpiex.getStatus());			
//		} catch (Exception e) {
//			Logger.debug(this, "Cannot insert: " + e.getMessage());
//			throw new KPIModelException("Could not insert into SIB: " + e.getMessage(), e);
//		}
//		throw new KPIModelException("Not implemented");
	}
	
	/**
	 * Inserts data into the SIB
	 * @param data data in Turtle/N3 format
	 * @return a String with the obtained blank nodes
	 * @throws KPIModelException if some error occurs in creation of the output message
	 */
	public String remove(String data) throws KPIModelException {
		return null;
//		try {
//
//			RDFM3 m3 = transformFromN3ToRDFM3(data);
//			
//		    return super.remove(m3.toString(), TypeAttribute.RDFM3);
//			} catch (KPIModelException kpiex) {
//				Logger.debug(this, "Cannot remove in the SIB: " + kpiex.getMessage());
//				throw new KPIModelException("Could not remove into SIB: " + kpiex.getMessage(), kpiex, kpiex.getStatus());			
//			} catch (Exception e) {
//				Logger.debug(this, "Cannot remove: " + e.getMessage());
//				throw new KPIModelException("Could not remove from SIB: " + e.getMessage(), e);
//		}
	}


	/**
	 * Updates data into the SIB
	 * @param newData data to be inserted in Turtle/N3 format
	 * @param oldData data to be removed in Turtle/N3 format
	 * @return a String with the obtained blank nodes
	 * @throws KPIModelException if some error occurs in creation of the output message
	 */
	public String update(String newData, String oldData) throws KPIModelException {
		return null;
//		try {
//			// Creates the RDF-M3 object with the triple
//			RDFM3 insertm3 = transformFromN3ToRDFM3(newData);			
//			RDFM3 removem3 = transformFromN3ToRDFM3(newData);
//			
//	    return super.update(insertm3.toString(), removem3.toString(), TypeAttribute.RDFM3);
//		} catch (KPIModelException kpiex) {
//			Logger.debug(this, "Cannot update in the SIB: " + kpiex.getMessage());
//			throw new KPIModelException("Could not update into SIB: " + kpiex.getMessage(), kpiex, kpiex.getStatus());			
//		} catch (Exception e) {
//			Logger.debug(this, "Cannot update: " + e.getMessage());
//			throw new KPIModelException("Could not update from SIB: " + e.getMessage(), e);
//		}
//		throw new KPIModelException("Not implemented");
	}

	@Override
	public void connectedToSIB() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disconnectedFromSIB() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void connectionToSIBTimeout() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void connectionToSIBError(String error) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void kpInitialized() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void requestProcessed() {
		// TODO Auto-generated method stub
		
	}	

	
//	private RDFM3 transformFromN3ToRDFM3(String data) {
//		// TODO Auto-generated method stub
//		return null;
//	}
}
