/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.model.direct;

import java.util.HashMap;

import org.smool.kpi.common.Logger;
import org.smool.kpi.model.AbstractModel;
import org.smool.kpi.model.exception.KPIModelException;
import org.smool.kpi.ssap.PendingTask;
import org.smool.kpi.ssap.SIB;
import org.smool.kpi.ssap.exception.KPISSAPException;
import org.smool.kpi.ssap.message.parameter.SSAPMessageParameter.NameAttribute;
import org.smool.kpi.ssap.message.parameter.SSAPMessageRDFParameter.TypeAttribute;
import org.smool.kpi.ssap.message.parameter.SSAPMessageStatusParameter.SIBStatus;


/**
 * This class represents the Model that allows the 
 * sending of information in RDF-M3 format.
 * 
 * @author Raul Otaolea, Raul.Otaolea@tecnalia.com, Tecnalia-Software
 * @author Fran Ruiz, Fran.Ruiz@tecnalia.com, Tecnalia-Software
 */
public class AbstractDirectModel extends AbstractModel {

	public AbstractDirectModel() {
		super();
	}	
	
	public AbstractDirectModel(SIB sib) {
		super();
		this.setSIB(sib);
	}
	
	/**
	 * Inserts a triple into the SIB
	 * @param data the data to be inserted
	 * @param encoding the encoding to be used
	 * @return a String with the results
	 * @throws KPISSAPException if some error occurs in creation of the output message
	 */
	public String insert(String data, TypeAttribute encoding) throws KPIModelException {
		try {
			// Sends it to the SIB
			PendingTask pt = getSIB().insert(data, encoding);
			this.getSIB().publish();
			pt.waitUntilFinished(this.getTimeout());
			if(!pt.isFinished()) {
				throw new KPIModelException("Could not INSERT in SIB due to timeout");
			}
			
			// Gets the status of the response
			SIBStatus status = (SIBStatus)pt.getProperty(NameAttribute.STATUS.getValue());

			if (!status.equals(SIBStatus.STATUS_OK)) {
				throw new KPIModelException("The insertion cannot be executed due to " + status.getValue(), status);				
			}
			
			String bNodes = (String) pt.getProperty(NameAttribute.BNODES.getValue());
			return bNodes;
		} catch (KPISSAPException ssapex) {
			Logger.error(this.getClass().getName(), "Cannot insert to the SIB: " + ssapex.getCause());
			ssapex.printStackTrace();
			throw new KPIModelException("Unable to insert data into the SIB", ssapex);
		} catch (KPIModelException kpiex) {
			Logger.debug(this.getClass().getName(), "Cannot insert in the SIB: " + kpiex.getMessage());
			throw new KPIModelException("Could not insert into SIB: " + kpiex.getMessage(), kpiex, kpiex.getStatus());			
		} catch (Exception e) {
			Logger.debug(this.getClass().getName(), "Cannot insert: " + e.getMessage());
			throw new KPIModelException("Could not insert into SIB: " + e.getMessage(), e);
		}
	}
	
	/**
	 * Removes a triple from the SIB
	 * @param data the data to be removed
	 * @param encoding the encoding to be used
	 * @return a String with the results
	 * @throws KPISSAPException if some error occurs in creation of the output message
	 */
	public String remove(String data, TypeAttribute encoding) throws KPIModelException {
		try {
			// Sends it to the SIB
			PendingTask pt = getSIB().remove(data, encoding);
			this.getSIB().publish();
			pt.waitUntilFinished(this.getTimeout());
			if(!pt.isFinished()) {
				throw new KPIModelException("Could not REMOVE in SIB due to timeout");
			}
			
			// Gets the status of the response
			SIBStatus status = (SIBStatus)pt.getProperty(NameAttribute.STATUS.getValue());

			if (!status.equals(SIBStatus.STATUS_OK)) {
				throw new KPIModelException("The removal cannot be executed due to " + status.getValue(), status);				
			}
			
			return null;
		} catch (KPISSAPException ssapex) {
			Logger.error(this.getClass().getName(), "Cannot remove from the SIB: " + ssapex.getCause());
			ssapex.printStackTrace();
			throw new KPIModelException("Unable to remove data from the SIB", ssapex);
		} catch (KPIModelException kpiex) {
			Logger.debug(this.getClass().getName(), "Cannot remove in the SIB: " + kpiex.getMessage());
			throw new KPIModelException("Could not remove into SIB: " + kpiex.getMessage(), kpiex, kpiex.getStatus());			
		} catch (Exception e) {
			Logger.debug(this.getClass().getName(), "Cannot remove: " + e.getMessage());
			throw new KPIModelException("Could not remove from SIB: " + e.getMessage(), e);
		}	
	}
	
	/**
	 * Updates a triple into the SIB
	 * @param newData the data to be inserted
	 * @param oldData the data to be removed
	 * @param encoding the encoding to be used
	 * @return a String with the results
	 * @throws KPISSAPException if some error occurs in creation of the output message
	 */
	public String update(String newData, String oldData, TypeAttribute encoding) throws KPIModelException {
		try {
			// Sends it to the SIB
			PendingTask pt = getSIB().update(newData, oldData, encoding);
			this.getSIB().publish();
			pt.waitUntilFinished(this.getTimeout());
			if(!pt.isFinished()) {
				throw new KPIModelException("Could not REMOVE in SIB due to timeout");
			}
			
			// Gets the status of the response
			SIBStatus status = (SIBStatus)pt.getProperty(NameAttribute.STATUS.getValue());

			if (!status.equals(SIBStatus.STATUS_OK)) {
				throw new KPIModelException("The update cannot be executed due to " + status.getValue(), status);				
			}
			
			String bNodes = (String) pt.getProperty(NameAttribute.BNODES.getValue());
			return bNodes;
		} catch (KPISSAPException ssapex) {
			Logger.error(this.getClass().getName(), "Cannot update from the SIB: " + ssapex.getCause());
			ssapex.printStackTrace();
			throw new KPIModelException("Unable to update data from the SIB", ssapex);
		} catch (KPIModelException kpiex) {
			Logger.debug(this.getClass().getName(), "Cannot update in the SIB: " + kpiex.getMessage());
			throw new KPIModelException("Could not update into SIB: " + kpiex.getMessage(), kpiex, kpiex.getStatus());			
		} catch (Exception e) {
			Logger.debug(this.getClass().getName(), "Cannot update: " + e.getMessage());
			throw new KPIModelException("Could not update from SIB: " + e.getMessage(), e);
		}		
	}

	/**
	 * Queries about a triple in the SIB
	 * @param subject the subject
	 * @param predicate the predicate
	 * @param object the object
	 * @param subjectType the subject type
	 * @param objectType the object type
	 * @return a PendingTask that controls the reception of the message
	 * @throws KPISSAPException if some error occurs in creation of the output message
	 */
	public String query(String query, TypeAttribute encoding) throws KPIModelException {
		try {
			// Sends it to the SIB
			PendingTask pt = getSIB().query(query, encoding);
			this.getSIB().publish();
			pt.waitUntilFinished(this.getTimeout());
			if(!pt.isFinished()) {
				throw new KPIModelException("Could not INSERT in SIB due to timeout");
			}
			
			// Gets the status of the response
			SIBStatus status = (SIBStatus)pt.getProperty(NameAttribute.STATUS.getValue());

			if (!status.equals(SIBStatus.STATUS_OK)) {
				throw new KPIModelException("The query cannot be executed due to " + status.getValue(), status);				
			}
			
			String results = (String) pt.getProperty(NameAttribute.RESULTS.getValue());
			return results;
		} catch (KPISSAPException ssapex) {
			Logger.error(this.getClass().getName(), "Cannot query to the SIB: " + ssapex.getCause());
			ssapex.printStackTrace();
			throw new KPIModelException("Unable to query data into the SIB", ssapex);
		} catch (KPIModelException kpiex) {
			Logger.debug(this.getClass().getName(), "Cannot query in the SIB: " + kpiex.getMessage());
			throw new KPIModelException("Could not query into SIB: " + kpiex.getMessage(), kpiex, kpiex.getStatus());			
		} catch (Exception e) {
			Logger.debug(this.getClass().getName(), "Cannot query: " + e.getMessage());
			throw new KPIModelException("Could not query into SIB: " + e.getMessage(), e);
		}
	}	
	
	/**
	 * Subscribes to a triple in the SIB
	 * @param subject the subject
	 * @param predicate the predicate
	 * @param object the object
	 * @param subjectType the subject type
	 * @param objectType the object type
	 * @return a PendingTask that controls the reception of the message
	 * @throws KPISSAPException if some error occurs in creation of the output message
	 */
	public HashMap<NameAttribute,String> subscribe(String query, TypeAttribute encoding) throws KPIModelException {
		HashMap<NameAttribute,String> map = new HashMap<NameAttribute,String>();
		try {
			// Sends it to the SIB
			PendingTask pt = getSIB().subscribe(query, encoding);
			this.getSIB().publish();
			pt.waitUntilFinished(this.getTimeout());
			if(!pt.isFinished()) {
				throw new KPIModelException("Could not SUBSCRIBE in SIB due to timeout");
			}
			
			// Gets the status of the response
			SIBStatus status = (SIBStatus)pt.getProperty(NameAttribute.STATUS.getValue());

			if (!status.equals(SIBStatus.STATUS_OK)) {
				throw new KPIModelException("The query cannot be executed due to " + status.getValue(), status);				
			}
			
			String results = (String) pt.getProperty(NameAttribute.RESULTS.getValue());
			if (results != null && results != "") {
				map.put(NameAttribute.RESULTS, results);
			}
			
			String subscriptionId = (String) pt.getProperty(NameAttribute.RESULTS.getValue());
			if (subscriptionId != null && !subscriptionId.equals("")) {
				map.put(NameAttribute.SUBSCRIPTIONID, subscriptionId);
			}
			return map;
		} catch (KPISSAPException ssapex) {
			Logger.error(this.getClass().getName(), "Cannot query to the SIB: " + ssapex.getCause());
			ssapex.printStackTrace();
			throw new KPIModelException("Unable to query data into the SIB", ssapex);
		} catch (KPIModelException kpiex) {
			Logger.debug(this.getClass().getName(), "Cannot query in the SIB: " + kpiex.getMessage());
			throw new KPIModelException("Could not query into SIB: " + kpiex.getMessage(), kpiex, kpiex.getStatus());			
		} catch (Exception e) {
			Logger.debug(this.getClass().getName(), "Cannot query: " + e.getMessage());
			throw new KPIModelException("Could not query into SIB: " + e.getMessage(), e);
		}
	}	

	@Override
	public void connectedToSIB() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disconnectedFromSIB() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void connectionToSIBTimeout() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void connectionToSIBError(String error) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void kpInitialized() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void requestProcessed() {
		// TODO Auto-generated method stub
		
	}
}
