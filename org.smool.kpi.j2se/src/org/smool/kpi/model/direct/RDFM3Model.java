/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.model.direct;

import java.util.HashMap;
import java.util.Vector;

import org.smool.kpi.common.Logger;
import org.smool.kpi.common.rdfm3.RDFM3;
import org.smool.kpi.common.rdfm3.RDFM3Triple;
import org.smool.kpi.model.exception.KPIModelException;
import org.smool.kpi.ssap.SIB;
import org.smool.kpi.ssap.exception.KPISSAPException;
import org.smool.kpi.ssap.message.parameter.SSAPMessageParameter.NameAttribute;
import org.smool.kpi.ssap.message.parameter.SSAPMessageRDFParameter.TypeAttribute;


/**
 * This class represents the Model that allows the 
 * sending of information in RDF-M3 format.
 * 
 * @author Raul Otaolea, Raul.Otaolea@tecnalia.com, Tecnalia-Software
 * @author Fran Ruiz, Fran.Ruiz@tecnalia.com, Tecnalia-Software
 */
public class RDFM3Model extends AbstractDirectModel {

	public RDFM3Model(SIB sib) {
		super(sib);
	}
	
	/**
	 * Inserts a triple into the SIB
	 * @param subject the subject
	 * @param predicate the predicate
	 * @param object the object
	 * @param subjectType the subject type
	 * @param objectType the object type
	 * @return a PendingTask that controls the reception of the message
	 * @throws KPISSAPException if some error occurs in creation of the output message
	 */
	public String insert(String subject, String predicate, String object, String subjectType, String objectType) throws KPIModelException {
		try {
			// Creates the RDF-M3 object with the triple
			RDFM3 m3 = getRDFM3(subject, predicate, object, subjectType, objectType);
			
			return super.insert(m3.toString(), TypeAttribute.RDFM3);
		} catch (KPIModelException kpiex) {
			Logger.debug(this.getClass().getName(), "Cannot insert in the SIB: " + kpiex.getMessage());
			throw new KPIModelException("Could not insert into SIB: " + kpiex.getMessage(), kpiex, kpiex.getStatus());			
		} catch (Exception e) {
			Logger.debug(this.getClass().getName(), "Cannot insert: " + e.getMessage());
			throw new KPIModelException("Could not insert into SIB: " + e.getMessage(), e);
		}
	}
	
	/**
	 * Inserts a triple into the SIB
	 * @param tripleVector a vector of triples, each one containing the subject, predicate, object, subject type and object type in positions 1 to 5 of the vector
	 * @return a PendingTask that controls the reception of the message
	 * @throws KPIModelExceptionslowfoodsps if some error occurs in creation of the output message
	 */
	public String insert(Vector<Vector<String>> tripleVector) throws KPIModelException {
		try {
			// Creates the RDF-M3 object with the triple
			RDFM3 m3 = getRDFM3(tripleVector);
			
			return super.insert(m3.toString(), TypeAttribute.RDFM3);
		} catch (KPIModelException kpiex) {
			Logger.debug(this.getClass().getName(), "Cannot insert in the SIB: " + kpiex.getMessage());
			throw new KPIModelException("Could not insert into SIB: " + kpiex.getMessage(), kpiex, kpiex.getStatus());			
		} catch (Exception e) {
			Logger.debug(this.getClass().getName(), "Cannot insert: " + e.getMessage());
			throw new KPIModelException("Could not insert into SIB: " + e.getMessage(), e);
		}	
	}	
	
	/**
	 * Removes a triple from the SIB
	 * @param subject the subject
	 * @param predicate the predicate
	 * @param object the object
	 * @param subjectType the subject type
	 * @param objectType the object type
	 * @return a PendingTask that controls the reception of the message
	 * @throws KPISSAPException if some error occurs in creation of the output message
	 */
	public String remove(String subject, String predicate, String object, String subjectType, String objectType) throws KPIModelException {
		try {
			// Creates the RDF-M3 object with the triple
			RDFM3 m3 = getRDFM3(subject, predicate, object, subjectType, objectType);
			
			return super.remove(m3.toString(), TypeAttribute.RDFM3);
			
		} catch (KPIModelException kpiex) {
			Logger.debug(this.getClass().getName(), "Cannot remove in the SIB: " + kpiex.getMessage());
			throw new KPIModelException("Could not remove into SIB: " + kpiex.getMessage(), kpiex, kpiex.getStatus());			
		} catch (Exception e) {
			Logger.debug(this.getClass().getName(), "Cannot remove: " + e.getMessage());
			throw new KPIModelException("Could not remove from SIB: " + e.getMessage(), e);
		}	
	}
	
	/**
	 * Removes a set of triples from the SIB
	 * @param tripleVector a vector of triples, each one containing the subject, predicate, object, subject type and object type in positions 1 to 5 of the vector
	 * @return a PendingTask that controls the reception of the message
	 * @throws KPISSAPException if some error occurs in creation of the output message
	 */
	public String remove(Vector<Vector<String>> tripleVector) throws KPIModelException {
		try {
			// Creates the RDF-M3 object with the triple
			RDFM3 m3 = getRDFM3(tripleVector);
			
			return super.remove(m3.toString(), TypeAttribute.RDFM3);
			
		} catch (KPIModelException kpiex) {
			Logger.debug(this.getClass().getName(), "Cannot remove from the SIB: " + kpiex.getMessage());
			throw new KPIModelException("Could not remove from SIB: " + kpiex.getMessage(), kpiex, kpiex.getStatus());			
		} catch (Exception e) {
			Logger.debug(this.getClass().getName(), "Cannot remove: " + e.getMessage());
			throw new KPIModelException("Could not remove from SIB: " + e.getMessage(), e);
		}	
	}
	
	/**
	 * Updates a triple into the SIB
	 * @param newSubject the new subject
	 * @param newPredicate the new predicate
	 * @param newObject the new object
	 * @param newSubjectType the new subject type
	 * @param newObjectType the new object type
	 * @param oldSubject the old subject
	 * @param oldPredicate the old predicate
	 * @param oldObject the old object
	 * @param oldSubjectType the old subject type
	 * @param oldObjectType the old object type 
	 * @return a PendingTask that controls the reception of the message
	 * @throws KPISSAPException if some error occurs in creation of the output message
	 */
	public String update(String newSubject, String newPredicate, String newObject, String newSubjectType, String newObjectType,
			String oldSubject, String oldPredicate, String oldObject, String oldSubjectType, String oldObjectType) throws KPIModelException {
		try {
			// Creates the RDF-M3 object with the triple
			RDFM3 insertm3 = getRDFM3(newSubject, newPredicate, newObject, newSubjectType, newObjectType);
			RDFM3 removem3 = getRDFM3(oldSubject, oldPredicate, oldObject, oldSubjectType, oldObjectType);
			
			return super.update(insertm3.toString(), removem3.toString(), TypeAttribute.RDFM3);
			
		} catch (KPIModelException kpiex) {
			Logger.debug(this.getClass().getName(), "Cannot update in the SIB: " + kpiex.getMessage());
			throw new KPIModelException("Could not update into SIB: " + kpiex.getMessage(), kpiex, kpiex.getStatus());			
		} catch (Exception e) {
			Logger.debug(this.getClass().getName(), "Cannot update: " + e.getMessage());
			throw new KPIModelException("Could not update from SIB: " + e.getMessage(), e);
		}		
	}
	
	/**
	 * Updates a set of triples from the SIB
	 * @param newTripleVector a vector of triples to be inserted, each one containing the subject, predicate, object, subject type and object type in positions 1 to 5 of the vector
	 * @param oldTripleVector a vector of triples to be removed, each one containing the subject, predicate, object, subject type and object type in positions 1 to 5 of the vector
	 * @return a PendingTask that controls the reception of the message
	 * @throws KPISSAPException if some error occurs in creation of the output message
	 */
	public String update(Vector<Vector<String>> newTripleVector, Vector<Vector<String>> oldTripleVector) throws KPIModelException {
		try {
			// Creates the RDF-M3 object with the triple
			RDFM3 insertm3 = getRDFM3(newTripleVector);
			RDFM3 removem3 = getRDFM3(oldTripleVector);
			
			return super.update(insertm3.toString(), removem3.toString(), TypeAttribute.RDFM3);
		} catch (KPIModelException kpiex) {
			Logger.debug(this.getClass().getName(), "Cannot update in the SIB: " + kpiex.getMessage());
			throw new KPIModelException("Could not update into SIB: " + kpiex.getMessage(), kpiex, kpiex.getStatus());			
		} catch (Exception e) {
			Logger.debug(this.getClass().getName(), "Cannot update: " + e.getMessage());
			throw new KPIModelException("Could not update from SIB: " + e.getMessage(), e);
		}	
	}

	/**
	 * Queries about a triple in the SIB
	 * @param subject the subject
	 * @param predicate the predicate
	 * @param object the object
	 * @param subjectType the subject type
	 * @param objectType the object type
	 * @return a PendingTask that controls the reception of the message
	 * @throws KPISSAPException if some error occurs in creation of the output message
	 */
	public String query(String subject, String predicate, String object, String subjectType, String objectType) throws KPIModelException {
		try {
			// Creates the RDF-M3 object with the triple
			RDFM3 m3 = getRDFM3(subject, predicate, object, subjectType, objectType);
			
			return super.query(m3.toString(), TypeAttribute.RDFM3);			
		} catch (KPIModelException kpiex) {
			Logger.debug(this.getClass().getName(), "Cannot query in the SIB: " + kpiex.getMessage());
			throw new KPIModelException("Could not query into SIB: " + kpiex.getMessage(), kpiex, kpiex.getStatus());			
		} catch (Exception e) {
			Logger.debug(this.getClass().getName(), "Cannot query: " + e.getMessage());
			throw new KPIModelException("Could not query into SIB: " + e.getMessage(), e);
		}
	}	
	
	/**
	 * Subscribes to a triple in the SIB
	 * @param subject the subject
	 * @param predicate the predicate
	 * @param object the object
	 * @param subjectType the subject type
	 * @param objectType the object type
	 * @return a PendingTask that controls the reception of the message
	 * @throws KPISSAPException if some error occurs in creation of the output message
	 */
	public HashMap<NameAttribute,String> subscribe(String subject, String predicate, String object, String subjectType, String objectType) throws KPIModelException {
		try {
			// Creates the RDF-M3 object with the triple
			RDFM3 m3 = getRDFM3(subject, predicate, object, subjectType, objectType);
			
			return super.subscribe(m3.toString(), TypeAttribute.RDFM3);
		} catch (KPIModelException kpiex) {
			Logger.debug(this.getClass().getName(), "Cannot query in the SIB: " + kpiex.getMessage());
			throw new KPIModelException("Could not query into SIB: " + kpiex.getMessage(), kpiex, kpiex.getStatus());			
		} catch (Exception e) {
			Logger.debug(this.getClass().getName(), "Cannot query: " + e.getMessage());
			throw new KPIModelException("Could not query into SIB: " + e.getMessage(), e);
		}
	}	

	/**
	 * Gets the RDF-M3 representation of the data
	 * @param subject a String with the subject
	 * @param predicate a String with the predicate
	 * @param object a String with the object
	 * @param subjectType the subject type (URI, bNode)
	 * @param objectType the object type (URI, literal)
	 * @return the RDF-M3 representation
	 * @throws KPIModelException when cannot make the transformation
	 */
	private RDFM3 getRDFM3(String subject, String predicate, String object, String subjectType, String objectType) throws KPIModelException {
		try {
			RDFM3 m3 = new RDFM3();
			RDFM3Triple.TypeAttribute subjectTypeAttribute = RDFM3Triple.TypeAttribute.findType(subjectType); // checking of value
			RDFM3Triple.TypeAttribute objectTypeAttribute = RDFM3Triple.TypeAttribute.findType(objectType); // checking of value
			RDFM3Triple triple = new RDFM3Triple(subject, subjectTypeAttribute, predicate, object, objectTypeAttribute);
			m3.addTriple(triple);
			
			return m3;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new KPIModelException("Cannot create RDF-M3 from given triple", ex);
		}
	}
	
	/**
	 * Gets the RDF-M3 representation of the data
	 * @param tripleVector a vector of triples, each one containing the subject, predicate, object, subject type and object type in positions 1 to 5 of the vector
	 * @return the RDF-M3 representation
	 * @throws KPIModelException when cannot make the transformation
	 */	
	private RDFM3 getRDFM3(Vector<Vector<String>> tripleVector) throws KPIModelException {
		try {
			RDFM3 m3 = new RDFM3();
			for(int i=0; i< tripleVector.size(); i++) { // following the same algorithm than in KPICore
				String s=tripleVector.elementAt(i).elementAt(0);
				String p=tripleVector.elementAt(i).elementAt(1);
				String o=tripleVector.elementAt(i).elementAt(2);
				String st=s==null?"URI":tripleVector.elementAt(i).elementAt(3);
				String ot=o==null?"URI":tripleVector.elementAt(i).elementAt(4);
	         
				RDFM3Triple.TypeAttribute subjectTypeAttribute = RDFM3Triple.TypeAttribute.findType(st);
				RDFM3Triple.TypeAttribute objectTypeAttribute = RDFM3Triple.TypeAttribute.findType(ot);
				RDFM3Triple triple = new RDFM3Triple(s, subjectTypeAttribute, p, o, objectTypeAttribute);
				m3.addTriple(triple);
	        }
			
			return m3;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new KPIModelException("Cannot create RDF-M3 from given triple vector", ex);
		}
	}

	@Override
	public void connectedToSIB() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disconnectedFromSIB() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void connectionToSIBTimeout() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void connectionToSIBError(String error) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void kpInitialized() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void requestProcessed() {
		// TODO Auto-generated method stub
		
	}
}
