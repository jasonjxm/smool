/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.model.direct;

import org.smool.kpi.common.Logger;
import org.smool.kpi.model.exception.KPIModelException;
import org.smool.kpi.ssap.SIB;
import org.smool.kpi.ssap.message.parameter.SSAPMessageRDFParameter.TypeAttribute;


/**
 * This class represents the Model that allows the 
 * sending of information in RDF-M3 format.
 * 
 * @author Raul Otaolea, Raul.Otaolea@tecnalia.com, Tecnalia-Software
 * @author Fran Ruiz, Fran.Ruiz@tecnalia.com, Tecnalia-Software
 */
public class OWLModel extends AbstractDirectModel {

	public OWLModel(SIB sib) {
		super(sib);
	}
	
	/**
	 * Inserts data into the SIB
	 * @param data data in Turtle/N3 format
	 * @return a String with the obtained blank nodes
	 * @throws KPIModelException if some error occurs in creation of the output message
	 */
	public String insert(String data) throws KPIModelException {
		try {
			return super.insert(data, TypeAttribute.RDFXML);
		} catch (KPIModelException kpiex) {
			Logger.debug(this.getClass().getName(), "Cannot insert in the SIB: " + kpiex.getMessage());
			throw new KPIModelException("Could not insert into SIB: " + kpiex.getMessage(), kpiex, kpiex.getStatus());			
		} catch (Exception e) {
			Logger.debug(this.getClass().getName(), "Cannot insert: " + e.getMessage());
			throw new KPIModelException("Could not insert into SIB: " + e.getMessage(), e);
		}
	}
	
	/**
	 * Inserts data into the SIB
	 * @param data data in Turtle/N3 format
	 * @return a String with the obtained blank nodes
	 * @throws KPIModelException if some error occurs in creation of the output message
	 */
	public String remove(String data) throws KPIModelException {
		try {
			return super.remove(data, TypeAttribute.RDFXML);			
		} catch (KPIModelException kpiex) {
			Logger.debug(this.getClass().getName(), "Cannot remove in the SIB: " + kpiex.getMessage());
			throw new KPIModelException("Could not remove into SIB: " + kpiex.getMessage(), kpiex, kpiex.getStatus());			
		} catch (Exception e) {
			Logger.debug(this.getClass().getName(), "Cannot remove: " + e.getMessage());
			throw new KPIModelException("Could not remove from SIB: " + e.getMessage(), e);
	}

	}
	
	/**
	 * Updates data into the SIB
	 * @param newData data to be inserted in Turtle/N3 format
	 * @param oldData data to be removed in Turtle/N3 format
	 * @return a String with the obtained blank nodes
	 * @throws KPIModelException if some error occurs in creation of the output message
	 */
	public String update(String newData, String oldData) throws KPIModelException {
		try {
			return super.update(newData, oldData, TypeAttribute.RDFXML);			
		} catch (KPIModelException kpiex) {
			Logger.debug(this.getClass().getName(), "Cannot update in the SIB: " + kpiex.getMessage());
			throw new KPIModelException("Could not update into SIB: " + kpiex.getMessage(), kpiex, kpiex.getStatus());			
		} catch (Exception e) {
			Logger.debug(this.getClass().getName(), "Cannot update: " + e.getMessage());
			throw new KPIModelException("Could not update from SIB: " + e.getMessage(), e);
		}
	}

	@Override
	public void connectedToSIB() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disconnectedFromSIB() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void connectionToSIBTimeout() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void connectionToSIBError(String error) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void kpInitialized() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void requestProcessed() {
		// TODO Auto-generated method stub
		
	}	
}
