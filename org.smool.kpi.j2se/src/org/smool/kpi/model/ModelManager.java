/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/

package org.smool.kpi.model;

import org.smool.kpi.common.ClasspathManager;
import org.smool.kpi.common.Logger;
import org.smool.kpi.connector.AbstractConnector;
import org.smool.kpi.connector.SIBDescriptor;
import org.smool.kpi.model.exception.KPIModelException;
import org.smool.kpi.model.smart.SmartModel;
import org.smool.kpi.ssap.ISIBDiscoveryListener;
import org.smool.kpi.ssap.SIB;
import org.smool.kpi.ssap.SIBDiscoveryAgent;
import org.smool.kpi.ssap.SIBDiscoverySupport;
import org.smool.kpi.ssap.exception.KPISSAPException;

public class ModelManager implements ISIBDiscoveryListener {

	/**
	 * Enumerated models
	 */
	public enum Model {
	SMARTMODEL("SmartModel", "org.smool.kpi.model.smart.SmartModel"),
	RDFM3MODEL("RDFM3Model", "org.smool.kpi.model.direct.RDFM3Model"),
	TURTLEMODEL("TurtleModel", "org.smool.kpi.model.direct.N3Model"),
	N3MODEL("N3Model", "org.smool.kpi.model.direct.N3Model");

		private final String name;
		private final String className;

		Model(String name, String className) {
			this.name = name;
			this.className = className;
		}

		public String getName() {
			return name;
		}

		public String getClassName() {
			return className;
		}

		public static Model findValue(String v) {
			for (Model model : values()) {
				if (model.getName().equals(v)) {
					return model;
				}
			}
			return null;
		}
	}

	/** The static instance of the Model Manager */
	private static ModelManager _instance;

	private SIBDiscoveryAgent agent;
	private SIBDiscoverySupport discoveryListeners;

	/**
	 * Constructor
	 */
	private ModelManager() {
		agent = SIBDiscoveryAgent.getInstance();
		agent.init();
		agent.addSIBDiscoveryListener(this);
		discoveryListeners = new SIBDiscoverySupport();

	}

	public static ModelManager newInstance() {
		return new ModelManager();
	}

	/**
	 * Creates an instance of the ModelManager
	 * 
	 * @return a new instance of the ModelManager
	 */
	public static ModelManager getInstance() {

		if (_instance == null) {
			synchronized (ModelManager.class) {
				if (_instance == null) {
					_instance = new ModelManager();
				}
			}
		}
		return _instance;
	}

	/**
	 * Adds a SIB Discoverer Listener to the current model manager
	 * 
	 * @param l
	 */
	public void addSIBDiscoveryListener(ISIBDiscoveryListener l) {
		discoveryListeners.addListener(l);
	}

	public void removeSIBDiscoveryListener(ISIBDiscoveryListener l) {
		discoveryListeners.removeListener(l);
	}

	/**
	 * Lookup for SIBs, sending a broadcasting signal to the available networks
	 * 
	 * @param asynch a boolean value indicating whether the search to be
	 *               asynchronous
	 * @throws KPIModelException
	 */
	public void lookForSIB(boolean asynch) throws KPIModelException {
		try {
			agent.inquiry(asynch);
		} catch (KPISSAPException ex) {
			ex.printStackTrace();
			throw new KPIModelException(ex.getMessage(), ex);
		}
	}

	/**
	 * Lookup for SIBs asynchronously, sending a broadcasting signal to the
	 * available networks
	 * 
	 * @throws KPIModelException
	 */
	public void lookForSIB() throws KPIModelException {
		try {
			lookForSIB(true);
		} catch (KPIModelException ex) {
			ex.printStackTrace();
			throw new KPIModelException(ex.getMessage(), ex);
		}
	}

	/**
	 * Sends an stop signal for stop looking up for SIBs
	 * 
	 * @throws KPIModelException
	 */
	public void stopLookForSIB() throws KPIModelException {
		try {
			agent.stopInquiry();
		} catch (KPISSAPException ex) {
			ex.printStackTrace();
			throw new KPIModelException(ex.getMessage(), ex);
		}
	}

	@Override
	public void SIBDiscovered(SIBDescriptor sibDescriptor) {
		discoveryListeners.fireSIBDiscovered(sibDescriptor);
	}

	@Override
	public void SIBConnectorDiscoveryFinished(String connectorName) {
		discoveryListeners.fireConnectorSIBDiscoveryFinished(connectorName);
	}

	@Override
	public void SIBDiscoveryFinished() {
		discoveryListeners.fireSIBDiscoveryFinished();
	}

	public AbstractModel createModel(Model model, String kpName, SIBDescriptor sibd) throws KPIModelException {
		try {
			SIB sib = new SIB(sibd.getSIBName(), kpName, sibd.getProperties());

			AbstractConnector connector = agent.createConnector(sibd.getConnectorName(), sibd.getProperties());
			sib.setConnector(connector);

			AbstractModel abstractModel = (AbstractModel) Class
					.forName(model.getClassName(), true, ClasspathManager.getInstance().getClassLoader()).newInstance();

			if (abstractModel != null) {
				abstractModel.setSIB(sib);
				return abstractModel;
			} else {
				throw new KPIModelException("Cannot find the specified model implementation (" + model.getName() + ")");
			}
		} catch (ClassNotFoundException ex) {

			throw new KPIModelException("", ex);
		} catch (InstantiationException ex) {

			throw new KPIModelException("", ex);
		} catch (IllegalAccessException ex) {

			throw new KPIModelException("", ex);
		} catch (KPIModelException ex) {

			throw new KPIModelException("", ex);
		}
	}

	/**
	 * Creates a new void Smart Model to contain new operations with the current SIB
	 * connection
	 * 
	 * @param kpName a valid KP descriptive name
	 * @param sibd   a SIB descriptor
	 * @return a new void SmartModel
	 * @throws KPIModelException
	 */
	public SmartModel createModel(String kpName, SIBDescriptor sibd) throws KPIModelException {
		try {

			SmartModel model = (SmartModel) this.createModel(Model.SMARTMODEL, kpName, sibd);
			// check Smartmodel is not null
			if (model != null) {
				return model;
			} else {
				Logger.error("Could not instantiate a SmartModel object");
				throw new KPIModelException("Could not instantiate a SmartModel object");
			}

		} catch (KPIModelException ex) {
			throw new KPIModelException("", ex);
		} catch (Exception ex) {
			throw new KPIModelException("", ex);
		}
	}

	/**
	 * Gets the list of connector names
	 * 
	 * @return an array of SIB connector names
	 */
	public String[] getConnectorNames() {
		return agent.getConnectorNames();
	}
}
