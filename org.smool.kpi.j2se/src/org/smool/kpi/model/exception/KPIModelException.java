/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.model.exception;

import org.smool.kpi.ssap.message.parameter.SSAPMessageStatusParameter.SIBStatus;

/**
 * The ModelException is related with exceptions arised
 * in model
 */
@SuppressWarnings("serial")
public class KPIModelException extends Exception {
	
	private SIBStatus status;
	
	public KPIModelException(SIBStatus status) {
		this.setStatus(status);
	}
	
	public KPIModelException(String message) {
		super(message);
	}

	public KPIModelException(String message, SIBStatus status) {
		super(message);
		this.setStatus(status);		
	}	
	
	public KPIModelException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public KPIModelException(String message, Throwable cause, SIBStatus status) {
		super(message, cause);
		this.setStatus(status);
	}

	/**
	 * @param status the status to set
	 */
	private void setStatus(SIBStatus status) {
		this.status = status;
	}

	/**
	 * @return the status
	 */
	public SIBStatus getStatus() {
		if (status != null) {
			return status;
		}
		return null;
	}
	
}
