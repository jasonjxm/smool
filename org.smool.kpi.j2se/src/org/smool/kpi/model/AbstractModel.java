/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.model;

import org.smool.kpi.common.Logger;
import org.smool.kpi.model.exception.KPIModelException;
import org.smool.kpi.ssap.ISIBConnectionListener;
import org.smool.kpi.ssap.PendingTask;
import org.smool.kpi.ssap.SIB;
import org.smool.kpi.ssap.exception.KPISSAPException;
import org.smool.kpi.ssap.message.parameter.SSAPMessageStatusParameter.SIBStatus;


/**
 * This abstract class provides the API for access to the common
 * operations with the SIB.
 * 
 * @author Raul Otaolea, Raul.Otaolea@esi.es, ESI
 * @author Fran Ruiz, Fran.Ruiz@esi.es, ESI
 */
public abstract class AbstractModel implements ISIBConnectionListener {
	
	/** The Semantic Information Broker reference */
	protected SIB sib;
	
	/** Timeout time to finish the publish action */
	private long timeout;	
	
	/** The model support that receives the calls from the SSAP layer */
	protected ModelSupport modelSupport;	
	
	public AbstractModel() {
		this.modelSupport = new ModelSupport();		
	}
	
	public void setSIB(SIB sib) {
		if(sib == null) {
			throw new NullPointerException("The SIB cannot be null");
		}
		this.sib = sib;		
	}
	
	public void addModelListener(IModelListener listener) {
		modelSupport.addListener(listener);
	}

	public void removeModelListener(IModelListener listener) {
		modelSupport.removeListener(listener);
	}

	/**
	 * Allows the connection to the SIB
	 */
	public void connect() throws KPIModelException {
		connect(null,null);
	}	

	/**
	 * Connection with credentials
	 * @param username the username to connect to the SIB
	 * @param password the password to connect to the SIB
	 * @throws KPIModelException when cannot connect to the SIB
	 */
	public void connect(String username, String password) throws KPIModelException {
		Logger.debug(this.getClass().getName(), "AbstractModel is connecting to SIB...");
		try {
			// Joins to the SIB
			PendingTask pt = getSIB().join(username,password);
			//TODO ARF 20-05-15 solve timing problem and remove sleep
			try{Thread.sleep(500);}catch(Exception e){;}//20-05-15 ARF Added a sleep to prevent. This is a TEMPORARY patch to solve issue #45. Then a TODO tag is added until properly solved 
			getSIB().publish();
			pt.waitUntilFinished(this.getTimeout());
			if(!pt.isFinished()) {
				throw new KPIModelException("Could not JOIN to SIB due to timeout");
			} 
			
			// Gets the status of the response
			SIBStatus status = (SIBStatus)pt.getProperty("status");

			if (!status.equals(SIBStatus.STATUS_OK)) {
				throw new KPIModelException("The connection cannot be executed due to " + status.getValue(), status);				
			}
			
			modelSupport.fireConnected();			
		} catch (KPISSAPException ssapex) {
			Logger.error(this.getClass().getName(), "Cannot connect to SIB: " + ssapex.getCause());
			ssapex.printStackTrace();
			throw new KPIModelException("Unable to connect to SIB", ssapex);
		} catch (KPIModelException modelex) {
			Logger.error(this.getClass().getName(), "Cannot connect to SIB: " + modelex.getCause());
			modelex.printStackTrace();
			throw new KPIModelException("Unable to connect to SIB", modelex.getCause(), modelex.getStatus());
		} catch (Exception e) {
			Logger.error(this.getClass().getName(), "Cannot connect: " + e.getCause());
			throw new KPIModelException("Could not connect to SIB", e);
		}
		
		Logger.debug(this.getClass().getName(), "Abstract Model connected to SIB");
	}	
	/**
	 * Allows the disconnection from the SIB
	 */
	public void disconnect() throws KPIModelException {
		Logger.debug(this.getClass().getName(), "Disconnecting from SIB...");
		
		try {
			PendingTask pt = getSIB().leave();
			this.getSIB().publish();
			pt.waitUntilFinished(this.getTimeout());
			if(!pt.isFinished()) {
				throw new KPIModelException("Could not LEAVE from SIB due to timeout");
			}
			
			// Gets the status of the response
			SIBStatus status = (SIBStatus)pt.getProperty("status");

			if (!status.equals(SIBStatus.STATUS_OK)) {
				throw new KPIModelException("The disconnection cannot be executed due to " + status.getValue(), status);				
			}
			
		} catch (KPISSAPException ssapex) {
			Logger.error(this.getClass().getName(), "Cannot disconnect from SIB: " + ssapex.getCause());
			ssapex.printStackTrace();
			throw new KPIModelException("Unable to disconnect from SIB", ssapex);
		} catch (KPIModelException kpiex) {
			Logger.debug(this.getClass().getName(), "Cannot disconnect from the SIB: " + kpiex.getMessage());
			throw new KPIModelException("Could not disconnect from SIB: " + kpiex.getMessage(), kpiex, kpiex.getStatus());			
		} catch (Exception e) {
			Logger.debug(this.getClass().getName(), "Cannot disconnect: " + e.getMessage());
			throw new KPIModelException("Could not disconnect from SIB: " + e.getMessage(), e);
		}
		
		Logger.debug(this.getClass().getName(), "Disconnected from SIB");
	}
	
	/**
	 * Gets the SIB
	 * @return the SIB reference
	 */
	public SIB getSIB() {
		return sib;
	}
	
	
	/**
	 * Return the timeout of the publish operation
	 * @return timeout in milliseconds
	 */
	public long getTimeout() {
		return timeout;
	}

	/**
	 * Set the timeout of the publish operation. 0 waits for ever.
	 */
	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}
	
	/**
	 * Resets the data
	 */
	protected void reset() {
	}
	
	@Override
	public void connectedToSIB() {
		Logger.debug(this.getClass().getName(), "Connected to SIB");
		modelSupport.fireConnected();
	}

	@Override
	public void connectionToSIBTimeout() {
		Logger.debug(this.getClass().getName(), "SIB timeout");
		reset();
		modelSupport.fireConnectionTimeout();
	}

	@Override
	public void disconnectedFromSIB() {
		Logger.debug(this.getClass().getName(), "Disconnected from SIB");
		reset();
		modelSupport.fireDisconnected();
	}

	@Override
	public void connectionToSIBError(String error) {
		Logger.debug(this.getClass().getName(), "SIB connection error: " + error);
		reset();
		modelSupport.fireConnectionError(error);
	}
	
	@Override
	public void requestProcessed() {
		Logger.debug(this.getClass().getName(), "Published in SIB");
		modelSupport.firePublished();
	}

	@Override
	public void kpInitialized() {
		Logger.debug(this.getClass().getName(), "KP Initialized in SIB");
		modelSupport.fireInitialized();
	}	
}
