/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.model;

import org.smool.kpi.common.AbstractSupport;

public class ModelSupport extends AbstractSupport<IModelListener> {
	
	public ModelSupport() {
		super();
	}
  
	public void fireConnected() {
		synchronized(listeners) {
			for(IModelListener listener : listeners) {
				listener.connected();
			}
		}
	}
	
	public void fireDisconnected() {
		synchronized(listeners) {
			for(IModelListener listener : listeners) {
				listener.disconnected();
			}
		}
	}
	
	public void fireConnectionTimeout() {
		synchronized(listeners) {
			for(IModelListener listener : listeners) {
				listener.connectionError("TIMEOUT");
			}
		}
	}

	public void fireConnectionError(String error) {
		synchronized(listeners) {
			for(IModelListener listener : listeners) {
				listener.connectionError(error);
			}
		}
	}
	
	/**
	 * Sends a notification to the SMOOLModel that the model was successfully inserted
	 */
	public void fireInitialized() {
		synchronized(listeners) {
			for(IModelListener listener : listeners) {
				listener.initialized();
			}
		}
	}
	
	/**
	 * Sends a notification to the SMOOLModel that the model was successfully inserted
	 */
	public void firePublished() {
		synchronized(listeners) {
			for(IModelListener listener : listeners) {
				listener.published();
			}
		}
	}	
}