/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/

package org.smool.kpi.model.smart.subscription;

import org.smool.kpi.model.smart.IAbstractOntConcept;
import org.smool.kpi.ssap.message.parameter.SSAPMessageRDFParameter.TypeAttribute;



public interface ISubscription<C extends IAbstractOntConcept>  {

	public void conceptAdded(C aoc);

	public void conceptRemoved(C aoc);

	public void conceptUpdated(C newConcept, C obsoleteConcept);

	public TypeAttribute getEncodingType();
}
