/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia) - initial API, implementation and documentation
 *******************************************************************************/

package org.smool.kpi.model.smart.encoding;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.smool.kpi.common.Logger;
import org.smool.kpi.model.exception.KPIModelException;
import org.smool.kpi.model.smart.AbstractOntConcept;
import org.smool.kpi.model.smart.IAbstractOntConcept;
import org.smool.kpi.model.smart.SmartModel;
import org.smool.kpi.model.smart.subscription.ISubscription;
import org.smool.kpi.ssap.PendingTask;
import org.smool.kpi.ssap.exception.KPISSAPException;
import org.smool.kpi.ssap.message.parameter.SSAPMessageStatusParameter.SIBStatus;
import org.smool.kpi.ssap.subscription.ISubscriptionListener;


/**
 * This abstract class represent the template for the operations related with
 * queries and subscriptions over AbstractOntConcepts. The classes that extends
 * this AbstractClass should implement the methods coming from ISubscriptionListener
 * (indicationReceived) for subscription indications and query, subscribe and unsubscribe
 * methods.
 *
 * @author Fran Ruiz, fran.ruiz@tecnalia.com, Tecnalia
 * @author Raul Otaolea, raul.otaolea@tecnalia.com, Tecnalia
 *
 */
public abstract class AbstractQueryProcessor implements ISubscriptionListener {

	/**  The set of listener receiving subscription notifications */
	protected  HashMap<Long, ISubscription<AbstractOntConcept>> subscriptions;

	/** The subscriptions associated to this model */
	protected HashMap<Long, Class<? extends AbstractOntConcept>> subscribedConcepts;

	/** Some standard associated constants referred to RDF and OWL language */
	protected static final String RDF_TYPE = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";
	protected static final String DATATYPE_PROPERTY = "http://www.w3.org/2002/07/owl#DatatypeProperty";
	protected static final String OBJECT_PROPERTY = "http://www.w3.org/2002/07/owl#ObjectProperty";
	protected static final String FUNCTIONAL_PROPERTY = "http://www.w3.org/2002/07/owl#FunctionalProperty";

	/** The queue of indications received from the SIB */
	protected static ExecutorService indicationQueue;

	/** The pending pending indication requests to be processed */
	protected static int pendingRequests;

	/** The object that locks the access to the pending requests */
	protected Object lock;

	/** The SmartModel associated to the current Query Processor */
	protected SmartModel model;

	/**
	 * Constructor of AbstractQueryProcessor that initializes
	 * the queues and maps used for subscriptions and indications.
	 *
	 * @param model The SmartModel associated to the current query processor
	 */
	public AbstractQueryProcessor(SmartModel model) {
		this.model = model;
		indicationQueue = Executors.newSingleThreadExecutor();
		subscribedConcepts = new HashMap<Long, Class<? extends AbstractOntConcept>>();
		subscriptions = new HashMap<Long, ISubscription<AbstractOntConcept>>();
		lock = new Object();
		pendingRequests = 0;
	}

	/**
	 * Queries about a given class.
	 * @param concept the concept class that we want to query about
	 * @return a list of AbstractOntConcepts belonging to the input concept class available in the SIB
	 * @throws KPIModelException when some error arise during querying
	 */
	public abstract <C extends IAbstractOntConcept> List<C> query(Class<C> concept) throws KPIModelException;

	/**
	 * Queries about a given instance.
	 * @param concept the concept class that we want to query about
	 * @param id the concept identifier
	 * @return a AbstractOntConcepts corresponding to the individual we asked for. <code>null</code> if not available
	 * @throws KPIModelException when some error arise during querying
	 */
	public abstract <C extends IAbstractOntConcept> C query(Class<C> concept, String id) throws KPIModelException;


	/**
	 * Subscribe to a concept class. Results are notified in the AbstractSubscription class
	 * @param concept the concept class to subscribe
	 * @param as the abstract subscription in what the responses are notified
	 * @throws KPIModelException when some error arise during subscription
	 */
	public abstract <C extends IAbstractOntConcept> void subscribe(Class<C> concept, ISubscription<? super C> as) throws KPIModelException;

	/**
	 * Queries about a given instance.
	 * @param concept the concept class that we want to query about
	 * @param id the concept identifier
	 * @return a AbstractOntConcepts corresponding to the individual we asked for. <code>null</code> if not available
	 * @throws KPIModelException when some error arise during querying
	 */
	public abstract <C extends IAbstractOntConcept> void subscribe(Class<C> concept, String id, ISubscription<? super C> as) throws KPIModelException;

	/**
	 * Unsubscribes from the concepts associated to a given AbstractSubscription
	 * @param as the AbstractSubscription to what unsubscribe
	 * @return <code>true</code> if unsubscription was correctly made
	 * @throws KPIModelException
	 */
	public <C extends IAbstractOntConcept> boolean unsubscribe(ISubscription<C> as) throws KPIModelException {
		try {
			long subscription = -1;
			// Obtain the subscription corresponding to the abstract subscription
			if (subscriptions.containsValue(as)) {
				for (long subscriptionId : subscriptions.keySet()) {
					if (subscriptions.get(subscriptionId).equals(as)) {
						subscription = subscriptionId;
					}
				}
			}

			Logger.debug(this.getClass().getName(), "Unsubscribe the subscription id: " + subscription);

			PendingTask pt = model.getSIB().unsubscribe(subscription);
			model.getSIB().publish();
			pt.waitUntilFinished(model.getTimeout());
			if(pt.isFinished()) {
				try {
					SIBStatus status = (SIBStatus)pt.getProperty("status");
					if(status == SIBStatus.STATUS_OK) {
						Long receivedSubscriptionId = (Long)pt.getProperty("subscriptionId");
						// If received subscription is not the same, then return false
						if ((receivedSubscriptionId != null) && (subscription == receivedSubscriptionId)) {
							model.getSIB().removeSubscriptionsListener(receivedSubscriptionId);
							subscriptions.remove(receivedSubscriptionId);
							subscribedConcepts.remove(receivedSubscriptionId);
							return true;
						} else {
							return false;
						}
					} else {
						throw new KPIModelException("The unsubscribe has executed with errors.");
					}
				} catch(Exception e) {
					throw new KPIModelException("An error has ocurred while unsubscribing", e);
				}
			} else {
				throw new KPIModelException("The query finished by timeout");
			}

		} catch (Exception e) {
			throw new KPIModelException(e.getMessage(), e.getCause());
		}
	}

	/**
	 * The obtained results
	 * @param results the SPARQL-results in XML-format
	 */
	public abstract void indicationReceived(long subscriptionId, String newResults, String obsoleteResults)  throws KPISSAPException;


	public void reset() {

	}
}
