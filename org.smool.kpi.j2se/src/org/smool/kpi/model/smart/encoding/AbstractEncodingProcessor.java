/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.model.smart.encoding;

import java.util.Collection;

import org.smool.kpi.model.exception.KPIModelException;
import org.smool.kpi.model.smart.AbstractOntConcept;


/**
 * This class is an abstract class that determine the set of methods 
 * needed to use different encodings using the model layer in the ADK.
 * 
 * @author Raul Otaolea, raul.otaolea@tecnalia.com, Tecnalia-Software
 * @author Fran Ruiz, fran.ruiz@tecnalia.com, Tecnalia-Software
 */
public abstract class AbstractEncodingProcessor {

	/**
	 * Constructor of the Abstract Encoding Processor
	 */
	public AbstractEncodingProcessor() {
	}
	
	/**
	 * Gets the string representation for embed content
	 */
	public abstract String getAddingContent(Collection<? extends AbstractOntConcept> newConcepts,
			Collection<? extends AbstractOntConcept> modelConcepts, 
			Collection<? extends AbstractOntConcept> unpublishedConcepts) throws KPIModelException;

	/**
	 * Gets the string representation of data to be send
	 * embedded in the SSAP message and latter update in
	 * the AbstractOntConcept
	 */
	public abstract String getRemovingContent(Collection<? extends AbstractOntConcept> obsoleteConcepts,
			Collection<? extends AbstractOntConcept> referencingConcepts,
			Collection<? extends AbstractOntConcept> modelConcepts)
			throws KPIModelException;
	
	/**
	 * Gets the string representation of data to be send
	 * embedded in the SSAP message and latter update in
	 * the AbstractOntConcept
	 */
	public abstract String getUpdateInsertGraph(Collection<? extends AbstractOntConcept> modelConcepts, Collection<? extends AbstractOntConcept> unpublishedConcepts) 
		throws KPIModelException;
	
	/**
	 * Gets the string representation of data to be send
	 * embedded in the SSAP message and latter update in
	 * the AbstractOntConcept
	 */
	public abstract String getUpdateRemoveGraph(Collection<? extends AbstractOntConcept> modelConcepts, Collection<? extends AbstractOntConcept> publishedConcepts) 
		throws KPIModelException;		
}
