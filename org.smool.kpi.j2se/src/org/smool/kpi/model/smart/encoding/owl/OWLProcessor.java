/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.model.smart.encoding.owl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.smool.kpi.model.exception.KPIModelException;
import org.smool.kpi.model.smart.AbstractOntConcept;
import org.smool.kpi.model.smart.encoding.AbstractEncodingProcessor;
import org.smool.kpi.model.smart.slots.FunctionalDatatypeSlot;
import org.smool.kpi.model.smart.slots.FunctionalObjectSlot;
import org.smool.kpi.model.smart.slots.FunctionalSlotChangeCommand;
import org.smool.kpi.model.smart.slots.NonFunctionalDatatypeSlot;
import org.smool.kpi.model.smart.slots.NonFunctionalObjectSlot;
import org.smool.kpi.model.smart.slots.NonFunctionalSlotAddCommand;
import org.smool.kpi.model.smart.slots.NonFunctionalSlotRemoveCommand;
import org.smool.kpi.model.smart.slots.Slot;
import org.smool.kpi.model.smart.util.NamespaceManager;


/**
 * This class is an class that allows the formatting for OWL encoding 
 * needed to use in the model layer in the ADK.
 * 
 * @author Raul Otaolea, raul.otaolea@tecnalia.com, Tecnalia-Software
 * @author Fran Ruiz, fran.ruiz@tecnalia.com, Tecnalia-Software
 */
public class OWLProcessor extends AbstractEncodingProcessor {

	private final String CLOSING_TAG = ">";
	private final String QUOTE = "\""; // OR "'"
	private final String SEPARATOR = " \n";
	
	private NamespaceManager nsManager;

	
	public OWLProcessor() {
		nsManager = new NamespaceManager();
	}
	
	/**
	 * Gets the String representation in OWL format for a set of AbstractOntConcept
	 * @param newConcepts the set of recently created concepts
	 * @param modelConcepts the set of AbstractOntConcepts belonging to the SmartModel
	 * @return a String representation of the concepts
	 * @throws KPIModelException when some error occur
	 */
	@Override
	public String getAddingContent(Collection<? extends AbstractOntConcept> newConcepts,
			Collection<? extends AbstractOntConcept> modelConcepts,
			Collection<? extends AbstractOntConcept> unpublishedConcepts) throws KPIModelException {
		
		StringBuilder sb = new StringBuilder();
		try {
			
			// Adding the namespaces corresponding to adding concepts
			for(AbstractOntConcept concept : newConcepts) {
				nsManager.addNsPrefixMap(concept._getIndividualPrefixMap());
			}
			
			// Adding the namespaces corresponding to model concepts containing adding commands
			for(AbstractOntConcept concept : modelConcepts) {
				nsManager.addNsPrefixMap(concept._getIndividualPrefixMap());
			}
			
			// Adding the namespaces corresponding to unpublished concepts
			for(AbstractOntConcept concept : unpublishedConcepts) {
				nsManager.addNsPrefixMap(concept._getIndividualPrefixMap());
			}			
			
			// Gets the namespace declaration in OWL format
			sb.append(this.getNamespaceDeclaration(nsManager));

			// Adds the representation of concepts
			for(AbstractOntConcept concept : newConcepts) {
				sb.append(this.getDirectOWLRepresentation(concept));
			}
			
			// Adds the representation of adding commands
			for(AbstractOntConcept concept : modelConcepts) {
				sb.append(this.getOWLRepresentation4AddingCommands(concept));
			}	
			
			// Adds the representation of unpublished concepts
			for(AbstractOntConcept concept : unpublishedConcepts) {
				sb.append(this.getDirectOWLRepresentation(concept));
			}
			
			sb.append(this.getNamespaceDeclarationEndElement());
		} catch (Exception ex) {
			throw new KPIModelException("Cannot generate OWL for the commands and added concepts :" + ex.getMessage(), ex);
		}
		
		return sb.toString();
	}

	/**
	 * Gets the String representation in OWL format for a set of AbstractOntConcept
	 * @param obsoleteConcepts the set of recently created concepts
	 * @param referencingConcepts the set of concepts referencing to the obsolete concepts
	 * @param modelConcepts the set of AbstractOntConcepts belonging to the SmartModel
	 * @return a String representation of the concepts
	 * @throws KPIModelException when some error occur
	 */
	@Override
	public String getRemovingContent(Collection<? extends AbstractOntConcept> obsoleteConcepts,
			Collection<? extends AbstractOntConcept> referencingConcepts,
			Collection<? extends AbstractOntConcept> modelConcepts) throws KPIModelException {
		StringBuilder sb = new StringBuilder();
		try {
			
			// The manager that will contain the namespace prefix mapping
			// for the publishing RDF command
			NamespaceManager nsManager = new NamespaceManager();
	
			// Adding the namespaces corresponding to removing concepts
			for(AbstractOntConcept concept : obsoleteConcepts) {
				nsManager.addNsPrefixMap(concept._getIndividualPrefixMap());
			}

			// Adding the namespaces corresponding to referencing concepts
			for(AbstractOntConcept concept : referencingConcepts) {
				nsManager.addNsPrefixMap(concept._getIndividualPrefixMap());
			}
			
			// Adding the namespaces corresponding to model concepts containing removing commands
			for(AbstractOntConcept concept : modelConcepts) {
				nsManager.addNsPrefixMap(concept._getIndividualPrefixMap());
			}
			
			// Gets the namespace declaration in OWL format
			sb.append(this.getNamespaceDeclaration(nsManager));

			// Adds the representation of concepts
			for(AbstractOntConcept concept : obsoleteConcepts) {
				sb.append(this.getDirectOWLRepresentation(concept));
			}

			// Adds the representation of concepts referring to removed concepts
			for(AbstractOntConcept concept : referencingConcepts) {
				sb.append(this.getOWLRepresentation4ReferencingConcepts(concept, obsoleteConcepts));
			}
			
			// Adds the representation of removing commands
			for(AbstractOntConcept concept : modelConcepts) {
				sb.append(this.getIndirectOWLRepresentation(concept));
			}			

			sb.append(this.getNamespaceDeclarationEndElement());
		} catch (Exception ex) {
			throw new KPIModelException("Cannot generate OWL for the commands and removed concepts :" + ex.getMessage(), ex);
		}
		
		return sb.toString();
	}

	/**
	 * Gets the String representation in OWL format for a set of AbstractOntConcept to be updated
	 * @param modelConcepts the set of AbstractOntConcepts belonging to the SmartModel
	 * @return a String representation of the concepts
	 * @throws KPIModelException when some error occur
	 */
	@Override
	public String getUpdateInsertGraph(Collection<? extends AbstractOntConcept> modelConcepts, 
			Collection<? extends AbstractOntConcept> unpublishedConcepts) throws KPIModelException {
		StringBuilder sb = new StringBuilder();
		try {
			
			// The manager that will contain the namespace prefix mapping
			// for the publishing RDF command
			NamespaceManager nsManager = new NamespaceManager();
			
			// Adding the namespaces corresponding to model concepts containing removing commands
			for(AbstractOntConcept concept : modelConcepts) {
				nsManager.addNsPrefixMap(concept._getIndividualPrefixMap());
			}
			
			// Adding the namespaces corresponding to unpublished concepts
			for(AbstractOntConcept concept : unpublishedConcepts) {
				nsManager.addNsPrefixMap(concept._getIndividualPrefixMap());
			}			
			
			// Gets the namespace declaration in OWL format
			sb.append(this.getNamespaceDeclaration(nsManager));

			// Adds the representation of removing commands
			for(AbstractOntConcept concept : modelConcepts) {
				sb.append(this.getIndirectUpdateInsertGraphOWLRepresentation(concept));
			}
			
			// Adds the representation of unpublished concepts
			for(AbstractOntConcept concept : unpublishedConcepts) {
				sb.append(this.getDirectOWLRepresentation(concept));
			}			
			
			sb.append(this.getNamespaceDeclarationEndElement());
		} catch (Exception ex) {
			throw new KPIModelException("Cannot generate OWL for the commands insert graph concepts :" + ex.getMessage(), ex);
		}
		
		return sb.toString();
	}

	/**
	 * Gets the String representation in OWL format for a set of AbstractOntConcept to be updated
	 * @param modelConcepts the set of AbstractOntConcepts belonging to the SmartModel
	 * @return a String representation of the concepts
	 * @throws KPIModelException when some error occur
	 */
	@Override
	public String getUpdateRemoveGraph(Collection<? extends AbstractOntConcept> modelConcepts, 
			Collection<? extends AbstractOntConcept> publishedConcepts) throws KPIModelException {
		StringBuilder sb = new StringBuilder();
		try {
			
			// The manager that will contain the namespace prefix mapping
			// for the publishing RDF command
			NamespaceManager nsManager = new NamespaceManager();
			
			// Adding the namespaces corresponding to model concepts containing removing commands
			for(AbstractOntConcept concept : modelConcepts) {
				nsManager.addNsPrefixMap(concept._getIndividualPrefixMap());
			}
			
			// Adding the namespaces corresponding to unpublished concepts
			for(AbstractOntConcept concept : publishedConcepts) {
				nsManager.addNsPrefixMap(concept._getIndividualPrefixMap());
			}			
			
			// Gets the namespace declaration in OWL format
			sb.append(this.getNamespaceDeclaration(nsManager));

			// Adds the representation of removing commands
			for(AbstractOntConcept concept : modelConcepts) {
				sb.append(this.getIndirectUpdateRemoveGraphOWLRepresentation(concept));
			}
			
			// Adds the representation of unpublished concepts
			for(AbstractOntConcept concept : publishedConcepts) {
				if (!modelConcepts.contains(concept)) {
					sb.append(this.getDirectOWLRepresentation(concept));
				}
			}				
			
			sb.append(this.getNamespaceDeclarationEndElement());
		} catch (Exception ex) {
			throw new KPIModelException("Cannot generate OWL for the commands remove graph concepts :" + ex.getMessage(), ex);
		}
		
		return sb.toString();
	}
	
	/**
	 * Get the OWL representation of an AbstractOntConcept
	 * @param aoc the concept to be represented
	 * @return a String representation of an AbstractOntConcept in OWL format
	 */
	private String getDirectOWLRepresentation(AbstractOntConcept aoc) {
		StringBuilder sb = new StringBuilder();
		boolean found = false;
		
		ArrayList<AbstractOntConcept> referredConcepts = new ArrayList<AbstractOntConcept>(); // Added by ANM to correct the remove bug.
		
		// the individual declaration opening tag
		sb.append(this.getIndividualDeclarationOpeningTag(aoc));
		
		// for each of the properties, gets the owl representation of the property
		for (Slot<?> slot : aoc._listSlots()) {
			if (slot.hasContent()) { // if the slot has content
				sb.append(this.getOWLRepresentation(slot));
				found = true;
				if (slot.isObjectProperty()) {
					if (slot.isFunctionalProperty()) {
						Object o = ((FunctionalObjectSlot<?>)slot).getValue();
						if (o instanceof AbstractOntConcept) {
							referredConcepts.add((AbstractOntConcept) o);
						}
					}
					else {
						ArrayList<?> vals = ((NonFunctionalObjectSlot<?>)slot).getValues();
						for (Object o : vals) {
							if (o instanceof AbstractOntConcept) {
								referredConcepts.add((AbstractOntConcept) o);
							}
						}
					}
				}
			}
		}
		// the individual declaration closing tag
		sb.append(this.getIndividualDeclarationClosingTag(aoc));
		
		for (AbstractOntConcept refConcept : referredConcepts) {
			sb.append( getDirectOWLRepresentation(refConcept) );
		}
		
		if (!found) { // if no published properties, the RDF representation is null
			return this.getIndividualDeclarationTag(aoc);
		}
		
		return sb.toString();
	}
	
	
	/**
	 * Returns the RDF representing to this class containing all the 
	 * content still not published in the SIB (removal content only)
	 * @return a String containing the RDF representation of the individual
	 */
	protected String getIndirectOWLRepresentation(AbstractOntConcept aoc) {
		StringBuilder sb = new StringBuilder();
		
		boolean found = false;
		
		// the individual declaration opening tag
		sb.append(this.getIndividualDeclarationOpeningTag(aoc));
		
		for (Slot<?> slot : aoc._listSlots()) {
			if (slot.isFunctionalProperty()) {
				for (FunctionalSlotChangeCommand<?> command : aoc._listFunctionalChangeCommands()) {
					if (command.getSlot().equals(slot) && command.getValue() == null) {
						// gets the owl representation of the value to be removed
						sb.append(this.getOWLRepresentation(slot));
						found = true;
					}
				}
			} else {
				for (NonFunctionalSlotRemoveCommand<?> command : aoc._listNonFunctionalRemovingCommands()) {
					if (command.getSlot().equals(slot)) {
						if (slot.isObjectProperty()) { // object property
							sb.append("\t");
							sb.append(this.getObjectPropertyDeclarationOpeningTag(slot));
							if(command.getValue() instanceof AbstractOntConcept) {
								if (((AbstractOntConcept)command.getValue())._getIndividualNamespace() != nsManager.getDefaultNamespace()) {
									sb.append(((AbstractOntConcept)command.getValue())._getIndividualNamespace());
								} else {
									sb.append("#");
								}
								sb.append(((AbstractOntConcept)command.getValue())._getIndividualID());
							}
							sb.append(this.getObjectPropertyDeclarationClosingTag(slot));
						} else { // datatype property
							NonFunctionalDatatypeSlot<?> nfdSlot = (NonFunctionalDatatypeSlot<?>) slot;
							sb.append("\t");
							sb.append(this.getDatatypePropertyDeclarationOpeningTag(slot));
							sb.append(nfdSlot.toEncodedValue(command.getValue()));
							sb.append(this.getDatatypePropertyDeclarationClosingTag(slot));						
						}
						found = true;
					}
				}
			}
		}
		
		// the individual declaration closing tag
		sb.append(this.getIndividualDeclarationClosingTag(aoc));
		
		if (!found) { // if no unpublished properties, the RDF representation is null
			return null;
		}		
		
		return sb.toString();			
	}
	
	/**
	 * Returns the RDF representing to this class containing all the 
	 * content still not published in the SIB (removal content only)
	 * @return a String containing the RDF representation of the individual
	 */
	protected String getIndirectUpdateInsertGraphOWLRepresentation(AbstractOntConcept aoc) {
		StringBuilder sb = new StringBuilder();
		
		boolean found = false;
		
		// the individual declaration opening tag
		sb.append(this.getIndividualDeclarationOpeningTag(aoc));
		
		for (Slot<?> slot : aoc._listSlots()) {
			if (slot.isFunctionalProperty()) {
				for (FunctionalSlotChangeCommand<?> command : aoc._listFunctionalChangeCommands()) {
					if (command.getSlot().equals(slot) && 
							//command.getSlot().getValue() != null && // previous null value 
							command.getValue() != null) {
						if (slot.isObjectProperty()) { // object property
							sb.append("\t");
							sb.append(this.getObjectPropertyDeclarationOpeningTag(slot));
							if(command.getValue() instanceof AbstractOntConcept) {
								if (((AbstractOntConcept)command.getValue())._getIndividualNamespace() != nsManager.getDefaultNamespace()) {
									sb.append(((AbstractOntConcept)command.getValue())._getIndividualNamespace());
								} else {
									sb.append("#");
								}
								sb.append(((AbstractOntConcept)command.getValue())._getIndividualID());
								
							}
							sb.append(this.getObjectPropertyDeclarationClosingTag(slot));
						} else { // datatype property
							FunctionalDatatypeSlot<?> fdSlot = (FunctionalDatatypeSlot<?>) slot;
							sb.append("\t");
							sb.append(this.getDatatypePropertyDeclarationOpeningTag(slot));
							sb.append(fdSlot.toEncodedValue(command.getValue()));
							sb.append(this.getDatatypePropertyDeclarationClosingTag(slot));						
						}
						found = true;
					}
				}			
			} else { // non-functional update commands
				for (NonFunctionalSlotAddCommand<?> command : aoc._listNonFunctionalAddingCommands()) {
					if (command.getSlot().equals(slot)) {
						if (slot.isObjectProperty()) { // object property
							sb.append("\t");
							sb.append(this.getObjectPropertyDeclarationOpeningTag(slot));
							if(command.getValue() instanceof AbstractOntConcept) {
								if (((AbstractOntConcept)command.getValue())._getIndividualNamespace() != nsManager.getDefaultNamespace()) {
									sb.append(((AbstractOntConcept)command.getValue())._getIndividualNamespace());
								} else {
									sb.append("#");
								}
								sb.append(((AbstractOntConcept)command.getValue())._getIndividualID());
							}
							sb.append(this.getObjectPropertyDeclarationClosingTag(slot));
						} else { // datatype property
							NonFunctionalDatatypeSlot<?> nfdSlot = (NonFunctionalDatatypeSlot<?>) slot;
							sb.append("\t");
							sb.append(this.getDatatypePropertyDeclarationOpeningTag(slot));
							sb.append(nfdSlot.toEncodedValue(command.getValue()));
							sb.append(this.getDatatypePropertyDeclarationClosingTag(slot));						
						}
						found = true;
					}
				}		
			}
		}
		
		// the individual declaration closing tag
		sb.append(this.getIndividualDeclarationClosingTag(aoc));
		
		if (!found) { // if no unpublished properties, the RDF representation is null
			return null;
		}		
		
		return sb.toString();			
	}		

	/**
	 * Returns the RDF representing to this class containing all the 
	 * content still not published in the SIB (removal content only)
	 * @return a String containing the RDF representation of the individual
	 */
	protected String getIndirectUpdateRemoveGraphOWLRepresentation(AbstractOntConcept aoc) {
		StringBuilder sb = new StringBuilder();
		
		boolean found = false;
		
		// the individual declaration opening tag
		sb.append(this.getIndividualDeclarationOpeningTag(aoc));
		
		for (Slot<?> slot : aoc._listSlots()) {
			if (slot.isFunctionalProperty()) {
				for (FunctionalSlotChangeCommand<?> command : aoc._listFunctionalChangeCommands()) {
					if (command.getSlot().equals(slot) && 
							command.getSlot().getValue() != null ) {
						//&& command.getValue() != null) { // removed value as well
						// gets the owl representation of the value to be removed
						sb.append(this.getOWLRepresentation(slot));
						found = true;
					}
				}
			} else {
				for (NonFunctionalSlotRemoveCommand<?> command : aoc._listNonFunctionalRemovingCommands()) {
					if (command.getSlot().equals(slot)) {
						if (slot.isObjectProperty()) { // object property
							sb.append("\t");
							sb.append(this.getObjectPropertyDeclarationOpeningTag(slot));
							if(command.getValue() instanceof AbstractOntConcept) {
								if (((AbstractOntConcept)command.getValue())._getIndividualNamespace() != nsManager.getDefaultNamespace()) {
									sb.append(((AbstractOntConcept)command.getValue())._getIndividualNamespace());
								} else {
									sb.append("#");
								}
								sb.append(((AbstractOntConcept)command.getValue())._getIndividualID());
								
							}
							sb.append(this.getObjectPropertyDeclarationClosingTag(slot));
						} else { // datatype property
							NonFunctionalDatatypeSlot<?> nfdSlot = (NonFunctionalDatatypeSlot<?>) slot;
							sb.append("\t");
							sb.append(this.getDatatypePropertyDeclarationOpeningTag(slot));
							sb.append(nfdSlot.toEncodedValue(command.getValue()));
							sb.append(this.getDatatypePropertyDeclarationClosingTag(slot));						
						}
						found = true;
					}
				}				
			}
		}
		
		// the individual declaration closing tag
		sb.append(this.getIndividualDeclarationClosingTag(aoc));
		
		if (!found) { // if no unpublished properties, the RDF representation is null
			return new String();
		}		
		
		return sb.toString();			
	}		
	
	
	/**
	 * Returns the RDF representing to this class containing all the 
	 * content still not published in the SIB (insertion content only)
	 * @return a String containing the RDF representation of the individual
	 */
	protected String getOWLRepresentation4AddingCommands(AbstractOntConcept aoc) {
		StringBuilder sb = new StringBuilder();
		
		boolean found = false;
		// the individual declaration opening tag
		sb.append(this.getIndividualDeclarationOpeningTag(aoc));

		for (Slot<?> slot : aoc._listSlots()) {
			if (slot.isFunctionalProperty()) {
				for (FunctionalSlotChangeCommand<?> command : aoc._listFunctionalChangeCommands()) {
					if (command.getSlot().equals(slot) && command.getSlot().getValue() == null) {
						if (slot.isObjectProperty()) { // functional object property
							sb.append("\t");
							sb.append(this.getObjectPropertyDeclarationOpeningTag(slot));
							if(command.getValue() instanceof AbstractOntConcept) {
								if (((AbstractOntConcept)command.getValue())._getIndividualNamespace() != nsManager.getDefaultNamespace()) {
									sb.append(((AbstractOntConcept)command.getValue())._getIndividualNamespace());
								} else {
									sb.append("#");
								}
								sb.append(((AbstractOntConcept)command.getValue())._getIndividualID());
							}
							sb.append(this.getObjectPropertyDeclarationClosingTag(slot));
						} else { // functional datatype property
							FunctionalDatatypeSlot<?> fdSlot = (FunctionalDatatypeSlot<?>) slot;
							sb.append("\t");
							sb.append(this.getDatatypePropertyDeclarationOpeningTag(slot));
							sb.append(fdSlot.toEncodedValue(command.getValue()));
							sb.append(this.getDatatypePropertyDeclarationClosingTag(slot));						
						}
						found = true;
					}
				}
			} else {
				for (NonFunctionalSlotAddCommand<?> command : aoc._listNonFunctionalAddingCommands()) {
					if (command.getSlot().equals(slot)) {
						if (slot.isObjectProperty()) {
							sb.append("\t");
							sb.append(this.getObjectPropertyDeclarationOpeningTag(slot));
							if(command.getValue() instanceof AbstractOntConcept) {
								if (((AbstractOntConcept)command.getValue())._getIndividualNamespace() != nsManager.getDefaultNamespace()) {
									sb.append(((AbstractOntConcept)command.getValue())._getIndividualNamespace());
								} else {
									sb.append("#");
								}
								sb.append(((AbstractOntConcept)command.getValue())._getIndividualID());
								
							}
							sb.append(this.getObjectPropertyDeclarationClosingTag(slot));
						} else {
							NonFunctionalDatatypeSlot<?> nfdSlot = (NonFunctionalDatatypeSlot<?>) slot;
							sb.append("\t");
							sb.append(this.getDatatypePropertyDeclarationOpeningTag(slot));
							sb.append(nfdSlot.toEncodedValue(command.getValue()));
							sb.append(this.getDatatypePropertyDeclarationClosingTag(slot));						
						}
						found = true;
					}
				}
			}
		}
	
		// the individual declaration closing tag
		sb.append(this.getIndividualDeclarationClosingTag(aoc));
		
		if (!found) { // if no unpublished properties, the RDF representation is null
			return null;
		}
		
		return sb.toString();
	}

	/**
	 * Returns the RDF representing the object properties (functional and non-functional) 
	 * of aoc that refer to the concepts in the given Collection.
	 * 
	 * @param aoc the concept whose properties will be checked.
	 * @param concepts a Collection containing the concepts that may be referred by aoc.
	 * @return a String containing the RDF representation of the object properties
	 */
	public String getOWLRepresentation4ReferencingConcepts(AbstractOntConcept aoc, 
			Collection<? extends AbstractOntConcept> concepts) {
		StringBuilder sb = new StringBuilder();
		boolean found = false;
		
		sb.append(this.getIndividualDeclarationOpeningTag(aoc));
		
		for (Slot<?> slot : aoc._listSlots()) {
			if (slot.isObjectProperty()) {
				if (slot.isFunctionalProperty()) {
					FunctionalObjectSlot<?> functionalSlot = (FunctionalObjectSlot<?>) slot;
					if (concepts.contains(functionalSlot.getValue())) {
						sb.append(getOWLRepresentation(functionalSlot));							
						found = true;
					}
				} else {
					NonFunctionalObjectSlot<?> nonFunctionalSlot = (NonFunctionalObjectSlot<?>) slot;
					for (AbstractOntConcept concept : nonFunctionalSlot.getValues()) {
						if (concepts.contains(concept)) {
							sb.append(getOWLRepresentation(nonFunctionalSlot, concept));							
							found = true;
						}						
					}
				}
			}
		}
		sb.append(this.getIndividualDeclarationClosingTag(aoc));
		
		if (!found) {
			return this.getIndividualDeclarationTag(aoc);
		}
		
		return sb.toString();		
	}		

	/**
	 * Get the OWL representation of a Property
	 * @param property the property to be represented
	 * @return a String representation of a Slot in OWL format
	 */
	public String getOWLRepresentation(Slot<?> property) {
		StringBuilder sb = new StringBuilder();
		
		if (property.isDatatypeProperty()) { // DATATYPE PROPERTY
			if (property.isFunctionalProperty()) {
				// functional datatype property
				FunctionalDatatypeSlot<?> fdSlot = (FunctionalDatatypeSlot<?>) property;
				if (fdSlot.getValue() != null) {
					sb.append("\t");
					sb.append(this.getDatatypePropertyDeclarationOpeningTag(property));
					sb.append(fdSlot.toEncodedValue());
					sb.append(this.getDatatypePropertyDeclarationClosingTag(property));
				}
			} else { // non functional datatype property
				// functional datatype property
				NonFunctionalDatatypeSlot<?> nfdSlot = (NonFunctionalDatatypeSlot<?>) property;
				for (String encodedValue : nfdSlot.toEncodedValue()) {
					sb.append("\t");
					sb.append(this.getDatatypePropertyDeclarationOpeningTag(property));
					sb.append(encodedValue);
					sb.append(this.getDatatypePropertyDeclarationClosingTag(property));					
				}
			}
		} else { // OBJECT PROPERTY
			if (property.isFunctionalProperty()) {
				// functional object property
				FunctionalObjectSlot<?> foSlot = (FunctionalObjectSlot<?>) property;
				if (foSlot.getValue() != null) {
					sb.append("\t");
					sb.append(this.getObjectPropertyDeclarationOpeningTag(property));
					if (foSlot.getValue()._getIndividualNamespace() != nsManager.getDefaultNamespace()) {
						sb.append(foSlot.getValue()._getIndividualNamespace());
					} else {
						sb.append("#");
					}
					sb.append(foSlot.getValue()._getIndividualID());
					sb.append(this.getObjectPropertyDeclarationClosingTag(property));
				}
			} else { // non functional object property
				NonFunctionalObjectSlot<?> nfoSlot = (NonFunctionalObjectSlot<?>) property;
				for (AbstractOntConcept value : nfoSlot.listValues()) {
					sb.append("\t");
					sb.append(this.getObjectPropertyDeclarationOpeningTag(property));
					if (value._getIndividualNamespace() != nsManager.getDefaultNamespace()) {
						sb.append(value._getIndividualNamespace());
					} else {
						sb.append("#");
					}
					sb.append(value._getIndividualID());
					sb.append(this.getObjectPropertyDeclarationClosingTag(property));		
				}
			}			
		}

		return sb.toString();		
	}
	
	
	/**
	 * Get the OWL representation of a given object property for a given value
	 * @param property the property to be represented
	 * @param concept the value to find
	 * @return a String representation of a Slot in OWL format
	 */
	
	private Object getOWLRepresentation(NonFunctionalObjectSlot<?> property, AbstractOntConcept concept) {
		StringBuilder sb = new StringBuilder();
		for (AbstractOntConcept value : property.listValues()) {
			if (concept.equals(value)) {
				sb.append("\t");
				sb.append(this.getObjectPropertyDeclarationOpeningTag(property));
				if (value._getIndividualNamespace() != nsManager.getDefaultNamespace()) {
					sb.append(value._getIndividualNamespace());
				} else {
					sb.append("#");
				}
				sb.append(value._getIndividualID());
				
				sb.append(this.getObjectPropertyDeclarationClosingTag(property));
			}
		}
		return sb.toString();
	}	
	/**
	 * Creates a namespace declaration
	 * @param manager a NamespaceManager populated with namespaces to be included
	 * @param prefix the prefix used for the current ontology
	 * @param namespace the namespace of current ontology
	 * @param baseNS the base URI for this document
	 * @param supportingPrefixNS the set of pairs <code><prefix,namespace></code> for supporting ontologies
	 */
	public String getNamespaceDeclaration(NamespaceManager manager) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append(this.getNamespaceDeclarationStartElement());

		if (manager.hasDefaultNamespace()) { // the RDF-XML representation of default namespace
			sb.append("xmlns=").append(QUOTE).append(manager.getDefaultNamespace()).append(QUOTE).append(SEPARATOR);
		}

		if (manager.hasBaseNsPrefix()) { // the RDF-XML representation of base namespace
			sb.append("xmlns:").append(manager.getBasePrefix());
			sb.append("=").append(QUOTE).append(manager.getBaseNamespace()).append(QUOTE).append(SEPARATOR);
			sb.append("xml:base=").append(QUOTE).append(manager.getBaseNamespace()).append(QUOTE).append(SEPARATOR);
		} else {
			sb.append("xml:base=").append(QUOTE).append(manager.getDefaultNamespace()).append(QUOTE).append(SEPARATOR);				
		}
		
		if (manager.hasAdditionalOntologyNamespaces()) {
			Map<String,String> supportingOntologyNsPrefixMap = manager.getSupportingOntologiesNsPrefixMap();
			for (String prefix : supportingOntologyNsPrefixMap.keySet()) {
				String namespace = supportingOntologyNsPrefixMap.get(prefix);
				sb.append("xmlns:").append(prefix);
				sb.append("=").append(QUOTE).append(namespace).append(QUOTE).append(SEPARATOR);
			}
		}
		
		// The OWL vocabulary namespace map
		Map<String,String> owlVocabularyNsPrefixMap = manager.getOWLVocabularyNsPrefixMap();
		for (String prefix : owlVocabularyNsPrefixMap.keySet()) {
			String namespace = owlVocabularyNsPrefixMap.get(prefix);
			sb.append("xmlns:").append(prefix);
			sb.append("=").append(QUOTE).append(namespace).append(QUOTE).append(SEPARATOR);
		}
		
		sb.append(CLOSING_TAG).append(SEPARATOR);	

		
		return sb.toString();
	}
	
	/**
	 * Creates the heading element for an XML-RDF 
	 * @return 
	 */
	private String getNamespaceDeclarationStartElement() {
		return "<rdf:RDF ";
	}

	private String getNamespaceDeclarationEndElement() {
		return "</rdf:RDF>";
	}

	/**
	 * Gets the OWL opening tag for an individual declaration
	 * @return a String representing the OWL individual declaration opening tag
	 */
	private String getIndividualDeclarationOpeningTag(AbstractOntConcept aoc) {
		StringBuilder sb = new StringBuilder();
		
		// START tag
		sb.append("\n<").append(aoc._getClassPrefix()).append(":").append(aoc._getClassID());
		sb.append(" rdf:ID=\"");
		sb.append(aoc._getIndividualID());
		sb.append("\">\n");
		
		return sb.toString();
	}

	/**
	 * Gets the OWL closing tag for an individual declaration
	 * @return a String representing the OWL individual declaration closing tag
	 */
	private String getIndividualDeclarationClosingTag(AbstractOntConcept aoc) {
		StringBuilder sb = new StringBuilder();
		
		sb.append("</").append(aoc._getClassPrefix()).append(":").append(aoc._getClassID()).append(">\n");
		
		return sb.toString();
	}
	
	/**
	 * Gets the OWL declaration tag for an individual declaration
	 * @return a String representing the RDF opening tag
	 */
	private String getIndividualDeclarationTag(AbstractOntConcept aoc) {
		StringBuilder sb = new StringBuilder();
		
		// START tag
		sb.append("\n<").append(aoc._getClassPrefix()).append(":").append(aoc._getClassID());
		sb.append(" rdf:ID=\"");
		sb.append(aoc._getIndividualID());
		sb.append("\"/>\n");
		
		return sb.toString();
	}

	/**
	 * Gets the OWL opening tag for a datatype property declaration
	 * @return a String representing the OWL datatype property declaration opening tag
	 */
	private String getDatatypePropertyDeclarationOpeningTag(Slot<?> property) {
		StringBuilder sb = new StringBuilder();
		
		String xsdDatatype = null;
		if (property.isDatatypeProperty() && property.isFunctionalProperty()) {
			xsdDatatype = ((FunctionalDatatypeSlot<?>) property).getRange();
		} else if (property.isDatatypeProperty() && !property.isFunctionalProperty()) {
			xsdDatatype = ((NonFunctionalDatatypeSlot<?>) property).getRange();
		}
		
		// START tag
		sb.append("<").append(property._getPrefix()).append(":").append(property.getName());
		sb.append(" rdf:datatype=\"");
		if (xsdDatatype != null) {
			sb.append(xsdDatatype);		
		} else {
			sb.append("xsd:string");
		}
		sb.append("\">");
		return sb.toString();
	}

	/**
	 * Gets the OWL closing tag for a datatype property declaration
	 * @return a String representing the OWL individual declaration closing tag
	 */
	private String getDatatypePropertyDeclarationClosingTag(Slot<?> property) {
		StringBuilder sb = new StringBuilder();
		
		sb.append("</").append(property._getPrefix()).append(":").append(property.getName()).append(">\n");
		
		return sb.toString();
	}
	
	/**
	 * Gets the OWL opening tag for an object property declaration
	 * @return a String representing the OWL object property declaration opening tag
	 */
	private String getObjectPropertyDeclarationOpeningTag(Slot<?> property) {
		StringBuilder sb = new StringBuilder();
		
		// START tag
		sb.append("<").append(property._getPrefix()).append(":").append(property.getName()).append(" rdf:resource=\"");
		
		return sb.toString();
	}
	
	/**
	 * Gets the OWL closing tag for an object property declaration
	 * @return a String representing the OWL object property declaration closing tag
	 */
	private String getObjectPropertyDeclarationClosingTag(Slot<?> property) {
		StringBuilder sb = new StringBuilder();
		sb.append("\"/>\n");
		return sb.toString();
	}
}
