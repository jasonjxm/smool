package org.smool.kpi.model.smart.encoding.m3.util;

import java.util.ArrayList;
import java.util.Collection;

import org.smool.kpi.model.smart.util.IRIUtil;


public class ResultsProperty implements Comparable<ResultsProperty> {
	private String propertyIRI;
	private ArrayList<String> newValues;
	private ArrayList<String> oldValues;
	
	/** Determines if the property is an owl:DatatypeProperty */
	private boolean datatypeProperty;

	/** Determines if the property is an owl:ObjectProperty */
	private boolean objectProperty;
	
	/** Determines if the property is an owl:FunctionalProperty */
	private boolean functional;	
	
	private boolean initialized;
	
	public ResultsProperty(String propertyIRI) {
		this.setPropertyIRI(propertyIRI);
		newValues = new ArrayList<String>();
		oldValues = new ArrayList<String>();
		this.setDatatypeProperty(false);
		this.setObjectProperty(false);
		this.setFunctional(false);
		this.initialized = false;
	}
	
	/**
	 * Sets the URI of the property
	 * @param uri the uri of the property
	 */
	public void setPropertyIRI(String uri) {
		this.propertyIRI = uri;
	}

	/**
	 * Gets the full URI of the property
	 * @return the uri
	 */
	public String getPropertyIRI() {
		return propertyIRI;
	}

	/**
	 * Gets the name used in the model layer for the current property
	 * @return the name of the property
	 */
	public String getName() {
		return getPropertyName(propertyIRI);
	}	

	/**
	 * Adds a new value to the property.
	 * @param value the String representation of the property. 
	 *         interpretation of the value should be based on the type of property (datatype or object).
	 */
	public void addNewValue(String value) {
		this.newValues.add(value);
	}

	/**
	 * Gets the value corresponding to the M3Property.
	 * It is assumed that the property is functional
	 * @return a String representing the property values. 
	 *         interpretation of the value should be based on the type of property (datatype or object).
	 */
	public String getNewValue() {
		if (newValues.size() > 0) {
			return (String) newValues.toArray()[0];
		} return null;
	}

	/**
	 * List the values corresponding to the M3Property.
	 * It is assumed that the property is non-functional
	 * @return a set of String representing the property values.
	 *         interpretation of the value should be based on the type of property (datatype or object).
	 */
	public Collection<String> listNewValues() {
		return newValues;
	}


	/**
	 * Adds a new value to the property.
	 * @param value the String representation of the property. 
	 *         interpretation of the value should be based on the type of property (datatype or object).
	 */
	public void addOldValue(String value) {
		this.oldValues.add(value);
	}

	/**
	 * Gets the value corresponding to the M3Property.
	 * It is assumed that the property is functional
	 * @return a String representing the property values. 
	 *         interpretation of the value should be based on the type of property (datatype or object).
	 */
	public String getOldValue() {
		if (oldValues.size() > 0) {
			return (String) oldValues.toArray()[0];
		} return null;
	}

	/**
	 * List the values corresponding to the M3Property.
	 * It is assumed that the property is non-functional
	 * @return a set of String representing the property values.
	 *         interpretation of the value should be based on the type of property (datatype or object).
	 */
	public Collection<String> listObsoleteValues() {
		return oldValues;
	}
	
	/**
	 * Sets the datatype property value
	 * @param objectProperty <code>true</code> if the current property is a owl:DatatypeProperty.
	 *        note: if the property is labeled as datatype property, then it is sure that it is not an object property
	 */
	public void setDatatypeProperty(boolean datatypeProperty) {
		this.datatypeProperty = datatypeProperty;
		if (datatypeProperty) {
			this.objectProperty = false;
		}
	}	
	
	/**
	 * Determines if the M3Property is a datatype property
	 * @return <code>true</code> if the property is datatyped.
	 */
	public boolean isDatatypeProperty() {
		return datatypeProperty;
	}

	/**
	 * Sets the object property value
	 * @param objectProperty <code>true</code> if the current property is a owl:ObjectProperty.
	 *        note: if the property is labeled as object property, then it is sure that it is not a datatype property
	 */
	public void setObjectProperty(boolean objectProperty) {
		this.objectProperty = objectProperty;
		if (objectProperty) {
			this.datatypeProperty = false;
		}
	}

	/**
	 * Determines if the M3Property is an object property
	 * @return <code>true</code> if the property is an object reference.
	 */
	public boolean isObjectProperty() {
		return objectProperty;
	}

	/**
	 * Sets the functional property value
	 * @param functional <code>true</code> if the current property is functional.
	 */
	public void setFunctional(boolean functional) {
		this.functional = functional;
	}

	/**
	 * Determines if the M3Property is a functional property
	 * @return <code>true</code> if the property is functional.
	 */
	public boolean isFunctional() {
		return functional;
	}
	
	/**
	 * Gets a java-based normalized name of the property
	 * @param uri the URI of the property
	 * @return the normalized name of the property
	 */
	private String getPropertyName(String uri) {
		String datatypePropertyName = IRIUtil.getElement(uri);
		datatypePropertyName = Naming.getNormalizedJavaPropertyName(datatypePropertyName);
		return datatypePropertyName;
		//return datatypePropertyName.substring(0, 1).toLowerCase() + datatypePropertyName.substring(1);
	}	

	/**
	 * Compares a M3Property to another M3Property based on the property URI
	 * @param property a M3Property
	 * @return the result of comparision
	 */
	@Override
	public int compareTo(ResultsProperty property) {
		return this.getPropertyIRI().compareTo(property.getPropertyIRI());
	}

	/**
	 * Returns the string representation of the M3Property
	 * @return the string representation of the M3Property
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if (functional) {
			sb.append(this.getName()).append(":").append(this.getNewValue());
		} else {
			sb.append(this.getName()).append(":");
			int size = this.newValues.size();
			for (int i = 0; i < size; i++) {
				if (i == 0) {
					sb.append("[");
				}

				sb.append(this.listNewValues().toArray()[i]);
				
				if (i == size - 1) {
					sb.append("]");
				} else {
					sb.append(",");
				}
			}
		}
		return sb.toString();
	}

	/**
	 * Determines if the results data has some content (added, removed or updated value)
	 * 
	 * @return <code>true</code> if there is some data to add, remove or update. <code>false</code> in other cases
	 */	
	public boolean hasContent() {
		if (newValues != null || oldValues != null) {
			for (String value : newValues) {
				if (value != null) { // if at least one value is not null, there is data to add or udpate
					return true;
				}
			}

			for (String value : oldValues) {
				if (value != null) { // if at least one value is not null, there is data to remove
					return true;
				}
			}
		
		}
		return false;
	}

	/**
	 * Sets if ontological data is initialized.
	 * Ontological data refers to functional, datatype and object properties
	 * @param initalized a boolean value determining if ontological data is initalized
	 */
	public void setInitialized(boolean initialized) {
		this.initialized = initialized;
	}

	/**
	 * Determines if ontological data is initialized.
	 * Ontological data refers to functional, datatype and object properties
	 * @return a boolean value determining if ontological data is initalized
	 */
	public boolean isInitialized() {
		return initialized;
	}	
	
}
