package org.smool.kpi.model.smart.encoding.m3.util;

public class IndividualRelationship {
	private Long subjectSubscriptionId;
	private String subjectIRI;
	private String propertyName;
	private Long objectSubscriptionId;
	private String objectIRI;
	
	public IndividualRelationship(String subjectIRI, String propertyName,
			String objectIRI) {
		this.subjectIRI = subjectIRI;
		this.propertyName = propertyName;
		this.objectIRI = objectIRI;
	}

	public Long getSubjectSubscriptionId() {
		return subjectSubscriptionId;
	}

	public void setSubjectSubscriptionId(Long subjectSubscriptionId) {
		this.subjectSubscriptionId = subjectSubscriptionId;
	}

	public String getSubjectIRI() {
		return subjectIRI;
	}

	public void setSubjectIRI(String subjectIRI) {
		this.subjectIRI = subjectIRI;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public Long getObjectSubscriptionId() {
		return objectSubscriptionId;
	}

	public void setObjectSubscriptionId(Long objectSubscriptionId) {
		this.objectSubscriptionId = objectSubscriptionId;
	}

	public String getObjectIRI() {
		return objectIRI;
	}

	public void setObjectIRI(String objectIRI) {
		this.objectIRI = objectIRI;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[").append(subjectIRI);
		sb.append("(").append(subjectSubscriptionId).append("), ");
		sb.append(propertyName).append(", ");
		sb.append(objectIRI).append("(").append(objectSubscriptionId).append(")] ");

		return sb.toString();
	}

}
