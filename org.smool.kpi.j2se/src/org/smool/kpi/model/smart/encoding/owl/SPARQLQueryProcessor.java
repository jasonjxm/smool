/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/

package org.smool.kpi.model.smart.encoding.owl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.smool.kpi.model.exception.KPIModelException;
import org.smool.kpi.model.smart.AbstractOntConcept;
import org.smool.kpi.model.smart.IAbstractOntConcept;
import org.smool.kpi.model.smart.SmartModel;
import org.smool.kpi.model.smart.encoding.AbstractQueryProcessor;
import org.smool.kpi.model.smart.parser.SPARQLResultsParser;
import org.smool.kpi.model.smart.parser.classes.Concept;
import org.smool.kpi.model.smart.slots.FunctionalDatatypeSlot;
import org.smool.kpi.model.smart.slots.FunctionalObjectSlot;
import org.smool.kpi.model.smart.slots.NonFunctionalDatatypeSlot;
import org.smool.kpi.model.smart.slots.NonFunctionalObjectSlot;
import org.smool.kpi.model.smart.slots.Slot;
import org.smool.kpi.model.smart.subscription.ISubscription;
import org.smool.kpi.ssap.PendingTask;
import org.smool.kpi.ssap.exception.KPISSAPException;
import org.smool.kpi.ssap.message.parameter.SSAPMessageRDFParameter.TypeAttribute;
import org.smool.kpi.ssap.message.parameter.SSAPMessageStatusParameter.SIBStatus;


/**
 * The SPARQL query processor allows to query the SIB in SPARQL format
 *
 * @author Fran Ruiz, fran.ruiz@tecnalia.com, Tecnalia
 * @author Raul Otaolea, raul.otaolea@tecnalia.com, Tecnalia
 *
 */
public class SPARQLQueryProcessor extends AbstractQueryProcessor {

	/**
	 * Constructor
	 * @param model the smart model associated to the current query processor
	 */
	public SPARQLQueryProcessor(SmartModel model) {
		super(model);
	}

	/**
	 * Queries about a determinate class
	 * @param concept a class extending an AbstractOntConcept
	 * @return the list of abstract ontology concepts belonging to the provided class. if no individuals
	 *         are found, a void list will be returned
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <C extends IAbstractOntConcept> List<C> query(Class<C> concept)	throws KPIModelException {
		try {
			// Create a new void instance of the concept
			// This allows to query about the data
			AbstractOntConcept aoc = (AbstractOntConcept) concept.newInstance();
			StringBuilder sb = new StringBuilder();
			sb.append(getCDATAOpeningTag());
			sb.append(aoc.toSPARQLQuery());
			sb.append(getCDATAClosingTag());
			PendingTask pt = model.getSIB().query(sb.toString(), TypeAttribute.SPARQL);

			model.getSIB().publish();
			pt.waitUntilFinished(model.getTimeout());
			if(pt.isFinished()) {
				try {
					SIBStatus status = (SIBStatus)pt.getProperty("status");
					if(status == SIBStatus.STATUS_OK) {
						String results = (String) pt.getProperty("results");

						if ((results != null) && !results.equals("")) {
							return (List<C>) processResults(results, (Class<AbstractOntConcept>)concept);
						}
					} else {
						throw new KPIModelException("The query has executed with errors.");
					}
				} catch(Exception e) {
					throw new KPIModelException("An error has ocurred while subscribing", e);
				}
			} else {
				throw new KPIModelException("The query finished by timeout");
			}
		} catch (KPIModelException e) {
			throw e;
		} catch (Exception e) {
			throw new KPIModelException(e.getMessage(), e.getCause());
		}

		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <C extends IAbstractOntConcept> void subscribe(Class<C> concept, ISubscription<? super C> as) throws KPIModelException {

		long subscriptionId;
		try {
			AbstractOntConcept aoc = (AbstractOntConcept) concept.newInstance();
			StringBuilder sb = new StringBuilder();
			sb.append(getCDATAOpeningTag());
			sb.append(aoc.toSPARQLQuery());
			sb.append(getCDATAClosingTag());
			PendingTask pt = model.getSIB().subscribe(sb.toString(), TypeAttribute.SPARQL);

			model.getSIB().publish();
			pt.waitUntilFinished(model.getTimeout());
			if(pt.isFinished()) {
				try {
					SIBStatus status = (SIBStatus)pt.getProperty("status");
					if(status == SIBStatus.STATUS_OK) {
						// gets the subscription identifier
						subscriptionId = (Long)pt.getProperty("subscriptionId");

						subscribedConcepts.put(subscriptionId, (Class<? extends AbstractOntConcept>) concept);

						String results = (String) pt.getProperty("results");

						if ((results != null) && !results.equals("")) {
							ArrayList<C> subscriptionConcepts = (ArrayList<C>) processResults(results, subscribedConcepts.get(subscriptionId));

							for(C subscriptionConcept : subscriptionConcepts) {
								as.conceptAdded(subscriptionConcept);
							}
						}

						this.model.getSIB().addSubscriptionsListener(subscriptionId, this);
						subscriptions.put(subscriptionId, (ISubscription<AbstractOntConcept>) as);

						//return subscriptionId;
					} else {
						throw new KPIModelException("The query has executed with errors.");
					}
				} catch(Exception e) {
					e.printStackTrace();
					throw new KPIModelException("An error has ocurred while subscribing", e);
				}
			} else {
				throw new KPIModelException("The query finished by timeout");
			}
		} catch (KPIModelException e) {
			throw e;
		} catch (Exception e) {
			throw new KPIModelException(e.getMessage(), e.getCause());
		}
	}

	@Override
	public void indicationReceived(final long subscriptionId, final String newResults,
			final String obsoleteResults) throws KPISSAPException {

		System.out.println("**** SPARQLQueryProcessor: IndicationReceived() ****");

		indicationQueue.submit(new Runnable() {
			public void run() {
			synchronized(model.getLock()) {
			try {
				ArrayList<? extends AbstractOntConcept> newConcepts = null;
				ArrayList<? extends AbstractOntConcept> obsoleteConcepts = null;
				if ((newResults != null) && !newResults.equals("")) {
					newConcepts = processResults(newResults, subscribedConcepts.get(subscriptionId));
				}

				if ((obsoleteResults != null) && !obsoleteResults.equals("")) {
					obsoleteConcepts = processResults(obsoleteResults, subscribedConcepts.get(subscriptionId));
				}

				if ((newConcepts == null) || (obsoleteConcepts == null)) {
					if (newConcepts == null) {
						for(AbstractOntConcept concept : obsoleteConcepts) {
							subscriptions.get(subscriptionId).conceptRemoved(concept);
						}
					} else if (obsoleteConcepts == null) {
						for(AbstractOntConcept concept : newConcepts) {
							subscriptions.get(subscriptionId).conceptAdded(concept);
						}
					}
				} else {
					for(AbstractOntConcept newConcept : newConcepts) {
						for(AbstractOntConcept obsoleteConcept : obsoleteConcepts) {
							if (newConcept.equals(obsoleteConcept)) {
								subscriptions.get(subscriptionId).conceptUpdated(newConcept, obsoleteConcept);
							}
						}
					}
				}
			} catch (KPIModelException ex) {
				ex.printStackTrace();
				//throw new KPIModelException(ex.getMessage(), ex);
			} catch (Exception ex) {
				ex.printStackTrace();
				//throw new KPIModelException(ex.getMessage(), ex);
			}
			}
			}
			});
	}

	private String getCDATAOpeningTag() {
		return "<![CDATA[";
	}

	private String getCDATAClosingTag() {
		return "]]>";
	}

	/**
	 * Process the results and transforms it into classes
	 * @param results the results to be processed
	 * @param stereotype the class associated to the subscription
	 * @return a set of AbstractOntConcepts that represents the results
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private ArrayList<AbstractOntConcept> processResults(String results, Class<? extends AbstractOntConcept> stereotype) throws KPIModelException {

		ArrayList<AbstractOntConcept> resultConcepts = new ArrayList<AbstractOntConcept>();

		try {
			// The SPARQL Results parser
			SPARQLResultsParser parser = new SPARQLResultsParser();

			// Gets the set of concepts
			ArrayList<Concept> concepts = parser.parse(results);

			// Explore each of the concepts
			for (Concept conceptResult : concepts) {
				// Gets a new instance
				AbstractOntConcept aoc = stereotype.newInstance();

				// Establish the IRI
				aoc._setIndividualIRI(conceptResult.getId());

				// Explores the datatype properties
				HashMap<String,ArrayList<String>> datatypeProperties = conceptResult.getDatatypeProperties();

				// Gets the content of the datatype properties and assign to the created instance
				for (String datatypeProperty : datatypeProperties.keySet()) {
					Slot<?> slot = aoc._getProperty(datatypeProperty);
					// Functional properties
					if (slot.isFunctionalProperty()) {
						FunctionalDatatypeSlot fdslot = (FunctionalDatatypeSlot) slot;
						fdslot.setValue(fdslot.fromEncodedValue(datatypeProperties.get(datatypeProperty).get(0)));
						aoc._addProperty(fdslot);
					} else { // Non-functional properties
						NonFunctionalDatatypeSlot nfdslot = (NonFunctionalDatatypeSlot) slot;
						for (String datatypeValue : datatypeProperties.get(datatypeProperty)) {
							nfdslot.addValue(nfdslot.fromEncodedValue(datatypeValue));
						}
						aoc._addProperty(nfdslot);
					}
				}

				// Gets the content of the object properties and assign to the created instance
				HashMap<String,ArrayList<Concept>> objectProperties = conceptResult.getObjectProperties();
				for (String objectProperty : objectProperties.keySet()) {
					Slot slot = aoc._getProperty(objectProperty);
					if (slot.isFunctionalProperty()) {
						Concept propertyValue = objectProperties.get(objectProperty).get(0);
						FunctionalObjectSlot foslot = (FunctionalObjectSlot) slot;
						AbstractOntConcept slotValue = createOntConcept(propertyValue, foslot.getSlotClass());
						foslot.setValue(slotValue);
						aoc._addProperty(foslot);
					} else {
						NonFunctionalObjectSlot nfoslot = (NonFunctionalObjectSlot) slot;
						for (Concept objectValue : objectProperties.get(objectProperty)) {
							AbstractOntConcept slotValue = createOntConcept(objectValue, nfoslot.getSlotClass());
							nfoslot.addValue(slotValue);
						}
						aoc._addProperty(nfoslot);
					}
				}

				resultConcepts.add(aoc);
			}
		} catch (KPIModelException ex) {
			ex.printStackTrace();
			throw new KPIModelException(ex.getMessage(), ex);
		} catch (IllegalAccessException ex) {
			ex.printStackTrace();
			throw new KPIModelException(ex.getMessage(), ex);
		} catch (InstantiationException ex) {
			ex.printStackTrace();
			throw new KPIModelException(ex.getMessage(), ex);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new KPIModelException(ex.getMessage(), ex);
		}

		return resultConcepts;
	}


	/**
	 * Process the results and transforms it into classes
	 * @param results the results to be processed
	 * @param stereotype the class associated to the subscription
	 * @return a set of AbstractOntConcepts that represents the results
	 */
	@SuppressWarnings({ "unchecked", "rawtypes"})
	//@SuppressWarnings({ "unchecked"})
	private AbstractOntConcept createOntConcept(Concept concept, Class<? extends AbstractOntConcept> stereotype) throws KPIModelException {
		AbstractOntConcept aoc = null;
		try {
			aoc = stereotype.newInstance();
			aoc._setIndividualIRI(concept.getId());
			for (String datatypeProperty : concept.getDatatypeProperties().keySet()) {
				Slot<?> slotProp = aoc._getProperty(datatypeProperty);
				if (slotProp.isFunctionalProperty()) {
					FunctionalDatatypeSlot fdslot = (FunctionalDatatypeSlot) slotProp;
					fdslot.setValue(fdslot.fromEncodedValue(concept.getDatatypeProperties().get(datatypeProperty).get(0)));
					aoc._addProperty(fdslot);
				} else {
					NonFunctionalDatatypeSlot nfdslot = (NonFunctionalDatatypeSlot) slotProp;
					for (String datatypeValue : concept.getDatatypeProperties().get(datatypeProperty)) {
						nfdslot.addValue(nfdslot.fromEncodedValue(datatypeValue));
					}
					aoc._addProperty(nfdslot);
				}
			}
			for (String objectProperty : concept.getObjectProperties().keySet()) {
				Slot slotProp = aoc._getProperty(objectProperty);
				if (slotProp.isFunctionalProperty()) {
					FunctionalObjectSlot foslot = (FunctionalObjectSlot) slotProp;
					Concept conceptValue = concept.getObjectProperties().get(objectProperty).get(0);
					AbstractOntConcept propertyValue = createOntConcept(conceptValue, foslot.getSlotClass());
					foslot.setValue(propertyValue);
					aoc._addProperty(foslot);
				} else {
					NonFunctionalObjectSlot nfoslot = (NonFunctionalObjectSlot) slotProp;
					for (Concept objectValue : concept.getObjectProperties().get(objectProperty)) {
						AbstractOntConcept propertyValue = createOntConcept(objectValue, nfoslot.getSlotClass());
						nfoslot.addValue(propertyValue);
					}
					aoc._addProperty(nfoslot);
				}
			}
		} catch (IllegalAccessException ex) {
			ex.printStackTrace();
			throw new KPIModelException(ex.getMessage(), ex);
		} catch (InstantiationException ex) {
			ex.printStackTrace();
			throw new KPIModelException(ex.getMessage(), ex);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new KPIModelException(ex.getMessage(), ex);
		}
		return aoc;
	}

	@Override
	public <C extends IAbstractOntConcept> C query(Class<C> concept, String id)
			throws KPIModelException {
		throw new KPIModelException("Not yet implemented");
	}

	@Override
	public <C extends IAbstractOntConcept> void subscribe(Class<C> concept, String id, ISubscription<? super C> as) throws KPIModelException {
		throw new KPIModelException("Not yet implemented");
	}


}
