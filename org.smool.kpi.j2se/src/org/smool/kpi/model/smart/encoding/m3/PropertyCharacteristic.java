package org.smool.kpi.model.smart.encoding.m3;

import org.smool.kpi.model.smart.encoding.m3.util.ResultsProperty;

public class PropertyCharacteristic implements Comparable<PropertyCharacteristic>{
	private String propertyIRI;
	private boolean functionalProp;
	private boolean datatypeProp;
	private boolean objectProp;

	public PropertyCharacteristic(String propertyIRI) {
		this.propertyIRI = propertyIRI;
	}
	
	public PropertyCharacteristic(ResultsProperty subsProp) {
		this.propertyIRI = subsProp.getPropertyIRI();
		this.functionalProp = subsProp.isFunctional();
		this.datatypeProp = subsProp.isDatatypeProperty();
		this.objectProp = subsProp.isObjectProperty();
	}

	/**
	 * @param propertyIRI the propertyIRI to set
	 */
	public void setPropertyIRI(String propertyIRI) {
		this.propertyIRI = propertyIRI;
	}

	/**
	 * @return the propertyIRI
	 */
	public String getPropertyIRI() {
		return propertyIRI;
	}

	/**
	 * @param functionalProp the functionalProp to set
	 */
	public void setFunctional(boolean functionalProp) {
		this.functionalProp = functionalProp;
	}

	/**
	 * @return the functionalProp
	 */
	public boolean isFunctional() {
		return functionalProp;
	}

	/**
	 * @param datatypeProp the datatypeProp to set
	 */
	public void setDatatype(boolean datatypeProp) {
		this.datatypeProp = datatypeProp;
	}

	/**
	 * @return the datatypeProp
	 */
	public boolean isDatatype() {
		return datatypeProp;
	}

	/**
	 * @param objectProp the objectProp to set
	 */
	public void setObject(boolean objectProp) {
		this.objectProp = objectProp;
	}

	/**
	 * @return the objectProp
	 */
	public boolean isObject() {
		return objectProp;
	}
	
	@Override
	public int compareTo(PropertyCharacteristic arg0) {
		return this.getPropertyIRI().compareTo(arg0.getPropertyIRI());
	}	
}
