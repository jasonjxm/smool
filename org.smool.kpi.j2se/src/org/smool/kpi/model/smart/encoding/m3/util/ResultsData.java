/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia - Software Systems Development
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia - Software Systems Development) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia - Software Systems Development) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.model.smart.encoding.m3.util;

import java.util.ArrayList;

import org.smool.kpi.common.rdfm3.RDFM3Triple;


/**
 * Subscription Data represents the data associated to retrieved message 
 * after a QUERY REQUEST or SUBSCRIPTION REQUEST. The data can be either
 * come from QUERY CONFIRM, SUBSCRIPTION CONFIRM or SUBSCRIPTION INDICATION. 
 * 
 * @author Fran Ruiz, Fran.Ruiz@tecnalia.com, Tecnalia - Software Systems Development
 *
 */
public class ResultsData implements Comparable<ResultsData> {
	/** The IRI corresponding to the retrieved individual */
	private String individualIRI;
	
	/** The set of properties that change in the retrieved indication */
	private ArrayList<ResultsProperty> properties;

	private boolean removable;
	
	/** The rdf:type IRI */
	private static final String RDF_TYPE = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";	
	
	/**
	 * Constructor of SubscriptionData
	 * @param individualIRI
	 */
	public ResultsData(String individualIRI) {
		this.setIndividualIRI(individualIRI);
		this.properties = new ArrayList<ResultsProperty>();
		this.removable = false;
	}
	
	/**
	 * @param individualID the individualID to set
	 */
	public void setIndividualIRI(String individualIRI) {
		this.individualIRI = individualIRI;
	}

	/**
	 * Gets the individual IRI
	 * @return the individualIRI
	 */
	public String getIndividualIRI() {
		return individualIRI;
	}
	
	/**
	 * Sets the new results to the current indication data
	 * @param triple the triple in RDFM3Triple format
	 */
	public void addNewValue(RDFM3Triple triple) {
		if (!isRDFType(triple)) {
			boolean found = false;
			for (ResultsProperty property : properties) {
				if (property.getPropertyIRI().equals(triple.getPredicate())) {
					if (triple.isObjectLiteral()) {
						property.addNewValue(triple.getObjectDataContent());
					} else {
						property.addNewValue(triple.getObject());
					}
					found = true;
				}
			}
			
			if (!found) {
				ResultsProperty property = new ResultsProperty(triple.getPredicate());
				if (triple.isObjectLiteral()) {
					property.addNewValue(triple.getObjectDataContent());
				} else {
					property.addNewValue(triple.getObject());
				}
				properties.add(property);
			}
		}
	}	

	/**
	 * Adds an obsolete value to the current ResultData
	 * @param triple the triple in RDFM3Triple format
	 */
	public void addObsoleteValue(RDFM3Triple triple) {
		if (!isRDFType(triple)) { // only take into account data that is not rdf-type
			boolean found = false;
			for (ResultsProperty property : properties) {
				if (property.getPropertyIRI().equals(triple.getPredicate())) {
					if (triple.isObjectLiteral()) {
						property.addOldValue(triple.getObjectDataContent());
					} else {
						property.addOldValue(triple.getObject());
					}					
					found = true;
				}
			}
			
			if (!found) {
				ResultsProperty property = new ResultsProperty(triple.getPredicate());
				if (triple.isObjectLiteral()) {
					property.addOldValue(triple.getObjectDataContent());
				} else {
					property.addOldValue(triple.getObject());
				}					
				properties.add(property);
			}
		}
	}
	
	/**
	 * Determines if the triple property is an rdf:type property
	 * @param triple a triple in RDFM3Triple format
	 * @return <code>true</code> if the property is an rdf:type property
	 */
	private boolean isRDFType(RDFM3Triple triple) {
		return triple.getPredicate().equals(RDF_TYPE);
	}
	
	/**
	 * Lists the properties
	 * @return a RDFM3Triple with the new results
	 */
	public ArrayList<ResultsProperty> listProperties() {
		return properties;
	}

	@Override
	public int compareTo(ResultsData data) {
		return this.getIndividualIRI().compareTo(data.getIndividualIRI());
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("id=").append(individualIRI.substring(individualIRI.lastIndexOf("#")));
		if (properties.size() > 0) {
			sb.append(" props={");
			for(ResultsProperty property : properties) {
				sb.append("\n[").append(property.toString()).append("]");
			}
			sb.append("}");
		}
		return sb.toString();
	}

	/**
	 * Determines if the results data has some content (added, removed or updated value)
	 * 
	 * @return <code>true</code> if there is some data to add, remove or update. <code>false</code> in other cases
	 */
	public boolean hasContent() {
		if (properties != null) {
			for (ResultsProperty property : properties) {
				if (property.hasContent()) { // if at least one property has content, there is data to add,remove or udpate
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Marks the individual as individual to be removed
	 * @param remove
	 */
	public void setRemovable(boolean removable) {
		this.removable = removable;
	}
	
	public boolean isRemovable() {
		return removable;
	}
	
	
	public boolean isRemovableIndividual() {
		return isRemovable();
	}

}
