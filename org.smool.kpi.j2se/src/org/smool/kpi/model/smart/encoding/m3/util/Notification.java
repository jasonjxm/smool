/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research & Innovation
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.model.smart.encoding.m3.util;

import org.smool.kpi.model.smart.AbstractOntConcept;

/**
 * A helper class containing the content of a notification
 * 
 * @author Raul Otaolea, Raul.Otaolea@tecnalia.com, Tecnalia - Software Systems Development
 * @author Fran Ruiz, Fran.Ruiz@tecnalia.com, Tecnalia - Software Systems Development
 */
public class Notification {
	private AbstractOntConcept newIndividual;
	private AbstractOntConcept oldIndividual;
	
	/**
	 * Constructor
	 * @param newIndividual the new individual
	 * @param oldIndividual the old individual
	 */
	public Notification(AbstractOntConcept newIndividual, AbstractOntConcept oldIndividual) {
		this.newIndividual = newIndividual;
		this.oldIndividual = oldIndividual;
	}

	/**
	 * Gets the new individual
	 * @return an AbstractOntConcept representing the new individual
	 */
	public AbstractOntConcept getNewIndividual() {
		return newIndividual;
	}

	/**
	 * Gets the old individual
	 * @return an AbstractOntConcept representing the obsolete individual
	 */
	public AbstractOntConcept getOldIndividual() {
		return oldIndividual;
	}

}
