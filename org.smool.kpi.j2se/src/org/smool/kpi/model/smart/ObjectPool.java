/*
 * Copyright 1999-2004 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * $Id: ObjectPool.java,v 1.2.4.1 2005/09/15 08:15:50 suresh_emailid Exp $
 */
package org.smool.kpi.model.smart;

import java.util.Vector;

/**
 * Pool of object of a given type to pick from to help memory usage Simplified
 * generic version of the original
 * com.sun.org.apache.xml.internal.utils.ObjectPool
 */
public class ObjectPool<C> implements java.io.Serializable {

	static final long serialVersionUID = -8519013691660936643L;

	/**
	 * Type of objects in this pool.
	 *
	 * @serial
	 */
	private final Class<C> objectType;

	/**
	 * Vector of given objects this points to.
	 *
	 * @serial
	 */
	private final Vector<C> freeStack;


	/**
	 * Constructor ObjectPool
	 *
	 * @param type Type of objects for this pool
	 */
	public ObjectPool(Class<C> type) {
		objectType = type;
		freeStack = new Vector<C>();
	}


	/**
	 * Constructor ObjectPool
	 *
	 * @param type Type of objects for this pool
	 * @param size Size of vector to allocate
	 */
	public ObjectPool(Class<C> type, int size) {
		objectType = type;
		freeStack = new Vector<C>( size );
	}


	/**
	 * Constructor ObjectPool
	 */
	public ObjectPool() {
		objectType = null;
		freeStack = new Vector<C>();
	}


	/**
	 * Get an instance of the given object in this pool if available
	 *
	 * @return an instance of the given object if available or null
	 */
	public synchronized Object getInstanceIfFree() {

		// Check if the pool is empty.
		if (!freeStack.isEmpty()) {

			// Remove object from end of free pool.
			C result = freeStack.lastElement();

			freeStack.setSize( freeStack.size() - 1 );

			return result;
		}

		return null;
	}


	/**
	 * Get an instance of the given object in this pool
	 *
	 * @return An instance of the given object
	 */
	public synchronized C getInstance() {

		// Check if the pool is empty.
		if (freeStack.isEmpty()) {
			Exception e;
			// Create a new object if so.
			try {
				return objectType.newInstance();
			}
			catch (InstantiationException ex) {
				e = ex;
			}
			catch (IllegalAccessException ex) {
				e = ex;
			}

			// Throw unchecked exception for error in pool configuration.
			throw new RuntimeException( "exception creating new instance for pool", e );
		}
		else {
			// Remove object from end of free pool.
			C result = freeStack.lastElement();

			freeStack.setSize( freeStack.size() - 1 );

			return result;
		}
	}


	/**
	 * Add an instance of the given object to the pool
	 *
	 * @param obj Object to add.
	 */
	public synchronized void freeInstance( C obj ) {
		freeStack.addElement( obj );
	}
}
