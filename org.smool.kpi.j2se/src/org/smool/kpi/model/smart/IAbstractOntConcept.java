/*****************************************************************************
  Project: org.smool.kpi.j2se
  ----------------------------------------------------------------------------
  Project Part  : org.smool.kpi.model.smart
  Creation date : Jul 15, 2011

 *****************************************************************************
 ***      Twente Institute of Wireless and Mobile Communications           ***
 ***      (C) COPYRIGHT TI-WMC, 2011, info@ti-wmc.nl                       ***
 ***      All rights reserved                                              ***
 *****************************************************************************/

package org.smool.kpi.model.smart;

import java.util.List;

//-------------------------------------------------------------------------
/**
 *
 */
public interface IAbstractOntConcept extends Comparable<IAbstractOntConcept> {


	/**
	 * Gets the class individual identifier
	 * @return a String with named class individual identifier
	 */
	public String _getIndividualID();

	/**
	 * Indicates if the status of this object is 'published in the SIB'
	 */
	public boolean _isPublished();

	/**
	 * Indicates if the status of this concept is INVALIDATED
	 * (=this instance is not part of any SIB [any more])
	 * TODO it might be a smart move to allow clients to read the state directly with _getState
	 */
	public boolean _isInvalid();

	/**
	 * Gets the current value for a given property
	 * @param <T> the property type
	 * @param attributeName the attribute name
	 * @return the current value for a given property
	 */
	public abstract <T> T getCurrentValue( final String attributeName );


	/**
	 * Gets the list of current attribute values
	 * @param <T>
	 * @param attributeName
	 * @return
	 */
	public abstract <T> List<T> listCurrentValues( String attributeName );

}