/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/

package org.smool.kpi.model.smart;

import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.smool.kpi.common.Logger;
import org.smool.kpi.model.smart.encoding.m3.util.ResultsData;
import org.smool.kpi.model.smart.encoding.m3.util.ResultsProperty;
import org.smool.kpi.model.smart.slots.FunctionalObjectSlot;
import org.smool.kpi.model.smart.slots.FunctionalSlot;
import org.smool.kpi.model.smart.slots.FunctionalSlotChangeCommand;
import org.smool.kpi.model.smart.slots.NonFunctionalObjectSlot;
import org.smool.kpi.model.smart.slots.NonFunctionalSlot;
import org.smool.kpi.model.smart.slots.NonFunctionalSlotAddCommand;
import org.smool.kpi.model.smart.slots.NonFunctionalSlotRemoveCommand;
import org.smool.kpi.model.smart.slots.Slot;
import org.smool.kpi.model.smart.util.IRIUtil;
import org.smool.kpi.model.smart.util.NamespaceManager;


/**
 * An abstract class that implements all the instances
 * of the ontology concepts
 *
 * @author Raul Otaolea, Raul.Otaolea@esi.es, ESI
 * @author Fran Ruiz, Fran.Ruiz@esi.es, ESI
 */
public abstract class AbstractOntConcept implements IAbstractOntConcept, Comparable<IAbstractOntConcept>, Cloneable {

	private enum Status {
		UNPUBLISHED, /** A concept not belonging that is not published */
		PUBLISHREQUESTED, /** A concept requested to be published in the SIB */
		PUBLISHED, /** A concept that is already published in the SIB */
		NOTPUBLISHED, /** A concept was rejected to be published in the SIB */
		OUTDATED, /** A concept that was modified after being published */
		INVALIDATED /** When the concept was superseded by other concept or removed from the SIB */
	}

	/** The individual fields */
	private String individualID;
	private String individualNamespace;

	/** The ontology class reference */
	private String classID;
	private String classNamespace;

	/** The namespace manager that stores the pairs <prefix, namespace> */
	private NamespaceManager nsManager;

	/** The set of attributes */
	private ArrayList<Slot<?>> slots;

	/** The object to be used in concurrent operations */
	private Object latch;

	/** The slot change command */
	protected ArrayList<FunctionalSlotChangeCommand<?>> functionalChangeCommands;
	protected ArrayList<NonFunctionalSlotAddCommand<?>> nonFunctionalAddingCommands;
	protected ArrayList<NonFunctionalSlotRemoveCommand<?>> nonFunctionalRemovingCommands;

	/** The status of the concept */
	private Status status;

	/** Anonymous */
	private boolean anonymous;

	private static HashMap<String, Long> anonIds = new HashMap<String, Long>();

	private ArrayList<ResultsData> lastChangesCache = new ArrayList<ResultsData>();


	/** A lock to synchronize anonymous generations*/
	private static Object anonLock = new Object();

	/** Identifier generation for anonymous concepts */
	private static final Random anonRandom = new Random( System.currentTimeMillis() );

	/**
	 * Constructor without parameters.
	 * It is supposed that the recently created individual is anon
	 * @see http://en.wikipedia.org/wiki/Blank_node#Anonymous_classes_in_OWL
	 */
	public AbstractOntConcept() {
		this._setAnon(true);
		slots = new ArrayList<Slot<?>>();
		functionalChangeCommands = new ArrayList<FunctionalSlotChangeCommand<?>>();
		nonFunctionalAddingCommands = new ArrayList<NonFunctionalSlotAddCommand<?>>();
		nonFunctionalRemovingCommands = new ArrayList<NonFunctionalSlotRemoveCommand<?>>();
		latch = new Object();
		nsManager = new NamespaceManager();
		status = Status.UNPUBLISHED;
	}

	/**
	 * Inits the fields associated to a ontology concept
	 */
	public abstract void init();

	/**
	 * Constructor for named class instances.
	 * @param individualID the named class instance identifier
	 */
	public AbstractOntConcept(String individualID) {
		this.individualID = individualID;

		slots = new ArrayList<Slot<?>>();
		functionalChangeCommands = new ArrayList<FunctionalSlotChangeCommand<?>>();
		nonFunctionalAddingCommands = new ArrayList<NonFunctionalSlotAddCommand<?>>();
		nonFunctionalRemovingCommands = new ArrayList<NonFunctionalSlotRemoveCommand<?>>();
		latch = new Object();
		nsManager = new NamespaceManager();
		status = Status.UNPUBLISHED;
	}


	/**
	 * Establish the individual context
	 * @param individualPrefix the individual prefix
	 * @param individualNamespace the individual namespace
	 */
	protected void _setClassContext(String classPrefix, String classIRI) {
		this.classNamespace = IRIUtil.getNamespace(classIRI);
		this.classID = IRIUtil.getElement(classIRI);
		nsManager.addNsPrefix(classPrefix, classNamespace);

		// if no individual namespace is set, then use the class namespace
		if (individualNamespace == null) {
			this.individualNamespace = classNamespace;
		}
	}

	/**
	 * Gets the class namespace
	 * @return a String with named class namespace
	 */
	public String _getClassNamespace() {
		return classNamespace;
	}

	/**
	 * Gets the class prefix
	 * @return a String with named class prefix
	 */
	public String _getClassPrefix() {
		return nsManager.getPrefix(classNamespace);
	}

	/**
	 * Gets the class name
	 * @return a String with named class identifier
	 */
	public String _getClassID() {
		return classID;
	}

	/**
	 * Gets the class IRI
	 * @return a String with named class full iri
	 */
	public String _getClassIRI() {
		StringBuffer sb = new StringBuffer();
		sb.append(_getClassNamespace());
		sb.append(_getClassID());
		return sb.toString();
	}

	/**
	 * Establish the individual context
	 * @param individualPrefix the individual prefix
	 * @param individualNamespace the individual namespace
	 */
	protected void _setIndividualContext(String individualPrefix, String individualNamespace) {
		this.individualNamespace = individualNamespace;
		nsManager.addNsPrefix(individualPrefix, individualNamespace);
	}

	/**
	 * Establish the default individual context
	 */
	protected void _setDefaultIndividualContext() {
		this.individualNamespace = nsManager.getDefaultNamespace();
		nsManager.addNsPrefix(nsManager.getDefaultIndividualPrefix(), individualNamespace);
	}

	/**
	 * Establish the individual context
	 * @param individualPrefix the individual prefix
	 * @param individualNamespace the individual namespace
	 */
	public void _setIndividualIRI(String individualIRI) {
		if (individualIRI != null) {
			this.individualNamespace = IRIUtil.getNamespace(individualIRI);
			this.individualID = IRIUtil.getElement(individualIRI);
		} else {
			this._setAnon(true);
		}
	}

	/**
	 * Establish the individual context
	 * @param individualPrefix the individual prefix
	 * @param individualNamespace the individual namespace
	 */
	public void _setIndividualID(String individualID) {
		if (individualID != null) {
			this.individualID = individualID;
		}

		if (this.individualNamespace == null) {
			this._setDefaultIndividualContext();
		}

	}

	/**
	 * Gets the class individual identifier
	 * @return a String with named class individual namespace
	 */
	public String _getIndividualNamespace() {
		return individualNamespace;
	}

	/**
	 * Gets the class individual identifier
	 * @return a String with named class individual prefix
	 */
	public String _getIndividualPrefix() {
		return nsManager.getPrefix(individualNamespace);
	}

	/**
	 * Gets the class individual identifier
	 * @return a String with named class individual identifier
	 */
	public String _getIndividualID() {
		if (!this._isAnon()) { // it not anonymous/blank node, return the individual identity
			return individualID;
		}

		synchronized(anonLock) {
			if (this.individualID == null) { // if not set, generate a new one
				// the pattern is "_" + ClassName + autogenerated id
				StringBuilder sb = new StringBuilder();

				final String anonClassID = this._getClassID();
				sb.append("_").append(anonClassID);
				final long id;
				final Long knowId = anonIds.get(anonClassID);
				if (knowId != null) {
					id = knowId<Long.MAX_VALUE ? (knowId+1) : Long.MIN_VALUE;
				} else {
					id = anonRandom.nextLong();
				}

				sb.append(id);
				anonIds.put(anonClassID, id); // update reference
				this.individualID = sb.toString();
			}
		}

		return individualID;
	}

	/**
	 * Gets the class individual IRI
	 * @return a String with named class individual iri
	 */
	public String _getIndividualIRI() {
		StringBuffer sb = new StringBuffer();
		sb.append(_getIndividualNamespace());
		sb.append(_getIndividualID());
		return sb.toString();
	}

	/**
	 * Gets the individual prefix mapping
	 * @return the individual prefix mapping
	 */
	public Map<String,String> _getIndividualPrefixMap() {
		return nsManager.getNsPrefixMap();
	}

	/**
	 * Returns if the class individual is a consumer
	 * @return <code>true</code> if the class individual is a consumer
	 */
	protected boolean _isConsumer() {
		return (this instanceof KPConsumer);
	}

	/**
	 * Returns if the class individual is a producer
	 * @return <code>true</code> if the class individual is a producer
	 */
	protected boolean _isProducer() {
		return (this instanceof KPProducer);
	}

	/**
	 * Adds a property defined in the ontology class
	 * @param slot an slot associated to the ontology class
	 */
	public void _addProperty(Slot<?> slot) {
		synchronized(latch) {
			if (slots.contains(slot)) {
				slots.remove(slot);
			}
			slots.add(slot); // adds to the slot list
			nsManager.addNsPrefix(slot._getPrefix(), slot._getNamespace()); // adds the object ns
		}
	}

	/**
	 * Adds a functional change command to the class.
	 * The change commands are generated to be deferred in time
	 * and executed after the response from the SIB is successfully received.
	 * @param command an FunctionalSlotChangeCommand command to be added
	 */
	private void _addCommand(FunctionalSlotChangeCommand<?> command) {
		synchronized(latch) {
			// if there was another pending change command, the previous one is removed
			FunctionalSlotChangeCommand<?> removedCommand = null;
			for (FunctionalSlotChangeCommand<?> changeCommand : functionalChangeCommands) {
				if (changeCommand.getSlot()._getIRI().equals(command.getSlot()._getIRI())) {
					removedCommand = changeCommand;
				}
			}

			if (removedCommand != null) {
				functionalChangeCommands.remove(removedCommand);
			}

			// adds the change command
			functionalChangeCommands.add(command);

			// something requested to be changed in this AbstractOntConcept
			this.status = Status.OUTDATED;
		}
	}

	/**
	 * Adds a collection adding command to the class.
	 * The adding commands are generated to be deferred in time
	 * and executed after the response from the SIB is successfully received.
	 *
	 * @param command an NonFunctionalSlotAddCommand command to be added
	 */
	private void _addCommand(NonFunctionalSlotAddCommand<?> command) {
		synchronized(latch) {
			nonFunctionalAddingCommands.add(command);

			// something requested to be changed in this AbstractOntConcept
			this.status = Status.OUTDATED;
		}
	}

	/**
	 * Adds a collection removing command to the class.
	 * The remove commands are generated to be deferred in time
	 * and executed after the response from the SIB is successfully received.
	 *
	 * @param command an NonFunctionalSlotRemoveCommand command to be added
	 */
	private void _addCommand(NonFunctionalSlotRemoveCommand<?> command) {
		synchronized(latch) {
			nonFunctionalRemovingCommands.add(command);

			// something requested to be changed in this AbstractOntConcept
			this.status = Status.OUTDATED;
		}
	}

	/**
	 * Gets a property given a property name
	 * @param propertyName the property name
	 * @return the Slot corresponding to the given property. <code>null</code> if
	 * no properties are found with the given name
	 */
	public Slot<?> _getProperty(String propertyName) {
		for (Slot<?> slot : slots) {
			if (slot.getName().equals(propertyName)) {
				return slot;
			}
		}
		return null;
	}

	/**
	 * Gets a functional property given a property name
	 * @param propertyName the property name
	 * @return the FunctionalSlot corresponding to the given property. <code>null</code> if
	 * no functional properties are found with the given name
	 */
	@SuppressWarnings("unchecked")
	public <T> FunctionalSlot<T> _getFunctionalProperty(String propertyName) {
		for (Slot<?> slot : slots) {
			if (slot instanceof FunctionalSlot<?>) {
				if (slot.getName().equals(propertyName)) {
					return (FunctionalSlot<T>)slot;
				}
			}
		}
		return null;
	}

	/**
	 * Gets a non-functional property given a property name
	 * @param propertyName the property name
	 * @return the NonFunctionalSlot corresponding to the given property. <code>null</code> if
	 * no non-functional properties are found with the given name
	 */
	@SuppressWarnings("unchecked")
	public <T> NonFunctionalSlot<T> _getNonFunctionalProperty(String propertyName) {
		for (Slot<?> slot : slots) {
			if (slot instanceof NonFunctionalSlot<?>) {
				if (slot.getName().equals(propertyName)) {
					return (NonFunctionalSlot<T>)slot;
				}
			}
		}
		return null;
	}

	/**
	 * Determines if the AbstractOntConcept has associated functional change commands
	 * @return <code>true</code> if has at least one functional change command
	 */
	protected boolean hasFunctionalChangeCommands() {
		for (FunctionalSlotChangeCommand<?> command : functionalChangeCommands) {
			if ((command.getSlot().getValue() != null) && (command.getValue() != null)) { // why we have include this?
				return true;
			}
		}
		return false;
	}

	/**
	 * Determines if the AbstractOntConcept has associated functional change commands
	 * @return <code>true</code> if has at least one functional change command
	 */
	protected boolean hasFunctionalInsertCommands() {
		for (FunctionalSlotChangeCommand<?> command : functionalChangeCommands) {
			if ((command.getSlot().getValue() == null) && (command.getValue() != null)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Determines if the AbstractOntConcept has associated functional change commands
	 * @return <code>true</code> if has at least one functional change command
	 */
	protected boolean hasFunctionalDeletionCommands() {
		for (FunctionalSlotChangeCommand<?> command : functionalChangeCommands) {
			if (command.getValue() == null) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Determines if the AbstractOntConcept has associated non-functional adding commands
	 * @return <code>true</code> if has at least one non-functional adding command
	 */
	protected boolean hasNonFunctionalAddingCommands() {
		return nonFunctionalAddingCommands.size() > 0;
	}

	/**
	 * Determines if the AbstractOntConcept has associated non-functional removing commands
	 * @return <code>true</code> if has at least one non-functional removing command
	 */
	protected boolean hasNonFunctionalRemovingCommands() {
		return nonFunctionalRemovingCommands.size() > 0;
	}

	/**
	 * Determines if the AbstractOntConcept has associated insert commands
	 * @return <code>true</code> if has associated insert commands
	 */
	protected boolean hasPendingInsertCommands() {
		return (hasNonFunctionalAddingCommands() || hasFunctionalInsertCommands());
	}

	/**
	 * Determines if the AbstractOntConcept if has associated removal commands
	 * @return <code>true</code> if has associated removal commands
	 */
	protected boolean hasPendingRemovalCommands() {
		return hasFunctionalDeletionCommands() || hasNonFunctionalRemovingCommands();
	}

	/**
	 * Get the list of (functional) change commands
	 * @return a collection of functional slot change command
	 */
	protected Collection<FunctionalSlotChangeCommand<?>> listChangeCommands() {
		return functionalChangeCommands;
	}

	@Override
	@Deprecated
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this._getIndividualID()).append("[").append(this._getClassID()).append("]\n");

		if (slots.size() > 0) {
			sb.append("props:\n");
		}
		sb.append("Slots:\n");
		for(Slot<?> slot : slots) {
			if (slot.hasContent()) {
				sb.append("\t[<").append(this._getIndividualID()).append("> ");
				sb.append(slot.getName());
				sb.append(" ");
				if (slot instanceof FunctionalSlot<?>) {
					FunctionalSlot<?> fSlot = (FunctionalSlot<?>)slot;
					sb.append(fSlot.getValue());
				} else if (slot instanceof NonFunctionalSlot<?>) {
					NonFunctionalSlot<?> cSlot = (NonFunctionalSlot<?>)slot;
					sb.append(cSlot.getValues());
				}
				sb.append("]\n");
			}
		}

//		sb.append(", pending command=");
		if (this.hasFunctionalChangeCommands()) {
			sb.append("Commands:\n");
			for(FunctionalSlotChangeCommand<?> scc : functionalChangeCommands) {
				sb.append("\t[<").append(this._getIndividualID()).append("> ");
				sb.append(scc.getSlot().getName());
				sb.append(" ");
				sb.append(scc.getSlot().getValue());
				sb.append("->");
				sb.append(scc.getValue());
				sb.append("]\n");
			}
		}

		return sb.toString();
	}

	/**
	 * Sets the current object to be published
	 */
	public void _setPublishRequested() {
		if ((this.status == Status.UNPUBLISHED) || (this.status == Status.OUTDATED)) {
			this.status = Status.PUBLISHREQUESTED;
		}
	}

	/**
	 * Sets the status of the current object as published in the SIB.
	 * This means that when one concept is published, the ones that depends on the root
	 * concept are also labeled as 'published'.
	 */
	public void _setPublished() {
		this.status = Status.PUBLISHED;

		// set publisehd all the object properties associated to current aoc
		for (Slot<?> slot : this._listSlots()) {
			if (slot.hasContent() && slot.isObjectProperty()) {
				if (slot instanceof FunctionalObjectSlot<?>) {
					AbstractOntConcept concept = ((FunctionalObjectSlot<?>)slot).getValue();
					if ((concept != null) && !concept._isPublished()) {
						concept._setPublished();
					}
				} else if (slot instanceof NonFunctionalObjectSlot<?>) {
					for (AbstractOntConcept concept : ((NonFunctionalObjectSlot<?>)slot).listValues()) {
						if ((concept != null) && !concept._isPublished()) {
							concept._setPublished();
						}
					}
				}
			}
		}
	}

	/**
	 * Sets the status of the current object as invalid
	 */
	public void _setInvalidated() {
		this.status = Status.INVALIDATED;
	}

	/**
	 * Sets the status of the current object as unpublished
	 */
	public void _setUnpublished() {
		this.status = Status.UNPUBLISHED;
	}

	/**
	 * Sets the status of the current object as unpublished
	 */
	public void _setOutdated() {
		this.status = Status.OUTDATED;
	}

	/**
	 * Indicates if the status of this object is 'published in the SIB'
	 */
	public boolean _isPublished() {
		return this.status == Status.PUBLISHED;
	}


	/**
	 * Indicates if the status of this object is OUTDATED (=has local changes that need to be published to the SIB)
	 */
	public boolean _isOutdated() {
		return this.status == Status.OUTDATED;
	}

	/**
	 * Indicates if the status of this concept is UNPUBLISHED (=has not ever been published to the SIB)
	 */
	public boolean _isUnpublished() {
		return this.status == Status.UNPUBLISHED;
	}

	/**
	 * Indicates if the status of this concept is INVALIDATED
	 * (=this instance is not part of any SIB [any more])
	 * TODO it might be a smart move to allow clients to read the state directly with _getState
	 */
	public boolean _isInvalid() {
		return this.status == Status.INVALIDATED;
	}

	/**
	 * Changes the value of a given property. If the KP is in a model, the update
	 * is postponed until publish method of SmoolModel is called and the response
	 * (if requested) is received
	 */
	protected <T> void updateAttribute(String name, T newValue) {

        FunctionalSlot<T> slot = this._getFunctionalProperty(name);
        if(this._isPublished() || this._isOutdated()) {
        	if (slot.isObjectProperty() && (slot.getValue() != null) && (newValue != null) && slot.getValue().equals(newValue)) {
        		return;
        	}
	        FunctionalSlotChangeCommand<T> scc = new FunctionalSlotChangeCommand<T>(slot, newValue);
        	this._addCommand(scc);
			this.status = Status.OUTDATED;
        } else if(this._isInvalid()) {
        	Logger.warn( "AOC potentially invalid (updated by query or subscription?)" +
        			". Can also happen when re-inserting a previously removed AOC instnace" + this._getIndividualIRI());
        } else {
        	slot.setValue(newValue);
        }
	}

	/**
	 * If exists the attribute name, adds to the list of values
	 * @param attributeName the attribute name
	 * @param attributeValue the attribute value to add
	 * @return <code>true</code> if the attribute is correctly added
	 */
	protected <T> void addAttribute(String attributeName, T attributeValue) {
		NonFunctionalSlot<T> slot = this._getNonFunctionalProperty(attributeName);
		if(this._isPublished() || this._isOutdated()) {
			NonFunctionalSlotAddCommand<T> command = new NonFunctionalSlotAddCommand<T>(slot, attributeValue);
			this._addCommand(command);
			this.status = Status.OUTDATED;
		} else if(this._isInvalid()) {
        	Logger.warn( "AOC potentially invalid (updated by query or subscription?)" +
        			". Can also happen when re-inserting a previously removed AOC instnace" + this._getIndividualIRI());
		} else {
			slot.addValue(attributeValue);
		}
	}

	/**
	 * If exists the value as part of the values of attribute, removes it
	 * @param attributeName the attribute name
	 * @param attributeValue the attribute value to remove
	 * @return <code>true</code> if the attribute is correctly removed
	 */
	protected <T> void removeAttribute(String attributeName, T attributeValue) {
		NonFunctionalSlot<T> slot = this._getNonFunctionalProperty(attributeName);
		if(this._isPublished() || this._isOutdated()) {
			NonFunctionalSlotRemoveCommand<T> command = new NonFunctionalSlotRemoveCommand<T>(slot, attributeValue);
			this._addCommand(command);
			this.status = Status.OUTDATED;
		} else if(this._isInvalid()) {
        	Logger.warn( "AOC potentially invalid (updated by query or subscription?)" +
        			". Can also happen when re-inserting a previously removed AOC instnace" + this._getIndividualIRI());
		} else {
			slot.removeValue(attributeValue);
		}
	}

	/**
	 * Determines if exists one attribute value
	 * @param attributeName the attribute name
	 * @param attributeValue the attribute value to remove
	 * @return <code>true</code> if the attribute is part of the values of the attribute
	 */
	protected <T> boolean containsAttribute(String attributeName, T attributeValue) {
		try {
			NonFunctionalSlot<T> slot = this._getNonFunctionalProperty(attributeName);
			return slot.contains(attributeValue);
		} catch (Exception ex) {
			return false;
		}
	}

	/**
	 * Lists the attribute values
	 * @param attributeName the attribute name
	 * @return a collection of values for the given attribute. returns <code>null</code> if the property does not exists.
	 */
	protected <T> Collection<T> listAttributes(String attributeName) {
		try {
			NonFunctionalSlot<T> slot = this._getNonFunctionalProperty(attributeName);
			return slot.getValues();
		} catch (Exception ex) {
			return null;
		}
	}

	/**
	 * Gets the iterator associated to a list
	 * @param attributeName the attribute name
	 * @return an iterator over values for the given attribute. returns <code>null</code> if the property does not exists.
	 */
	protected <T> Iterator<T> attributeIterator(String attributeName) {
		try {
			NonFunctionalSlot<T> slot = this._getNonFunctionalProperty(attributeName);
			return slot.getValues().iterator();
		} catch (Exception ex) {
			return null;
		}
	}

	/**
	 * Get the list of non-functional object slots
	 * @return a collection of non-functional object slots
	 */
	protected Collection<NonFunctionalObjectSlot<?>> _listCollectionObjectSlots() {
		ArrayList<NonFunctionalObjectSlot<?>> nonFunctionalSlots = new ArrayList<NonFunctionalObjectSlot<?>>();
		for(Slot<?> slot : slots) {
			if (slot instanceof NonFunctionalObjectSlot<?>) {
				nonFunctionalSlots.add((NonFunctionalObjectSlot<?>)slot);
			}
		}

		return nonFunctionalSlots;
	}

	/**
	 * Get the list of functional object slots
	 * @return a collection of functional object slots
	 */
	protected Collection<FunctionalObjectSlot<?>> _listFunctionalObjectSlots() {
		ArrayList<FunctionalObjectSlot<?>> functionalSlots = new ArrayList<FunctionalObjectSlot<?>>();
		for(Slot<?> slot : slots) {
			if (slot instanceof FunctionalObjectSlot<?>) {
				functionalSlots.add((FunctionalObjectSlot<?>)slot);
			}
		}
		return functionalSlots;
	}

	/**
	 * Get the list of slots
	 * @param direct if only gets direct slot objects (<code>true</code>) or not
	 * @return a collection of functional object slots
	 */
	protected Collection<? extends AbstractOntConcept> _listSlotObjects(boolean direct) {
		ArrayList<AbstractOntConcept> slotConcepts = new ArrayList<AbstractOntConcept>();

		for(Slot<?> slot : slots) {
			if (slot instanceof FunctionalObjectSlot<?>) {
				AbstractOntConcept concept = ((FunctionalObjectSlot<?>)slot).getValue();
				if (concept != null) {
					if (!slotConcepts.contains(concept)) { // stop criteria: the concept is already included
						slotConcepts.add(concept);
						if (!direct) {
							slotConcepts.addAll(concept._listSlotObjects(direct));
						}
					}
				}
			} else if (slot instanceof NonFunctionalObjectSlot<?>) {
				for (AbstractOntConcept concept : ((NonFunctionalObjectSlot<?>)slot).listValues()) {
					if (concept != null) {
						if (!slotConcepts.contains(concept)) {
							slotConcepts.add(concept);
							if (!direct) {
								slotConcepts.addAll(concept._listSlotObjects(direct));
							}
						}
					}
				}
			}
		}

		return slotConcepts;
	}

	/**
	 * Implementation of comparable interface
	 * @param concept the concept to compare
	 */
	@Override
	public int compareTo(IAbstractOntConcept concept) {
		return this._getIndividualIRI().compareTo(((AbstractOntConcept) concept)._getIndividualIRI());
	}

	/**
	 * Implementation of equals interface
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof AbstractOntConcept) {
			//return this._getIndividualIRI().equals(((AbstractOntConcept)obj)._getIndividualIRI());
			AbstractOntConcept that = (AbstractOntConcept)obj;
			return this._getIndividualNamespace().equals( that._getIndividualNamespace() )
				&& this._getIndividualID().equals( that._getIndividualID() );
		} else {
			return false;
		}
	}

	/**
	 * Gets the has code of this concept
	 * @return the hash code of this
	 */
	@Override
	public int hashCode() {
		return _getIndividualIRI().hashCode();
	}

	/**
	 * Executes the commands associated to this class
	 */
	public void executeCommands() {
		synchronized(latch) {
			for (FunctionalSlotChangeCommand<?> command : functionalChangeCommands) {
				command.run();
			}

			for (NonFunctionalSlotAddCommand<?> command : nonFunctionalAddingCommands) {
				command.run();
			}

			for (NonFunctionalSlotRemoveCommand<?> command : nonFunctionalRemovingCommands) {
				command.run();
			}

			 // clear commands after execution
			functionalChangeCommands.clear();
			nonFunctionalAddingCommands.clear();
			nonFunctionalRemovingCommands.clear();

			// Deep into the commands of the children
			for(Slot<?> slot : slots) {
				if (slot instanceof FunctionalObjectSlot<?>) {
					AbstractOntConcept concept = ((FunctionalObjectSlot<?>)slot).getValue();
					if ((concept != null) && concept.hasPendingExecutionCommands()) {
						concept.executeCommands();
					}
				} else if (slot instanceof NonFunctionalObjectSlot<?>) {
					for (AbstractOntConcept concept : ((NonFunctionalObjectSlot<?>)slot).listValues()) {
						if ((concept != null) && concept.hasPendingExecutionCommands()) {
							concept.executeCommands();
						}
					}
				}
			}
		}
	}

	/**
	 * Determines if there are pending commands to execute
	 * @return <code>true</code> if there are pending commands to execute
	 */
	public boolean hasPendingCommands() {
		return this.hasFunctionalChangeCommands() || this.hasNonFunctionalAddingCommands() || this.hasNonFunctionalRemovingCommands();
	}

	/**
	 * Determines if there are pending commands to execute
	 * @return <code>true</code> if there are pending commands to execute
	 */
	public boolean hasPendingExecutionCommands() {
		return this.hasPendingCommands(false) || this.hasFunctionalInsertCommands() || this.hasFunctionalDeletionCommands();
	}

	/**
	 * Determines if there are pending commands to execute
	 * @return <code>true</code> if there are pending commands to execute
	 */
	public boolean hasPendingCommands(boolean direct) {
		if (!direct) {
			for(Slot<?> slot : slots) {
				if (slot instanceof FunctionalObjectSlot<?>) {
					AbstractOntConcept concept = ((FunctionalObjectSlot<?>)slot).getValue();
					if ((concept != null) && concept.hasPendingCommands(false)) {
						return true;
					}
				} else if (slot instanceof NonFunctionalObjectSlot<?>) {
					for (AbstractOntConcept concept : ((NonFunctionalObjectSlot<?>)slot).listValues()) {
						if ((concept != null) && concept.hasPendingCommands(false)) {
							return true;
						}
					}
				}
			}
		}
		return this.hasFunctionalChangeCommands() || this.hasFunctionalInsertCommands() || this.hasFunctionalDeletionCommands()
			|| this.hasNonFunctionalAddingCommands() || this.hasNonFunctionalRemovingCommands();
	}

	/**
	 * Determines if there are pending commands to execute
	 * @return <code>true</code> if there are pending commands to execute
	 */
	public boolean hasPendingUpdateCommands() {
		return this.hasFunctionalChangeCommands() || (this.hasNonFunctionalAddingCommands() && this.hasNonFunctionalRemovingCommands());
	}

	/**
	 * Gets the set of concepts that contains the parametrized concept as object property
	 * @param concept an AbstractOntConcept
	 * @return a list of objects that contains this property
	 */
	protected Collection<? extends AbstractOntConcept> _listObjectsContainingObjectProperty(AbstractOntConcept concept) {

		ArrayList<AbstractOntConcept> concepts = new ArrayList<AbstractOntConcept>();

		for(Slot<?> slot : slots) {
			if (slot instanceof FunctionalObjectSlot<?>) {
				AbstractOntConcept currentConcept = ((FunctionalObjectSlot<?>)slot).getValue();
				if ((currentConcept != null) && !currentConcept.equals(this)) {
					if (currentConcept.equals(concept)) {
						concepts.add(this);
					}

					if (!currentConcept.equals(concept)) {
						concepts.addAll(currentConcept._listObjectsContainingObjectProperty(concept));
					}
				}
			} else if (slot instanceof NonFunctionalObjectSlot<?>) {
				for (AbstractOntConcept currentConcept : ((NonFunctionalObjectSlot<?>)slot).listValues()) {
					if ((currentConcept != null) && !currentConcept.equals(this)) {
						if (currentConcept.equals(concept)) {
							concepts.add(this);
						}
					}
				}
			}
		}

		return concepts;
	}

	/**
	 * Gets the list of concepts that has some attached commands
	 * @return a collection of AbstractOntConcepts
	 */
	protected Collection<? extends AbstractOntConcept> _listObjectsContainingCommands(boolean direct) {
		ArrayList<AbstractOntConcept> commandConcepts = new ArrayList<AbstractOntConcept>();

		for(Slot<?> slot : slots) {
			if (slot instanceof FunctionalObjectSlot<?>) {
				AbstractOntConcept concept = ((FunctionalObjectSlot<?>)slot).getValue();
				if (concept != null) {
					if (concept.hasPendingCommands()) {
						commandConcepts.add(concept); // adds as a concept to update if exists some direct pending command
					}

					if (!direct) {
						commandConcepts.addAll(concept._listObjectsContainingCommands(direct));
					}
				}
			} else if (slot instanceof NonFunctionalObjectSlot<?>) {
				for (AbstractOntConcept concept : ((NonFunctionalObjectSlot<?>)slot).listValues()) {
					if (concept != null) {
						if (concept.hasPendingCommands()) {
							commandConcepts.add(concept);
						}

						if (!direct) {
							commandConcepts.addAll(concept._listObjectsContainingCommands(direct));
						}
					}
				}
			}
		}

		return commandConcepts;
	}

	/**
	 * Gets the list of concepts that has some attached commands
	 * @return a collection of AbstractOntConcepts
	 */
	protected Collection<? extends AbstractOntConcept> _listObjectsContainingInsertCommands(boolean direct) {
		ArrayList<AbstractOntConcept> commandConcepts = new ArrayList<AbstractOntConcept>();

		for(Slot<?> slot : slots) {
			if (slot instanceof FunctionalObjectSlot<?>) {
				AbstractOntConcept concept = ((FunctionalObjectSlot<?>)slot).getValue();
				if (concept != null) {
					if (concept.hasPendingInsertCommands() && !concept.hasPendingUpdateCommands()) {
						commandConcepts.add(concept);
					}
					if (!direct) {
						commandConcepts.addAll(concept._listObjectsContainingInsertCommands(direct));
					}
				}
			} else if (slot instanceof NonFunctionalObjectSlot<?>) {
				for (AbstractOntConcept concept : ((NonFunctionalObjectSlot<?>)slot).listValues()) {
					if (concept != null) {
						if (concept.hasPendingInsertCommands() && !concept.hasPendingUpdateCommands()) {
							commandConcepts.add(concept);
						}
						if (!direct) {
							commandConcepts.addAll(concept._listObjectsContainingInsertCommands(direct));
						}
					}
				}
			}
		}

		return commandConcepts;
	}

	/**
	 * Gets the list of concepts that has some attached commands
	 * @return a collection of AbstractOntConcepts
	 */
	protected Collection<? extends AbstractOntConcept> _listObjectsContainingRemoveCommands(boolean direct) {
		ArrayList<AbstractOntConcept> commandConcepts = new ArrayList<AbstractOntConcept>();

		for(Slot<?> slot : slots) {
			if (slot instanceof FunctionalObjectSlot<?>) {
				AbstractOntConcept concept = ((FunctionalObjectSlot<?>)slot).getValue();
				if (concept != null) {
					if (concept.hasPendingRemovalCommands() && !concept.hasPendingUpdateCommands()) {
						commandConcepts.add(concept);
					}
					if (!direct) {
						commandConcepts.addAll(concept._listObjectsContainingRemoveCommands(direct));
					}
				}
			} else if (slot instanceof NonFunctionalObjectSlot<?>) {
				for (AbstractOntConcept concept : ((NonFunctionalObjectSlot<?>)slot).listValues()) {
					if (concept != null) {
						if (concept.hasPendingRemovalCommands() && !concept.hasPendingUpdateCommands()) {
							commandConcepts.add(concept);
						}
						if (!direct) {
							commandConcepts.addAll(concept._listObjectsContainingRemoveCommands(direct));
						}
					}
				}
			}
		}

		return commandConcepts;
	}

	/**
	 * Gets the list of functional change commands
	 * @return a set of functional change commands associated to the current concept
	 */
	protected Collection<? extends AbstractOntConcept> _listFunctionalDerivedRemovedConcepts() {
		ArrayList<AbstractOntConcept> derivedRemovedConcepts = new ArrayList<AbstractOntConcept>();

		synchronized(latch) {
			for (FunctionalSlotChangeCommand<?> removeCommand : functionalChangeCommands) {
				if ((removeCommand.getValue() == null) && (removeCommand.getSlot() != null) && removeCommand.getSlot().isObjectProperty()) {
					derivedRemovedConcepts.add((AbstractOntConcept)removeCommand.getSlot().getValue());
				}
			}

		}
		return derivedRemovedConcepts;
	}



	/**
	 * Get the list of slots
	 * @return a collection of functional object slots
	 */
	protected Collection<? extends AbstractOntConcept> _listUnpublishedUpdateSlotObjects(boolean direct) {
		ArrayList<AbstractOntConcept> slotConcepts = new ArrayList<AbstractOntConcept>();

		if (this.hasPendingUpdateCommands()) {
			for (FunctionalSlotChangeCommand<?> command : this.listChangeCommands()) {
				Slot<?> slot = command.getSlot();
				if (slot.isObjectProperty()) {
					if (slot instanceof FunctionalObjectSlot<?>) {
						AbstractOntConcept concept = (AbstractOntConcept)command.getValue();
						if ((concept != null) && (concept._isUnpublished())) {
							if (!slotConcepts.contains(concept)) {
								slotConcepts.add(concept);
								if (!direct) {
									slotConcepts.addAll(concept._listUnpublishedSlotObjects(direct));
								}
							}
						}
					}
				}
			}

			// adds the pending unpublished data
			for (NonFunctionalSlotAddCommand<?> command : this._listNonFunctionalAddingCommands()) {
				Slot<?> slot = command.getSlot();
				if (slot.isObjectProperty()) {
					if (slot instanceof NonFunctionalObjectSlot<?>) {
						AbstractOntConcept concept = (AbstractOntConcept)command.getValue();
						if ((concept != null) && (!concept._isPublished())) {
							if (!slotConcepts.contains(concept)) {
								slotConcepts.add(concept);
								if (!direct) {
									slotConcepts.addAll(concept._listUnpublishedSlotObjects(direct));
								}
							}
						}
					}
				}
			}
		}

		return slotConcepts;
	}

	/**
	 * Get the list of slots
	 * @return a collection of functional object slots
	 */
	private Collection<? extends AbstractOntConcept> _listUnpublishedSlotObjects(boolean direct) {
		ArrayList<AbstractOntConcept> slotConcepts = new ArrayList<AbstractOntConcept>();

		for (Slot<?> slot : this._listSlots()) {
			if (slot.isObjectProperty()) {
				if (slot instanceof FunctionalObjectSlot<?>) {
					FunctionalObjectSlot<?> foslot = (FunctionalObjectSlot<?>) slot;
					AbstractOntConcept concept = foslot.getValue();
					if ((concept != null) && (!concept._isPublished())) {
						if (!slotConcepts.contains(concept)) {
							slotConcepts.add(concept);
							if (!direct) {
								slotConcepts.addAll(concept._listUnpublishedSlotObjects(direct));
							}
						}
					}
				} else if (slot instanceof NonFunctionalObjectSlot<?>){
					NonFunctionalObjectSlot<?> nfoslot = (NonFunctionalObjectSlot<?>) slot;
					for (AbstractOntConcept concept : nfoslot.listValues()) {
						if ((concept != null) && (!concept._isPublished())) {
							if (!slotConcepts.contains(concept)) {
								slotConcepts.add(concept);
								if (!direct) {
									slotConcepts.addAll(concept._listUnpublishedSlotObjects(direct));
								}
							}
						}
					}
				}
			}
		}

		return slotConcepts;
	}


	/**
	 * Get the list of slots
	 * @return a collection of functional object slots
	 */
	protected Collection<? extends AbstractOntConcept> _listPublishedRemoveSlotObjects(boolean direct) {
		ArrayList<AbstractOntConcept> slotConcepts = new ArrayList<AbstractOntConcept>();

		if (this.hasPendingUpdateCommands()) {
			for (FunctionalSlotChangeCommand<?> command : this.listChangeCommands()) {
				Slot<?> slot = command.getSlot();
				if (slot.isObjectProperty()) {
					if (slot instanceof FunctionalObjectSlot<?>) {
						FunctionalObjectSlot<?> foslot = (FunctionalObjectSlot<?>) slot;
						AbstractOntConcept concept = foslot.getValue();
						if ((concept != null) && (concept._isPublished())) {
							if (!slotConcepts.contains(concept)) {
								slotConcepts.add(concept);
								if (!direct) {
									slotConcepts.addAll(concept._listPublishedSlotObjects(direct));
								}
							}
						}
					}
				}
			}

			// adds the pending unpublished data
			for (NonFunctionalSlotRemoveCommand<?> command : this._listNonFunctionalRemovingCommands()) {
				Slot<?> slot = command.getSlot();
				if (slot.isObjectProperty()) {
					if (slot instanceof NonFunctionalObjectSlot<?>) {
						AbstractOntConcept concept = (AbstractOntConcept)command.getValue();
						if ((concept != null) && (!concept._isPublished())) {
							if (!slotConcepts.contains(concept)) {
								slotConcepts.add(concept);
								if (!direct) {
									slotConcepts.addAll(concept._listUnpublishedSlotObjects(direct));
								}
							}
						}
					}
				}
			}

		}

		return slotConcepts;
	}

	/**
	 * Get the list of slots published slots of a given concept
	 * @return a collection of functional object slots
	 */
	private Collection<? extends AbstractOntConcept> _listPublishedSlotObjects(boolean direct) {
		ArrayList<AbstractOntConcept> slotConcepts = new ArrayList<AbstractOntConcept>();

		for (Slot<?> slot : this._listSlots()) {
			if (slot.isObjectProperty()) {
				if (slot instanceof FunctionalObjectSlot<?>) {
					FunctionalObjectSlot<?> foslot = (FunctionalObjectSlot<?>) slot;
					AbstractOntConcept concept = foslot.getValue();
					if ((concept != null) && (concept._isPublished())) {
						if (!slotConcepts.contains(concept)) {
							slotConcepts.add(concept);
							if (!direct) {
								slotConcepts.addAll(concept._listPublishedSlotObjects(direct));
							}
						}
					}
				} else if (slot instanceof NonFunctionalObjectSlot<?>){
					NonFunctionalObjectSlot<?> nfoslot = (NonFunctionalObjectSlot<?>) slot;
					for (AbstractOntConcept concept : nfoslot.listValues()) {
						if ((concept != null) && (concept._isPublished())) {
							if (!slotConcepts.contains(concept)) {
								slotConcepts.add(concept);
								if (!direct) {
									slotConcepts.addAll(concept._listPublishedSlotObjects(direct));
								}
							}
						}
					}
				}
			}
		}

		return slotConcepts;
	}

	/**
	 * Get the list of slots
	 * @return a collection of functional object slots
	 */
	protected Collection<? extends AbstractOntConcept> _listUnpublishedInsertSlotObjects(boolean direct) {
		ArrayList<AbstractOntConcept> slotConcepts = new ArrayList<AbstractOntConcept>();

		if (this.hasPendingInsertCommands()) {
			for (NonFunctionalSlotAddCommand<?> command : this._listNonFunctionalAddingCommands()) {
				Slot<?> slot = command.getSlot();
				if (slot.isObjectProperty()) {
					if (slot instanceof NonFunctionalObjectSlot<?>) {
						AbstractOntConcept concept = (AbstractOntConcept)command.getValue();
						if ((concept != null) && (!concept._isPublished())) {
							if (!slotConcepts.contains(concept)) {
								slotConcepts.add(concept);
								// TODO To be tested
//								if (!direct) {
//									slotConcepts.addAll(concept._listUnpublishedSlotObjects(direct));
//								}
							}
						}
					}
				}
			}
		}

		return slotConcepts;
	}
	/**
	 * Gets the SPARQL-query representation of this object
	 * @return a String representing a full-sparql-query
	 */
	public String toSPARQLQuery() {
		StringBuilder sbSPARQL = new StringBuilder();
		sbSPARQL.append(nsManager.toSPARQLPrefixMapping());

		StringBuilder sbSelectClause = new StringBuilder();
		sbSelectClause.append("SELECT ?classId ");
		for (Slot<?> slot : slots) {
			sbSelectClause.append("?");
			sbSelectClause.append(slot.getName());
			if (slot.isObjectProperty()) {
				sbSelectClause.append("_classId");
			}
			sbSelectClause.append(" ");
		}

		sbSelectClause.append("\n");

		StringBuilder sbWhereClause = new StringBuilder();
		sbWhereClause.append("WHERE {");

		sbWhereClause.append("?classId rdf:type ");
		sbWhereClause.append(this._getClassPrefix());
		sbWhereClause.append(":");
		sbWhereClause.append(this._getClassID());

		for (Slot<?> slot : slots) {
			sbWhereClause.append(" .\n");
			sbWhereClause.append("OPTIONAL {?classId ");
			sbWhereClause.append(slot._getPrefix()).append(":").append(slot.getName());
			sbWhereClause.append(" ");
			sbWhereClause.append("?").append(slot.getName());
			if (slot.isObjectProperty()) {
				sbWhereClause.append("_classId");
				if (slot.isFunctionalProperty()) {
					FunctionalObjectSlot<? extends AbstractOntConcept> foslot = (FunctionalObjectSlot<?>) slot;
					sbWhereClause.append(foslot.getWhereClauses());
					sbSelectClause.append(foslot.getSelectVariables());
				} else if (!slot.isFunctionalProperty()) {
					NonFunctionalObjectSlot<? extends AbstractOntConcept> nfoslot = (NonFunctionalObjectSlot<?>) slot;
					sbWhereClause.append(nfoslot.getWhereClauses());
					sbSelectClause.append(nfoslot.getSelectVariables());
				}
			}
			sbWhereClause.append("}");
		}

		sbWhereClause.append("}");

		sbSPARQL.append(sbSelectClause.toString());
		sbSPARQL.append("\n");
		sbSPARQL.append(sbWhereClause.toString());


		return sbSPARQL.toString();

	}

	/**
	 * Gets the variables associated to an Abstract Ont Concept
	 * @return a String representing a sparql-query variables associated to an ontology concept
	 */
	public String toSPARQLSELECTClause(String slotName) {
		StringBuilder sb = new StringBuilder();
		for (Slot<?> slot : slots) {
			sb.append(" .\n");
			sb.append("\t?").append(slotName).append("_classId").append(" ");
			sb.append(slot._getPrefix()).append(":").append(slot.getName());
			sb.append(" ");
			sb.append("?").append(slotName).append("_").append(slot.getName());
			if (slot.isObjectProperty() && slot.isFunctionalProperty()) {
				FunctionalObjectSlot<? extends AbstractOntConcept> foslot = (FunctionalObjectSlot<?>) slot;
				sb.append(foslot.getWhereClauses());
			}
		}

		return sb.toString();
	}

	/**
	 * Gets the variables associated to an Abstract Ont Concept
	 * @return a String representing a sparql-query variables associated to an ontology concept
	 */
	public String toVariables(String slotName) {
		StringBuilder sb = new StringBuilder();
		for (Slot<?> slot : slots) {
			sb.append(" ");
			sb.append("?").append(slotName).append("_").append(slot.getName());
			if (slot.isObjectProperty() && slot.isFunctionalProperty()) {
				FunctionalObjectSlot<? extends AbstractOntConcept> foslot = (FunctionalObjectSlot<?>) slot;
				sb.append(foslot.getSelectVariables());
			}
		}

		return sb.toString();
	}

	/**
	 * Get the actual type arguments a child class has used to extend a generic base class.
	 *
	 * @param baseClass the base class
	 * @param childClass the child class
	 * @return a list of the raw classes for the actual type arguments.
	 */
	public static <T> List<Class<?>> getTypeArguments(
		Class<T> baseClass, Class<? extends T> childClass) {
	    Map<Type, Type> resolvedTypes = new HashMap<Type, Type>();
	    Type type = childClass;
	    // start walking up the inheritance hierarchy until we hit baseClass
	    while (!getClass(type).equals(baseClass)) {
	    	if (type instanceof Class<?>) {
	    		// there is no useful information for us in raw types, so just keep going.
	    		type = ((Class<?>) type).getGenericSuperclass();
	    	} else {
	    		ParameterizedType parameterizedType = (ParameterizedType) type;
	    		Class<?> rawType = (Class<?>) parameterizedType.getRawType();
	    		Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
	    		TypeVariable<?>[] typeParameters = rawType.getTypeParameters();
	    		for (int i = 0; i < actualTypeArguments.length; i++) {
	    			resolvedTypes.put(typeParameters[i], actualTypeArguments[i]);
	    		}

	    		if (!rawType.equals(baseClass)) {
	    			type = rawType.getGenericSuperclass();
	    		}
	    	}
	    }

	    // finally, for each actual type argument provided to baseClass, determine (if possible)
	    // the raw class for that type argument.
	    Type[] actualTypeArguments;
	    if (type instanceof Class<?>) {
	    	actualTypeArguments = ((Class<?>) type).getTypeParameters();
	    } else {
	    	actualTypeArguments = ((ParameterizedType) type).getActualTypeArguments();
	    }

	    List<Class<?>> typeArgumentsAsClasses = new ArrayList<Class<?>>();
	    // resolve types by chasing down type variables.
	    for (Type baseType: actualTypeArguments) {
	    	while (resolvedTypes.containsKey(baseType)) {
	    		baseType = resolvedTypes.get(baseType);
	    	}

	    	typeArgumentsAsClasses.add(getClass(baseType));
	    }
	    return typeArgumentsAsClasses;
	}

	/**
	 * Get the underlying class for a type, or null if the type is a variable type.
	 * @param type the type
	 * @return the underlying class
	 */
	public static Class<?> getClass(Type type) {
		if (type instanceof Class<?>) {
			return (Class<?>) type;
	    } else if (type instanceof ParameterizedType) {
	      return getClass(((ParameterizedType) type).getRawType());
	    } else if (type instanceof GenericArrayType) {
	    	Type componentType = ((GenericArrayType) type).getGenericComponentType();
	    	Class<?> componentClass = getClass(componentType);
	    	if (componentClass != null ) {
	    		return Array.newInstance(componentClass, 0).getClass();
	    	} else {
	    		return null;
	    	}
	    } else {
	    	return null;
	    }
	}

	/**
	 * Gets the returned class
	 * @return
	 */
	public Class<?> returnedClass() {
		return getTypeArguments(AbstractOntConcept.class, getClass()).get(0);
	}

	/**
	 * Gets the RDF-M3 first query
	 * @return
	 */
	public String toRDFM3Query() {
		StringBuilder sb = new StringBuilder();
		sb.append("<triple_list xmlns:rdfs='http://www.w3.org/2000/01/rdf-schema#'>\n");
		sb.append("<triple>");
		sb.append("<subject>sib:any</subject>");
		sb.append("<predicate>rdfs:subClassOf</predicate>");
		sb.append("<object type='URI'>");
		sb.append(this._getClassIRI());
		sb.append("</object>");
		sb.append("</triple>");
		sb.append("</triple_list>");

		return sb.toString();
	}

	/**
	 * Copies all the information of other abstract ont concept
	 * @param otherAOC the other abstract ont concept
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		if (this != null) {
			AbstractOntConcept clone = (AbstractOntConcept) super.clone();

			clone.nsManager.addNsPrefixMap(this._getIndividualPrefixMap());
			clone.slots = new ArrayList<Slot<?>>();
			try {
				for (Slot<?> slot : this.slots) {
					clone.slots.add(slot.clone());
				}
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
			return clone;
		} else {
			return null;
		}
	}

	/**
	 * Copies all the information of other abstract ont concept
	 * @param otherAOC the other abstract ont concept
	 */
	public void copyFrom(AbstractOntConcept otherAOC) {
		this.nsManager.addNsPrefixMap(otherAOC._getIndividualPrefixMap());
		this.slots = new ArrayList<Slot<?>>();
		this._cloneSlots(otherAOC._listSlots());
	}

	/**
	 * Clones the slots
	 * @param slotList
	 */
	private void _cloneSlots(ArrayList<? extends Slot<?>> slotList) {
		this.slots = new ArrayList<Slot<?>>();

		try {
			for (Slot<?> slot : slotList) {
				this.slots.add(slot.clone());
			}
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * list all the properties associated to the current abstract ont concept
	 * @return the list of properties associated to the current concept
	 */
	public ArrayList<? extends Slot<?>> _listSlots() {
		return slots;
	}

	/**
	 * Gets the list of functional change commands
	 * @return a set of functional change commands associated to the current concept
	 */
	public Collection<FunctionalSlotChangeCommand<?>> _listFunctionalChangeCommands() {
		return functionalChangeCommands;
	}

	/**
	 * Gets the list of non-functional add commands
	 * @return a set of non-functional add commands associated to the current concept
	 */
	public Collection<NonFunctionalSlotAddCommand<?>> _listNonFunctionalAddingCommands() {
		return nonFunctionalAddingCommands;
	}

	/**
	 * Gets the list of non-functional remove commands
	 * @return a set of non-functional remove commands associated to the current concept
	 */
	public Collection<NonFunctionalSlotRemoveCommand<?>> _listNonFunctionalRemovingCommands() {
		return nonFunctionalRemovingCommands;
	}

	/**
	 * Sets if the current AbstractOntConcept is a blank node / anonymous node
	 * @param anon the value to set
	 */
	private void _setAnon(boolean anon) {
		this.anonymous = anon;
		if (anon) {
			//this.individualNamespace = null;
			this.individualID = null;
		}
	}

	/**
	 * Gets if the current AbstractOntConcept is a blank node / anonymous node
	 * @return <code>true</code> if the current AbstractOntConcept is a blank node
	 */
	public boolean _isAnon() {
		return anonymous;
	}

	/**
	 * Gets if the current AbstractOntConcept is a blank node / anonymous node
	 * @return <code>true</code> if the current AbstractOntConcept is a blank node
	 */
	public boolean _isBNode() {
		return _isAnon();
	}

	//-------------------------------------------------------------------------
 	/**
	 * Attempt for a 'work-around' for https://sg1.esilab.org/trac/smool/ticket/175
	 *
	 * WARNIGN: This is a quick-fix. In no way do I suggest that this is a good solution
	 * or that it should be kept!
	 *
	 * Idea: In the conceptUpdated() method of a subscription
	 * (after a hot-fix of Fran contained in this class/SmarModel), the
	 * newConcept is NOT registered in the smart model, the obsolete concept is.
	 *
	 * Now the subscription-KP becomes responsible for updating the oldConcept instance
	 * (that it already knows about and my use elsewhere in the KP) with the new
	 * values. This method attempts doing that.
	 *
	 * Then annoying thing is, that a KP also needs to do this update after each query.
	 *
	 * @param other
	 * @see SmartModel#getSingletonConcept(AbstractOntConcept)
	 * @author hartmut
	 */
	public void updateFrom(AbstractOntConcept other) {
		if (this.equals(other)) { // equality means that they have the same identification iri
			synchronized(latch) {
				slots = other.slots;
				functionalChangeCommands = other.functionalChangeCommands;
				nonFunctionalAddingCommands = other.nonFunctionalAddingCommands;
				nonFunctionalRemovingCommands = other.nonFunctionalRemovingCommands;
			}
		}
	}



	/**
	 * Gets the current value for a given property
	 * @param <T> the property type
	 * @param attributeName the attribute name
	 * @return the current value for a given property
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	//@SuppressWarnings({ "unchecked" })
	public <T> T getCurrentValue( final String attributeName ) {
		for (FunctionalSlotChangeCommand command : functionalChangeCommands) {
			final FunctionalSlot<T> slot = command.getSlot();
			if(slot.getName().equals( attributeName ) && (slot.getValue() != null)) {
				return (T) command.getValue();
			}
		}
		return (T) _getFunctionalProperty( attributeName ).getValue();
	}

	/**
	 * Gets the list of current attribute values
	 * @param <T>
	 * @param attributeName
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	//@SuppressWarnings({ "unchecked" })
	public <T> List<T> listCurrentValues(String attributeName) {
		NonFunctionalSlot<T> slot = this._getNonFunctionalProperty(attributeName);
		final ArrayList<T> currentList = new ArrayList( slot.getValues() );

		for (NonFunctionalSlotAddCommand<?> command : nonFunctionalAddingCommands) {
			final NonFunctionalSlot<?> nfSlot = command.getSlot();
			if(nfSlot.getName().equals( attributeName )) {
				currentList.add( (T) command.getValue() );
	        }
		}

		for (NonFunctionalSlotRemoveCommand<?> command : nonFunctionalRemovingCommands) {
			final NonFunctionalSlot<?> nfSlot = command.getSlot();
			if(nfSlot.getName().equals( attributeName )) {
				currentList.remove( command.getValue() );
			}
		}
		return currentList;
	}

	//-------------------------------------------------------------------------
	/**
	 * Converts a regular {@link Date} to the XMLGregorian type used in ADK
	 *
	 * @param date	the date to convert
	 * @return
	 */
	private static ObjectPool<GregorianCalendar>calendars = new ObjectPool<GregorianCalendar>(GregorianCalendar.class);
	public final static javax.xml.datatype.XMLGregorianCalendar dateToXmlGregorian( Date date ) {
		GregorianCalendar calendar = calendars.getInstance();
		calendar.setTime( date );
		try {
			return DatatypeFactory.newInstance().newXMLGregorianCalendar( calendar );
		} catch (DatatypeConfigurationException e) {
			Logger.error( "Serious internal error in dateToXmlGregorian" + e.getMessage() );
			throw new UnsupportedOperationException( e );
		} finally {
			calendars.freeInstance( calendar );
		}
	}

	//-------------------------------------------------------------------------
	/**
	 * Converts an XMLGregorian type to a regular {@link Date}
	 * @param xmlc
	 * @return
	 */
	public final static Date fromXmlGregorian(javax.xml.datatype.XMLGregorianCalendar xmlc) {
		return xmlc.toGregorianCalendar().getTime();
	}

	/**
	 * @return the lastChangesCache
	 */
	public ArrayList<ResultsData> getLastChangesCache() {
		return lastChangesCache;
	}

	/**
	 * @param lastChangesCache the lastChangesCache to set
	 */
	public void setLastChangesCache(ArrayList<ResultsData> lastChangesCache) {
		this.lastChangesCache = lastChangesCache;
	}

	/**
	 * A method to print on file or console the contents of the lastChangesCache attribute. Thus, a logic layer user can have a quick look at the last changes in the AOC
	 *
	 */
	public void showLastChanges(){

		//get the
		ArrayList<ResultsData> results = this.getLastChangesCache();

		Iterator<ResultsData> it = results.iterator();

		//Iterate the list of ResultsData objects
		while (it.hasNext()){

			//Get the current ResultsProperty in the list
			ArrayList<ResultsProperty> p = it.next().listProperties();
			Iterator<ResultsProperty> it2 = p.iterator();

			//Iterate through the ResultsProperty objects
			while (it2.hasNext()){
				//Get the current ResultsProperty object
				ResultsProperty r = it2.next();
				//Get both the collections of new and old values and print them
				Collection<String> newVal = r.listNewValues();
				Collection<String> oldVal = r.listObsoleteValues();
				Iterator<String> ov = oldVal.iterator();
				Logger.info("List of old properties values :");
				Logger.info("===============================");
				while (ov.hasNext()){
					Logger.info(ov.next());
				}
				Iterator<String> nv = newVal.iterator();
				Logger.info("List of new properties values :");
				Logger.info("===============================");
				while (nv.hasNext()){
					Logger.info(nv.next());
				}
			}
		}
	}


	/**
	 * This method will return a collection of the latest changes occurred in the enclosing AOC
	 * @return 		A HashMap with pairs String-String, each having a pair of old-value, new-value.
	 * 				If the new value does not exist, the VALUE will be REMOVED since it means that
	 * 				an old value (KEY) has been removed. Vice-versa, if an old value does not exist,
	 * 				the KEY will be NEW since it means that a new concept has been added. Existing
	 * 				KEY-VALUE pairs mean actual modifications
	 *
	 *
	 */
	public HashMap<String, HashMap<String, String>> getLastChanges(){

		HashMap<String, HashMap<String, String>> ret = new HashMap<String, HashMap<String, String>>();


		//get the latests changes
		ArrayList<ResultsData> results = this.getLastChangesCache();

		Iterator<ResultsData> it = results.iterator();

		//Iterate the list of ResultsData objects
		while (it.hasNext()){

			HashMap<String, String> values = new HashMap<String, String>();

			ResultsData rd = it.next();

			//Get the indvidualIRI of the ResultsData current object
			String individualIRI = rd.getIndividualIRI();

			//Get the current ResultsProperty list
			ArrayList<ResultsProperty> p = rd.listProperties();
			Iterator<ResultsProperty> it2 = p.iterator();

			//Iterate through the ResultsProperty objects
			while (it2.hasNext()){


				String ovalue = new String();
				String nvalue = new String();
				String value = new String();

				//Get the current ResultsProperty object
				ResultsProperty r = it2.next();
				//Get the propertyIRI from the ResultsProperty current object
				String propertyIRI = r.getPropertyIRI();

				//Get both the collections of new and old values and print them
				Collection<String> newVal = r.listNewValues();
				Collection<String> oldVal = r.listObsoleteValues();

				Iterator<String> ov = oldVal.iterator();
				if (!ov.hasNext()) {
	                ovalue = "ADDED";
                }
                else {
	                ovalue = ov.next();
                }

				Iterator<String> nv = newVal.iterator();
				if (!nv.hasNext()) {
	                nvalue = "REMOVED";
                }
                else {
	                nvalue = nv.next();
                }

				if (ovalue.equalsIgnoreCase("ADDED")){
					value = ovalue;
				}
				else{
					if (nvalue.equalsIgnoreCase("REMOVED")){
						value = nvalue;
					}
					else{
						value = "MODIFIED";
					}

				}

				//Add a new entry in the HashMap of values
				values.put(propertyIRI, value);

			}

			//Add a new entry in the HashMap to be returned
			ret.put(individualIRI, values);
		}

		return ret;
	}


}
