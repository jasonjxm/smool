/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.model.smart.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;

public class XSDUtils {
	
	static final String HEXES = "0123456789ABCDEF";
	
	/**
	 * Gets the Hex representation for a given byte array
	 * @param raw a byte array
	 * @return the string representation of that byte array
	 */
	public static String getHex( byte [] raw ) {
	    if ( raw == null ) {
	      return null;
	    }
	    final StringBuilder hex = new StringBuilder( 2 * raw.length );
	    for ( final byte b : raw ) {
	      hex.append(HEXES.charAt((b & 0xF0) >> 4))
	         .append(HEXES.charAt((b & 0x0F)));
	    }
	    return hex.toString();
	  }

	/**
	 * Decodes a String into a byte array format
	 * @param s the string
	 * @return the byte representation of the string
	 */
	public static byte[] hexStringToByteArray(String s) {
		byte[] b = new byte[s.length() / 2];
		for (int i = 0; i < b.length; i++){
			int index = i * 2;
		    int v = Integer.parseInt(s.substring(index, index + 2), 16);
		    b[i] = (byte)v;
		}
		return b;
	}
	
	public Object getXSDObject(String value) {
		 
		if (!isXSDType(value)) {
			value = transformToXSD(value);
		}
		
		if (isString(value)) {
			return getXSDValue(value);
		} else if (isInt(value)) {
			return getInt(getXSDValue(value));
		} else if (isDateTime(value)) {
			return getDateTime(getXSDValue(value));
		 } else if (isByteArray(value)) {
			 return getByteArray(getXSDValue(value));
		 } else if (isLong(value)) {
			 return getLong(getXSDValue(value));
		 } else if (isShort(value)) {
			 return getShort(getXSDValue(value));
		 } else if (isFloat(value)) {
			 return getFloat(getXSDValue(value));
		 } else if (isDouble(value)) {
			 return getDouble(getXSDValue(value));
		 } else if (isBoolean(value)) {
			 return getBoolean(getXSDValue(value));
		 } else if (isByte(value)) {
			 return getByte(getXSDValue(value));
		 } else if (isBigDecimal(value)) {
			 return getBigDecimal(getXSDValue(value));
		 } else if (isBigInteger(value)) {
			 return getBigInteger(getXSDValue(value));
		 } else if (isQName(value)) {
			 return getQName(getXSDValue(value));
		 } else if (isDuration(value)) {
			 return getDuration(getXSDValue(value));
		 } else {
			 return getObject(getXSDValue(value));
		 }
	}

	private boolean isXSDType(String value) {
		if (!value.contains("xsd:")) {
			return false;
		}
		return true;
	}

	private String transformToXSD(String value) {
		return value.replace("http://www.w3.org/2001/XMLSchema#", "xsd:");
	}

	
	private boolean isString(String value) {
		if (value.endsWith("^^xsd:string") || value.endsWith("^^xsd:anySimpleType")) {
			return true;
		}
		return false;
	}

	private String getXSDValue(String value) {
		String returnValue = value.substring(0, value.indexOf("^^"));
		return returnValue;
	}

	private boolean isInt(String value) {
		if (value.endsWith("^^xsd:int") || value.endsWith("^^xsd:unsignedShort")) {
			return true;
		}
		return false;
	}

	private int getInt(String xsdValue) {
		try {
			Integer value = new Integer(xsdValue);
			return value.intValue();
		} catch (NumberFormatException nfe) {
			nfe.printStackTrace();
		}
		return 0;
	}

	private boolean isDateTime(String value) {
		if (value.endsWith("^^xsd:dateTime") || value.endsWith("^^xsd:time") ||
				value.endsWith("^^xsd:date") || value.endsWith("^^xsd:g")) {
			return true;
		}
		return false;
	}

	private XMLGregorianCalendar getDateTime(String xsdValue) {
		try {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
			String rfc822TZ = xsdValue.substring(0, xsdValue.lastIndexOf(":")) + xsdValue.substring(xsdValue.lastIndexOf(":")+1);
			Date date = df.parse(rfc822TZ);			
			GregorianCalendar gcal = new GregorianCalendar();
			gcal.setTime(date);
			XMLGregorianCalendar fec = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcal);

			return fec;
		} catch (ParseException pex) {
			pex.printStackTrace();
		} catch (DatatypeConfigurationException dtce) {
			dtce.printStackTrace();
		}
		
		return null;
	}

	private boolean isByteArray(String value) {
		if (value.endsWith("^^xsd:base64Binary") || value.endsWith("^^xsd:hexBinary")) {
			return true;
		}
		return false;
	}

	private byte[] getByteArray(String xsdValue) {
		try {
			return xsdValue.getBytes();
		} catch (NumberFormatException nfe) {
			nfe.printStackTrace();
		}
		return null;
	}

	private boolean isLong(String value) {
		if (value.endsWith("^^xsd:long") || value.endsWith("^^xsd:unsignedInt")) {
			return true;
		}
		return false;
	}

	private long getLong(String xsdValue) {
		try {
			Long value = new Long(xsdValue);
			return value.longValue();
		} catch (NumberFormatException nfe) {
			nfe.printStackTrace();
		}
		return 0;
	}

	private boolean isShort(String value) {
		if (value.endsWith("^^xsd:short") || value.endsWith("^^xsd:unsignedByte")) {
			return true;
		}
		return false;
	}

	private short getShort(String xsdValue) {
		try {
			Short value = new Short(xsdValue);
			return value.shortValue();
		} catch (NumberFormatException nfe) {
			nfe.printStackTrace();
		}
		return 0;
	}

	private boolean isFloat(String value) {
		if (value.endsWith("^^xsd:float")) {
			return true;
		}
		return false;
	}

	private float getFloat(String xsdValue) {
		try {
			Float value = new Float(xsdValue);
			return value.floatValue();
		} catch (NumberFormatException nfe) {
			nfe.printStackTrace();
		}
		return 0;
	}
	
	private boolean isDouble(String value) {
		if (value.endsWith("^^xsd:double")) {
			return true;
		}
		return false;
	}

	private double getDouble(String xsdValue) {
		try {
			return Double.parseDouble(xsdValue);
		} catch (NumberFormatException nfe) {
			nfe.printStackTrace();
		}
		return 0;
	}
	
	private boolean isBoolean(String value) {
		if (value.endsWith("^^xsd:boolean")) {
			return true;
		}
		return false;
	}

	private boolean getBoolean(String xsdValue) {
		try {
			Boolean value = new Boolean(xsdValue);
			return value.booleanValue();
		} catch (NumberFormatException nfe) {
			nfe.printStackTrace();
		}
		return false;
	}	

	private boolean isByte(String value) {
		if (value.endsWith("^^xsd:byte")) {
			return true;
		}
		return false;
	}

	private byte getByte(String xsdValue) {
		try {
			Byte value = new Byte(xsdValue);
			return value.byteValue();
		} catch (NumberFormatException nfe) {
			nfe.printStackTrace();
		}
		return 0;
	}
	
	private boolean isBigInteger(String value) {
		if (value.endsWith("^^xsd:integer")) {
			return true;
		}
		return false;
	}

	private BigInteger getBigInteger(String xsdValue) {
		try {
			BigInteger value = new BigInteger(xsdValue);
			return value;
		} catch (NumberFormatException nfe) {
			nfe.printStackTrace();
		}
		return null;
	}	

	private boolean isBigDecimal(String value) {
		if (value.endsWith("^^xsd:decimal")) {
			return true;
		}
		return false;
	}

	private BigDecimal getBigDecimal(String xsdValue) {
		try {
			BigDecimal value = new BigDecimal(xsdValue);
			return value;
		} catch (NumberFormatException nfe) {
			nfe.printStackTrace();
		}
		return null;
	}
	
	private boolean isQName(String value) {
		if (value.endsWith("^^xsd:QName") || value.endsWith("^^xsd:NOTATION")) {
			return true;
		}
		return false;
	}

	private QName getQName(String xsdValue) {
		try {
			QName value = new QName(xsdValue);
			return value;
		} catch (NumberFormatException nfe) {
			nfe.printStackTrace();
		}
		return null;
	}		

	private boolean isDuration(String value) {
		if (value.endsWith("^^xsd:duration")) {
			return true;
		}
		return false;
	}

	private Duration getDuration(String xsdValue) {
		try {
			DatatypeFactory factory = DatatypeFactory.newInstance();
			Duration value = factory.newDuration(xsdValue);
			return value;
		} catch (DatatypeConfigurationException nfe) {
			nfe.printStackTrace();
		}
		return null;
	}
	
	private Object getObject(String xsdValue) {
		Object obj = new Object();
		return obj;
	}

	public static String getRawContent(String stringRepresentation) {
		String rawString = stringRepresentation;
		if (rawString.contains("^^")) {
			rawString = rawString.substring(0, rawString.indexOf("^^"));
		}
		return rawString;
	}	

}
