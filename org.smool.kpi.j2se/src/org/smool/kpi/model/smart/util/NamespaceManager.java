/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.model.smart.util;

import java.util.HashMap;
import java.util.Map;

/**
 * This helper class allows the storage of namespaces defined for a given
 * ontology.
 * 
 * @author Fran Ruiz, fran.ruiz@tecnalia.com, Tecnalia-Software
 *
 */
public class NamespaceManager {

	/** The namespace prefix mapping for supporting ontologies*/
	private HashMap<String,String> supportingOntologies;
	
	/** The default namespace */
	private String defaultNamespace;
	private String defaultIndividualPrefix;
	private String ontologyNamespace;
	private String ontologyPrefix;
	
	public static final String OWL_PREFIX = "owl";
	public static final String OWL_NAMESPACE = "http://www.w3.org/2002/07/owl#" ;

	public static final String RDF_PREFIX = "rdf";
	public static final String RDF_NAMESPACE = "http://www.w3.org/1999/02/22-rdf-syntax-ns#" ;
	
	public static final String RDFS_PREFIX = "rdfs";
	public static final String RDFS_NAMESPACE = "http://www.w3.org/2000/01/rdf-schema#" ;
	
	public static final String XSD_PREFIX = "xsd";
	public static final String XSD_NAMESPACE = "http://www.w3.org/2001/XMLSchema#";
	
	private static final String SMOOL_DEFAULT_NAMESPACE = "http://www.smool.org/ontology/individual#";
	private static final String SMOOL_DEFAULT_INDIVIDUAL_PREFIX = "individual";
	
	/**
	 * The constructor
	 */
	public NamespaceManager() {
		 this.supportingOntologies = new HashMap<String,String>();
		 this.setDefaultNamespace(SMOOL_DEFAULT_NAMESPACE);
		 this.defaultIndividualPrefix = SMOOL_DEFAULT_INDIVIDUAL_PREFIX;
		 this.ontologyPrefix = null;
		 this.ontologyNamespace = null;
	}
	
	/**
	 * Constructor with default namespace spec
	 * @param defaultNS the default namespace
	 */
	public NamespaceManager(String defaultNS) {
		 this.supportingOntologies = new HashMap<String,String>();		
		 this.setDefaultNamespace(defaultNS);
		 this.ontologyPrefix = null;
		 this.ontologyNamespace = null;
	}
	
	/**
	 * Constructor with default namespace spec
	 * @param defaultNS the default namespace
	 */
	public NamespaceManager(String basePrefix, String baseNS) {
		 this.supportingOntologies = new HashMap<String,String>();		
		 this.ontologyPrefix = basePrefix;
		 this.ontologyNamespace = baseNS;
		 this.setDefaultNamespace(baseNS); // the default namespace is set to the base namespace
	}	

	/**
	 * Gets the OWL Vocabulary namespace prefix mapping
	 * @return a map with the owl vocabulary namespace mapping
	 */
	public Map<String,String> getOWLVocabularyNsPrefixMap() {
		HashMap<String,String> owlNamespaces = new HashMap<String,String>();

		owlNamespaces.put(OWL_PREFIX, OWL_NAMESPACE);
		owlNamespaces.put(RDF_PREFIX, RDF_NAMESPACE);
		owlNamespaces.put(RDFS_PREFIX, RDFS_NAMESPACE);
		owlNamespaces.put(XSD_PREFIX, XSD_NAMESPACE);
		
		return owlNamespaces;
		
	}
	
	/**
	 * Gets the namespace prefix mapping
	 * @return a map with the namespace prefix mapping
	 */
	public Map<String,String> getSupportingOntologiesNsPrefixMap() {
		return supportingOntologies;
	}
	
	/**
	 * Adds a namespace prefix to the list of prefixes
	 * @param prefix the prefix string
	 * @param namespace the namespace corresponding to the indicated prefix
	 */
	public void addSupportingOntologyPrefixNS(String prefix, String namespace) {
		supportingOntologies.put(prefix, namespace);
	}
	
	/**
	 * Sets the DEFAULT and BASE namespace prefix  
	 * @param prefix the prefix to be used as default
	 * @param namespace the default namespace (it will be associated to the given prefix
	 * @see http://www.w3.org/TR/1999/REC-xml-names-19990114/
	 */
	public void setBasePrefixNs(String prefix, String namespace) {
		// The base namespace
		this.ontologyNamespace = namespace;
		this.ontologyPrefix = prefix;
	}
	
	/**
	 * Gets the base prefix
	 * @return the base prefix for the ontology  
	 */
	public String getBasePrefix() {
		return this.ontologyPrefix;
	}

	/**
	 * Gets the base namespace
	 * @return the base namespace for the ontology  
	 */
	public String getBaseNamespace() {
		return this.ontologyNamespace;
	}
	
	/**
	 * Determines if the ontology has set a base namespace and prefix 
	 * @return <code>true</code> if the base namespace and prefix is set
	 */
	public boolean hasBaseNsPrefix() {
		return (ontologyPrefix != null && ontologyNamespace != null);
	}	
	
	/**
	 * Sets the default namespace for the current ontology
	 * @param defaultNamespace the default namespace to set
	 */
	public void setDefaultNamespace(String defaultNamespace) {
		this.defaultNamespace = defaultNamespace;
		if (this.ontologyPrefix != null) { // modify the ontology namespace with the default one
			this.ontologyNamespace = defaultNamespace;
		}
	}

	/**
	 * Gets the default namespace for the current ontology
	 * @return the default namespace
	 */
	public String getDefaultNamespace() {
		return defaultNamespace;
	}
	
	/**
	 * Gets the default prefix associated for individual
	 * @return the default prefix for individuals
	 */
	public String getDefaultIndividualPrefix() {
		return defaultIndividualPrefix;
	}	
	
	/**
	 * Determines if the ontology has set a default namespace 
	 * @return <code>true</code> if the default namespace is set
	 */
	public boolean hasDefaultNamespace() {
		return (defaultNamespace != null);
	}

	/**
	 * Attaches a prefix mapping to the current prefix mapping
	 * @param individualPrefixMap the individual prefix mapping
	 */
	public void addNsPrefixMap(Map<String, String> individualPrefixMap) {
		this.supportingOntologies.putAll(individualPrefixMap);		
	}

	/**
	 * Gets the prefix corresponding to the namespace
	 * @param namespace the namespace to find
	 * @return the namespace prefix. <code>null</code> if not found
	 */
	public String getPrefix(String namespace) {
		if (namespace == null) {
			return null;
		}
			
		if (this.ontologyNamespace != null && this.ontologyNamespace.equals(namespace)) {
			return this.ontologyPrefix;	
		}
		
		for (String key : supportingOntologies.keySet()) {
			if (supportingOntologies.get(key).equals(namespace)) {
				return key;
			}
		}
		
		return null;
	}	
	
	/**
	 * Determines if there are more supporting ontologies in namespace declaration
	 * @return <code>true</code> if there are additional ontology namespaces
	 */
	public boolean hasAdditionalOntologyNamespaces() {
		return supportingOntologies != null && supportingOntologies.size() > 0;
	}
	
	/**
	 * Gets the namespace prefix mapping
	 * @return a map with the namespace prefix mapping for the current ontology
	 */
	public Map<String,String> getNsPrefixMap() {
		Map<String,String> nsPrefixMap = new HashMap<String,String>();
		if (this.hasBaseNsPrefix()) {
			nsPrefixMap.put(this.getBasePrefix(), this.getBaseNamespace());
		}
		
		nsPrefixMap.putAll(supportingOntologies);
		return nsPrefixMap;
	}

	/**
	 * Adds a namespace to the current ontology
	 * @param prefix a prefix 
	 * @param namespace a namespace
	 */
	public void addNsPrefix(String prefix, String namespace) {
		if (this.hasBaseNsPrefix()) {
			if (this.getBasePrefix().equals(prefix) && this.getBaseNamespace().equals(namespace)) {
				this.supportingOntologies.put(prefix, namespace);
			}
		} 
		this.supportingOntologies.put(prefix, namespace);
		
	}	
	
//	/**
//	 * Gets the XML-RDF representation of the namespace set
//	 * @return a String representation in XML-RDF format
//	 */
//	public String toRDF() {
//		StringBuilder sb = new StringBuilder();
//
//		if (defaultNamespace != null) {
//			sb.append(writeBaseNamespace());
//		}
//		sb.append(writeNamespaces(ontologyNamespaces));
//		sb.append(writeNamespaces(getOWLVocabularyNsPrefixMap()));
//		return sb.toString();
//	}
//	
//	/**
//	 * Obtains the XML-RDF representation of namespace mapping
//	 * @param namespaces the namespace prefix mapping
//	 * @return a String representation in XML-RDF format
//	 */
//	private String writeNamespaces(Map<String,String> namespaces) {
//		StringBuilder sb = new StringBuilder();
//
//		for (String prefix : namespaces.keySet()) {
//			String namespace = namespaces.get(prefix);
//			sb.append("\n\txmlns:");
//			sb.append(prefix);
//			sb.append("='");
//			sb.append(namespace);
//			sb.append("'");
//		}
//		
//		return sb.toString();
//	}
//	
//	/**
//	 * Obtains the XML-RDF representation of default namespace mapping
//	 * @return a String representation in XML-RDF format
//	 */
//	private String writeBaseNamespace() {
//		StringBuilder sb = new StringBuilder();
//
//		sb.append("\nxmlns='");
//		sb.append(defaultNamespace);
//		sb.append("'\n");
//
//		sb.append("xml:base='");
//		sb.append(defaultNamespace);
//		sb.append("'");
//		return sb.toString();
//	}




	public String toSPARQLPrefixMapping() {
		StringBuffer sb = new StringBuffer();
		
		sb.append("PREFIX ").append(RDF_PREFIX);
		sb.append(":<").append(RDF_NAMESPACE);
		sb.append("> \n");
		
		
		if (this.hasBaseNsPrefix()) {
			if (this.getBasePrefix() != null && this.getBaseNamespace() != null) {
				sb.append("PREFIX ").append(this.getBasePrefix());
				sb.append(":<").append(this.getBaseNamespace());
				sb.append("> \n");				
			}
		} 

		for (String key : supportingOntologies.keySet()) {
			if (!key.equals("")) {
				sb.append("PREFIX ").append(key);
				sb.append(":<").append(supportingOntologies.get(key));
				sb.append("> \n");
			}
		}
		
		return sb.toString();
	}	
}
