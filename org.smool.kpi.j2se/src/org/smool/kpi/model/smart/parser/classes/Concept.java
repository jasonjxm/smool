package org.smool.kpi.model.smart.parser.classes;

import java.util.ArrayList;
import java.util.HashMap;

public class Concept implements Comparable<Concept> {
	private String id;

	private HashMap<String,ArrayList<String>> datatypeProperties;
	
	private HashMap<String,ArrayList<Concept>> objectProperties;
	private ArrayList<String> objectPropertyNames;
	
	public Concept(String id) {
		this.id = id;
		this.datatypeProperties = new HashMap<String,ArrayList<String>>();
		this.objectProperties = new HashMap<String,ArrayList<Concept>>();
		this.objectPropertyNames = new ArrayList<String>();
	}
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	@Override
	public int compareTo(Concept concept) {
		return this.getId().compareTo(concept.getId());
	}
	
	@Override
	public boolean equals(Object obj) {
		Concept concept = (Concept) obj;
		return this.getId().equals(concept.getId());
	}

	public boolean add(String property, String value) {
		if (datatypeProperties.get(property) == null) {
			ArrayList<String> values = new ArrayList<String>();
			values.add(value);
			datatypeProperties.put(property, values);
			return true;
		} else {
			ArrayList<String> values = datatypeProperties.get(property);
			if (!values.contains(value)) {
				values.add(value);
				datatypeProperties.put(property, values);
			}
			return true;
		}
	}

	private boolean isObjectPropertyValue(String property) {
		for (String objectPropertyName : objectPropertyNames) {
			if (property.startsWith(objectPropertyName + "_")) {
				if (!property.endsWith("_classId")) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean isObjectPropertyId(String property) {
		for (String objectPropertyName : objectPropertyNames) {
			if (property.startsWith(objectPropertyName + "_")) {
				if (property.endsWith("_classId")) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("OBJECT ").append(this.id).append("\n");
		for (String property : datatypeProperties.keySet()) {
			sb.append("Datatype property ").append(property).append(": ");
			for (String value : datatypeProperties.get(property)) {
				sb.append("[" + value + "],");
			}
			sb.append("\n");
		}
		for (String property : objectProperties.keySet()) {
			sb.append("Object property ").append(property).append(":\n");
			for (Concept value : objectProperties.get(property)) {
				sb.append("[").append(value.toString()).append("]\n");
			}
		}
		return sb.toString();
	}

	public void setObjectProperties(ArrayList<String> objectPropertyNames) {
		this.objectPropertyNames = objectPropertyNames;
	}

	public void createObjectProperties(HashMap<String, String> concept) {
		for (String objectPropertyName : objectPropertyNames) {
			String propertyValue = concept.get(objectPropertyName + "_classId");
			if (propertyValue != null) {
				Concept newConcept = new Concept(propertyValue);
				for (String propertyName : concept.keySet()) {
					if (propertyName.startsWith(objectPropertyName + "_")) {
						if (!isObjectPropertyId(propertyName)) {
							String suffix = propertyName.substring(propertyName.lastIndexOf("_")+1);
							newConcept.add(suffix, concept.get(propertyName));
						}
					}
				}
				if (objectProperties.get(objectPropertyName) == null) {
					ArrayList<Concept> concepts = new ArrayList<Concept>();
					concepts.add(newConcept);
					objectProperties.put(objectPropertyName, concepts);
				} else {
					ArrayList<Concept> concepts = objectProperties.get(objectPropertyName);
					if (!concepts.contains(newConcept)) {
						concepts.add(newConcept);
					}
					//objectProperties.put(objectPropertyName, concepts);
				}
			}
		}
	}

	public void assignConceptValues(HashMap<String, String> concept) {
		for (String propertyName : concept.keySet()) {
			if (!isObjectPropertyValue(propertyName) && !isObjectPropertyId(propertyName) &&
					!isClassId(propertyName)) {
				this.add(propertyName, concept.get(propertyName));
			}
		}
		
	}

	private boolean isClassId(String propertyName) {
		if (propertyName.equals("classId")) {
			return true;
		}
		return false;
	}

	public HashMap<String, ArrayList<String>> getDatatypeProperties() {
		return datatypeProperties;
	}
	
	public HashMap<String, ArrayList<Concept>> getObjectProperties() {
		return objectProperties;
	}
	
}
