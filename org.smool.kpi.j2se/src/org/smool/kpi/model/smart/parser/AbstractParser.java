/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.model.smart.parser;

import java.util.Collection;

import org.smool.kpi.model.exception.KPIModelException;
import org.smool.kpi.model.smart.parser.classes.Concept;


/**
 * This abstract parser is used to be implemented by all parsers that
 * decode subscriptions indications/confirm responses.
 * 
 * @author Fran Ruiz, Fran.Ruiz@tecnalia.com, Tecnalia
 *
 */
public abstract class AbstractParser {

	public abstract boolean validate(String results) throws KPIModelException;

	public abstract Collection<Concept> parse(String results) throws KPIModelException;
}
