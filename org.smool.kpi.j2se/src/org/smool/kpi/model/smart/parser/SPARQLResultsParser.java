/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.model.smart.parser;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;

import org.smool.kpi.model.exception.KPIModelException;
import org.smool.kpi.model.smart.parser.classes.Concept;
import org.smool.kpi.model.smart.parser.classes.ConceptUtil;


/**
 * This class implements the parser for SPARQL-Results in XML format.
 * 
 * @author Fran Ruiz, Fran.Ruiz@tecnalia.com, Tecnalia-Tecnalia
 *
 */
public class SPARQLResultsParser extends AbstractParser {

	private static final String TAG_SPARQL = "sparql";
	private static final String TAG_HEAD = "head";
	private static final String TAG_VARIABLE = "variable";
	private static final String ATT_NAME = "name";
	private static final String TAG_RESULTS = "results";
	private static final String TAG_RESULT = "result";
	private static final String TAG_BINDING = "binding";
	private static final String TAG_BNODE = "bnode";
	private static final String TAG_URI = "uri";
	private static final String TAG_LITERAL = "literal";
	private static final String ATT_DATATYPE = "datatype";
	private static final String ATT_LANG = "xml:lang";
	
	@Override
	public ArrayList<Concept> parse(String results) throws KPIModelException {
		ArrayList<HashMap<String,String>> conceptSet = new ArrayList<HashMap<String,String>>();
		
		System.out.println("Parsing results:\n" + results);
		
		ArrayList<Concept> concepts = new ArrayList<Concept>();
		
		try {
			ByteArrayInputStream bis = new ByteArrayInputStream(results.getBytes());
			
			// We will use StaX pull
			XMLInputFactory factory = XMLInputFactory.newInstance();
			XMLStreamReader reader = factory.createXMLStreamReader(bis);
			
			// START_DOCUMENT
			int eventType;
			
			while (reader.hasNext()) {
				eventType = reader.next();
				switch(eventType) {
				case XMLEvent.START_ELEMENT:
					if (reader.getLocalName().equals(TAG_SPARQL)) {
						//System.out.println("Reading Namespaces from SPARQL tag");
						// Gets the namespace
						this.getNamespaces(reader);
					} else if (reader.getLocalName().equals(TAG_HEAD)) {
						// Gets the namespace
						//System.out.println("Reading Variables from HEAD tag");
						this.getVariables(reader);
					} else if (reader.getLocalName().equals(TAG_RESULTS)) {
						// Gets the namespace
						//System.out.println("Reading results from RESULTS tag");
						conceptSet.addAll(this.getResults(reader));
					}
				}
			}
			
			//System.out.println("Done parsing");
		} catch (XMLStreamException ex) {
			ex.printStackTrace();
			throw new KPIModelException("Cannot parse sparql results due to " + ex.getMessage(), ex);
		} 
		
		ConceptUtil conceptUtil = new ConceptUtil();
		ArrayList<String> objectProperties = new ArrayList<String>();
		for(HashMap<String,String> concept : conceptSet) {
			objectProperties.addAll(conceptUtil.getObjectProperties(concept));
		}
		
		for(HashMap<String,String> concept : conceptSet) {
			String conceptId = concept.get("classId");
			Concept currentConcept = null;
			boolean alreadyDefined = false;
			for (Concept insertedConcept : concepts) {
				if (insertedConcept.getId().equals(conceptId)) {
					currentConcept = insertedConcept;
					alreadyDefined = true;
				}
			}
			if (currentConcept == null) {
				currentConcept = new Concept(conceptId);
			}
			
			currentConcept.setObjectProperties(objectProperties);
			
			currentConcept.createObjectProperties(concept);
			
			currentConcept.assignConceptValues(concept);
			
			if(!alreadyDefined) {
				concepts.add(currentConcept);
			}
		}
		
		return concepts;
	}
	
	/**
	 * Gets the variables associated to the SPARQL-results
	 * @param reader the XML Stream reader
	 */
	private void getVariables(XMLStreamReader reader) {
		int eventType = -1;
		try {
			do {
				eventType = reader.next();
				switch(eventType) {
				case XMLEvent.START_ELEMENT:
				
					if(reader.getLocalName().equals(TAG_VARIABLE)){
						// Gets the variables
						@SuppressWarnings("unused")
						String variable = this.getAttribute(reader, ATT_NAME);
						//System.out.println("Variable " + variable);
					}
				}
			} while(!reader.hasNext() || !(eventType == XMLStreamConstants.END_ELEMENT &&
					reader.getLocalName().equals(TAG_HEAD)));
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}
	}
		
	/**
	 * Gets the results associated to the SPARQL-results
	 * @param reader the XML Stream reader
	 */
	private Collection<HashMap<String,String>> getResults(XMLStreamReader reader) {
		int eventType = -1;
		ArrayList<HashMap<String,String>> conceptSet = new ArrayList<HashMap<String,String>>();
		
		try {
			do {
				eventType = reader.next();
				switch(eventType) {
				case XMLEvent.START_ELEMENT:
				
					if(reader.getLocalName().equals(TAG_RESULT)){
						// Gets the variables
						conceptSet.add(this.getResult(reader));
					}
				}
			} while(!reader.hasNext() || !(eventType == XMLStreamConstants.END_ELEMENT &&
					reader.getLocalName().equals(TAG_RESULTS)));
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}
		
		return conceptSet;
	}
		
	/**
	 * Gets one result associated to the SPARQL-results results tag
	 * @param reader the XML Stream reader
	 */
	private HashMap<String,String> getResult(XMLStreamReader reader) {
		int eventType = -1;
		
		HashMap<String,String> result = new HashMap<String,String>();
		
		try {
			do {
				eventType = reader.next();
				switch(eventType) {
				case XMLEvent.START_ELEMENT:
					if(reader.getLocalName().equals(TAG_BINDING)){
						String variable = getAttribute(reader, ATT_NAME);
						// Gets the variables
						String binding = getBinding(reader);
						result.put(variable, binding);
						//System.out.println(variable + " = " + binding);
					}
				}
			} while(!reader.hasNext() || !(eventType == XMLStreamConstants.END_ELEMENT &&
					reader.getLocalName().equals(TAG_RESULT)));
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * Gets the bindings associated to the SPARQL-results
	 * @param reader the XML Stream reader
	 */
	private String getBinding(XMLStreamReader reader) {
		int eventType = -1;
		StringBuffer sb = new StringBuffer();
		try {
			do {
				eventType = reader.next();
				switch(eventType) {
				case XMLEvent.START_ELEMENT:
					if(reader.getLocalName().equals(TAG_LITERAL)){
						sb.append(getLiteral(reader));
					} else if(reader.getLocalName().equals(TAG_URI)){
						sb.append(getURI(reader));
					} else if(reader.getLocalName().equals(TAG_BNODE)){
						sb.append(getBNode(reader));
					}
				}
			} while(!reader.hasNext() || !(eventType == XMLStreamConstants.END_ELEMENT &&
					reader.getLocalName().equals(TAG_BINDING)));
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}
		
		return sb.toString();
	}
	
	/**
	 * Gets the URI associated to the SPARQL-results
	 * @param reader the XML Stream reader
	 */
	private String getURI(XMLStreamReader reader) {
		int eventType = -1;
		StringBuffer sb = new StringBuffer();
		try {
			do {
				eventType = reader.next();
				
				if (eventType == XMLStreamConstants.CHARACTERS) {
					sb.append(reader.getText());
				}
			} while(!reader.hasNext() || !(eventType == XMLStreamConstants.END_ELEMENT &&
					reader.getLocalName().equals(TAG_URI)));
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}			
		
		return sb.toString();
		
	}	

	/**
	 * Gets the blank nodes associated to the SPARQL-results
	 * @param reader the XML Stream reader
	 */
	private String getBNode(XMLStreamReader reader) {
		int eventType = -1;
		StringBuffer sb = new StringBuffer();
		try {
			do {
				eventType = reader.next();
				
				if (eventType == XMLStreamConstants.CHARACTERS) {
					sb.append(reader.getText());
				}
			} while(!reader.hasNext() || !(eventType == XMLStreamConstants.END_ELEMENT &&
					reader.getLocalName().equals(TAG_BNODE)));
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}			
		
		return sb.toString();
	}	

	/**
	 * Gets the literal associated to the SPARQL-results
	 * @param reader the XML Stream reader
	 */
	private String getLiteral(XMLStreamReader reader) {
		int eventType = -1;
		StringBuffer sb = new StringBuffer();
		try {
			String datatype = getAttribute(reader, ATT_DATATYPE);
			String lang = getAttribute(reader, ATT_LANG);
			if (lang != null) {
				sb.append("(@");
				sb.append(lang);
				sb.append(")");
			}			
			do {
				eventType = reader.next();
				
				if (eventType == XMLStreamConstants.CHARACTERS) {

					
					String text = reader.getText();
					System.out.println("LITERAL:" + text);
					sb.append(text);
					//sb.append(reader.getText());

				}
			} while(!reader.hasNext() || !(eventType == XMLStreamConstants.END_ELEMENT &&
					reader.getLocalName().equals(TAG_LITERAL)));

			if (datatype != null) {
				sb.append("^^");
				sb.append(datatype);
			}		
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}			
		
		return sb.toString();		
	}	

	/**
	 * Gets the attributes assocaited to a tag
	 * @param reader the XML Stream reader
	 * @param attributeName the attribute name
	 */
    private String getAttribute(XMLStreamReader reader, String attributeName) {
        int count = reader.getAttributeCount() ;
        if(count > 0) {
            for(int i = 0 ; i < count ; i++) {
            	if (reader.getAttributeName(i).toString().equals(attributeName)) {
            		return reader.getAttributeValue(i);
            	}
            }            
        }
        
        return null;
    }	
	
	/**
	 * Gets the namespaces associated to a tag
	 * @param reader the XML Stream reader
	 */
    private HashMap<String,String> getNamespaces(XMLStreamReader reader) {
        HashMap<String,String> nsPrefix = new HashMap<String,String>();
    	int count = reader.getNamespaceCount();
        if(count > 0){
        	String prefix = "", namespace;
            for(int i = 0 ; i < count ; i++) {
                //System.out.print("xmlns");
                if(reader.getNamespacePrefix(i) != null) {
                    //System.out.print(":" + reader.getNamespacePrefix(i));
                    prefix = reader.getNamespacePrefix(i);
                }                
                //System.out.print("=");
                //System.out.print("\"");
                //System.out.print(reader.getNamespaceURI(i));
                namespace = reader.getNamespaceURI(i);
                //System.out.print("\"");
                //System.out.print("\n");
                if (prefix != null) {
                	nsPrefix.put(prefix, namespace);
                }
            }            
        }
        
        return nsPrefix;
    }
    
	@Override
	public boolean validate(String results) throws KPIModelException {
//		ByteArrayInputStream bis = new ByteArrayInputStream(results.getBytes());
//
//		SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_INSTANCE_NS_URI);
//		Schema schemaXSD = factory.newSchema(schema);
//		Validator validator = schemaXSD.newValidator();
//		validator.validate(bis);
		return false;
	}
}
