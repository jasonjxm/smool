package org.smool.kpi.model.smart.parser.classes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class ConceptUtil {

	public Collection<String> getObjectProperties(HashMap<String,String> map) {

		ArrayList<String> objectProperties = new ArrayList<String>();
		
		for (String property : map.keySet()) {
			if (property.endsWith("_classId")) {
				String propertyName = getPropertyName(property);
				objectProperties.add(propertyName);
			}
		}
		return objectProperties;
	}

	private String getPropertyName(String property) {
		int idx = property.lastIndexOf("_classId");
		return property.substring(0, idx);
	}
}
