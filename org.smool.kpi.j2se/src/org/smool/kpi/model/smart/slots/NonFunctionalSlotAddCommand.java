/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.model.smart.slots;


/**
 * This class represents a command to add new values to a CollectionSlot
 * 
 * @author Fran Ruiz, Fran.Ruiz@tecnalia.com, Tecnalia-Tecnalia
 */
public class NonFunctionalSlotAddCommand<T> extends AbstractCommand {

	private NonFunctionalSlot<T> slot = null;
	private T newValue = null;

	public NonFunctionalSlotAddCommand(NonFunctionalSlot<T> slot, T value) {
		this.slot = slot;
		this.newValue = value;
	}
	
	public void run() {
		slot.addValue(newValue);
	}

	public T getValue() {
		return newValue;
	}
//
//	public String toOWL() {
//		StringBuilder sb = new StringBuilder();
//		sb.append("\t");
//		sb.append(slot.getOWLOpeningTag());
//		if(newValue instanceof AbstractOntConcept) {
//			sb.append(((AbstractOntConcept)newValue)._getIndividualID());
//		} else {
//			sb.append(newValue);
//		}
//		sb.append(slot.getOWLClosingTag());
//		return sb.toString();
//	}

	public NonFunctionalSlot<T> getSlot() {
		return slot;
	}
}
