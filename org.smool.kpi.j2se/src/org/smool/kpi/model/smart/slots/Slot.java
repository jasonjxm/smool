/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.model.smart.slots;

import org.smool.kpi.model.smart.util.IRIUtil;

/**
 * A class representing a generic semantic property
 * 
 * @author Fran Ruiz, Fran.Ruiz@tecnalia.com, Tecnalia-Tecnalia
 */
public abstract class Slot<T> implements Comparable<Slot<T>>, Cloneable {

	final protected Class<T> typeOfT;
	
	/** Semantic properties */
	protected String _propertyPrefix;
	protected String _propertyIRI;
	protected String _propertyName;
	protected String _propertyNamespace;

	protected Slot(Class<T> typeOfT) {
		this.typeOfT = typeOfT;
	}
	
	/**
	 * Sets the slot's iri
	 * @param iri the property iri
	 */
	public void _setIRI(String iri) {
		this._propertyIRI = iri;
		this._propertyName = IRIUtil.getElement(_propertyIRI);
		this._propertyNamespace = IRIUtil.getNamespace(_propertyIRI);
	}
	
	public String _getIRI() {
		return this._propertyIRI;
	}

	/**
	 * Sets the slot's prefix
	 * @param prefix the property prefix
	 */
	public void _setPrefix(String prefix) {
		this._propertyPrefix = prefix;
	}

	public String _getNamespace() {
		return this._propertyNamespace;
	}

	public String _getPrefix() {
		return this._propertyPrefix;
	}
	
	public String getName() {
		return _propertyName;
	}
	
	public String toString() {
		return getName();
	}
	
	/**
	 * Determines whether the slot range is an object
	 * @return <code>true</code> if this is an object property
	 */
	public boolean isObjectProperty() {
		return (this instanceof NonFunctionalObjectSlot<?> || this instanceof FunctionalObjectSlot<?>);
	}
	
	/**
	 * Determines whether the slot range is a datatype
	 * @return <code>true</code> if this is a datatype property
	 */
	public boolean isDatatypeProperty() {
		return (this instanceof NonFunctionalDatatypeSlot<?> || this instanceof FunctionalDatatypeSlot<?>);
	}
	
	/**
	 * Determines whether the slot is functional
	 * @return <code>true</code> if this is a functional property
	 */
	public boolean isFunctionalProperty() {
		return (this instanceof FunctionalSlot<?>);
	}

	public abstract boolean hasContent();
	
	@SuppressWarnings("unchecked")
	public Slot<T> clone() throws CloneNotSupportedException {
	    Slot<T> clone = (Slot<T>)super.clone();
		return clone;
	}
	
	@Override
    public int compareTo(Slot<T> other) {
        return this._getIRI().compareTo(other._getIRI());
    }

    public boolean equals(Object obj) {
        return this._getIRI().equals(((Slot<?>)obj)._getIRI());
    }	
	
}
