/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.model.smart.slots;


/**
 * This class represents a command to add new values to a CollectionSlot
 * 
 * @author Fran Ruiz, Fran.Ruiz@tecnalia.com, Tecnalia-Tecnalia
 */
public class NonFunctionalSlotRemoveCommand<T> extends AbstractCommand {

	private NonFunctionalSlot<T> slot = null;
	private T removeValue = null;

	public NonFunctionalSlotRemoveCommand(NonFunctionalSlot<T> slot, T value) {
		this.slot = slot;
		this.removeValue = value;
	}
	
	public void run() {
		slot.removeValue(removeValue);
	}

	public T getValue() {
		return removeValue;
	}

	public NonFunctionalSlot<T> getSlot() {
		return slot;
	}
}
