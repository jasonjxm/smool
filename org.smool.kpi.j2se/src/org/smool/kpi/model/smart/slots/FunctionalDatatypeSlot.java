/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.model.smart.slots;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.apache.xml.utils.QName;
import org.smool.kpi.model.smart.util.XSDUtils;



public class FunctionalDatatypeSlot<T> extends FunctionalSlot<T> implements Cloneable {

	private String xsdDatatype = null;

	public FunctionalDatatypeSlot(Class<T> classOfT) {
		super(classOfT);
	}

	public void setRange(String range) {
		this.xsdDatatype = range;
	}

	public String getRange() {
		return this.xsdDatatype;
	}	
	
	@Override
	public int compareTo(Slot<T> o) {
		FunctionalDatatypeSlot<T> slot = (FunctionalDatatypeSlot<T>) o;
		return this._propertyIRI.compareTo(slot._propertyIRI);
	}	

	@Override
	public FunctionalDatatypeSlot<T> clone() throws CloneNotSupportedException {
		FunctionalDatatypeSlot<T> clone = (FunctionalDatatypeSlot<T>)super.clone();
		return clone;
	}

	/**
	 * Gets the encoded value for a given raw datatype
	 * @param rawValue the raw value
	 * @return the string representation of that raw value
	 */
	public String toEncodedValue() {
		if (this.xsdDatatype.equals("xsd:Base64Binary") || this.xsdDatatype.equals("xsd:HexBinary")) {
			// I already know that we should send the hex value
			return XSDUtils.getHex((byte[])value);
		} else if (this.xsdDatatype.equals("xsd:Double")) {
			return Double.toString((Double)value);
		} else if (this.xsdDatatype.equals("xsd:Integer")) {
			return ((BigInteger)value).toString();
		} else if (this.xsdDatatype.equals("xsd:Int") || this.xsdDatatype.equals("xsd:UnsignedShort")) {
			return Integer.toString((Integer)value);
		} else if (this.xsdDatatype.equals("xsd:Long")) {
			return Long.toString((Long) value);
		} else if (this.xsdDatatype.equals("xsd:Short") || this.xsdDatatype.equals("xsd:UnsignedByte")) {
			return Short.toString((Short)value);
		} else if (this.xsdDatatype.equals("xsd:Decimal")) {
			return ((BigDecimal)value).toString();
		} else if (this.xsdDatatype.equals("xsd:Float")) {
			return Float.toString((Float)value);
		} else if (this.xsdDatatype.equals("xsd:Byte")) {
			return Byte.toString((Byte)value);
		} else if (this.xsdDatatype.equals("xsd:UnsignedInt")) {
			return Long.toString((Long)value);
		} else if (this.xsdDatatype.equals("xsd:Boolean")) {
			return Boolean.toString((Boolean)value);
		} else if (this.xsdDatatype.equals("xsd:DateTime") || this.xsdDatatype.equals("xsd:Date")
				|| this.xsdDatatype.equals("xsd:Time") || this.xsdDatatype.equals("xsd:G")) {
			return value.toString();			
		} else if (this.xsdDatatype.equals("xsd:Duration")) {
			return value.toString();	
		} else if (this.xsdDatatype.equals("xsd:QName") ||
				this.xsdDatatype.equals("xsd:NOTATION")) {
			return value.toString();	
		} else {
			return value.toString();
		}
	}
	
	/**
	 * Gets the encoded value for a given raw datatype
	 * @param rawValue the raw value
	 * @return the string representation of that raw value
	 */
	public String toEncodedValue(Object rawValue) {
		if (this.xsdDatatype.equals("xsd:Base64Binary") || this.xsdDatatype.equals("xsd:HexBinary")) {
			// I already know that we should send the hex value
			return XSDUtils.getHex((byte[])rawValue);
		} else if (this.xsdDatatype.equals("xsd:Double")) {
			return Double.toString((Double)rawValue);
		} else if (this.xsdDatatype.equals("xsd:Integer")) {
			return ((BigInteger)rawValue).toString();
		} else if (this.xsdDatatype.equals("xsd:Int") || this.xsdDatatype.equals("xsd:UnsignedShort")) {
			return Integer.toString((Integer)rawValue);
		} else if (this.xsdDatatype.equals("xsd:Long")) {
			return Long.toString((Long) rawValue);
		} else if (this.xsdDatatype.equals("xsd:Short") || this.xsdDatatype.equals("xsd:UnsignedByte")) {
			return Short.toString((Short)rawValue);
		} else if (this.xsdDatatype.equals("xsd:Decimal")) {
			return ((BigDecimal)rawValue).toString();
		} else if (this.xsdDatatype.equals("xsd:Float")) {
			return Float.toString((Float)rawValue);
		} else if (this.xsdDatatype.equals("xsd:Byte")) {
			return Byte.toString((Byte)rawValue);
		} else if (this.xsdDatatype.equals("xsd:UnsignedInt")) {
			return Long.toString((Long)rawValue);
		} else if (this.xsdDatatype.equals("xsd:Boolean")) {
			return Boolean.toString((Boolean)rawValue);
		} else if (this.xsdDatatype.equals("xsd:DateTime") || this.xsdDatatype.equals("xsd:Date")
				|| this.xsdDatatype.equals("xsd:Time") || this.xsdDatatype.equals("xsd:G")) {
			return rawValue.toString();			
		} else if (this.xsdDatatype.equals("xsd:Duration")) {
			return rawValue.toString();	
		} else if (this.xsdDatatype.equals("xsd:QName") ||
				this.xsdDatatype.equals("xsd:NOTATION")) {
			return rawValue.toString();	
		} else {
			return rawValue.toString();
		}
	}	
	
	@SuppressWarnings("unchecked")
	public T fromEncodedValue(String stringRepresentation) {
		String rawString = XSDUtils.getRawContent(stringRepresentation);
		if (this.xsdDatatype.equals("xsd:Base64Binary") || this.xsdDatatype.equals("xsd:HexBinary")) {
			return (T) XSDUtils.hexStringToByteArray(rawString);
		} else if (this.xsdDatatype.equals("xsd:Double")) {
			return (T) Double.valueOf(rawString);
		} else if (this.xsdDatatype.equals("xsd:Boolean")) {
			return (T) Boolean.valueOf(rawString);
		} else if (this.xsdDatatype.equals("xsd:Byte")) {
			return (T) Byte.valueOf(rawString);
		} else if (this.xsdDatatype.equals("xsd:Float")) {
			return (T) Float.valueOf(rawString);
		} else if (this.xsdDatatype.equals("xsd:Decimal")) {
			return (T) BigDecimal.valueOf(Long.valueOf(rawString));
		} else if (this.xsdDatatype.equals("xsd:Int") || this.xsdDatatype.equals("xsd:UnsignedShort")) {
			return (T) Integer.valueOf(rawString);
		} else if (this.xsdDatatype.equals("xsd:Integer")) {
			return (T) BigInteger.valueOf(Long.valueOf(rawString));
		} else if (this.xsdDatatype.equals("xsd:Long") || this.xsdDatatype.equals("xsd:UnsignedInt")) {
			return (T) Long.valueOf(rawString);
		} else if (this.xsdDatatype.equals("xsd:Short") || this.xsdDatatype.equals("xsd:UnsignedByte")) {
			return (T) Short.valueOf(rawString);			
		} else if (this.xsdDatatype.equals("xsd:DateTime") || this.xsdDatatype.equals("xsd:Date")
				|| this.xsdDatatype.equals("xsd:Time") || this.xsdDatatype.equals("xsd:G")) {
			try {
				return (T) DatatypeFactory.newInstance().newXMLGregorianCalendar(rawString);
			} catch (DatatypeConfigurationException e) {
				e.printStackTrace();
				return null;
			}
		} else if (this.xsdDatatype.equals("xsd:Duration")) {
			try {
				return (T) DatatypeFactory.newInstance().newDuration(rawString);
			} catch (DatatypeConfigurationException e) {
				e.printStackTrace();
				return null;
			}
		} else if (this.xsdDatatype.equals("xsd:QName") || 
				this.xsdDatatype.equals("xsd:NOTATION")) {
			return (T) QName.getQNameFromString(rawString);			
		} else {
			return (T) rawString;
		}
	}
	
	/**
	 * Gets the slot's value
	 * @return the slot value
	 */
	@Override
	public T getValue() {
		return value;
	}
	
	/**
	 * Sets a new value to the slot
	 * @param newValue the new value
	 */
	@Override
	public void setValue(T newValue) {
		this.value = newValue;
	}
}
