/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.model.smart.slots;


public class FunctionalSlotChangeCommand<T> extends AbstractCommand {

	private FunctionalSlot<T> slot = null;
	private T newValue = null;

	public FunctionalSlotChangeCommand(FunctionalSlot<T> slot, T value) {
		this.slot = slot;
		this.newValue = value;
	}
	
	public void run() {
		slot.setValue(newValue);
	}

	public T getValue() {
		return newValue;
	}

	/**
	 * Gets the slot associated to this FunctionalSlotChangeCommand
	 * @return the FunctionalSlot
	 */
	public FunctionalSlot<T> getSlot() {
		return slot;
	}
}
