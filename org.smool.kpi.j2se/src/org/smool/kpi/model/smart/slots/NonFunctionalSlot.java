/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.model.smart.slots;

import java.util.ArrayList;
import java.util.Collection;

public abstract class NonFunctionalSlot<T> extends Slot<T> {

	protected ArrayList<T> values = new ArrayList<T>();

	public NonFunctionalSlot(Class<T> classOfT) {
		super(classOfT);
	}
	
	public ArrayList<T> getValues() {
		return values;
	}
	
	public boolean contains(T newValue) {
		for(T value : values) {
			if (value.equals(newValue)) {
				return true;
			}
		}
		return false;
	}

	public boolean addValue(T newValue) {
		if (!contains(newValue)) {
			values.add(newValue);
			return true;
		}
		return false;
	}
	
	public boolean addAll(Collection<T> newValues) {
		boolean success = true;
		for (T newValue : newValues) {
			if (!contains(newValue)) {
				values.add(newValue);
			} else {
				success = false;
			}
		}
		return success;
	}	

	public boolean removeAll(Collection<T> obsoleteValues) {
		boolean success = true;
		for (T obsoleteValue : obsoleteValues) {
			if (contains(obsoleteValue)) {
				values.remove(obsoleteValue);
			} else {
				success = false;
			}
		}
		return success;
	}		
	
	public boolean removeValue(T oldValue) {
		if (contains(oldValue)) {
			values.remove(oldValue);
			return true;
		}
		return false;
	}

	public int size() {
		return values.size();
	}
	
	/**
	 * List the values
	 */
	public Collection<T> listValues() {
		return values;
	}
	
	@Override
	public boolean hasContent() {
		boolean contentFound = false;
		for (T value : values) {
			if (value != null) {
				contentFound = true;
			}
		}
		return contentFound;
	}
	
	@Override
	public NonFunctionalSlot<T> clone() throws CloneNotSupportedException {
		NonFunctionalSlot<T> clone = (NonFunctionalSlot<T>)super.clone();
		return clone;
	}		
}