/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research & Innovacion.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.model.smart.slots;

import java.util.ArrayList;
import java.util.Collection;

import org.smool.kpi.model.exception.KPIModelException;
import org.smool.kpi.model.smart.AbstractOntConcept;


/**
 * Represents a non functional object property of an  
 * @author Fran Ruiz, fran.ruiz@tecnalia.com, Tecnalia - Software Systems Development
 *
 * @param <T> any subclass of an AbstractOntConcept
 */
public class NonFunctionalObjectSlot<T extends AbstractOntConcept> extends NonFunctionalSlot<T> {

	/**
	 * Constructor
	 * @param classOfT the class of AbstractOntConcept subclass
	 */
	public NonFunctionalObjectSlot(Class<T> classOfT) {
		super(classOfT);
	}	

	/**
	 * List the values of the non-functional object property
	 * @return a collection of AbstractOntConcepts
	 */
	@Override
	public Collection<T> listValues() {
		return (Collection<T>) values;
	}
	
	
	public String getWhereClauses() {
		try {
			AbstractOntConcept aoc = (AbstractOntConcept) typeOfT.newInstance();

			StringBuilder sb = new StringBuilder();
			sb.append(" .\n");
			sb.append("\t?").append(this.getName()).append("_classId").append(" ");
			sb.append("rdf:type ").append(aoc._getClassPrefix()).append(":").append(aoc._getClassID());
			
			sb.append(aoc.toSPARQLSELECTClause(this.getName()));
			return sb.toString();
		} catch (IllegalAccessException ex) {
			ex.printStackTrace();
			return null;
		} catch (InstantiationException ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
	public String getSelectVariables() {
		try {
			AbstractOntConcept aoc = (AbstractOntConcept) typeOfT.newInstance();

			StringBuilder sb = new StringBuilder();
			sb.append(aoc.toVariables(this.getName()));
			return sb.toString();
		} catch (IllegalAccessException ex) {
			ex.printStackTrace();
			return null;
		} catch (InstantiationException ex) {
			ex.printStackTrace();
			return null;
		}
	}		

	public AbstractOntConcept createNewVoidInstance() throws KPIModelException {
		AbstractOntConcept aoc;
		try {
			aoc = (AbstractOntConcept) typeOfT.newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
			throw new KPIModelException(e.getMessage(), e);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			throw new KPIModelException(e.getMessage(), e);
		}
		return aoc;
	}

	/**
	 * Gets the slot class
	 * @return the typed class
	 * @throws KPIModelException
	 */
	public Class<? extends AbstractOntConcept> getSlotClass() throws KPIModelException {
		return typeOfT;
	}
	
	@Override
	public int compareTo(Slot<T> o) {
		NonFunctionalObjectSlot<T> slot = (NonFunctionalObjectSlot<T>) o;
		return this._propertyIRI.compareTo(slot._propertyIRI);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public NonFunctionalObjectSlot<T> clone() throws CloneNotSupportedException {
		NonFunctionalObjectSlot<T> clone = (NonFunctionalObjectSlot<T>)super.clone();
		clone.values = new ArrayList<T>();
		clone.values = (ArrayList<T>) this.values.clone();
		return clone;
	}
}
