/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.model.smart.slots;

import org.smool.kpi.model.exception.KPIModelException;
import org.smool.kpi.model.smart.AbstractOntConcept;


public class FunctionalObjectSlot<T extends AbstractOntConcept> extends FunctionalSlot<T> {

	public FunctionalObjectSlot(Class<T> classOfT) {
		super(classOfT);
	}

	@Override
	public T getValue() {
		return value;
	}
	
	@Override
	public void setValue(T value) {
		this.value = value;
	}

	public String getWhereClauses() {
		try {
			AbstractOntConcept aoc = (AbstractOntConcept) typeOfT.newInstance();

			StringBuilder sb = new StringBuilder();
			sb.append(" .\n");
			sb.append("\t?").append(this.getName()).append("_classId").append(" ");
			sb.append("rdf:type ").append(aoc._getClassPrefix()).append(":").append(aoc._getClassID());
			
			sb.append(aoc.toSPARQLSELECTClause(this.getName()));
			return sb.toString();
		} catch (IllegalAccessException ex) {
			ex.printStackTrace();
			return null;
		} catch (InstantiationException ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
	public String getSelectVariables() {
		try {
			AbstractOntConcept aoc = (AbstractOntConcept) typeOfT.newInstance();

			StringBuilder sb = new StringBuilder();
			sb.append(aoc.toVariables(this.getName()));
			return sb.toString();
		} catch (IllegalAccessException ex) {
			ex.printStackTrace();
			return null;
		} catch (InstantiationException ex) {
			ex.printStackTrace();
			return null;
		}
	}	

	public AbstractOntConcept createNewVoidInstance() throws KPIModelException {
		AbstractOntConcept aoc;
		try {
			aoc = (AbstractOntConcept) typeOfT.newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
			throw new KPIModelException(e.getMessage(), e);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			throw new KPIModelException(e.getMessage(), e);
		}
		return aoc;
	}	

	public Class<? extends AbstractOntConcept> getSlotClass() throws KPIModelException {
		return typeOfT;
	}		
	
	@Override
	public int compareTo(Slot<T> o) {
		FunctionalObjectSlot<T> slot = (FunctionalObjectSlot<T>) o;
		return this._propertyIRI.compareTo(slot._propertyIRI);
	}

	@SuppressWarnings("unchecked")
	@Override
	public FunctionalObjectSlot<T> clone() throws CloneNotSupportedException {
		FunctionalObjectSlot<T> clone = (FunctionalObjectSlot<T>)super.clone();
		clone.value = (T) (value != null ? value.clone() : null);
		return clone;
	}		
}
