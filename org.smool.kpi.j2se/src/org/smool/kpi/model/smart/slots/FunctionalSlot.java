/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.model.smart.slots;

/**
 * A class representing a generic functional property
 * 
 * @author fran
 */
public abstract class FunctionalSlot<T> extends Slot<T> implements Cloneable {

	/** Slot instance value */
	protected T value;
	
	public FunctionalSlot(Class<T> classOfT) {
		super(classOfT);
	}

	/**
	 * Gets the slot's value
	 * @return the slot value
	 */
	public T getValue() {
		return value;
	}
	
	/**
	 * Sets a new value to the slot
	 * @param newValue the new value
	 */
	public void setValue(T newValue) {
		this.value = newValue;
	}
	
	@Override
	public boolean hasContent() {
		return (this.getValue() != null);
	}
	
	@Override
	public FunctionalSlot<T> clone() throws CloneNotSupportedException {
		FunctionalSlot<T> clone = (FunctionalSlot<T>)super.clone();
		clone.value = this.value;
		return clone;
	}	
}
