/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.model.smart.slots;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.apache.xml.utils.QName;
import org.smool.kpi.model.smart.util.XSDUtils;


public class NonFunctionalDatatypeSlot<T> extends NonFunctionalSlot<T> implements Cloneable {

	private String xsdDatatype = null;

	public NonFunctionalDatatypeSlot(Class<T> classOfT) {
		super(classOfT);
	}
	
	@Override
	public Collection<T> listValues() {
		return values;
	}	

	public void setRange(String range) {
		this.xsdDatatype = range;
	}
	
	public String getRange() {
		return this.xsdDatatype;
	}	
	
	/**
	 * add a new value to the current non-functional datatype property values
	 * @param value a value
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean addValue(T value) {
		if (value instanceof String) {
			String strValue = (String) value;
			if (isXSDDatatype(strValue)) {
				XSDUtils utils = new XSDUtils();
				Object obj = utils.getXSDObject(strValue);
				super.addValue((T)obj);
				return true;
			} else {
				super.addValue(value);
				return true;
			}
		} else {
			return super.addValue(value);
		}
	}
	
	/**
	 * removes a new value to the current non-functional datatype property values
	 * @param value a value
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean removeValue(T oldValue) {
		if (oldValue instanceof String) {
			String strValue = (String) oldValue;
			if (isXSDDatatype(strValue)) {
				XSDUtils utils = new XSDUtils();
				Object obj = utils.getXSDObject(strValue);
				super.removeValue((T)obj);
				return true;
			} else {
				super.removeValue(oldValue);
				return true;
			}
		} else {
			return super.removeValue(oldValue);
		}
	}	
	
	private boolean isXSDDatatype(String value) {
		if (value.contains("^^")) {
			return true;
		}
		return false;
	}	
	
	@Override
	public int compareTo(Slot<T> o) {
		NonFunctionalDatatypeSlot<T> slot = (NonFunctionalDatatypeSlot<T>) o;
		return this._propertyIRI.compareTo(slot._propertyIRI);
	}

	@SuppressWarnings("unchecked")
	@Override
	public NonFunctionalDatatypeSlot<T> clone() throws CloneNotSupportedException {
		NonFunctionalDatatypeSlot<T> clone = (NonFunctionalDatatypeSlot<T>)super.clone();
		clone.values = new ArrayList<T>();		
		clone.values = (ArrayList<T>) this.values.clone();
		return clone;
	}
	
	/**
	 * Gets the encoded value for a given raw datatype
	 * @return the string representation of that raw value
	 */
	public Collection<String> toEncodedValue() {
		System.out.println("NF----------" + this.xsdDatatype);
		ArrayList<String> encodedValues = new ArrayList<String>();
		for (T value : values) {
			if (this.xsdDatatype.equals("xsd:Base64Binary") || this.xsdDatatype.equals("xsd:HexBinary")) {
				// I already know that we should send the hex value
				encodedValues.add(XSDUtils.getHex((byte[])value));
			} else if (this.xsdDatatype.equals("xsd:Double")) {
				encodedValues.add(Double.toString((Double)value));
			} else if (this.xsdDatatype.equals("xsd:Integer")) {
				encodedValues.add(((BigInteger)value).toString());
			} else if (this.xsdDatatype.equals("xsd:Int") || this.xsdDatatype.equals("xsd:UnsignedShort")) {
				encodedValues.add(Integer.toString((Integer)value));
			} else if (this.xsdDatatype.equals("xsd:Short") || this.xsdDatatype.equals("xsd:UnsignedByte")) {
				encodedValues.add(Short.toString((Short)value));
			} else if (this.xsdDatatype.equals("xsd:Decimal")) {
				encodedValues.add(((BigDecimal)value).toString());
			} else if (this.xsdDatatype.equals("xsd:Float")) {
				encodedValues.add(Float.toString((Float)value));
			} else if (this.xsdDatatype.equals("xsd:Byte")) {
				encodedValues.add(Byte.toString((Byte)value));
			} else if (this.xsdDatatype.equals("xsd:UnsignedInt")) {
				encodedValues.add(Long.toString((Long)value));
			} else if (this.xsdDatatype.equals("xsd:Boolean")) {
				encodedValues.add(Boolean.toString((Boolean)value));
			} else if (this.xsdDatatype.equals("xsd:Long")) {
				encodedValues.add(Long.toString((Long) value));
			} else if (this.xsdDatatype.equals("xsd:DateTime") || this.xsdDatatype.equals("xsd:Date")
					|| this.xsdDatatype.equals("xsd:Time") || this.xsdDatatype.equals("xsd:G")) {
				encodedValues.add(value.toString());			
			} else if (this.xsdDatatype.equals("xsd:Duration")) {
				encodedValues.add(value.toString());	
			} else if (this.xsdDatatype.equals("xsd:QName") ||
					this.xsdDatatype.equals("xsd:NOTATION")) {
				encodedValues.add(value.toString());	
			} else {
				encodedValues.add(value.toString());
			}
		}
		
		return encodedValues;
	}
	
	/**
	 * Gets the encoded value for a given raw datatype
	 * @param rawValue the raw value
	 * @return the string representation of that raw value
	 */
	public String toEncodedValue(Object rawValue) {
		if (this.xsdDatatype.equals("xsd:Base64Binary") || this.xsdDatatype.equals("xsd:HexBinary")) {
			// I already know that we should send the hex value
			return XSDUtils.getHex((byte[])rawValue);
		} else if (this.xsdDatatype.equals("xsd:Double")) {
			return Double.toString((Double)rawValue);
		} else if (this.xsdDatatype.equals("xsd:Integer")) {
			return (((BigInteger)rawValue)).toString();
		} else if (this.xsdDatatype.equals("xsd:Int") || this.xsdDatatype.equals("xsd:UnsignedShort")) {
			return Integer.toString((Integer)rawValue);
		} else if (this.xsdDatatype.equals("xsd:Short") || this.xsdDatatype.equals("xsd:UnsignedByte")) {
			return Short.toString((Short)rawValue);
		} else if (this.xsdDatatype.equals("xsd:Decimal")) {
			return ((BigDecimal)rawValue).toString();
		} else if (this.xsdDatatype.equals("xsd:Float")) {
			return Float.toString((Float)rawValue);
		} else if (this.xsdDatatype.equals("xsd:Byte")) {
			return Byte.toString((Byte)rawValue);
		} else if (this.xsdDatatype.equals("xsd:UnsignedInt")) {
			return Long.toString((Long)rawValue);
		} else if (this.xsdDatatype.equals("xsd:Boolean")) {
			return Boolean.toString((Boolean)rawValue);
		} else if (this.xsdDatatype.equals("xsd:Long")) {
			return Long.toString((Long) rawValue);
		} else if (this.xsdDatatype.equals("xsd:DateTime") || this.xsdDatatype.equals("xsd:Date")
				|| this.xsdDatatype.equals("xsd:Time") || this.xsdDatatype.equals("xsd:G")) {
			return rawValue.toString();			
		} else if (this.xsdDatatype.equals("xsd:Duration")) {
			return rawValue.toString();	
		} else if (this.xsdDatatype.equals("xsd:QName") ||
				this.xsdDatatype.equals("xsd:NOTATION")) {
			return rawValue.toString();	
		} else {
			return rawValue.toString();
		}
	}
	
	@SuppressWarnings("unchecked")
	public T fromEncodedValue(String stringRepresentation) {
		String rawString = XSDUtils.getRawContent(stringRepresentation);

		if (this.xsdDatatype.equals("xsd:Base64Binary") || this.xsdDatatype.equals("xsd:HexBinary")) {
			return (T) XSDUtils.hexStringToByteArray(rawString);
		} else if (this.xsdDatatype.equals("xsd:Double")) {
			return (T) Double.valueOf(rawString);
		} else if (this.xsdDatatype.equals("xsd:Boolean")) {
			return (T) Boolean.valueOf(rawString);
		} else if (this.xsdDatatype.equals("xsd:Byte")) {
			return (T) Byte.valueOf(rawString);
		} else if (this.xsdDatatype.equals("xsd:Float")) {
			return (T) Float.valueOf(rawString);
		} else if (this.xsdDatatype.equals("xsd:Decimal")) {
			return (T) BigDecimal.valueOf(Long.valueOf(rawString));
		} else if (this.xsdDatatype.equals("xsd:Int") || this.xsdDatatype.equals("xsd:UnsignedShort")) {
			return (T) Integer.valueOf(rawString);
		} else if (this.xsdDatatype.equals("xsd:Integer")) {
			return (T) BigInteger.valueOf(Long.valueOf(rawString));
		} else if (this.xsdDatatype.equals("xsd:Long") || this.xsdDatatype.equals("xsd:UnsignedInt")) {
			return (T) Long.valueOf(rawString);
		} else if (this.xsdDatatype.equals("xsd:Short") || this.xsdDatatype.equals("xsd:UnsignedByte")) {
			return (T) Short.valueOf(rawString);			
		} else if (this.xsdDatatype.equals("xsd:DateTime") || this.xsdDatatype.equals("xsd:Date")
				|| this.xsdDatatype.equals("xsd:Time") || this.xsdDatatype.equals("xsd:G")) {
			try {
				return (T) DatatypeFactory.newInstance().newXMLGregorianCalendar(rawString);
			} catch (DatatypeConfigurationException e) {
				e.printStackTrace();
				return null;
			}
		} else if (this.xsdDatatype.equals("xsd:Duration")) {
			try {
				return (T) DatatypeFactory.newInstance().newDuration(rawString);
			} catch (DatatypeConfigurationException e) {
				e.printStackTrace();
				return null;
			}
		} else if (this.xsdDatatype.equals("xsd:QName") || 
				this.xsdDatatype.equals("xsd:NOTATION")) {
			return (T) QName.getQNameFromString(rawString);			
		} else {
			return (T) rawString;
		}
	}
}
