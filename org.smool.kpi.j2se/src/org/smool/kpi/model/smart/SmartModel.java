/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/

package org.smool.kpi.model.smart;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.smool.kpi.common.KPProperties;
import org.smool.kpi.common.Logger;
import org.smool.kpi.connector.ConnectionErrorException;
import org.smool.kpi.model.AbstractModel;
import org.smool.kpi.model.IModelListener;
import org.smool.kpi.model.exception.KPIModelException;
import org.smool.kpi.model.smart.encoding.AbstractEncodingProcessor;
import org.smool.kpi.model.smart.encoding.AbstractQueryProcessor;
import org.smool.kpi.model.smart.encoding.m3.RDFM3QueryProcessor;
import org.smool.kpi.model.smart.encoding.owl.OWLProcessor;
import org.smool.kpi.model.smart.encoding.owl.SPARQLQueryProcessor;
import org.smool.kpi.model.smart.subscription.AbstractSubscription;
import org.smool.kpi.model.smart.subscription.ISubscription;
import org.smool.kpi.ssap.ComposedPendingTask;
import org.smool.kpi.ssap.PendingTask;
import org.smool.kpi.ssap.SIB;
import org.smool.kpi.ssap.exception.KPISSAPException;
import org.smool.kpi.ssap.message.SSAPMessage.TransactionType;
import org.smool.kpi.ssap.message.parameter.SSAPMessageParameter.NameAttribute;
import org.smool.kpi.ssap.message.parameter.SSAPMessageRDFParameter.TypeAttribute;
import org.smool.kpi.ssap.message.parameter.SSAPMessageStatusParameter.SIBStatus;


/**
 * This class represents the Smart Model that contains the SIB reference
 * and process all add, remove and publish instructions received from
 * the KP logic layer.
 *
 * @author Raul Otaolea, Raul.Otaolea@tecnalia.com, Tecnalia
 * @author Fran Ruiz, Fran.Ruiz@tecnalia.com, Tecnalia
 */
public class SmartModel extends AbstractModel {
	/** The set of last removed concepts */
	private HashMap<String, AbstractOntConcept> removedConcepts;

	/** The set of newly added concepts */
	private HashMap<String, AbstractOntConcept> newConcepts;

	/** The set of abstract ontology concepts that are already part of the model */
	private HashMap<String, AbstractOntConcept> conceptsByID;

	/** The processor that process the query-related operations */
	private HashMap<TypeAttribute, AbstractQueryProcessor> queryProcessors;

	/** The processor that process the query-related operations */
	private HashMap<TypeAttribute, AbstractEncodingProcessor> encodingProcessors;

	/** Publish operation lock */
	private Object lock = new Object();

	private static final TypeAttribute defaultEncoding = TypeAttribute.RDFXML;

	private TypeAttribute encoding;

	private static final String MAPPING = "/config/mapping.properties";
	
	/** Mapping between the ontology class urls and the corresponding class */
	private HashMap<String,String> ontologyMapping = new HashMap<String,String>();
	
	/**
	 * Constructor for the Smart Model. Initialized the smart model,
	 * setting the default values
	 * @throws KPIModelException 
	 */
	public SmartModel() throws KPIModelException {
		super();

		this.conceptsByID = new HashMap<String, AbstractOntConcept>();
		this.newConcepts = new HashMap<String,AbstractOntConcept>();
		this.removedConcepts = new HashMap<String,AbstractOntConcept>();
		this.setTimeout(0);

		this.encoding = defaultEncoding;
		
		Properties props = new Properties();
		try {
			InputStream mapping = this.getClass().getResourceAsStream(MAPPING);
			if (mapping != null ){
				props.load(mapping);
			}
			else{
				throw new KPIModelException("Could not open mapping properties file");
			}
		} catch (IOException e) {
			//Add a more claryfing message
			Logger.error("Could not read mapping properties file!");
		}
		
		for (Object concept : props.keySet()) {
			String conceptURL = (String) props.get(concept);
			ontologyMapping.put(conceptURL, (String)concept);
		}
	}

	@Override
	public void setSIB(SIB sib) {
		super.setSIB(sib);
		this.getSIB().addSIBConnectionListener(this);

		this.queryProcessors = new HashMap<TypeAttribute, AbstractQueryProcessor>();
		this.queryProcessors.put(TypeAttribute.RDFXML, new SPARQLQueryProcessor(this));
		this.queryProcessors.put(TypeAttribute.RDFM3, new RDFM3QueryProcessor(this));

		this.encodingProcessors = new HashMap<TypeAttribute, AbstractEncodingProcessor>();
		this.encodingProcessors.put(TypeAttribute.RDFXML, new OWLProcessor());
	}

	/**
	 * Establishes the encoding to be used for the current Smart Model.
	 * @param encoding the encoding to be used
	 * @throws KPIModelException if the encoding is not supported by the Smart Model
	 */
	public void setEncoding(TypeAttribute encoding) throws KPIModelException {
		if ((encoding == TypeAttribute.RDFXML) || (encoding == TypeAttribute.SPARQL)) {
			this.encoding = TypeAttribute.RDFXML;
		} else if (encoding == TypeAttribute.RDFM3) {
			this.encoding = TypeAttribute.RDFM3;
		} else {
			throw new KPIModelException("Encoding not supported");
		}
	}

	/**
	 * Resets the connection
	 */
	@Override
	protected void reset() {
		this.conceptsByID.clear();
		this.newConcepts.clear();
		this.removedConcepts.clear();
		for (TypeAttribute ta : queryProcessors.keySet()) {
			queryProcessors.get(ta).reset();
		}
	}

	/**
	 * Connects to the SIB.
	 * @param insertModel determines if the ontology model is inserted before
	 * @throws KPIModelException when some error arise during connection
	 */
	public void connect(boolean insertModel) throws KPIModelException {
		this.connect(null, null, insertModel);
	}

	/**
	 * Connects to the SIB.
	 * @param username the username provided for authenticating in the SIB
	 * @param password the password provided for authenticating in the SIB
	 * @param insertModel determines if the ontology model is inserted before
	 * @throws KPIModelException when some error arise during connection
	 */
	public void connect(String username, String password, boolean insertModel) throws KPIModelException {
		Logger.debug(this.getClass().getName(), "SmartModel is connecting to SIB...");

		// Synchronize here to ensure that other threads can not insert/remove KPs and/or
		// change commands during the publication.
		synchronized(lock) {

			try {
				this.connect(username,password);

				if (insertModel) {
					this.insertModel();
				}
			} catch (KPIModelException modelex) {
				Logger.error(this.getClass().getName(), "Cannot connect to SIB: " + modelex.getCause());
				modelex.printStackTrace();
				throw new KPIModelException("Unable to connect to SIB", modelex.getCause(), modelex.getStatus());
			} catch (Exception e) {
				Logger.error(this.getClass().getName(), "Cannot connect: " + e.getCause());
				throw new KPIModelException("Could not connect to SIB", e);
			}
		}

		Logger.debug(this.getClass().getName(), "SmartModel connected to SIB");
	}

	/**
	 * Inserts the initial model into the SIB
	 * @throws KPIModelException when some error arise during connection
	 */
	public void insertModel() throws KPIModelException {
		Logger.debug(this.getClass().getName(), "Inserting ontology model in the SIB");
		try {
			// Gets the ontology model
			String initialModel = KPProperties.getInstance().getModelOntology();

			// Joins to the SIB
			PendingTask pt = getSIB().insert(initialModel, TypeAttribute.RDFXML);
			getSIB().publish();
			pt.waitUntilFinished(this.getTimeout());
			if(!pt.isFinished()) {
				throw new KPIModelException("Could not do initial INSERT into the SIB");
			}

			// Gets the status of the response
			SIBStatus status = (SIBStatus)pt.getProperty(NameAttribute.STATUS.getValue());

			if (!status.equals(SIBStatus.STATUS_OK)) {
				throw new KPIModelException("The model insertion cannot be executed due to " + status.getValue(), status);
			}

			modelSupport.fireInitialized();
		} catch (KPISSAPException ssapex) {
			Logger.error(this.getClass().getName(), "Cannot insert the initial model in the SIB: " + ssapex.getCause());
			ssapex.printStackTrace();
			throw new KPIModelException("Unable to insert initial model in the SIB", ssapex);
		} catch (KPIModelException modelex) {
			Logger.error(this.getClass().getName(), "Cannot insert the initial model in the SIB: " + modelex.getCause());
			modelex.printStackTrace();
			throw new KPIModelException("Unable to insert initial model in the SIB", modelex.getCause(), modelex.getStatus());
		} catch (Exception e) {
			Logger.error(this.getClass().getName(), "Cannot insert the initial model: " + e.getCause());
			throw new KPIModelException("Could not insert initial model in SIB", e);
		}

		Logger.debug(this.getClass().getName(), "Ontology model inserted into the SIB");
	}

	/**
	 * Removes the initial model into the SIB
	 * @throws KPIModelException when some error arise during connection
	 */
	public void removeModel() throws KPIModelException {
		Logger.debug(this.getClass().getName(), "Removing ontology model from SIB");
		// Synchronize here to ensure that other threads can not insert/remove KPs and/or
		// change commands during the publication.
		synchronized(lock) {

			try {
				// Gets the ontology model
				String initialModel = KPProperties.getInstance().getModelOntology();

				// Joins to the SIB
				PendingTask pt = getSIB().remove(initialModel, TypeAttribute.RDFXML);
				getSIB().publish();
				pt.waitUntilFinished(this.getTimeout());
				if(!pt.isFinished()) {
					throw new KPIModelException("Could not do model removal from SIB");
				}

				// Gets the status of the response
				SIBStatus status = (SIBStatus)pt.getProperty(NameAttribute.STATUS.getValue());

				if (!status.equals(SIBStatus.STATUS_OK)) {
					throw new KPIModelException("The model removal cannot be executed due to " + status.getValue(), status);
				}

			} catch (KPISSAPException ssapex) {
				Logger.error(this.getClass().getName(), "Cannot remove the initial model in the SIB: " + ssapex.getCause());
				ssapex.printStackTrace();
				throw new KPIModelException("Unable to remove initial model in the SIB", ssapex);
			} catch (KPIModelException modelex) {
				Logger.error(this.getClass().getName(), "Cannot remove the initial model in the SIB: " + modelex.getCause());
				modelex.printStackTrace();
				throw new KPIModelException("Unable to remove initial model in the SIB", modelex.getCause(), modelex.getStatus());
			} catch (Exception e) {
				Logger.error(this.getClass().getName(), "Cannot remove the initial model: " + e.getCause());
				throw new KPIModelException("Could not remove initial model in SIB", e);
			}

			Logger.debug(this.getClass().getName(), "Ontology model removed from SIB");
		}
	}

	/**
	 * Adds an abstract concept to the model
	 * @param concept the abstract concept representing an instantiated class
	 * @return the concept class
	 * @throws KPIModelException when concept is part of another {@link SmartModel}
	 * @throws NullPointerException when concept is null
	 */
	public IAbstractOntConcept add(IAbstractOntConcept iconcept) throws KPIModelException {
		final AbstractOntConcept concept = (AbstractOntConcept) iconcept;
		if (concept == null) {
			throw new NullPointerException("The added concept is null");
		}

		// Synchronize here to ensure that other threads can not insert/remove KPs and/or
		// change commands during the publication.
		synchronized(lock) {
			final boolean isConceptAlreadyAdded = concept._isPublished() || concept._isOutdated();
			
			if (isConceptAlreadyAdded) {
				final boolean isOurConcept = getIndividualInstance(concept)!=null;
				if(isOurConcept) {
					Logger.debug(concept._getIndividualID() + " is already published");
					return concept;
				}
                else {
	                throw new KPIModelException( concept._getIndividualID() + " belongs to a different model" );
                }
			}

			newConcepts.put(concept._getIndividualIRI(), concept);
			return concept;
		}
	}

	//-------------------------------------------------------------------------
	/**
	 * Registers an individual in the model if it is new.
	 * 
	 * @param individual		the individual to add
	 */
	public void addPublished(AbstractOntConcept individual) {
		if (individual == null) {
			throw new NullPointerException("individual==null");
		}

		this.conceptsByID.put(individual._getIndividualIRI(), individual);
//		if (conceptsByID.get(individual._getIndividualIRI()) == null) { // publish the new concept received from subscription/indication
//			this.conceptsByID.put(individual._getIndividualIRI(), individual);
//		}
	}


	/**
	 * Removes the AbstractKP from the list of classes that already belongs to the model
	 * @param kp the AbstractKP
	 * @return the AbstractKP removed
	 * @throws KPIModelException
	 */
	public IAbstractOntConcept remove(IAbstractOntConcept iconcept) throws KPIModelException {
		AbstractOntConcept concept = (AbstractOntConcept)iconcept;
		if(concept == null) {
			throw new KPIModelException("The KP can not be null");
		} else if(!concept._isPublished() && !concept._isOutdated()) {
			throw new KPIModelException(concept._getIndividualID() + " does not belong to any model");
		}

		// Synchronize here to ensure that other threads can not insert/remove KPs and/or
		// change commands during the publication.
		synchronized(lock) {
			try {
				removedConcepts.put(concept._getIndividualIRI(), concept);
				return concept;
			} catch (Exception ex) {
				ex.printStackTrace();
				throw new KPIModelException("Cannot remove the model: " + ex.getMessage(), ex);
			}
		}
	}

	/**
	 * Replaces the object instance that this SmartModel uses to represent
	 * a specific ontology concept.
	 * Each instance (individual IRI) of a concept in the SIB
	 * is represented by exactly one object in the SmartModel.
	 *
	 * @param concept 		the new concept instance to represent its concept in the smart model
	 * @return the previous concept
	 * @throws NullPointerException when <code>aoc==null</code>
	 * @throws KPIModelException	aoc does not represent a concept in this model
	 * 								(i.e., when {@link #(IAbstractOntConcept)}==null)
	 */
	public IAbstractOntConcept replaceIndividualInstance(IAbstractOntConcept aoc) throws KPIModelException {
		if (aoc == null) {
			throw new NullPointerException("The concept is null");
		}
		// Save us some casts
		final AbstractOntConcept _aoc = (AbstractOntConcept)aoc;

		// Synchronize here to ensure that other threads can not insert/remove KPs and/or
		// change commands.
		synchronized(lock) {
			final AbstractOntConcept previousConcept = conceptsByID.put( _aoc._getIndividualIRI(), _aoc );
			if((previousConcept != null)  && (previousConcept!=aoc)) {
				previousConcept._setInvalidated();
				return previousConcept;
//			} else {
//				conceptsByID.remove( _aoc._getIndividualIRI() );
//				throw new KPIModelException("There was not previous concept");
				// COMMENT Not needed to throw an exception
			}
			
			return null;
			
		}
	}

	/**
	 * Returns the concept instance representing the <i>individual IRI</i> of the
	 * provided concept.
	 *
	 * Each instance (individual IRI) of a concept in the SIB
	 * is represented by exactly one object in the SmartModel.
	 *
	 * Attempt for a 'work-around' for https://sg1.esilab.org/trac/smool/ticket/175
	 * This method allows a KP to retrieve the single object instance, that the SmartModel
	 * is able to handle as representing a concept in the SIB.
	 *
	 * @return	the object instance representing the provided individual (IRI)
	 * 			in this SmartModel or <code>null</code>, if it does not contain such an individual.
	 * 			Can be identical to <code>individual</code>
	 */
	@SuppressWarnings("unchecked")
    public <C extends IAbstractOntConcept> C getIndividualInstance(C individual ) {
		synchronized (lock) {
			return (C) conceptsByID.get( ((AbstractOntConcept) individual)._getIndividualIRI() );
        }
	}

	/**
	 * The method to publish all the operations made over the model, and
	 * send to the SIB
	 * @throws ConnectionErrorException when there was an error during data sending
	 */
	public void publish() throws KPIModelException {
		Logger.debug(this.getClass().getName(), "Publishing...");

		// Synchronize here to ensure that other threads can not insert/remove KPs and/or
		// change commands during the publication.
		synchronized(lock) {
			String insertData;
			String removeData;
			HashMap<NameAttribute,String> updateMap;

			try {
				// Checks if there are insertions, removes or updates
				insertData = checkInsertions(encoding);
				removeData = checkRemovals(encoding);
				updateMap = checkUpdates(encoding);
			} catch(KPIModelException e) {
				throw new KPIModelException("Could not create XMLs:" + e.getMessage(), e, e.getStatus());
			}

			try {
				if ((insertData == null) && (removeData == null) && ((updateMap == null) || (updateMap.size() == 0))) {
					Logger.debug("There is no data to insert/remove/update");
					return;
				}

				ComposedPendingTask cpt = new ComposedPendingTask();

				if ((removeData != null) && !removeData.equals("")) {
					cpt.addPendingSubtask(getSIB().remove(removeData, encoding));
				}

				if ((insertData != null) && !insertData.equals("")) {
					cpt.addPendingSubtask(getSIB().insert(insertData, encoding));
				}

				if ((updateMap != null) && (updateMap.get(NameAttribute.INSERTGRAPH) != null) &&
						(updateMap.get(NameAttribute.REMOVEGRAPH) != null)) {
					cpt.addPendingSubtask(getSIB().update(updateMap.get(NameAttribute.INSERTGRAPH), updateMap.get(NameAttribute.REMOVEGRAPH), encoding));
				}

				getSIB().publish();
				cpt.waitUntilFinished(this.getTimeout());

				if(!cpt.isFinished()) {
					throw new KPIModelException("Could not perform operation to SIB due to timeout");
				}

				HashMap<TransactionType,String> bNodeMap = new HashMap<TransactionType,String>();
				for (PendingTask pt : cpt.listPendingTasks()) {
					// Gets the status of the response
					SIBStatus status = (SIBStatus)pt.getProperty(NameAttribute.STATUS.getValue());

					if (!status.equals(SIBStatus.STATUS_OK)) {
						throw new KPIModelException("The disconnection cannot be executed due to " + status.getValue(), status);
					}

					String bNodes = (String)pt.getProperty(NameAttribute.BNODES.getValue());
					if (bNodes != null) {
						bNodeMap.put(pt.getType(), bNodes);
					}
				}


				// Removed the deleted KPs
				Iterator<AbstractOntConcept> itRemovedConcepts = removedConcepts.values().iterator();
				while(itRemovedConcepts.hasNext()) {
					AbstractOntConcept aoc = itRemovedConcepts.next();
					aoc._setInvalidated();
					conceptsByID.remove(aoc._getIndividualIRI());
				}
				
				// Include added aoc to the model
				Iterator<AbstractOntConcept> itNewConcepts = newConcepts.values().iterator();
				while(itNewConcepts.hasNext()) {
					AbstractOntConcept aoc = itNewConcepts.next();
					aoc._setPublished();
					conceptsByID.put(aoc._getIndividualIRI(), aoc);
				}

				executeCommands();

				newConcepts.clear();
				removedConcepts.clear();

				modelSupport.firePublished();
			} catch (KPIModelException e) {
				e.printStackTrace();
				throw new KPIModelException(e.getMessage(), e.getCause(), e.getStatus());
			} catch (Exception e) {
				e.printStackTrace();
				throw new KPIModelException(e.getMessage(), e.getCause());
			}
		}
	}

	/**
	 * This method check the classes to be sent to the SIB
	 * in an SSAP INSERT REQUEST message request
	 * @return
	 */
	private String checkInsertions(TypeAttribute encoding) throws KPIModelException {

		ArrayList<AbstractOntConcept> concepts = new ArrayList<AbstractOntConcept>();

		try {
			// Inserts the add concepts
			ArrayList<AbstractOntConcept> temp = new ArrayList<AbstractOntConcept>();
			if (newConcepts.size() > 0) {
				for (AbstractOntConcept newConcept : newConcepts.values()) {
					temp.add(newConcept);
					temp.addAll(newConcept._listSlotObjects(false));
				}
			}
			for(AbstractOntConcept tempConcept : temp) {
				if (!tempConcept._isPublished()) {
//				if (!conceptsByID.containsKey(tempConcept._getIndividualIRI())) {
					if (!concepts.contains(tempConcept)) {
						concepts.add(tempConcept);
					}
				}
			}
		} catch (Exception ex) {
			throw new KPIModelException("Cannot get the concepts to be added : " + ex.getMessage(), ex);
		}

		ArrayList<AbstractOntConcept> commandConcepts = new ArrayList<AbstractOntConcept>();
		ArrayList<AbstractOntConcept> unpublishedConcepts = new ArrayList<AbstractOntConcept>();

		try {
			// Checks if there are pending insert commands
			for(AbstractOntConcept kp : conceptsByID.values()) {
				if (kp.hasPendingInsertCommands() && !kp.hasPendingUpdateCommands()) {
					commandConcepts.add(kp);
				}
				commandConcepts.addAll(kp._listObjectsContainingInsertCommands(false));
			}

			for (AbstractOntConcept kp : newConcepts.values()) {
				commandConcepts.addAll(kp._listObjectsContainingCommands(false));
			}

			// Checks the references where the updated Concept is present
			for(AbstractOntConcept concept : commandConcepts) {
				unpublishedConcepts.addAll(concept._listUnpublishedInsertSlotObjects(false));
			}

		} catch (Exception ex) {
			throw new KPIModelException("Cannot include pending insert commands : " + ex.getMessage(), ex);
		}


		if ((concepts.size() > 0) || (commandConcepts.size() > 0)) {
			try {
				// Gets the processor
				AbstractEncodingProcessor processor = encodingProcessors.get(encoding);

				if(processor == null) {
					throw new KPIModelException("There is no encoding processor for the selected " + encoding.getValue());
				} else {
					return processor.getAddingContent(concepts, commandConcepts, unpublishedConcepts);
				}
			} catch (Exception ex) {
				throw new KPIModelException("Cannot generate "+ encoding.getValue()+" for the commands and added concepts :" + ex.getMessage(), ex);
			}
		}

		return null;
	}

	/**
	 * This method check the classes to be sent to the SIB
	 * in an SSAP REMOVE REQUEST message request
	 * @param encoding the encoding to be used to send data to the sib
	 * @return
	 */
	private String checkRemovals(TypeAttribute encoding) throws KPIModelException {

		ArrayList<AbstractOntConcept> derivedConcepts = new ArrayList<AbstractOntConcept>();

		try {
			// Checks the references where the removedConcept is present
			for(AbstractOntConcept concept : conceptsByID.values()) {
				for (AbstractOntConcept obsoleteConcept : removedConcepts.values()) {
					if (!removedConcepts.containsValue(concept)) {
						derivedConcepts.addAll(concept._listObjectsContainingObjectProperty(obsoleteConcept));
					}
				}
			}
		} catch (Exception ex) {
			throw new KPIModelException("Cannot get the concepts referencing to the concept to be removed : " + ex.getMessage(), ex);
		}

		ArrayList<AbstractOntConcept> commandConcepts = new ArrayList<AbstractOntConcept>();

		try {
			// Checks if there are pending remove commands
			for(AbstractOntConcept kp : conceptsByID.values()) {
				if (kp.hasPendingRemovalCommands() && !kp.hasPendingUpdateCommands()) {
					commandConcepts.add(kp);
				}
				commandConcepts.addAll(kp._listObjectsContainingRemoveCommands(false));
			}

		} catch (Exception ex) {
			throw new KPIModelException("Cannot include pending removal commands : " + ex.getMessage(), ex);
		}

		if ((removedConcepts.values().size() > 0) || (derivedConcepts.size() > 0) || (commandConcepts.size() > 0)) {
			try {
				// Gets the processor
				AbstractEncodingProcessor processor = encodingProcessors.get(encoding);

				if(processor == null) {
					throw new KPIModelException("There is no encoding processor for the selected " + encoding.getValue());
				} else {
					return processor.getRemovingContent(removedConcepts.values(), derivedConcepts, commandConcepts);
				}
			} catch (Exception ex) {
				throw new KPIModelException("Cannot generate "+ encoding.getValue()+" for the commands and added concepts :" + ex.getMessage(), ex);
			}
		}

		return null;
	}

	/**
	 * This method check the classes to be sent to the SIB
	 * in an SSAP UPDATE REQUEST message request
	 * @return
	 */
	private HashMap<NameAttribute, String> checkUpdates(TypeAttribute encoding) throws KPIModelException {
		ArrayList<AbstractOntConcept> commandConcepts = new ArrayList<AbstractOntConcept>();
		ArrayList<AbstractOntConcept> unpublishedConcepts = new ArrayList<AbstractOntConcept>();
		ArrayList<AbstractOntConcept> publishedConceptsToBeRemoved = new ArrayList<AbstractOntConcept>();

		try {
			for(AbstractOntConcept concept : conceptsByID.values()) {
				if (concept.hasPendingUpdateCommands()) {
					commandConcepts.add(concept);
				}
				commandConcepts.addAll(concept._listObjectsContainingCommands(false));
			}

			// Checks the references where the updated Concept is present
			for(AbstractOntConcept concept : commandConcepts) {
				unpublishedConcepts.addAll(concept._listUnpublishedUpdateSlotObjects(false));
			}

			// Checks the references where the updated Concept is present
			for(AbstractOntConcept concept : commandConcepts) {
				publishedConceptsToBeRemoved.addAll(concept._listPublishedRemoveSlotObjects(false));
			}

		} catch (Exception ex) {
			throw new KPIModelException("Cannot get the concepts referencing to the concept to be updated : " + ex.getMessage(), ex);
		}

		if (commandConcepts.size() > 0) {
			HashMap<NameAttribute,String> updateContent = new HashMap<NameAttribute,String>();

			try {
				// Gets the processor
				AbstractEncodingProcessor processor = encodingProcessors.get(encoding);

				if(processor == null) {
					throw new KPIModelException("There is no encoding processor for the selected " + encoding.getValue());
				} else {
					String insertData = processor.getUpdateInsertGraph(commandConcepts, unpublishedConcepts);
					String removeData = processor.getUpdateRemoveGraph(commandConcepts, publishedConceptsToBeRemoved);
					updateContent.put(NameAttribute.INSERTGRAPH, insertData);
					updateContent.put(NameAttribute.REMOVEGRAPH, removeData);
					return updateContent;
				}
			} catch (Exception ex) {
				throw new KPIModelException("Cannot generate "+ encoding.getValue()+" for the commands and added concepts :" + ex.getMessage(), ex);
			}

		}

		return null;
	}

	/**
	 * Execute the pending commands associated to the AbstractOntConcpets
	 */
	private void executeCommands() {
		// Execute the commands
		for (AbstractOntConcept concept : conceptsByID.values()) {
			if (concept.hasPendingExecutionCommands()) {
				concept.executeCommands();
				concept._setPublished();	// FIXME Hartmut: this is still very inefficient because it iterates over the commands twice
			}
		}
	}

	/**
	 * Determines if the connection is still alive
	 * @return <code>true</code> if the SIB reference is connected
	 */
	public boolean isConnected() {
		return getSIB().isConnected();
	}

	/**
	 * Queries to the SIB
	 * @param query the SPARQL query
	 * @return
	 * @return the obtained results
	 * @throws ConnectionErrorException
	 */
	public <C extends IAbstractOntConcept> List<C> query(Class<C> concept, TypeAttribute encoding) throws KPIModelException {
		if(concept == null) {
			throw new KPIModelException("The query concept cannot be null");
		}

		// Synchronize here to ensure that other threads can not insert/remove KPs and/or
		// change commands during the publication.
		synchronized(lock) {

			// Gets the processor
			AbstractQueryProcessor processor = queryProcessors.get(encoding);

			if(processor == null) {
				throw new KPIModelException("There is no query processor for the selected " + encoding.getValue());
			} else {
				return processor.query(concept);
			}
		}
	}

	/**
	 * Queries to the SIB
	 * @param concept the AbstractOntConcept the SPARQL query
	 * @return
	 * @return the obtained results
	 * @throws ConnectionErrorException
	 */
	@SuppressWarnings("unchecked")
	public <C extends IAbstractOntConcept> C query(Class<C> concept, String aocId, TypeAttribute encoding) throws KPIModelException {

		if(concept == null) {
			throw new KPIModelException("The query concept cannot be null");
		}

		// Synchronize here to ensure that other threads can not insert/remove KPs and/or
		// change commands during the publication.
		synchronized(lock) {

			// Gets the processor
			AbstractQueryProcessor processor = queryProcessors.get(encoding);

			if(processor == null) {
				throw new KPIModelException("There is no query processor for the selected " + encoding.getValue());
			} else {
				return (C) processor.query((Class<AbstractOntConcept>)concept, aocId);
			}
		}
	}

	/**
	 * Subscribes to the SIB
	 * @param concept the class to what to subscribe
	 * @param listener the object that will be notified when and INDICATION arrives
	 * @throws ConnectionErrorException
	 */
	public <C extends IAbstractOntConcept> void subscribe(Class<C> concept, ISubscription<? super C> as)
			throws KPIModelException {
		if((concept == null) || (as == null)) {
			throw new KPIModelException("The query and the subscription can not be null");
		}

		// Synchronize here to ensure that other threads can not insert/remove KPs and/or
		// change commands during the publication.
		synchronized(lock) {

			TypeAttribute encoding = as.getEncodingType();

			// Gets the processor
			AbstractQueryProcessor processor = queryProcessors.get(encoding);

			if(processor == null) {
				throw new KPIModelException("There is no query processor for the selected " + encoding.getValue());
			} else {
				processor.subscribe(concept, as);
			}
		}
	}

	/**
	 * Subscribes to the SIB
	 * @param concept the AbstractOntConcept the SPARQL query
	 * @return
	 * @return the obtained results
	 * @throws ConnectionErrorException
	 */
	public <C extends IAbstractOntConcept> void subscribe(Class<C> concept, String aocId, AbstractSubscription<? super C> as) throws KPIModelException {

		if((concept == null) || (as == null)) {
			throw new KPIModelException("The query and the subscription can not be null");
		}

		// Synchronize here to ensure that other threads can not insert/remove KPs and/or
		// change commands during the publication.
		synchronized(lock) {

			TypeAttribute encoding = as.getEncodingType();

			// Gets the processor
			AbstractQueryProcessor processor = queryProcessors.get(encoding);

			if(processor == null) {
				throw new KPIModelException("There is no query processor for the selected " + encoding.getValue());
			} else {
				processor.subscribe(concept, aocId, as);
			}
		}
	}

	/**
	 * Unsubscribes from the SIB
	 * @param concept the class to what to subscribe
	 * @param listener the object that will be notified when and INDICATION arrives
	 * @return the obtained results
	 * @throws KPIModelException
	 */
	public boolean unsubscribe(ISubscription<? extends IAbstractOntConcept> as) throws KPIModelException {

		if(as == null) {
			throw new KPIModelException("The abstract subscription cannot be null to unsubscribe");
		}

		// Synchronize here to ensure that other threads can not insert/remove KPs and/or
		// change commands during the publication.
		synchronized(lock) {

			TypeAttribute encoding = as.getEncodingType();
			// Gets the processor
			AbstractQueryProcessor processor = queryProcessors.get(encoding);



			if(processor == null) {
				throw new KPIModelException("There is no query processor for the selected " + encoding.getValue());
			} else {
				return processor.unsubscribe(as);
			}
		}
	}

	/**
	 * Adds a listener
	 * @param listener the IModelListener
	 */
    @Override
	public void addModelListener(IModelListener listener) {
		modelSupport.addListener(listener);
	}

	/**
	 * Removes a listener
	 * @param listener the IModelListener
	 */
    @Override
	public void removeModelListener(IModelListener listener) {
		modelSupport.removeListener(listener);
	}

	//-------------------------------------------------------------------------
    /**
     * Returns the locking object of this model.
     * @return	the locking object of this smart model
     */
    public final Object getLock() {
	    return lock;
    }

	/**
	 * Gets the concept name based on the provided URL
	 * @param conceptURL the URL of the ontology class
	 * @return a package format of the implemented class. <code>null</code> if not defined for the current KP
	 */
	public String getConceptName(String conceptURL) {
		return ontologyMapping.get(conceptURL);
	}
}
