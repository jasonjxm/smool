/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.connector;

import java.util.Properties;

import org.smool.kpi.connector.exception.KPIConnectorException;


public abstract class AbstractConnector {
	
	protected String name;
	protected ConnectorMessageSupport messageListeners;
	protected ConnectionSupport connectionListeners;
	protected Properties properties;
	
	/** Indicates if the connector must keep the connection open after sending a message*/
	protected boolean keepAlive;
	
	public AbstractConnector() {
		messageListeners = new ConnectorMessageSupport();
		connectionListeners = new ConnectionSupport();
	}
	
	public abstract void write(byte[] msg);
	
	public abstract String getName();
	public abstract void connect() throws KPIConnectorException;
	public abstract void disconnect() throws KPIConnectorException;
	
	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	public Properties getProperties() {
		return this.properties;
	}
	
	public String getProperty(String key) {
		if(this.properties == null) {
			return null;
		} else {
			return this.properties.getProperty(key);
		}
	}
	
	public void addSIBMessageListener(IConnectorMessageListener listener) {
		messageListeners.addListener(listener);
	}

	public void removeSIBMessageListener(IConnectorMessageListener listener) {
		messageListeners.removeListener(listener);
	}
	
	public void addConnectionListener(IConnectionListener listener) {
		connectionListeners.addListener(listener);
	}

	public void removeConnectionListener(IConnectionListener listener) {
		connectionListeners.removeListener(listener);
	}
	
	public void setKeepAlive(boolean keepAlive) {
		this.keepAlive = keepAlive;
	}

	public boolean keepAlive() {
		return this.keepAlive;
	}
	

}
