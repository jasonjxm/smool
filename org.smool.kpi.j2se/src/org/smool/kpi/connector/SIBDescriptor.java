/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/

package org.smool.kpi.connector;

import java.util.Properties;

/**
 * The descriptor that identifies a SIB
 */
public class SIBDescriptor implements Comparable<SIBDescriptor>{

	private String sibName;
	private String connectorName;
	private Properties properties;

	public SIBDescriptor(String sibName, String connectorName, Properties properties) {
		this.sibName = sibName;
		this.connectorName = connectorName;
		this.properties = properties;
	}

	public String getSIBName() {
		return sibName;
	}

	public String getConnectorName() {
		return connectorName;
	}

	public Properties getProperties() {
		return properties;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("SIB: ");
		sb.append(sibName);
		sb.append(", protocol: ");
		sb.append(connectorName);
		sb.append(", properties: ");
		sb.append(properties);
		return sb.toString();
	}

	/**
	 * Allows the equality comparison to another object
	 * @param obj the object to be compared to the current triple
	 * @return <code>true</code> if the triple is the same
	 */	
	@Override
	public int compareTo(SIBDescriptor sd) {
		if (!this.getSIBName().equals(sd.getSIBName())) {
			return this.getSIBName().compareTo(sd.getSIBName());
		} else if (!this.getConnectorName().equals(sd.getConnectorName())) {
			return this.getConnectorName().compareTo(sd.getConnectorName());
		}
		return 0;
	}
	
	/**
	 * Our implementation of hashCode
	 */
	@Override
    public int hashCode() {
	    final int prime = 31;
	    int result = 1;
	    result = (prime * result)
	            + ((connectorName == null) ? 0 : connectorName.hashCode());
	    result = (prime * result) + ((sibName == null) ? 0 : sibName.hashCode());
	    return result;
    }

	/**
	 * Allows the equality comparison to another object
	 * @param obj the object to be compared to the current triple
	 * @return <code>true</code> if the triple is the same
	 */	
	@Override
    public boolean equals( Object obj ) {
	    if (this == obj) {
	        return true;
        }
	    if (obj == null) {
	        return false;
        }
	    if (getClass() != obj.getClass()) {
	        return false;
        }
	    SIBDescriptor other = (SIBDescriptor) obj;
	    if (connectorName == null) {
		    if (other.connectorName != null) {
	            return false;
            }
	    }
	    else if (!connectorName.equals( other.connectorName )) {
	        return false;
        }
	    if (sibName == null) {
		    if (other.sibName != null) {
	            return false;
            }
	    }
	    else if (!sibName.equals( other.sibName )) {
	        return false;
        }
	    return true;
    }

	public boolean canEqual( Object other ) {
		return other instanceof SIBDescriptor;
	}


}
