package org.smool.kpi.connector.impl.zigbee;

@SuppressWarnings("serial")
public class MessageNotCompleteException extends Exception{
	
	public MessageNotCompleteException(){
		super("The message is not completed yet");
	}

}
