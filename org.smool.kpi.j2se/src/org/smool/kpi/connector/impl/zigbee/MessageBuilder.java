package org.smool.kpi.connector.impl.zigbee;

import java.util.Hashtable;

public class MessageBuilder {
	
	public final static int MAX_TIME_TO_DELETE=30000;
	private static MessageBuilder instance=null;
	private Hashtable<Integer, Hashtable<Integer,ConcreteMessage>> htdevices;
	
	
	private MessageBuilder(){
		this.htdevices=new Hashtable<Integer, Hashtable<Integer,ConcreteMessage>>();
	}
	
	
	public static MessageBuilder getInstance(){
		if(instance==null)
			instance=new MessageBuilder();
		
		return instance;
	}
	
	public synchronized void addNewMessage(int _addrIdentifier, int _idMessage, ConcreteMessage _message){
		Hashtable<Integer,ConcreteMessage> htMessage=this.htdevices.get(_addrIdentifier);
	
		if(htMessage==null)
			htMessage=new Hashtable<Integer,ConcreteMessage>();
		
		htMessage.put(_idMessage, _message);
		
		this.htdevices.put(_addrIdentifier, htMessage);
	
	}
	
	public synchronized ConcreteMessage getMessage(int _addrIdentifier, int _idMessage){
		Hashtable<Integer,ConcreteMessage> htMessage=this.htdevices.get(_addrIdentifier);
		if(htMessage==null)
			return null;
		else{
			return htMessage.get(_idMessage);
		}
		
		
	}
	
	public synchronized void deleteMessage(int _addrIdentifier, int _idMessage){
		Hashtable<Integer,ConcreteMessage> htMessage=this.htdevices.get(_addrIdentifier);
		if(htMessage!=null){
			htMessage.remove(_idMessage);
		}
		
		//Checks if there are more messages associated to the addess, if not, delete the address
		if(htMessage.isEmpty())
			this.htdevices.remove(_addrIdentifier);
		
		//Now checks the addess by address and message by message which of them must be deleted due to maximun life-time reached
		long currentTime=new java.util.Date().getTime();
		for(int devIdentifier:this.htdevices.keySet()){
			Hashtable<Integer,ConcreteMessage> htCurrentDev=this.htdevices.get(devIdentifier);
			
			for(int messageIdentifier:htCurrentDev.keySet()){
				long lastTime=htCurrentDev.get(messageIdentifier).getLastUpdated();
				
				if(currentTime-lastTime>MAX_TIME_TO_DELETE){
					htCurrentDev.remove(messageIdentifier);
				}
			}
			if(htCurrentDev.isEmpty()){
				this.htdevices.remove(devIdentifier);
			}
		}
		
	}
	
}
