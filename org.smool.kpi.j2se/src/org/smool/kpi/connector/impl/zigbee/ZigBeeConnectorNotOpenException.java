package org.smool.kpi.connector.impl.zigbee;

@SuppressWarnings("serial")
public class ZigBeeConnectorNotOpenException extends Exception{
	
	public ZigBeeConnectorNotOpenException(String _port, int _baudRate){
		super("Can not open serial port "+_port+" at "+_baudRate+" bauds");
	}

}
