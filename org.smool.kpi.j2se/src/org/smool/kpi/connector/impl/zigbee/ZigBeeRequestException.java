package org.smool.kpi.connector.impl.zigbee;


@SuppressWarnings("serial")
public class ZigBeeRequestException extends Exception{
	
	public ZigBeeRequestException(String _strAddr){
		super("Can not send message to "+_strAddr+" device");
	}

}
