package org.smool.kpi.connector.impl.zigbee;
	
import java.util.Properties;

import org.smool.kpi.connector.AbstractSIBDiscoverer;
import org.smool.kpi.connector.SIBDescriptor;
import org.smool.kpi.connector.exception.KPIConnectorException;

import com.rapplogic.xbee.api.ApiId;
import com.rapplogic.xbee.api.AtCommand;
import com.rapplogic.xbee.api.AtCommandResponse;
import com.rapplogic.xbee.api.PacketListener;
import com.rapplogic.xbee.api.XBee;
import com.rapplogic.xbee.api.XBeeAddress64;
import com.rapplogic.xbee.api.XBeeResponse;
import com.rapplogic.xbee.api.zigbee.NodeDiscover;
import com.rapplogic.xbee.api.zigbee.ZNetRxResponse;
import com.rapplogic.xbee.api.zigbee.ZNetTxRequest;
import com.rapplogic.xbee.util.ByteUtils;


public class ZigbeeSIBDiscoverer extends AbstractSIBDiscoverer implements Runnable{
	
	
	private XBee xbee;
	
	private Thread discovererThread;
	
	public ZigbeeSIBDiscoverer(){
		this.xbee=new XBee();
		
		this.discovererThread=new Thread(this);
	}
	
	
	
	
	@Override
	public String getConnectorName() {
		return ZigbeeConnector.CONNECTOR_NAME;
	}

	@Override
	public void inquiry() throws KPIConnectorException {
		this.discovererThread.start();
	}

	@SuppressWarnings("deprecation")
	@Override
	public void stopInquiry() throws KPIConnectorException {
		try{
			this.discovererThread.stop();
			this.xbee.close();
			this.discoveryListener.connectorSIBDiscoveryFinished(ZigbeeConnector.CONNECTOR_NAME);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	
	public void run(){
		try {
			
			Properties zbPropertiesFile=new Properties();
			try{
				zbPropertiesFile.load(getClass().getResourceAsStream("zigbee.properties"));	
			}catch(Exception e){
				e.printStackTrace();
			}
			
			xbee.open((String)zbPropertiesFile.get(ZigbeeConnector.SERIAL_PORT), Integer.parseInt((String)zbPropertiesFile.get(ZigbeeConnector.RATE)));
			
			// get the Node discovery timeout
			AtCommandResponse nodeTimeout = (AtCommandResponse)xbee.sendSynchronous(new AtCommand("NT"));
			 
			
			// default is 6 seconds
			long nodeDiscoveryTimeout = ByteUtils.convertMultiByteToInt(nodeTimeout.getValue()) * 100;
			
						
			xbee.addPacketListener(new PacketListener() {
				
				public void processResponse(XBeeResponse response) {
					if (response.getApiId() == ApiId.AT_RESPONSE) {
						NodeDiscover nd = NodeDiscover.parse((AtCommandResponse)response); //It is a candidate to be a gateway of a SIB, we send
																						   //a query to confirm if it is a SIB or not
														
						
						XBeeAddress64 addr=nd.getNodeAddress64();
						
						ZNetTxRequest isSIBRequest=new ZNetTxRequest(addr,new int[]{'s','i','b','?'});
						//Preguntar por el SIB
						try{
							xbee.sendAsynchronous(isSIBRequest);
			
						}catch(Exception e){
							//No es SIB
							e.printStackTrace();
						}
						
					} else if(response.getApiId()== ApiId.ZNET_RX_RESPONSE) {
						ZNetRxResponse echoResp=(ZNetRxResponse)response;
						int[] data=echoResp.getData();
						
						StringBuilder sibName=new StringBuilder();
						if(data.length>3){
							int[] bName=new int[data.length-3];
							
							System.arraycopy(data, 3, bName, 0, data.length-3);
							for(int i=0;i<bName.length;i++)
								sibName.append((char)bName[i]);
							
							
							Properties zbPropertiesFile=new Properties();
							try{
								zbPropertiesFile.load(getClass().getResourceAsStream("zigbee.properties"));	
							}catch(Exception e){
								e.printStackTrace();
							}
							
							
							Properties prop=new Properties();
							prop.put(ZigbeeConnector.SERIAL_PORT, zbPropertiesFile.get(ZigbeeConnector.SERIAL_PORT));
							prop.put(ZigbeeConnector.RATE, zbPropertiesFile.get(ZigbeeConnector.RATE));
							
							
							int[] addr=echoResp.getRemoteAddress64().getAddress();
							String strAddr=new String();
							for(int i=0;i<addr.length;i++){
								strAddr+=addr[i];
								if(i < addr.length){
									strAddr+=" ";
								}
							}
							
							prop.put(ZigbeeConnector.REMOTE_ADDRESS, strAddr);
							
							
							SIBDescriptor sd=new SIBDescriptor(sibName.toString(), getConnectorName(), prop);
							
							discoveryListener.connectorSIBDiscovered(sd);
							
						}
					}					
				}
			});
						
			xbee.sendAsynchronous(new AtCommand("ND"));
			
			// wait for nodeDiscoveryTimeout milliseconds
			Thread.sleep(nodeDiscoveryTimeout);
			
		}catch(Exception e){
			
			e.printStackTrace();
			
			//log.debug("Time is up!  You should have heard back from all nodes by now.  If not make sure all nodes are associated and/or try increasing the node timeout (NT)");
		} finally {
			this.discoveryListener.connectorSIBDiscoveryFinished(ZigbeeConnector.CONNECTOR_NAME);
			xbee.close();
		}
	}
}
