package org.smool.kpi.connector.impl.zigbee;

import java.util.ArrayList;
import java.util.Properties;
import java.util.StringTokenizer;

import org.smool.kpi.connector.AbstractConnector;
import org.smool.kpi.connector.exception.KPIConnectorException;

import com.rapplogic.xbee.api.XBeeAddress64;


import org.smool.kpi.connector.impl.zigbee.ZigBeeConnectorNotOpenException;

public class ZigbeeConnector extends AbstractConnector {

	public final static String SERIAL_PORT="SERIAL_PORT";
	public final static String RATE="RATE";
	public final static String REMOTE_ADDRESS="REMOTE_ADDRESS";
	
	public final static String CONNECTOR_NAME="ZigBee";
	
	private ZigBeeServer zbServer;
	
	
	public ZigbeeConnector(){
		this.name=CONNECTOR_NAME;
		
	}
	
	@Override
	public void write(byte[] _msg) {
		try{
			StringTokenizer strtk=new StringTokenizer(properties.getProperty(ZigbeeConnector.REMOTE_ADDRESS)," ");
			
			ArrayList<Integer> ls=new ArrayList<Integer>();
			while(strtk.hasMoreElements()){
				String tk=strtk.nextToken();
				ls.add(new Integer(Integer.parseInt(tk)));
			}
			
			int[] addrint=new int[ls.size()];
			for(int i=0;i<ls.size();i++){
				addrint[i]=ls.get(i).intValue();
			}
			
			XBeeAddress64 address=new XBeeAddress64(addrint);
			
			this.zbServer.send(_msg, address);
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	
	public void notifyResponse(byte[] vData){
		this.messageListeners.fireMessageReceived(vData);
	}
	
	
	@Override
	public void connect() throws KPIConnectorException {
		try {
			this.zbServer=new ZigBeeServer(this.properties.getProperty(ZigbeeConnector.SERIAL_PORT), this.properties.getProperty(ZigbeeConnector.RATE), this);
			
			this.zbServer.openServer();
		} catch (ZigBeeConnectorNotOpenException e) {
			e.printStackTrace();
			throw new KPIConnectorException("Could not connect due to: "+e.getMessage());
		}
		
	}

	@Override
	public void disconnect() throws KPIConnectorException {
		this.zbServer.closeServer();
	}

	@Override
	public String getName() {
		return ZigbeeConnector.CONNECTOR_NAME;
	}
	
	
	@Override
	public void setProperties(Properties properties) {
		super.setProperties(properties);
	}

	

}
