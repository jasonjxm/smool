package org.smool.kpi.connector.impl.zigbee;

@SuppressWarnings("serial")
public class ZigBeePropertiesFileNotFoundException extends Exception{
	
	public ZigBeePropertiesFileNotFoundException(String _msg){
		super(_msg);
	}

}
