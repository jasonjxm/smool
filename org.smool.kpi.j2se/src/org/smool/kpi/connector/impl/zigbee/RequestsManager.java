package org.smool.kpi.connector.impl.zigbee;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.smool.kpi.connector.impl.zigbee.ConcreteMessage;
import org.smool.kpi.connector.impl.zigbee.MessageBuilder;

import com.rapplogic.xbee.api.ApiId;
import com.rapplogic.xbee.api.PacketListener;
import com.rapplogic.xbee.api.XBeeAddress16;
import com.rapplogic.xbee.api.XBeeResponse;
import com.rapplogic.xbee.api.zigbee.ZNetRxResponse;


public class RequestsManager implements PacketListener{
	
	private ZigbeeConnector zbConnector;
	private ExecutorService responsesProcessor;
	
	public RequestsManager(ZigbeeConnector _zbConnector){
		super();
		this.zbConnector=_zbConnector;
		this.responsesProcessor=Executors.newSingleThreadExecutor();
	}
	
	@Override
	public void processResponse(final XBeeResponse response){
		
		this.responsesProcessor.submit(new Runnable(){
				public void run(){
					if (response.getApiId() == ApiId.ZNET_RX_RESPONSE) {//It is a message from a KP
						
						ZNetRxResponse rx = (ZNetRxResponse)response;
						
						int[] receivedData=rx.getData();
						
						
						XBeeAddress16 remoteAddress=rx.getRemoteAddress16();
						int addrIdentifier=remoteAddress.get16BitValue();
									
						int idMensaje=receivedData[0];
						int idTrama=receivedData[1];
						int numTramas=1;
						
						int[] messageData;
						
						if(idTrama==0){//first weft, the following int contains the number of messages that will composes the entire message
							numTramas=receivedData[2];
							messageData=new int[receivedData.length-3];
							System.arraycopy(receivedData, 3, messageData, 0, receivedData.length-3);
						}else{
							messageData=new int[receivedData.length-2];
							System.arraycopy(receivedData, 2, messageData, 0, receivedData.length-2);
						}
						
						MessageBuilder mb=MessageBuilder.getInstance();
						
						ConcreteMessage current=mb.getMessage(addrIdentifier, idMensaje);
						if(current==null){
							current=new ConcreteMessage(idMensaje);
							mb.addNewMessage(addrIdentifier, idMensaje, current);
						}
				
						if(idTrama==0){
							current.setNumParts(numTramas);
						}
						
						current.addPart(idTrama, messageData);
						
						if(current.isCompleted()){
							try{
								int[] message=current.getMessage();
								
								StringBuilder sb=new StringBuilder();
								for(int i=0;i<message.length;i++){
									sb.append((char)message[i]);
								}
								
								zbConnector.notifyResponse(sb.toString().getBytes());
								
								mb.deleteMessage(addrIdentifier, idMensaje);
								
							}catch(Exception e){
								e.printStackTrace();
							}
						}
							
						
						
					}
				}
			});
		
	}

}
