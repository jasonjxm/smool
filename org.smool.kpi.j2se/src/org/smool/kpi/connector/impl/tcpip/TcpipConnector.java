/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.connector.impl.tcpip;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.LinkedBlockingQueue;

import org.smool.kpi.common.Logger;
import org.smool.kpi.connector.AbstractConnector;
import org.smool.kpi.connector.ConnectionSupport;
import org.smool.kpi.connector.exception.KPIConnectorException;


public class TcpipConnector extends AbstractConnector implements Runnable {

	//host is maintained to keep backwards compatibility <=2.0.3 release
	private static final String HOST = "HOST";
	private static final String DEFAULT_IPADDRESS = "DEFAULT_IPADDRESS";
	private static final String PORT = "PORT";
	private static final String KEEP_ALIVE = "KEEP_ALIVE";
	/** This is used only for testing purpose, connecting to NOKIA SIB, should not be used otherwise */
	private static boolean NOKIA_SIB = false;
	
	private Thread thread;
	
	/** Connection to server */
	private SocketChannel channel;

	/** Queue for incoming events */
	private LinkedBlockingQueue<ClientMessage> inQueue;
	
	/** Queue for outgoing events */
	private LinkedBlockingQueue<ClientMessage> outQueue;

	/** Lock to avoid expensive run() */
	private Object lock;
	
	/** Reference to NIOEventReader that reads events from the server */
	private SocketChannelReader reader;

	/** Buffer for outgoing events */
	private ByteBuffer writeBuffer;

	/** Still running? */
	private boolean running;
	
    public TcpipConnector() throws IOException {
    	super();
		this.inQueue = new LinkedBlockingQueue<ClientMessage>();
		this.outQueue = new LinkedBlockingQueue<ClientMessage>();
		this.writeBuffer = ByteBuffer.allocate(TCPIPConfiguration.BUFFER_SIZE);
		this.lock = new Object();
		
		// start the reader (without a channel)
		this.reader = new SocketChannelReader(this);
		
		// start processing incoming/outgoing messages.
		this.thread = new Thread(this, "TcpipConnector");
		this.thread.start();
    }
    
    
	/** 
	 * This is the threadeable method. Checks if there are pending messages in the output and input queues and
	 * process them. Waiting condition is whether any of the queues have at least a message
	 * 
	 * @return	None
	 */
	public void run() {
		running = true;
		while(running) {
			try {
				if(inQueue.size() == 0 && outQueue.size() == 0) {
					synchronized(lock) {
						lock.wait();
					}
				}
				//Check if we have to reconnect, only when KEEP_ALIVE is set to false
				if (!this.keepAlive){
					connect();
				}
				processIncomingMessages();
				writeOutgoingMessages();
				//Ask if we should close the connection once the queue has been processed
				//Check if we have to reconnect, only when KEEP_ALIVE is set to false
				if (!this.keepAlive){
					disconnect();
				}
				
			} catch (KPIConnectorException cex) {
				cex.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		Logger.debug(this.getClass().getName(), "Client TCP/IP connector ends");
	}

	/** 
	 * Handle incoming messages from the inQueue.
	 * 
	 * @return	None
	 * 
	 */
	protected void processIncomingMessages() throws KPIConnectorException {
		try {
			if(inQueue.size() > 0) {
			
				ClientMessage msg;
				while (inQueue.size() > 0) {
					msg = inQueue.take();
					Logger.debug(this.getClass().getName(), "Received: " + msg);
					this.messageListeners.fireMessageReceived(msg.getPayload());
				}	

			}
		} catch(InterruptedException iex) {
			iex.printStackTrace();
			throw new KPIConnectorException("The socket is interrupted: " + iex.getMessage(), iex);
		}
	}
	
	
	/** 
	 * A test method to be used only when connecting to the SIB from NOKIA in the writeOutgoingMessages method
	 * This method returns what is contained among two XML tags from an String representing an XML fragment
	 * 
	 * @param	xml		The string containing the XML fragment
	 * @param	tag		The tag to look for in the XML fragment, i.e. parent, <> are not required.
	 * 
	 * @return	The string containing the value of the tag searched
	 */
	private String getTagContent(String xml, String tag) {
		
		String opener = getOpener(tag);
		String closer = getCloser(tag);
		int from = xml.indexOf(opener) + opener.length();
		int to = xml.indexOf(closer);
		return xml.substring(from, to);
	}

	/** 
	 * A test method to be used only when connecting to the SIB from NOKIA in the writeOutgoingMessages method
	 * Returns the opener tag for a given tag
	 * 
	 * @param	tag		The tag from which the opener must be created
	 * 
	 * @return	The string containing the opener for the given tag
	 */
	private String getOpener(String tag) {
		
		StringBuilder sb = new StringBuilder();
		sb.append("<");
		sb.append(tag);
		sb.append(">");
		return sb.toString();
	}

	/** 
	 * A test method to be used only when connecting to the SIB from NOKIA in the writeOutgoingMessages method
	 * Returns the closer tag for a given tag
	 * 
	 * @param	tag		The tag from which the opener must be created
	 * 
	 * @return	The string containing the opener for the given tag
	 */
	private String getCloser(String tag) {
		
		StringBuilder sb = new StringBuilder();
		sb.append("</");
		sb.append(tag);
		sb.append(">");
		return sb.toString();
	}
	
	/** 
	 * A test method to be used only when connecting to the SIB from NOKIA in the writeOutgoingMessages method
	 * Returns the string representation of a ArrayList containing RDF-M3 triples
	 * 
	 * @param	tripleList		The ArrayList containing the RDF-M3 triples
	 * 
	 * @return	The string containing the RDF-M3 triples as a String
	 */
    private String getTripleListFromTripleList( ArrayList<ArrayList<String>> tripleList )
    {	
    	
    	String ANYURI = "http://www.nokia.com/NRC/M3/sib#any";
    	String list="<triple_list>";
    	
        for(int i=0; i< tripleList.size() ;i++){
        	
        	String s=tripleList.get(i).get(0);
        	String p=tripleList.get(i).get(1);
        	String o=tripleList.get(i).get(2);
        	String st=s==null?"URI":tripleList.get(i).get(3);
        	String ot=o==null?"URI":tripleList.get(i).get(4);
         
        	list=list
            +" <triple>"        
            +"  <subject type=\""+st+"\">"+ (s==null?ANYURI:s)+"</subject>"
            +"  <predicate>"+                                                   (p==null?ANYURI:p)+"</predicate>"
            +"  <object type=\"" +ot+"\">"+ (o==null?ANYURI:o)+"</object>"
            +" </triple>";
        }
        return list+"</triple_list>";
    }

    /** 
	 * A test method to be used only when connecting to the SIB from NOKIA in the writeOutgoingMessages method
	 * Creates a new RDF-M3 triple, which is an ArrayList<String> where:
	 * 
	 * Position 0: Its the subject
	 * Position 1: Its the predicate
	 * Position 2: Its the object
	 * Position 3: Its the type for the subject
	 * Position 4: Its the type for the object
	 * 
	 * @param s the string representation of the subject. The value 'null' means any value
     * @param p the string representation of the predicate. The value 'null' means any value
     * @param o the string representation of the object. The value 'null' means any value
     * @param s_type the string representation of the subject type. Allowed values are: uri, literal
     * @param o_type the string representation of the object type. Allowed values are: uri, literal
	 * 
	 * @return	An ArrayList<String> that represents a RDF-M3 triple
	 */
	private ArrayList<String> newTriple(String s,String p,String o, String s_type, String o_type){
        
		ArrayList<String> triple=new ArrayList<String>();
		String ANYURI = "http://www.nokia.com/NRC/M3/sib#any";
        
        triple.add(0, (s==null?ANYURI:s) ); 
        triple.add(1, (p==null?ANYURI:p) );
        triple.add(2, (o==null?ANYURI:o) );
        triple.add(3, s_type);
        triple.add(4, o_type);

        return triple;
	}
	
	

	/** 
	 * Write all events waiting in the outQueue.
	 * 
	 * @return	None
	 * 
	 */
	private void writeOutgoingMessages() throws KPIConnectorException {
		try {
			
			ClientMessage msg;
			if(outQueue.size() > 0) {
		
				while (outQueue.size() > 0) {
					msg = outQueue.take();
					long time = System.currentTimeMillis();
					//NOTE: A test to see compatibility with Nokia SIB. If the message is an INSERT, change payload and insert
					//a new INSERT with pure RDF-M3 contents, since it seems that nokia SIB doesn't like RDF-XML encoding
					//Should be deleted when enconding in RDF-M3 is included/supported
					String payload = new String(msg.getPayload());
					//just for testing purposes
					if (payload.contains("<transaction_type>INSERT</transaction_type>") && NOKIA_SIB)
					{
						String transactionId = getTagContent(payload, "transaction_id");
						String nodeId = getTagContent(payload, "node_id");
						String spaceId = getTagContent(payload, "space_id");
						String newPayload = "<SSAP_message>"
					     +"<message_type>REQUEST</message_type>"
					     +"<transaction_type>INSERT</transaction_type>"
					     +"<transaction_id>"+ transactionId  + "</transaction_id>"
					     +"<node_id>" + nodeId + "</node_id>"
					     +"<space_id>"+ spaceId +"</space_id>"
					     +"<parameter name=\"confirm\">TRUE</parameter>"
					     +"<parameter name=\"insert_graph\"  encoding=\"RDF-M3\">";
					    
						ArrayList<ArrayList<String>> triples = null;
						triples = new ArrayList<ArrayList<String>>();
						String URI = "URI";
						String LITERAL = "literal";
							
						triples.add( newTriple( "Person_01", "rdf:type", "Person", URI, URI) );
						triples.add( newTriple( "Person_02", "rdf:type", "Person", URI, URI) );

						triples.add( newTriple( "Person_01", "HasName", "Fabio", URI, LITERAL) );
						triples.add( newTriple( "Person_02", "HasName", "Guido", URI, LITERAL) );

						triples.add( newTriple( "Environment_01", "rdf:type", "Environment", URI, URI) );
						triples.add( newTriple( "Environment_02", "rdf:type", "Environment", URI, URI) );

						triples.add( newTriple( "Environment_01", "HasFriendlyName", "Room_01", URI, LITERAL) );
						triples.add( newTriple( "Environment_02", "HasFriendlyName", "Room_02", URI, LITERAL) );

						triples.add( newTriple( "Environment_01", "ContainsEntity", "Person_01", URI, URI) );
						triples.add( newTriple( "Environment_02", "ContainsEntity", "Person_02", URI, URI) );
							
						triples.add(newTriple( "Person", "rdfs:subClassOf", "thing", URI, URI) );
						triples.add(newTriple( "Environment", "rdfs:subClassOf", "thing", URI, URI) );

						newPayload=newPayload+this.getTripleListFromTripleList(triples);					     
						newPayload=newPayload +"</parameter>" + "</SSAP_message>";
						msg = new ClientMessage(newPayload.getBytes());
						writeMessage(msg);
					}
					else{
						writeMessage(msg);
					}
					Logger.debug(this.getClass().getName(), new StringBuilder("Sent message in ")
						.append((System.currentTimeMillis()-time))
						.append(" ms: ")
						.append(msg).toString());
				}
			}
		} catch (KPIConnectorException cex) {
			throw new KPIConnectorException("Cannot write an outgoing message: " + cex.getMessage(), cex);
		} catch (InterruptedException iex) {
			throw new KPIConnectorException("The socket is interrupted: " + iex.getMessage(), iex);
		}
	}

	/**
	 * Connects to a SIB using TCP/IP
	 * 
	 * @return	None
	 */
	public synchronized void connect() throws KPIConnectorException {
		if(this.channel != null && this.channel.isConnected()) {
			return;
		}
		try {
			//set the running flag to true
			this.running=true;
			String host = this.properties.getProperty(TcpipConnector.HOST);
			String defaultIPAddress = this.properties.getProperty(TcpipConnector.DEFAULT_IPADDRESS);
			int port = Integer.parseInt(this.properties.getProperty(TcpipConnector.PORT));
			//This is done to keep backwards compatibility with previous KP that did not specifically set the KEEP_ALIVe
			//flag when setting the connector properties. 
			boolean keep_alive = Boolean.parseBoolean(this.properties.getProperty(TcpipConnector.KEEP_ALIVE));
			//Keep Alive equals false should only occuer when the NOKIA_SIB flag is set to true, meaning that our KP is connecting
			//to NOKIA's SIB, otherwise will always be TRUE cuz otherwise this KP won't work well with our SIB
			if (TcpipConnector.NOKIA_SIB && !keep_alive){
				this.keepAlive = false;
			}
			else{
				this.keepAlive = true;
			}
			
			InetSocketAddress inetAddress;
			
			//this is done to keep backward compatibility prior 2.0.4 release
			if(host == null || host.equals("")) {
				//check if defaultIp is also empty
				if (defaultIPAddress == null || defaultIPAddress.equals(""))
					throw new UnknownHostException();
				//otherwise start listening on defaultIp
				else
				{
					inetAddress = new InetSocketAddress(defaultIPAddress, port);
				}
			}
			//start listening on host
			else
			{
				inetAddress = new InetSocketAddress(host, port);
			}
				
			// open the socket channel
			this.channel = SocketChannel.open(inetAddress);
			this.channel.configureBlocking(false);
			this.channel.socket().setTcpNoDelay(true);
			
			//Check if the reader channel is not null, may happen if a disconnect has ocurred
			if (this.reader==null)
				this.reader = new SocketChannelReader(this);
			
			// assign the channel to the reader.
			this.reader.setChannel(this.channel);
			this.reader.start();
			
		} catch (UnknownHostException ex) {
			throw new KPIConnectorException("The host is unknown: " + ex.getMessage(), ex);
		} catch (SocketException ex) {
			throw new KPIConnectorException("There was an error in TCP/IP connection:" + ex.getMessage(), ex);
		} catch (ClosedChannelException ex) {
			throw new KPIConnectorException("The socket channel was closed: " + ex.getMessage(), ex);
		} catch (IOException ex) {
			throw new KPIConnectorException("There was an I/O exception in openning/using the socket channel: " + ex.getMessage(), ex);
		} catch(Exception e) {
			e.printStackTrace();
			throw new KPIConnectorException(e.getMessage(), e);
		}
	}
	
	/**
	 * Disconnect the client stop our readers and close the channel.
	 */
	public void disconnect() throws KPIConnectorException {
		try {
			if (channel != null) {
				channel.close();
				channel = null;
			}
			//Call destroy method so the SocketChannelReader and the TcpIpConnector threads are terminated gracefully
			destroy();
		} catch (IOException ex) {
			throw new KPIConnectorException("There was an I/O exception in openning/using the socket channel: " + ex.getMessage(), ex);
		}
	}    

	/** 
	 * Send a message to the server.
	 * 
	 * @param	cm		A ClientMessage object representing the message to be sent to the SIB
	 * @return	None
	 */
	private void writeMessage(ClientMessage cm) throws KPIConnectorException {
		try {
			// Calculate how many chunks are needed to send whole message.
			int count = cm.getPayloadSize() / writeBuffer.capacity();
			if(cm.getPayloadSize() % writeBuffer.capacity() > 0) {
				count++;
			}
			
			// Send the chunks 
			int offset = 0;
			int lenght = (cm.getPayloadSize() >= writeBuffer.capacity())? writeBuffer.capacity() : cm.getPayloadSize();
			for(int i = 0; i < count; i++) {
				writeBuffer.clear();
				writeBuffer.put(cm.getPayload(), offset, lenght);
				writeBuffer.flip();
				channelWrite(channel, writeBuffer);
				offset += writeBuffer.capacity();
				if(i == count-2) {
					lenght = cm.getPayloadSize() - offset;
				} else {
					lenght = writeBuffer.capacity();
				}
			}
			
		} catch (KPIConnectorException ex) {
			throw new KPIConnectorException("Cannot write the message in the socket channel" + ex.getMessage(), ex);
		}	
	}

	 /** 
     * Write the contents of a ByteBuffer to the given SocketChannel.
     * 
     * @param	channel		The SocketChannel object to be used when sending the contents from the buffer
     * @param	writeBuffer	The ByteBuffer object that contains the bytes to be sent to the user
     * @return	None
     * 
     */
    private void channelWrite(SocketChannel channel, ByteBuffer writeBuffer) throws KPIConnectorException {
		long nbytes = 0;
		long toWrite = writeBuffer.remaining();
	
		// loop on the channel.write() call since it will not necessarily
		// write all bytes in one shot
		try {
		    while (nbytes != toWrite) {
		    	nbytes += channel.write(writeBuffer);
		    }
		} catch (ClosedChannelException cce) {
			cce.printStackTrace();
			throw new KPIConnectorException("The socket was closed: " + cce.getMessage(), cce);
		} catch (Exception e) {
			e.printStackTrace();
			throw new KPIConnectorException("Cannot write data in the socket channel: " + e.getMessage(), e);
		} 
	
		// get ready for another write if needed
		writeBuffer.rewind();

    }

    /** 
     * Adds a new message in the output queue
     * 
     * @param	msg		An array of bytes containing the data to be sent
     * @return	None
     * 
     */
	@Override
	public void write(byte[] msg) {
		outQueue.add(new ClientMessage(msg));
		synchronized(lock) {
			lock.notifyAll();
		}
	}	
	
	/**
	 * Adds a incoming message to the incoming messages queue. 
	 * 
	 * @param 	cm 	The ClientMessage object that contains the data received from the SIB
	 * @return	None
	 */
	public void addIncomingMessage(ClientMessage cm) {
		inQueue.add(cm);
		synchronized(lock) {
			lock.notifyAll();
		}
	}
	
	
	/**
	 * Used to gracefully stop both writing and reading threads from the TcpIpConnector object 
	 * 
	 * @return	None
	 */
	public void destroy() {
		
		if (this.keepAlive){
			//set control flag
			running = false;
			
			if(reader != null) {
				reader.shutdown();
			}
			
			reader = null;
			thread = null;
			//we must notify locking threads that disconnection actually happened
			synchronized(lock) {
				lock.notifyAll();
			}
		}
		//if keep alive is set to false, we should not stop the main thread, only the reading one
		else{
			if(reader != null) {
				reader.shutdown();
			}
			
			reader = null;
		}
		
		/*
		 * Stop the thread AT ANY COST
		 * 
		 * ARF 15-06-18: Destroy is "DESTROY" and not setting some if() to close or not,
		 * and leaving the run() method working.... **WHO** was the responsible of
		 * this?. This is critical when reconnecting KPs because the thread of the TCP
		 * connector is not destroyed and on each ModelManager.createModel() a new
		 * tcpipcpnnector thread is created...
		 */
		this.running = false;
	}
	
	/**
	 * Getters and setters
	 */
	public ConnectionSupport getConnectionSupport() {
		return connectionListeners;
	}
	@Override
	public String getName() {
		return "TCP/IP";
	}
	@Override
	public void setProperties(Properties properties) {
		super.setProperties(properties);
		// Establish the keepAlive property.
		String tmp = this.properties.getProperty(TcpipConnector.KEEP_ALIVE); 
		if(tmp != null) {
			this.keepAlive = Boolean.parseBoolean(tmp);
		}
	}
	
	
}
