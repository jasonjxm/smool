/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.connector.impl.tcpip;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.LinkedList;


/**
 * ClientConnectionAttachment.java
 *
 * This class is used as the attachment for each channel registered
 * with the Selector.
 * It holds the temporary incoming data and checks the completeness 
 * of each message.
 *
 * @author <a href="mailto:raul.otaolea@esi.es">Raul Otaolea</a>
 * @version 1.0
 */
public abstract class ClientConnectionAttachment {
    
    /** Temporary buffer before it is converted into a message. */
    private byte[] buffer;
        
    /** List of ClientMessages already decoded. */
    private LinkedList<ClientMessage> decodedMessages;

	private int alloc;
	private int used;
	
	public ClientConnectionAttachment() {
		// [ROM] TODO: Modify hard coded values with values read from TCPIPServer.properties.
		buffer = new byte[TCPIPConfiguration.BUFFER_SIZE];
		alloc = TCPIPConfiguration.BUFFER_SIZE;
		used = 0;

		decodedMessages = new LinkedList<ClientMessage>();
	}
	
	/** Check if there are enough bytes to build message(s). */
	public boolean checkForMessages() throws MessageParseException {
		int[] indexes = parseMessages(buffer);
		if(indexes == null) {
			return false;
		} else {
			if(indexes.length % 2 != 0) {
				throw new MessageParseException("The length of the indexes is no even.");
			}
			try {
				// Create client messages.
				for(int i = 0; i < indexes.length; i+=2) {
					String s;
					if (Charset.isSupported("UTF-8")) {
						s = new String(buffer, Charset.forName("UTF-8"));
					}
					else {
						s = new String(buffer);
					}
					s = s.substring(indexes[i], indexes[i+1]+1);
					
					byte[] payload;
					if (Charset.isSupported("UTF-8")) {
						payload = s.getBytes(Charset.forName("UTF-8"));
					}
					else {
						payload = s.getBytes();
					}
					ClientMessage cm = new ClientMessage(payload);
					decodedMessages.add(cm);
				}
				// Compact the buffer.
				int remaining = used - indexes[indexes.length-1] - 1;
				int newLength = remaining;
				if(newLength < TCPIPConfiguration.BUFFER_SIZE) {
					newLength = TCPIPConfiguration.BUFFER_SIZE;
				}
				byte[] newBuffer = new byte[newLength];
				System.arraycopy(buffer, indexes[indexes.length-1]+1, newBuffer, 0, remaining);
				buffer = newBuffer;
				alloc = newLength;
				used = remaining;
				return true;
			} catch(Exception e) {
				throw new MessageParseException(e.toString());
			}
		}	
	}
	
	/**
	 * 
	 * @return int[] Returns pairs of indexes indicating the start index (included) 
	 * and the last index (included) of the detected messages.
	 */
	public abstract int[] parseMessages(byte[] buffer);
		
	/**
	 * Indicates if there are pending messages.
	 * @return
	 */
	public boolean hasMessages() {
		return decodedMessages.size() > 0;
	}

	/**
	 * This method removes the messages returned each time is called.
	 * @return Iterator with the parsed messages.
	 */
	public Iterator<ClientMessage> getMessages() {
		if(hasMessages()) {
			LinkedList<ClientMessage> messages = new LinkedList<ClientMessage>();
			Iterator<ClientMessage> i = decodedMessages.iterator();
			while(i.hasNext()) {
				ClientMessage cm = i.next();
				i.remove();
				messages.add(cm);
			}
			return messages.iterator();
		} else {
			return null;
		}
	}
	
	/**
	 * Clear this array
	 */
	public void reset() { 
		used = 0; 
	}

	/**
	 * Returns the lenght of this array
	 */
	public int length() { 
		return used; 
	}
	
	/**
	 * Add a byte to this array.
	 * @param b byte to add
	 */
	public void add(byte b) { 
		buffer[used++] = b;
		if (used == alloc) {
			grow(TCPIPConfiguration.BUFFER_GROW);
		}
	}

	/**
	 * Add a array of bytes to this array
	 * @param b Byte array to add
	 * @param len Lenght of byte array to add.
	 */
	public void add(byte[] b, int len) {
		if (used+len >= alloc) {
			grow(len + TCPIPConfiguration.BUFFER_GROW);
		}
		System.arraycopy(b, 0, buffer, used, len);
		used += len;
	}
	/**
	 * Add a ByteArray to this array
	 * @param b ByteArray to add
	 */

	public void add(ByteBuffer bb) {
		bb.flip();
		if(used+bb.limit() >= alloc) {
			grow(bb.limit() + TCPIPConfiguration.BUFFER_GROW);
		}
		
		bb.get(buffer, used, bb.limit());
		//String s = new String(buffer);
		//Logger.debug("Incoming chunk of bytes read from SIB as follows: " + s);
		used += bb.limit();
	}

	private void grow(int newGap) {
		alloc += newGap;
		byte [] n = new byte[alloc];
		System.arraycopy(buffer, 0, n, 0, used);
		buffer = n;
	}
}
