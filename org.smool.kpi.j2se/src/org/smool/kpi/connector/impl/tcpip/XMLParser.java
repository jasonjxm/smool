/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.connector.impl.tcpip;

import java.util.Properties;

/**
 * To avoid including a XML Parser lib and gain weight in the final JAR, we have
 * implement very simple routines in order to parse the XML received from the SIB.
 * @author Raul Otaolea <raul.otaolea@esi.es>
 *
 */
public class XMLParser {

	// Constants related with the sib xml.
	private static final String NAME = "name";
	private static final String PARAMETER = "parameter";

	private XMLParser() {
	}
	
	/**
	 * 
	 * @param xml containing the info of the TCP/IP gateway.
	 * @return The name of the SIB the TCP/IP gateway is connected to.
	 */
	public static String getSIBName(String xml) {
		return XMLParser.getTagContent(xml, XMLParser.NAME);
	}

	/**
	 * 
	 * @param xml containing the info of the TCP/IP gateway.
	 * @return The parameters (name=value) in a Properties object.
	 */
	public static Properties getProperties(String xml) {
		Properties p = new Properties();
		
		String opener = new StringBuilder("<").append(XMLParser.PARAMETER).toString();
		String closer = XMLParser.getCloser(XMLParser.PARAMETER);
		
		int openerFrom = xml.indexOf(opener);
		while(openerFrom != -1) {
			openerFrom += opener.length();
			int openerIndex = xml.indexOf(">", openerFrom);
			
			int fromComa = xml.indexOf("\"", openerFrom) + 1;
			int toComa = xml.indexOf("\"", fromComa);
			String key = xml.substring(fromComa, toComa);
			
			int toIndex = xml.indexOf(closer, openerIndex);
			String value = xml.substring(openerIndex+1, toIndex);
			p.setProperty(key, value);
			openerFrom = xml.indexOf(opener, toIndex + closer.length());
		}
		return p;
	}
	
	
	private static String getTagContent(String xml, String tag) {
		String opener = XMLParser.getOpener(tag);
		String closer = XMLParser.getCloser(tag);
		int from = xml.indexOf(opener) + opener.length();
		int to = xml.indexOf(closer);
		return xml.substring(from, to);
	}
	
	private static String getOpener(String tag) {
		StringBuilder sb = new StringBuilder();
		sb.append("<");
		sb.append(tag);
		sb.append(">");
		return sb.toString();
	}
	
	private static String getCloser(String tag) {
		StringBuilder sb = new StringBuilder();
		sb.append("</");
		sb.append(tag);
		sb.append(">");
		return sb.toString();
	}
	
}
