/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.connector.impl.tcpip;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.util.Properties;

import org.smool.kpi.common.Logger;
import org.smool.kpi.connector.AbstractSIBDiscoverer;
import org.smool.kpi.connector.SIBDescriptor;


public class TcpipSIBDiscoverer extends AbstractSIBDiscoverer implements Runnable {
	
	// Constants related with the discovery.
	private static final String TCPIP = "TCP/IP";
	private static final int PORT = 5555;
	private static final String MULTICAST_HOST = "235.0.0.1";
	private static final String DISCOVER_MESSAGE = "<KP>ping</KP>";
	
	private Thread thread;
	private volatile boolean running;
	
	private Object lock;
	
	public TcpipSIBDiscoverer() {
		lock = new Object();
	}

	@Override
	/**
	 * Start the discovery.
	 */
	public void inquiry() {
		// Guarantee that the inquiry is executed once at a time to avoid creating more than one thread.
		synchronized(lock) {
			if(running) {
				return;
			}
			thread = new Thread(this);
			thread.start();
		}
	}

	/**
	 * This method sends multicast messages trying to discover SIBs. If a SIB responds, then creates a SIB descriptor and includes it on the list
	 */
	@Override
	public void run() {
		running = true;
		//Check if we must carry on, NOTE: This won't stop if the DiscoveryListener don't tell us to, it is its duty to do so, otherwise this 
		//thread will be kept querying 
		while (running){
			try (DatagramSocket ds = new DatagramSocket()){
				InetAddress group = InetAddress.getByName(MULTICAST_HOST);
				byte[] data = DISCOVER_MESSAGE.getBytes();
				ds.setSoTimeout(60000);
		 		DatagramPacket dp = new DatagramPacket(data, data.length, group, TcpipSIBDiscoverer.PORT);
		 		ds.send(dp);
				byte[] buf = new byte[1024];
				dp = new DatagramPacket(buf, buf.length);
				ds.receive(dp);
				if(dp.getLength() > 0) {
					byte[] tmp = new byte[dp.getLength()];
					System.arraycopy(dp.getData(), 0, tmp, 0, tmp.length);
					String received = new String(tmp);
					Logger.debug(this.getClass().getName(), "Received from SIB: " + received);
					SIBDescriptor sibDescriptor = createSIBDescriptor(received);
					this.discoveryListener.connectorSIBDiscovered(sibDescriptor);
				}
			}
			catch(SocketTimeoutException e) {
				Logger.error("Socket time excedeed while waiting a response when discovering SIBs. Trying again");
			} 
			catch (IOException e) {
				Logger.error("There was some kind of IO error while waiting for a response when discovering SIBs. Trying again");
			}			

		}
		this.discoveryListener.connectorSIBDiscoveryFinished(getConnectorName());
	}

	@Override
	public void stopInquiry() {
		running = false;
	}
	
	@Override
	public String getConnectorName() {
		return TCPIP;
	}
	
	/**
	 * Creates a new instance of the SIBDescriptor that represents the discovered SIB
	 * @param xml An XML chunk with all the data associated to the discovered SIB
	 * @return	A reference to the newly created SIBDescriptor
	 */
	private SIBDescriptor createSIBDescriptor(String xml) {
		
		if(xml == null || xml.length() == 0) {
			return null;
		}
		
		String sibName = XMLParser.getSIBName(xml);
		Properties p = XMLParser.getProperties(xml);

		SIBDescriptor sibDescriptor = new SIBDescriptor(sibName, getConnectorName(), p);		
		return sibDescriptor;
	}
	
	
}
