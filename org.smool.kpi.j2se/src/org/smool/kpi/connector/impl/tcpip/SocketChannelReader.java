/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.connector.impl.tcpip;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

import org.smool.kpi.common.Logger;


public class SocketChannelReader extends Thread {

	/** connection to server */
	private SocketChannel channel;

	/** selector to manage server connection */
	private Selector selector;

	/** indicates if running */
	private boolean running;
	
	/** the working buffer */
	private ByteBuffer readBuffer;

	/** The TcpipConnector */
	private TcpipConnector connector;
	
	/**
	 * constructor.
	 * @throws IOException If the selector can not be done
	 */
	public SocketChannelReader(TcpipConnector connector) throws IOException {
		super("SocketChannelReader");
		this.connector = connector;
		readBuffer = ByteBuffer.allocate(255);
	
		this.selector = Selector.open();
	}

	public void setChannel(SocketChannel channel) throws ClosedChannelException {


			this.channel = channel;
			this.channel.register(selector, SelectionKey.OP_READ, new SSAPMessageParser());
		
	}
	
	/**
	 * The runnable method
	 */
	public void run() {

		running = true;
		while (running) {
			try {
				/**
				 * This method performs a blocking selection operation. 
				 * It returns only after at least one channel is selected, 
				 * this selector's wakeup method is invoked, or the current 
				 * thread is interrupted, whichever comes first.
				 * @return The number of keys, possibly zero, whose ready-operation sets were updated
				 */

				selector.select();
				Set<SelectionKey> readyKeys = selector.selectedKeys();

				Iterator<SelectionKey> i = readyKeys.iterator();
				while (i.hasNext()) {
					SelectionKey key = (SelectionKey) i.next();
					i.remove();
					SocketChannel channel = (SocketChannel) key.channel();
					SSAPMessageParser attachment = (SSAPMessageParser) key.attachment();

					try {
						long nbytes = channel.read(readBuffer);
						if (nbytes == -1) {
							channel.close();
							connector.getConnectionSupport().fireConnectionClosed(connector);
							return;
						}

						try {
							attachment.add(readBuffer);
							if (attachment.checkForMessages()) {
								Iterator<ClientMessage> msgList = attachment.getMessages();
								while (msgList.hasNext()) {
									//Logger.debug(this, "There is some message in the buffer");
									ClientMessage cm = msgList.next();
									connector.addIncomingMessage(cm);
								}
							}
							readBuffer.compact();
						} catch (IllegalArgumentException e) {
							Logger.error(this.getClass().getName(), "illegal argument while parsing incoming event");
						}
					} catch (IOException ioe) {
						channel.close();
						connector.getConnectionSupport().fireConnectionClosed(connector);
					}
				}
			} catch (IOException ioe2) {
				Logger.error(this.getClass().getName(), "error during select(): " + ioe2.getMessage());
				connector.getConnectionSupport().fireConnectionError(connector, ioe2.getMessage());
			} catch (Exception e) {
				Logger.error(this.getClass().getName(), "exception during select()");
				connector.getConnectionSupport().fireConnectionError(connector, e.getMessage());
			}
		}
		Logger.debug(this.getClass().getName(), "TCP/IP J2SE Connector SockeChannelReader ends");
	}

	public void shutdown() {
		running = false;
		// force the selector to unblock
		selector.wakeup();
	}
}
