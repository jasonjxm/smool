/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.connector.impl.tcpip;

import java.nio.charset.Charset;
import java.util.ArrayList;


/**
 * SSAPMessageParser.java
 * 
 * This class is responsible for detecting a SSAP message from the received string
 * of bytes. A SSAP message starts with the tag <SSAP_message> and
 * ends with </SSAP_message>
 * 
 * @author <a href="mailto:raul.otaolea@esi.es">Raul Otaolea</a>
 * @date 03/05/2010
 * @version 1.0
 */

public class SSAPMessageParser extends ClientConnectionAttachment {
	
	private final static String START_TOKEN = "<SSAP_MESSAGE>";
	private final static String END_TOKEN = "</SSAP_MESSAGE>";
	
	private ArrayList<Integer> indexes = new ArrayList<Integer>();
	
	public SSAPMessageParser() {
	}
	
	public int[] parseMessages(byte[] buffer) {
		
		String s;
		try {
			if (Charset.isSupported("UTF-8")) {
				s = new String(buffer, Charset.forName("UTF-8")).toUpperCase();
			}
			else {
				s = new String(buffer).toUpperCase();
			}
		} catch(Exception e) {
			return null;
		}
		
		indexes.clear();
		int fromIndex = 0;
		int pos = 0;
		while(pos != -1) {
			pos = s.indexOf(SSAPMessageParser.START_TOKEN, fromIndex);
			if(pos != -1) {			
				// Start token found.
				fromIndex = pos;
				pos = s.indexOf(SSAPMessageParser.END_TOKEN, fromIndex + SSAPMessageParser.START_TOKEN.length());
				if(pos != -1) {
					indexes.add(fromIndex);
					indexes.add(pos + SSAPMessageParser.END_TOKEN.length()-1);
					fromIndex = pos + SSAPMessageParser.END_TOKEN.length();
				}
			}
		}
		
		if(indexes.size() == 0) {
			return null;
		} else {
			int[] idx = new int[indexes.size()];
			for(int i = 0; i < indexes.size(); i++) {
				idx[i] = indexes.get(i);
			}
			return idx;
		}
	}

}
