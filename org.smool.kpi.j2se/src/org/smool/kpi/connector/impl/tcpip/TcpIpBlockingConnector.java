/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Alejandro Villamarin (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    
 *******************************************************************************/ 

package org.smool.kpi.connector.impl.tcpip;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.Iterator;
import java.util.Properties;
import java.util.Vector;
import java.util.concurrent.LinkedBlockingQueue;

import org.smool.kpi.common.Logger;
import org.smool.kpi.connector.AbstractConnector;
import org.smool.kpi.connector.ConnectionSupport;
import org.smool.kpi.connector.exception.KPIConnectorException;


public class TcpIpBlockingConnector extends AbstractConnector implements Runnable {

	//host is maintained to keep backwards compatibility <=2.0.3 release
	/** Required constants */
	private static final String HOST = "HOST";
	private static final String DEFAULT_IPADDRESS = "DEFAULT_IPADDRESS";
	private static final String PORT = "PORT";
	private final int SOCKET_TIMEOUT_DELAY = 5000;
	
	/** This two variables are used for conversion from String to ByteBuffer and vice-versa */
	public static Charset charset = Charset.forName("UTF-8");
	public static CharsetEncoder encoder = charset.newEncoder();
	
    /**
     * This variable store the last error message code.
     * Users can use this code to understand what occurs.
     * Negative value means ERROR!!!
     */
    private int KP_ERROR_ID=0;
	
	/**
     * Error message code list
     */
//    private final int ERR_NO_ERROR = 0;
//    private final int ERR_Conected = 1;
//    private final int ERR_UnknownHost = -2;
//    private final int ERR_IOException = -3;
    private final int ERR_ConnectionClose=4;
    private final int ERR_EXC_CLOSE_CONN = -5;
    private final int ERR_MsgSent=6;
    private final int ERR_MsgSent_EXC_CONN=-7;
    private final int ERR_MsgSent_CONN_UNAVAILABLE =-8;
    private final int ERR_RECEIVE_FAIL = -9;
    private final int ERR_SOCKET_TIMEOUT=-10;   
//    private final int ERR_EVENT_HANDLER_NULL=-11; 
//    private final int ERR_Subscription_DONE=12;
//    private final int ERR_Subscription_NOT_DONE=-13;    
//    private final int ERR_InsertProtection_FAIL=-14;
//    private final int ERR_RemoveProtection_FAIL=-15;    

    /**
     * Error message explanation
     */
     String ERR_MSG[]={
		    "@NO ERROR"
		   ,"@Conected"
		   ,"@ERR_UnknownHost"
		   ,"@ERR_IOException"    					   
		   ,"@ConnectionClose"
		   ,"@ERR_EXC_CLOSE_CONN"
		   ,"@MsgSent"
		   ,"@ERR_MsgSent_EXC_CONN"
		   ,"@ERR_MsgSent_CONN_UNAVAILABLE"
		   ,"@ERR_RECEIVE_FAIL"
		   ,"@ERR_SOCKET_TIMEOUT"
		   ,"@ERR_EVENT_HANDLER_NULL"
		   ,"@Subscription DONE"
		   ,"@ERR_Subscription NOT DONE"
        };
	
	/** The thread used to process incoming and outgoing messages */
	private Thread thread;
	
	/** Queue for incoming events */
	private LinkedBlockingQueue<ClientMessage> inQueue;
	
	/** Queue for outgoing events */
	private LinkedBlockingQueue<ClientMessage> outQueue;

	/** Lock to avoid expensive run() */
	private Object lock;
	
	/** Still running? */
	private boolean running;
	
	/** The socket used to establish the connection with the SIB*/ 
	private Socket kpSocket;
	/** The output writer used to send messages */
	private PrintWriter out;
	/** The input Reader used to store read messages */
	private BufferedReader in;
	/** The reading thread used for reading messages */
	private Thread readingThread;
	
	/** 
	 * Constructor
	 */
    public TcpIpBlockingConnector() throws IOException {
    	
    	super();
    	//Initialize reading and writing queues
		this.inQueue = new LinkedBlockingQueue<ClientMessage>();
		this.outQueue = new LinkedBlockingQueue<ClientMessage>();
		this.lock = new Object();
				
		// start processing incoming/outgoing messages.
		this.thread = new Thread(this, "TcpipBlockingConnector");
		this.thread.start();
    }
    
	/** 
	 * Handle incoming messages from the inQueue.
	 */
	public void run() {
		
		//set running flag
		running = true;
		
		//while control flag is set, process incoming and outgoing messages
		while(running) {
			
			try {
				//if both queues are empty, wait
				if(inQueue.size() == 0 && outQueue.size() == 0) {
					synchronized(lock) {
						lock.wait();
					}
				}
				//otherwise process both queues
				processIncomingMessages();
				writeOutgoingMessages();
				
			} catch (KPIConnectorException cex) {
				Logger.error("There was an connection error with the SIB");
				cex.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		Logger.debug(this.getClass().getName(), "Client TCP/IP connector ends");
 	}

	/** 
	 * Handle incoming messages from the inQueue.
	 * 
	 * @return None
	 */
	protected void processIncomingMessages() throws KPIConnectorException {
		try {
			// If the connection must be kept closed, disconnect the socket.
			if(inQueue.size() > 0) {
				//Create a new ClientMessage object for the read message
				ClientMessage msg;
				while (inQueue.size() > 0) {
					
					msg = inQueue.take();
					Logger.debug(this.getClass().getName(), "Received: " + msg);
					//notify to the upper layers that a message has been received
					this.messageListeners.fireMessageReceived(msg.getPayload());
				}
			}
		} 
		catch(InterruptedException iex) {
			iex.printStackTrace();
			throw new KPIConnectorException("The reading socket with the SIB has been interrupted: " + iex.getMessage(), iex);
		}
	}
	
	
	private String getTagContent(String xml, String tag) {
		String opener = getOpener(tag);
		String closer = getCloser(tag);
		int from = xml.indexOf(opener) + opener.length();
		int to = xml.indexOf(closer);
		return xml.substring(from, to);
	}
	
	private String getOpener(String tag) {
		StringBuilder sb = new StringBuilder();
		sb.append("<");
		sb.append(tag);
		sb.append(">");
		return sb.toString();
	}
	
	private String getCloser(String tag) {
		StringBuilder sb = new StringBuilder();
		sb.append("</");
		sb.append(tag);
		sb.append(">");
		return sb.toString();
	}

	/** 
	 * Write all events waiting in the outQueue.
	 * 
	 * @return	None
	 */
	private void writeOutgoingMessages() throws KPIConnectorException {
		
		try {
			//The message to be sent
			ClientMessage msg;
			//process all the messages to be sent to the SIB
			if(outQueue.size() > 0) {
				while (outQueue.size() > 0) {
					msg = outQueue.take();
					long time = System.currentTimeMillis();
					String payload = new String(msg.getPayload());
					//just for testing purposes
					if (payload.contains("<transaction_type>INSERT</transaction_type>"))
					{
						String transactionId = getTagContent(payload, "transaction_id");
						String nodeId = getTagContent(payload, "node_id");
						String spaceId = getTagContent(payload, "space_id");
						String newPayload = "<SSAP_message>"
					     +"<message_type>REQUEST</message_type>"
					     +"<transaction_type>INSERT</transaction_type>"
					     +"<transaction_id>"+ transactionId  + "</transaction_id>"
					     +"<node_id>" + nodeId + "</node_id>"
					     +"<space_id>"+ spaceId +"</space_id>"
					     +"<parameter name=\"confirm\">TRUE</parameter>"
					     +"<parameter name=\"insert_graph\"  encoding=\"RDF-M3\">";
					    
						Vector<Vector<String>> triples = null;
						triples = new Vector<Vector<String>>();
						String URI = "URI";
						String LITERAL = "literal";
							
						triples.add( newTriple( "Person_01", "rdf:type", "Person", URI, URI) );
						triples.add( newTriple( "Person_02", "rdf:type", "Person", URI, URI) );

						triples.add( newTriple( "Person_01", "HasName", "Fabio", URI, LITERAL) );
						triples.add( newTriple( "Person_02", "HasName", "Guido", URI, LITERAL) );

						triples.add( newTriple( "Environment_01", "rdf:type", "Environment", URI, URI) );
						triples.add( newTriple( "Environment_02", "rdf:type", "Environment", URI, URI) );

						triples.add( newTriple( "Environment_01", "HasFriendlyName", "Room_01", URI, LITERAL) );
						triples.add( newTriple( "Environment_02", "HasFriendlyName", "Room_02", URI, LITERAL) );

						triples.add( newTriple( "Environment_01", "ContainsEntity", "Person_01", URI, URI) );
						triples.add( newTriple( "Environment_02", "ContainsEntity", "Person_02", URI, URI) );
							
						triples.add(newTriple( "Person", "rdfs:subClassOf", "thing", URI, URI) );
						triples.add(newTriple( "Environment", "rdfs:subClassOf", "thing", URI, URI) );

						
						
						newPayload=newPayload+this.getTripleListFromTripleVector(triples);
					     
						newPayload=newPayload
					     +"</parameter>"
					     +"</SSAP_message>";
						sendSSAPMsg(newPayload);
					}
					else{
						sendSSAPMsg(payload);
					}
					
					//writeMessage(msg);
					Logger.debug(this.getClass().getName(), new StringBuilder("Sent message in ")
						.append((System.currentTimeMillis()-time))
						.append(" ms: ")
						.append(msg).toString());
				}
			}
		} 
		catch (InterruptedException iex) {
			throw new KPIConnectorException("The writing socket with the SIB has been interrupted: " + iex.getMessage(), iex);
		}
	}

	

	
    private String getTripleListFromTripleVector( Vector<Vector<String>> tripleVector )
    {	
    	
    	String ANYURI = "http://www.nokia.com/NRC/M3/sib#any";
    	String tripleList="<triple_list>";
        for(int i=0; i< tripleVector.size() ;i++){
        	String s=tripleVector.elementAt(i).elementAt(0);
        	String p=tripleVector.elementAt(i).elementAt(1);
        	String o=tripleVector.elementAt(i).elementAt(2);
        	String st=s==null?"URI":tripleVector.elementAt(i).elementAt(3);
        	String ot=o==null?"URI":tripleVector.elementAt(i).elementAt(4);
         
        	tripleList=tripleList
            +" <triple>"        
            +"  <subject type=\""+st+"\">"+ (s==null?ANYURI:s)+"</subject>"
            +"  <predicate>"+                                                   (p==null?ANYURI:p)+"</predicate>"
            +"  <object type=\"" +ot+"\">"+ (o==null?ANYURI:o)+"</object>"
            +" </triple>";
        }
        return tripleList+"</triple_list>";
    }//private String getTripleListFromTripleVector( Vector<Vector<String>> tripleList )

	private Vector<String> newTriple(String s,String p,String o, String s_type, String o_type){
        
		Vector<String> triple=new Vector<String>();
		String ANYURI = "http://www.nokia.com/NRC/M3/sib#any";
        
        triple.add(0, (s==null?ANYURI:s) ); 
        triple.add(1, (p==null?ANYURI:p) );
        triple.add(2, (o==null?ANYURI:o) );
        triple.add(3, s_type);
        triple.add(4, o_type);

        return triple;
	}

	/**
	 * Connects to a SIB using TCP/IP
	 * 
	 * @return	None
	 */
	public synchronized void connect() throws KPIConnectorException {
		
		
        try
        {
			String host = this.properties.getProperty(TcpIpBlockingConnector.HOST);
			String defaultIPAddress = this.properties.getProperty(TcpIpBlockingConnector.DEFAULT_IPADDRESS);
			int port = Integer.parseInt(this.properties.getProperty(TcpIpBlockingConnector.PORT));
			
			//this is done to keep backward compatibility prior 2.0.4 release
			if(host == null || host.equals("")) {
				//check if defaultIp is also empty
				if (defaultIPAddress == null || defaultIPAddress.equals(""))
					throw new UnknownHostException();
				//otherwise start listening on defaultIp
				else
				{
					kpSocket = new Socket(defaultIPAddress, port);
					kpSocket.setKeepAlive(true);
				}
			}
			//start listening on host
			else
			{
				kpSocket = new Socket(host, port);
				kpSocket.setKeepAlive(true);
			}
			
            this.running = true;
            //get input and output streams
            out = new PrintWriter(kpSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(kpSocket.getInputStream()));
            
        } 
        catch (UnknownHostException e){
        	Logger.error("Unknown Host, cannot reach SIB");
            
        } 
        catch (IOException e){
        	Logger.error("Couldn't get I/O streams for the connection");
        
        }
	}
	
	/**
	 * Close the input/output streams and close the Socket
	 * 
	 * @return	None
	 */
	public void disconnect(){

		try{
			out.close();
 		   	in.close();
 		   	kpSocket.close();
 		   	
 		   	this.running = false;
		}
 	   	catch(Exception e){
 	   		Logger.error("KpCore:closeConnection:Exception:\n"+e);		
		}
	}    

//	/** 
//	 * Send a message to the server. 
//	 * 
//	 * @param	cm	The message to be sent, as a ClientMessage instance
//	 * @return	int	Return code, negative means an error happened
//	 */
//	private int writeMessage(ClientMessage cm){
//		
//		//check if the socket output stream is still available
//		if(this.kpSocket.isOutputShutdown()){ 
//			return ERR_MsgSent_CONN_UNAVAILABLE;
//		}
//		//otherwise, send a message
//		String message = new String(cm.getPayload());
//  	   	out.println(message);
//  	   
//  	   try
//  	   {
//  		   //If the message is not a SUBSCRIBE, then clse the outputStream of the socket
//  		   if(message.indexOf("SUBSCRIBE")<0){
//  			   this.kpSocket.shutdownOutput();
//  		   }
//  		   else{
//  			   Logger.debug("\n*** KpCore:send:shutdownOutput:OPERATION_NOT_TO_BE_DONE:this.kpSocket.shutdownOutput() \n\nSSAP MSG:"+ cm.getPayload());  
//  		   }
//  	   }
//  	   catch(Exception e){ 
//  		   Logger.error("EXCEPTION:"+e);
//  		   return ERR_MsgSent_EXC_CONN;
//  	   }
//
//  	   return this.ERR_MsgSent;	
//	}
	
	
	/**
     * Method that waits until a message is received from the SIB 
     * 
     * @return return a string representation of the 
     *         XML message received from the SIB. A null value means 
     *         that an error occurs. In this case, check the error state
     *         with the functions: getErrMess, getErrID          
     *          
     * @throws java.net.SocketException         
     */
    private String receive() throws Exception 
    {  
    	//initialize return value
    	String ret=null;
    	//Establish TimeOut
    	this.kpSocket.setSoTimeout(SOCKET_TIMEOUT_DELAY);
    	//create an attachment for the first read
    	SSAPMessageParser attachment = new SSAPMessageParser();
    	try
    	{ 
    		//ret = this.in.readLine();
    		ret=this.readBytes();
     	  
    		if (!ret.isEmpty()){
    			//check if a complete message has been received
    			if(ret.contains("<SSAP_message>") && ret.contains("</SSAP_message>")){ 
  				
    				try{
    					//convert the string to a bytebuffer
    					byte[] bytes = ret.getBytes();
//    					TcpIpBlockingConnector.encoder.reset();
    					attachment.add(bytes, ret.length());
    					//attachment.add(buf);
    					if (attachment.checkForMessages()){
    						Iterator<ClientMessage> msgList = attachment.getMessages();
    						while (msgList.hasNext()){
    							//Logger.debug(this, "There is some message in the buffer");
    							ClientMessage cm = msgList.next();
    							addIncomingMessage(cm);
    						}
    					}	
    				}
    				catch (IllegalArgumentException e){
    					Logger.error(this.getClass().getName(), "illegal argument while parsing incoming event");
    				}
    				//This control is mandatory   
    				Logger.debug("A complete message has been received");
    			}
    		} 
    	}
    	catch(Exception e){
    		this.KP_ERROR_ID=ERR_SOCKET_TIMEOUT; 
    		return null;
    	}
   
    	return ret;
    }
    
    
    /**
     * Method that waits until a message is received from the SIB 
     * 
     * @return String	A string representation of the 
     *         XML message received from the SIB. A null value means 
     *         that an error has occurred. In this case, check the error state
     *         with the functions: getErrMess, getErrID          
     *          
     * @throws java.net.SocketException         
     */
    private	String readBytes()
    {
    	//the String to be returned
    	String msg = "";
    	//bytes read counter
        int c=0;
        
        try{
        	//try to read one byte from the input stream
        	while((c=in.read())!=-1){  
        		//build the message
        		msg=msg+(char)c;
                //check if the read messages already have a complete SSAP message 
                if(msg.contains("<SSAP_message>") && msg.contains("</SSAP_message>")){
                  	
//                	if(this.xmlTools.isSubscriptionConfirmed(msg)){
//                		this.KP_ERROR_ID=this.ERR_Subscription_DONE;
                  	    startReadingThread(kpSocket,in,out);
                  	    return msg;
//                  	}//if(this.xmlTools.isSubscriptionConfirmed(ret))
//                  	else{
//                  		Logger.debug("KpCore:readByteXByteSIBmsg:SSAP message recognized");
//                  	}
                  	
                 }
            }
            Logger.debug("READ LOOP TERMINATED");
            disconnect();
         }
        catch(Exception e){
        	Logger.error("Exception on while receiving message:\n"+e);
            this.KP_ERROR_ID=this.ERR_SOCKET_TIMEOUT;
        }

 	   	return msg;
    }
    
    /**
     * The SSAP protocol is based on some operation to do atomically:<p/>
     * - open the connection<br/>
     * - sent a message<br/>
     * - wait for the answer<br/>
     * <p/>  
     * This method encapsulates all this operations
     * <p/>  
     * Send a pre-formed XML message to the SIB
     * 
     * @param  msg the string representation of the XML message to send. null value are not allowed.
     * @return return a string representation of the 
     *         XML message received from the SIB. A null value means 
     *         that an error occurs. In this case, check the error state
     *         with the functions: getErrMess, getErrID          
     */
    private String sendSSAPMsg(String msg)
    {  
    	//string to be returned
    	String ret="";
    	int err=0;

    	//deb_print("KpCore:message to send:_"+msg.replace("\n", "")+"_");
    	
    	try {
    		if (kpSocket.isClosed()){
    			connect();
    		}
		} catch (KPIConnectorException e1) {
			Logger.error("There was an error trying to connect...");
			e1.printStackTrace();
		}
    	
    	//check if there was an error while sending the message
    	if((err=send(msg))<0){
    		Logger.error("SSAP:ERROR:"+ERR_MSG[(err*-1)] );  
    		KP_ERROR_ID=err; 
    		return null;
    	}
    	else{
    		Logger.debug("SSAP:"+ERR_MSG[(err)] );
    	}

    	Logger.debug("SSAP:Read the answer...");
       
    	//Check if the message is not unsubscribe...dunno why
    	if(msg.indexOf("<transaction_type>UNSUBSCRIBE</transaction_type>")<0){
    		try{ 
    			ret = receive();
    			Logger.debug("SSAP:answer:"+"ok _"+ret.replace("\n", "")+"_\n");
    		}
    		catch(Exception e){
    			
    			err=this.KP_ERROR_ID;
                if(err<0){ 
                	Logger.error("KpCore:SSAP:ERROR:"+ERR_MSG[(err*-1)] );
                }
                else{ 
                	err=ERR_RECEIVE_FAIL;
                     if(err<0){
                    	 Logger.error("KpCore:SSAP:ERROR:"+ERR_MSG[(err*-1)] );
                     }
                }
     	        Logger.error("EXCEPTION: "+e.toString()); e.printStackTrace(); KP_ERROR_ID=err; return null;
     	    }
    	}
       	else{
       		Logger.debug("SSAP:UnSubscription Request:no answer expected:"+"ok"+"\n"); 
       	}

       Logger.debug("KpCore:SSAP:Close connection...");
       closeConnection();
       
       if(this.KP_ERROR_ID>=0){
    	   this.KP_ERROR_ID=err;
       }
       return ret;
    }
    
    /**
     * Close the socket connection with the SIB 
     * 
     * @return return an error code 
     */
    
    private int closeConnection()
    { 
    	try
 	   	{
    		out.close();
    		in.close();
    		kpSocket.close();
 	   	}
    	catch(Exception e){
    		Logger.error("KpCore:closeConnection:Exception:\n"+e);	
    		return this.ERR_EXC_CLOSE_CONN;
    	}	
 		return ERR_ConnectionClose;
    }
    
    
    /**
     * Send a pre-formed XML message to the SIB
     * 
     * @param  msg the string representation of the XML message to send. null value are not allowed.
     * @return return an error code 
     */
    private int send(String msg)
    {
    	//Check if the socket output stream is closed
    	if(this.kpSocket.isOutputShutdown()){ 
    		return ERR_MsgSent_CONN_UNAVAILABLE;
    	}
    	//Otherwise send the message
 	   	out.println(msg);
 	   
 	   	try
 	   	{
 	   		//If the message was not a subscription, close the socket output 
 	   		if(msg.indexOf("SUBSCRIBE")<0){ 
 	   			this.kpSocket.shutdownOutput();
 	   		}
 	   		else{
 	   			Logger.debug("\n*** OPERATION_NOT_TO_BE_DONE:this.socket.shutdownOutput() \n\nSSAP MSG:"+msg);  
 	   		}
 	   	}
 	   	catch(Exception e){ 
 	   		Logger.error("EXCEPTION:"+e);
 	   		return ERR_MsgSent_EXC_CONN;
 	   	}

 	   	return this.ERR_MsgSent;
    }
    
    
//    /**
//     * This method returns the string representation of the last
//     * operation result.
//     * It is possible to know the final state of each operation 
//     * by calling this method.
//     * 
//     * @return return a string explanation message for the last error occurred
//     */
//    private String getErrMess()
//    {  
//    	if(KP_ERROR_ID<0){
//    		return "ERROR MESSAGE:"+ERR_MSG[(KP_ERROR_ID *-1)];
//    	}
//    	else{
//    		return "SERVICE NOTE:"+ERR_MSG[(KP_ERROR_ID    )];
//    	}
//    }

    
    
    /**
     * Method to run the reading thread
     * 
     * @param socket the socket descriptor of the opened socket with the SIB 
     * @param in input stream to receive message from the SIB
     * @param out output stream to sent message to the SIB
     */
    private void startReadingThread(Socket socket,BufferedReader in,PrintWriter out)
    {
        //required variables
        final Socket         ft_kpSocket = socket;
        final PrintWriter    ft_out      = out;
        final BufferedReader ft_in       = in;


        readingThread = new Thread()
        { 
        	public void run(){
        		
        		String msg_event="";      
        		Logger.debug("\n[EVENT HANDLER THREAD STARTED]___________________________________");
        		int c=0;            
        		SSAPMessageParser attachment = new SSAPMessageParser();
        		
        		
        		//try to read from the input stream
        		try{
	        		while((c=ft_in.read())!=-1){ 
	        			//build the message
	        			msg_event=msg_event+(char)c;
	        			//check if a complete message has been received
	        			if(msg_event.contains("<SSAP_message>") && msg_event.contains("</SSAP_message>")){ 
	        				
							try {
								//convert the string to a bytebuffer
								ByteBuffer buf = ByteBuffer.wrap(msg_event.getBytes());
								attachment.add(buf);
								if (attachment.checkForMessages()) {
									Iterator<ClientMessage> msgList = attachment.getMessages();
									while (msgList.hasNext()) {
										//Logger.debug(this, "There is some message in the buffer");
										ClientMessage cm = msgList.next();
										addIncomingMessage(cm);
									}
								}
								
							} catch (IllegalArgumentException e) {
								Logger.error(this.getClass().getName(), "illegal argument while parsing incoming event");
							}
	        				//This control is mandatory   
	        				Logger.debug("A complete message has been received");
	                     }
	        			msg_event="";
	                     
	                }
	                //close the connection
	        		try{
	        			ft_out.close();
	        			ft_in.close();
	        			ft_kpSocket.close();
	         	   	}
	        		catch(Exception e){
	        			Logger.error("KpCore:startEventHandlerThread:closing connection:Exception:\n"+e);
	        			e.printStackTrace();
	         	    
	         	   	}	
        		}
        		catch(Exception e){
        			Logger.error("Exception:\n"+e);
        			e.printStackTrace();
        		}
        	}/*RUN*/
        }/*new Thread()*/;
        //start the reading thread
        readingThread.start();  
    }
    
    /**
     * This method is still available but never used.
     * The SIB event handler thread should stop himself.
     * 
     */
    public boolean stopEventThread()
    {
 	   this.readingThread.interrupt();
 	   return this.readingThread.isInterrupted();
    }   


	 /** 
     * Adds a new element in the output Queue
     * 
     * @param	msg	A byte array with the data to be sent 
     * 
     * @return	None
     */
	@Override
	public void write(byte[] msg) {
		//Add a new element in the queue
		outQueue.add(new ClientMessage(msg));
		//notify possible waiting threads
		synchronized(lock) {
			lock.notifyAll();
		}
	}	
	
	/**
	 * Adds a incoming message to the incoming messages queue.
	 *  
	 * @param	cm	Client Message
	 * @return	None
	 */
	public void addIncomingMessage(ClientMessage cm) {
		//Add a new element in the queue
		inQueue.add(cm);
		//notify possible waiting thread (the condition is whether the queue has any elements)
		synchronized(lock) {
			lock.notifyAll();
		}
	}
	

	
	/**
	 * Getters and setters
	 */
	public ConnectionSupport getConnectionSupport() {
		return connectionListeners;
	}
	
    int getErrID(){
    	return KP_ERROR_ID;
    }
	
	public String getName() {
		return "TCP/IP";
	}
	
	@Override
	public void setProperties(Properties properties) {
		super.setProperties(properties);
	}
	
	/**
	 * This is a helper function that converts a given String to a its ByteBuffer representation
	 * Taken from http://stackoverflow.com/questions/1252468/java-converting-string-to-and-from-bytebuffer-and-associated-problems
	 * @author ZenBlender
	 * @param	msg		The String to be converted
	 * @return	A ByteBuffer object that represents the String parsed as ByteBuffer
	 */
	public static ByteBuffer str_to_bb(String msg){
		  try{
		    return encoder.encode(CharBuffer.wrap(msg));
		  }catch(Exception e){e.printStackTrace();}
		  return null;
		}
}

