/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.connector.impl.bluetooth;

import java.util.ArrayList;

import javax.bluetooth.DataElement;
import javax.bluetooth.DeviceClass;
import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.DiscoveryListener;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.ServiceRecord;
import javax.bluetooth.UUID;

import org.smool.kpi.common.Logger;
import org.smool.kpi.connector.AbstractSIBDiscoverer;
import org.smool.kpi.connector.SIBDescriptor;

public class BluetoothSIBDiscoverer extends AbstractSIBDiscoverer implements DiscoveryListener{
	
	private static transient boolean running;
	// NAO
	private static transient boolean isSibServiceFounded;
	//finNAO
	
	private ArrayList<RemoteDevice> remoteDevices; //List of found devices
	private DiscoveryAgent agent;//Bt discovery agent
	private ArrayList<ServiceRecord> services;
	private int serviceSearchTransactionId=0;
	
	public BluetoothSIBDiscoverer(){
		BluetoothSIBDiscoverer.running=false;
		BluetoothSIBDiscoverer.isSibServiceFounded=false;
		this.remoteDevices=new ArrayList<RemoteDevice>();
		this.services=new ArrayList<ServiceRecord>();
		try{
    		LocalDevice local = LocalDevice.getLocalDevice();
    		agent = local.getDiscoveryAgent();
    		
    	}catch(Exception e){
    		e.printStackTrace();
    		this.discoveryListener.connectorSIBDiscoveryFinished(this.getConnectorName());
    	}
	}
	
	//Methods for discovering Bluetooth Gateways
	
	@Override
	public void deviceDiscovered(RemoteDevice _remoteDevice, DeviceClass _deviceClass) {
		
		this.remoteDevices.add(_remoteDevice);
	}

	@Override
	public synchronized void inquiryCompleted(int arg0) {
		UUID[] uuids = {new UUID(0x0100L)};//Discovers every url published for the discoveved device 
		
		int[] attrSet = { 0x0001, 0x0002,0x003, 0x0004, 0x0100 };//To extract attributes from server URL
		int numDevices=remoteDevices.size();
		try{
			//Se podria adoptar alg�n mecanismo de nomenclatura para no buscar servicios en todos los bluetooth
			//Es decir ponerle un friendly name al dispositivo del tipo SIB_XXXX
			//NAO exit only when sib sevice has been founded or there are not more devices
			//for(int i=0;i<numDevices && running;i++){
			for(int i=0;i<numDevices && !isSibServiceFounded;i++){
			//fin NAO
				try{
					String name="";
					try{
						name=remoteDevices.get(i).getFriendlyName(false);
						Logger.debug("SIB bluetooth name is " + name);
					}catch(Exception e){
						e.printStackTrace();
					}
					//NAO, allow the search of services in every founded device
//					if(!nombre.equals("") && nombre.startsWith(Properties.getInstance().getProperty("btConnectorProperties", "SIBFriendlyNameStartsWith"))){
						serviceSearchTransactionId=agent.searchServices(attrSet, uuids, remoteDevices.get(i), this);//Launches a service discovery for every device
						wait();
//					}
//					else if(nombre.equals("")){
//						serviceSearchTransactionId=agent.searchServices(attrSet, uuids, remoteDevices.get(i), this);//Lanza la busqueda de servicios dispositivo a dispositivo
//						wait();
//					}
					// finNAO
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			this.discoveryListener.connectorSIBDiscoveryFinished(this.getConnectorName());
			
		}
		BluetoothSIBDiscoverer.running=false;
		//logger.info("InquiryCompleted for all devices");
		this.discoveryListener.connectorSIBDiscoveryFinished(this.getConnectorName());
		
	}

	@Override
	public synchronized void serviceSearchCompleted(int arg0, int arg1) {
		notify();	
		BluetoothSIBDiscoverer.running=false;
	}

	@Override
	public void servicesDiscovered(int arg0, ServiceRecord[] _services) {
		
		for (ServiceRecord service:_services) {
			services.add(service);
			DataElement serviceName = service.getAttributeValue(0x0100);
			String strServiceName=new String();
			 if (serviceName != null) {
                 strServiceName= (String) serviceName.getValue();
             } else {
                 strServiceName=service.getConnectionURL(ServiceRecord.NOAUTHENTICATE_NOENCRYPT,false);
             }
			 
			 if(strServiceName.startsWith("btgwsib")){
				 strServiceName=strServiceName.substring(7, strServiceName.indexOf("_-_-_"));
								 
				java.util.Properties p = new java.util.Properties();
				p.setProperty(BluetoothConnector.CONNECTION_URL, service.getConnectionURL(ServiceRecord.NOAUTHENTICATE_NOENCRYPT,false));
				SIBDescriptor sibDescriptor = new SIBDescriptor(strServiceName, this.getConnectorName(), p);
				this.discoveryListener.connectorSIBDiscovered(sibDescriptor);
				//NAO
				BluetoothSIBDiscoverer.isSibServiceFounded = true;
				//finNAO
			 }
			 
		}
		
	}
	
	
	//Methods for integration with J2SE Smart application
	
	@Override
	public String getConnectorName() {
		return BluetoothConnector.CONNECTOR_NAME;
	}

	@Override
	public void inquiry() {
		try{
			running=true;
			agent.startInquiry(DiscoveryAgent.GIAC, this);
		}catch(Exception e){
			this.discoveryListener.connectorSIBDiscoveryFinished(this.getConnectorName());
		}
	}

	@Override
	public void stopInquiry() {
		running = false;
		try{
			agent.cancelInquiry(this);
		}catch(Exception e){
			
		}
		
		try{
			agent.cancelServiceSearch(serviceSearchTransactionId);
		}catch(Exception e){
			
		}finally{
			this.discoveryListener.connectorSIBDiscoveryFinished(this.getConnectorName());
		}
		
	}
	
	public static boolean isRunning(){
		return running;
	}

}
