/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.connector.impl.bluetooth;

import java.io.DataOutputStream;

import javax.microedition.io.Connector;
import javax.obex.ClientSession;
import javax.obex.HeaderSet;
import javax.obex.Operation;
import javax.obex.ResponseCodes;

public class SendState extends ExchangerState{
	
	public final static int TIME_TO_WRITE_ON_REMOTE=500;
	public final static int SLEEP_TIME_AFTER_SEND_ERROR=2000;
	
	private ClientSession connection = null;
	private String url = null;
	private Operation operation = null;
	private DataOutputStream out = null;
		
	protected SendState(ServerThread _serverThread,String _connectionUrl) {
		super(_serverThread);
		this.url=_connectionUrl;
	}

	@Override
	protected int onGet(Operation op) {
		try{
			op.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return ResponseCodes.OBEX_HTTP_CONFLICT;
	}

	@Override
	protected int onPut(Operation op) {
		try{
			op.close();
		}catch(Exception e){
			e.printStackTrace();

		}
		
		return ResponseCodes.OBEX_HTTP_CONFLICT;
	}
	
	public int doSend(final byte[] _obj, int _numFails){
		
		int responseCode=ResponseCodes.OBEX_HTTP_INTERNAL_ERROR;
		try{
			connection = (ClientSession) Connector.open(url,Connector.WRITE,true);
			connection.connect(null);
			
			// Initiate the PUT request
			HeaderSet headerSet=connection.createHeaderSet();
			headerSet.setHeader(48, this.serverThread.getUuid());//48 first of User Defined Identifiers
		
			operation = connection.put(headerSet);// the same to the onPut method of the server (Bluetooth gateway)
			out = operation.openDataOutputStream();
			
			byte[] vData = _obj; 
			
			int vlen = vData.length;
			byte[] tmpBuf = new byte[vlen + 4];
			System.arraycopy(vData, 0, tmpBuf, 4, vlen);
			tmpBuf[0] = (byte) ((vlen >>> 24) & 0xff);
			tmpBuf[1] = (byte) ((vlen >>> 16) & 0xff);
			tmpBuf[2] = (byte) ((vlen >>> 8) & 0xff);
			tmpBuf[3] = (byte) ((vlen >>> 0) & 0xff);
			
			out.write(tmpBuf);
			Thread.sleep(TIME_TO_WRITE_ON_REMOTE);
			
			try{
				out.close();
			}catch(Exception e){
				e.printStackTrace();
				
				out.flush();
				out.close();
			}
			out=null;
			
			responseCode = operation.getResponseCode();
			operation.close();
			
			this.serverThread.setState(new IdleState(this.serverThread));
			
	        operation=null;
	        
	        if(responseCode != ResponseCodes.OBEX_HTTP_OK){
	           	if(_numFails < RequestObject.DELETE_ON_FAILS){
	        		Thread.sleep(SLEEP_TIME_AFTER_SEND_ERROR);
	        		_numFails++;
	        		serverThread.reAddPendingRequest(new RequestObject(_obj, url, _numFails));
	        	}
	        }
		}catch(Exception e){
			e.printStackTrace();
			
			if(_numFails < RequestObject.DELETE_ON_FAILS){
				try{
					Thread.sleep(SLEEP_TIME_AFTER_SEND_ERROR);
				}catch(Exception ex){
					
				}
				_numFails++;
				serverThread.reAddPendingRequest(new RequestObject(_obj, url, _numFails));
			}
		}
		finally	{
			
		      try{
		    	  if(out!=null){
		    		  out.close();		    		  
		    	  }
		      }catch(Exception e){
		    	  e.printStackTrace();
		      }
		      out=null;
		      
		     
			
		      try { // finalizing operation
		    	  if(operation!=null){
		    		  operation.close();
		    	  }
		      } 
		      catch (Exception e3) {
		    	e3.printStackTrace();
		      }
		      operation=null;

		      try{
		    	  if(connection!=null){
		    		  connection.disconnect(null);
		    	  }
		      }catch(Exception e){
		    	  
		      }
		      
		      try{
		    	  if(connection!=null){
		    		  connection.close();
		    	  }
		      }catch(Exception e){
		    	  e.printStackTrace();
		      }
		      
		      connection=null;
		      
		      if(!(this.serverThread.getState() instanceof IdleState)){
		    	  this.serverThread.setState(new IdleState(this.serverThread));
		      }
		      
		    }
		
		return responseCode;
	}

	@Override
	protected void stopBluetoothOperations() {
		if(connection!=null){
			try{
				connection.close();
			}catch(Exception e){
				e.printStackTrace();
			}
			connection=null;
		}
		
		if(operation!=null){
			try{
				operation.abort();
			}catch(Exception e){
				e.printStackTrace();
			}
			try{
				operation.close();
			}catch(Exception e){
				e.printStackTrace();
			}
			operation=null;
		}
		
		if(out != null){
			try{
				out.close();
			}catch(Exception e){
				e.printStackTrace();
			}
			out=null;
		}
	}
		

}
