/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.connector.impl.bluetooth;

public class RequestObject {

	private byte[] message;
	private String urlConnection;
	private int numFails;
	public final static int DELETE_ON_FAILS=4;
	
	
	public RequestObject(byte[] _message, String _urlConnection, int _numFails){
		super();
		this.message=_message;
		this.urlConnection=_urlConnection;
		this.numFails=_numFails;
		
	}


	public byte[] getMessage() {
		return message;
	}


	public void setMessage(byte[] message) {
		this.message = message;
	}


	public String getUrlConnection() {
		return urlConnection;
	}


	public void setUrlConnection(String urlConnection) {
		this.urlConnection = urlConnection;
	}


	public int getNumFails() {
		return numFails;
	}


	public void setNumFails(int numFails) {
		this.numFails = numFails;
	}
	
	
}
