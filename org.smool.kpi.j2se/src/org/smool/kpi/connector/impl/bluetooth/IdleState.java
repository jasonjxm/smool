/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.connector.impl.bluetooth;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.InputStream;

import javax.obex.Operation;
import javax.obex.ResponseCodes;

public class IdleState extends ExchangerState {
	
	public IdleState(ServerThread _parent) {
		super(_parent);
	}

	@Override
	protected int onGet(Operation op) {
		try{
			op.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return ResponseCodes.OBEX_HTTP_OK;
	}

	@Override
	protected int onPut(Operation op) {
		try	{
			InputStream in = op.openInputStream();
			byte[] fullResult = null;
			{
				byte[] buf = new byte[256];
				ByteArrayOutputStream bout = new ByteArrayOutputStream(2048);
				for (int len = in.read(buf); len >= 0; len = in.read(buf))
					bout.write(buf, 0, len);
				fullResult = bout.toByteArray();
				bout.close();
			}
			ByteArrayInputStream bin = new ByteArrayInputStream(fullResult);
			DataInputStream din = new DataInputStream(bin);
			
			
			int size=din.readInt();
			byte[] vData = new byte[size];
			din.read(vData);

			op.close();
			in.close();
			din.close();
			bin.close();
			serverThread.getListener().notifyResponse(vData);
			
			return ResponseCodes.OBEX_HTTP_OK;

		} 
		catch (Exception e) {
			e.printStackTrace();
			return ResponseCodes.OBEX_HTTP_INTERNAL_ERROR;
		}
	}

	@Override
	protected void stopBluetoothOperations() {
		
	}

}
