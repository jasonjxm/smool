/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.connector.impl.bluetooth;

import java.io.IOException;

import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.LocalDevice;
import javax.microedition.io.Connection;
import javax.microedition.io.Connector;
import javax.obex.HeaderSet;
import javax.obex.Operation;
import javax.obex.ServerRequestHandler;
import javax.obex.SessionNotifier;

import org.smool.kpi.common.Logger;


public class ServerThread extends ServerRequestHandler implements Runnable{
	
	private final static int MAX_TIME_TO_BE_CONNECTED=Integer.parseInt(Properties.getInstance().getProperty("btConnectorProperties","RemoteConnectionMaxTimeHold"));
	private final static int DISCONNECTION_TIME=Integer.parseInt(Properties.getInstance().getProperty("btConnectorProperties", "DisconnectionTime"));
	private ExchangerState state;
	private BluetoothConnector listener;
	
	final private String uuid = Properties.getInstance().getProperty("btConnectorProperties", "SmartAppUuid");
	final private String serverURL = "btgoep://localhost:" + uuid;
	
	private boolean cancelWaitingInvoked = false;
	private boolean connected=false;
	 
	private Thread waitingThread;
	private SendThread sendThread;
	
	private SessionNotifier notifier = null;
	private Connection con = null;
	
	public ServerThread(BluetoothConnector _listener){
		this.listener=_listener;
		this.sendThread=new SendThread(this);
		this.sendThread.startSendThread();
	}
	
	public BluetoothConnector getListener() {
			return listener;
	}

	public void setListener(BluetoothConnector listener) {
		this.listener = listener;
	}

	public ExchangerState getState() {
		return state;
	}


	public void setState(ExchangerState state) {
		this.state = state;
	}
	
	public String getUuid(){
		return this.uuid;
	}
	
	public boolean isConnected(){
		return this.connected;
	}
	
	
	public void startWaiting() {
		cancelWaitingInvoked = false;

		setState(new IdleState(this));
		waitingThread = new Thread(this);//instatiates the class a thread 
		waitingThread.start();//Starts the thread
	}
	 
	public void cancelWaiting(){
		cancelWaitingInvoked = true;

		try{
		    notifier.close(); // indicate to acceptAndOpen that it is canceled
		    cancelWaitingInvoked=true;
			getState().stopBluetoothOperations();
		    
		}catch (IOException e) {
		      // Ignore, closing anyway
		}
	}
	
	public int onGet(Operation op) {
		return getState().onGet(op);
	}

	public int onPut(Operation op) {
		return getState().onPut(op);
	}
	
	public synchronized void onDisconnect(HeaderSet request, HeaderSet reply) {
		super.onDisconnect(request, reply);
		notify();// stops waiting in run()
	}
	
	

	@Override
	public synchronized void run() {
		// initialize stack and make the device discoverable
		try{
			LocalDevice local = LocalDevice.getLocalDevice();
			local.setDiscoverable(DiscoveryAgent.GIAC); 
		}catch (Exception e){
			e.printStackTrace();
			return;
		}
		
		try{
			notifier = (SessionNotifier) Connector.open(serverURL);
		}catch (IOException e2){
				
		}

		// the cycle stops only if cancelWaiting() was called
		do{
			try{      
				try{
					con = notifier.acceptAndOpen(this);
					this.connected=true;
					
					wait(MAX_TIME_TO_BE_CONNECTED); // wait until the remote device disconnects
					
					try{
						if(con!=null){
							con.close();
						}
					}catch (IOException e0){
						e0.printStackTrace();
						con=null;
					}
					
					this.connected=false;
					
					Thread.sleep(DISCONNECTION_TIME);
					sendThread.notifySendThread();
					
				}catch (Exception e1){
					e1.printStackTrace();
					Logger.error(this.getClass().getName(), e1.getMessage());
					return;
				}
			}catch (Exception e){
				e.printStackTrace();
				Logger.error(this.getClass().getName(), e.getMessage());
		        return;
			}

		}while (!cancelWaitingInvoked);
		
		try{
			notifier.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	
		
	}
	
	public boolean isStarted(){
		return this.waitingThread.isAlive();
	}
	

	public void addPendingRequest(RequestObject _requestObject){
		this.sendThread.addRequestObject(_requestObject);
		
	}
	
	public void reAddPendingRequest(RequestObject _requestObject){
		this.sendThread.reAddRequestObject(_requestObject);
	}

}
