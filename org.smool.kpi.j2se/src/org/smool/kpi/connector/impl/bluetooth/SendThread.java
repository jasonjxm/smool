/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.connector.impl.bluetooth;

import java.util.ArrayList;

import javax.obex.ResponseCodes;


public class SendThread implements Runnable{
	private final static int MAX_TIME_TO_BE_CONNECTED=Integer.parseInt(Properties.getInstance().getProperty("btConnectorProperties","RemoteConnectionMaxTimeHold"));
	
	private Thread sendThread;
	private ArrayList<RequestObject> pendingRequests;
	private ServerThread serverThread;
	private boolean finish;
		
	public SendThread(ServerThread _serverThread){
		this.sendThread=new Thread(this, "Blutooth SendThread");
		this.pendingRequests=new ArrayList<RequestObject>();
		this.serverThread=_serverThread;
		this.finish=false;
	}
	
	public synchronized void addRequestObject(RequestObject _requestObject){
		this.pendingRequests.add(_requestObject);

		if(!this.serverThread.isConnected())
			notify();
	}
	
	public synchronized void reAddRequestObject(RequestObject _requestObject){
		this.pendingRequests.add(0, _requestObject);
		
		if(!this.serverThread.isConnected())
			notify();
	}
	
	
	public void startSendThread(){
		this.sendThread.start();
	}
	
	

	
	public synchronized void stopSendThread(){
		this.finish=true;
		notify();
	}
	
	public synchronized void notifySendThread(){
		notify();
	}
	
	
	
	public synchronized void run(){
		while(!finish){
			if(!this.pendingRequests.isEmpty()){
				
				if(!this.serverThread.isConnected()){

					RequestObject requestObject=this.pendingRequests.get(0);
					this.pendingRequests.remove(0);

					SendState sendState=new SendState(this.serverThread, requestObject.getUrlConnection());
					this.serverThread.setState(sendState);
					int responseCode=sendState.doSend(requestObject.getMessage(), requestObject.getNumFails());
					
					if(responseCode==ResponseCodes.OBEX_HTTP_OK){
						try{
							wait(MAX_TIME_TO_BE_CONNECTED);
							
						}catch(Exception e){
							e.printStackTrace();
						}
					}
					
				}else{
					try{
						wait();
					}catch(Exception e){
						
					}
				}
			}else{
				try{
					wait();
				}catch(Exception e){
					
				}
			}
		}
	}
}
