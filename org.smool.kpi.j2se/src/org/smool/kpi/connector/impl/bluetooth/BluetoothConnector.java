/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.connector.impl.bluetooth;

import com.intel.bluetooth.BlueCoveConfigProperties;

import org.smool.kpi.connector.AbstractConnector;
import org.smool.kpi.connector.exception.KPIConnectorException;

public class BluetoothConnector extends AbstractConnector {

	public final static String CONNECTOR_NAME = "Bluetooth";
	public final static String CONNECTION_URL = "CONNECTION_URL";
	public final static String OBEX_MTU="4096";

	private ServerThread serverThread;

	private boolean isStarted;
	

	public BluetoothConnector() {
		this.name = CONNECTOR_NAME;
		Properties prop = Properties.getInstance();
		System.setProperty(BlueCoveConfigProperties.PROPERTY_OBEX_TIMEOUT, prop.getProperty("btConnectorProperties", "ConnectionTimeOut"));
		System.setProperty(BlueCoveConfigProperties.PROPERTY_OBEX_MTU, OBEX_MTU);

		this.serverThread = new ServerThread(this);
		this.isStarted = false;
	}

	public String getConnectionUrl() {
		return this.properties.getProperty(CONNECTION_URL);
	}

	@Override
	public void write(byte[] _msg) {
		try {
			if (!BluetoothSIBDiscoverer.isRunning()) {
				this.connect();
				this.serverThread.addPendingRequest(new RequestObject(_msg, this.getConnectionUrl(), 0));
			} else {
				// Perhaps an Exception
			}
		} catch (KPIConnectorException cex) {
			cex.printStackTrace();
		}
	}
	
	

	// Receives SIB responses to communicate to this KP
	public void notifyResponse(byte[] vData) {
		this.messageListeners.fireMessageReceived(vData);
	}

	@Override
	public void connect() throws KPIConnectorException {
		try {
			if (!this.isStarted) {
				this.isStarted = true;
				this.serverThread.startWaiting();
				while (!this.serverThread.isStarted());
			}
		} catch (Exception ex) {
			throw new KPIConnectorException(ex.getMessage(), ex);
		}

	}

	@Override
	public void disconnect() throws KPIConnectorException {
		serverThread.cancelWaiting();
		this.isStarted = false;
	}

	@Override
	public String getName() {
		return BluetoothConnector.CONNECTOR_NAME;
	}

}
