/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.connector.impl.bluetooth;

import javax.obex.Operation;

public abstract class ExchangerState {

	protected ServerThread serverThread;
	
	protected ExchangerState(ServerThread _serverThread){
		this.serverThread=_serverThread;
	}
	
	protected abstract int onGet(Operation op);
	protected abstract int onPut(Operation op);
	protected abstract void stopBluetoothOperations();
}
