/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/

package org.smool.kpi.ssap;

import java.io.File;
import java.io.FileFilter;
import java.net.JarURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.smool.kpi.common.ClasspathManager;
import org.smool.kpi.common.Logger;
import org.smool.kpi.connector.AbstractConnector;
import org.smool.kpi.connector.AbstractSIBDiscoverer;
import org.smool.kpi.connector.IConnectorSIBDiscoveryListener;
import org.smool.kpi.connector.SIBDescriptor;
import org.smool.kpi.connector.exception.KPIConnectorException;
import org.smool.kpi.ssap.exception.KPISSAPException;


public class SIBDiscoveryAgent implements IConnectorSIBDiscoveryListener {

	//private static final String PATH_SEPARATOR = System.getProperty("path.separator");
	private static final String FILE_SEPARATOR = System.getProperty("file.separator");

	private static final String CONNECTORS_ROOT = "/org/smool/kpi/connector/impl/";
	private static final String DISCOVERER_SUFFIX = "SIBDiscoverer";
	private static final String CONNECTOR_SUFFIX = "Connector";

	private static SIBDiscoveryAgent _instance;

	private SIBDiscoverySupport discoveryListeners;
	private HashMap<String, String> connectors;
	private HashMap<String, AbstractSIBDiscoverer> discoverers;
	private HashMap<String, AbstractSIBDiscoverer> discoverersAux;
	
	private transient boolean lookingForSIBs;
	private int finishedDiscoverers;

	private Object latch;

	private final static FileFilter DIRECTORIES_ONLY = new FileFilter() {
		public boolean accept(File f) {
			if (f.exists() && f.isDirectory()) {
	            return true;
            }
            else {
	            return false;
            }
		}
	};

	private SIBDiscoveryAgent() {
		discoveryListeners = new SIBDiscoverySupport();
		connectors = new HashMap<String, String>();
		discoverers = new HashMap<String, AbstractSIBDiscoverer>();
		discoverersAux = new HashMap<String, AbstractSIBDiscoverer>();
		lookingForSIBs = false;
		latch = new Object();
	}

	public static SIBDiscoveryAgent getInstance() {
		if(_instance == null) {
			synchronized(SIBDiscoveryAgent.class) {
				if(_instance == null) {
					_instance = new SIBDiscoveryAgent();
				}
			}
		}
		return _instance;
	}

	/**
	 * Creates the connectors found in the classpath under CONNECTORS_ROOT.
	 * @throws URISyntaxException
	 */
	public void init() {


		List<String> dirs = this.getSubdirectoriesOf(SIBDiscoveryAgent.CONNECTORS_ROOT);
		if(dirs.size() == 0) {
			Logger.error(this.getClass().getName(), "No connectors found!");
			return;
		}

		for(String dir : dirs) {
			try {
				// Build the path.
				StringBuilder sb1 = new StringBuilder();
				sb1.append(SIBDiscoveryAgent.CONNECTORS_ROOT);
				sb1.append(dir);
				sb1.append("/");
				sb1.append(dir.substring(0, 1).toUpperCase());
				sb1.append(dir.substring(1));

				// Create discoverer.
				StringBuilder sb2 = new StringBuilder();
				sb2.append(sb1);
				sb2.append(DISCOVERER_SUFFIX);
				String s = sb2.toString().substring(1).replace('/', '.');
				AbstractSIBDiscoverer asd = (AbstractSIBDiscoverer)Class.forName(s, true, ClasspathManager.getInstance().getClassLoader()).newInstance();
				asd.setSIBDiscoveryListener(this);

				// Create connector.
				sb2 = new StringBuilder();
				sb2.append(sb1);
				sb2.append(CONNECTOR_SUFFIX);
				s = sb2.toString().substring(1).replace('/', '.');
//				AbstractConnector c = (AbstractConnector)Class.forName(s).newInstance();

				// Add both.
				discoverers.put(asd.getConnectorName(), asd);
				discoverersAux.put(asd.getConnectorName(), asd);
				connectors.put(asd.getConnectorName(), s);

			} catch(Exception e) {
				Logger.error(this.getClass().getName(), "Error found trying to instance connector " + dir);
			}
		}
	}

	public void inquiry() throws KPISSAPException {
		try {
			inquiry(true);
		} catch (KPISSAPException ex) {
			ex.printStackTrace();
			throw new KPISSAPException(ex.getMessage(), ex);
		}
	}

	public synchronized void inquiry(boolean asynch) throws KPISSAPException {
		if(lookingForSIBs) {
			return;
		}
		//Calling init here again or otherwise discovery will only work once, since discovererAux will be empty when discovery finishes
		init();
		lookingForSIBs = true;
		finishedDiscoverers = 0;

		try {
			for(AbstractSIBDiscoverer discoverer: discoverers.values()) {
				discoverer.inquiry();
			}
		} catch (KPIConnectorException ex) {
			ex.printStackTrace();
			throw new KPISSAPException(ex.getMessage(), ex);
		}

		if(!asynch) {
			synchronized(latch) {
				try {
					latch.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public synchronized void stopInquiry() throws KPISSAPException {
		if(!lookingForSIBs) {
			return;
		}
		try {
			for(AbstractSIBDiscoverer discoverer: discoverers.values()) {
				discoverer.stopInquiry();
			}
		} catch (KPIConnectorException ex) {
			ex.printStackTrace();
			throw new KPISSAPException(ex.getMessage(), ex);
		}

	}

	public void addSIBDiscoveryListener(ISIBDiscoveryListener listener) {
		discoveryListeners.addListener(listener);
	}

	public void removeSIBDiscoveryListener(ISIBDiscoveryListener listener) {
		discoveryListeners.removeListener(listener);
	}

	public AbstractConnector createConnector(String connectorName, Properties properties) {
		try {
			String className = connectors.get(connectorName);
//			boolean alive = Boolean.parseBoolean((String)properties.get("KEEP_ALIVE"));
//			//If the connector to be created is TcpipConnector and the keep alive is set to false, instead of creating
//			//NOTE: This is an addition added by AVK 17/10/2011 to instantiate the new class TcpIp
//			if (className.equals("org.smool.kpi.connector.impl.tcpip.TcpipConnector") && !alive){
//				AbstractConnector newConnector = (AbstractConnector)Class.forName("org.smool.kpi.connector.impl.tcpip.TcpIpBlockingConnector", true, ClasspathManager.getInstance().getClassLoader()).newInstance();
//				newConnector.setProperties(properties);
//				return newConnector;
//			}
//			else{
				AbstractConnector newConnector = (AbstractConnector)Class.forName(className, true, ClasspathManager.getInstance().getClassLoader()).newInstance();
				newConnector.setProperties(properties);
				return newConnector;
//			}
				
			

		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void connectorSIBDiscovered(SIBDescriptor sibDescriptor) {
		discoveryListeners.fireSIBDiscovered(sibDescriptor);
	}

	@Override
	public void connectorSIBDiscoveryFinished(String connectorName) {
		discoveryListeners.fireConnectorSIBDiscoveryFinished(connectorName);
		// Only throw and event when ALL discoverers finish the discovery.
		if (discoverersAux.containsKey(connectorName)){
			discoverersAux.remove(connectorName);
			finishedDiscoverers++;
		}
		if(finishedDiscoverers == discoverers.size()) {
			finishedDiscoverers++;
			discoveryListeners.fireSIBDiscoveryFinished();
			lookingForSIBs = false;
			synchronized(latch) {
				latch.notify();
			}
		}
	}
	/**
	 * Looks for he subdirectories of a given directory. It looks for them in the
	 * classpath directories and JARs.
	 * @param parent The parent directory.
	 * @return A list of the directories names.
	 */
	private List<String> getSubdirectoriesOf(String parent) {

		List<String> subdirs = new ArrayList<String>();
		File file = null;

//		String classpath = System.getProperty("java.class.path");
//		StringTokenizer st = new StringTokenizer(classpath, PATH_SEPARATOR);
//		while (st.hasMoreTokens()) {
//			String path = st.nextToken();
//			file = new File(path);
//			include(parent, file, subdirs);
//		}

		URL[] classpath = ClasspathManager.getInstance().getURLs();
		for(int i = 0; i < classpath.length; i++) {
			URL fileURL = classpath[i];
			try {
				file = new File(fileURL.toURI());
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
			include(parent, file, subdirs);
		}

		return subdirs;
	}

	/**
	 * Include the subdirectories of the directory passed in the parameter parent
	 * @param parent The directory whom find the subdirectories.
	 * @param file The file name of a jar or a classpath directory.
	 * @param subdirs The List where save the subdirectories.
	 */
	private final void include(String parent, File file, List<String> subdirs) {
		if (!file.exists()) {
			return;
		}

		if(file.isDirectory()) {
			if(parent.endsWith("/")) {
				parent = parent.substring(0, parent.length()-1);
			}
			File[] dirs = file.listFiles(DIRECTORIES_ONLY);
			String platformIndependentParent = parent.replace("/", FILE_SEPARATOR);
			if(file.getAbsolutePath().endsWith(platformIndependentParent)) {
				for (int i = 0; i < dirs.length; i++) {
					subdirs.add(dirs[i].getName());
				}
			} else {
				for (int i = 0; i < dirs.length; i++) {
					include(parent, dirs[i], subdirs);
				}
			}
		} else {
			// could be a JAR file
			includeJar(parent, file, subdirs);
			return;
		}
	}

	/**
	 * Include the subdirectories of the directory passed in the parameter parent
	 * @param parent The directory whom find the subdirectories.
	 * @param file The file name of the jar.
	 * @param subdirs The List where save the subdirectories.
	 */
	private void includeJar(String parent, File file, List<String> subdirs) {

		URL jarURL = null;
		JarFile jar = null;
		try {
			StringBuilder jarFileName = new StringBuilder().append("jar:file:").append(file.getAbsolutePath()).append("!/");
			jarURL = new URL(jarFileName.toString());
			JarURLConnection conn = (JarURLConnection) jarURL.openConnection();
			jar = conn.getJarFile();
		} catch (Exception e) {
			return;
		}

		if ((jar == null) || (jarURL == null)) {
			return;
		}

		if(parent.startsWith("/")) {
			parent = parent.substring(1);
		}
		if(!parent.endsWith("/")) {
			parent = parent + "/";
		}

		Enumeration<JarEntry> e = jar.entries();
		while (e.hasMoreElements()) {
			JarEntry entry = e.nextElement();

			if(entry.isDirectory()) {
				int index = entry.getName().indexOf(parent);
				if(index != -1) {
					try {
						String suffix = entry.getName().substring(index + parent.length());
						StringTokenizer st = new StringTokenizer(suffix, "/");
						if(st.countTokens() == 1) {
							String s = st.nextToken();
							if(s.endsWith("/")) {
								s = s.substring(0, s.length()-1);
							}
							subdirs.add(s);
						}
					} catch(Exception ex) {
					}
				}
			}
		}
	}

	public String[] getConnectorNames() {
		ArrayList<String> l = new ArrayList<String>();
		for(String conName : connectors.keySet()) {
			l.add(conName);
		}
		return l.toArray(new String[connectors.size()]);
	}

}
