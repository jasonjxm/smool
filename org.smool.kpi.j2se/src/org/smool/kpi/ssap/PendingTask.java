/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.ssap;

import java.util.HashMap;

import org.smool.kpi.ssap.message.SSAPMessage.TransactionType;


public class PendingTask {
	
	/** Indicates if the task has finished */
	protected volatile boolean finished;
	
	/** Indicates if this task is part of another PendingTask */
	protected ComposedPendingTask composite;
	
	/** The transaction type */
	protected TransactionType type;
	
	/** A list of properties in case the pending task want to return any values */
	protected HashMap<String, Object> properties;
	
	public PendingTask(TransactionType type) {
		this.finished = false;
		this.properties = new HashMap<String, Object>();
		this.type = type;
	}
	
	/**
	 * Fires a notify to unblock the threads blocked in the waitToFinish method. 
	 */
	public synchronized void fireNotify() {
		this.finished = true;
		notifyAll();
		if(composite != null) {
			composite.subtaskFinished(this);
		}
	}

	/**
	 * Wait until the task associated to this pending task is finished. 
	 * @return true if the waiting block finish normally, or false if it is interrupted.
	 */
	public boolean waitUntilFinished() {
		return this.waitUntilFinished(0);
	}
	
	/**
	 * Wait until the task associated to this pending task is finished. If the task 
	 * takes more than the time passed in the timeout parameter, the method will
	 * leave.
	 * @param timeout Time in milliseconds for waiting this task to finish
	 * @return true if the waiting block finish normally, or false if it was interrupted.
	 */
	public synchronized boolean waitUntilFinished(long timeout) {
		while(!finished) {
			try {
				wait(timeout);
				return true;
			} catch (InterruptedException e) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Indicates if this task has finished.
	 * @return boolean indicating if this task has finished.
	 */
	public boolean isFinished() {
		return this.finished;
	}
	
	/**
	 * This method is used by the ComposedPendingTask to assign subtasks.
	 * @param composite
	 */
	public void setComposite(ComposedPendingTask composite) {
		this.composite = composite;
	}
	
	/**
	 * Add a property to this list of properties.
	 * @param key The key of the property.
	 * @param value The value of the property.
	 */
	public void addProperty(String key, Object value) {
		properties.put(key, value);
	}
	
	/**
	 * Retrieves an object of the property.
	 * @param key The key associated with the value to retrieve.
	 * @return The object.
	 */
	public Object getProperty(String key) {
		return properties.get(key);
	}
	
	public TransactionType getType() {
		return this.type;
	}
}
