/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.ssap.subscription;

import java.util.HashMap;

import org.smool.kpi.ssap.exception.KPISSAPException;


public class SubscriptionSupport implements ISubscriptionListener {

	protected HashMap<Long, ISubscriptionListener> listeners = null;
	
	public SubscriptionSupport() {
		super();
		listeners = new HashMap<Long, ISubscriptionListener>();
	}
  
	public void fireIndicationReceived(long subscriptionId, String newResults, String obsoleteResults) throws KPISSAPException {
		ISubscriptionListener listener = listeners.get(subscriptionId);
		if(listener == null) {
			throw new KPISSAPException("The listener for subscription " +subscriptionId + " not found");
		}
		listener.indicationReceived(subscriptionId, newResults, obsoleteResults);
	}
	
    public boolean addListener(long subscriptionId, ISubscriptionListener listener) {
        if(listener == null) {
            throw new IllegalArgumentException("The listener can not be null");
        }
        listeners.put(subscriptionId, listener);
        return true;
    }

    public ISubscriptionListener removeListener(long subscriptionId) {
    	return listeners.remove(subscriptionId);
    }

	public void clear() {
		listeners.clear();
	}

	@Override
	public void indicationReceived(long subscriptionId, String newResults, String obsoleteResults) {
		try {
			fireIndicationReceived(subscriptionId, newResults, obsoleteResults);
		} catch (KPISSAPException e) {
			e.printStackTrace();
		}
	}
	
	public int getSize() {
		return listeners.size();
	}
}
