/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.ssap.parser;

import org.smool.kpi.ssap.message.SSAPMessage;
import org.smool.kpi.ssap.message.SSAPMessageIndication;
import org.smool.kpi.ssap.message.SSAPMessageResponse;
import org.smool.kpi.ssap.message.SSAPMessage.MessageType;
import org.smool.kpi.ssap.message.SSAPMessage.TransactionType;
import org.smool.kpi.ssap.message.parameter.SSAPMessageParameter;
import org.smool.kpi.ssap.message.parameter.SSAPMessageRDFParameter;
import org.smool.kpi.ssap.message.parameter.SSAPMessageStatusParameter;
import org.smool.kpi.ssap.message.parameter.SSAPMessageSubscriptionIdParameter;
import org.smool.kpi.ssap.message.parameter.SSAPMessageParameter.NameAttribute;
import org.smool.kpi.ssap.message.parameter.SSAPMessageRDFParameter.TypeAttribute;
import org.smool.kpi.ssap.message.parameter.SSAPMessageStatusParameter.SIBStatus;


/**
 * This class is responsible for parsing the messages that comes from the
 * gateways. 
 * 
 * @author Raul Otaolea, Raul.Otaolea@tecnalia.com, Tecnalia
 *
 */
public class SIBMessageParser {
	
	//Tags
	private static final String TAG_NODE_ID 				= "node_id";
	private static final String TAG_SPACE_ID 				= "space_id";
	private static final String TAG_TRANSACTION_ID 			= "transaction_id";
	private static final String TAG_TRANSACTION_TYPE 		= "transaction_type";
	private static final String TAG_MESSAGE_TYPE 			= "message_type";
			
	public static SSAPMessage parse(byte[] rawMsg) throws SSAPMessageParseException {
		String xml = new String(rawMsg);
		
		String nodeId = getTagContent(xml, TAG_NODE_ID);
		String spaceId = getTagContent(xml, TAG_SPACE_ID);
		String sTransactionId = getTagContent(xml, TAG_TRANSACTION_ID); 
		long transactionId;
		try {
			transactionId= Long.parseLong(sTransactionId);
		} catch(NumberFormatException nfe) {
			throw new SSAPMessageParseException("The value of the tag '"+TAG_TRANSACTION_ID+"' is not a number '"+sTransactionId+"'");
		}
		String sTransactionType = getTagContent(xml, TAG_TRANSACTION_TYPE);
		TransactionType tranType;
		try {
			tranType= TransactionType.valueOf(sTransactionType);
		} catch(Exception e) {
			throw new SSAPMessageParseException("The value of the tag '"+TAG_TRANSACTION_TYPE+"' is not valid '"+sTransactionType+"'");
		}
		
		String sMsgType = getTagContent(xml, TAG_MESSAGE_TYPE);
		MessageType msgType;
		try {
			msgType = MessageType.valueOf(sMsgType);
		} catch(Exception e) {
			throw new SSAPMessageParseException("The value of the tag '"+TAG_MESSAGE_TYPE+"' is not valid '"+sMsgType+"'");
		}

		// Create the response.
		SSAPMessage response;
		if(msgType == MessageType.CONFIRM) {
			response = new SSAPMessageResponse();
		} else {
			response = new SSAPMessageIndication();
		}
		response.setNodeId(nodeId);
		response.setSpaceId(spaceId);
		response.setTransactionId(transactionId);
		response.setTransactionType(tranType);
		response.setMessageType(msgType);

		// The status parameter is mandatory except in INDICATIONS.
		if(msgType != MessageType.INDICATION) {
			String sStatus = getParameterContent(xml, NameAttribute.STATUS.getValue());
			SIBStatus msgStatus = SIBStatus.find(sStatus);
			if(msgStatus == null){
				throw new SSAPMessageParseException("The status parameter not found");
			}
			SSAPMessageParameter statusParam = new SSAPMessageStatusParameter(msgStatus);
			response.addParameter(statusParam);	
		}
		
		//Depending on the transaction type, we create the appropriate parameters.
		if(tranType == TransactionType.QUERY) {
			String sResults = getParameterContent(xml, NameAttribute.RESULTS.getValue());
			SSAPMessageParameter resultsParam = new SSAPMessageRDFParameter(NameAttribute.RESULTS, TypeAttribute.RDFXML, sResults);
			response.addParameter(resultsParam);	
		} else if(tranType == TransactionType.SUBSCRIBE) {
			// Both in CONFIRM and INDICATION we have a subscription_id parameter.
			SSAPMessageParameter subscriptionIdParam = getSubscriptionIdParameter(xml);
			response.addParameter(subscriptionIdParam);	

			if(msgType == MessageType.CONFIRM) {
				String sResults = getParameterContent(xml, NameAttribute.RESULTS.getValue());
				SSAPMessageParameter resultsParam = new SSAPMessageRDFParameter(NameAttribute.RESULTS, TypeAttribute.RDFXML, sResults);
				response.addParameter(resultsParam);		
			} else if(msgType == MessageType.INDICATION) {
				String newResults = getParameterContent(xml, NameAttribute.NEWRESULTS.getValue());
				SSAPMessageParameter newResultsParam = new SSAPMessageRDFParameter(NameAttribute.NEWRESULTS, TypeAttribute.RDFXML, newResults);
				response.addParameter(newResultsParam);		

				String obsoleteResults = getParameterContent(xml, NameAttribute.OBSOLETERESULTS.getValue());
				SSAPMessageParameter obsoleteResultsParam = new SSAPMessageRDFParameter(NameAttribute.OBSOLETERESULTS, TypeAttribute.RDFXML, obsoleteResults);
				response.addParameter(obsoleteResultsParam);		
			}
		// in subscription response, it is not needed the subscription_id
		} else if(tranType == TransactionType.UNSUBSCRIBE) {
			// The parameters both in the INDICATION and CONFIRMATION are the same.
			SSAPMessageParameter subscriptionIdParam = getSubscriptionIdParameter(xml);
			response.addParameter(subscriptionIdParam);	
		}
		return response;
	}
	
	private static SSAPMessageSubscriptionIdParameter getSubscriptionIdParameter(String xml) throws SSAPMessageParseException {
		String sSubscriptionId = getParameterContent(xml, NameAttribute.SUBSCRIPTIONID.getValue()); 
		long subscriptionId;
		try {
			subscriptionId= Long.parseLong(sSubscriptionId);
		} catch(NumberFormatException nfe) {
			throw new SSAPMessageParseException("The value of the parameter '"+NameAttribute.SUBSCRIPTIONID+"' is not a number '"+sSubscriptionId+"'");
		}
		return new SSAPMessageSubscriptionIdParameter(subscriptionId);
	}
	
	private static String getTagContent(String xml, String tag) {
		String opener = new StringBuffer().append("<").append(tag).append(">").toString();
		String closer = new StringBuffer().append("</").append(tag).append(">").toString();
		
		return getContent(xml, opener, closer);
	}
	
	private static String getParameterContent(String xml, String name) {
		
		String opener = new StringBuffer().append("<parameter name='").append(name).append("'>").toString();
		String closer = new StringBuffer().append("</").append("parameter").append(">").toString();
		
		String result = getContent(xml,opener, closer);
		if (result == null) {
			result = getContent(xml,opener.replace("'", "\""),closer);
		}
		return result;
	}
	
	private static String getContent(String xml, String opener, String closer) {
		
		int openerIndex = xml.toLowerCase().indexOf(opener.toLowerCase());
		if(openerIndex == -1) {
			return null;
		}
		openerIndex += opener.length();
		
		int closerIndex = xml.toLowerCase().indexOf(closer.toLowerCase(), openerIndex);
		if(closerIndex == -1) {
			return null;
		}
		
		return xml.substring(openerIndex, closerIndex);
	}
}
