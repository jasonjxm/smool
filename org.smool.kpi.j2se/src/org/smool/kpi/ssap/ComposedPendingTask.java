/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.ssap;

import java.util.ArrayList;
import java.util.Collection;

public class ComposedPendingTask extends PendingTask {
	
	private ArrayList<PendingTask> tasks;
	
	public ComposedPendingTask() {
		super(null);
		tasks = new ArrayList<PendingTask>();
	}
	
	/**
	 * Add a subtask to this composite task. When all subtask finish, this composite task
	 * will also finish.
	 * @param subtask The subtask to be controlled by this composite task.
	 */
	public void addPendingSubtask(PendingTask subtask) {
		this.tasks.add(subtask);
		subtask.setComposite(this);
	}

	/**
	 * This method is used by the subtasks belonging to this composite task to notify
	 * that they have finished.
	 * @param subtask The subtask finished.
	 */
	public void subtaskFinished(PendingTask subtask) {
		// Verify that the task belongs to this composite task.
		if(tasks.contains(subtask)) {
			for(PendingTask pt : tasks) {
				// If at least one subtask is not finished, return.
				if(!pt.isFinished()) {
					return;
				}
			}
			// If all subtasks are finished, notify that this composite task is finished.
			this.fireNotify();
		}
	}
	
	/**
	 * Gets the list of pending tasks
	 * @return a collection of pending tasks
	 */
	public Collection<PendingTask> listPendingTasks() {
		return this.tasks;
	}

	public int size() {
		return tasks.size();
	}
}
