/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.ssap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.smool.kpi.common.Logger;
import org.smool.kpi.connector.AbstractConnector;
import org.smool.kpi.connector.ConnectionErrorException;
import org.smool.kpi.connector.IConnectionListener;
import org.smool.kpi.connector.IConnectorMessageListener;
import org.smool.kpi.connector.exception.KPIConnectorException;
import org.smool.kpi.ssap.actions.JoinCallback;
import org.smool.kpi.ssap.actions.LeaveCallback;
import org.smool.kpi.ssap.actions.RequestCallback;
import org.smool.kpi.ssap.actions.SSAPCallback;
import org.smool.kpi.ssap.exception.KPISSAPException;
import org.smool.kpi.ssap.message.InputMessageWorker;
import org.smool.kpi.ssap.message.OutputMessageWorker;
import org.smool.kpi.ssap.message.SSAPMessageRequest;
import org.smool.kpi.ssap.message.SSAPMessage.TransactionType;
import org.smool.kpi.ssap.message.parameter.SSAPMessageCredentialsParameter;
import org.smool.kpi.ssap.message.parameter.SSAPMessageParameter;
import org.smool.kpi.ssap.message.parameter.SSAPMessageRDFParameter;
import org.smool.kpi.ssap.message.parameter.SSAPMessageParameter.NameAttribute;
import org.smool.kpi.ssap.message.parameter.SSAPMessageRDFParameter.TypeAttribute;
import org.smool.kpi.ssap.subscription.ISubscriptionListener;
import org.smool.kpi.ssap.subscription.SubscriptionSupport;


public class SIB implements IConnectionListener, IConnectorMessageListener {
	
	private AbstractConnector connector;	// Connector being used to connect to.
	
	private String nodeId;	// Node identification.
	private String spaceId;	// Smart Space identification.
	
	private boolean dirty;	// Indicates if the SIB has operations to do.
	private boolean connected = false; // Indicates if this object is connected to a SIB server.
	
	private final ReentrantReadWriteLock rwlPending = new ReentrantReadWriteLock(); // To synchronize operations in pending msgs.
	private final ReentrantReadWriteLock rwlCached = new ReentrantReadWriteLock(); // To synchronize operations in cached msgs.
	
	private List<OutputMessageWorker> cachedMessages;	// A list of the operations to send to the SIB (only in asynch mode)
	private HashMap<Long, PendingSSAPResponse> pendingMessages;	// A list of messages sent to the SIB but no answered yet. 
	private ExecutorService inQueue;	// The queue of incoming messages to the SIB.
	private ExecutorService outQueue;	// The queue of the outgoing messages from the SIB.
	
	private SIBConnectionSupport connectionSupport;
	
	private int timeout;	// Indicates the max value in milliseconds that a request wait for a response from the SIB.
	
	private boolean confirmationRequired = true;
	
	/*
	 * Indicates the value of the keepAlive property of the connector.
	 * It is only used in subscribe and unsubscribe methods for compatibility with other SIBS.
	 */
	private boolean connectorKeepAliveBackup = false;

	protected SubscriptionSupport subscriptions; 
	
	/**
	 * Constructor of the SIB
	 * @param name the smart space id
	 * @param nodeId the node identifier
	 */
	public SIB(String name, String nodeId) {
		this(name, nodeId, null);
	}
	
	/**
	 * Constructor of the SIB
	 * @param spaceId the smart space id
	 * @param nodeId
	 * @param properties
	 */
	public SIB(String spaceId, String nodeId, Properties properties) {
		this.spaceId = spaceId;
		this.nodeId = nodeId;
		// Still not using these SIB properties. May be for credentials in the future.
		this.dirty = false;
		this.cachedMessages = new ArrayList<OutputMessageWorker>();
		this.pendingMessages = new HashMap<Long, PendingSSAPResponse>();
		inQueue = Executors.newSingleThreadExecutor();
		outQueue = Executors.newSingleThreadExecutor();
		connectionSupport = new SIBConnectionSupport();
		
		subscriptions = new SubscriptionSupport();
	}
	
	public void setConnector(AbstractConnector connector) {
		if(this.connector != null) {
			this.connector.removeConnectionListener(this);
			this.connector.removeSIBMessageListener(this);
		}
		this.connector = connector;
		this.connector.addConnectionListener(this);
		this.connector.addSIBMessageListener(this);
	}

	public void addSIBConnectionListener(ISIBConnectionListener listener) {
		connectionSupport.addListener(listener);
	}
	
	public void removeSIBConnectionListener(ISIBConnectionListener listener) {
		connectionSupport.removeListener(listener);
	}
	
	public void publish() throws KPISSAPException {
		Logger.debug(this.getClass().getName(), "SIB starts publishing");
		rwlCached.writeLock().lock();
		try {
			if(cachedMessages.size() == 0) {
				return;
			}
			for(OutputMessageWorker worker : cachedMessages) {
				outQueue.submit(worker);
			}
			cachedMessages.clear();
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new KPISSAPException("The operation is invalid: " + ex.getMessage(), ex);
		} finally {
			rwlCached.writeLock().unlock();
		}
		Logger.debug(this.getClass().getName(), "SIB ends publishing");
	}
	
	public String getName() {
		return spaceId;
	}

	public boolean isDirty() {
		return dirty;
	}
	
	public void setConnected(boolean connected) {
		if(this.connected == connected) {
			return;
		}
		this.connected = connected;
		if (connected) {
			connectionSupport.fireConnectedToSIB();
		} else {
			connectionSupport.fireDisconnectedFromSIB();
		}
	}
	
	public boolean isConnected() {
		return this.connected;
	}
	
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
	
	public int getTimeout() {
		return timeout;
	}
	
	/**
	 * Sets the value of <code>confirm</code> parameter in the SSAPMessage 
	 * @param confirm a boolean value
	 */
	public void setConfirmation(boolean confirm) {
		this.confirmationRequired = confirm;
	}
	
	/**
	 * Gets the value of <code>confirm</code> parameter in the SSAPMessage 
	 * @return a boolean value
	 */
	public boolean getConfirmation() {
		return this.confirmationRequired;
	}
	
	/**
	 * Adds this KP to the SIB
	 * @throws ConnectionErrorException when something does not work
	 */
	public PendingTask join() throws KPISSAPException {
		return this.join(null,null);
	}
		
	/**
	 * Adds this KP to the SIB
	 * @throws ConnectionErrorException when something does not work
	 */
	public PendingTask join(String username, String passwd) throws KPISSAPException {
		Logger.debug(this.getClass().getName(), "JOIN REQUEST@" + this.getNodeId());
		try {
			// 1. Before sending the join request, we have to be connected to a SIB
			connect();
			
			// 2. Create the SSAPMessageRequest
			SSAPMessageRequest request = new SSAPMessageRequest(nodeId, spaceId, TransactionType.JOIN);
			
			if (username != null && !username.equals("") && 
					passwd != null && !passwd.equals("")) {
				SSAPMessageParameter param = new SSAPMessageCredentialsParameter(username,passwd);
				request.addParameter(param);
			}
			
			SSAPCallback callback = new JoinCallback(this);
			
			ArrayList<SSAPCallback> v = new ArrayList<SSAPCallback>();
			v.add(callback);
			
			return sendToSIB(v, request);
		} catch (KPISSAPException sax) {
			sax.printStackTrace();
			throw new KPISSAPException("Cannot send join request to the SIB: " + sax.getMessage(), sax);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new KPISSAPException("An error produced when sending the join request to the SIB: " + ex.getMessage(), ex);
		}		
	}

	/**
	 * Removes this KP from the SIB
	 * @throws ConnectionErrorException when something does not work
	 */
	public PendingTask leave() throws KPISSAPException {
		try {
			// 1. Creates the leave message
			SSAPMessageRequest request = new SSAPMessageRequest(nodeId, spaceId, TransactionType.LEAVE);
	
			SSAPCallback callback = new LeaveCallback(this);
			
			ArrayList<SSAPCallback> v = new ArrayList<SSAPCallback>();
			v.add(callback);
			
			return this.sendToSIB(v, request);
		} catch (KPISSAPException sax) {
			sax.printStackTrace();
			throw new KPISSAPException("Cannot send leave request to the SIB: " + sax.getMessage(), sax);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new KPISSAPException("An error produced when sending the leave request to the SIB: " + ex.getMessage(), ex);
		}
	}

	/**
	 * Inserts into the SIB one model
	 * @param xmlRDF the XML/RDF version of the new model
	 * @return the transaction identifier
	 * @throws ConnectionErrorException when something does not work
	 */
	public PendingTask insert(String content, TypeAttribute encoding) throws KPISSAPException {
		try {
			// 1. Creates the INSERT request based on the XML/RDF 
			SSAPMessageRequest request = new SSAPMessageRequest(nodeId, spaceId, TransactionType.INSERT);
			SSAPMessageParameter parameter = new SSAPMessageRDFParameter(NameAttribute.INSERTGRAPH, encoding, content); 
			request.addParameter(parameter);
			
			// 2. Adds the confirmation required
			if (confirmationRequired) {
				SSAPMessageParameter confirm = new SSAPMessageParameter(NameAttribute.CONFIRM, SSAPMessageParameter.TRUE); 
				request.addParameter(confirm);
			}
			
			SSAPCallback callback = new RequestCallback(this);
			
			ArrayList<SSAPCallback> v = new ArrayList<SSAPCallback>();
			v.add(callback);
			
			return this.sendToSIB(v, request);
		} 
		catch (KPISSAPException sax) {
			sax.printStackTrace();
			throw new KPISSAPException("Cannot send insert request to the SIB: " + sax.getMessage(), sax);
		} 
		catch (Exception ex) {
			ex.printStackTrace();
			throw new KPISSAPException("An error produced when sending the insert request to the SIB: " + ex.getMessage(), ex);
		}
	}

	/**
	 * Removes from the SIB the content information
	 * @param content the content to be removed
	 * @param encoding the encoding to be used for the content 
	 * @return the pending task that waits for the execution
	 * @throws KPISSAPException when something occurs 
	 */
	public PendingTask remove(String content, TypeAttribute encoding) throws KPISSAPException {
		try {
			SSAPMessageRequest request = new SSAPMessageRequest(nodeId, spaceId, TransactionType.REMOVE);
			SSAPMessageParameter parameter = new SSAPMessageRDFParameter(NameAttribute.REMOVEGRAPH, TypeAttribute.RDFXML, content); 
			request.addParameter(parameter);
			if (confirmationRequired) {
				SSAPMessageParameter confirm = new SSAPMessageParameter(NameAttribute.CONFIRM, SSAPMessageParameter.TRUE); 
				request.addParameter(confirm);
			}
	
			SSAPCallback callback = new RequestCallback(this);
			
			ArrayList<SSAPCallback> v = new ArrayList<SSAPCallback>();
			v.add(callback);
			
			return this.sendToSIB(v, request);
		} catch (KPISSAPException sax) {
			sax.printStackTrace();
			throw new KPISSAPException("Cannot send remove request to the SIB: " + sax.getMessage(), sax);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new KPISSAPException("An error produced when sending the remove request to the SIB: " + ex.getMessage(), ex);
		}
			
	}

	public PendingTask update(String insertContent, String removeContent, TypeAttribute encoding) throws KPISSAPException {
		try {
			SSAPMessageRequest request = new SSAPMessageRequest(nodeId, spaceId, TransactionType.UPDATE);
			SSAPMessageParameter parameter = new SSAPMessageRDFParameter(NameAttribute.INSERTGRAPH, encoding, insertContent); 
			request.addParameter(parameter);
			parameter = new SSAPMessageRDFParameter(NameAttribute.REMOVEGRAPH, encoding, removeContent); 
			request.addParameter(parameter);
			if (confirmationRequired) {
				SSAPMessageParameter confirm = new SSAPMessageParameter(NameAttribute.CONFIRM, SSAPMessageParameter.TRUE); 
				request.addParameter(confirm);
			}		
			
			SSAPCallback callback = new RequestCallback(this);
			
			ArrayList<SSAPCallback> v = new ArrayList<SSAPCallback>();
			v.add(callback);
			
			return this.sendToSIB(v, request);
		} catch (KPISSAPException sax) {
			sax.printStackTrace();
			throw new KPISSAPException("Cannot send update request to the SIB: " + sax.getMessage(), sax);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new KPISSAPException("An error produced when sending the update request to the SIB: " + ex.getMessage(), ex);
		}
	}

	/**
	 * Sends a SSAP Message SUBSCRIBE request to the SIB
	 * @param query the query to what to subscribe in the SIB
	 * @param encoding a valid type attribute that determines the query language
	 * @return a PendingTask representing the asynchronous waiting of the task to be performed
	 * @throws KPISSAPException when some error arise during subscription process
	 */
	public PendingTask subscribe(String query, TypeAttribute encoding) throws KPISSAPException {
		try {
			SSAPMessageRequest request = new SSAPMessageRequest(nodeId, spaceId, TransactionType.SUBSCRIBE);

//          C-SIB does not support the second parameter (encoding/type)
//			SSAPMessageParameter parameter = new SSAPMessageRDFParameter(NameAttribute.QUERY, TypeAttribute.RDFM3, query); 
//			request.addParameter(parameter);
			
			SSAPMessageParameter parameter = new SSAPMessageParameter(NameAttribute.QUERY, query); 
			request.addParameter(parameter);
			
			// NOTE: The current C-SIB is not compliant with D5.21 specification, witch applied the parameter 'encoding' instead of 'type' parameter
			parameter = new SSAPMessageParameter(NameAttribute.TYPE, encoding.getValue()); 
			request.addParameter(parameter);
			
			SSAPCallback callback = new RequestCallback(this);
			
			ArrayList<SSAPCallback> v = new ArrayList<SSAPCallback>();
			v.add(callback);

			// this comes from the subscription in rdf-m3
			if(this.subscriptions.getSize() == 0) {
				this.connectorKeepAliveBackup = this.connector.keepAlive();
				this.connector.setKeepAlive(true);
			}
			
			return this.sendToSIB(v, request);
		} catch (KPISSAPException sax) {
			sax.printStackTrace();
			throw new KPISSAPException("Cannot send subscribe request to the SIB: " + sax.getMessage(), sax);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new KPISSAPException("An error produced when sending the subscribe request to the SIB: " + ex.getMessage(), ex);
		}
	}

	/**
	 * Sends a SSAP Message UNSUBSCRIBE REQUEST to the SIB
	 * @param subscriptionId the subscription to be removed from the SIB
	 * @return a PendingTask representing the asynchronous waiting of the task to be performed
	 * @throws KPISSAPException when some error arise during subscription process
	 */
	public PendingTask unsubscribe(long subscriptionId) throws KPISSAPException {
		try {
			SSAPMessageRequest request = new SSAPMessageRequest(nodeId, spaceId, TransactionType.UNSUBSCRIBE);
			SSAPMessageParameter parameter = new SSAPMessageParameter(NameAttribute.SUBSCRIPTIONID, new Long(subscriptionId).toString()); 
			request.addParameter(parameter);
			
			SSAPCallback callback = new RequestCallback(this);
			
			ArrayList<SSAPCallback> v = new ArrayList<SSAPCallback>();
			v.add(callback);
			
			if(subscriptions.getSize() == 0) {
				this.connector.setKeepAlive(this.connectorKeepAliveBackup);
			}
			return this.sendToSIB(v, request);
		} catch (KPISSAPException sax) {
			sax.printStackTrace();
			throw new KPISSAPException("Cannot send unsubscribe request to the SIB: " + sax.getMessage(), sax);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new KPISSAPException("An error produced when sending the unsubscribe request to the SIB: " + ex.getMessage(), ex);
		}
	}

	/**
	 * Sends to the SIB a QUERY REQUEST, including the input parameters
	 * @param query the query to be executed
	 * @param encoding a valid type attribute that determines the query language
	 * @return a PendingTask representing the asynchronous waiting of the task to be performed
	 * @throws KPISSAPException when some error arise during query execution
	 */
	public PendingTask query(String query, TypeAttribute encoding) throws KPISSAPException {
		try {
			SSAPMessageRequest request = new SSAPMessageRequest(nodeId, spaceId, TransactionType.QUERY);
			
//          C-SIB does not support the second parameter (encoding/type)
//			SSAPMessageParameter parameter = new SSAPMessageRDFParameter(NameAttribute.QUERY, TypeAttribute.RDFM3, query); 
//			request.addParameter(parameter);
			
			SSAPMessageParameter parameter = new SSAPMessageParameter(NameAttribute.QUERY, query); 
			request.addParameter(parameter);
			
			// NOTE: The current C-SIB is not compliant with D5.21 specification, witch applied the parameter 'encoding' instead of 'type' parameter
			parameter = new SSAPMessageParameter(NameAttribute.TYPE, encoding.getValue()); 
			request.addParameter(parameter);
			
			SSAPCallback callback = new RequestCallback(this);
			
			ArrayList<SSAPCallback> v = new ArrayList<SSAPCallback>();
			v.add(callback);
			
			return this.sendToSIB(v, request);
		} catch (KPISSAPException sax) {
			sax.printStackTrace();
			throw new KPISSAPException("Cannot send query to the SIB: " + sax.getMessage(), sax);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new KPISSAPException("An error produced when sending the query to the SIB: " + ex.getMessage(), ex);
		}
	}

	/**
	 * Sends a REQUEST to the SIB, with the corresponding tasks to be performed after receiving a response from the SIB
	 * @param callbacks actions to be executed after the retreival of a response from the SIB
	 * @param msg the SSAPMessageRequests that represent a SSAP message request
	 * @return the PendingTask
	 * @throws KPISSAPException
	 */
	private PendingTask sendToSIB(ArrayList<SSAPCallback> callbacks, SSAPMessageRequest request) 
			throws KPISSAPException {
		try {
			//Logger.debug("sending " + request.toString());
			StringBuilder sb = new StringBuilder();
			sb.append(request.getTransactionType()).append(".").append(request.getMessageType());
			sb.append("@").append(this.getNodeId());
			sb.append(">").append(this.getSpaceId());
			Logger.debug(this.getClass().getName(), sb.toString());
			
			PendingTask pt = new PendingTask(request.getTransactionType());
			OutputMessageWorker worker = new OutputMessageWorker(this, connector, pt);
			worker.setMessage(request);
			if(callbacks != null) {
				worker.addCallbacks(callbacks);
			}
			
			rwlCached.writeLock().lock();
			try {
				cachedMessages.add(worker);
			} finally {
				rwlCached.writeLock().unlock();
				dirty = true;
			}
			
			return pt;
		} catch (Exception ex) {
			throw new KPISSAPException("Cannot send to the SIB :" + ex.getMessage(), ex);
		}
	}
	
	@Override
	public void connectionClosed(AbstractConnector connector) {
		this.connected = false;
		connectionSupport.fireDisconnectedFromSIB();
	}

	@Override
	public void connectionError(AbstractConnector connector, String error) {
		this.connected = false;
		connectionSupport.fireConnectionToSIBError(error);		
	}

	@Override
	public void connectionTimeout(AbstractConnector connector) {
		this.connected = false;
		connectionSupport.fireConnectionToSIBTimeout();
	}
	
	/**
	 * This method receives all messages from the SIB.
	 */
	@Override
	public void messageReceived(byte[] rawMessage) {
		InputMessageWorker msg = new InputMessageWorker(this, rawMessage);
		inQueue.submit(msg);
	}
	
	public void addPendingSSAPTask(PendingSSAPResponse task) {
		rwlPending.writeLock().lock();
		try {
			pendingMessages.put(task.getTransactionId(), task);
			Logger.debug(this.getClass().getName(), "Pending task added. Count="+pendingMessages.size());
		} finally {
			rwlPending.writeLock().unlock();
		}
	}
	
	public PendingSSAPResponse getPendingSSAPTask(long transactionId) {
		rwlPending.readLock().lock();
		try {
			return pendingMessages.get(transactionId);
		} finally {
			rwlPending.readLock().unlock();
		}
	}
	
	public void finalizePendingRequest(long transactionId) {
		rwlPending.writeLock().lock();
		try {
			pendingMessages.remove(transactionId);
			Logger.debug(this.getClass().getName(), "Pending task removed. Count="+pendingMessages.size());
		} finally {
			rwlPending.writeLock().unlock();
		}
	}
	
	public int getPendingMessagesCount() {
		rwlPending.readLock().lock();
		try {
			return pendingMessages.size();
		} finally {
			rwlPending.readLock().unlock();
		}
	}
	
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Name: ");
		sb.append(spaceId);
		sb.append(", spaceId: ");
		sb.append(spaceId);
		sb.append(", connector: ");
		sb.append(connector.getName());
		return sb.toString();
	}

	public String getNodeId() {
		return this.nodeId;
	}

	public String getTrimmedKPName() {
		return getTrimmedResourceName(this.nodeId);
	}	
	
	public String getSpaceId() {
		return this.spaceId;
	}

	public void notifyKPInitialized() {
		connectionSupport.fireKPInitialized();
	}

	public void notifyRequestProcessed() {
		connectionSupport.fireRequestProcessed();
	}
	
	private void connect() throws KPISSAPException {
		try {
			if(this.isConnected()) {
				return;
			}
			connector.connect();		
			inQueue = Executors.newSingleThreadExecutor();
			outQueue = Executors.newSingleThreadExecutor();
		} catch (KPIConnectorException cex) {
			cex.printStackTrace();
			throw new KPISSAPException("Cannot connect to the SMOOL IOP:" + cex.getMessage(), cex);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new KPISSAPException("Error in connection: " + ex.getMessage(), ex);
		}
	}
	
	public void destroy() throws KPISSAPException {
		try {
			if(inQueue != null) {
				inQueue.shutdown();
				inQueue = null;
			}
			if(outQueue != null) {
				outQueue.shutdown();
				outQueue = null;
			}
			if(connector != null) {
				connector.disconnect();
			}
			
			this.cachedMessages.clear();
			this.pendingMessages.clear();
		} catch (KPIConnectorException cex) {
			cex.printStackTrace();
			throw new KPISSAPException("Cannot disconnect due to " + cex.getMessage(), cex);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new KPISSAPException("Error in disconnecting: " + ex.getMessage(), ex);
		}
	}

	public void addSubscriptionsListener(long subscriptionId, ISubscriptionListener listener) {
		this.subscriptions.addListener(subscriptionId, listener);
	}

	public void removeSubscriptionsListener(long subscriptionId) {
		this.subscriptions.removeListener(subscriptionId);
	}
	
	public void fireIndicationReceived(long subscriptionId, String newResults, String obsoleteResults) {
		try  {
			this.subscriptions.fireIndicationReceived(subscriptionId, newResults, obsoleteResults);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	/**
	 * Obtains a valid resource name to be sent to the SIB
	 * @param resourceName a resource name
	 * @return a String with an normalized class name in java
	 */
	private String getTrimmedResourceName(String resourceName) {
		
		StringBuilder unicodeResourceName = new StringBuilder(); // Used to hold the output.
        char current;
        
		if (resourceName == null || ("".equals(resourceName))) {
			return "";
		}
        
		resourceName = resourceName.trim();
		
        if (resourceName.startsWith(":")) {
        	resourceName = "_" + resourceName.substring(1);
        }
		
        for (int i = 0; i < resourceName.length(); i++) {
            current = resourceName.charAt(i); // NOTE: No IndexOutOfBoundsException caught here; it should not happen.
            if ((current == 0x9) || (current == 0xA) || (current == 0xD) ||
                ((current >= 0x20) && (current <= 0xD7FF)) ||
                ((current >= 0xE000) && (current <= 0xFFFD)) ||
                ((current >= 0x10000) && (current <= 0x10FFFF)))
            	unicodeResourceName.append(current);
        }
	
		StringBuilder validResourceName = new StringBuilder();
		
		StringTokenizer st = new StringTokenizer(resourceName, ".:-(){}&$ ");
	    
		while (st.hasMoreTokens()) {
	         String word = st.nextToken();
	         validResourceName.append(word);
		}
	    
		return validResourceName.toString();
	}	
}
