/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.ssap;

import org.smool.kpi.common.AbstractSupport;
import org.smool.kpi.connector.SIBDescriptor;


public class SIBDiscoverySupport extends AbstractSupport<ISIBDiscoveryListener> {
	
	public SIBDiscoverySupport() {
		super();
	}
  
	public void fireSIBDiscovered(SIBDescriptor sib) {
		synchronized(listeners) {
			for(ISIBDiscoveryListener listener : listeners) {
				listener.SIBDiscovered(sib);
			}
		}
	}

	public void fireConnectorSIBDiscoveryFinished(String connectorName) {
		synchronized(listeners) {
			for(ISIBDiscoveryListener listener : listeners) {
				listener.SIBConnectorDiscoveryFinished(connectorName);
			}
		}
	}
	
	public void fireSIBDiscoveryFinished() {
		synchronized(listeners) {
			for(ISIBDiscoveryListener listener : listeners) {
				listener.SIBDiscoveryFinished();
			}
		}
	}

}
