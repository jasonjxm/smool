/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.ssap.actions;

import org.smool.kpi.ssap.SIB;
import org.smool.kpi.ssap.exception.KPISSAPException;


public class ModelInsertCallback extends SSAPCallback {
	
	public ModelInsertCallback(SIB sib) {
		super(sib);
	}

	public void processCallback() throws KPISSAPException {
		try {
			sib.notifyKPInitialized();
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new KPISSAPException("Cannot process the callback for the model insertion: " + ex.getMessage(), ex);
		}
	}	
}
