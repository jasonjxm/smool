/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.ssap.actions;

import org.smool.kpi.ssap.SIB;
import org.smool.kpi.ssap.exception.KPISSAPException;


public abstract class SSAPCallback implements Runnable {

	protected SIB sib;
	private boolean error;
	private Throwable exception;
	
	public SSAPCallback(SIB sib) {
		this.sib = sib;
	}

	public abstract void processCallback() throws KPISSAPException;
	
	@Override
	public void run() {
		try {
			processCallback();
		} catch (KPISSAPException sax) {
			sax.printStackTrace();
			this.error = true;
			this.exception = sax;
		} catch(Exception e) {
			this.error = true;
			this.exception = e;
		}
	}
	
	public boolean hadError() {
		return this.error;
	}
	
	public Throwable getException() {
		return exception;
	}
}
