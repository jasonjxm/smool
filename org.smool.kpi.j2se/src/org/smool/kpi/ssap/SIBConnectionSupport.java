/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.ssap;

import org.smool.kpi.common.AbstractSupport;

public class SIBConnectionSupport extends AbstractSupport<ISIBConnectionListener> {
	
	public SIBConnectionSupport() {
		super();
	}
  
	public void fireConnectedToSIB() {
		synchronized(listeners) {
			for(ISIBConnectionListener listener : listeners) {
				listener.connectedToSIB();
			}
		}
	}
	
	public void fireDisconnectedFromSIB() {
		synchronized(listeners) {
			for(ISIBConnectionListener listener : listeners) {
				listener.disconnectedFromSIB();
			}
		}
	}
	
	public void fireConnectionToSIBTimeout() {
		synchronized(listeners) {
			for(ISIBConnectionListener listener : listeners) {
				listener.connectionToSIBTimeout();
			}
		}
	}

	public void fireConnectionToSIBError(String error) {
		synchronized(listeners) {
			for(ISIBConnectionListener listener : listeners) {
				listener.connectionToSIBError(error);
			}
		}
	}
	
	/**
	 * Sends a notification to the SMOOLModel that the model was successfully inserted
	 */
	public void fireKPInitialized() {
		synchronized(listeners) {
			for(ISIBConnectionListener listener : listeners) {
				listener.kpInitialized();
			}
		}
	}
	
	/**
	 * Sends a notification to the SMOOLModel that there was an error in the
	 * SSAP Message sending
	 */
	public void fireRequestProcessed() {
		synchronized(listeners) {
			for(ISIBConnectionListener listener : listeners) {
				listener.requestProcessed();
			}
		}
	}	
		
}