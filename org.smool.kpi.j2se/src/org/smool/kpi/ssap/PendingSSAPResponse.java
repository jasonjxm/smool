/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.ssap;

import java.util.ArrayList;

import org.smool.kpi.ssap.actions.SSAPCallback;
import org.smool.kpi.ssap.exception.KPISSAPException;
import org.smool.kpi.ssap.message.SSAPMessageRequest;


public class PendingSSAPResponse {

	private long id;
	private SSAPMessageRequest request;
	private ArrayList<SSAPCallback> callbacks;
	private long timestamp;
	private PendingTask pendingTask;
	
	public PendingSSAPResponse() {
		callbacks = new ArrayList<SSAPCallback>();
	}
	
	/**
	 * @return the id
	 */
	public long getTransactionId() {
		return id;
	}

	/**
	 * @return the request
	 */
	public SSAPMessageRequest getSSAPMessageRequest() {
		return request;
	}
	/**
	 * @param request the request to set
	 */
	public void setSSAPMessageRequest(SSAPMessageRequest request) {
		this.request = request;
		this.id = request.getTransactionId();
	}
	
	/**
	 * @param callback the callback to set
	 */
	public void addCallback(SSAPCallback callback) {
		this.callbacks.add(callback);
	}
	
	/**
	 * @param timestamp the timestamp to set
	 */
	public void timestamp() {
		this.timestamp = System.currentTimeMillis();
	}
	
	public long getElapsedTime() {
		return System.currentTimeMillis() - timestamp;
	}
	
	public boolean executeCallbacks() throws KPISSAPException {
		boolean error = false;
		for(SSAPCallback callback : callbacks) {
			callback.run();
			error &= callback.hadError();
			if (error) {
				throw new KPISSAPException("Cannot execute callbacks: " + callback.getException().getMessage(), callback.getException());
			}
		}
		return !error;
	}	

	public PendingTask getPendingTask() {
		return this.pendingTask;
	}
	
	public void setPendingTask(PendingTask pendingTask) {
		this.pendingTask = pendingTask;
	}
	
	public void notifyPendingTask() {
		if(this.pendingTask != null) {
			this.pendingTask.fireNotify();
		}
	}
}
