/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.ssap.message;

import java.util.Collection;

import org.smool.kpi.ssap.message.parameter.SSAPMessageParameter;


/**
 * The specialized SSAPMessage for requests
 * 
 * @author Raul Otaolea
 */
public class SSAPMessageRequest extends SSAPMessage {

	private static long transactionBase;	// Transaction generator.
	private static Object generatorLatch = new Object();	
	
	/** the time of generation */
	private long sentTime;
	
	public SSAPMessageRequest(String nodeId, String spaceId, long transactionId, 
			TransactionType tranType) {
		super();
		setNodeId(nodeId);
		setSpaceId(spaceId);
		setTransactionType(tranType);
		setMessageType(MessageType.REQUEST);
		setTransactionId(transactionId);
	}

	public SSAPMessageRequest(String nodeId, String spaceId, long transactionId, 
			TransactionType tranType, Collection<SSAPMessageParameter> parameters) {
		super();
		setNodeId(nodeId);
		setSpaceId(spaceId);
		setTransactionType(tranType);
		setMessageType(MessageType.REQUEST);
		setTransactionId(transactionId);
		this.addParameters(parameters);
	}	
	
	public SSAPMessageRequest(String nodeId, String spaceId, 
			TransactionType tranType) {
		super();
		setNodeId(nodeId);
		setSpaceId(spaceId);
		setTransactionType(tranType);
		setMessageType(MessageType.REQUEST);
		setTransactionId(generateTransactionId());
	}
	
	private long generateTransactionId() {
		synchronized(generatorLatch) {
			return ++transactionBase;
		}
	}
	
	/**
	 * Saves the time
	 */
	public void saveTime() {
		this.sentTime = System.currentTimeMillis();
	}
	
	/**
	 * Gets the time for this SSAPMessage
	 * @return the long representation of the time
	 */
	public long getSentTime() {
		return sentTime;
	}
}
