/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.ssap.message;

import org.smool.kpi.common.Logger;
import org.smool.kpi.ssap.PendingSSAPResponse;
import org.smool.kpi.ssap.SIB;
import org.smool.kpi.ssap.exception.KPISSAPException;
import org.smool.kpi.ssap.message.SSAPMessage.MessageType;
import org.smool.kpi.ssap.message.SSAPMessage.TransactionType;
import org.smool.kpi.ssap.message.parameter.SSAPMessageRDFParameter;
import org.smool.kpi.ssap.message.parameter.SSAPMessageSubscriptionIdParameter;
import org.smool.kpi.ssap.message.parameter.SSAPMessageStatusParameter.SIBStatus;
import org.smool.kpi.ssap.parser.SIBMessageParser;
import org.smool.kpi.ssap.parser.SSAPMessageParseException;


/**
 * The runnable class that is queued into the incoming message queue.
 * 
 * @author Fran Ruiz, Fran.Ruiz@tecnalia.com, Tecnalia
 * @author Raul Otaolea, Raul.Otaolea@tecnalia.com, Tecnalia
 *    
 */
public class InputMessageWorker implements Runnable {

	/** The SIB */
	private SIB sib;
	
	/** The received message */
	private byte[] rawMsg;
	
	/**
	 * Constructor
	 * @param sib a SIB
	 * @param response the received response
	 */
	public InputMessageWorker(SIB sib, byte[] response) {
		this.sib = sib;
		this.rawMsg = response;
	}
	
	@Override
	public void run() {
		PendingSSAPResponse task = null;
		try {
			SSAPMessage response = SIBMessageParser.parse(rawMsg);
		
			// INDICATIONs have no pending tasks, so must actively notify the SIB.
			if(response.getMessageType() == MessageType.INDICATION) {
				SSAPMessageRDFParameter newResults = (SSAPMessageRDFParameter)response.getNewResultsParameter();
				SSAPMessageRDFParameter obsoleteResults = (SSAPMessageRDFParameter)response.getObsoleteResultsParameter();
				SSAPMessageSubscriptionIdParameter p = (SSAPMessageSubscriptionIdParameter)response.getSubscriptionIdParameter();
				sib.fireIndicationReceived(new Long(p.getContent()), newResults.getContent(), obsoleteResults.getContent());
			// The rest of the messages have a pending task.
			} else {
				task = sib.getPendingSSAPTask(response.getTransactionId());
				if(task == null) {
					Logger.warn(this.getClass().getName(), "There is no pending task for: " + response.getTransactionId());
					return;
				}
			
				sib.finalizePendingRequest(response.getTransactionId());
				if(response.getStatusParameter().getStatus() == SIBStatus.STATUS_OK) {
					task.executeCallbacks();
				} 
				task.getPendingTask().addProperty("status", response.getStatusParameter().getStatus());
				
				// In case of CONFIRM of a SUBSCRIPTION message, add a property with the subscriptionId.
				if(response.getTransactionType() == TransactionType.SUBSCRIBE 
					|| response.getTransactionType() == TransactionType.UNSUBSCRIBE) {
					SSAPMessageSubscriptionIdParameter p = (SSAPMessageSubscriptionIdParameter)response.getSubscriptionIdParameter();
					task.getPendingTask().addProperty("subscriptionId", p.getSubscriptionId());
					if(response.getTransactionType() == TransactionType.SUBSCRIBE) {
						SSAPMessageRDFParameter results = (SSAPMessageRDFParameter)response.getResultsParameter();
						task.getPendingTask().addProperty("results", results.getContent());
					}
				}
				
				// In case of CONFIRM of a QUERY message, add a property with the results.
				if(response.getTransactionType() == TransactionType.QUERY &&
						response.getMessageType() == MessageType.CONFIRM) {
					SSAPMessageRDFParameter results = (SSAPMessageRDFParameter)response.getResultsParameter();
					task.getPendingTask().addProperty("results", results.getContent());
				}				
			}
		} catch (SSAPMessageParseException spex) {
			spex.printStackTrace();
//			if(task != null) {
//				task.getPendingTask().addProperty("error", spex);
//			}
		} catch (KPISSAPException ex) {
			ex.printStackTrace();
			if(task != null) {
				task.getPendingTask().addProperty("error", ex);
			}
		} catch(Exception e) {
			if(task != null) {
				task.getPendingTask().addProperty("error", e);
			}
		} finally {
			if(task != null) {
				task.notifyPendingTask();
			}
		}
	}
}
