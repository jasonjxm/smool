/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.ssap.message;

public class Triple {

	private String subject;
	private String predicate;
	private String objectType;
	private String objectValue;
	
	public static String WILDCARD = "sib:any";
	
	public static String LITERAL = "literal";
	public static String URI = "uri";
	
	public Triple () {
		this.subject = null;
		this.predicate = null;
		this.objectType = null;
		this.objectValue = null;
	}
	
	public Triple (String subject, String predicate, String objectType, String objectValue) {
		this.subject = subject;
		this.predicate = predicate;
		this.objectType = objectType;
		this.objectValue = objectValue;
	}
	
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getSubject() {
		return subject;
	}
	public void setPredicate(String predicate) {
		this.predicate = predicate;
	}
	public String getPredicate() {
		return predicate;
	}
	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}
	public String getObjectType() {
		return objectType;
	}
	public void setObjectValue(String objectValue) {
		this.objectValue = objectValue;
	}
	public String getObjectValue() {
		return objectValue;
	}
	
	public boolean hasBNode() {
		if (subject.contains("_:") || objectValue.contains("_:")) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("<");
		
		sb.append("<");
		sb.append(subject);
		sb.append(">");
		
		sb.append("<");
		sb.append(predicate);
		sb.append(">");

		sb.append("<");
		sb.append(objectValue);
		sb.append(">");
		
		sb.append(">");
		return sb.toString();
	}
}
