/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.ssap.message;

import java.util.ArrayList;

import org.smool.kpi.common.Logger;
import org.smool.kpi.connector.AbstractConnector;
import org.smool.kpi.ssap.PendingSSAPResponse;
import org.smool.kpi.ssap.PendingTask;
import org.smool.kpi.ssap.SIB;
import org.smool.kpi.ssap.actions.SSAPCallback;
import org.smool.kpi.ssap.message.parameter.SSAPMessageParameter;


/**
 * The runnable class that is queued into the outgoing message queue.
 * 
 * @author Fran Ruiz, Fran.Ruiz@tecnalia.com, Tecnalia
 * @author Raul Otaolea, Raul.Otaolea@tecnalia.com, Tecnalia
 *    
 */
public class OutputMessageWorker implements Runnable {

	private SSAPMessageRequest request;
	private AbstractConnector connector;
	private SIB sib;
	private ArrayList<SSAPCallback> callbacks;
	private PendingTask pendingTask;
	
	
	public OutputMessageWorker(SIB sib, AbstractConnector connector, PendingTask pendingTask) {
		this.sib = sib;
		this.connector = connector;
		this.callbacks = new ArrayList<SSAPCallback>();
		this.pendingTask = pendingTask;
	}

	public void setMessage(SSAPMessageRequest msg) {
		this.request = msg;
	}
	
	public SSAPMessageRequest getMessage() {
		return request;
	}
	
	public long getTransactionId() {
		if(request == null) {
			return -1;
		} else {
			return request.getTransactionId();
		}
	}
	
	public void run() {
		// In the INSERT, REMOVE and UPDATE operations, the confirmation is optional.If a confirmation is not needed
		// the message do not generate a pending task.
		switch(request.getTransactionType()) {
			case INSERT:
			case REMOVE:
			case UPDATE:
				SSAPMessageParameter param = request.getConfirmParameter();
				// if not specified, do not wait for the response
				if(param != null) {
					Boolean confirmation = new Boolean(param.getContent());
					if(confirmation) {
						this.addToPendingTasks();
					} else {
						pendingTask.notifyAll();
					}
				}
				break;
			case JOIN:
			case LEAVE:
			case QUERY:
			case SUBSCRIBE:
			case UNSUBSCRIBE:
				this.addToPendingTasks();
				break;
			default:
				pendingTask.notifyAll();
				Logger.warn(this.getClass().getName(), "Unknown transaction type '" + request.getTransactionType() + "'");
		}
		
		// Tell to the connector to send the information.
		connector.write(request.toByteArray());
	}

	private void addToPendingTasks() {
		PendingSSAPResponse task = new PendingSSAPResponse();
		task.timestamp();
		task.setSSAPMessageRequest(request);
		task.setPendingTask(pendingTask);
		for (SSAPCallback callback : callbacks) {
			task.addCallback(callback);
		}
		sib.addPendingSSAPTask(task);	
	}
	
	public void addCallback(SSAPCallback callback) {
		this.callbacks.add(callback);
	}

	public void addCallbacks(ArrayList<SSAPCallback> callbackList) {
		this.callbacks.addAll(callbackList);
	}
}
