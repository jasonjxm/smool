/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.kpi.ssap.message.parameter;

/**
 * Extension to parse the subscription id parameter
 * 
 * @author Fran Ruiz, Fran.Ruiz@tecnalia.com, Tecnalia
 * @author Raul Otaolea, Raul.Otaolea@tecnalia.com, Tecnalia
 *
 */
public class SSAPMessageSubscriptionIdParameter extends SSAPMessageParameter {
	
	public SSAPMessageSubscriptionIdParameter(long id) {
		super(NameAttribute.SUBSCRIPTIONID, String.valueOf(id));
	}
	
	public long getSubscriptionId() {
		return Long.parseLong(content);
	}
}
