SMOOL feature with all the required bundles (see SMOOL WIKI)

see http://www.vogella.com/tutorials/EclipsePlugin/article.html

## create new feature (already done)

- eclipse/new/other/feature, then add the the org.osgi.service.log impl plugin and then add all the smool plugins (some of them could be removed)

- optional: create a category file from the eclipse/new/other/category


## build

- in feature.xml/tab overview/section exporting, follow the steps. In the export one you can use whatever folder although there is a **updatesite** folder in the code just for this case. Make sure in the options tab that you select the category.xml if you created it before.

> Sometimes some jars are not regenerated so BETTER TO START a run configuration before these steps so make sure that the generated jars contain the correct files.

## install

In another Eclipse:

- first install GEMDE (features core & mofscript engine for gemde)
- then install SMOOL (help/install new software and add the folder of the updatesite) and restart 

## uninstall

- eclipse/help/about/installation_details/installed_software and remove the SMOOL features

## test/logs

- start from command line `./eclipse -consoleLog -debug`