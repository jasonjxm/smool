/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.wizard.project;

import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.smool.sdk.common.nls.SmoolMessages;
import org.smool.sdk.common.properties.PropertyLoader;
import org.smool.sdk.core.generator.kpi.SmartAppProject;
import org.smool.sdk.ui.wizard.project.data.WizardData;
import org.smool.sdk.ui.wizard.project.pages.NewKPIProjectPage;
import org.smool.sdk.ui.wizard.project.pages.PlatformSelectionPage;


/**
 * The ADKWizard class determines the Wizard that will be thrown
 * in the activation of this plugin.
 *
 * An implementation of a wizard. The ADKWizard must contain the following 
 * methods to configure the wizard: 
 * 
 * <ul><li>addPage</li>
 * <li>setHelpAvailable</li>
 * <li>setDefaultPageImageDescriptor</li>
 * <li>setDialogSettings</li>
 * <li>setNeedsProgressMonitor</li>
 * <li>setTitleBarColor</li>
 * <li>setWindowTitle</li></ul>
 *  
 * This class may override these methods if required:
 *  
 * <ul><li>createPageControls</li>
 * <li>performCancel</li>
 * <li>addPages</li>
 * <li>performFinish</li>
 * <li>dispose</li></ul>
 * 
 * @author Fran Ruiz, Fran.Ruiz@tecnalia.com, Tecnalia
 *
 */
public class ProjectWizard extends Wizard implements INewWizard {

	private static final String WIZARD_IMAGE = "images.smool";
	
	/** The new project page */
	private NewKPIProjectPage newProjectPage;

	/** The connector and platform selection page */
	private PlatformSelectionPage platform;
	
	/** the workbench instance */
	protected IWorkbench workbench;	

	/** workbench selection when the wizard was started */
	protected IStructuredSelection selection;	

	/** The logger */
	//private static Logger logger = Logger.getLogger(ProjectWizard.class);
	
	/** The model that stores all data selected during the wizard **/
	protected WizardData model = null;
	
	/**
	 * Constructor
	 */
	public ProjectWizard() {
		super();
		
		// Creates a new ADK Model
		model = new WizardData();
		
		// sets the window page title
		setWindowTitle(SmoolMessages.ADKWizard_windowTitle);
		
		// allows the usage of the progress monitor
		setNeedsProgressMonitor(true);
		
	}
	
	/**
	 * Adds any wizard page to this wizard.
	 * This method is called just before the wizard becomes visible, 
	 * to give the wizard the opportunity to add any lazily created pages.
	 */
	public void addPages() {
		// Get the image descriptor to show it in the wizard
		ImageDescriptor imageDesc = new ImageDescriptor() {
			@Override
			public ImageData getImageData() {
				ImageData imgData = null;
				try {
					// Obtain the URL
		 			PropertyLoader props = PropertyLoader.getInstance();
					String smoolImg = props.getProperty(WIZARD_IMAGE);
					
					// get the bundle and its location
				    InputStream is = this.getClass().getResourceAsStream(smoolImg);
					imgData = new ImageData(is);
				} catch (Exception e) {
					System.out.println("An exception arised loading the image: " + e.toString());
					e.printStackTrace();
				}
				return imgData;
			}
		};
		
		// Creates a page where the location and selection of domain are selected
		newProjectPage = new NewKPIProjectPage();
		newProjectPage.setImageDescriptor(imageDesc);
		addPage(newProjectPage);

		// Creates a page where the platform and connectors are selected
		platform = new PlatformSelectionPage(workbench, selection);
		platform.setImageDescriptor(imageDesc);
		addPage(platform);
	}
	
	/**
	 * Initializes this creation wizard using the passed workbench 
	 * and object selection.
	 * This method is called after the no argument constructor and before 
	 * other methods are called.
	 * 
	 *  @param workbench the current workbench
	 *  @param selection the current object selection
	 */
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.workbench = workbench;
		this.selection = selection;
		model = new WizardData();
	}
	
	/**
	 * Subclasses must implement this Wizard method to perform any 
	 * special finish processing for their wizard.
	 * 
	 * @return true to indicate the finish request was accepted, 
	 * and false to indicate that the finish request was refused
	 *
	 */
	@Override
	public boolean performFinish() {
		System.out.println("Performing finish");
		final String projectName = newProjectPage.getProjectName();
		ArrayList<String> models = new ArrayList<String>(newProjectPage.listModelLibraries());
		for (String modelLibrary : models) {
			model.addModel(modelLibrary);
		}
		
		// Starts the progress monitor
		IRunnableWithProgress op = new IRunnableWithProgress() {
			public void run(IProgressMonitor monitor) throws 
				InvocationTargetException, InterruptedException {
				
                try {
                	doFinish(projectName, monitor);
                } catch (CoreException e) {
                    throw new InvocationTargetException(e);
                } finally {
                    monitor.done();
                }				
			}
		};
		
        try {
        	System.out.println("Running the progress monitor");
            this.getContainer().run(true, false, op);
            
            
        } catch (InterruptedException e) {
            return false;
        } catch (InvocationTargetException e) {
            Throwable realException = e.getTargetException();
            MessageDialog.openError(getShell(), "Error", realException.getMessage());
            return false;
//        } catch (WorkbenchException e) {
//			logger.error("Cannot perform finish due to a WorkbenchException: " + e.getMessage());
//			e.printStackTrace();
        } catch (Exception e) {
			System.out.println("Cannot perform finish due to a Exception:" + e.getMessage());
			e.printStackTrace();
		}
        return true;		
	}

	/**
     * The worker method. It will find the container, create the
     * file if missing or just replace its contents, and open
     * the editor on the newly created file.
     */
    private void doFinish(String projectName, IProgressMonitor monitor)
    		throws CoreException {
    	try {	
    	
    		// Getting the information of the project
    		SmartAppProject projectGenerator = new SmartAppProject(model);
			
    		projectGenerator.generate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
    }
	
	/**
	 * Gets the platform and connection selection page
	 * @return The platform and connection selection page
	 */
	public PlatformSelectionPage getPlatformPage() {
		return platform;
	}
	
	/**
	 * Gets the model containing the selections made during the wizard
	 * @return The ADKModel
	 */
    public WizardData getModel() {
		return model;
	}
    
    @Override
    public boolean canFinish() {
    	boolean projectPageComplete = newProjectPage.isPageComplete();
    	
    	boolean platformSelectionComplete = platform.isPageComplete();
    	
    	return projectPageComplete & platformSelectionComplete;
    }
}
