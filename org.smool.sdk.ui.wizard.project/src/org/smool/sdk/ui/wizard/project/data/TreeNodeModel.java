/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.wizard.project.data;

import org.eclipse.swt.graphics.Image;

/**
 * This class is an abstract class that implements the tree node model
 * to generate the elements contained in the TreeViewer
 * 
 * @author Fran Ruiz, Fran.Ruiz@tecnalia.com, Tecnalia
 *
 */
public abstract class TreeNodeModel {

	/** The name */
	protected String name;
	
	/** The image associated to this node */
	protected Image image;
	
	/** The logger */
	//private static Logger logger = Logger.getLogger(TreeNodeModel.class);

	/**
	 * Constructor of the TreeNodeModel.
	 * Establishes the initial values for the nodes.
	 */
	public TreeNodeModel() {
		System.out.println("Creates a new TreeNodeModel");
		name = null;
		image = null;
	}
	
	/**
	 * Gets the name to display for this node in the tree
	 * @return a String indicating the node name
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Sets the name to display for this node in the tree
	 * @param name a String indicating the node name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the image to display for this node in the tree
	 * @return the image to display in the tree node
	 */
	public Image getImage() {
		return this.image;
	}
	
	/**
	 * Sets the image to display for this node in the tree
	 * @param image the image to display associated to the node
	 */
	public void setImage(Image image) {
		this.image = image;
	}	

}
