/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.wizard.project.pages.frames;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.osgi.util.TextProcessor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.smool.sdk.common.nls.SmoolMessages;


/**
 * A class that implements the frame where a project location
 * is set.
 *  
 * @author Fran Ruiz, Fran.Ruiz@tecnalia.com, Tecnalia
 *
 */
public class LocationSelectionArea {

	/** Constants definition */
	private static final int SIZING_TEXT_FIELD_WIDTH = 250;

	private static final String FILE_SCHEME = "file"; //$NON-NLS-1$
	
	/** Widget definition */
    private Label locationLabel;
    private Text locationPathField;
    private Button browseButton;
    
    /** The project name */
    private String projectName = "";    
    
    /** The user path */
    private String userPath = "";
    
    /** The default location button */
    private Button useDefaultsButton;
    
    /** The Project Reference */
    private IProject existingProject;
    
    /** The project path */
    private IPath initialLocationFieldValue;
    
    /** The logger */
	//private static Logger logger = Logger.getLogger(LocationSelectionArea.class);

    /**
     * Create a new instance of the receiver.
     * @param composite The composite
     * @param startProject The starting project
     */
    public LocationSelectionArea(Composite composite, IProject startProject) {
 
         projectName = startProject.getName();
         existingProject = startProject;
 
         boolean defaultEnabled = true;
         
         try {
             defaultEnabled = startProject.getDescription().getLocationURI() == null;
         } catch (CoreException e1) {
        	 System.out.println("Using default project");
         }

         createContents(composite, defaultEnabled);
     }
 
     /**
      * Create a new instance of a ProjectContentsLocationArea.
      * @param composite The composite
      */
     public LocationSelectionArea(Composite composite) {
         initialLocationFieldValue = Platform.getLocation();
    	 
    	 // If it is a new project always start enabled
         createContents(composite, true);
     }
 
     /**
      * Create the project location components.
      * 
      * @param composite The composite
      * @param defaultEnabled Determines if the 'Use default location' check 
      * is enabled by default 
      */
     private void createContents(Composite composite, boolean defaultEnabled) {
 
    	 Group locationGroup = new Group(composite, SWT.SHADOW_ETCHED_IN);
    	 locationGroup.setText(SmoolMessages.ProjectSelectionArea_groupTitle);    	 
    	 
    	 // In this frame we have to establish 4 columns:
    	 // One for the check of default location
    	 // One for the location label
    	 // One for the text field
    	 // One for the browse button
    	 int columns = 4;
 
         // project location group
    	 //Composite location = new Composite(locationGroup, SWT.NONE);
    	 GridLayout layout = new GridLayout();

    	 layout.numColumns = columns;

    	 locationGroup.setLayout(layout);
    	 locationGroup.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

         // Sets the default location button
         useDefaultsButton = new Button(locationGroup, SWT.CHECK | SWT.RIGHT);
         useDefaultsButton.setText(SmoolMessages.ProjectSelectionArea_defaultTitle);
         // Use defaultEnables value to set it checked or not
         useDefaultsButton.setSelection(defaultEnabled);

         GridData buttonData = new GridData();
         buttonData.horizontalSpan = columns;
         useDefaultsButton.setLayoutData(buttonData);
 
         createUserEntryArea(locationGroup, !defaultEnabled);
 
         // Ads a listener to uses the location in base of whether the
         // button is set or not
         useDefaultsButton.addSelectionListener(new SelectionAdapter() {
             // The Selection Adapter must adapt the widget
        	 public void widgetSelected(SelectionEvent e) {
                 
        		 // The useDefaults gets the value of check button
        		 boolean useDefaults = useDefaultsButton.getSelection();
 
                 // If default location is activated
        		 if (useDefaults) {
                     // Get the default location path field text
        			 userPath = locationPathField.getText();
        			 // Sets the text based on locale configuration
        			 // e.g. if linux, backslash, if windows slash
                     locationPathField.setText(TextProcessor
                             .process(getDefaultPathDisplayString()));
                 } else {
                     // Gets the location from the user input
                	 locationPathField.setText(TextProcessor.process(userPath));
                 }
        		 
        		 // Sets the user location input texts enabled
                 setUserAreaEnabled(!useDefaults);
             }
         });
      
		 // sets the user area enable in function of the value of defaultEnabled
		 setUserAreaEnabled(!defaultEnabled);
     }

     /**
      * Return whether or not we are currently showing the default location 
      * for the project.
      * 
      * @return boolean True if the use default location check-button is activated.
      */
     public boolean isDefault() {
         return useDefaultsButton.getSelection();
     }
 
     /**
      * Create the project location input components: location path, browse, ...
      * 
      * @param composite The composite
      * @param enabled the initial enabled state of the widgets created 
      */
     private void createUserEntryArea(Composite composite, boolean enabled) {
    	 // Location path label
    	 locationLabel = new Label(composite, SWT.NONE);
    	 locationLabel.setText(SmoolMessages.ProjectSelectionArea_locationLabel);
 
         // project location path text field
         locationPathField = new Text(composite, SWT.BORDER);
         GridData data = new GridData(GridData.FILL_HORIZONTAL);
         data.widthHint = SIZING_TEXT_FIELD_WIDTH;
         data.horizontalSpan = 2;
         locationPathField.setLayoutData(data);
 
         // Adds the browse button to locate the directory
         browseButton = new Button(composite, SWT.PUSH);
         browseButton.setText(SmoolMessages.ProjectSelectionArea_browseLabel);
         
         // Adds a listener for the button (action associated when clicked)
         browseButton.addSelectionListener(new SelectionAdapter() {
             // Throw the folder selection dialog
        	 public void widgetSelected(SelectionEvent event) {
                 handleLocationBrowseButtonPressed();
             }
         });
 
         browseButton.setEnabled(enabled);

         if (initialLocationFieldValue != null) {
        	 locationPathField.setText(initialLocationFieldValue.toOSString());
         }
     }
     
     /**
      * Associates the listener to the location path field
      * @param listener a modify listener
      */
     public void setListener (ModifyListener listener) {
    	 locationPathField.addModifyListener(listener);
     }
 
     /**
      * Return the path we are going to display. 
      * If it is a file URI then remove
      * the file prefix.
      * 
      * @return String with default path
      */
     private String  getDefaultPathDisplayString() {
 
         URI defaultURI = null;
         
         // If the project exists 
         if (existingProject != null) {
             defaultURI = existingProject.getLocationURI();
         }
 
         // Handle local folders specially. 
         // Assume a file if there is no project to query
         if (defaultURI == null || defaultURI.getScheme().equals(FILE_SCHEME)) {
             return Platform.getLocation().append(projectName).toString();
         }
         
         // Returns the default URI appending the project name if necessary
         return defaultURI.toString();
     }
 
     /**
      * Sets if the path area is enabled.
      * 
      * @param enabled A boolean value indicating the enable of the user input area
      */
     private void setUserAreaEnabled(boolean enabled) {
 
    	 // Sets enable fields
    	 locationLabel.setEnabled(enabled);
    	 locationPathField.setEnabled(enabled);
         browseButton.setEnabled(enabled);
     }
 
     /**
      * Return the browse button. Usually referenced in order to set the layout
      * data for a dialog.
      * 
      * @return Button The button component
      */
     public Button getBrowseButton() {
         return browseButton;
     }
 
     /**
      * Open an appropriate directory browser
      */
     private void handleLocationBrowseButtonPressed() {

    	 // Variables
    	 String selectedDirectory = null;
         String dirName = getPathFromLocationField();
    	 
         // Create a directory dialog
         DirectoryDialog dialog = new DirectoryDialog(
        		 locationPathField.getShell());
    	 
         // Sets a message to the directory
         dialog.setMessage(SmoolMessages.ProjectSelectionArea_directoryDialogFolder);
 
         // If the directory name is null
         if (!dirName.equals("")) {
			 File path = new File(dirName);
			 if (path.exists() && path.isDirectory()) {
				 dialog.setFilterPath((new Path(path.getAbsolutePath())).toOSString());
			 }
         }
         
         dialog.setFilterPath(dirName);
 
         selectedDirectory = dialog.open();

         if (selectedDirectory != null) {
             updateLocationField(selectedDirectory);
         }
     }
 
     /**
      * Update the location path field based on the selected path.
      * 
      * @param selectedPath The selected path
      */
     private void updateLocationField(String selectedPath) {
         locationPathField.setText(TextProcessor.process(selectedPath));
     }
 
     /**
      * Return the path on the location field.
      * 
      * @return String
      */
     private String getPathFromLocationField() {
    	 // A URI containing the path selection
         URI fieldURI;
         try {
             fieldURI = new URI (locationPathField.getText());
         } catch (URISyntaxException  e) {
             return locationPathField.getText();
         }
         return fieldURI.getPath();
     }
 
     /**
      * Check if the entry in the widget location is valid. 
      * If it is valid return null. 
      * Otherwise return a string that indicates the problem.
      * 
      * @return String containing the error if the location cannot be reached or null if the location is correct.
      */
     public String checkValidLocation() {
 
    	 // If the location is checked to default
         if (isDefault()) {
             return null;
         }
 
         // Checks if the text field is empty
         String locationFieldContents = locationPathField.getText();
         if (locationFieldContents.length() == 0) {
             return (SmoolMessages.ProjectSelectionArea_validationLocationNotSpecified);
         }
 
         // The selected path
         URI newPath = getProjectLocationURI();
         if (newPath == null) {
             return (SmoolMessages.ProjectSelectionArea_errorInvalidProjectLocation);
         }
 
         // Create a dummy project for the purpose of validation if necessary
         IProject project = existingProject;
         
         // if it is not selected the existing project
         if (project == null) {
             // Get the last segment (project name)
        	 String name = new Path(locationFieldContents).lastSegment();
         
	    	 // If the path is correct syntactically, gets the project from workspace 
	    	 if (name != null && Path.EMPTY.isValidSegment(name)){
	             project = ResourcesPlugin.getWorkspace().getRoot().getProject(name);
	         } else {
	             return (SmoolMessages.ProjectSelectionArea_errorInvalidProjectLocation);
	     	 }
	
	         // Obtain the location status
	    	 IStatus locationStatus = project.getWorkspace().validateProjectLocationURI(project, newPath);
	 
	    	 // If the location status is not ok, then return the location status
	    	 if (!locationStatus.isOK()) {
	             return locationStatus.getMessage();
	         }
	         
	    	 // If existing project is not null
	    	 if (existingProject != null) {
	             URI  projectPath = existingProject.getLocationURI();
	             if (projectPath != null && projectPath.equals(newPath)) {
	                 return (SmoolMessages.ProjectSelectionArea_errorPathAlreadySelected);
	             }
	         }
         }
 
         return null;
     }
 
     /**
      * Get the URI for the location field if possible.
      * 
      * @return URI or <code>null</code> if it is not valid.
      */
     public URI getProjectLocationURI() {
 
    	 URI pathURI = null;
    	 
    	 try {
    		 //pathURI = new URI("file", locationPathField.getText(), null);
    		 File file = new File(locationPathField.getText());
    		 if (!file.isDirectory()) {
    			 pathURI = null;
    		 } else {
    			 pathURI = file.toURI();
    		 }
    	 } catch (Exception ex) {
    		 System.out.println(ex.toString());
    	 }
    	 
    	 return pathURI;
     }
 
     /**
      * Set the text to the default or clear it if not using the defaults.
      * 
      * @param newName the name of the project to use. If <code>null</code> use the
      * existing project name.
      */
     public void updateProjectName(String newName) {
         projectName = newName;
         if (isDefault()) {
             locationPathField.setText(TextProcessor
                     .process(getDefaultPathDisplayString()));
         }
     }
 
    /**
     * Return the location for the project. If we are using defaults then return
     * the workspace root so that core creates it with default values.
     * 
     * @return String with the project location
     */
     public String getProjectLocation() {
	     if (isDefault()) {
	         return Platform.getLocation().toString();
	     }
	     return locationPathField.getText();
	 }

	/**
	 * Returns the value of the default button
	 * @return <code>true</code> if the default button is checked
	 */
     public boolean isDefaultChecked() {
		return useDefaultsButton.getSelection();
	}

	/**
	 * Sets the location text
	 * @param strPath the path
	 */
	public void setLocationText(String strPath) {
		locationPathField.setText(strPath);
	}
}
