/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.wizard.project.data;

import java.util.ArrayList;

/**
 * This class contains the information about the available platform.
 * This information will be used by the ADK Wizard to be displayed for the user
 * 
 * @author Fran Ruiz, Fran.Ruiz@tecnalia.com, Tecnalia
 *
 */
public class Platform extends TreeNodeModel {

	/** The description of the connector */
	private String description;

	/** The Connectors associated to this platform */
	private ArrayList<Connector> connectors;
	
	/** The libraries references */
	private ArrayList<String> platformLibraries;
	
	/** The logger */
	//private static Logger logger = Logger.getLogger(Platform.class);

	/**
	 * Public constructor
	 */
	public Platform() {
		super();
		description = null;
		connectors = new ArrayList<Connector>();
		platformLibraries = new ArrayList<String>();
		System.out.println("Created a new platform node");
	}
	
	/**
	 * Gets the description to the current platform
	 * @return a string with description of the content of the platform 
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Sets the description associated to the current platform
	 * @param description a string indicating the description of the platform
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the list of connectors
	 * @return the arraylist of connectors 
	 */
	public ArrayList<Connector> listConnectors() {
		return connectors;
	}
	
	/**
	 * Adds a connector to the list of connector
	 * @param connector a Connector object
	 */
	public void addConnector(Connector connector) {
		if (!this.connectors.contains(connector)) {
			this.connectors.add(connector);
		}
	}	
	
	/**
	 * Gets the list of library references
	 * @return the arraylist of library references 
	 */
	public ArrayList<String> listLibraries() {
		return platformLibraries;
	}
	
	/**
	 * Adds a library reference
	 * @param library the library reference
	 */
	public void addLibrary(String library) {
		if (!this.platformLibraries.contains(library)) {
			this.platformLibraries.add(library);
		}
	}		
}
