/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.wizard.project.pages;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;
import org.smool.sdk.common.nls.SmoolMessages;
import org.smool.sdk.common.properties.PropertyLoader;
import org.smool.sdk.ui.wizard.project.ProjectWizard;
import org.smool.sdk.ui.wizard.project.data.Model;
import org.smool.sdk.ui.wizard.project.pages.frames.LocationSelectionArea;
import org.smool.sdk.ui.wizard.project.wizard.util.ModelHandler;


/**
 * This class implements the UI page of the ADK Wizard where
 * we can determine the application domain of the Knowledge Processor
 * 
 * @author Fran Ruiz, Fran.Ruiz@tecnalia.com, Tecnalia
 * @see org.eclipse.jface.wizard.WizardPage
 */
public class NewKPIProjectPage extends WizardPage {

	/** Widgets: The project name field */
	private Text projectNameField = null;
	
	/** The Location Selection Area */
	private LocationSelectionArea locationArea;
	
	/** The logger */
	//private static Logger logger = Logger.getLogger(NewKPIProjectPage.class);
	
	/** The Initial project Name */
	private String initialProjectName;
	
	/** The Text Field width */
	private static final int SIZING_TEXT_FIELD_WIDTH = 250;

	private static final String HELP_BUNDLE = "bundle.help";
	
	private static final String HELP_CONTENT_KEY = "newproject";
	
    /** Defines the model key of smool.properties */
    private static final String LOCATOR = "model.locator";
    
    private HashMap<Button,Model> configurationMap;
    
	private ArrayList<String> modelLibraries;    

	/** Listener corresponding to the project name modification */
	private Listener projectNameModifyListener = new Listener() {
		public void handleEvent(Event e) {
			setLocationForSelection();
			setPageComplete(validatePage());
		}
	};
	
	/** Listener corresponding to the project name */
	private ModifyListener locationModifiyListener = new ModifyListener() {
        public void modifyText(ModifyEvent e) {
			setPageComplete(validatePage());
        }		
	};	
	
	/**
	 * Constructor for DomainPage.
	 */
	public NewKPIProjectPage() {
		super(SmoolMessages.NewSmartAppProjectPage_windowTitle);
		setTitle(SmoolMessages.NewSmartAppProjectPage_pageTitle);
		setDescription(SmoolMessages.NewSmartAppProjectPage_pageDescription);
	}	
	
	/**
	 * Creates the wizard page components
	 * @param parent the parent composite
	 */
	@Override
	public void createControl(Composite parent) {
		// Creates a new composite
		Composite container = new Composite(parent, SWT.NULL);
		container.setFont(parent.getFont());
		
		// Initializes the computation of horizontal and vertical 
		// dialog units based on the size of current font. 
		initializeDialogUnits(parent);
		
		String helpBundle = PropertyLoader.getInstance().getProperty(HELP_BUNDLE);
		StringBuffer helpContents = new StringBuffer();
		helpContents.append(helpBundle).append(".").append(HELP_CONTENT_KEY);
		
		// Help system
		PlatformUI.getWorkbench().getHelpSystem().setHelp(container, helpContents.toString());

		// Creates a new grid layout
		container.setLayout(new GridLayout());
		container.setLayoutData(new GridData(GridData.FILL_BOTH));
		 
		// Creates the project and package name group
		createProjectNameGroup(container);

		// Creates the project location group
		createProjectLocationGroup(container);

		// Creates the properties group
		createProjectConfigurationGroup(container);
		
		// Validates the page and sets the completeness of the page
		setPageComplete(validatePage());

        // Show description on opening
        setErrorMessage(null);
        setMessage(null);
        setControl(container);
	}

	/**
	 * Creates the Smool project name and package name specification controls.
	 * @param parent the parent composite
	 */
	private final void createProjectNameGroup(Composite parent) {
		// project specification group
		Composite projectNameGroup = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		projectNameGroup.setLayout(layout);
		projectNameGroup.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		// The project name label
		Label projectNameLabel = new Label(projectNameGroup, SWT.NONE);
		projectNameLabel.setText(SmoolMessages.NewSmartAppProjectPage_projectLabel);
		projectNameLabel.setFont(parent.getFont());
	
		// KP project name text field
		projectNameField = new Text(projectNameGroup, SWT.BORDER);
		GridData data = new GridData(GridData.FILL_HORIZONTAL);
		data.widthHint = SIZING_TEXT_FIELD_WIDTH;
		projectNameField.setLayoutData(data);
		projectNameField.setFont(parent.getFont());

		// Set the initial value first before listener
		// to avoid handling an event during the creation.
		if (initialProjectName != null) {
	        projectNameField.setText(initialProjectName);
	    }
	    
		projectNameField.addListener(SWT.Modify, projectNameModifyListener);
		
	}

	/**
	 * Creates the Smool project location group controls.
	 * Due to the project location has been included in the frames package,
	 * it will create only the corresponding areas
	 * 
	 * @param parent the parent composite
	 */
	private final void createProjectLocationGroup(Composite parent) {
		locationArea = new LocationSelectionArea(parent);

		locationArea.setListener(locationModifiyListener);
		
        // Scale the button based on the rest of the dialog
		setButtonLayoutData(locationArea.getBrowseButton());
	}	
	
	/**
	 * Creates the Smool project configuration options controls.
	 * @param parent the parent composite
	 */
	private final void createProjectConfigurationGroup(Composite parent) {
		configurationMap = new HashMap<Button,Model>();
		modelLibraries = new ArrayList<String>();
		
		// project specification group
		Group projectConfigurationGroup = new Group(parent, SWT.NONE);
		projectConfigurationGroup.setText(SmoolMessages.NewSmartAppProjectPage_configurationGroupTitle);    	 

		GridLayout layout = new GridLayout();
		projectConfigurationGroup.setLayout(layout);
		projectConfigurationGroup.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		
		// Gets the models from either remote or local model.xml file
		ModelHandler handler = new ModelHandler(LOCATOR);
		
		ArrayList<Model> modelList = new ArrayList<Model>(handler.getModel());
		
		for (Model model : modelList) {
			Button modelCheck = new Button(projectConfigurationGroup, SWT.CHECK);
			modelCheck.setSelection(model.isDefaultChecked());
			
			if (model.isDefaultChecked()) {
				for (String modelLibrary : model.listLibraries()) {
					modelLibraries.add(modelLibrary);
				}				
			}
			
			if (model.getDescriptor() != null && !model.getDescriptor().equals("") ) {
				StringBuilder modelTextKey = new StringBuilder();
				modelTextKey.append("NewSmartAppProjectPage_configuration_");
				modelTextKey.append(model.getDescriptor());
				try {
					Field messageField = SmoolMessages.class.getDeclaredField(modelTextKey.toString());
					messageField.setAccessible(true);

					modelCheck.setText((String)messageField.get(SmoolMessages.class.newInstance()));
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (SecurityException e) {
					e.printStackTrace();
				} catch (NoSuchFieldException e) {
					e.printStackTrace();
				}
			}
			
			configurationMap.put(modelCheck, model);
			
			modelCheck.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent event) {
					Button button = (Button) event.widget;
					if (button.getSelection()) {
						for (String modelLibrary : configurationMap.get(button).listLibraries()) {
							System.out.println("Adding " + modelLibrary);
							modelLibraries.add(modelLibrary);
						}
					} else {
						for (String modelLibrary : configurationMap.get(button).listLibraries()) {
							System.out.println("Removing " + modelLibrary);
							modelLibraries.remove(modelLibrary);
						}						
					}
			    }
			});
			
		}		
	}	
	
   /**
    * Returns whether this page's controls currently all contain valid 
    * values.
    *
    * @return <code>true</code> if all controls are valid, and
    * <code>false</code> if at least one is invalid
    */
	protected boolean validatePage() {
		// sets the error messages to null
    	setErrorMessage(null);
    	 
    	// validates the project reference
    	if (!validateProjectReference()) {
    		return false;
    	}
    	 
        // if the validation is correct, then return the default message
        setMessage(SmoolMessages.NewSmartAppProjectPage_validationDefaultMessage);
    	 
        return true;
    }
     
   /**
    * Validates the project reference
    * @return <code>true</code> if the user has specified some value in the project name
    */
    private boolean validateProjectReference() {
    	// Obtains the current workspace
    	IWorkspace workspace = ResourcesPlugin.getWorkspace();
 
        String projectFieldContents = getProjectNameFieldValue();
        if (projectFieldContents.equals("")) { 
        	setErrorMessage(null);
            setMessage(SmoolMessages.NewSmartAppProjectPage_validationProjectNotSpecified);
            return false;
        }
 
        IStatus nameStatus = workspace.validateName(projectFieldContents,
        		IResource.PROJECT);
        if (!nameStatus.isOK()) {
        	setErrorMessage(nameStatus.getMessage());
            return false;
        }
 
        IProject handle = getProjectHandle();
        if (handle.exists()) {
        	setErrorMessage(SmoolMessages.NewSmartAppProjectPage_errorProjectAlreadyExists);
            return false;
        }
         
        // if not using the default value validate the location.
        if (!locationArea.isDefault()) {
            String validLocationMessage = locationArea.checkValidLocation();
            if (validLocationMessage != null) { //there is no destination location given
            	System.out.println("This is the message to show:" + validLocationMessage);
            	if (validLocationMessage.equals(SmoolMessages.ProjectSelectionArea_validationLocationNotSpecified)) {
            		setErrorMessage(null);
            		setMessage(validLocationMessage);
            	} else {
            		setErrorMessage(validLocationMessage);
            	}
                return false;
            }
         }
 
         if (!nameStatus.isOK()) {
             setErrorMessage(nameStatus.getMessage());
             return false;
         }
 
         return true;
     }

     /**
      * Returns the value of the project name field
      * with leading and trailing spaces removed.
      * 
      * @return the project name in the field
      */
     private String getProjectNameFieldValue() {
         if (projectNameField == null) {
             return "";
         } else {
        	 return projectNameField.getText().trim();
         }
     }
     
     /**
      * Gets the location path
      * @return the IPath corresponding to the location path
      */
     public IPath getLocationPath() {
    	 return new Path(locationArea.getProjectLocation());
     }

     /**
      * Gets the libraries to include
      * @return the list of libraries to include
      */
     public Collection<String> listModelLibraries() {
    	 return this.modelLibraries;
     }

    /**
     * Creates a project resource handle for the current project name field
     * value. The project handle is created relative to the workspace root.
     * <p>
     * This method does not create the project resource; this is the
     * responsibility of <code>IProject::create</code> invoked by the new
     * project resource wizard.
     * </p>
     * 
     * @return the new project resource handle
     */
     public IProject getProjectHandle() {
         return ResourcesPlugin.getWorkspace().getRoot().getProject(
                 getProjectName());
     }

     /**
      * Returns the current project name as entered by the user, or its anticipated
      * initial value.
      *
      * @return the project name, its anticipated initial value, or <code>null</code>
      * if no project name is known
      */
     public String getProjectName() {
         if (projectNameField == null) {
             return initialProjectName;
         }
 
         return getProjectNameFieldValue();
     }
     
   /**
    * Set the location to the default location if we are set to useDefaults.
    */
    private void setLocationForSelection() {
    	if (locationArea.isDefaultChecked()) {
    		IPath defaultPath = Platform.getLocation().append(getProjectNameFieldValue());
    		locationArea.setLocationText(defaultPath.toOSString());
    	}
    }

	/**
	 * Implements the getNextPage method to obtain the following page in the
	 * wizard and update the ontology references
	 */
    @Override
	public IWizardPage getNextPage(){

    	// Saves the selections made in this first page to the model
    	saveWizardPageSelection();
    	
    	return (super.getNextPage());
	}

    /**
     * Saves this page selections
     */
    private void saveWizardPageSelection() {
    	// Sets the project name
    	if (projectNameField.getText() != null) {
    		((ProjectWizard)getWizard()).getModel().setProjectName(projectNameField.getText());
    	}
    	
    	// Sets the location
    	if (locationArea.getProjectLocation() != null) {
    		((ProjectWizard)getWizard()).getModel().setPath(getLocationPath(), locationArea.isDefault());
    	}
    	
    }
}
