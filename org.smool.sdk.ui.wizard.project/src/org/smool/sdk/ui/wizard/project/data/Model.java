/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.wizard.project.data;

import java.util.ArrayList;

/**
 * This class contains the information about available model.
 * This information will be used by the ADK Wizard to be displayed for the user
 * 
 * @author Fran Ruiz, Fran.Ruiz@tecnalia.com, Tecnalia
 *
 */
public class Model {

	/** The name of the model */
	private String name;

	/** The description of the connector */
	private String descriptor;
	
	/** If by default is checked */
	private boolean defaultChecked;
	
	/** The libraries references */
	private ArrayList<String> modelLibraries;
	
	/**
	 * Public constructor
	 */
	public Model() {
		this.setName(null);
		this.descriptor = null;
		this.setDefaultChecked(false);
		modelLibraries = new ArrayList<String>();
	}
	

	/**
	 * Sets the name for the model
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the name of the model
	 * @return the name of the model
	 */
	public String getName() {
		return name;
	}		
	
	/**
	 * Gets the descriptor
	 * @return a string with the descriptor 
	 */
	public String getDescriptor() {
		return descriptor;
	}
	
	/**
	 * Sets the descriptor
	 * @param descriptor a string indicating the descriptor
	 */
	public void setDescriptor(String descriptor) {
		this.descriptor = descriptor;
	}

	/**
	 * Gets the list of library references
	 * @return the arraylist of library references 
	 */
	public ArrayList<String> listLibraries() {
		return modelLibraries;
	}
	
	/**
	 * Adds a library reference
	 * @param library the library reference
	 */
	public void addLibrary(String library) {
		if (!this.modelLibraries.contains(library)) {
			this.modelLibraries.add(library);
		}
	}

	/**
	 * Sets the default checked 
	 * @param defaultChecked the defaultChecked to set
	 */
	public void setDefaultChecked(boolean defaultChecked) {
		this.defaultChecked = defaultChecked;
	}

	/**
	 * Gets the default checked value
	 * @return the defaultChecked
	 */
	public boolean isDefaultChecked() {
		return defaultChecked;
	}
	
}
