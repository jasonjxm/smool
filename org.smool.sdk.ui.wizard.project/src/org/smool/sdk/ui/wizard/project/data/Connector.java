/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.wizard.project.data;

import java.util.ArrayList;


/**
 * This class contains the information about the available connection.
 * This information will be used by the ADK Wizard to be displayed for the user
 * 
 * @author Fran Ruiz, Fran.Ruiz@tecnalia.com, Tecnalia
 *
 */
public class Connector extends TreeNodeModel {

	/** The description */
	private String description;

	/** The logger */
	//private static Logger logger = Logger.getLogger(Connector.class);

	/** The connector libraries references */
	private ArrayList<String> connectorLibraries;
	
	/**
	 * Constructor
	 */
	public Connector() {
		super();
		description = null;
		connectorLibraries = new ArrayList<String>();
		System.out.println("Created a new Connection node");
	}
	
	/**
	 * Gets the description to the current connector
	 * @return a string with description of the content of the connector 
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Sets the description associated to the current connector
	 * @param description a string indicating the description of the node
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the list of library references
	 * @return the arraylist of library references 
	 */
	public ArrayList<String> listLibraries() {
		return connectorLibraries;
	}
	
	/**
	 * Adds a library reference
	 * @param library the library reference
	 */
	public void addLibrary(String library) {
		if (!this.connectorLibraries.contains(library)) {
			this.connectorLibraries.add(library);
		}
	}		
}
