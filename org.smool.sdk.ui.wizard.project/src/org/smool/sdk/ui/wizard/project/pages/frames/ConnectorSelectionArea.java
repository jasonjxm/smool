/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.wizard.project.pages.frames;

import java.util.Collection;
import java.util.HashMap;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.smool.sdk.common.nls.SmoolMessages;
import org.smool.sdk.ui.wizard.project.data.Connector;


/**
 * A class that gets from an URI the available connectors and shows it
 * in a tree viewer.
 * 
 * @author Fran Ruiz, Fran.Ruiz@tecnalia.com, Tecnalia
 *
 */
public class ConnectorSelectionArea {

	/** Widget definition */
	private Label connectionLabel;
	
	/** The map for mapping buttons and the associated Platform object */
	private HashMap<Button,Connector> connectorButtons = new HashMap<Button,Connector>();

    /** The Logger */
	//private static Logger logger = Logger.getLogger(ConnectorSelectionArea.class);
	
	/** The composite containing the connectors */
	private Composite connectorGroupContainer;

    /**
     * Create a new instance of a SmartEnvironmentSelectionArea.
     * 
     * @param composite The composite
     */
    public ConnectorSelectionArea(Composite composite) {
    	 
         // If it is a new project always start enabled
         createContents(composite);
     }
 
     /**
      * Create the area containing the check box tree viewer containing
      * the smart environment.
      * 
      * @param composite The composite
      */
     private void createContents(Composite composite) {
     	// Populates the model with the xml data obtained from 
     	// Smool properties 
     	System.out.println("Creating Connection Selection content");
  		Composite connectorContainer = new Composite(composite, SWT.NONE);
  		connectorContainer.setLayout(new GridLayout());
  		connectorContainer.setLayoutData(new GridData(GridData.FILL_BOTH));

     	connectionLabel = new Label(connectorContainer, SWT.NONE);
     	connectionLabel.setText(SmoolMessages.ConnectorSelectionArea_label);
     	connectionLabel.setFont(connectorContainer.getFont());
     	connectionLabel.setEnabled(false);

  		connectorGroupContainer = new Composite(connectorContainer, SWT.BORDER);
  		connectorGroupContainer.setLayout(new GridLayout());
  		connectorGroupContainer.setLayoutData(new GridData(GridData.FILL_BOTH));
  		connectorGroupContainer.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_GRAY));
  		connectorGroupContainer.setEnabled(false);
     }
     
     /**
      * Updating the Connectors
      * @param connectors
      */
     public void setAvailableConnectors(Collection<Connector> connectors) {
    	 System.out.println("Setting the available connectors");

    	 for (Button button : connectorButtons.keySet()) {
    		 button.dispose();
    	 }
    	 
    	 connectorButtons.clear();
    	 
    	 if (connectors.size() > 0) {
    		 connectionLabel.setEnabled(true);
    		 connectionLabel.setRedraw(true);
    	     
    		 //connectorGroupContainer.setRedraw(false);
    		 connectorGroupContainer.setEnabled(true);
   	  		 connectorGroupContainer.setLayout(new GridLayout());
    	  	 connectorGroupContainer.setLayoutData(new GridData(GridData.FILL_BOTH));
    	  	 connectorGroupContainer.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
    			 
    		 for (Connector connector : connectors) {
    			 System.out.println("Adding the "+connector.getName()+" connector...");
    			 Button connectorButton = new Button(connectorGroupContainer, SWT.CHECK);
    			 connectorButton.setText(connector.getName());
    			 connectorButton.setImage(connector.getImage()); 			
    			 connectorButton.setBackground(connectorGroupContainer.getBackground());
    			 connectorButtons.put(connectorButton, connector);
    		 }

    	  	 connectorGroupContainer.layout();
    	 } else {
    		 // disables the label
    		 connectionLabel.setEnabled(false);
    	     // disables de container
    		 connectorGroupContainer.setEnabled(false);
    	 }
     }
     
     /**
      * Assigns the listener to all created buttons
      * @param listener a listener
      */
     public void setListener(Listener listener) {
  		for (Button connectorButton : connectorButtons.keySet()) {
  			connectorButton.addListener(SWT.Selection, listener);
  		}
     }
     
     /**
      * Gets the connector widgets
      * @return the set of buttons added
      */
     public Collection<Button> getPlatformWidgets() {
     	return connectorButtons.keySet();
     }
     
     /**
      * Gets the Connector corresponding to the selected button
      * @param button the selected button
      * @return the Connector element
      */
     public Connector getConnector(Button button) {
     	return connectorButtons.get(button);
     }     
}
