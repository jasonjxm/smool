/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.wizard.project;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;
import org.smool.sdk.common.properties.PropertyLoader;


/**
 * The activator class controls the starting and stopping of the
 * ADK Project Wizard. 
 * @author Fran Ruiz, ESI
 */
public class ProjectWizardActivator extends AbstractUIPlugin {

	private static final String PLUGIN_REFERENCE = "bundle.wizard.project";
	
	// The plug-in ID
	public static final String PLUGIN_ID = PropertyLoader.getInstance().getProperty(PLUGIN_REFERENCE);

	// The shared instance
	private static ProjectWizardActivator plugin;
	
	// The bundle context
	@SuppressWarnings("unused")
	private BundleContext bc;
	
	// Define a static logger variable so that it references the
    // Logger instance named "Model".
	//static Logger logger = null;
	
	
	/**
	 * Creates the plug-in runtime object.
	 * 
	 * Note that instances of plug-in runtime classes are automatically 
	 * created by the platform in the course of plug-in activation. 
	 */
	public ProjectWizardActivator() {
//		try {
//			BasicConfigurator.configure();
//			logger = Logger.getLogger(ProjectWizardActivator.class);
//		} catch (Exception e) {
//			System.out.println ("Excepci�n al inicializar el log: " + e.toString());
//		}
	}

	/**
	 * The AbstractUIPlugin implementation of this Plugin method 
	 * refreshes the plug-in actions. Activator extend this 
	 * method, but must send super first. Starts up this plug-in.
	 * 
	 * This method should be overridden in subclasses that need to 
	 * do something when this plug-in is started. Implementors should 
	 * call the inherited method at the first possible point 
	 * to ensure that any system requirements can be met.
	 * 
	 *  If this method throws an exception, it is taken as an 
	 *  indication that plug-in initialization has failed; 
	 *  as a result, the plug-in will not be activated; 
	 *  moreover, the plug-in will be marked as disabled and 
	 *  ineligible for activation for the duration.
	 *  
	 *  Plug-in startup code should be robust. 
	 *  In the event of a startup failure, the plug-in's shutdown 
	 *  method will be invoked automatically, in an attempt to 
	 *  close open files, etc.
	 *  
	 *  Note 1: This method is automatically invoked by the 
	 *  platform the first time any code in the plug-in is executed.
	 *  
	 *  Note 2: This method is intended to perform simple 
	 *  initialization of the plug-in environment. The platform may 
	 *  terminate initializers that do not complete in a timely 
	 *  fashion.
	 *  
	 *  Note 3: The class loader typically has monitors acquired 
	 *  during invocation of this method. It is strongly recommended 
	 *  that this method avoid synchronized blocks or other thread 
	 *  locking mechanisms, as this would lead to deadlock 
	 *  vulnerability.
	 *  
	 *  Note 4: The supplied bundle context represents the plug-in 
	 *  to the OSGi framework. For security reasons, it is strongly 
	 *  recommended that this object should not be divulged.
	 *  
	 *  Note 5: This method and the Plugin.stop(BundleContext) may be 
	 *  called from separate threads, but the OSGi framework ensures 
	 * that both methods will not be called simultaneously. 
	 *  
	 * <b>Clients must never explicitly call this method.</b>
	 * 
	 * @param context the bundle context for this plug-in
	 * @throws Exception if this plug-in did not start up properly
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 * 
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		try {
			this.bc = context;
		} catch (Exception ex) {
			System.out.println("Cannot start the ProjectPluginActivator. Cause: " + ex.getMessage());
			ex.printStackTrace();
		}
	}

	/**
	 * The AbstractUIPlugin implementation of this Plugin method 
	 * saves this plug-in's preference and dialog stores and 
	 * shuts down its image registry (if they are in use). 
	 * Subclasses may extend this method, but must send super last. 
	 * A try-finally statement should be used where necessary to 
	 * ensure that super.shutdown() is always done. Stops this 
	 * plug-in.
	 * 
	 * This method should be re-implemented in subclasses that 
	 * need to do something when the plug-in is shut down. 
	 * Implementors should call the inherited method as late as 
	 * possible to ensure that any system requirements can be met.
	 * 
	 * Plug-in shutdown code should be robust. In particular, 
	 * this method should always make an effort to shut down 
	 * the plug-in. Furthermore, the code should not assume 
	 * that the plug-in was started successfully, as this method 
	 * will be invoked in the event of a failure during startup.
	 *  
	 * Note 1: If a plug-in has been automatically started, 
	 * this method will be automatically invoked by the platform 
	 * when the platform is shut down. 
	 *  
	 * Note 2: This method is intended to perform simple 
	 * termination of the plug-in environment. The platform may 
	 * terminate invocations that do not complete in a timely 
	 * fashion.
	 * 
	 * Note 3: The supplied bundle context represents the plug-in 
	 * to the OSGi framework. For security reasons, it is strongly 
	 * recommended that this object should not be divulged. 
	 * 
	 * Note 4: This method and the Plugin.start(BundleContext) 
	 * may be called from separate threads, but the OSGi framework 
	 * ensures that both methods will not be called simultaneously.
	 *  
	 * <b>Clients must never explicitly call this method.</b>
	 *  
	 * @param context the bundle context for this plug-in
	 * @throws Exception if this method fails to shut down this plug-in
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		super.stop(context);
		plugin = null;
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance of this plugin.
	 */
	public static ProjectWizardActivator getDefault() {
		return plugin;
	}

	/**
	 * Gets the image descriptor for an image file located within 
	 * the specified plug-in.
	 * 
	 *  This is a convenience method that simply locates the image 
	 *  file in within the plug-in (no image registries are involved). 
	 *  The path is relative to the root of the plug-in, 
	 *  and takes into account files coming from plug-in fragments. 
	 *  The path may include $arg$ elements. However, the path 
	 *  must not have a leading "." or path separator. 
	 *  Clients should use a path like "icons/mysample.gif" 
	 *  rather than "./icons/mysample.gif" or "/icons/mysample.gif". 
	 *  
	 *  @param path the relative path of the image file, relative to the root of the plug-in; the path must be legal.
	 *  @return an image descriptor, or null if no image could be found
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}
	
    /**
     * Logs a message to the default Eclipse log.
     * 
     * @param severity One of IStatus' severity codes: OK, ERROR, INFO, WARNING or CANCEL.
     * @param format The format string, like for String.format().
     * @param args The arguments for the format string, like for String.format().
     */
    public static void log(int severity, String format, Object ... args) {
        String message = String.format(format, args);
        Status status = new Status(severity, PLUGIN_ID, message);
        getDefault().getLog().log(status);
    }

    /**
     * Logs an exception to the default Eclipse log.
     * <p/>
     * The status severity is always set to ERROR.
     * 
     * @param exception The exception to log. Its call trace will be recorded.
     * @param format The format string, like for String.format().
     * @param args The arguments for the format string, like for String.format().
     */
    public static void log(Throwable exception, String format, Object ... args) {
        String message = String.format(format, args);
        Status status = new Status(IStatus.ERROR, PLUGIN_ID, message, exception);
        getDefault().getLog().log(status);
    }	
}
