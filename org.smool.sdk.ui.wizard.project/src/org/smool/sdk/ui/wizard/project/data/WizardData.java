/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.wizard.project.data;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.core.runtime.IPath;

/**
 * Constains the intermediate information to create a new Knowledge Processor
 * project using the Wizard
 * 
 * @author Fran Ruiz, Fran.Ruiz@tecnalia.com, Tecnalia
 */
public class WizardData {

	/** The selected platform **/
	private Platform selectedPlatform;
	
	/** The list of selected connectors */
	private ArrayList<Connector> selectedConnectors;
	
	private ArrayList<String> selectedModels;
	
	/** The logger */
	//private static Logger logger = Logger.getLogger(WizardData.class);

	/** The project name */
	private String projectName = null;
	
	/** The package name */
	private String packageName = null;
	
	/** The Project Path in where to located the project */
	private IPath projectPath = null;

	/** Determines if the project location is the default one */
	private boolean isDefaultLocation = false;
	
	/**
	 * Constructor
	 */
	public WizardData(){
		selectedPlatform = null;
		selectedConnectors = new ArrayList<Connector>();
		selectedModels = new ArrayList<String>();
	}
	
	/**
	 * Adds a connector to the current connector list
	 * @param connector a Connector object 
	 */
	public void addConnector(Connector connector) {
		System.out.println("Added connection " + connector.getName());
		
		if (!selectedConnectors.contains(connector)) {
			selectedConnectors.add(connector);
		}
	}
	
	/**
	 * Removes a connector to the current connector list
	 * @param connector a Connector object
	 */
	public void removeConnector(Connector connector) {
		System.out.println("Removed connector " + connector.getName());

		if (selectedConnectors.contains(connector)) {
			selectedConnectors.remove(connector);
		}
	}

	/**
	 * Clears / Removes the connector list
	 */
	private void disposeConnectors() {
		System.out.println("Disposing connectors");
		
		selectedConnectors.clear();
	}
	
	/**
	 * Adds a connector to the current connector list
	 * @param connector a Connector object 
	 */
	public void addModel(String model) {
		System.out.println("Added " + model);
		
		if (!selectedModels.contains(model)) {
			selectedModels.add(model);
		}
	}
	
	/**
	 * Removes a connector to the current connector list
	 * @param connector a Connector object
	 */
	public void removeModel(String model) {
		System.out.println("Removed " + model);

		if (selectedModels.contains(model)) {
			selectedModels.remove(model);
		}
	}
	
	
	/**
	 * Gets the model libraries references
	 * @return a set of string representing internal model libraries
	 */
	public Collection<String> listModelLibraries() {
		return selectedModels;
	}
	
	/**
	 * Sets the selected platform
	 * @param platform a Platform object
	 */
	public void setPlatform(Platform platform) {
		System.out.println("Adding platform " + platform.getName());
		this.selectedPlatform = platform;
		this.disposeConnectors();
	}
	
	/**
	 * Removes a platform to the current platform list
	 * @return the selected platform
	 */
	public Platform getPlatform() {
		return selectedPlatform;
	}
	
	/**
	 * Gets the connector libraries references
	 * @return a set of string representing internal connector libraries
	 */
	public Collection<String> listConnectorLibraries() {
		ArrayList<String> librariesReferences = new ArrayList<String>();
		
		for (Connector connector : selectedConnectors) {
			if (connector.listLibraries().size() > 0) {
				librariesReferences.addAll(connector.listLibraries());
			}
		}
		return librariesReferences;
	}

	/**
	 * Gets the connector libraries references
	 * @return a set of string representing internal connector libraries
	 */
	public Collection<String> listPlatformLibraries() {
		return selectedPlatform.listLibraries();
	}
	
	/**
	 * Determines whether the user has selected a platform
	 */
	public boolean hasSelectedPlatform() {
		return selectedPlatform != null;
	}	
	
	/**
	 * Determines whether the user has selected any connector
	 */
	public boolean hasSelectedConnectors() {
		return selectedConnectors.size() > 0;
	}	
	
	/**
	 * @param projectName the projectName to set
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	/**
	 * @return the projectName
	 */
	public String getProjectName() {
		return projectName;
	}

	/**
	 * @param packageName the packageName to set
	 */
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	/**
	 * @return the packageName
	 */
	public String getPackageName() {
		return packageName;
	}

	/**
	 * Get all libraries references
	 * @return a collection of libraries references
	 */
	public Collection<String> listAllLibraries() {

		ArrayList<String> librariesReferences = new ArrayList<String>();
		
		librariesReferences.addAll(listConnectorLibraries());

		librariesReferences.addAll(listPlatformLibraries());
		
		librariesReferences.addAll(listModelLibraries());
		return librariesReferences;
	}
	
	/**
	 * Sets the source location for the project
	 * @param iPath the path
	 * @param isDefault determines if the path is the default path
	 */
	public void setPath(IPath iPath, boolean isDefault) {
		this.projectPath = iPath;
		this.isDefaultLocation = isDefault;
	}

	/**
	 * Gets the base location for the project
	 * @return the base location for the project
	 */
	public IPath getPath() {
		return projectPath;
	}

	/**
	 * Determines if the default location was selected in the wizard
	 */
	public boolean isDefaultLocation() {
		return isDefaultLocation;
	}	
}
