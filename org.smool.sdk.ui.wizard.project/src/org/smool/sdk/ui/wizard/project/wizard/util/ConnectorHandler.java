/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.wizard.project.wizard.util;

import static org.w3c.dom.Node.ELEMENT_NODE;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.eclipse.swt.graphics.Image;
import org.smool.sdk.common.sax.ResourceUtil;
import org.smool.sdk.ui.wizard.project.data.Connector;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * This class contains the information about the available connectors.
 * This information will be used by the ADK Wizard to be displayed for the user
 * 
 * @author Fran Ruiz, Fran.Ruiz@tecnalia.com, Tecnalia
 *
 */
public class ConnectorHandler {

	private String[] allowedNodes = {"connection"}; 
	
	private static String NAME_ELEMENT = "name";
	private static String DESC_ELEMENT = "description";
	private static String IMAGE_ELEMENT = "icon";
	private static String RESOURCES_ELEMENT = "resources";
	private static String LIBRARY_ELEMENT = "resource";
	
	/** The logger */
	//private static Logger logger = Logger.getLogger(ConnectorHandler.class);
	
	/** 
	 * Gets the Connector for a given Node
	 * @param node the node
	 * @return the list of connectors
	 */
	public ArrayList<Connector> getConnectors(Node node) {
		return createConnectorModel(node);
	}
	
	/**
	 * Creates the connector model based on the root Element
	 * obtained after processing the XML
	 * @param parentNode The parent node 
	 * @return The array list of smart environments
	 */
	private ArrayList<Connector> createConnectorModel(Object parentNode) {
		// Create a list of Smart Environments
		ArrayList<Connector> list = new ArrayList<Connector>();
		
		// Obtain the node list from the root
		NodeList nl = ((Node) parentNode).getChildNodes();
		
		// Visit all the nodes
		for (int i = 0; i < nl.getLength(); i++) {
			// Obtain the node
			Node node = nl.item(i);
 		   
			boolean isConnectionNode = ArrayUtils.contains(allowedNodes, node.getNodeName());

			if (isConnectionNode) {
				// Gets all possible parameters
				String name = getConnectorElement(node, NAME_ELEMENT);
				String description = getConnectorElement(node, DESC_ELEMENT);
				String image = getConnectorElement(node, IMAGE_ELEMENT);
				List<String> resourceLibraries = getResourceLibraries(node);

				// Create a smart environment object and populate it
				Connector con = new Connector();
				
				// Update the name
				if (name != null) {
					con.setName(name);
				}
				
				// Update the description
				if (description != null) {
					con.setDescription(description);
				}
				
				// Update the description
				if (resourceLibraries.size() > 0) {
					for (String reference : resourceLibraries) {
						con.addLibrary(reference);
					}
				}
				
				// Update the image
				if (image != null) {
					InputStream in = ResourceUtil.getInputStream(image);

					if (in != null) {
						con.setImage(new Image(null, in));
					} else {
						System.out.println("Cannot load image");
					}
				}

				// Adds the Connection to the list
				list.add(con);
				
			} else { // if it is not an smart environment node
	   		   	if (node.getNodeType() == ELEMENT_NODE) {
	   		   		list.addAll(createConnectorModel(node));
	   		   	}			
	   		}
		}
		return list;
	}

	/**
	 * Gets the element string
	 * @param node A node
	 * @param element The element to query
	 * @return A string with the element. <code>null</code> if not found
	 */
	private String getConnectorElement(Node node, String element) {
		String result = null;
		
		try {
			// Get the child node list
			NodeList nl = node.getChildNodes();
			
			for (int i = 0; i < nl.getLength(); i++) {
				Node child = nl.item(i);
				if (child.getNodeType() == ELEMENT_NODE && child.getNodeName() == element) {
					result = child.getTextContent();
				}
			}
		} catch (Exception ex) {
			System.out.println("There is an error obtaining the text: " + ex.toString());
			ex.printStackTrace();
		} 
		return result;
	}	

	/**
	 * Gets the element string
	 * @param node A node
	 * @param element The element to query
	 * @return A string with the element. <code>null</code> if not found
	 */
	private ArrayList<String> getConnectorElements(Node node, String element) {
		ArrayList<String> result = new ArrayList<String>();

		try {
			// Get the child node list
			NodeList nl = node.getChildNodes();
			
			for (int i = 0; i < nl.getLength(); i++) {
				Node child = nl.item(i);
				if (child.getNodeType() == ELEMENT_NODE && child.getNodeName() == element) {
					result.add(child.getTextContent());
				}
			}
		} catch (Exception ex) {
			System.out.println("There is an error obtaining the text: " + ex.toString());
			ex.printStackTrace();
		} 
		return result;
	}
	
	/**
	 * Gets the list of node elements
	 * @param node A node
	 * @return A string with the name. An empty list if not found
	 */
	private List<String> getResourceLibraries(Node node) {

		ArrayList<String> resourceLibraries = new ArrayList<String>();
		
		try {
			// Get the child node list
			NodeList nl = node.getChildNodes();
			
			for (int i = 0; i < nl.getLength(); i++) {
				Node child = nl.item(i);
				if (child.getNodeType() == ELEMENT_NODE && child.getNodeName() == RESOURCES_ELEMENT) {
					ArrayList<String> libraryElements = getConnectorElements(child, LIBRARY_ELEMENT);
					if (libraryElements != null) {
						resourceLibraries.addAll(libraryElements);
					}
				}
			}
		} catch (Exception ex) {
			System.out.println("There is an error obtaining the resource libraries: " + ex.toString());
			ex.printStackTrace();
		} 
		return resourceLibraries;
	}
}
