/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.wizard.project.pages.frames;

import java.util.Collection;
import java.util.HashMap;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.smool.sdk.common.nls.SmoolMessages;
import org.smool.sdk.ui.wizard.project.data.Platform;
import org.smool.sdk.ui.wizard.project.wizard.util.PlatformHandler;


/**
 * A class that gets from an URI the available connectors and shows it
 * in a tree viewer.
 * 
 * @author Fran Ruiz, ESI
 *
 */
public class PlatformSelectionArea {

	/** Widget definition */
	private Label platformLabel;
	
	/** The map for mapping buttons and the associated Platform object */
	private HashMap<Button,Platform> platformButtons = new HashMap<Button,Platform>();
	
    /** Defines the key of smool.properties */
    private static final String LOCATOR = "platform.locator";
    
    /** The Logger */
	//private static Logger logger = Logger.getLogger(PlatformSelectionArea.class);
	
	/**
	 * Create a new instance of a SmartEnvironmentSelectionArea.
	 * 
	 * @param composite The composite
	 */
	public PlatformSelectionArea(Composite composite) {
		//If it is a new project always start enabled
        createContents(composite);
    }
 
    /**
     * Create the area containing the check box tree viewer containing
     * the smart environment.
     * 
     * @param composite The composite
     */
    private void createContents(Composite composite) {
    	// Populates the model with the xml data obtained from 
    	// Smool properties 
    	System.out.println("Creating Connection Selection content");
    	PlatformHandler handler = new PlatformHandler(LOCATOR);
    	 
 		Composite platformContainer = new Composite(composite, SWT.NONE);
 		platformContainer.setLayout(new GridLayout());
 		platformContainer.setLayoutData(new GridData(GridData.FILL_BOTH));

    	platformLabel = new Label(platformContainer, SWT.NONE);
 		platformLabel.setText(SmoolMessages.PlatformSelectionArea_label);
 		platformLabel.setFont(platformContainer.getFont());

 		Composite platformGroupContainer = new Composite(platformContainer, SWT.BORDER);
    	platformGroupContainer.setLayout(new GridLayout());
    	platformGroupContainer.setLayoutData(new GridData(GridData.FILL_BOTH));
    	platformGroupContainer.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		 
 		for (Platform platform : handler.getModel()) {
 			Button platformButton = new Button(platformGroupContainer, SWT.RADIO);
 			platformButton.setText(platform.getName());
 			platformButton.setImage(platform.getImage()); 			
 			platformButton.setBackground(platformGroupContainer.getBackground());
 			platformButtons.put(platformButton, platform);
 		}
    }
    
    /**
     * Assigns the listener to all created buttons
     * @param listener a listener
     */
    public void setListener(Listener listener) {
 		for (Button platformButton : platformButtons.keySet()) {
 			platformButton.addListener(SWT.Selection, listener);
 		}
    }
    
    /**
     * Gets the platform widgets
     * @return the set of buttons added
     */
    public Collection<Button> getPlatformWidgets() {
    	return platformButtons.keySet();
    }
    
    /**
     * Gets the Platform corresponding to the selected button
     * @param button the selected button
     * @return the Platform element
     */
    public Platform getPlatform(Button button) {
    	return platformButtons.get(button);
    }
}
