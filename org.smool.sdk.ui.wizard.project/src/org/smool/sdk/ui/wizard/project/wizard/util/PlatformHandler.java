/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.wizard.project.wizard.util;

import static org.w3c.dom.Node.ELEMENT_NODE;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.graphics.Image;
import org.smool.sdk.common.sax.ResourceUtil;
import org.smool.sdk.ui.wizard.project.data.Connector;
import org.smool.sdk.ui.wizard.project.data.Platform;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * This class contains the information about the available platform.
 * This information will be used by the ADK Wizard to be displayed for the user
 * 
 * @author Fran Ruiz, Fran.Ruiz@tecnalia.com, Tecnalia
 *
 */
public class PlatformHandler extends XMLHandler {

	/** The list of platforms */
	private static ArrayList<Platform> platformList = new ArrayList<Platform>();
	
	private static final int ELEMENT_TYPE = Node.ELEMENT_NODE;
	
	/** The allowed nodes */
	private static String[] allowedNodes = {"platform"}; 
	
	/** The tag names */
	private static String NAME_ELEMENT = "name";
	private static String DESC_ELEMENT = "description";
	private static String IMAGE_ELEMENT = "icon";
	private static String CONNECTIONS_ELEMENT = "connections";
	private static String RESOURCES_ELEMENT = "resources";
	private static String LIBRARY_ELEMENT = "resource";
	
	/** The logger */
	//private static Logger logger = Logger.getLogger(PlatformHandler.class);
	
	/**
	 * The constructor for generate the platform model
	 * @param key The key corresponding to smool properties in what is
	 * contained the URL
	 */
	public PlatformHandler(String key) {
		super(key);
	}	
	
	/**
	 * Creates the smart environment model
	 */
	protected void read(Element rootElement) {
		platformList = new ArrayList<Platform>();
		platformList.addAll(createPlatformModel(rootElement));		
	}
	
	/**
	 * Determines if the element is an smart environment
	 * @param elementName the element name
	 * @return <code>true</code> if the element is an smart environment
	 */
	private boolean isPlatform(String elementName) {
		for (int i=0; i < allowedNodes.length; i++) {
			if ( elementName.equals(allowedNodes[i]) ) {
				return true;
			}
	    }
		return false;
	}	
	
	/**
	 * Creates the smart environment model based on the root Element
	 * obtained after processing the XML
	 * @param parentNode The parent node 
	 * @return The array list of smart environments
	 */
	private ArrayList<Platform> createPlatformModel(Object parentNode) {
		// Create a list of Smart Environments
		ArrayList<Platform> list = new ArrayList<Platform>();
		
		// Obtain the node list from the root
		NodeList nl = ((Node) parentNode).getChildNodes();
		
		// Visit all the nodes
		for (int i = 0; i < nl.getLength(); i++) {
			// Obtain the node
			Node node = nl.item(i);
 		   
			int type = node.getNodeType();
			
			if (type == ELEMENT_TYPE && isPlatform(node.getNodeName())) { 
			
				// Gets all possible parameters
				String name = getPlatformElement(node, NAME_ELEMENT);
				String description = getPlatformElement(node, DESC_ELEMENT);
				String image = getPlatformElement(node, IMAGE_ELEMENT);
				List<String> resourceLibraries = getResourceLibraries(node);
				List<Connector> connectors = getConnectors(node);

				// Create a smart environment object and populate it
				Platform platform = new Platform();
				
				// Set the name
				if (name != null) {
					platform.setName(name);
				}
				
				// Set the description
				if (description != null) {
					platform.setDescription(description);
				}
				
				// Set the resource libraries the description
				if (resourceLibraries.size() > 0) {
					for (String reference : resourceLibraries) {
						platform.addLibrary(reference);
					}
				}

				// Update the image
				if (image != null) {
					InputStream in = ResourceUtil.getInputStream(image);

					if (in != null) {
						platform.setImage(new Image(null, in));
					} else {
						System.out.println("Cannot load image");
					}
				}
				
				// And finally, add the list of submodels
				if (connectors.size() > 0) {
					for (Connector connector : connectors) {
						platform.addConnector(connector);
					}
				}
				
				// Adds the Smart Environment to the list
				list.add(platform);
				
			} else { // if it is not an smart environment node
	   		   	if (node.getNodeType() == ELEMENT_NODE) {
	   		   		list.addAll(createPlatformModel(node));
	   		   	}			
	   		}
		}
		return list;
	}


	/**
	 * Gets the generated model
	 * @return the model
	 */
	public ArrayList<Platform> getModel() {
		return platformList;
	}
	
	/**
	 * Gets the element string
	 * @param node A node
	 * @param element The element to query
	 * @return A string with the element. <code>null</code> if not found
	 */
	private String getPlatformElement(Node node, String element) {
		String result = null;
		
		try {
			// Get the child node list
			NodeList nl = node.getChildNodes();
			
			for (int i = 0; i < nl.getLength(); i++) {
				Node child = nl.item(i);
				if (child.getNodeType() == ELEMENT_NODE && child.getNodeName() == element) {
					result = child.getTextContent();
				}
			}
		} catch (Exception ex) {
			System.out.println("There is an error obtaining the text: " + ex.toString());
			ex.printStackTrace();
		} 
		return result;
	}

	/**
	 * Gets the element string
	 * @param node A node
	 * @param element The element to query
	 * @return A string with the element. <code>null</code> if not found
	 */
	private ArrayList<String> listPlatformElements(Node node, String element) {
		ArrayList<String> result = new ArrayList<String>();

		try {
			// Get the child node list
			NodeList nl = node.getChildNodes();
			
			for (int i = 0; i < nl.getLength(); i++) {
				Node child = nl.item(i);
				if (child.getNodeType() == ELEMENT_NODE && child.getNodeName() == element) {
					result.add(child.getTextContent());
				}
			}
		} catch (Exception ex) {
			System.out.println("There is an error obtaining the text: " + ex.toString());
			ex.printStackTrace();
		} 
		return result;
	}	
	/**
	 * Gets the list of node elements
	 * @param node A node
	 * @return A string with the name. An empty list if not found
	 */
	private List<String> getResourceLibraries(Node node) {

		ArrayList<String> resourceLibraries = new ArrayList<String>();
		
		try {
			// Get the child node list
			NodeList nl = node.getChildNodes();
			
			for (int i = 0; i < nl.getLength(); i++) {
				Node child = nl.item(i);
				if (child.getNodeType() == ELEMENT_NODE && child.getNodeName() == RESOURCES_ELEMENT) {
					ArrayList<String> libraryElements = listPlatformElements(child, LIBRARY_ELEMENT);
					if (libraryElements.size() > 0) {
						resourceLibraries.addAll(libraryElements);
					}
				}
			}
		} catch (Exception ex) {
			System.out.println("There is an error obtaining the resource libraries: " + ex.toString());
			ex.printStackTrace();
		} 
		return resourceLibraries;
	}
	
	/**
	 * Gets the list of node elements
	 * @param node A node
	 * @return A string with the name. An empty list if not found
	 */
	private List<Connector> getConnectors(Node node) {

		ArrayList<Connector> connectors = new ArrayList<Connector>();
		
		try {
			// Get the child node list
			NodeList nl = node.getChildNodes();
			
			for (int i = 0; i < nl.getLength(); i++) {
				Node child = nl.item(i);
				if (child.getNodeType() == ELEMENT_NODE && child.getNodeName() == CONNECTIONS_ELEMENT) {
					ConnectorHandler handler = new ConnectorHandler();
					connectors = handler.getConnectors(child);
				}
			}
		} catch (Exception ex) {
			System.out.println("There is an error obtaining the text: " + ex.toString());
			ex.printStackTrace();
		} 
		return connectors;
	}
}
