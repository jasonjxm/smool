/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.wizard.project.pages;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;
import org.smool.sdk.common.nls.SmoolMessages;
import org.smool.sdk.common.properties.PropertyLoader;
import org.smool.sdk.ui.wizard.project.ProjectWizard;
import org.smool.sdk.ui.wizard.project.data.Connector;
import org.smool.sdk.ui.wizard.project.data.Platform;
import org.smool.sdk.ui.wizard.project.pages.frames.ConnectorSelectionArea;
import org.smool.sdk.ui.wizard.project.pages.frames.PlatformSelectionArea;


/**
 * This class implements the UI page of the ADK Wizard where
 * we can determine the application domain of the Knowledge Processor
 * 
 * @author Fran Ruiz, Fran.Ruiz@tecnalia.com, Tecnalia
 *
 */
public class PlatformSelectionPage extends WizardPage {

	private static final String HELP_BUNDLE = "bundle.help";

	private static final String HELP_CONTENT_KEY = "platform";

	/** The Connector Selection Area */
	private ConnectorSelectionArea connectors;
	
	/** The Platform Selection Area */
	private PlatformSelectionArea platforms;
	
	/** The Workbench instance */
	protected IWorkbench workbench;	
	
	/** workbench selection when the wizard was started */
	protected IStructuredSelection selection;
	
	/** Listener for the radio selection */
	Listener platformSelectionListener = new Listener() {
		public void handleEvent(Event event) {
			Button selectedRadio = (Button) event.widget;

			for (Button platformButton : platforms.getPlatformWidgets()) {
    	    	if (platformButton.equals(selectedRadio)) {
    	    		platformButton.setSelection(true);
    	    		Platform selectedPlatform = platforms.getPlatform(platformButton);
    	    		((ProjectWizard)getWizard()).getModel().setPlatform(selectedPlatform);
    	    		connectors.setAvailableConnectors(selectedPlatform.listConnectors());
    	    	} else {
    	    		platformButton.setSelection(false);
    	    	}
			}
			
			connectors.setListener(connectorSelectionListener);

			// check the validity of the page content
			boolean valid = validatePage();
			setPageComplete(valid);
			
		}
	};
	
	/** Listener for the connector selection */
	private Listener connectorSelectionListener = new Listener() {
		public void handleEvent(Event event) {
			Button selectedCheck = (Button) event.widget;

			for (Button connectorButton : connectors.getPlatformWidgets()) {
    	    	if (connectorButton.equals(selectedCheck)) {
    	    		if (selectedCheck.getSelection()) {
	    	    		Connector selectedConnector = connectors.getConnector(connectorButton);
	    	    		((ProjectWizard)getWizard()).getModel().addConnector(selectedConnector);
    	    		} else {
	    	    		Connector selectedConnector = connectors.getConnector(connectorButton);
	    	    		((ProjectWizard)getWizard()).getModel().removeConnector(selectedConnector);
    	    		}
    	    	}
			}
			
			// check the validity of the page content
			boolean valid = validatePage();
			setPageComplete(valid);
			
		}
	};	
	
	/**
	 * Constructor for Connection and Platform Page.
	 *  @param workbench the current workbench
	 *  @param selection the current object selection
	 */
	public PlatformSelectionPage(IWorkbench workbench, IStructuredSelection selection) {
		super(SmoolMessages.ConnectorPlatformPage_windowTitle);
		setTitle(SmoolMessages.ConnectorPlatformPage_pageTitle);
		setDescription(SmoolMessages.ConnectorPlatformPage_pageDescription);
		this.workbench = workbench;
		this.selection = selection;
	}	
	
	/**
	 * Creates the wizard page components
	 * @param parent the parent composite
	 */
	@Override
	public void createControl(Composite parent) {
		// Creates a new composite
		Composite container = new Composite(parent, SWT.NULL);
		container.setFont(parent.getFont());
		
		initializeDialogUnits(parent);
		
		String helpBundle = PropertyLoader.getInstance().getProperty(HELP_BUNDLE);
		StringBuffer helpContents = new StringBuffer();
		helpContents.append(helpBundle).append(".").append(HELP_CONTENT_KEY);
		
		// Help system
		PlatformUI.getWorkbench().getHelpSystem().setHelp(container, helpContents.toString());

   	 	GridLayout layout = new GridLayout();

   	 	int columns = 2;
   	 	layout.numColumns = columns;
   	 	layout.makeColumnsEqualWidth = true;
   	 	
		container.setLayout(layout);
		container.setLayoutData(new GridData(GridData.FILL_BOTH));   	 	

		platforms = new PlatformSelectionArea(container);
		platforms.setListener(platformSelectionListener);
		
		connectors = new ConnectorSelectionArea(container);

		setPageComplete(validatePage());

        // Show description on opening
        setErrorMessage(null);
        
        setMessage(null);
        setControl(container);
	}

 	/**
 	 * Gets the Connection Selection area
 	 * @return the connectionSelectionArea object
 	 */
 	public ConnectorSelectionArea getConnectionTree() {
 		return connectors;
 	}
 	
 	/**
	 * Gets the Platform Selection area
	 * @return the platformSelectionArea object
	 */
	public PlatformSelectionArea getPlatformTree() {
		return platforms;
	}

    /**
     * Returns whether this page's controls currently all contain valid 
     * values.
     *
     * @return <code>true</code> if all controls are valid, and
     * <code>false</code> if at least one is invalid
     */
    protected boolean validatePage() {
   	 
	   	 // sets the error messages to null
	   	 setErrorMessage(null);
	   	 
	   	 // validates the project reference
	   	 if (!validatePlatformSelection()) {
	   		 return false;
	   	 }
	   	 
	   	 // validates the package name
	   	 if (!validateConnectorSelection()) {
	   		 return false;
	   	 }
	   	 
        // if the validation is correct, then return the default message
        setMessage(SmoolMessages.ConnectorPlatformPage_validationDefaultMessage);
   	 
        return true;
    }
    
    /**
     * Validates the selection of a platform
     * @return <code>true</code> if the platform selection has made
     */
    private boolean validatePlatformSelection() {

        if (!((ProjectWizard)getWizard()).getModel().hasSelectedPlatform()) { 
       	 setErrorMessage(null);
       	 setMessage(SmoolMessages.ConnectorPlatformPage_validationPlatformNotSelected);
            return false;
        }

        return true;
    }
    
    /**
     * Validates the selection of a connector
     * @return <code>true</code> if the connector selection has made
     */
    private boolean validateConnectorSelection() {

        if (!((ProjectWizard)getWizard()).getModel().hasSelectedConnectors()) { 
       	 setErrorMessage(null);
       	 setMessage(SmoolMessages.ConnectorPlatformPage_validationConnectorNotSelected);
            return false;
        }

        return true;
    }    
    
}
