/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.ui.wizard.project.wizard.util;

import static org.w3c.dom.Node.ELEMENT_NODE;

import java.util.ArrayList;
import java.util.List;

import org.smool.sdk.ui.wizard.project.data.Model;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * This class contains the information about available model layers.
 * This information will be used by the ADK Wizard to be displayed for the user
 * 
 * @author Fran Ruiz, Fran.Ruiz@tecnalia.com, Tecnalia
 *
 */
public class ModelHandler extends XMLHandler {

	/** The list of platforms */
	private static ArrayList<Model> modelList = new ArrayList<Model>();
	
	private static final int ELEMENT_TYPE = Node.ELEMENT_NODE;
	
	/** The allowed nodes */
	private static String[] allowedNodes = {"model"}; 
	
	/** The tag names */
	private static String NAME_ELEMENT = "name";
	private static String DESC_ELEMENT = "descriptor";
	private static String DEFAULT_ELEMENT = "default";
	private static String RESOURCES_ELEMENT = "resources";
	private static String LIBRARY_ELEMENT = "resource";
	
	/**
	 * The constructor for generate the platform model
	 * @param key The key corresponding to smool properties in what is
	 * contained the URL
	 */
	public ModelHandler(String key) {
		super(key);
	}	
	
	/**
	 * Creates the smart environment model
	 */
	protected void read(Element rootElement) {
		modelList = new ArrayList<Model>();
		modelList.addAll(createModel(rootElement));		
	}
	
	/**
	 * Determines if the element is a model
	 * @param elementName the element name
	 * @return <code>true</code> if the element is a model
	 */
	private boolean isModel(String elementName) {
		for (int i=0; i < allowedNodes.length; i++) {
			if ( elementName.equals(allowedNodes[i]) ) {
				return true;
			}
	    }
		return false;
	}	
	
	/**
	 * Creates the smart environment model based on the root Element
	 * obtained after processing the XML
	 * @param parentNode The parent node 
	 * @return The array list of smart environments
	 */
	private ArrayList<Model> createModel(Object parentNode) {
		// Create a list of Smart Environments
		ArrayList<Model> list = new ArrayList<Model>();
		
		// Obtain the node list from the root
		NodeList nl = ((Node) parentNode).getChildNodes();
		
		// Visit all the nodes
		for (int i = 0; i < nl.getLength(); i++) {
			// Obtain the node
			Node node = nl.item(i);
 		   
			int type = node.getNodeType();
			
			if (type == ELEMENT_TYPE && isModel(node.getNodeName())) { 
			
				// Gets all possible parameters
				String name = getModelElement(node, NAME_ELEMENT);
				String descriptor = getModelElement(node, DESC_ELEMENT);
				String defaultStr = getModelElement(node, DEFAULT_ELEMENT);
				List<String> resourceLibraries = getResourceLibraries(node);

				// Create a model object and populate it
				Model model = new Model();
				
				// Set the name
				if (name != null) {
					model.setName(name);
				}
				
				// Set the descriptor
				if (descriptor != null) {
					model.setDescriptor(descriptor);
				}
				
				// Set the resource libraries the description
				if (resourceLibraries.size() > 0) {
					for (String reference : resourceLibraries) {
						model.addLibrary(reference);
					}
				}

				// default checked
				if (defaultStr != null) {
					model.setDefaultChecked(Boolean.parseBoolean(defaultStr));					
				}
				
				// Adds the Smart Environment to the list
				list.add(model);
				
			} else { // if it is not an smart environment node
	   		   	if (node.getNodeType() == ELEMENT_NODE) {
	   		   		list.addAll(createModel(node));
	   		   	}			
	   		}
		}
		return list;
	}


	/**
	 * Gets the generated model
	 * @return the model
	 */
	public ArrayList<Model> getModel() {
		return modelList;
	}
	
	/**
	 * Gets the element string
	 * @param node A node
	 * @param element The element to query
	 * @return A string with the element. <code>null</code> if not found
	 */
	private String getModelElement(Node node, String element) {
		String result = null;
		
		try {
			// Get the child node list
			NodeList nl = node.getChildNodes();
			
			for (int i = 0; i < nl.getLength(); i++) {
				Node child = nl.item(i);
				if (child.getNodeType() == ELEMENT_NODE && child.getNodeName() == element) {
					result = child.getTextContent();
				}
			}
		} catch (Exception ex) {
			System.out.println("There is an error obtaining the text: " + ex.toString());
			ex.printStackTrace();
		} 
		return result;
	}

	/**
	 * Gets the element string
	 * @param node A node
	 * @param element The element to query
	 * @return A string with the element. <code>null</code> if not found
	 */
	private ArrayList<String> listModelElements(Node node, String element) {
		ArrayList<String> result = new ArrayList<String>();

		try {
			// Get the child node list
			NodeList nl = node.getChildNodes();
			
			for (int i = 0; i < nl.getLength(); i++) {
				Node child = nl.item(i);
				if (child.getNodeType() == ELEMENT_NODE && child.getNodeName() == element) {
					result.add(child.getTextContent());
				}
			}
		} catch (Exception ex) {
			System.out.println("There is an error obtaining the text: " + ex.toString());
			ex.printStackTrace();
		} 
		return result;
	}	
	/**
	 * Gets the list of node elements
	 * @param node A node
	 * @return A string with the name. An empty list if not found
	 */
	private List<String> getResourceLibraries(Node node) {

		ArrayList<String> resourceLibraries = new ArrayList<String>();
		
		try {
			// Get the child node list
			NodeList nl = node.getChildNodes();
			
			for (int i = 0; i < nl.getLength(); i++) {
				Node child = nl.item(i);
				if (child.getNodeType() == ELEMENT_NODE && child.getNodeName() == RESOURCES_ELEMENT) {
					ArrayList<String> libraryElements = listModelElements(child, LIBRARY_ELEMENT);
					if (libraryElements.size() > 0) {
						resourceLibraries.addAll(libraryElements);
					}
				}
			}
		} catch (Exception ex) {
			System.out.println("There is an error obtaining the resource libraries: " + ex.toString());
			ex.printStackTrace();
		} 
		return resourceLibraries;
	}	
}
