/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.smool.sdk.core.generator.kpi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.launching.JavaRuntime;
import org.eclipse.ui.IPerspectiveDescriptor;
import org.eclipse.ui.IPerspectiveRegistry;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.smool.sdk.common.sax.ResourceUtil;
import org.smool.sdk.ui.perspective.PerspectiveActivator;
import org.smool.sdk.ui.perspective.SmoolNature;
import org.smool.sdk.ui.wizard.project.ProjectWizardActivator;
import org.smool.sdk.ui.wizard.project.data.WizardData;


/**
 * This class implements the interface ISmoolProject that exposes the methods
 * to create a new project in Eclipse platform.
 * 
 * @author Fran Ruiz, Fran.Ruiz@tecnalia.com, Tecnalia
 */
public class SmartAppProject {

	/** The Project representation in Workspace */
	private IProject project;
	
	/** The Java Project representation in Workspace */
	private IJavaProject javaProject;
	  
	/** The ADK Model **/
	private WizardData wizardModel;
	
	/** The logger */
	//private static Logger logger;
	
	/**
	 * Constructor of SmartAppProject. 
	 * @throws CoreException 
	 */
	public SmartAppProject() {
		//logger = Logger.getLogger(SmartAppProject.class);
	}	
	
	/**
	 * Constructor of SmartAppProject. 
	 * @throws CoreException 
	 */
	public SmartAppProject(WizardData model) throws CoreException {
		//logger = Logger.getLogger(SmartAppProject.class);
		
		this.wizardModel = model;
	}
	
	public void generate() {
		System.out.println("Generating the smart application project...");
		try {
			createProject(wizardModel, new NullProgressMonitor()) ;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}
	  
    /**
     * Create a new Smart Application Project.
     * @param model The ADK Model with the selection made in the wizard
     * @param monitor the progress monitor
     * @throws CoreException when core exception is throwed 
     * @throws IOException when some i/o problem arised
     * @throws MalformedURLException when malformed ulr exception arised
	 */
	private void createProject(WizardData model, IProgressMonitor monitor) 
			throws CoreException, MalformedURLException, IOException { 

		System.out.println("Initiating the Smart Application project creation...");
		  
		// Variable used to count the total size of tasks for the progress monitor
		int totalTasks = 12; 

		if (model.getProjectName() != null) {
			// Start the monitoring with 12 tasks
			monitor.beginTask("Creating the Smart Application Project", totalTasks);

			// Task 1: Creates a new java project
			monitor.worked(1);//--------------------------------------------------------------------
			monitor.setTaskName("Creating a new project...");

			// Gets the workspace root
			IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
			
			// Gets the IProject object
			this.project = root.getProject(model.getProjectName());
	    
			if (!model.isDefaultLocation()) {
				System.out.println("The path is set to " + model.getPath().toOSString());

				IProjectDescription description = 
					ResourcesPlugin.getWorkspace().newProjectDescription(model.getProjectName());

				description.setLocation(model.getPath());				

				// And creates it
				getProject().create(description, null);
			} else { // when the default location is set, create the project
				System.out.println("No path was set in the project");
				// And creates it
				getProject().create(null);
			}

			// Open it
			getProject().open(null);
			
			// TODO [FJR] Depending on the selection made in the wizard, 
			// we have to create a Java project, a C project or a Python project
			javaProject = JavaCore.create(getProject());
			
			// Creation of project structure
			// Task 2: Creates the binaries folder
			monitor.worked(1);//--------------------------------------------------------------------
			monitor.setTaskName("Creating project structure...");
			// Binary folder
			IFolder binFolder = createBinFolder();
			
			// Setting the Smool Nature
			monitor.worked(1);//--------------------------------------------------------------------
			monitor.setTaskName("Setting the SMOOL nature...");
			setSMOOLNature();

			// Creates the class path
			javaProject.setRawClasspath(new IClasspathEntry[0], null);
			
			// Set the Java compile
			javaProject.setOption(JavaCore.COMPILER_COMPLIANCE, JavaCore.VERSION_1_5);
			
			// Creates the output folder
			createOutputFolder(binFolder);
			
			// Creates the source folder
			createSourceFolder();
		    
			// Task 7: Adds the Java System Libraries
			monitor.worked(1);//--------------------------------------------------------------------
			monitor.setTaskName("Adding system and smart application libraries...");
			addSystemLibraries();
			  
			// Creates the library folder
			IFolder libFolder = createLibFolder();

			// Adding the libraries
			for (String library : model.listAllLibraries()) {
				this.addJar(libFolder, ProjectWizardActivator.PLUGIN_ID, library);
			}
			 
			// Adding the libraries
			//this.addLoggingLibraries(libFolder);
			
			monitor.worked(1);//--------------------------------------------------------------------
			monitor.setTaskName("Creating the semantic model for the smart application...");
			
			// Task 11: Building
			monitor.worked(1);//--------------------------------------------------------------------
			monitor.setTaskName("Building the recently created project");
			// Added by FJR
			openPerspective();
			
			build();
		}
	}

	/**
	 * This method converts a InputStream to String
	 * @param is The InputStream
	 * @return a string representing the content of the inputstream
	 * @throws IOException
	 */
	@SuppressWarnings("unused")
	private String convertStreamToString(InputStream is) throws IOException { 
		if (is != null) {
			StringBuilder sb = new StringBuilder();
			String line;
			try { 
				BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8")); 
	
				while ((line = reader.readLine()) != null) { 
					sb.append(line).append("\n"); 
				} 
			} finally { 
				is.close(); 
			} 
			
			return sb.toString(); 
		} else {         
			return ""; 
		} 
	} 	
	
	/**
	 * Gets the recently created project reference
	 * @return the IProject reference
	 */
	private IProject getProject() {
	    return project;
	}

	/**
	 * Creates the binaries folder
	 * @return IFolder structure for binaries
	 * @throws CoreException when some error in bin folder creation arises
	 */
	private IFolder createBinFolder() throws CoreException {
		IFolder binFolder = getProject().getFolder("bin");
	    binFolder.create(false, true, null);
	    return binFolder;
	}

	/**
	 * Creates the library folder
	 * @return IFolder structure for imported libraries
	 * @throws CoreException when some error in lib folder creation arises
	 */
	private IFolder createLibFolder() throws CoreException {
		IFolder libFolder = getProject().getProject().getFolder("lib");
	    libFolder.create(false, true, null);
	    return libFolder;
	}
	
	/**
	 * Sets the SMOOL Nature to the project
	 * @throws CoreException when some error in setting smool nature arises
	 */
	private void setSMOOLNature() throws CoreException {
	   try {
		   IProjectDescription description = project.getDescription();
		   String[] natures = description.getNatureIds();
		   String[] newNatures = new String[natures.length + 2];
		   System.arraycopy(natures, 0, newNatures, 0, natures.length);
		   newNatures[natures.length] = SmoolNature.NATURE_ID;
		   newNatures[natures.length+1] = JavaCore.NATURE_ID;
		   //newNatures[natures.length+1] = SofiaNature.NATURE_ID;
		   IStatus status = ResourcesPlugin.getWorkspace().validateNatureSet(natures);

		   // check the status and decide what to do
		   if (status.getCode() == IStatus.OK) {
			   description.setNatureIds(newNatures);
			   getProject().setDescription(description, null);
		   } else {
			   System.out.println("Nature is not a valid nature for the current workspace.");
			   throw new CoreException(status);
		   }
	   } catch (CoreException cex) {
		   System.out.println("Cannot set SMOOL Nature");
		   cex.printStackTrace();
	   }
	}

	/**
	 * Creates the output folder
	 * @param binFolder The binaries folder
	 * @throws JavaModelException
	 */
	private void createOutputFolder(IFolder binFolder) throws JavaModelException {
	    IPath outputLocation = binFolder.getFullPath();
	    javaProject.setOutputLocation(outputLocation, null);
	}

	/**
	 * Creates the source folder
	 * @return the IPackageFragmentRoot that represents the source folder
	 * @throws CoreException when some error arises
	 */
	private IPackageFragmentRoot createSourceFolder() throws CoreException {
	    IFolder folder = getProject().getFolder("src");
	    folder.create(false, true, null);
	    IPackageFragmentRoot root = javaProject.getPackageFragmentRoot(folder);

	    // TODO [FJR] COMPROBAR
	    IClasspathEntry[] oldEntries = javaProject.getRawClasspath();
	    IClasspathEntry[] newEntries = new IClasspathEntry[oldEntries.length + 1];
	    System.arraycopy(oldEntries, 0, newEntries, 0, oldEntries.length);
	    newEntries[oldEntries.length] = JavaCore.newSourceEntry(root.getPath());
	    javaProject.setRawClasspath(newEntries, null);
	    return root;
	}

	/**
	 * Adds the Java Classes
	 * @throws JavaModelException
	 */
	private void addSystemLibraries() throws JavaModelException {
		// TODO [FJR] Establish user selected platform
		IClasspathEntry[] oldEntries = javaProject.getRawClasspath();
		IClasspathEntry[] newEntries = new IClasspathEntry[oldEntries.length + 1];
	    System.arraycopy(oldEntries, 0, newEntries, 0, oldEntries.length);
	    newEntries[oldEntries.length] = JavaRuntime.getDefaultJREContainerEntry();
	    javaProject.setRawClasspath(newEntries, null);
	}

//	/**
//	 * Adds the logging libraries to the project
//	 * @throws IOException 
//	 * @throws JavaModelException 
//	 */
//	private void addLoggingLibraries (IFolder libFolder) throws IOException, JavaModelException {
//	    try {
//			IPath result = findResourceInPlugin(libFolder, ProjectWizardActivator.PLUGIN_ID, "/lib/log4j-1.2.13.jar");
//		    IClasspathEntry[] oldEntries = javaProject.getRawClasspath();
//		    IClasspathEntry[] newEntries = new IClasspathEntry[oldEntries.length + 1];
//		    System.arraycopy(oldEntries, 0, newEntries, 0, oldEntries.length);
//		    newEntries[oldEntries.length] = JavaCore.newLibraryEntry(result, null,
//		         null);
//		    javaProject.setRawClasspath(newEntries, null);
//	    } catch (IOException ex) {
//	    	System.out.println("I/O Exception during finding of the Jar file.");
//	    	throw new IOException(ex);
//	    } catch (JavaModelException ex) {
//	    	System.out.println("JavaModelException during finding of the Jar file.");
//	    	throw new JavaModelException(ex);
//	    } catch (Exception ex) {
//	    	System.out.println("Exception during finding of the Jar file. ");
//	    	ex.printStackTrace();
//	    }
//	}
	
	/**
	 * Adds a jar file to the project
	 * @param plugin the plugin in what is located the plugin
	 * @param jar the jar reference
	 * @throws MalformedURLException when the jar reference is invalid
	 * @throws IOException when cannot create the jar
	 * @throws JavaModelException when there is a JavaModelException
	 */
	private void addJar (IFolder libFolder, String plugin, String jar) throws MalformedURLException,
			IOException, JavaModelException {
		System.out.println("Finding " + plugin + " in " + jar);
	    try {
			IPath result = findResourceInPlugin(libFolder, plugin, jar);
		    IClasspathEntry[] oldEntries = javaProject.getRawClasspath();
		    IClasspathEntry[] newEntries = new IClasspathEntry[oldEntries.length + 1];
		    System.arraycopy(oldEntries, 0, newEntries, 0, oldEntries.length);
		    newEntries[oldEntries.length] = JavaCore.newLibraryEntry(result, null,
		         null);
		    javaProject.setRawClasspath(newEntries, null);
	    } catch (IOException ex) {
	    	System.out.println("I/O Exception during finding of the Jar file " + jar);
	    	throw new IOException(ex);
	    } catch (JavaModelException ex) {
	    	System.out.println("JavaModelException during finding of the Jar file " + jar);
	    	throw new JavaModelException(ex);
	    } catch (Exception ex) {
	    	System.out.println("Exception during finding of the Jar file " + jar);
	    	ex.printStackTrace();
	    }
	}
	
	/**
	 * Finds a file in a plugin. Used for import of jar files
	 * @param pluginId the plugin identifier
	 * @param fileName the filename to find out
	 * @return the absolute path
	 * @throws MalformedURLException when some error arises
	 * @throws IOException when some io error arises
	 */
	protected IPath findResourceInPlugin (IFolder libFolder, String pluginId, String fileName)
			throws MalformedURLException, IOException {
		
		IPath absolutePath = null;
		System.out.println("Finding resource " + fileName);

	    // convert from URL to an IPath
		InputStream is = ResourceUtil.getInputStream(fileName);
		
		if (is != null) {
		    IFile libFile = libFolder.getFile(fileName.substring(fileName.lastIndexOf("/")));
		    
		    copyJarToNewFile(is, libFile); 
		    absolutePath = libFile.getFullPath();
		} else {
			System.out.println("The URL cannot be obtained for file " + fileName);
		}
	    return absolutePath;
	}

	/**
	 * Copies a jar file to a destination location
	 * @param is The input stream
	 * @param newFile the destination file
	 */
	private void copyJarToNewFile(InputStream is, IFile newFile) {

        try {
    	    newFile.create(is, true, null);
    	    
        } catch (CoreException ex) {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ex2) {
                    System.out.println("Some error occurred");
                }
            }
        } catch (Exception ex) {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ex2) {
                    System.out.println("Some error occurred");
                }
            }
        }
    }

	/**
	 * Opens the SMOOL perspective in the recently created project
	 */
	private void openPerspective() {
		// TODO [FJR] Here, open the perspective.
		
		PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {
			public void run() {
				try {
					System.out.println("Getting perspective registry...");
					IPerspectiveRegistry perspectiveRegistry = PlatformUI.getWorkbench().getPerspectiveRegistry();
	
					IPerspectiveDescriptor[] perspectiveList = perspectiveRegistry.getPerspectives();
					
					for (int i = 0; i < perspectiveList.length; i++) {
						IPerspectiveDescriptor myPerspective = perspectiveList[i];
						System.out.println("Perspective " + myPerspective.getId());
					}
					
					System.out.println("Setting default perspective...");
					perspectiveRegistry.setDefaultPerspective(PerspectiveActivator.PLUGIN_ID + ".sofiaperspective");
					
					System.out.println("Getting SMOOL perspective descriptor...");
					IPerspectiveDescriptor smoolPerspective =
						perspectiveRegistry.findPerspectiveWithId(PerspectiveActivator.PLUGIN_ID + ".sofiaperspective");
	
					System.out.println("Getting active workbench window...");
					IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
					
					if (window == null) {
						System.out.println("Active workbench window is null");
						return;
					} 
					
					System.out.println("Getting active page...");
					IWorkbenchPage page = window.getActivePage();
					if (page == null) {
						System.out.println("Active page is null");
						return;
					}
					System.out.println("Setting perspective...");
					page.setPerspective(smoolPerspective);
					System.out.println("Done perspective stuff!");
				} catch (Exception ex) {
					System.out.println("Cannot get current active page. Reason: " + ex.getMessage());
					ex.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Builds the current project
	 */
	protected void build() {
		try {
			IProgressMonitor myProgressMonitor = null;
			project.build(IncrementalProjectBuilder.INCREMENTAL_BUILD, myProgressMonitor);
		} catch (CoreException e) {
			e.printStackTrace();
		};
	}
}
